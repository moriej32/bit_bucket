<!-- Start Layout Wrapper -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="layout-2-wrapper<?php if( $x%2 == 0 ){ echo ' solid-bg'; } ?>">

        <div class="grid-6 first">
		    <div class="galleryOverlay" style="display:none;">
								   <?php 
								       // global $mpcrf_options;
									//	$mpcrf_optionsss = get_option('rfbwp_options',true);
										$post = get_post($postID); 
                                        $bookidsss = $post->post_name;
										
										 $bid = strtolower(str_replace(" ", "", get_the_title( $postID )));	
										 
										echo $valuetest =  do_shortcode( '[responsive-flipbooksss id="'. $bid .'"]' );
                                         
                                            ?>
											
				 </div>
            <div class="image-holder galleryImgCont">
				<a href="<?php echo get_permalink(); ?>">
				<!--<div class="hover-item"></div>-->
				</a>
				<?php
				 if( has_post_thumbnail() ){
					the_post_thumbnail('small-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px" )  );
				 }else{
					echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
				 }

				?>

            </div>
        </div>

        <div class="layout-2-details">
            <h3 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <div>File Type: Book</div>
            <?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
            <div class="video_excerpt_text"><?php echo remove_p(get_the_excerpt()); ?></div>
            <ul class="stats">
                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                <li><?php videopress_countviews( get_the_ID() ); ?></li>
                <li><i class="fa fa-wrench"></i>
                <?php
                    $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
                    echo count($result);
                ?> Endorsments</li>
                <li><i class="fa  fa-heart"></i>
                <?php
                  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
                  $result1 = $wpdb->get_results($sql_aux);
                  echo count($result1);
                ?> Favorites</li>
                <!--end favorite-->
                <li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
                <li><?php comments_number() ?></li>
            </ul>
            <div class="clear"></div>
            <p><?php echo videopress_content('240'); ?></p>
            <?php if($profile_id && $profile_id == get_current_user_id()){ ?>
            <p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i></a></p>
            <?php } ?>
        </div>
         <?php
                $postID=$post->ID;

                $book_author = get_the_author_meta( 'display_name', $post->post_author );

                $refr = get_post_meta($postID,'video_options_refr',true);
                 if($refr){}
                 else{$refr= 'No reference given'; }

                $thumbnail_id=get_post_thumbnail_id($postID);
                $thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
                $imgsrc= $thumbnail_url [0];

                $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");
        ?>


        <div class="clear"></div>

        <ul class="bottom-detailsul hidden">
         <li>
           <a class="detailsblock-btn" data-postID="<?php echo $postID; ?>" data-modal-id="videosModal" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($post->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $post->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo count($result); ?>" data-favr="<?php echo count($result1); ?> " data-revs="<?php echo $revnum ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a>
         </li>
        </ul>
    </div>
</div>
<!-- End Layout Wrapper -->