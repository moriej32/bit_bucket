<?php function layout3( $videopress_title, $videopress_categoryid, $videopress_items, $videopress_banner, $videopress_banner_link ){ ?>

<h6 class="title-bar"><span><?php echo $videopress_title; ?></span> <a href="<?php echo get_category_link( $videopress_categoryid ); ?>" title="View All"><i class="fa fa-angle-right"></i></a></h6>

    <!-- Start Categorized post -->
    <div class="layout-3 desktop-view">
    <?php
	// Set Variable
	$spacingcounter = 0;
	$gridcounter = 0;
	$post_item = 8;
	
	$args = array(
	'orderby'  => 'date',
	'order'    => 'DESC',
	'posts_per_page' => $videopress_items,
	'cat'	=> $videopress_categoryid,
	);
								
	query_posts($args);
	while ( have_posts() ) : the_post();
	$spacingcounter++;
	$gridcounter++;
	?>
    
    <?php if( $spacingcounter%5 == 0 ){ echo '<div class="clear"></div><div class="spacing-20"></div>'; } ?>
    
    <div class="grid-one-fourth <?php if( $gridcounter == '1' ){ echo 'first'; }elseif( $gridcounter == '4' ){ echo 'last'; $gridcounter = 0; } ?>">
    
    	<div class="image-holder">
        <a href="<?php echo get_permalink(); ?>">
        <div class="hover-item"></div>
        </a>
        
    	<?php
		if( has_post_thumbnail() ){
        	the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
		}else{
			echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
		}
		?>
        
        </div>
    
    <ul class="stats">
    <li><?php videopress_countviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    </div>
    
    <?php endwhile; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    <?php require('responsive-layout.php'); ?>
    
    <div class="spacing-40"></div>
    
    <?php
	// Show Banner Ads in layout
	if( $videopress_banner != '' || $videopress_banner_link != '' ){
    echo '<div class="layout-banner-ads">
		 <a href="'.$videopress_banner_link.'"><img src="'.$videopress_banner.'" alt="Banner Ads"></a>
		 </div>';
	echo '<div class="spacing-40"></div>';
	}
	?>
    
<?php } ?>