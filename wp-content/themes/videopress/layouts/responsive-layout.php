
    <!-- Start Categorized post -->
    <div class="layout-2 responsive-view">
    <?php
	// Set Variable
	$x = 0;
	
	$args = array(
	'orderby'  => 'date',
	'order'    => 'DESC',
	'posts_per_page' => $post_item,
	'cat'	=> $videopress_categoryid,
	);
								
	query_posts($args);
	while ( have_posts() ) : the_post();
	$x++;
	?>
    <!-- Start Layout Wrapper -->
    <div class="layout-2-wrapper<?php if( $x%2 == 0 ){ echo ' solid-bg'; } ?>">
    	
        <div class="grid-6 first">
            <div class="image-holder">
            <a href="<?php echo get_permalink(); ?>">
            <div class="hover-item"></div>
            </a>
            
            <?php
			if( has_post_thumbnail() ){
            	the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
			}else{
				echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
			}
			?>
            
            </div>
        </div>
    
        <div class="layout-2-details">
        <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
        <ul class="stats">
            <li><?php videopress_countviews( get_the_ID() ); ?></li>
            <li><?php comments_number() ?></li>
            <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
        </ul>
        <div class="clear"></div>
        <p><?php echo videopress_content('240'); ?></p>
        </div>
    
    <div class="clear"></div>
    </div>
    <!-- End Layout Wrapper -->
    <?php endwhile; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->