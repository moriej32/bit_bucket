<?php function layout2( $videopress_title, $videopress_categoryid, $videopress_items, $videopress_banner, $videopress_banner_link ){ ?>

<h6 class="title-bar"><span><?php echo $videopress_title; ?></span> <a href="<?php echo get_category_link( $videopress_categoryid ); ?>" title="View All"><i class="fa fa-angle-right"></i></a></h6>



    <!-- Start Categorized post -->
    <div class="layout-2">
    <?php
	// Set Variable
	$x = 0;
	
	$args = array(
	'orderby'  => 'date',
	'order'    => 'DESC',
	'posts_per_page' => $videopress_items,
	'cat'	=> $videopress_categoryid,
	);
								
	query_posts($args);
	while ( have_posts() ) : the_post();
	$x++;
	?>
    <!-- Start Layout Wrapper -->
    <div class="layout-2-wrapper<?php if( $x%2 == 0 ){ echo ' solid-bg'; } ?>">
    	
        <div class="grid-6 first">
            <div class="image-holder">
            <a href="<?php echo get_permalink(); ?>">
            <div class="hover-item"></div>
            </a>
            
            <?php
			if( has_post_thumbnail() ){
            	the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
			}else{
				echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
			}
			?>
            
            </div>
        </div>
    
        <div class="layout-2-details">
        <h3 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
        <ul class="stats">
            <li><?php videopress_countviews( get_the_ID() ); ?></li>
            <li><?php comments_number() ?></li>
            <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
        </ul>
        <div class="clear"></div>
        <p><?php echo videopress_content('240'); ?></p>
        </div>
    
    <div class="clear"></div>
    </div>
    <!-- End Layout Wrapper -->
    <?php endwhile; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    <div class="spacing-30"></div>
    
    <?php
	// Show Banner Ads in layout
	if( $videopress_banner != '' || $videopress_banner_link != '' ){
    echo '<div class="layout-banner-ads">
		 <a href="'.$videopress_banner_link.'"><img src="'.$videopress_banner.'" alt="Banner Ads"></a>
		 </div>';
	echo '<div class="spacing-40"></div>';
	}
	?>
    
<?php } ?>