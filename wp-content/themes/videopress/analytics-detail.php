<?php /* Template Name: analytics-detail*/ ?>

<style type="text/css">
.footer-container .container.p_90 {
  
    bottom: 0;
    background-color: #000;
    padding: 10px;
	padding--bottom: 0px;
}
.footer-container.nopadding{
	background-color: #fff;	
}
@charset "utf-8";
/* CSS Document */
.shadow {
	box-shadow: 0px 1px 4px 1px rgba(0,0,0,0.3);
}
.section1 {
	padding: 50px 0px 50px 0px;
}
.section2 {
	padding: 50px 0px 50px 0px;
}
.section3 {
	padding: 50px 0px 50px 0px;
}
.videos {
	position: relative;
	padding-top: 10px;
	padding-bottom: 10px;
}
/*
.tab-content {   
  width: 30% !important;    
} */
.vidoes-pic {
	position: absolute;
	left: 0;
}
.video-text {
	margin-left: 116px;
	min-height: 92px;
}
.revenue {
	padding-top: 50px;
	text-align: center;
}
.videos .video-text p {
	margin: 0 0 0em;
	    font-size: 13px;
}
.sort-button {
	padding-top: 10px;
}
.chart {
	padding-top: 25px;
}
p {
    text-align: left;
}
.footer-container {
   
    padding-bottom: 0px;
 
}

form li a {
    padding-left: 10px;
    font-weight: normal !important;
    color: black;
}
.footer-container {
    padding-top: 40px;
    padding-bottom: 0px !important;
    background: #000000;
    color: #666666;
}
</style>
<!-- Bootstrap -->
<link href="https://bootswatch.com/paper/bootstrap.min.css" rel="stylesheet">

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<?php
	get_header();
	$post_id = 976;
	if(isset($_REQUEST['id']))
		$post_id = $_REQUEST['id'];
	$filter = 'year';
	if(isset($_REQUEST['filter']))
		$filter = $_REQUEST['filter'];
	$table_name = $wpdb->prefix."oio_custom_clicks_count";
	$table_name2 = $wpdb->prefix."oio_custom_views_count";
	$post_table = $wpdb->prefix."posts";	
	$post_urlq=$wpdb->get_row("SELECT guid, post_type FROM $post_table WHERE ID=".$post_id);
	$post_url = $post_urlq->guid;
	$post_type = $post_urlq->post_type;
	$vr = "Videos Ads Revenue";
	if( $post_type == 'dyn_book')
	{
		$post_url = get_post_permalink( $post_id );
	}		
	if($filter == 'year')
	{
		$cat = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		$data = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$data2 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$dataB = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$dataV = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$title = "This Year Monthly Revenue";
		$xtitle = "Months";
		$titleB = "This Year Monthly Banner Ads Clicks";
		$titleV = "This Year Monthly Video Ads Views";
		
		$q = "select sum(cost) as ccost, count(*) as bcount, month(date) as cmonth from $table_name WHERE YEAR(date) = YEAR(NOW()) and contentUrl='".$post_url."' group by month(date)";
		$res = $wpdb->get_results( $q );
		if($res)
		{
			foreach ($res as $d)
			{
				$data2[$d->cmonth-1] = (float) $d->ccost; 
				$dataB[$d->cmonth-1] = (float) $d->bcount; 
			}
		}
		
		$q2 = "select sum(cost) as ccost, count(*) as vcount, month(date) as cmonth from $table_name2 WHERE YEAR(date) = YEAR(NOW()) and contentUrl='".$post_url."' group by month(date)";
		$res2 = $wpdb->get_results( $q2 );			
		if($res2)
		{
			foreach ($res2 as $d2)
			{
				$data[$d2->cmonth-1] = (float) $d2->ccost;  
				$dataV[$d2->cmonth-1] = (float) $d2->vcount;
			}
		}		
	}
	elseif($filter == 'month')
	{
		for($j=1; $j<=date('t'); $j++)
		{
			$cat[] = "$j";	
			$data[] = 0;
			$data2[] = 0;
			$dataB[] = 0;
			$dataV[] = 0;
		}
		$title = "This Month Daily Revenue";
		$xtitle = "Dates";
		$titleB = "This Month Daily Banner Ads Clicks";
		$titleV = "This Month Daily Video Ads Views";
		
		$q = "select sum(cost) as ccost, count(*) as bcount, day(date) as cday from $table_name WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."' group by day(date)";
		$res = $wpdb->get_results( $q );
		if($res)
		{
			foreach ($res as $d)
			{
				$data2[$d->cday-1] = (float) $d->ccost; 
				$dataB[$d->cday-1] = (float) $d->bcount; 				
			}
		}
		
		$q2 = "select sum(cost) as ccost, count(*) as vcount, day(date) as cday from $table_name2 WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."' group by day(date)";
		$res2 = $wpdb->get_results( $q2 );			
		if($res2)
		{
			foreach ($res2 as $d2)
			{
				$data[$d2->cday-1] = (float) $d2->ccost;  
				$dataV[$d2->cday-1] = (float) $d2->vcount;
			}
		}
		
	}
	elseif($filter == 'day')
	{
		$cat = array("1 AM","2 AM","3 AM","4 AM","5 AM","6 AM","7 AM","8 AM","9 AM","10 AM","11 AM","12 PM","13 PM","14 PM","15 PM","16 PM","17 PM","18 PM","19 PM","20 PM","21 PM","22 PM","23 PM","24 AM");
		$data = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$data2 = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$dataB = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$dataV = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$title = "Today Hourly Revenue";
		$xtitle = "Hours";
		$titleB = "Today Hourly Banner Ads Clicks";
		$titleV = "Today Hourly Video Ads Views";
		
		$q = "select sum(cost) as ccost, count(*) as bcount, hour(time) as chour from $table_name WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)= MONTH(NOW()) AND DAY(date) = DAY(NOW()) and contentUrl='".$post_url."' group by hour(time)";
		$res = $wpdb->get_results( $q );
		if($res)
		{
			foreach ($res as $d)
			{
				$data2[$d->chour-1] = (float) $d->ccost; 
				$dataB[$d->chour-1] = (float) $d->bcount; 				
			}
		}
		
		$q2 = "select sum(cost) as ccost, count(*) as vcount, hour(time) as chour from $table_name2 WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) AND DAY(date) = DAY(NOW()) and contentUrl='".$post_url."' group by hour(time)";
		$res2 = $wpdb->get_results( $q2 );			
		if($res2)
		{
			foreach ($res2 as $d2)
			{
				$data[$d2->chour-1] = (float) $d2->ccost;  
				$dataV[$d2->chour-1] = (float) $d2->vcount;
			}
		}
		
	}
	
?>
<div class="container p_90">
<div class="grid-3-4 centre-block single-page">
<div class="entry" style="text-align:center;background-color:#ececec;">            

<?php 

	if(1) { 
		//is_user_logged_in()
		//$userId = get_current_user_id();
		$userId = 5;
				
		$getadfreeuser=$wpdb->get_row("SELECT sum(cost) as clickR FROM $table_name WHERE contentUrl='".$post_url."'");
		$banner_clicks_total = $getadfreeuser->clickR;		
		
		$getadfreeusery=$wpdb->get_row("SELECT sum(cost) as clickY FROM $table_name WHERE YEAR(date) = YEAR(NOW()) and contentUrl='".$post_url."'");
		$banner_clicks_year = $getadfreeusery->clickY;
		
		$getadfreeuserm=$wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."'");
		$banner_clicks_month = $getadfreeuserm->clickM;
		
		$getadfreeuserd=$wpdb->get_row("SELECT sum(cost) as clickD FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW()) and contentUrl='".$post_url."'");
		$banner_clicks_day = $getadfreeuserd->clickD;
		
		$getadfreeuser2=$wpdb->get_row("SELECT sum(cost) as viewR FROM $table_name2 WHERE contentUrl='".$post_url."'");
		$video_views_total = $getadfreeuser2->viewR;		
			
		$getadfreeusery2=$wpdb->get_row("SELECT sum(cost) as clickY FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) and contentUrl='".$post_url."'");
		$video_views_year = $getadfreeusery2->clickY;

		$getadfreeuserm2=$wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."'");
		$video_views_month = $getadfreeuserm2->clickM;

		$getadfreeuserd2=$wpdb->get_row("SELECT sum(cost) as clickD FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW()) and contentUrl='".$post_url."'");
		$video_views_day = $getadfreeuserd2->clickD;
		
		$grand_total = $banner_clicks_total + $video_views_total;		
		$year_total  = $banner_clicks_year + $video_views_year;
		$month_total = $banner_clicks_month + $video_views_month;
		$day_total = $banner_clicks_day + $video_views_day;
		
		$byear=$wpdb->get_row("SELECT count(*) as clickY FROM $table_name WHERE YEAR(date) = YEAR(NOW()) and contentUrl='".$post_url."'");
		$byear_count= $byear->clickY;
		
		$bmonth=$wpdb->get_row("SELECT count(*) as clickM FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."'");
		$bmonth_count = $bmonth->clickM;
		
		$bday=$wpdb->get_row("SELECT count(*) as clickD FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW()) and contentUrl='".$post_url."'");
		$bday_count = $bday->clickD;
		
		$vyear=$wpdb->get_row("SELECT count(*) as clickY FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) and contentUrl='".$post_url."'");
		$vyear_count= $vyear->clickY;
		
		$vmonth=$wpdb->get_row("SELECT count(*) as clickM FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."'");
		$vmonth_count = $vmonth->clickM;
		
		$vday=$wpdb->get_row("SELECT count(*) as clickD FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW()) and contentUrl='".$post_url."'");
		$vday_count = $vday->clickD;

?>
<div>
	<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
		<button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
		
		<ul class="dropdown-menu dropdown-menu-left" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
			<form method="post" name="frm" id="frm">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#" onClick = "changeGraph(1);">Day</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#" onClick = "changeGraph(2);">Month</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#" onClick = "changeGraph(3);">Year</a></li>
			<input type="hidden" name="filter" id="filter">
			<input type="hidden" name="id" id="id" value="<?php echo $post_id; ?>">
			</form>
		</ul>		
	</div>
	<div class="row">
		<div class="col-lg-8 col-md-8">
			<div class="chart">
				<div id="chart1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
		  <div class="row revenue">
			<div class="col-md-8 col-xs-8">
			  <p style="font-size: 13px;">Total Revenue Today:</p>
			  <p style="font-size: 13px;">Total Revenue This Month:</p>
			  <p style="font-size: 13px;">Total Revenue This Year:</p>
			  <p style="font-size: 13px;">Total Revenue All Time:</p>
			</div>
			<div class="col-md-4 col-xs-4">
			  <p style="font-size: 13px;">$ <?php echo $day_total; ?></p>
			  <p style="font-size: 13px;">$ <?php echo $month_total; ?></p>
			  <p style="font-size: 13px;">$ <?php echo $year_total; ?></p>
			  <p style="font-size: 13px;">$ <?php echo $grand_total; ?></p>
			</div>
		  </div>
		</div>
	</div>
</div>
<?php
if($post_type != 'product')
{
?>
<div>
	<!--
	<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
		<button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
		<ul class="dropdown-menu dropdown-menu-left" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Day</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Month</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Year</a></li>
		</ul>
	</div>
	-->
	<div class="row">
		<div class="col-lg-8 col-md-8">
			<div class="chart">
				<div id="chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
		  <div class="row revenue">
			<div class="col-md-10 col-xs-10">
			  <p style="font-size: 13px;">Total Video Ads Views Today:</p>
			  <p style="font-size: 13px;">Total Video Ads Views This Month:</p>
			  <p style="font-size: 13px;">Total Video Ads Views This Year:</p>			  
			</div>
			<div class="col-md-2 col-xs-2">
			  <p style="font-size: 13px;"> <?php echo $vday_count; ?></p>
			  <p style="font-size: 13px;"> <?php echo $vmonth_count; ?></p>
			  <p style="font-size: 13px;"> <?php echo $vyear_count; ?></p>			  
			</div>
		  </div>
		</div>
	</div>
</div>
<script>
$(function () {
	var cat = [];
	var cat = <?php echo json_encode($cat); ?>;
	var dataV = [];
	var dataV = <?php echo json_encode($dataV); ?>;
    Highcharts.chart('chart2', {
        title: {
            text: '<?php echo $titleV; ?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: '<?php echo $xtitle; ?>'
            },
            categories: cat
        },
        yAxis: {
            title: {
                text: 'Views'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' '
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: 'Video Ads Views',
            data: dataV
        }]
    });
});
</script> 
<script> 
$(function () {
	var cat = [];
	var cat = <?php echo json_encode($cat); ?>;
	var data = [];
	var data = <?php echo json_encode($data); ?>;
	var data2 = [];
	var data2 = <?php echo json_encode($data2); ?>;
    Highcharts.chart('chart1', {
        title: {
            text: '<?php echo $title; ?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: '<?php echo $xtitle; ?>'
            },
            categories: cat
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: '<?php echo $vr; ?>',
            data: data
        },{
            name: 'Banner Ads Revenue',
            data: data2
        }]
    });
});
</script> 
<?php
}
else
{
?>
<script> 
$(function () {
	var cat = [];
	var cat = <?php echo json_encode($cat); ?>;
	var data = [];
	var data = <?php echo json_encode($data); ?>;
	var data2 = [];
	var data2 = <?php echo json_encode($data2); ?>;
    Highcharts.chart('chart1', {
        title: {
            text: '<?php echo $title; ?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: '<?php echo $xtitle; ?>'
            },
            categories: cat
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: 'Banner Ads Revenue',
            data: data2
        }]
    });
});
</script> 
<?php	
}
?>

<div>
	<!--
	<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
		<button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
		<ul class="dropdown-menu dropdown-menu-left" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Day</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Month</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Year</a></li>
		</ul>
	</div> 
	-->
	<div class="row">
		<div class="col-lg-8 col-md-8">
			<div class="chart">
				<div id="chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
		  <div class="row revenue">
			<div class="col-md-10 col-xs-10">
			  <p style="font-size: 13px;">Total Banner Ads Clicks Today:</p>
			  <p style="font-size: 13px;">Total Banner Ads Clicks This Month:</p>
			  <p style="font-size: 13px;">Total Banner Ads Clicks This Year:</p>			  
			</div>
	
			<div class="col-md-2 col-xs-2">
			  <p style="font-size: 13px;"> <?php echo $bday_count; ?></p>
			  <p style="font-size: 13px;"> <?php echo $bmonth_count; ?></p>
			  <p style="font-size: 13px;"> <?php echo $byear_count; ?></p>
			</div>
		  </div>
		</div>
	</div>
</div>
	
<?php 
	}
else{ 
?>
	<h2>You need to login to access this page.</h2>
<?php }?>
<div class="clear"></div>
</div>
</div>
</div>
<div class="spacing-40"></div>
<?php get_footer();?>
<script
  src="https://code.jquery.com/jquery-1.11.2.js"
  integrity="sha256-WMJwNbei5YnfOX5dfgVCS5C4waqvc+/0fV7W2uy3DyU="
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>

function changeGraph (id)
{
	if(id==3)
		document.getElementById('filter').value = "year";
	else if(id==2)
		document.getElementById('filter').value = "month";
	else if(id==1)
		document.getElementById('filter').value = "day";
	
	document.getElementById('frm').submit();	
}

$(function () {
	var cat = [];
	var cat = <?php echo json_encode($cat); ?>;
	var dataB = [];
	var dataB = <?php echo json_encode($dataB); ?>;
    Highcharts.chart('chart3', {
        title: {
            text: '<?php echo $titleB; ?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: '<?php echo $xtitle; ?>'
            },
            categories: cat
        },
        yAxis: {
            title: {
                text: 'Clicks'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#333333'
            }]
        },
        tooltip: {
            valueSuffix: ' '
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: 'Banner Ads Clicks',
            data: dataB
        }]
    });
});
</script>

