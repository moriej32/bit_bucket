<?php /* Template Name: Home Page */ ?>
<?php get_header(); ?>

<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>-->
<!-- start homepage header -->

<div class="container homepage-header">

<div class='homepage_main_video'><?php get_template_part('includes/home-page-videos/home-page-video-1'); ?></div>

	<div class='homepage_videos_wrapper'>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-2'); ?></div>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-3'); ?></div>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-4'); ?></div>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-5'); ?></div>
	</div>

</div>
<script>
jQuery("video").each(function(){
jQuery(this).click(function() {
 if(this.paused)
   this.play();
  else
    this.pause();
});
});
</script>
<!-- end homepage header -->


<div class="container p_90">
<?php

/* if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == "my_post_type") {
	if($_POST['file']){
	$path='/var/www/html/bitbucket/wp-content/uploads/diynation_admin';
	$path=$path.'/'.$_FILES['file']['name'];
		if(move_uploaded_file($_FILES['file']['tmp_name'],$path)){
			$title     = $_POST['post_title'];
			$content   = $_POST['post_content'];
			$video_options =$path;

			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');

			$title     = $_POST['post_title'];
			$content   = $_POST['post_content'];

			$data= 	 array('video_type'      => 'upload',
			               'video_upload' => $_POST['file'],
						   'youtube_url' => '',
						   'embed_code'=>'');

			$post_category = $_POST['post_category'] ;
			$post = array(
				'post_title' => $title,
				'post_content' => $post_content,
				'post_category'=>$post_category,
				'post_status' => 'publish'
			);

			$pid = wp_insert_post($post,true);

			add_post_meta($pid, 'your_meta_key', $your_meta_value);
		}
	}
	else if($_POST['youtube_url']){


			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');
			$title     = $_POST['post_title'];
			$content   = $_POST['post_content'];

			$data= 	 array('video_type'      => 'youtube_url',
			               'video_upload' => '',
						   'youtube_url' => $_POST['youtube_url'],
						   'embed_code'=>'');

			$post_category = $_POST['post_category'] ;
			$post = array(
				'post_title' => $title,
				'post_content' => $post_content,
				'post_category'=>$post_category,
				'post_status' => 'publish'
			);

			$pid = wp_insert_post($post,true);

			add_post_meta($pid, 'video_options', $data);

	}
	else if($_POST['embed_code']){


			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');
			$title     = $_POST['post_title'];
			$content   = $_POST['post_content'];

			$data= 	 array('video_type'      => 'embed_code',
			               'video_upload' => '',
						   'youtube_url' => '',
						   'embed_code'=>$_POST['embed_code']);

			$post_category = $_POST['post_category'] ;
			$post = array(
				'post_title' => $title,
				'post_content' => $post_content,
				'post_category'=>$post_category,
				'post_status' => 'publish'
			);

			$pid = wp_insert_post($post,true);

			add_post_meta($pid, 'video_options', $data);

	}


}  */

?>

<!-- start 90 precent wrapper -->
 <?php
	/* ================================================================== */
	/* Start of Loop */
	/* ================================================================== */
	while (have_posts()) : the_post();
	?>

    <!-- Start Video Player -->
    <?php //get_template_part('includes/videoplayer'); ?>
    <!-- End Video Player -->

    <!-- Start Video Heading -->
    <!--<div class="video-heading">-->
    <!-- Start Sharer -->
    <?php //get_template_part('includes/sharer'); ?>
    <!-- End Sharer -->
    <!--<h6 class="video-title"><?php //the_title(); ?></h6>
    <div class="clear"></div>
    </div>-->
    <!-- End Video Heading -->

    <!-- Start Content -->
    <ul class="stats">
        <li><?php //videopress_countviews( get_the_ID() ); ?></li>
        <li><?php //comments_number() ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>

    <?php if (1 == 0){ //if( vp_option('vpt_option.show_user') == '1' ){ ?>
    <div class="post-by">
    <a href="<?php echo get_author_posts_url( $post->post_author ); ?>"><?php echo get_avatar( $post->post_author, 50 ); ?></a>
    <a href="<?php echo get_author_posts_url( $post->post_author ); ?>" class="post-by-link"><?php the_author(); ?></a>
    <?php echo '<div class="post-by-vid-count">'.count_user_posts( $post->post_author ).' Videos Uploaded</div>'; ?>
    <div class="clear"></div>
    </div>
    <?php } ?>

    <!-- Start Content -->
    <div class="entry">
    <?php
    // Display The Content
    if($post->post_content==""){
    		echo '<div class="content-empty">No Description</div>';
		}else{
			the_content();
		}
	?>
    <div class="clear"></div>
    </div>
    <!-- End Content -->


	<?php
	// Link Pages Navigation
	$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
    wp_link_pages( $args );

	// Display the Tags
	$before = '<div class="spacer-20"></div>
			   <div class="post-tags">
			   <span><i class="fa fa-tags"></i>Tags</span>';

	$after = '</div>';
	the_tags( $before,', ',$after );
	?>



    <div class="clear"></div>
    <!-- End Content -->
<!--	<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Upload</a></li>
    <li><a href="#tabs-2"> External URL</a></li>
    <li><a href="#tabs-3">Embed Code</a></li>
  </ul>
  <form id="custom-post-type" name="custom-post-type" enctype="multipart/form-data" method="post" action="">
  <label for="title">Post Title</label><br />
  Title<input type="text" id="title" value=""  size="20" name="post_title" />
 <br/>
Description<br/><textarea id="description" tabindex="3" name="post_content" cols="50" rows="6"></textarea>
  <div id="category-all" class="tabs-panel">
			<input type="hidden" name="post_category[]" value="0">			<ul id="categorychecklist" data-wp-lists="list:category" class="categorychecklist form-no-clear">

<li id="category-3" class="popular-category"><label class="selectit"><input value="3" type="checkbox" name="post_category[]" id="in-category-3" checked="checked"> Arts Entertainment</label></li>

<li id="category-1" class="popular-category"><label class="selectit"><input value="1" type="checkbox" name="post_category[]" id="in-category-1"> Blog Post</label></li>

<li id="category-5" class="popular-category"><label class="selectit"><input value="5" type="checkbox" name="post_category[]" id="in-category-5"> Cars Other Vehicles</label></li>

<li id="category-8"><label class="selectit"><input value="8" type="checkbox" name="post_category[]" id="in-category-8"> Computers Electronics</label></li>

<li id="category-19" class="popular-category"><label class="selectit"><input value="19" type="checkbox" name="post_category[]" id="in-category-19"> DYN</label></li>

<li id="category-11"><label class="selectit"><input value="11" type="checkbox" name="post_category[]" id="in-category-11"> Education  Communications</label></li>

<li id="category-14"><label class="selectit"><input value="14" type="checkbox" name="post_category[]" id="in-category-14"> Family Life</label></li>

<li id="category-23" class="popular-category"><label class="selectit"><input value="23" type="checkbox" name="post_category[]" id="in-category-23"> Featured</label></li>

<li id="category-17" class="popular-category"><label class="selectit"><input value="17" type="checkbox" name="post_category[]" id="in-category-17"> Finance, Business Legal</label></li>

<li id="category-20"><label class="selectit"><input value="20" type="checkbox" name="post_category[]" id="in-category-20"> Food Entertaining</label></li>

<li id="category-29" class="popular-category"><label class="selectit"><input value="29" type="checkbox" name="post_category[]" id="in-category-29"> Health</label></li>

<li id="category-6" class="popular-category"><label class="selectit"><input value="6" type="checkbox" name="post_category[]" id="in-category-6"> Hobbies Crafts</label></li>

<li id="category-9" class="popular-category"><label class="selectit"><input value="9" type="checkbox" name="post_category[]" id="in-category-9"> Holidays Traditions</label></li>

<li id="category-12"><label class="selectit"><input value="12" type="checkbox" name="post_category[]" id="in-category-12"> Home Garden</label></li>

<li id="category-22"><label class="selectit"><input value="22" type="checkbox" name="post_category[]" id="in-category-22"> Other</label></li>

<li id="category-15"><label class="selectit"><input value="15" type="checkbox" name="post_category[]" id="in-category-15"> Personal Care Style</label></li>

<li id="category-18"><label class="selectit"><input value="18" type="checkbox" name="post_category[]" id="in-category-18"> Pets Animals</label></li>

<li id="category-21"><label class="selectit"><input value="21" type="checkbox" name="post_category[]" id="in-category-21"> Philosophy Religion</label></li>

<li id="category-4"><label class="selectit"><input value="4" type="checkbox" name="post_category[]" id="in-category-4"> Relationships</label></li>

<li id="category-7"><label class="selectit"><input value="7" type="checkbox" name="post_category[]" id="in-category-7"> Sports Fitness</label></li>

<li id="category-10" class="popular-category"><label class="selectit"><input value="10" type="checkbox" name="post_category[]" id="in-category-10"> Travel</label></li>

<li id="category-13"><label class="selectit"><input value="13" type="checkbox" name="post_category[]" id="in-category-13"> Work World</label></li>

<li id="category-16"><label class="selectit"><input value="16" type="checkbox" name="post_category[]" id="in-category-16"> Youth</label></li>
			</ul>
		</div>

  <div id="tabs-1">

<input class="vp-js-upload vp-button button" name ="file"type="file" value="Choose File">

  </div>
  <div id="tabs-2">
 <input type="text" name="youtube_url" class="vp-input input-large" value="">

  </div>
  <div id="tabs-3">
	<textarea class="vp-input" name="embed_code"></textarea>
  </div>

<input type="submit" value="Publish" tabindex="6" id="submit" name="submit" /></div>
	  <input type="hidden" name="action" value="my_post_type" />
</form>!-->

    <!-- Start Related Videos -->
    <div class="spacing-40"></div>
    <?php //get_template_part('includes/related-videos'); ?>
    <!-- End Related Videos -->


    <?php
	/* ================================================================== */
	/* End of Loop */
	/* ================================================================== */
	endwhile;
	?>


    <!-- Start Content -->
    <div class="">



    <!-- Start Entries -->
    <div class="grid-3-4 homepage-layout">

	<!-- FEATURED VIDEOS -->
	 <!--<div id="featured_videos_container">
	 <?php //get_template_part('includes/featured-playlist-home'); ?>
	 </div>
	 <hr/>-->

	<!-- POPULAR VIDEOS -->
	 <!--<div id="popular_videos_container">
	 <?php //the_widget('videopress_PopularVideos'); ?>
     <?php //get_template_part('includes/popular-videos-home'); ?>
	 </div>
	 <hr/>-->

	<!-- RELATED VIDEOS -->
	 <!--<div id="related_videos_container">
	 <?php //get_template_part('includes/related-videos'); ?>
	 </div>
	 <hr/>-->

	 <!-- POPULAR CHANNELS -->
	 <!--<div id="popular_channels_container">
	 <?php //get_template_part('includes/popular-channels'); ?>
	 </div>
	 <hr/>-->


	 <div id="categories_container">
     <h6 class="title-bar"><span>Browse Our Categories</span></h6>
     <div class="spacer-dark"></div>
     <?php
	 function array_random($arr, $num = 1) {
		  shuffle($arr);

		  $r = array();
		  for ($i = 0; $i < $num; $i++) {
			  $r[] = $arr[$i];
		  }
		  return $num == 1 ? $r[0] : $r;
	 }
	 ?>
	 <?php get_template_part('includes/random-category-videos'); ?>
     <?php //get_template_part('includes/random-category-videos'); ?>
     <?php //get_template_part('includes/random-category-videos'); ?>
     <?php //get_template_part('includes/random-category-videos'); ?>
	 </div>
	 <hr/>


    <!-- Content -->
    <!--<div class="entry">

    <div class="clear"></div>
    </div>-->
    <!-- End Content -->

    <?php get_template_part('layouts/layout-builder'); ?>

    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <!-- <?php //get_sidebar(); ?>-->
    <!-- End Widgets -->


    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>

     <!-- bottom section -->
     <!-- RANDOM CATEGORIES -->

    <!-- end random categories section -->
    <!-- End Content -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>

</div><!-- end 90 percent wrapper -->
<div class="sign_up_area">
<div class="sign_up_title">Join the Nation Today</div>
<div class="sign_up_text"><span class="icon-quote-left"></span> Where Anything is Possible - Just Press Play ! <span class="icon-quote-right"></span></div>

		<div class="fifty-percent alignleft">
			<a href="<?php echo home_url(); ?>/sign-in/#login"><div class="login_button">LOG IN</div></a>
		</div>

		<div class="fifty-percent alignright">
			<a href="<?php echo home_url(); ?>/sign-in/#signup"><div class="sign_up_button">SIGN UP</div></a>
		</div>

</div>

<script language="javascript" type="text/javascript">

/*     jquery('#categories_container').jscroll({
		loadingHtml: '<img src="<?php echo get_template_directory_uri();?>/images/loading.gif" alt="Loading" /> Loading...',
		padding: 20,
		nextSelector: '#nav-below li a',
		contentSelector: '#categories_container div.post'
	}); */

</script>



<?php get_footer(); ?>