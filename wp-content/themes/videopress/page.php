
<?php
$urlstr=filter_input(INPUT_SERVER, 'REQUEST_URI');

//$strstr=mb_substr($urlstr,1,-1);
$trueurl=strstr($urlstr,"my-account");
if($trueurl){
?>
<style>
	article.hentry {
		margin: 0 0 2.236em;
	}
	.col-1.address {
		float: left;
		margin-right: 2.666667% !important;
	}
	p#hello-sing-out {
		padding-bottom: 11px;
	}
	.woocommerce-MyAccount-navigation ul {
		margin-left: 0;
		border-top: 1px solid rgba(0,0,0,.05);
	}
	.page-template-template-fullwidth-php .woocommerce-MyAccount-navigation {
		width: 21.7391304348%;
		float: left;
		margin-right: 4.347826087%;
	}
	.woocommerce-MyAccount-navigation ul li {
		list-style: none;
		border-bottom: 1px solid rgba(0,0,0,.05);
		position: relative;
	}
	.woocommerce-MyAccount-navigation ul li a {
		padding: .857em 0;
		display: block;    color: #96588a;
	}
	.col-full {
		max-width: 67.141em;
		margin-left: auto;
		margin-right: auto;
		padding: 0 2.618em;
		box-sizing: content-box;
	}
	.woocommerce-MyAccount-navigation {
		width: 21.7391304348%;
		float: left;
		margin-right: 4.347826087%;
	}

	.woocommerce-MyAccount-content {
		width: 73.9130434783%;
		float: right;
		margin-right: 0;
	}
	.woocommerce-MyAccount-navigation {
		width: 21.7391304348%;
		float: left;
		margin-right: 4.347826087%;
	}
	.woocommerce-MyAccount-navigation ul li a {
		padding: .857em 0;
		display: block;
		color: #96588a;
		text-decoration: none !important;
	}

	.site-main {
		margin-bottom: 2.618em;
	}
	.hentry {
		margin: 0 0 4.236em;
	}
	.col-full {
		max-width: 67.141em;
		margin-left: auto;
		margin-right: auto;
		padding: 0 2.618em;
		box-sizing: content-box;
	}
	.centre-block{    margin-right: initial;}
	.entry{
		max-width: 67.141em;
		margin-left: auto;
		margin-right: auto;
		padding: 0 2.618em;
		box-sizing: content-box;    border: 1px solid rgba(255, 255, 255, 0);
		box-shadow: 0px 1px 1px rgba(255, 255, 255, 0);
	}
	.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--orders a:before {
		content: "\f291";
	}
	.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--dashboard a:before {
		content: "\f0e4";
	}
	.woocommerce-MyAccount-navigation ul li a:before {
		display: inline-block;
		font: normal normal normal 1em/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		content: "\f0f6";
		line-height: 1.618;
		margin-left: .53em;
		width: 1.387em;
		text-align: right;
		float: right;
		opacity: .25;
	}
	.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--downloads a:before {
		content: "\f1c6";
	}
	.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--edit-address a:before {
		content: "\f015";
	}.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--edit-account a:before {
		 content: "\f007";
	 }.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--customer-logout a:before {
		  content: "\f08b";
	  }
	.entry h1:first-child {    font-weight: bold;
		font-size: 25px;
		margin-bottom: 10px;
	}
	.woocommerce-MyAccount-navigation ul li a:hover > .woocommerce-MyAccount-navigation ul li a:before{
		background: red;color:red
	}
	a {
		color: #c166b0;
	}
	/*.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link--dashboard.is-active  a:before{   opacity: inherit;}*/
	.woocommerce-MyAccount-navigation ul li:hover > a:before{opacity: inherit;}
	.centre-block {
		margin-right: 0 !important;}
	.grid-3-4.centre-block.single-page .entry {
		border: 1px solid #ffffff !important;
		box-shadow: 0px 1px 1px #ffffff !important;
	}
	.input-text,.select2-choice{
		padding: .618em;
		background-color: #f2f2f2;
		color: #43454b;
		outline: 0;
		border: 0;
		-webkit-appearance: none;
		border-radius: 2px;
		box-sizing: border-box;
		font-weight: 400;
		box-shadow: inset 0 1px 1px rgba(0,0,0,.125);
	}
	.select2-choice{
		background-color: #f2f2f2 !important;
	}
	.woocommerce p .button{
		border: 0 #43454b;
		background: #43454b;
		color: #fff;
		cursor: pointer;
		padding: .618em 1.387em;
		text-decoration: none;
		font-weight: 600;
		text-shadow: none;
		display: inline-block;
		outline: 0;
		-webkit-appearance: none;
		-webkit-font-smoothing: antialiased;
		border-radius: 0;
		box-shadow: inset 0 -1px 0 rgba(0,0,0,.3);
		background-color: #7d3f71 !important;
		border-color: #7d3f71 !important;
		color: white !important;
	}

	.woocommerce p .button:hover{
		background-color: #834377 !important;
	}
	#download-link-product,.button.view,.button.leave_feedback{
		background-color: #96588a !important;
		border-color: #96588a !important;
		color: #ffffff !important;text-decoration: none !important;
		padding: .618em .857em;    line-height: 17px !important;
		font-size: .857em;
		margin-right: .236em;
	}
	  .button.leave_feedback:hover{background-color: #834777 !important;}#download-link-product:hover{background-color: #834777 !important;}.button.view:hover{	background-color: #834777 !important;}
	table thead th {
		text-transform: uppercase;
		padding: 1.387em !important;
		vertical-align: middle !important;
		font-weight: 900 !important;
	}
	table td, table th {
		padding: 1em 1.387em !important;
		text-align: left !important;
		vertical-align: middle !important;
		font-weight: 900 !important;
	}


</style>
<?php } ?>


<?php
// DEFAULT TEMPLATE
get_header();

?>

    <!-- Start Content -->
    <div class="container p_90">

    <?php
    //	get_template_part('includes/featured-playlist');
	?>

    <!-- Start Entries -->
    <div class="grid-3-4 centre-block single-page">
    <!-- Content -->
    <?php if(is_page("leaderboard")){
	?> <div class="entry grey-background"> <?php
	}else{ ?> <div class="entry"><?php } ?>
            <?php check_block_pages();

		// Start The Loop
		while (have_posts()) : the_post();
		if(is_page('channels')){
			echo '<h1>';
			    echo 'Live Stream';
			echo '</h1>';
		}else{
			if($_GET['account'] && isset($_GET['account'])){
				if($_GET['account']=='order') {
					echo '<h1>';
				    echo 'Orders';echo '</h1>';
				}elseif($_GET['account']=='downloads'){
					echo '<h1>';
					echo 'Downloads';echo '</h1>';
				}else{
					echo '<h1>';
					echo the_title();
					echo '</h1>';
				}
			}else{
			echo '<h1>';
			    echo the_title();
			echo '</h1>';}
		}
		 if (check_block_pages()){
		     echo "Your account has been blocked, please contact Admin";
         }else{

			   $urlstr=filter_input(INPUT_SERVER, 'REQUEST_URI'); ;
			   //$strstr=mb_substr($urlstr,1,-1);
			  $trueurl=strstr($urlstr,"my-account");

            if($trueurl && is_user_logged_in()){
					?>


					<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">


							<article id="post-7" class="post-7 page type-page status-publish hentry">
								<div class="entry-content">
									<div class="woocommerce">
										<nav class="woocommerce-MyAccount-navigation">
											<ul>
												<li id="dashboard"
													class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active">
													<a href="<?php site_url(); ?>/bit_bucket/my-account/">Dashboard</a>
												</li>
												<li id="orders"
													class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders">
													<a href="<?php site_url(); ?>/bit_bucket/my-account?account=order">Orders</a>
												</li>
												<li id="downloads"
													class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--downloads">
													<a href="<?php site_url(); ?>/bit_bucket/my-account/?account=downloads">Downloads</a>
												</li>
												<li id="addresses"
													class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-address">
													<a href="<?php site_url(); ?>/bit_bucket/my-account/edit-address/">Addresses</a>
												</li>
												<li id="account"
													class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account">
													<a href="<?php site_url(); ?>/bit_bucket/my-account/edit-account">Account
														Details</a>
												</li>
												<li id="logout"
													class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
													<a href="<?php site_url(); ?>/bit_bucket/my-account/customer-logout">Logout</a>
												</li>
											</ul>
										</nav>


										<div class="woocommerce-MyAccount-content">
											<?php
											the_content();
											?>
										</div>
									</div>
								</div><!-- .entry-content -->
							</article><!-- #post-## -->

						</main><!-- #main -->
					</div>
					<?php

			}else{
				the_content();
			}
				  ?>













			 <?php
         }
		endwhile;
		// End Of Loop
	?>
    <div class="clear"></div>
        <?php
           $page_slug = trim($_SERVER["REQUEST_URI"] , '/' );
           $list = explode("/",$page_slug);
           if(in_array( "channel",$list)){
        ?>
        <div class="video-heading">
            <div class="flag_post custom_btn"><span id="addflag-anchor" title="Flag as inappropriate">Flag</span></div>


             <?php
             /*
			 * Custom Button by Kbihm
			 */

			 $author_id=$post->post_author;
			 $postId=get_the_ID();
			 echo '<div id="endorse_wrapper">';
			 echo endorsement_btn($postId,$author_id);
			 echo '</div>';

			/*------------------end----------------------------*/


				/*----- old code-------*/
				/*
                $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID() . ' AND user_id = ' . get_current_user_id());
                if (empty($result)){
                ?>
                <div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
                <?php }else{ ?>
                <div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>



            <?php
                }
                */
                $result = $wpdb->get_results( 'SELECT user_id FROM `wp_subscribe` WHERE post_id = ' . get_the_ID() . ' AND user_id = ' . get_current_user_id());
                if (empty($result)){
            ?>
              <div id="endorse_wrapper"><div id="subscribe_button" class="endorse_video_no custom_btn" style="background-color: #6B68C9 !important;"><span>Subscribe</span></div></div>
             <?php }else{ ?>
              <div id="endorse_wrapper"><div id="subscribe_button" class="endorse_video_no custom_btn" style="background-color: #120F9D !important;"><span>Subscribed!</span></div></div>
            <?php }
			/* get_current_user_id() */
			$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_the_author_meta('ID')." and status=1");
			$paypalacc='';
				if($datapaypal){
					foreach ($datapaypal as $reg_datagen){
					$paypalacc=$reg_datagen->paypal_email;
				}
			}

			 if($paypalacc!==''){ ?>
			 &nbsp;&nbsp;
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#model_donate">Donate</button>

		<!--Donate Popup-->
		<div class="modal fade" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   <div class="modal-dialog">
			  <div class="modal-content">
				 <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
				 </div>
				 <div class="modal-body">
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
						<input type="hidden" name="cmd" value="_xclick">
						<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
						<input type="hidden" name="address_ override" value="<?php echo $paypalacc ?>">
						<input type="hidden" name="lc" value="US">
						<input type="hidden" name="item_name" value="Do It Yourself Nation">
						<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
						<input type="hidden" name="currency_code" value="USD">
						<input type="hidden" name="button_subtype" value="services">
						<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
						<div class="form-group">
							<span for="amount" class="control-label col-sm-4">Amount:</span>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="amount" required >
							</div>
						</div>
						<input type="hidden" name="on0" value="Email" required >
						<div class="form-group">
							<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="os0" maxlength="300" size="40">
							</div>
						</div>
						<input type="hidden" name="on1" value="Message" required>
						<div class="form-group">
							<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="os1" maxlength="300" size="40">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-8">
								<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
							</div>
						</div>

					</form>
				 </div>
			  </div>
		  </div>
		</div>
		<?php }

                get_template_part('includes/flag-videos');
        ?>

        <div class="clear"></div>
        <br />
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- ResponsiveAds -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-4835145942281602"
             data-ad-slot="5378992541"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>

        </div>

        <?php } ?>
    </div>
    <!-- End Content -->

    <?php get_template_part('layouts/layout-builder'); ?>

    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <!-- <?php //get_sidebar(); ?>-->
    <!-- End Widgets -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->
 <script>
 var check_id = "<?php echo get_current_user_id(); ?>";

 // endorse video / un-endorse
 jQuery('#endorse_button').on('click',function(){
	if (check_id == 0){
		alert("You must be logged in to endorse this channel.");
	}else{

	jQuery.ajax({
		url: "<?php echo get_home_url() ?>/endorse_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
		type: "GET",
		}).done(function(data) {
			//console.log(data);
			if (data == "endorsed"){
				jQuery('#endorse_button').html('<span>Endorsed!</span>');
				jQuery('#endorse_button').css('background-color','#0F9D58');
			}
			if (data == "unendorsed"){
				jQuery('#endorse_button').html('<span>Endorse</span>');
				jQuery('#endorse_button').css('background-color','#120F9D');
			}
		});

	}
 });

 // subscribe video / non-subscribe
 jQuery('#subscribe_button').on('click',function(){
	if (check_id == 0){
		alert("You must be logged in to set a channel as subscribe.");
	}else{

	jQuery.ajax({
		url: "<?php echo get_home_url() ?>/subscribe_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
		type: "GET",
		}).done(function(data) {
			//console.log(data);
			if (data == "subscribe"){
				jQuery('#subscribe_button').html('<span>Marked as subscribe!</span>');
				jQuery('#subscribe_button').css('background-color','#120F9D');
			}
			if (data == "unsubscribe"){
				jQuery('#subscribe_button').html('<span>Subscribe</span>');
				jQuery('#subscribe_button').css('background-color','#6B68C9');
			}
		});

	}
 });
 $('#bbpress-forums').parent('.entry').addClass('grey-background');
</script>
<?php get_footer(); ?>
