<?php
function insert_views_data()
{
	$ip = $_POST["ip"];
	$post_url = $_POST["post_url"];
	$datatype = $_POST["datatype"];
	$author_id = $_POST["author_id"];
	$itemid = $_POST['itemid'];
	$date = date('Y-m-d');
	$time = date('H:i:s'); 
	
	global $wpdb;
	
	$itemrow = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."oiopub_purchases WHERE item_id=".$itemid);
	if($itemrow->item_duration_left>0){
		$item_duration_left = $itemrow->item_duration_left - 1;
		//$item_duration = $itemrow->item_duration + 1;
		$wpdb->update(
			'wp_oiopub_purchases',
			array(
				'item_duration' => $item_duration_left  // integer (number)
				//'item_duration_left' =>  $item_duration_left  // integer (number)
			),
			array( 'item_id' => $itemid ),
			array(
				'%d',   // value1
				//'%d'    // value2
			),
			array( '%d' )
		);
		/* $query = "UPDATE wp_oiopub_purchases SET item_duration=item_duration+1 and 	item_duration_left=	item_duration_left-1 WHERE item_id=$itemid";
		$wpdb->query($query);  */
	}
	
	$check_revenue_status = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_revenue_status WHERE revenue_status = '1' and user_id =".$author_id);
	$check_revenue_status_row = $wpdb->num_rows;
	
	if($check_revenue_status_row>0){	
		$duration = floor($_POST["getDuration"]);
		$cost = ($duration / 100) * 0.10 * 0.45;
		//echo "Cost are: {$cost}";
	}else{
		$cost = 0;
		//echo "Cost are: {$cost}";
	}
	
	 	
	//$check = $wpdb->get_var("SELECT COUNT(*) FROM $table_name where ipAddress= '$ip' and contentUrl= '$post_url' and  DATEDIFF(NOW(),date) > 7");
	
	$table_name = $wpdb->prefix . "oio_custom_views_count";
	$result=$wpdb->get_row("SELECT * FROM $table_name where ipAddress= '$ip' and contentUrl= '$post_url' and DATEDIFF(NOW(),date) < 7");
	$check_row = $wpdb->num_rows;
	//$id = $result->id;
	//echo "Rows: {$check_row}"; 
	
	 if($check_row <= 0){
		$wpdb->insert($table_name, array( 
				'ipAddress' => $ip,
				'contentUrl' => $post_url,
				'author_id' => $author_id,
				'views' => 1,
				'time' => $time,
				'date' => $date,
				'duration' => $duration,
				'cost' => $cost
			));			
	} 
	/* $user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->users" );
	echo "<p>User count is {$user_count}</p>"; 	 */
		
	// stop doing stuff
	die();
}

add_action( 'wp_ajax_insert_views_data', 'insert_views_data' );
//add_action( 'wp_ajax_nopriv_insert_views_data', 'insert_views_data' );