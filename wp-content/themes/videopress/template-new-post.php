<?php /* Template Name: New Post */ ?>
<?php
// start admin
if ( ! defined( 'WP_ADMIN' ) ) {
	define( 'WP_ADMIN', true );
}

if ( ! defined('WP_NETWORK_ADMIN') )
	define('WP_NETWORK_ADMIN', false);

if ( ! defined('WP_USER_ADMIN') )
	define('WP_USER_ADMIN', false);

if ( ! WP_NETWORK_ADMIN && ! WP_USER_ADMIN ) {
	define('WP_BLOG_ADMIN', true);
}

if ( isset($_GET['import']) && !defined('WP_LOAD_IMPORTERS') )
	define('WP_LOAD_IMPORTERS', true);

require_once( ABSPATH . '/wp-load.php');

nocache_headers();

if ( get_option('db_upgraded') ) {
	flush_rewrite_rules();
	update_option( 'db_upgraded',  false );

	/**
	 * Fires on the next page load after a successful DB upgrade.
	 *
	 * @since 2.8.0
	 */
	do_action( 'after_db_upgrade' );
} elseif ( get_option('db_version') != $wp_db_version && empty($_POST) ) {
	if ( !is_multisite() ) {
		wp_redirect( admin_url( 'upgrade.php?_wp_http_referer=' . urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ) ) );
		exit;

	/**
	 * Filter whether to attempt to perform the multisite DB upgrade routine.
	 *
	 * In single site, the user would be redirected to wp-admin/upgrade.php.
	 * In multisite, the DB upgrade routine is automatically fired, but only
	 * when this filter returns true.
	 *
	 * If the network is 50 sites or less, it will run every time. Otherwise,
	 * it will throttle itself to reduce load.
	 *
	 * @since 3.0.0
	 *
	 * @param bool true Whether to perform the Multisite upgrade routine. Default true.
	 */
	} elseif ( apply_filters( 'do_mu_upgrade', true ) ) {
		$c = get_blog_count();

		/*
		 * If there are 50 or fewer sites, run every time. Otherwise, throttle to reduce load:
		 * attempt to do no more than threshold value, with some +/- allowed.
		 */
		if ( $c <= 50 || ( $c > 50 && mt_rand( 0, (int)( $c / 50 ) ) == 1 ) ) {
			require_once( ABSPATH . WPINC . '/http.php' );
			$response = wp_remote_get( admin_url( 'upgrade.php?step=1' ), array( 'timeout' => 120, 'httpversion' => '1.1' ) );
			/** This action is documented in wp-admin/network/upgrade.php */
			do_action( 'after_mu_upgrade', $response );
			unset($response);
		}
		unset($c);
	}
}

require_once(ABSPATH . 'wp-admin/includes/admin.php');

//auth_redirect();

// Schedule trash collection
if ( !wp_next_scheduled('wp_scheduled_delete') && !defined('WP_INSTALLING') )
	wp_schedule_event(time(), 'daily', 'wp_scheduled_delete');

set_screen_options();

$date_format = get_option('date_format');
$time_format = get_option('time_format');

wp_enqueue_script( 'common' );

// $pagenow is set in vars.php
// $wp_importers is sometimes set in wp-admin/includes/import.php
//
// The remaining variables are imported as globals elsewhere,
//     declared as globals here
global $pagenow, $wp_importers, $hook_suffix, $plugin_page, $typenow, $taxnow;

$page_hook = null;

$editing = false;

if ( isset($_GET['page']) ) {
	$plugin_page = wp_unslash( $_GET['page'] );
	$plugin_page = plugin_basename($plugin_page);
}

if ( isset( $_REQUEST['post_type'] ) && post_type_exists( $_REQUEST['post_type'] ) )
	$typenow = $_REQUEST['post_type'];
else
	$typenow = '';

if ( isset( $_REQUEST['taxonomy'] ) && taxonomy_exists( $_REQUEST['taxonomy'] ) )
	$taxnow = $_REQUEST['taxonomy'];
else
	$taxnow = '';

if ( WP_NETWORK_ADMIN )
	require(ABSPATH . 'wp-admin/network/menu.php');
elseif ( WP_USER_ADMIN )
	require(ABSPATH . 'wp-admin/user/menu.php');
else
	require(ABSPATH . 'wp-admin/menu.php');

if ( current_user_can( 'manage_options' ) ) {
	/**
	 * Filter the maximum memory limit available for administration screens.
	 *
	 * This only applies to administrators, who may require more memory for tasks like updates.
	 * Memory limits when processing images (uploaded or edited by users of any role) are
	 * handled separately.
	 *
	 * The WP_MAX_MEMORY_LIMIT constant specifically defines the maximum memory limit available
	 * when in the administration back-end. The default is 256M, or 256 megabytes of memory.
	 *
	 * @since 3.0.0
	 *
	 * @param string 'WP_MAX_MEMORY_LIMIT' The maximum WordPress memory limit. Default 256M.
	 */
	@ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
}

/**
 * Fires as an admin screen or script is being initialized.
 *
 * Note, this does not just run on user-facing admin screens.
 * It runs on admin-ajax.php and admin-post.php as well.
 *
 * This is roughly analgous to the more general 'init' hook, which fires earlier.
 *
 * @since 2.5.0
 */
do_action( 'admin_init' );

if ( isset($plugin_page) ) {
	if ( !empty($typenow) )
		$the_parent = $pagenow . '?post_type=' . $typenow;
	else
		$the_parent = $pagenow;
	if ( ! $page_hook = get_plugin_page_hook($plugin_page, $the_parent) ) {
		$page_hook = get_plugin_page_hook($plugin_page, $plugin_page);

		// Backwards compatibility for plugins using add_management_page().
		if ( empty( $page_hook ) && 'edit.php' == $pagenow && '' != get_plugin_page_hook($plugin_page, 'tools.php') ) {
			// There could be plugin specific params on the URL, so we need the whole query string
			if ( !empty($_SERVER[ 'QUERY_STRING' ]) )
				$query_string = $_SERVER[ 'QUERY_STRING' ];
			else
				$query_string = 'page=' . $plugin_page;
			wp_redirect( admin_url('tools.php?' . $query_string) );
			exit;
		}
	}
	unset($the_parent);
}

$hook_suffix = '';
if ( isset( $page_hook ) ) {
	$hook_suffix = $page_hook;
} elseif ( isset( $plugin_page ) ) {
	$hook_suffix = $plugin_page;
} elseif ( isset( $pagenow ) ) {
	$hook_suffix = $pagenow;
}

set_current_screen();

// Handle plugin admin pages.
if ( isset($plugin_page) ) {
	if ( $page_hook ) {
		/**
		 * Fires before a particular screen is loaded.
		 *
		 * The load-* hook fires in a number of contexts. This hook is for plugin screens
		 * where a callback is provided when the screen is registered.
		 *
		 * The dynamic portion of the hook name, `$page_hook`, refers to a mixture of plugin
		 * page information including:
		 * 1. The page type. If the plugin page is registered as a submenu page, such as for
		 *    Settings, the page type would be 'settings'. Otherwise the type is 'toplevel'.
		 * 2. A separator of '_page_'.
		 * 3. The plugin basename minus the file extension.
		 *
		 * Together, the three parts form the `$page_hook`. Citing the example above,
		 * the hook name used would be 'load-settings_page_pluginbasename'.
		 *
		 * @see get_plugin_page_hook()
		 *
		 * @since 2.1.0
		 */
		do_action( 'load-' . $page_hook );
		if (! isset($_GET['noheader']))
			require_once(ABSPATH . 'wp-admin/admin-header.php');

		/**
		 * Used to call the registered callback for a plugin screen.
		 *
		 * @ignore
		 * @since 1.5.0
		 */
		do_action( $page_hook );
	} else {
		if ( validate_file($plugin_page) )
			wp_die(__('Invalid plugin page'));

		if ( !( file_exists(WP_PLUGIN_DIR . "/$plugin_page") && is_file(WP_PLUGIN_DIR . "/$plugin_page") ) && !( file_exists(WPMU_PLUGIN_DIR . "/$plugin_page") && is_file(WPMU_PLUGIN_DIR . "/$plugin_page") ) )
			wp_die(sprintf(__('Cannot load %s.'), htmlentities($plugin_page)));

		/**
		 * Fires before a particular screen is loaded.
		 *
		 * The load-* hook fires in a number of contexts. This hook is for plugin screens
		 * where the file to load is directly included, rather than the use of a function.
		 *
		 * The dynamic portion of the hook name, `$plugin_page`, refers to the plugin basename.
		 *
		 * @see plugin_basename()
		 *
		 * @since 1.5.0
		 */
		do_action( 'load-' . $plugin_page );

		if ( !isset($_GET['noheader']))
			require_once(ABSPATH . 'wp-admin/admin-header.php');

		if ( file_exists(WPMU_PLUGIN_DIR . "/$plugin_page") )
			include(WPMU_PLUGIN_DIR . "/$plugin_page");
		else
			include(WP_PLUGIN_DIR . "/$plugin_page");
	}

	include(ABSPATH . 'wp-admin/admin-footer.php');

	exit();
} elseif ( isset( $_GET['import'] ) ) {

	$importer = $_GET['import'];

	if ( ! current_user_can('import') )
		wp_die(__('You are not allowed to import.'));

	if ( validate_file($importer) ) {
		wp_redirect( admin_url( 'import.php?invalid=' . $importer ) );
		exit;
	}

	if ( ! isset($wp_importers[$importer]) || ! is_callable($wp_importers[$importer][2]) ) {
		wp_redirect( admin_url( 'import.php?invalid=' . $importer ) );
		exit;
	}

	/**
	 * Fires before an importer screen is loaded.
	 *
	 * The dynamic portion of the hook name, `$importer`, refers to the importer slug.
	 *
	 * @since 3.5.0
	 */
	do_action( 'load-importer-' . $importer );

	$parent_file = 'tools.php';
	$submenu_file = 'import.php';
	$title = __('Import');

	if (! isset($_GET['noheader']))
		require_once(ABSPATH . 'wp-admin/admin-header.php');

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	define('WP_IMPORTING', true);

	/**
	 * Whether to filter imported data through kses on import.
	 *
	 * Multisite uses this hook to filter all data through kses by default,
	 * as a super administrator may be assisting an untrusted user.
	 *
	 * @since 3.1.0
	 *
	 * @param bool false Whether to force data to be filtered through kses. Default false.
	 */
	if ( apply_filters( 'force_filtered_html_on_import', false ) ) {
		kses_init_filters();  // Always filter imported data with kses on multisite.
	}

	call_user_func($wp_importers[$importer][2]);

	include(ABSPATH . 'wp-admin/admin-footer.php');

	// Make sure rules are flushed
	flush_rewrite_rules(false);

	exit();
} else {
	/**
	 * Fires before a particular screen is loaded.
	 *
	 * The load-* hook fires in a number of contexts. This hook is for core screens.
	 *
	 * The dynamic portion of the hook name, `$pagenow`, is a global variable
	 * referring to the filename of the current page, such as 'admin.php',
	 * 'post-new.php' etc. A complete hook for the latter would be
	 * 'load-post-new.php'.
	 *
	 * @since 2.1.0
	 */
	do_action( 'load-' . $pagenow );

	/*
	 * The following hooks are fired to ensure backward compatibility.
	 * In all other cases, 'load-' . $pagenow should be used instead.
	 */
	if ( $typenow == 'page' ) {
		if ( $pagenow == 'post-new.php' )
			do_action( 'load-page-new.php' );
		elseif ( $pagenow == 'post.php' )
			do_action( 'load-page.php' );
	}  elseif ( $pagenow == 'edit-tags.php' ) {
		if ( $taxnow == 'category' )
			do_action( 'load-categories.php' );
		elseif ( $taxnow == 'link_category' )
			do_action( 'load-edit-link-categories.php' );
	}
}

if ( ! empty( $_REQUEST['action'] ) ) {
	/**
	 * Fires when an 'action' request variable is sent.
	 *
	 * The dynamic portion of the hook name, `$_REQUEST['action']`,
	 * refers to the action derived from the `GET` or `POST` request.
	 *
	 * @since 2.6.0
	 */
	do_action( 'admin_action_' . $_REQUEST['action'] );
}
// end admin
global $post_type, $post_type_object, $post;

if ( ! isset( $_GET['post_type'] ) ) {
	$post_type = 'post';
} elseif ( in_array( $_GET['post_type'], get_post_types( array('show_ui' => true ) ) ) ) {
	$post_type = $_GET['post_type'];
} else {
	wp_die( __('Invalid post type') );
}
$post_type_object = get_post_type_object( $post_type );

if ( 'post' == $post_type ) {
	$parent_file = 'edit.php';
	$submenu_file = 'post-new.php';
} elseif ( 'attachment' == $post_type ) {
	if ( wp_redirect( admin_url( 'media-new.php' ) ) )
		exit;
} else {
	$submenu_file = "post-new.php?post_type=$post_type";
	if ( isset( $post_type_object ) && $post_type_object->show_in_menu && $post_type_object->show_in_menu !== true ) {
		$parent_file = $post_type_object->show_in_menu;
		// What if there isn't a post-new.php item for this post type?
		if ( ! isset( $_registered_pages[ get_plugin_page_hookname( "post-new.php?post_type=$post_type", $post_type_object->show_in_menu ) ] ) ) {
			if (	isset( $_registered_pages[ get_plugin_page_hookname( "edit.php?post_type=$post_type", $post_type_object->show_in_menu ) ] ) ) {
				// Fall back to edit.php for that post type, if it exists
				$submenu_file = "edit.php?post_type=$post_type";
			} else {
				// Otherwise, give up and highlight the parent
				$submenu_file = $parent_file;
			}
		}
	} else {
		$parent_file = "edit.php?post_type=$post_type";
	}
}

$title = $post_type_object->labels->add_new_item;

$editing = true;
?>
<?php get_header(); ?>

    <!-- Start Content -->
    <div class="container p_90">

    <!-- Start Entries -->
    <div class="grid-3-4 centre-block single-page">

    <?php
	/* ================================================================== */
	/* Start of Loop */
	/* ================================================================== */
	while (have_posts()) : the_post();

// start admin
?>

<form id="post" method="post" action="/doityourselfnation.org/wp-admin/post.php" name="post">
<?php
$post_ID = isset($post_ID) ? (int) $post_ID : 0;
$user_ID = isset($user_ID) ? (int) $user_ID : 0;
$action = isset($action) ? $action : '';
$nonce_action = 'update-post_' . $post_ID;
wp_nonce_field($nonce_action);
echo "<input type='hidden' id='post_ID' name='post_ID' value='" . esc_attr($post_ID) . "' />";
?>
<input type="hidden" id="user-id" name="user_ID" value="<?php echo (int) $user_ID ?>" />

<input type="hidden" value="/doityourselfnation.org/wp-admin/post-new.php" name="_wp_http_referer"><input type="hidden" value="1" name="user_ID" id="user-id">
<input type="hidden" value="editpost" name="action" id="hiddenaction">
<input type="hidden" value="editpost" name="originalaction" id="originalaction">
<input type="hidden" value="1" name="post_author" id="post_author">
<input type="hidden" value="post" name="post_type" id="post_type">
<input type="hidden" value="auto-draft" name="original_post_status" id="original_post_status">
<input type="hidden" value="http://wpdev.mainstreethost.com/doityourselfnation.org/wp-admin/edit.php?post_type=page" name="referredby" id="referredby">
<input type="hidden" value="http://wpdev.mainstreethost.com/doityourselfnation.org/wp-admin/edit.php?post_type=page" name="_wp_original_http_referer"><input type="hidden" value="1" name="auto_draft" id="auto_draft">

<?php
wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );
?>

<div id="poststuff">
<div class="metabox-holder columns-2" id="post-body">
<div id="post-body-content" style="position: relative;">

<div id="titlediv">
<div id="titlewrap">
		<label for="title" id="title-prompt-text" class="">Enter title here</label>
	<input type="text" autocomplete="off" spellcheck="true" id="title" value="" size="30" name="post_title">
         </div>
<div class="inside">
	<div class="hide-if-no-js" id="edit-slug-box">
		</div>
</div>
<?php wp_nonce_field( 'samplepermalink', 'samplepermalinknonce', false ); ?></div><!-- /titlediv -->
<div class="postarea wp-editor-expand" id="postdivrich">

<div class="wp-core-ui wp-editor-wrap tmce-active has-dfw" id="wp-content-wrap" style="padding-top: 55px;"><link media="all" type="text/css" href="http://wpdev.mainstreethost.com/doityourselfnation.org/wp-includes/css/editor.min.css?ver=4.2.2" id="editor-buttons-css" rel="stylesheet">
<div class="wp-editor-tools hide-if-no-js" id="wp-content-editor-tools" style="position: absolute; top: 0px; width: 1081px;"><div class="wp-media-buttons" id="wp-content-media-buttons"><a title="Add Media" data-editor="content" class="button insert-media add_media" id="insert-media-button" href="#"><span class="wp-media-buttons-icon"></span> Add Media</a><style>.gform_media_icon{
                    background-position: center center;
				    background-repeat: no-repeat;
				    background-size: 16px auto;
				    float: left;
				    height: 16px;
				    margin: 0;
				    text-align: center;
				    width: 16px;
					padding-top:10px;
                    }
                    .gform_media_icon:before{
                    color: #999;
				    padding: 7px 0;
				    transition: all 0.1s ease-in-out 0s;
                    }
                    .wp-core-ui a.gform_media_link{
                     padding-left: 0.4em;
                    }
                 </style>
                  <a title="Add Gravity Form" id="add_gform" class="button gform_media_link" href="#"><div style="background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSItMTUgNzcgNTgxIDY0MCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAtMTUgNzcgNTgxIDY0MCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGcgaWQ9IkxheWVyXzIiPjxwYXRoIGZpbGw9IiM5OTk5OTkiIGQ9Ik00ODkuNSwyMjdMNDg5LjUsMjI3TDMxNS45LDEyNi44Yy0yMi4xLTEyLjgtNTguNC0xMi44LTgwLjUsMEw2MS44LDIyN2MtMjIuMSwxMi44LTQwLjMsNDQuMi00MC4zLDY5Ljd2MjAwLjVjMCwyNS42LDE4LjEsNTYuOSw0MC4zLDY5LjdsMTczLjYsMTAwLjJjMjIuMSwxMi44LDU4LjQsMTIuOCw4MC41LDBMNDg5LjUsNTY3YzIyLjItMTIuOCw0MC4zLTQ0LjIsNDAuMy02OS43VjI5Ni44QzUyOS44LDI3MS4yLDUxMS43LDIzOS44LDQ4OS41LDIyN3ogTTQwMSwzMDAuNHY1OS4zSDI0MXYtNTkuM0g0MDF6IE0xNjMuMyw0OTAuOWMtMTYuNCwwLTI5LjYtMTMuMy0yOS42LTI5LjZjMC0xNi40LDEzLjMtMjkuNiwyOS42LTI5LjZzMjkuNiwxMy4zLDI5LjYsMjkuNkMxOTIuOSw0NzcuNiwxNzkuNiw0OTAuOSwxNjMuMyw0OTAuOXogTTE2My4zLDM1OS43Yy0xNi40LDAtMjkuNi0xMy4zLTI5LjYtMjkuNnMxMy4zLTI5LjYsMjkuNi0yOS42czI5LjYsMTMuMywyOS42LDI5LjZTMTc5LjYsMzU5LjcsMTYzLjMsMzU5Ljd6IE0yNDEsNDkwLjl2LTU5LjNoMTYwdjU5LjNIMjQxeiIvPjwvZz48L3N2Zz4=')" class="gform_media_icon svg"><br></div><div style="padding-left: 20px;">Add Form</div></a><style type="text/css"></style><a style="padding-left: .4em;" title="Add Slider" class="button soliloquy-choose-slider" href="#" id="soliloquy-media-modal-button"><span style="background: transparent url(http://wpdev.mainstreethost.com/doityourselfnation.org/wp-content/plugins/soliloquy/assets/css/images/editor-icon.png) no-repeat scroll 0 0; width: 16px; height: 16px; display: inline-block; vertical-align: text-top;" class="soliloquy-media-icon"></span> Add Slider</a></div>
<div class="wp-editor-tabs"><button onclick="switchEditors.switchto(this);" class="wp-switch-editor switch-tmce" id="content-tmce" type="button">Visual</button>
<button onclick="switchEditors.switchto(this);" class="wp-switch-editor switch-html" id="content-html" type="button">Text</button>
</div>
</div>
<div class="wp-editor-container" id="wp-content-editor-container"><div id="ed_toolbar" class="quicktags-toolbar" style="position: absolute; top: 0px; width: 1041px;"><input type="button" value="b" class="ed_button button button-small" id="qt_content_strong"><input type="button" value="i" class="ed_button button button-small" id="qt_content_em"><input type="button" value="link" class="ed_button button button-small" id="qt_content_link"><input type="button" value="b-quote" class="ed_button button button-small" id="qt_content_block"><input type="button" value="del" class="ed_button button button-small" id="qt_content_del"><input type="button" value="ins" class="ed_button button button-small" id="qt_content_ins"><input type="button" value="img" class="ed_button button button-small" id="qt_content_img"><input type="button" value="ul" class="ed_button button button-small" id="qt_content_ul"><input type="button" value="ol" class="ed_button button button-small" id="qt_content_ol"><input type="button" value="li" class="ed_button button button-small" id="qt_content_li"><input type="button" value="code" class="ed_button button button-small" id="qt_content_code"><input type="button" value="more" class="ed_button button button-small" id="qt_content_more"><input type="button" value="close tags" title="Close all open tags" class="ed_button button button-small" id="qt_content_close"><button title="Distraction-free writing mode" class="ed_button qt-dfw" id="qt_content_dfw" type="button"></button></div><div role="application" tabindex="-1" hidefocus="1" class="mce-tinymce mce-container mce-panel" id="mceu_27" style="visibility: hidden; border-width: 1px;"><div class="mce-container-body mce-stack-layout" id="mceu_27-body"><div role="group" tabindex="-1" hidefocus="1" class="mce-toolbar-grp mce-container mce-panel mce-first mce-stack-layout-item" id="mceu_28" style="position: absolute; top: 0px; width: 1079px;"><div class="mce-container-body mce-stack-layout" id="mceu_28-body"><div role="toolbar" class="mce-container mce-toolbar mce-first mce-stack-layout-item" id="mceu_29"><div class="mce-container-body mce-flow-layout" id="mceu_29-body"><div class="mce-container mce-first mce-last mce-flow-layout-item mce-btn-group" id="mceu_30" role="group"><div id="mceu_30-body"><div aria-labelledby="mceu_0" tabindex="-1" class="mce-widget mce-btn mce-first" id="mceu_0" role="button" aria-label="Bold"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-bold"></i></button></div><div aria-labelledby="mceu_1" tabindex="-1" class="mce-widget mce-btn" id="mceu_1" role="button" aria-label="Italic"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-italic"></i></button></div><div aria-labelledby="mceu_2" tabindex="-1" class="mce-widget mce-btn" id="mceu_2" role="button" aria-label="Strikethrough"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-strikethrough"></i></button></div><div aria-labelledby="mceu_3" tabindex="-1" class="mce-widget mce-btn" id="mceu_3" role="button" aria-label="Bullet list"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-bullist"></i></button></div><div aria-labelledby="mceu_4" tabindex="-1" class="mce-widget mce-btn" id="mceu_4" role="button" aria-label="Numbered list"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-numlist"></i></button></div><div aria-labelledby="mceu_5" tabindex="-1" class="mce-widget mce-btn" id="mceu_5" role="button" aria-label="Blockquote"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-blockquote"></i></button></div><div aria-labelledby="mceu_6" tabindex="-1" class="mce-widget mce-btn" id="mceu_6" role="button" aria-label="Horizontal line"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-hr"></i></button></div><div aria-labelledby="mceu_7" tabindex="-1" class="mce-widget mce-btn" id="mceu_7" role="button" aria-label="Align left"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-alignleft"></i></button></div><div aria-labelledby="mceu_8" tabindex="-1" class="mce-widget mce-btn" id="mceu_8" role="button" aria-label="Align center"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-aligncenter"></i></button></div><div aria-labelledby="mceu_9" tabindex="-1" class="mce-widget mce-btn" id="mceu_9" role="button" aria-label="Align right"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-alignright"></i></button></div><div aria-labelledby="mceu_10" tabindex="-1" class="mce-widget mce-btn" id="mceu_10" role="button" aria-label="Insert/edit link"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-link"></i></button></div><div aria-labelledby="mceu_11" tabindex="-1" class="mce-widget mce-btn" id="mceu_11" role="button" aria-label="Remove link"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-unlink"></i></button></div><div aria-labelledby="mceu_12" tabindex="-1" class="mce-widget mce-btn" id="mceu_12" role="button" aria-label="Insert Read More tag"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-wp_more"></i></button></div><div aria-labelledby="mceu_13" tabindex="-1" class="mce-wp-dfw mce-btn mce-widget" id="mceu_13" role="button" aria-disabled="false" aria-pressed="false" aria-label="Distraction-free writing mode"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-dfw"></i></button></div><div aria-labelledby="mceu_14" tabindex="-1" class="mce-widget mce-btn mce-last mce-active" id="mceu_14" role="button" aria-label="Toolbar Toggle" aria-pressed="true"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-wp_adv"></i></button></div></div></div></div></div><div role="toolbar" class="mce-container mce-toolbar mce-last mce-stack-layout-item" id="mceu_31"><div class="mce-container-body mce-flow-layout" id="mceu_31-body"><div class="mce-container mce-first mce-last mce-flow-layout-item mce-btn-group" id="mceu_32" role="group"><div id="mceu_32-body"><div aria-labelledby="mceu_15" tabindex="-1" class="mce-widget mce-btn mce-menubtn mce-fixed-width mce-listbox mce-first" id="mceu_15" role="button" aria-haspopup="true"><button tabindex="-1" type="button" role="presentation" id="mceu_15-open"><span>Paragraph</span> <i class="mce-caret"></i></button></div><div aria-labelledby="mceu_16" tabindex="-1" class="mce-widget mce-btn" id="mceu_16" role="button" aria-label="Underline"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-underline"></i></button></div><div aria-labelledby="mceu_17" tabindex="-1" class="mce-widget mce-btn" id="mceu_17" role="button" aria-label="Justify"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-alignjustify"></i></button></div><div aria-haspopup="true" tabindex="-1" role="button" class="mce-widget mce-btn mce-colorbutton" id="mceu_18" aria-label="Text color"><button tabindex="-1" type="button" hidefocus="1" role="presentation"><i class="mce-ico mce-i-forecolor"></i><span class="mce-preview" id="mceu_18-preview"></span></button><button tabindex="-1" hidefocus="1" class="mce-open" type="button"> <i class="mce-caret"></i></button></div><div aria-labelledby="mceu_19" tabindex="-1" class="mce-widget mce-btn" id="mceu_19" role="button" aria-pressed="false" aria-label="Paste as text"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-pastetext"></i></button></div><div aria-labelledby="mceu_20" tabindex="-1" class="mce-widget mce-btn" id="mceu_20" role="button" aria-label="Clear formatting"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-removeformat"></i></button></div><div aria-labelledby="mceu_21" tabindex="-1" class="mce-widget mce-btn" id="mceu_21" role="button" aria-label="Special character"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-charmap"></i></button></div><div aria-labelledby="mceu_22" tabindex="-1" class="mce-widget mce-btn" id="mceu_22" role="button" aria-label="Decrease indent"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-outdent"></i></button></div><div aria-labelledby="mceu_23" tabindex="-1" class="mce-widget mce-btn" id="mceu_23" role="button" aria-label="Increase indent" aria-disabled="false"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-indent"></i></button></div><div aria-labelledby="mceu_24" tabindex="-1" class="mce-widget mce-btn mce-disabled" id="mceu_24" role="button" aria-label="Undo" aria-disabled="true"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-undo"></i></button></div><div aria-labelledby="mceu_25" tabindex="-1" class="mce-widget mce-btn mce-disabled" id="mceu_25" role="button" aria-label="Redo" aria-disabled="true"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-redo"></i></button></div><div aria-labelledby="mceu_26" tabindex="-1" class="mce-widget mce-btn mce-last" id="mceu_26" role="button" aria-label="Keyboard Shortcuts"><button tabindex="-1" type="button" role="presentation"><i class="mce-ico mce-i-wp_help"></i></button></div></div></div></div></div></div></div><div role="group" tabindex="-1" hidefocus="1" class="mce-edit-area mce-container mce-panel mce-stack-layout-item" id="mceu_33" style="border-width: 1px 0px 0px; padding-top: 67px;"><iframe frameborder="0" id="content_ifr" allowtransparency="true" title="Rich Text Area. Press Alt-Shift-H for help" style="width: 100%; height: 300px; display: block;" src="javascript:&quot;&quot;"></iframe></div><div role="group" tabindex="-1" hidefocus="1" class="mce-statusbar mce-container mce-panel mce-last mce-stack-layout-item" id="mceu_34" style=""><div class="mce-container-body mce-flow-layout" id="mceu_34-body"><div class="mce-path mce-first mce-last mce-flow-layout-item" id="mceu_35"><div aria-level="0" id="mceu_35-0" tabindex="-1" data-index="0" class="mce-path-item mce-last" role="button">p</div></div></div></div></div></div><textarea id="content" name="content" cols="40" autocomplete="off" style="height: 300px; margin-top: 7px; display: none;" class="wp-editor-area" aria-hidden="true"></textarea><div class="wp-exclude-emoji" id="content-textarea-clone" style="font-family: Consolas,Monaco,monospace; font-size: 13px; line-height: 19.5px; white-space: pre-wrap; word-wrap: break-word; width: 1059px;"></div></div>
<div class="uploader-editor">
		<div class="uploader-editor-content">
			<div class="uploader-editor-title">Drop files to upload</div>
		</div>
	</div></div>

<table id="post-status-info" style=""><tbody><tr>
	<td id="wp-word-count">Word count: <span class="word-count">0</span></td>
	<td class="autosave-info">
	<span class="autosave-message">&nbsp;</span>
	</td>
	<td class="hide-if-no-js" id="content-resize-handle"><br></td>
</tr></tbody></table>

</div>
</div><!-- /post-body-content -->

<div class="postbox-container" id="postbox-container-1">
<div class="meta-box-sortables ui-sortable" id="side-sortables" style=""><div class="postbox " id="submitdiv">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Publish</span></h3>
<div class="inside">
<div id="submitpost" class="submitbox">

<div id="minor-publishing">

<div style="display:none;">
<p class="submit"><input type="submit" value="Save" class="button" id="save" name="save"></p></div>

<div id="minor-publishing-actions">
<div id="save-action">
<input type="submit" class="button" value="Save Draft" id="save-post" name="save">
<span class="spinner"></span>
</div>
<div id="preview-action">
<a id="post-preview" target="wp-preview-103" href="http://wpdev.mainstreethost.com/doityourselfnation.org/?p=103&amp;preview=true" class="preview button">Preview</a>
<input type="hidden" value="" id="wp-preview" name="wp-preview">
</div>
<div class="clear"></div>
</div><!-- #minor-publishing-actions -->

<div id="misc-publishing-actions">

<div class="misc-pub-section misc-pub-post-status"><label for="post_status">Status:</label>
<span id="post-status-display">
Draft</span>
<a class="edit-post-status hide-if-no-js" href="#post_status"><span aria-hidden="true">Edit</span> <span class="screen-reader-text">Edit status</span></a>

<div class="hide-if-js" id="post-status-select">
<input type="hidden" value="draft" id="hidden_post_status" name="hidden_post_status">
<select id="post_status" name="post_status">
<option value="pending">Pending Review</option>
<option value="draft" selected="selected">Draft</option>
</select>
 <a class="save-post-status hide-if-no-js button" href="#post_status">OK</a>
 <a class="cancel-post-status hide-if-no-js button-cancel" href="#post_status">Cancel</a>
</div>

</div><!-- .misc-pub-section -->

<div id="visibility" class="misc-pub-section misc-pub-visibility">
Visibility: <span id="post-visibility-display">Public</span>
<a class="edit-visibility hide-if-no-js" href="#visibility"><span aria-hidden="true">Edit</span> <span class="screen-reader-text">Edit visibility</span></a>

<div class="hide-if-js" id="post-visibility-select">
<input type="hidden" value="" id="hidden-post-password" name="hidden_post_password">
<input type="checkbox" value="sticky" id="hidden-post-sticky" name="hidden_post_sticky" style="display:none">
<input type="hidden" value="public" id="hidden-post-visibility" name="hidden_post_visibility">
<input type="radio" checked="checked" value="public" id="visibility-radio-public" name="visibility"> <label class="selectit" for="visibility-radio-public">Public</label><br>
<span id="sticky-span"><input type="checkbox" value="sticky" name="sticky" id="sticky"> <label class="selectit" for="sticky">Stick this post to the front page</label><br></span>
<input type="radio" value="password" id="visibility-radio-password" name="visibility"> <label class="selectit" for="visibility-radio-password">Password protected</label><br>
<span id="password-span"><label for="post_password">Password:</label> <input type="text" maxlength="20" value="" id="post_password" name="post_password"><br></span>
<input type="radio" value="private" id="visibility-radio-private" name="visibility"> <label class="selectit" for="visibility-radio-private">Private</label><br>

<p>
 <a class="save-post-visibility hide-if-no-js button" href="#visibility">OK</a>
 <a class="cancel-post-visibility hide-if-no-js button-cancel" href="#visibility">Cancel</a>
</p>
</div>

</div><!-- .misc-pub-section -->

<div class="misc-pub-section curtime misc-pub-curtime">
	<span id="timestamp">
	Publish <b>immediately</b></span>
	<a class="edit-timestamp hide-if-no-js" href="#edit_timestamp"><span aria-hidden="true">Edit</span> <span class="screen-reader-text">Edit date and time</span></a>
	<div class="hide-if-js" id="timestampdiv"><div class="timestamp-wrap"><label class="screen-reader-text" for="mm">Month</label><select name="mm" id="mm">
			<option value="01">01-Jan</option>
			<option value="02">02-Feb</option>
			<option value="03">03-Mar</option>
			<option value="04">04-Apr</option>
			<option value="05">05-May</option>
			<option value="06">06-Jun</option>
			<option value="07">07-Jul</option>
			<option value="08">08-Aug</option>
			<option selected="selected" value="09">09-Sep</option>
			<option value="10">10-Oct</option>
			<option value="11">11-Nov</option>
			<option value="12">12-Dec</option>
</select> <label class="screen-reader-text" for="jj">Day</label><input type="text" autocomplete="off" maxlength="2" size="2" value="11" name="jj" id="jj">, <label class="screen-reader-text" for="aa">Year</label><input type="text" autocomplete="off" maxlength="4" size="4" value="2015" name="aa" id="aa"> @ <label class="screen-reader-text" for="hh">Hour</label><input type="text" autocomplete="off" maxlength="2" size="2" value="11" name="hh" id="hh"> : <label class="screen-reader-text" for="mn">Minute</label><input type="text" autocomplete="off" maxlength="2" size="2" value="38" name="mn" id="mn"></div><input type="hidden" value="00" name="ss" id="ss">

<input type="hidden" value="09" name="hidden_mm" id="hidden_mm">
<input type="hidden" value="09" name="cur_mm" id="cur_mm">
<input type="hidden" value="11" name="hidden_jj" id="hidden_jj">
<input type="hidden" value="11" name="cur_jj" id="cur_jj">
<input type="hidden" value="2015" name="hidden_aa" id="hidden_aa">
<input type="hidden" value="2015" name="cur_aa" id="cur_aa">
<input type="hidden" value="11" name="hidden_hh" id="hidden_hh">
<input type="hidden" value="11" name="cur_hh" id="cur_hh">
<input type="hidden" value="38" name="hidden_mn" id="hidden_mn">
<input type="hidden" value="38" name="cur_mn" id="cur_mn">

<p>
<a class="save-timestamp hide-if-no-js button" href="#edit_timestamp">OK</a>
<a class="cancel-timestamp hide-if-no-js button-cancel" href="#edit_timestamp">Cancel</a>
</p>
</div>
</div>
</div>
<div class="clear"></div>
</div>

<div id="major-publishing-actions">
<div id="delete-action">
<a href="http://wpdev.mainstreethost.com/doityourselfnation.org/wp-admin/post.php?post=103&amp;action=trash&amp;_wpnonce=7b60cdee7b" class="submitdelete deletion">Move to Trash</a></div>

<div id="publishing-action">
<span class="spinner"></span>
		<input type="hidden" value="Publish" id="original_publish" name="original_publish">
		<input type="submit" value="Publish" class="button button-primary button-large" id="publish" name="publish"></div>
<div class="clear"></div>
</div>
</div>

</div>
</div>
<div class="postbox " id="categorydiv">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Categories</span></h3>
<div class="inside">
	<div class="categorydiv" id="taxonomy-category">
		<ul class="category-tabs" id="category-tabs">
			<li class="tabs"><a href="#category-all">All Categories</a></li>
			<li class="hide-if-no-js"><a href="#category-pop">Most Used</a></li>
		</ul>

		<div style="display: none;" class="tabs-panel" id="category-pop">
			<ul class="categorychecklist form-no-clear" id="categorychecklist-pop">

		<li class="popular-category" id="popular-category-23">
			<label class="selectit">
				<input type="checkbox" value="23" id="in-popular-category-23">
				Featured			</label>
		</li>


		<li class="popular-category" id="popular-category-3">
			<label class="selectit">
				<input type="checkbox" value="3" id="in-popular-category-3">
				Arts &amp; Entertainment			</label>
		</li>


		<li class="popular-category" id="popular-category-29">
			<label class="selectit">
				<input type="checkbox" value="29" id="in-popular-category-29">
				Health			</label>
		</li>


		<li class="popular-category" id="popular-category-5">
			<label class="selectit">
				<input type="checkbox" value="5" id="in-popular-category-5">
				Cars &amp; Other Vehicles			</label>
		</li>


		<li class="popular-category" id="popular-category-6">
			<label class="selectit">
				<input type="checkbox" value="6" id="in-popular-category-6">
				Hobbies &amp; Crafts			</label>
		</li>


		<li class="popular-category" id="popular-category-9">
			<label class="selectit">
				<input type="checkbox" value="9" id="in-popular-category-9">
				Holidays &amp; Traditions			</label>
		</li>


		<li class="popular-category" id="popular-category-10">
			<label class="selectit">
				<input type="checkbox" value="10" id="in-popular-category-10">
				Travel			</label>
		</li>


		<li class="popular-category" id="popular-category-17">
			<label class="selectit">
				<input type="checkbox" value="17" id="in-popular-category-17">
				Finance, Business &amp; Legal			</label>
		</li>


		<li class="popular-category" id="popular-category-21">
			<label class="selectit">
				<input type="checkbox" value="21" id="in-popular-category-21">
				Philosophy &amp; Religion			</label>
		</li>

					</ul>
		</div>

		<div class="tabs-panel" id="category-all">
			<input type="hidden" value="0" name="post_category[]">			<ul class="categorychecklist form-no-clear" data-wp-lists="list:category" id="categorychecklist">

<li class="popular-category" id="category-3"><label class="selectit"><input type="checkbox" id="in-category-3" name="post_category[]" value="3"> Arts &amp; Entertainment</label></li>

<li id="category-1"><label class="selectit"><input type="checkbox" id="in-category-1" name="post_category[]" value="1"> Blog Post</label></li>

<li class="popular-category" id="category-5"><label class="selectit"><input type="checkbox" id="in-category-5" name="post_category[]" value="5"> Cars &amp; Other Vehicles</label></li>

<li id="category-8"><label class="selectit"><input type="checkbox" id="in-category-8" name="post_category[]" value="8"> Computers &amp; Electronics</label></li>

<li id="category-19"><label class="selectit"><input type="checkbox" id="in-category-19" name="post_category[]" value="19"> DYN</label></li>

<li id="category-11"><label class="selectit"><input type="checkbox" id="in-category-11" name="post_category[]" value="11"> Education &amp; Communications</label></li>

<li id="category-14"><label class="selectit"><input type="checkbox" id="in-category-14" name="post_category[]" value="14"> Family Life</label></li>

<li class="popular-category" id="category-23"><label class="selectit"><input type="checkbox" id="in-category-23" name="post_category[]" value="23"> Featured</label></li>

<li class="popular-category" id="category-17"><label class="selectit"><input type="checkbox" id="in-category-17" name="post_category[]" value="17"> Finance, Business &amp; Legal</label></li>

<li id="category-20"><label class="selectit"><input type="checkbox" id="in-category-20" name="post_category[]" value="20"> Food &amp; Entertaining</label></li>

<li class="popular-category" id="category-29"><label class="selectit"><input type="checkbox" id="in-category-29" name="post_category[]" value="29"> Health</label></li>

<li class="popular-category" id="category-6"><label class="selectit"><input type="checkbox" id="in-category-6" name="post_category[]" value="6"> Hobbies &amp; Crafts</label></li>

<li class="popular-category" id="category-9"><label class="selectit"><input type="checkbox" id="in-category-9" name="post_category[]" value="9"> Holidays &amp; Traditions</label></li>

<li id="category-12"><label class="selectit"><input type="checkbox" id="in-category-12" name="post_category[]" value="12"> Home &amp; Garden</label></li>

<li id="category-22"><label class="selectit"><input type="checkbox" id="in-category-22" name="post_category[]" value="22"> Other</label></li>

<li id="category-15"><label class="selectit"><input type="checkbox" id="in-category-15" name="post_category[]" value="15"> Personal Care &amp; Style</label></li>

<li id="category-18"><label class="selectit"><input type="checkbox" id="in-category-18" name="post_category[]" value="18"> Pets &amp; Animals</label></li>

<li class="popular-category" id="category-21"><label class="selectit"><input type="checkbox" id="in-category-21" name="post_category[]" value="21"> Philosophy &amp; Religion</label></li>

<li id="category-4"><label class="selectit"><input type="checkbox" id="in-category-4" name="post_category[]" value="4"> Relationships</label></li>

<li id="category-7"><label class="selectit"><input type="checkbox" id="in-category-7" name="post_category[]" value="7"> Sports &amp; Fitness</label></li>

<li class="popular-category" id="category-10"><label class="selectit"><input type="checkbox" id="in-category-10" name="post_category[]" value="10"> Travel</label></li>

<li id="category-13"><label class="selectit"><input type="checkbox" id="in-category-13" name="post_category[]" value="13"> Work World</label></li>

<li id="category-16"><label class="selectit"><input type="checkbox" id="in-category-16" name="post_category[]" value="16"> Youth</label></li>
			</ul>
		</div>
				<div class="wp-hidden-children" id="category-adder">
				<h4>
					<a class="hide-if-no-js" href="#category-add" id="category-add-toggle">
						+ Add New Category					</a>
				</h4>
				<p class="category-add wp-hidden-child" id="category-add">
					<label for="newcategory" class="screen-reader-text">Add New Category</label>
					<input type="text" aria-required="true" value="New Category Name" class="form-required form-input-tip" id="newcategory" name="newcategory">
					<label for="newcategory_parent" class="screen-reader-text">
						Parent Category:					</label>
					<select class="postform" id="newcategory_parent" name="newcategory_parent">
	<option value="-1">&mdash; Parent Category &mdash;</option>
	<option value="3" class="level-0">Arts &amp; Entertainment</option>
	<option value="1" class="level-0">Blog Post</option>
	<option value="5" class="level-0">Cars &amp; Other Vehicles</option>
	<option value="8" class="level-0">Computers &amp; Electronics</option>
	<option value="19" class="level-0">DYN</option>
	<option value="11" class="level-0">Education &amp; Communications</option>
	<option value="14" class="level-0">Family Life</option>
	<option value="23" class="level-0">Featured</option>
	<option value="17" class="level-0">Finance, Business &amp; Legal</option>
	<option value="20" class="level-0">Food &amp; Entertaining</option>
	<option value="29" class="level-0">Health</option>
	<option value="6" class="level-0">Hobbies &amp; Crafts</option>
	<option value="9" class="level-0">Holidays &amp; Traditions</option>
	<option value="12" class="level-0">Home &amp; Garden</option>
	<option value="22" class="level-0">Other</option>
	<option value="15" class="level-0">Personal Care &amp; Style</option>
	<option value="18" class="level-0">Pets &amp; Animals</option>
	<option value="21" class="level-0">Philosophy &amp; Religion</option>
	<option value="4" class="level-0">Relationships</option>
	<option value="7" class="level-0">Sports &amp; Fitness</option>
	<option value="10" class="level-0">Travel</option>
	<option value="13" class="level-0">Work World</option>
	<option value="16" class="level-0">Youth</option>
</select>

				</p>
			</div>
			</div>
	</div>
</div>
<div class="postbox " id="tagsdiv-post_tag">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Tags</span></h3>
<div class="inside">
<div id="post_tag" class="tagsdiv">
	<div class="jaxtag">
	<div class="nojs-tags hide-if-js">
	<p>Add or remove tags</p>
	<textarea id="tax-input-post_tag" class="the-tags" cols="20" rows="3" name="tax_input[post_tag]"></textarea></div>
 		<div class="ajaxtag hide-if-no-js">
		<label for="new-tag-post_tag" class="screen-reader-text">Tags</label>
		<p><input type="text" value="" autocomplete="off" size="16" class="newtag form-input-tip" name="newtag[post_tag]" id="new-tag-post_tag">
		<input type="button" value="Add" class="button tagadd"></p>
	</div>
	<p class="howto">Separate tags with commas</p>
		</div>
	<div class="tagchecklist"></div>
</div>
<p class="hide-if-no-js"><a id="link-post_tag" class="tagcloud-link" href="#titlediv">Choose from the most used tags</a></p>
</div>
</div>
<div class="postbox " id="postimagediv">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Featured Image</span></h3>
<div class="inside">
<p class="hide-if-no-js"><a class="thickbox" id="set-post-thumbnail" href="http://wpdev.mainstreethost.com/doityourselfnation.org/wp-admin/media-upload.php?post_id=103&amp;type=image&amp;TB_iframe=1&amp;width=753&amp;height=437" title="Set featured image">Set featured image</a></p></div>
</div>
</div></div>
<div class="postbox-container" id="postbox-container-2">
<div class="meta-box-sortables ui-sortable" id="normal-sortables"><div class="postbox " id="video_options_metabox">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Video Upload Options</span></h3>
<div class="inside">
<div class="vp-metabox"><div id="video_options[video_type]" data-vp-type="vp-radiobutton" class="vp-field vp-radiobutton vp-checked-field vp-meta-single">
	<div class="label">
		<label>Video Type</label>
		<div class="description">
</div>	</div>
	<div class="field">
		<div class="input">
<label>
		<input type="radio" value="upload" name="video_options[video_type]" class="vp-input">
	<span></span>Upload</label>
<label>
		<input type="radio" value="youtube_url" name="video_options[video_type]" class="vp-input">
	<span></span>External URL</label>
<label>
		<input type="radio" value="embed_code" name="video_options[video_type]" class="vp-input">
	<span></span>Embed Code</label>

		</div>
		<div class="vp-js-bind-loader vp-field-loader vp-hide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/vafpress/public/img/ajax-loader.gif"></div>
		<div class="validation-msgs"><ul></ul></div>
	</div>
</div><div id="video_options[video_upload]" data-vp-dependency="vp_dep_is_upload|video_type" data-vp-type="vp-upload" class="vp-field vp-upload vp-meta-single vp-hide vp-dep-inactive">
	<div class="label">
		<label>Upload Video</label>
		<div class="description">
</div>	</div>
	<div class="field">
		<div class="input">
<input type="text" value="" name="video_options[video_upload]" id="video_options[video_upload]" readonly="" class="vp-input">
<div class="buttons">
	<input type="button" value="Choose File" class="vp-js-upload vp-button button">
	<input type="button" value="x" class="vp-js-remove-upload vp-button button">
</div>
<div class="image">
	<img alt="" src="">
</div>

		</div>
		<div class="vp-js-bind-loader vp-field-loader vp-hide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/vafpress/public/img/ajax-loader.gif"></div>
		<div class="validation-msgs"><ul></ul></div>
	</div>
</div><div id="video_options[youtube_url]" data-vp-dependency="vp_dep_is_youtube_url|video_type" data-vp-type="vp-textbox" class="vp-field vp-textbox vp-meta-single vp-hide vp-dep-inactive">
	<div class="label">
		<label>URL</label>
		<div class="description">
</div>	</div>
	<div class="field">
		<div class="input">
<input type="text" value="" class="vp-input input-large" name="video_options[youtube_url]">

		</div>
		<div class="vp-js-bind-loader vp-field-loader vp-hide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/vafpress/public/img/ajax-loader.gif"></div>
		<div class="validation-msgs"><ul></ul></div>
	</div>
</div><div id="video_options[embed_code]" data-vp-dependency="vp_dep_is_embed_code|video_type" data-vp-type="vp-textarea" class="vp-field vp-textarea vp-meta-single vp-hide vp-dep-inactive">
	<div class="label">
		<label>Embed Code</label>
		<div class="description">
</div>	</div>
	<div class="field">
		<div class="input">
<textarea name="video_options[embed_code]" class="vp-input"></textarea>

		</div>
		<div class="vp-js-bind-loader vp-field-loader vp-hide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/vafpress/public/img/ajax-loader.gif"></div>
		<div class="validation-msgs"><ul></ul></div>
	</div>
</div>
<div id="video_options[nb_1]" data-vp-type="vp-notebox" class="vp-field vp-notebox vp-meta-single note-normal">
		<i class="fa fa-lightbulb-o"></i>
	<div class="label">Fun Facts</div>
	<div class="description"><p>Take note that External URL can be anything, video file such as mp4, 3gp, FLV etc. it can also be a YouTube URL and RTMP Live Steaming( rtmp:example.com/stream and Third Party CDN</p>
</div>
</div>
</div><input type="hidden" value="6e64f314d6" name="video_options_nonce"></div>
</div>
<div class="postbox  hide-if-js" id="postexcerpt" style="">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Excerpt</span></h3>
<div class="inside">
<label for="excerpt" class="screen-reader-text">Excerpt</label><textarea id="excerpt" name="excerpt" cols="40" rows="1"></textarea>
<p>Excerpts are optional hand-crafted summaries of your content that can be used in your theme. <a target="_blank" href="https://codex.wordpress.org/Excerpt">Learn more about manual excerpts.</a></p>
</div>
</div>
<div class="postbox  hide-if-js" id="trackbacksdiv" style="">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Send Trackbacks</span></h3>
<div class="inside">
<p><label for="trackback_url">Send trackbacks to:</label> <input type="text" value="" class="code" id="trackback_url" name="trackback_url"><br> (Separate multiple URLs with spaces)</p>
<p>Trackbacks are a way to notify legacy blog systems that you’ve linked to them. If you link other WordPress sites, they’ll be notified automatically using <a target="_blank" href="https://codex.wordpress.org/Introduction_to_Blogging#Managing_Comments">pingbacks</a>, no other action necessary.</p>
</div>
</div>
<div class="postbox  hide-if-js" id="postcustom" style="">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Custom Fields</span></h3>
<div class="inside">
<div id="postcustomstuff">
<div id="ajax-response"></div>

<table style="display: none;" id="list-table">
	<thead>
	<tr>
		<th class="left">Name</th>
		<th>Value</th>
	</tr>
	</thead>
	<tbody data-wp-lists="list:meta" id="the-list">
	<tr><td></td></tr>
	</tbody>
</table><p><strong>Add New Custom Field:</strong></p>
<table id="newmeta">
<thead>
<tr>
<th class="left"><label for="metakeyselect">Name</label></th>
<th><label for="metavalue">Value</label></th>
</tr>
</thead>

<tbody>
<tr>
<td class="left" id="newmetaleft">
<select name="metakeyselect" id="metakeyselect">
<option value="#NONE#">&mdash; Select &mdash;</option>

<option value="layout_builder">layout_builder</option>
<option value="playlist">playlist</option>
<option value="popularity_count">popularity_count</option>
<option value="video_options">video_options</option>
<option value="wpfront-user-role-editor-nav-menu-data">wpfront-user-role-editor-nav-menu-data</option></select>
<input type="text" value="" name="metakeyinput" id="metakeyinput" class="hide-if-js">
<a onclick="jQuery('#metakeyinput, #metakeyselect, #enternew, #cancelnew').toggle();return false;" class="hide-if-no-js" href="#postcustomstuff">
<span id="enternew">Enter new</span>
<span class="hidden" id="cancelnew">Cancel</span></a>
</td>
<td><textarea cols="25" rows="2" name="metavalue" id="metavalue"></textarea></td>
</tr>

<tr><td colspan="2">
<div class="submit">
<input type="submit" data-wp-lists="add:the-list:newmeta" value="Add Custom Field" class="button" id="newmeta-submit" name="addmeta"></div>
<input type="hidden" value="fed0d119ae" name="_ajax_nonce-add-meta" id="_ajax_nonce-add-meta"></td></tr>
</tbody>
</table>
</div>
<p>Custom fields can be used to add extra metadata to a post that you can <a target="_blank" href="https://codex.wordpress.org/Using_Custom_Fields">use in your theme</a>.</p>
</div>
</div>
<div class="postbox  hide-if-js" id="commentstatusdiv" style="">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Discussion</span></h3>
<div class="inside">
<input type="hidden" value="1" name="advanced_view">
<p class="meta-options">
	<label class="selectit" for="comment_status"><input type="checkbox" checked="checked" value="open" id="comment_status" name="comment_status"> Allow comments.</label><br>
	<label class="selectit" for="ping_status"><input type="checkbox" checked="checked" value="open" id="ping_status" name="ping_status"> Allow <a target="_blank" href="https://codex.wordpress.org/Introduction_to_Blogging#Managing_Comments">trackbacks and pingbacks</a> on this page.</label>
	</p>
</div>
</div>
<div class="postbox  hide-if-js" id="slugdiv" style="">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Slug</span></h3>
<div class="inside">
<label for="post_name" class="screen-reader-text">Slug</label><input type="text" value="" id="post_name" size="13" name="post_name">
</div>
</div>
<div class="postbox  hide-if-js" id="authordiv" style="">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>Author</span></h3>
<div class="inside">
<label for="post_author_override" class="screen-reader-text">Author</label>
<select class="" id="post_author_override" name="post_author_override">
	<option selected="selected" value="1">Matty B</option>
</select></div>
</div>
</div><div class="meta-box-sortables ui-sortable" id="advanced-sortables"><div class="postbox " id="aiosp">
<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle ui-sortable-handle"><span>All in One SEO Pack Pro<a href="http://semperplugins.com/sections/postpage-settings/" target="_blank" class="aioseop_help_text_link aioseop_meta_box_help"><span>Help</span></a></span></h3>
<div class="inside">
<input type="hidden" value="aiosp_edit" name="aiosp_edit">
<input type="hidden" value="a08496a43a" name="nonce-aioseop-edit">
<div class="aioseop aioseop_options aiosp_settings"><div id="aiosp_upgrade_wrapper" class="aioseop_wrapper aioseop_no_label  aioseop_html_type"><div class="aioseop_input"><span class="aioseop_option_input"><div class="aioseop_option_div"><a href="http://semperplugins.com/support/" target="_blank">Support Forum</a></div></span><p style="clear:left"></p></div></div><div id="aiosp_snippet_wrapper" class="aioseop_wrapper aioseop_html_type"><div class="aioseop_input"><span style="text-align:left;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_snippet_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Preview Snippet</label></a></span></div>
<div class="aioseop_input aioseop_top_label">
<span class="aioseop_option_input"><div class="aioseop_option_div">
																							<script>
																							jQuery(document).ready(function() {
																								jQuery("#aiosp_title_wrapper").bind("input", function() {
																								    jQuery("#aiosp_snippet_title").text(jQuery("#aiosp_title_wrapper input").val().replace(/&lt;(?:.|\n)*?&gt;/gm, ""));
																								});
																								jQuery("#aiosp_description_wrapper").bind("input", function() {
																								    jQuery("#aioseop_snippet_description").text(jQuery("#aiosp_description_wrapper textarea").val().replace(/&lt;(?:.|\n)*?&gt;/gm, ""));
																								});
																							});
																							</script>
																							<div class="preview_snippet"><div id="aioseop_snippet"><h3><a><span id="aiosp_snippet_title"></span> | Do It Yourself Nation</a></h3><div><div><cite id="aioseop_snippet_link">http://wpdev.mainstreethost.com/doityourselfnation.org/?p=103</cite></div><span id="aioseop_snippet_description"></span></div></div></div><script>var aiosp_title_extra = 24;</script></div><div id="aiosp_snippet_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">A preview of what this page might look like in search engine results.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_title_wrapper" class="aioseop_wrapper aioseop_text_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_title_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Title</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="text" value="" onkeyup="if (typeof countChars == &quot;function&quot;) countChars(document.post.aiosp_title,document.post.length1)" onkeydown="if (typeof countChars == &quot;function&quot;) countChars(document.post.aiosp_title,document.post.length1)" placeholder="" size="60" name="aiosp_title">
<br><input type="text" value="0" style="width: 53px; height: 23px; margin: 0px; padding: 0px 0px 0px 10px; color: rgb(81, 81, 81); background-color: rgb(238, 238, 238);" maxlength="3" size="3" name="length1" readonly=""> characters. Most search engines use a maximum of 60 chars for the title.<script>jQuery( document ).ready(function() { if (typeof countChars == "function") countChars(document.post.aiosp_title,document.post.length1); });</script></div><div id="aiosp_title_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">A custom title that shows up in the title tag for this page.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_description_wrapper" class="aioseop_wrapper aioseop_textarea_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_description_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Description</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><textarea onkeyup="if (typeof countChars == &quot;function&quot;) countChars(document.post.aiosp_description,document.post.length2)" onkeydown="if (typeof countChars == &quot;function&quot;) countChars(document.post.aiosp_description,document.post.length2)" cols="80" rows="2" placeholder="" name="aiosp_description"></textarea><br><input type="text" value="0" style="width: 53px; height: 23px; margin: 0px; padding: 0px 0px 0px 10px; color: rgb(81, 81, 81); background-color: rgb(238, 238, 238);" maxlength="3" size="3" name="length2" readonly=""> characters. Most search engines use a maximum of 160 chars for the description.<script>jQuery( document ).ready(function() { if (typeof countChars == "function") countChars(document.post.aiosp_description,document.post.length2); });</script></div><div id="aiosp_description_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">The META description for this page. This will override any autogenerated descriptions.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_keywords_wrapper" class="aioseop_wrapper aioseop_text_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_keywords_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Keywords (comma separated)</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="text" value="" placeholder="" size="57" name="aiosp_keywords">
</div><div id="aiosp_keywords_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">A comma separated list of your most important keywords for this page that will be written as META keywords.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_custom_link_wrapper" class="aioseop_wrapper aioseop_text_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_custom_link_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Custom Canonical URL</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="text" value="" size="60" name="aiosp_custom_link">
</div><div id="aiosp_custom_link_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Override the canonical URLs for this post.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_noindex_wrapper" class="aioseop_wrapper aioseop_checkbox_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_noindex_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Robots Meta NOINDEX</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="checkbox" name="aiosp_noindex">
</div><div id="aiosp_noindex_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Check this box to ask search engines not to index this page.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_nofollow_wrapper" class="aioseop_wrapper aioseop_checkbox_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_nofollow_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Robots Meta NOFOLLOW</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="checkbox" name="aiosp_nofollow">
</div><div id="aiosp_nofollow_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Check this box to ask search engines not to follow links from this page.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_noodp_wrapper" class="aioseop_wrapper aioseop_checkbox_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_noodp_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Robots Meta NOODP</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="checkbox" name="aiosp_noodp">
</div><div id="aiosp_noodp_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Check this box to ask search engines not to use descriptions from the Open Directory Project for your entire site.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/#exclude-site-from-the-open-directory-project">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_noydir_wrapper" class="aioseop_wrapper aioseop_checkbox_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_noydir_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Robots Meta NOYDIR</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="checkbox" name="aiosp_noydir">
</div><div id="aiosp_noydir_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Check this box to ask Yahoo! not to use descriptions from the Yahoo! directory for your entire site.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/#exclude-site-from-yahoo-directory">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_disable_wrapper" class="aioseop_wrapper aioseop_checkbox_type"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_disable_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Disable on this page/post</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="checkbox" name="aiosp_disable">
</div><div id="aiosp_disable_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Disable SEO on this page.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div><div id="aiosp_disable_analytics_wrapper" class="aioseop_wrapper aioseop_checkbox_type" style="display: none;"><div class="aioseop_input"><span style="text-align:right;vertical-align:top;" class="aioseop_option_label"><a onclick="toggleVisibility('aiosp_disable_analytics_tip');" title="Click for Help!" style="cursor:pointer;" class="aioseop_help_text_link"><label class="aioseop_label textinput">Disable Google Analytics</label></a></span><span class="aioseop_option_input"><div class="aioseop_option_div"><input type="checkbox" name="aiosp_disable_analytics">
</div><div id="aiosp_disable_analytics_tip" style="display:none" class="aioseop_help_text_div"><label class="aioseop_help_text">Disable Google Analytics on this page.<br><a target="_blank" href="http://semperplugins.com/sections/postpage-settings/">Click here for documentation on this setting</a></label></div></span><p style="clear:left"></p></div></div></div></div>
</div>
</div></div>
</div><!-- /post-body -->
<br class="clear">
</div><!-- /poststuff -->
</form>

<?php

	/* ================================================================== */
	/* End of Loop */
	/* ================================================================== */
	endwhile;
	?>

    <div>Content</div>
    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <!-- <?php //get_sidebar(); ?>-->
    <!-- End Widgets -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->

<?php get_footer(); ?>