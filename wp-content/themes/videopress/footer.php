	
	</div> <!-- End innermain-container -->
	<!-- Footer -->
    <div class="footer-container <?php if( vp_option( 'vpt_option.enable_footer_widget' ) != '1'){ echo 'nopadding'; } ?>">
    <div class="container p_90">
	
    
    <?php get_sidebar('footer'); ?>
    
    <!-- Footer Copyright -->
    <div class="full-width" id="copyright">
        <div id="copyright-text"><?php echo date('Y') . ' ' . vp_option('vpt_option.copyright'); ?></div> 
        <div class="footermiddle"><a href="<?php echo home_url( '/privacy-policy' ); ?>">Privacy Policy</a>&nbsp;&nbsp;<a href="<?php echo home_url( '/contact-us' ); ?>">Contact Us</a>
		&nbsp;&nbsp;<a href="<?php echo home_url( '/terms-of-service' ); ?>">Terms of Service</a>
		</div>
        <?php if( vp_option('vpt_option.logo_small') != '' ){ ?>
        <div id="footer-logo">
        	<a href="<?php echo home_url( '/' ); ?>">
            <img src="<?php echo vp_option('vpt_option.logo_small'); ?>" alt="small logo" />
            </a>
        </div>
		<?php } ?>
        
    <div class="clear"></div>
    </div>
    <!-- End Footer Copyright -->
    
    <div class="clear"></div>
    </div>
    </div>
<!--- // veena code --->
<?php if ( is_user_logged_in() ) { ?>
<div class="main_chatter_box" style="display: none;"><?php echo do_shortcode('[chatroom]'); ?></div>
<div class="mainChatBoxArea"><span class="textChatBox" >Chat<span class="countunredm"></span> </span><span class="close on" title="Open Chat"><i class="fa fa-plus"></i></span><span class="close off" title="Close Chat" style="display:none;" ><i class="fa fa-minus"></i></span>
<div id="cr_private_chat_totalunread">click</div>
</div>
<?php $_ajaxsss = admin_url('admin-ajax.php'); ?>
<script>
jQuery(document).ready(function($){
	$('.wcWindowTitle').html('');
	$('#cr_private_chat_totalunread').click();
	$('.mainChatBoxArea .close.on').live('click',function(){
		$('.close.on').hide();
		$('.close.off').show();
		$('.textChatBox').hide();
		$('.main_chatter_box').show();
		$height = $(window).height()/2 + 70;
        $('.chatroomBody').height($height);
		$('.RefreshMembersList[data-event=cr_refresh_friends]').click();
		
	});
	$('.mainChatBoxArea .close.off').live('click',function(){		
		$('.close.on').show();
		$('.close.off').hide();
		$('.textChatBox').show();
		$('.main_chatter_box').hide();
		$('#cr_private_chat_totalunread').click();
	});
	
	
	
	$('#cr_private_chat_totalunread').live('click',function(){
		// console.log();
		$.ajax({
							url: "<?php echo $_ajaxsss; ?>",						
							type: "POST",
							data: { 
								action: "cr_totalunred_message_currentuser_footer",
							 },
							success: function(data) { 
							
									$('.countunredm').html(data);
							
							}
						});
     
     });
});
</script>
<?php 
     //$paramaeewa = $_GET['param'];
	  
	  if (isset($_POST["group-hname"]) && !empty($_POST["group-hname"])) 
	  {
		  $paramaeewa = $_POST["group-hname"];
		  $displayhh = "display:block";
	  }


?>
<style>
#cr_private_chat_totalunread{display:none;}
.countuntotal{
	background-color: #FFAF34;
    border-radius: 50%;
    padding: 1px 4px 1px 5px;
    color: #000;
    font-size: 12px;
}
.mainChatBoxArea {
	background-color: #dddee0;
    bottom: 0;
    color: #666;
    padding: 4px 6px;
    position: fixed;
	right: 0;
	display: inline-block;
    z-index: 999999;
}
.mainChatBoxArea .textChatBox{
	width: 170px;
	display: inline-block;
}

.wcWindowTitle{
	bottom: 0!important;
    display: none!important;
	/* display: inline!important; */
    margin: 0!important;
    padding: 0!important;
    width: 0!important;
}

.wcUnreadMessagesFlag{
	/* color: #fff; */
}
	<?php for($i=0;$i<50;$i++){ ?>
	.wcMessages.wcMessages<?php echo $i; ?>,
	.wcControls.wcControls<?php echo $i; ?>{
		display:none!important;
	}
	<?php } ?>
 	
.wcMessagesContainerTab.wcChannelTab,
.wcPmNavigationButton.wcLeftButton,
.wcPmNavigationButton.wcRightButton{
	display:none!important;
}
.main_chatter_box {
    bottom: 0;
    position: fixed;
    right: 0;
    width: 100%;
    max-width: 467px;
	border: 1px solid #dddee0;
}
.wcMessagesContainerTab.wcChannelTab{
	display:none!important;
}
.wcContainer .wcMessagesContainersTabs{
	padding: 0;	
}
.wcWidth500 .wcMessagesContainersTabs > .wcMessagesContainerTabActive > a{
	padding: 0;	
}
</style>

<?php } ?>
<style>
.elite_vp_videoPlayerAD{ background: transparent !important; }
</style>
<script>
jQuery(document).ready(function($){
	$(window).resize(function() {
        var bodyheight = $(this).height()/2;
          $('.wpmci-popup-cnt-inr-wrp').css("min-height", bodyheight + "px" );
        
    }).resize();
});
</script>
<!--- // veena code end --->
    <!-- End Footer -->
	<!--</div> End fullmain-container -->
	<!-- Modal -->
<div class="modal fade" id="model_flag2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="furl" >
					<input type="hidden" name="url" id="ftitle" >
					<input type="hidden" name="url" id="aurl" >
					<textarea rows="10" id="reason" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit2" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>


<?php wp_footer(); ?>
<script>
	function slid(){ 
		jQuery('.homepage-video-holder').slick('unslick');
			jQuery('.homepage-video-holder').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: true,
			lazyLoad: 'progressive',
            responsive: [
                {
                    breakpoint: 1112,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 650,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
		}
</script>

<?php
if( is_user_logged_in() ){

$profile_id = get_current_user_id();
$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id  AND type =1 ORDER BY id DESC");
$data_file = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =2 ORDER BY id DESC");
$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =3 ORDER BY id DESC");

if($data){
?>

<div class="modal fade" id="playlistModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" 
               data-dismiss="modal" aria-hidden="false">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">Add video to playlist</h4>
         </div>
         <div class="modal-body">
			<!--<form method="post" id="form-add-playlist">
				<label>SELECT Playlist to add Video</label>
				<select name="plval">
				<?php foreach($data as $d): ?>
					<option value="<?= $d->id; ?>"><?= $d->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songid">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>-->
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">Close
            </button>
         </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php } } ?>

<div class="modal fade" id="videostest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add video to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist">
				<label>SELECT Playlist to add Video</label>
				<select name="plval">
				<?php foreach($data as $d): ?>
					<option value="<?= $d->id; ?>"><?= $d->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songid">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist').serialize(),
			success:function (data){	
				jQuery('#videostest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#videostest').click(function(e){
		jQuery('#videostest_mod').modal();
	});
</script> 


<!-- file model -->
<div class="modal fade" id="filetest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Files to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file">
				<label>SELECT Playlist to add Files</label>
				<select name="plval">
				<?php foreach($data_file as $datafile): ?>
					<option value="<?= $datafile->id; ?>"><?= $datafile->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-file').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest').click(function(e){
		jQuery('#filetest_mod').modal();
	});
</script> 


<!-- End file model -->
<!-- book model -->
<div class="modal fade" id="booktest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Books to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-book">
				<label>SELECT Playlist to add Books</label>
				<select name="plval">
				<?php foreach($data_book as $databook): ?>
					<option value="<?= $databook->id; ?>"><?= $databook->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songidss">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>


<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-book').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-book').serialize(),
			success:function (data){	
				jQuery('#booktest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#bookplaytest').click(function(e){
		jQuery('#booktest_mod').modal();
	});
	$('.widget-user-profile .rights .widget-profile-controls').hide();
	/* jQuery(document).ready(function($){
            jQuery('.wcUsersList').attr("style","height:300px");  
    }); */

</script> 
<!--<img src='<?php echo home_url(); ?>/DYNMockUp04.jpg' />-->
<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
</body>
</html>