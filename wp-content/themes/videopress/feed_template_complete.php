<?php 
/* Template Name: Feed Template complete */
get_header(); 

$bag = get_user_meta( get_current_user_id(), 'background_image_feed', true );
//$feedbgimg = home_url() .'/'. $bag;
$feedbgimg = home_url() .'/'. str_replace('%2F', '/',rawurlencode($bag));
?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/elitevideosource/css/elite.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/elitevideosource/css/elite-font-awesome.css" type="text/css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/elitevideosource/css/jquery.mCustomScrollbar.css" type="text/css">
         <link rel="stylesheet" href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css?ver=2.5.5" type="text/css">
       
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/froogaloop.js" type="text/javascript"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/jquery.mCustomScrollbar.js" type="text/javascript"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/THREEx.FullScreen.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/videoPlayer.js" type="text/javascript"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/Playlist.js" type="text/javascript"></script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/ZeroClipboard.js"></script>
<script type='text/javascript' src='//www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6'></script>			

			<script type='text/javascript' src='http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/assets/js/turn.min.js?ver=4.2.4'></script>
<script type='text/javascript' src='http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/assets/js/flipbook.min.js?ver=4.2.4'></script>

<style>
.fullmain-container{background-color: #e9ebee;background-image:url(<?php echo $feedbgimg;?>);}
.grid-3-4-sidebar.single-page {
    background: #fff;
    border-radius: 2px;
}
 #option-social a.opt-facebook {
    background: #3e5b98;
}
#option-social a.opt-twitter {
    background: #4da7de;
}
#option-social a.opt-googleplus {
    background: #dd4b39;
}
#option-social a.opt-linkedin {
    background: #3371b7;
}
#option-social a.opt-dig {
    background: #1d1d1b;
}
 #option-social a {
    padding: 5px;
    padding-right: 10px;
    padding-left: 10px;
    padding-bottom: 10px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    color: #FFF;
    font-size: 18px;
    text-transform: capitalize;
    margin: 0 10px 10px 0px;
    font-weight: 200;
    text-decoration: none !important;
}
.grid-3-4-sidebar {
    width: 76%;
    float: left;
}
.video-heading.btn-primary {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
    
    left: -46%;
    z-index: unset;
    position: relative;
}

button.button, input.button {
    font-size: 100%;
    margin: 0;
    line-height: 1;
    cursor: pointer;
    position: relative;
    font-family: inherit;
    text-decoration: none;
    overflow: visible;
    padding: .618em 1em;
    font-weight: 700;
    border-radius: 3px;
    left: auto;
    color: #515151;
    background-color: #ebe9eb;
    border: 0;
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
}
 button.button.alt, input.button.alt {
    background-color: #a46497;
    color: #fff;
    -webkit-font-smoothing: antialiased;
}

.form-add .single_add_to_cart_button.button.alt {
    border-radius: 4px;
    display: inline-block;
    line-height: 19px;
    text-align: center;
    height: 34px !important;
    float: left;
}
.quantity{  float: right;}
form.cart {
    margin-bottom: 0%;
}
#product-feed div.thumbnails a {
    float: left;
       width: auto;
    margin-right: 1px;
       margin: 0 auto;
}
#product-feed div.thumbnails a img{
	  width: 100px;
    height: 100px;
}
.woocommerce-main-image img{    max-height: 300px;}
#product-feed span.onsale {
    min-height: 2.236em;
    min-width: 6.236em;
    border-radius: 0px;
    font-weight: bold;
}
#product-feed span.onsale {
    min-height: 3.236em;
    min-width: 3.236em;
    padding: .202em;
    font-weight: 700;
    position: absolute;
    text-align: center;
    line-height: 3.236;
    top: -.5em;
    left: -.5em;
    margin: 0;
    border-radius: 100%;
    background-color: #77a464;
    color: #fff;
    font-size: .857em;
    -webkit-font-smoothing: antialiased;
}
.onsale {
    left: 0px !important;
    top: 0em !important;
    position: relative !important;
    padding: 9px!important;
    padding-left: 25px !important;
    padding-right: 25px !important;
} 
.price del {
    opacity: .5;
}
.loading-bar{
	    width: 250px;
    margin: 0 auto;
}
</style>
	<style>
				.elite_vp_logo{margin-top:6px;}
				.fa-elite-info,.elite_vp_infoBtn,.elite_vp_shareBtn {display:none}
				.fa-elite-share-square-o{display:none}

				#oio-banner-1{
					height:140px;
				}
				.oio-banner-zone .oio-slot a, .oio-banner-zone .oio-slot a:hover, .oio-banner-zone .oio-slot img, .oio-banner-zone .oio-slot object {
					position: absolute !important;
					top: 0 !important;
					left: 0 !important;
					height: 150px !important;
				}

			</style>

    <div class="container" >
		
<?php
/*
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

    $bkstart = ($paged==1) ? 0 : intval($paged-1) * 12;
	
if(!is_user_logged_in()){

	//$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
	wp_redirect(home_url());
}
else if(is_user_logged_in()){
	
	$user_id = get_current_user_id();
	
	$friends_subscribers_qry = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 UNION SELECT c.author_id from '.$wpdb->prefix.'authors_suscribers c WHERE c.suscriber_id = '.$user_id;
	
	
	$friends_subscribers_ids = $wpdb->get_results( $friends_subscribers_qry );
	
	$fs_ids = '';
	
	$count = 0;
	foreach($friends_subscribers_ids as $key=>$value){
		
		if($count == 0)
			$fs_ids .= $value->id;
		else
			$fs_ids .= ','.$value->id;
		
		$count++;	
	}

	if($fs_ids == ''){
		
		//$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
		$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' AND post_author='$user_id' AND ID NOT IN (SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='privacy-option' AND meta_value='private') ORDER BY post_date DESC LIMIT 50";

	}else{
		
		//~ $btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date LIMIT 50";
		
		$btquerydafd = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('post','dyn_file','dyn_book','product') AND post_author IN ($fs_ids,$user_id) AND post_status = 'publish' ORDER BY lastviewfeed DESC,post_date DESC ";
		$btquery = $btquerydafd.' LIMIT  ' . $bkstart .', 50';
	}
	
	
}

$bresult = $wpdb->get_results( $btquery );
$bktotal=$wpdb->query($btquerydafd);*/
?>
<script>
var $ = jQuery.noConflict();
 var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
$(document).ready(function() {
var $ = jQuery.noConflict();
	$('#morepost').scrollPagination({

		nop     : 5, // The number of posts per scroll to be loaded
		offset  : 10, // Initial offset, begins at 0 in this case
		error   : 'No More Posts!', // When the user reaches the end this is the message that is
		                            // displayed. You can change this if you want.
		delay   : 10, // When you scroll down the posts will load after a delayed amount of time.
		               // This is mainly for usability concerns. You can alter this as you see fit
		scroll  : true // The main bit, if set to false posts will not load as the user scrolls. 
		               // but will still load if the user clicks.
		
	});
	var $ = jQuery.noConflict();
});
var $ = jQuery.noConflict();
</script>
<script>
//$(document).scrollTop();

	/*
 $(window).scroll(function() { 
   var loadmorefeed = 2;
   if($(document).scrollTop() + $(window).height() > $(document).height()) {
	  
      $.ajax({
							url: "<?php echo admin_url('admin-ajax.php'); ?>",
							type: "POST",
							data: {action : 'feed_page_more_post_loading_feed',loadmorefeed : loadmorefeed},
							success : function(response){
								loadmorefeed = loadmorefeed+2;
								 $.ajax({
							url: "<?php echo admin_url('admin-ajax.php'); ?>",
							type: "POST",
							data: {action : 'feedallcontent_feed',loadmorefeed : loadmorefeed},
							success : function(response){
								alert(response);
								$('#morepost').append(response);
							}
						    })
								
								
								//alert(response);
								//$('.container').append(response);
							}
						})
   }
   
});


*/
var $ = jQuery.noConflict();
(function($) {

	$.fn.scrollPagination = function(options) {
		
		var settings = { 
			nop     : 5, // The number of posts per scroll to be loaded
			offset  : 0, // Initial offset, begins at 0 in this case
			error   : 'No More Posts!', // When the user reaches the end this is the message that is
			                            // displayed. You can change this if you want.
			delay   : 500, // When you scroll down the posts will load after a delayed amount of time.
			               // This is mainly for usability concerns. You can alter this as you see fit
			scroll  : true // The main bit, if set to false posts will not load as the user scrolls. 
			               // but will still load if the user clicks.
		}
		
		// Extend the options so they work with the plugin
		if(options) {
			$.extend(settings, options);
		}
		
		// For each so that we keep chainability.
		return this.each(function() {		
			
			// Some variables 
			$this = $(this);
			$settings = settings;
			var offset = $settings.offset;
			var busy = false; // Checks if the scroll action is happening 
			                  // so we don't run it multiple times
			
			// Custom messages based on settings
			if($settings.scroll == true) $initmessage = 'Scroll for more or click here';
			else $initmessage = 'Click for more';
			
			// Append custom messages and extra UI
			$this.append('<div class="loadmoreposload"></div><div class="loading-bar">'+$initmessage+'</div>');
			
			function getData() {
				
				// Post data to ajax.php
				$.post(ajaxurl, {
						
					action        : 'feedallcontent_feed_callback',
				    number        : $settings.nop,
				    offset        : offset,
					trys          :2,
					
					    
				}, function(data) {
					 console.log (data);
						//alert(data);
					// Change loading bar content (it may have been altered)
					$this.find('.loading-bar').html($initmessage);
					//	$.globalEval(data);
					// If there is no data returned, there are no more posts to be shown. Show error
					if(data == "") { 
						$this.find('.loading-bar').html($settings.error);	
					}
					else {
						
						// Offset increases
					    offset = offset+$settings.nop; 
						    
						// Append the data to the content div
					   	$this.find('.loadmoreposload').append(data);
					$('.loadmoreposload').find("script").each(function(i){
							eval($(this).text());
							
						});
						
						// No longer busy!	
						busy = false;
					}	
						
				});
					
			}	
			
			//getData(); // Run function initially
			
			// If scrolling is enabled
			if($settings.scroll == true) {
				// .. and the user is scrolling
				$(window).scroll(function() {
					
					// Check the user is at the bottom of the element
					if($(window).scrollTop() + $(window).height() > $this.height() && !busy) {
						
						// Now we are working, so busy is true
						busy = true;
						
						// Tell the user we're loading posts
						$this.find('.loading-bar').html('<img src="<?php echo get_template_directory_uri(); ?>/loading_processmaker.gif">');
						
						// Run the function to fetch the data inside a delay
						// This is useful if you have content in a footer you
						// want the user to see.
						setTimeout(function() {
							
							getData();
							
						}, $settings.delay);
							
					}	
				});
			}
			
			// Also content can be loaded by clicking the loading bar/
			$this.find('.loading-bar').click(function() {
			
				if(busy == false) {
					busy = true;
					getData();
				}
			
			});
			
		});
	}

})(jQuery);

var $ = jQuery.noConflict();

</script>
 <?php echo feedallcontent(); ?>
 <div id="morepost"></div>
</div></div>
	<?php
$big = 999999999; // need an unlikely integer
 
echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => ($bktotal/150)
) );
?>
<?php get_footer(); ?>