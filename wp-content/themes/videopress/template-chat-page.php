<?php /* Template Name: Chat Page */ ?>
<?php get_header(); ?>

    <!-- Start Content -->
    <div class="container">

    <?php get_template_part('includes/featured-playlist'); ?>

    <!-- Start Entries -->
    <div class="entry">

	<?php if ( is_active_sidebar( 'sidebar-chat' )  ) : ?>
			<?php dynamic_sidebar( 'sidebar-chat' ); ?>
	<?php endif; ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <!-- Content -->
    <?php
    // Start The Loop
    while (have_posts()) : the_post();
		the_content();
    endwhile;
	// End Of Loop
	?>




    <!-- End Content -->
    <div class="clear"></div>



    </div>
    </div>
    <!-- End Entries -->

    <div class="clear"></div>
    </div>
    <div class="spacing-20"></div>
    <!-- End Content -->

<?php get_footer(); ?>