<?php

/**
 * bbPress User Profile Edit Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<?php
  $user_id = get_current_user_id();
  //subscriber
  $endorse_feed = get_user_meta( $user_id, '_endorse_feed', true); 
  if($endorse_feed == 1 || $endorse_feed == '')
  {
	$endorse_feed_check = "checked";  
  }
  
  $favorite_feed = get_user_meta( $user_id, '_favorite_feed', true); 
   if($favorite_feed == 1 || $favorite_feed == '')
  {
	$favorite_feed_check = "checked";  
  }
  
  $donate_feed = get_user_meta( $user_id, '_donate_feed', true); 
   if($donate_feed == 1 || $donate_feed == '')
  {
	$donate_feed_check = "checked";  
  }
  $flag_feed = get_user_meta( $user_id, '_flag_feed', true); 
   if($flag_feed == 1 || $flag_feed == '')
  {
	$falg_feed_check = "checked";  
  }
  $review_feed = get_user_meta( $user_id, '_review_feed', true); 
   if($review_feed == 1 || $review_feed== '')
  {
	$review_feed_check = "checked";  
  }
  
  $upload_feed = get_user_meta( $user_id, '_upload_feed', true); 
   if($upload_feed == 1 || $upload_feed == '')
  {
	$upload_feed_check = "checked";  
  }
  
  $comment_feed = get_user_meta( $user_id, '_comment_feed', true); 
   if($comment_feed == 1 || $comment_feed== '')
  {
	$comment_feed_check= "checked";  
  }
  
  $viewnumber_feed = get_user_meta( $user_id, '_viewnumber_feed', true); 
   if($viewnumber_feed == 1 || $viewnumber_feed== '')
  {
	$viewnumber_feed_check= "checked";  
  }
  $downloadcount_feed = get_user_meta( $user_id, '_downloadcount_feed', true); 
   if($downloadcount_feed == 1 || $downloadcount_feed== '')
  {
	$downloadcount_feed_check= "checked";  
  }
  
  $posttime_feed = get_user_meta( $user_id, '_posttime_feed', true); 
   if($posttime_feed == 1 || $posttime_feed== '')
  {
	$posttime_feed_check= "checked";  
  }
  $taglist_feed = get_user_meta( $user_id, '_taglist_feed', true); 
   if($taglist_feed == 1 || $taglist_feed== '')
  {
	$taglist_feed_check= "checked";  
  }
  $numberofsale_feed = get_user_meta( $user_id, '_numberofsale_feed', true); 
   if($numberofsale_feed == 1 || $numberofsale_feed== '')
  {
	$numberofsale_feed_check= "checked";  
  }
  
    $product_feed = get_user_meta( $user_id, '_add_play_list_feed', true); 
   if($product_feed == 1 || $product_feed== '')
  {
	$add_play_list_feed= "checked";  
  }
  
  $edited_feed = get_user_meta( $user_id, '_edited_feed', true); 
   if($edited_feed == 1 || $edited_feed== '')
  {
	$edited_feed= "checked";  
  }
  
  
  // friends
  
   $endorsef_feed = get_user_meta( $user_id, '_endorsef_feed', true); 
  if($endorsef_feed == 1 || $endorsef_feed == '')
  {
	$endorsef_feed_check = "checked";  
  }
  
  $favoritef_feed = get_user_meta( $user_id, '_favoritef_feed', true); 
   if($favoritef_feed == 1 || $favoritef_feed == '')
  {
	$favoritef_feed_check = "checked";  
  }
  
  $donatef_feed = get_user_meta( $user_id, '_donatef_feed', true); 
   if($donatef_feed == 1 || $donatef_feed == '')
  {
	$donatef_feed_check = "checked";  
  }
  $flagf_feed = get_user_meta( $user_id, '_flagf_feed', true); 
   if($flagf_feed == 1 || $flagf_feed == '')
  {
	$falgf_feed_check = "checked";  
  }
  $reviewf_feed = get_user_meta( $user_id, '_reviewf_feed', true); 
   if($reviewf_feed == 1 || $reviewf_feed== '')
  {
	$reviewf_feed_check = "checked";  
  }
  
  $uploadf_feed = get_user_meta( $user_id, '_uploadf_feed', true); 
   if($uploadf_feed == 1 || $uploadf_feed == '')
  {
	$uploadf_feed_check = "checked";  
  }
  
  $commentf_feed = get_user_meta( $user_id, '_commentf_feed', true); 
   if($commentf_feed == 1 || $commentf_feed== '')
  {
	$commentf_feed_check= "checked";  
  }
  
  $viewnumberf_feed = get_user_meta( $user_id, '_viewnumberf_feed', true); 
   if($viewnumberf_feed == 1 || $viewnumberf_feed== '')
  {
	$viewnumberf_feed_check= "checked";  
  }
  $downloadcountf_feed = get_user_meta( $user_id, '_downloadcountf_feed', true); 
   if($downloadcountf_feed == 1 || $downloadcountf_feed== '')
  {
	$downloadcountf_feed_check= "checked";  
  }
  
  $posttimef_feed = get_user_meta( $user_id, '_posttimef_feed', true); 
   if($posttimef_feed == 1 || $posttimef_feed== '')
  {
	$posttimef_feed_check= "checked";  
  }
  $taglistf_feed = get_user_meta( $user_id, '_taglistf_feed', true); 
   if($taglistf_feed == 1 || $taglistf_feed== '')
  {
	$taglistf_feed_check= "checked";  
  }
  $numberofsalef_feed = get_user_meta( $user_id, '_numberofsalef_feed', true); 
   if($numberofsalef_feed == 1 || $numberofsalef_feed== '')
  {
	$numberofsalef_feed_check= "checked";  
  }
  
   $productf_feed = get_user_meta( $user_id, '_add_play_listf_feed', true); 
   if($productf_feed == 1 || $productf_feed== '')
  {
	$add_play_listf_feed= "checked";  
  }
  
  $editedf_feed = get_user_meta( $user_id, '_editedf_feed', true); 
   if($editedf_feed == 1 || $editedf_feed== '')
  {
	$editedf_feed= "checked";  
  }
?>
<form id="bbp-your-profile" action="<?php bbp_user_profile_edit_url( bbp_get_displayed_user_id() ); ?>" method="post" enctype="multipart/form-data">

	<h2 class="entry-title"><?php _e( 'Name', 'bbpress' ) ?></h2>

	<?php do_action( 'bbp_user_edit_before' ); ?>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Name', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_name' ); ?>

		<div>
			<label for="first_name"><?php _e( 'First Name', 'bbpress' ) ?></label>
			<input type="text" name="first_name" id="first_name" value="<?php bbp_displayed_user_field( 'first_name', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="last_name"><?php _e( 'Last Name', 'bbpress' ) ?></label>
			<input type="text" name="last_name" id="last_name" value="<?php bbp_displayed_user_field( 'last_name', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="nickname"><?php _e( 'Nickname', 'bbpress' ); ?></label>
			<input type="text" name="nickname" id="nickname" value="<?php bbp_displayed_user_field( 'nickname', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="display_name"><?php _e( 'Display Name', 'bbpress' ) ?></label>

			<?php bbp_edit_user_display_name(); ?>

		</div>

		<?php do_action( 'bbp_user_edit_after_name' ); ?>

	</fieldset>

	<h2 class="entry-title"><?php _e( 'Contact Info', 'bbpress' ) ?></h2>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Contact Info', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_contact' ); ?>

		<div>
			<label for="url"><?php _e( 'Website', 'bbpress' ) ?></label>
			<input type="text" name="url" id="url" value="<?php bbp_displayed_user_field( 'user_url', 'edit' ); ?>" class="regular-text code" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<?php foreach ( bbp_edit_user_contact_methods() as $name => $desc ) : ?>

			<div>
				<label for="<?php echo esc_attr( $name ); ?>"><?php echo apply_filters( 'user_' . $name . '_label', $desc ); ?></label>
				<input type="text" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $name ); ?>" value="<?php bbp_displayed_user_field( $name, 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
			</div>

		<?php endforeach; ?>

		<?php do_action( 'bbp_user_edit_after_contact' ); ?>

	</fieldset>

	<h2 class="entry-title"><?php bbp_is_user_home_edit() ? _e( 'About Yourself', 'bbpress' ) : _e( 'About the user', 'bbpress' ); ?></h2>

	<fieldset class="bbp-form">
		<legend><?php bbp_is_user_home_edit() ? _e( 'About Yourself', 'bbpress' ) : _e( 'About the user', 'bbpress' ); ?></legend>

		<?php do_action( 'bbp_user_edit_before_about' ); ?>

		<div>
			<label for="description"><?php _e( 'Biographical Info', 'bbpress' ); ?></label>
			<textarea name="description" id="description" rows="5" cols="30" tabindex="<?php bbp_tab_index(); ?>"><?php bbp_displayed_user_field( 'description', 'edit' ); ?></textarea>
		</div>

		<?php do_action( 'bbp_user_edit_after_about' ); ?>

	</fieldset>

	<h2 class="entry-title"><?php _e( 'Account', 'bbpress' ) ?></h2>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Account', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_account' ); ?>

		<div>
			<label for="user_login"><?php _e( 'Username', 'bbpress' ); ?></label>
			<input type="text" name="user_login" id="user_login" value="<?php bbp_displayed_user_field( 'user_login', 'edit' ); ?>" disabled="disabled" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="email"><?php _e( 'Email', 'bbpress' ); ?></label>

			<input type="text" name="email" id="email" value="<?php bbp_displayed_user_field( 'user_email', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />

			<?php

			// Handle address change requests
			$new_email = get_option( bbp_get_displayed_user_id() . '_new_email' );
			if ( !empty( $new_email ) && $new_email !== bbp_get_displayed_user_field( 'user_email', 'edit' ) ) : ?>

				<span class="updated inline">

					<?php printf( __( 'There is a pending email address change to <code>%1$s</code>. <a href="%2$s">Cancel</a>', 'bbpress' ), $new_email['newemail'], esc_url( self_admin_url( 'user.php?dismiss=' . bbp_get_current_user_id()  . '_new_email' ) ) ); ?>

				</span>

			<?php endif; ?>

		</div>

		<div id="password">
			<label for="pass1"><?php _e( 'New Password', 'bbpress' ); ?></label>
			<fieldset class="bbp-form password">
				<input type="password" name="pass1" id="pass1" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" />
				<span class="description"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.', 'bbpress' ); ?></span>

				<input type="password" name="pass2" id="pass2" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" />
				<span class="description"><?php _e( 'Type your new password again.', 'bbpress' ); ?></span><br />

				<div id="pass-strength-result"></div>
				<span class="description indicator-hint"><?php _e( 'Your password should be at least ten characters long. Use upper and lower case letters, numbers, and symbols to make it even stronger.', 'bbpress' ); ?></span>
			</fieldset>
		</div>

		<?php do_action( 'bbp_user_edit_after_account' ); ?>

	</fieldset>

	<?php if ( current_user_can( 'edit_users' ) && ! bbp_is_user_home_edit() ) : ?>

		<h2 class="entry-title"><?php _e( 'User Role', 'bbpress' ) ?></h2>

		<fieldset class="bbp-form">
			<legend><?php _e( 'User Role', 'bbpress' ); ?></legend>

			<?php do_action( 'bbp_user_edit_before_role' ); ?>

			<?php if ( is_multisite() && is_super_admin() && current_user_can( 'manage_network_options' ) ) : ?>

				<div>
					<label for="super_admin"><?php _e( 'Network Role', 'bbpress' ); ?></label>
					<label>
						<input class="checkbox" type="checkbox" id="super_admin" name="super_admin"<?php checked( is_super_admin( bbp_get_displayed_user_id() ) ); ?> tabindex="<?php bbp_tab_index(); ?>" />
						<?php _e( 'Grant this user super admin privileges for the Network.', 'bbpress' ); ?>
					</label>
				</div>

			<?php endif; ?>

			<?php bbp_get_template_part( 'form', 'user-roles' );
			?>

			<?php do_action( 'bbp_user_edit_after_role' ); ?>

		</fieldset>

	<?php endif; ?>

	<?php do_action( 'bbp_user_edit_after' ); ?>

	<fieldset class="submit">
		<legend><?php _e( 'Save Changes', 'bbpress' ); ?></legend>
		<div>

			<?php bbp_edit_user_form_fields(); ?>

			<button type="submit" tabindex="<?php bbp_tab_index(); ?>" id="bbp_user_edit_submit" name="bbp_user_edit_submit" class="button submit user-submit"><?php bbp_is_user_home_edit() ? _e( 'Update Profile', 'bbpress' ) : _e( 'Update User', 'bbpress' ); ?></button>
		</div>
	</fieldset>

</form>

<form id="bbp-your-profile-feed" style="display:none" method="post" action="">

<div class="alert alert-success v-d" style="display:none">
		<div id="status" style="display:none"> <strong > Successfully Update Feed For Subscriptions and Friends</strong></div>
	</div>
<style>
    .checkboxfeed{float: left;
    margin: 5px;}
	#bbp-your-profile-feed fieldset.bbp-form input {
        margin: 11px;
}
#bbp-your-profile-feed label{display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: normal;
    font-size: 1.4em;

    line-height: 32px;}
	#bbp-your-profile-feed h2.entry-title {
    font-size: 1.4em;
    margin: 0;
    padding-bottom: 10px;
    padding-top: 0;
    clear: none;
    font-weight: 700;
}
#feeds_submit{
	    float: right;
    background-color: #23527c;
    color: #fff;
    padding: 7px;
    border: 3px;
    border-radius: 3px;
}
#cupp_container{display:none;}
</style>
	<h2 class="entry-title"><?php _e( 'Subscriptions', 'bbpress' ) ?></h2>
	<fieldset class="bbp-form">
                 <div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="add_play_list_feed" name="add_play_list_feed" <?php echo $add_play_list_feed; ?> />
						<?php _e( 'Add Playlist Display on Feed. If Uncheck then Subscriptions do not view Add Playlist on feed.', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="edited_feed" name="edited_feed" <?php echo $edited_feed; ?> />
						<?php _e( 'Edit Display on Feed. If Uncheck then Subscriptions do not view Edit on feed.', 'bbpress' ); ?>
					</label>
				</div>
				 
				 
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="endorse_feed" name="endorse_feed" <?php echo $endorse_feed_check; ?> />
						<?php _e( 'Endorsed Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox" id="favorite_feed" name="favorite_feed" <?php echo $favorite_feed_check; ?>  />
						<?php _e( 'Favorited Display on Feed. Uncheck for hide from feed?', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox" id="donate_feed" name="donate_feed" <?php echo $donate_feed_check; ?>  />
						<?php _e( 'Donated Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="flag_feed" name="flag_feed" <?php echo $falg_feed_check; ?> />
						<?php _e( 'Flagged Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="review_feed" name="review_feed" <?php echo $review_feed_check; ?> />
						<?php _e( 'Reviewed Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="upload_feed" name="upload_feed" <?php echo $upload_feed_check; ?> />
						<?php _e( 'Uploaded display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="comment_feed" name="comment_feed" <?php echo $comment_feed_check; ?> />
						<?php _e( 'Commented on display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<!--<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="viewnumber_feed" name="viewnumber_feed" <?php echo $viewnumber_feed_check; ?> />
						<?php _e( 'Number of View display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>-->
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="downloadcount_feed" name="downloadcount_feed" <?php echo $downloadcount_feed_check; ?> />
						<?php _e( 'Download Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<!--<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="posttime_feed" name="posttime_feed" <?php echo $posttime_feed_check; ?> />
						<?php _e( 'Post date display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>-->
				
				<!--<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="taglist_feed" name="taglist_feed" <?php echo $taglist_feed_check; ?> />
						<?php _e( 'Tag List display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>-->
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="numberofsale_feed" name="numberofsale_feed" <?php echo $numberofsale_feed_check; ?> />
						<?php _e( 'Purchased display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
    </fieldset>	
<h2 class="entry-title"><?php _e( 'Friends', 'bbpress' ) ?></h2>
	<fieldset class="bbp-form">
                 <div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="add_play_listf_feed" name="add_play_listf_feed" <?php echo $add_play_listf_feed; ?> />
						<?php _e( 'Add Playlist Display on Feed. If Uncheck then Friends do not view Add Playlist on feed.', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="editedf_feed" name="editedf_feed" <?php echo $editedf_feed; ?> />
						<?php _e( 'Edit Display on Feed. If Uncheck then Friends do not view Edit on feed.', 'bbpress' ); ?>
					</label>
				</div>
				 
				 
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="endorsef_feed" name="endorsef_feed" <?php echo $endorsef_feed_check; ?> />
						<?php _e( 'Endorsed Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox" id="favoritef_feed" name="favoritef_feed" <?php echo $favoritef_feed_check; ?>  />
						<?php _e( 'Favorited Display on Feed. Uncheck for hide from feed?', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox" id="donatef_feed" name="donatef_feed" <?php echo $donatef_feed_check; ?>  />
						<?php _e( 'Donated Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="flagf_feed" name="flagf_feed" <?php echo $falgf_feed_check; ?> />
						<?php _e( 'Flagged Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="reviewf_feed" name="reviewf_feed" <?php echo $reviewf_feed_check; ?> />
						<?php _e( 'Reviewed Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="uploadf_feed" name="uploadf_feed" <?php echo $uploadf_feed_check; ?> />
						<?php _e( 'Uploaded display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="commentf_feed" name="commentf_feed" <?php echo $commentf_feed_check; ?> />
						<?php _e( 'Commented on display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<!--<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="viewnumberf_feed" name="viewnumberf_feed" <?php echo $viewnumberf_feed_check; ?> />
						<?php _e( 'Number of View display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>-->
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="downloadcountf_feed" name="downloadcountf_feed" <?php echo $downloadcountf_feed_check; ?> />
						<?php _e( 'Download Count Display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
				<!--<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="posttimef_feed" name="posttimef_feed" <?php echo $posttimef_feed_check; ?> />
						<?php _e( 'Post date display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>-->
				
				<!--<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="taglistf_feed" name="taglistf_feed" <?php echo $taglistf_feed_check; ?> />
						<?php _e( 'Tag List display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>-->
				<div>
					 
					<label>
						<input class="checkbox checkboxfeed" type="checkbox"  id="numberofsalef_feed" name="numberofsalef_feed" <?php echo $numberofsalef_feed_check; ?> />
						<?php _e( 'Purchased display on Feed. Uncheck for hide from feed', 'bbpress' ); ?>
					</label>
				</div>
    </fieldset>		
		<div>

		

			<button type="submit" id="feeds_submit" name="feeds_submit" class="button submit user-submit">Save Change</button>
		</div>
	
</form>
<form id="bbp-your-notable-feed" style="display:none" method="post" action="">
<style>
th,td{text-align:center;}
</style>
<h2>Notable Member</h2>
<table style="width:100%">
<thead>
<tr>
<th>Name</th>
<th>Title</th>
<th>Action</th>
</tr>
</thead>
<?php
$nepfData = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."notablemember WHERE member_id =".$user_id );
		
		    
		if(count($nepfData) > 0) {
			foreach($nepfData as $value)
			{
				echo '<tr class="notable'.$value->nata_id.'">';
				echo '<td>' .  $value->member_name .'</td>';
				echo '<td>' .  $value->member_title .'</td>';
				echo '<td><a href="#" class="del-notable" data-id="'. $value->nata_id . '">Delete</a></td>';
			
				echo '</tr>';
			}
		}
			?>
</table>			
</form>
 <script>
				jQuery(document).ready(function($) {
				//	e.preventDefault();
					$('.bbp-user-feeds-link').click(function(){
							
						$('#bbp-your-profile').remove();
						$('#bbp-your-profile-feed').show();
						$('#feedssubfriend').addClass('current');
						 
						$('#feedssubedit').removeClass('current');
					});
					  $("#bbp-your-profile-feed").on('submit',(function(e) {
							e.preventDefault();
						$('#bbp-your-profile').remove();
						$('#bbp-your-profile-feed').show();
						$('#feedssubfriend').addClass('current');
						//var endorse_feed = $('#endorse_feed').val(); 
						//subscriber
						var endorse_feed = $("#endorse_feed").is(':checked') ? 1 : 0;
						var favorite_feed = $("#favorite_feed").is(':checked') ? 1 : 0;
						var add_play_list_feed = $("#add_play_list_feed").is(':checked') ? 1 : 0;
						var edited_feed = $("#edited_feed").is(':checked') ? 1 : 0;
						//var book_feed = $("#book_feed").is(':checked') ? 1 : 0;
						//var product_feed = $("#product_feed").is(':checked') ? 1 : 0;
						var donate_feed = $("#donate_feed").is(':checked') ? 1 : 0;
						var review_feed = $("#review_feed").is(':checked') ? 1 : 0;
						var falg_feed = $("#flag_feed").is(':checked') ? 1 : 0;
						var upload_feed = $("#upload_feed").is(':checked') ? 1 : 0;
						var comment_feed = $("#comment_feed").is(':checked') ? 1 : 0;
						//var viewnumber_feed = $("#viewnumber_feed").is(':checked') ? 1 : 0;
						var downloadcount_feed = $("#downloadcount_feed").is(':checked') ? 1 : 0;
						//var posttime_feed = $("#posttime_feed").is(':checked') ? 1 : 0;
						//var taglist_feed = $("#taglist_feed").is(':checked') ? 1 : 0;
						var numberofsale_feed = $("#numberofsale_feed").is(':checked') ? 1 : 0;
						
						// friends
						
						var endorsef_feed = $("#endorsef_feed").is(':checked') ? 1 : 0;
						var favoritef_feed = $("#favoritef_feed").is(':checked') ? 1 : 0;
						var add_play_listf_feed = $("#add_play_listf_feed").is(':checked') ? 1 : 0;
						var editedf_feed = $("#editedf_feed").is(':checked') ? 1 : 0;
						//var bookf_feed = $("#bookf_feed").is(':checked') ? 1 : 0;
						//var productf_feed = $("#productf_feed").is(':checked') ? 1 : 0;
						var donatef_feed = $("#donatef_feed").is(':checked') ? 1 : 0;
						var reviewf_feed = $("#reviewf_feed").is(':checked') ? 1 : 0;
						var falgf_feed = $("#flagf_feed").is(':checked') ? 1 : 0;
						var uploadf_feed = $("#uploadf_feed").is(':checked') ? 1 : 0;
						var commentf_feed = $("#commentf_feed").is(':checked') ? 1 : 0;
						//var viewnumberf_feed = $("#viewnumberf_feed").is(':checked') ? 1 : 0;
						var downloadcountf_feed = $("#downloadcountf_feed").is(':checked') ? 1 : 0;
						//var posttimef_feed = $("#posttimef_feed").is(':checked') ? 1 : 0;
						//var taglistf_feed = $("#taglistf_feed").is(':checked') ? 1 : 0;
						var numberofsalef_feed = $("#numberofsalef_feed").is(':checked') ? 1 : 0;
						
					
						$('#feedssubedit').removeClass('current');
						//alert(endorse_feed);
						$.ajax({
							url: "<?php echo admin_url('admin-ajax.php'); ?>",
							type: "POST",
							data: {action : 'feedssubs_feed',endorse_feed : endorse_feed,
							favorite_feed: favorite_feed,
							add_play_list_feed: add_play_list_feed,
							edited_feed: edited_feed,
							donate_feed: donate_feed,
							review_feed: review_feed,
							falg_feed: falg_feed,
							upload_feed: upload_feed,
							comment_feed: comment_feed,
							downloadcount_feed: downloadcount_feed,
							numberofsale_feed: numberofsale_feed,
							endorsef_feed: endorsef_feed,
							favoritef_feed: favoritef_feed,
							editedf_feed: editedf_feed,
							add_play_listf_feed: add_play_listf_feed,
							donatef_feed: donatef_feed,
							reviewf_feed: reviewf_feed,
							falgf_feed: falgf_feed,
							uploadf_feed: uploadf_feed,
							commentf_feed: commentf_feed,
							downloadcountf_feed: downloadcountf_feed,
							numberofsalef_feed: numberofsalef_feed,
							},
							success : function(data){
								//$('.alert-success').show();
								$('.alert-success').show(1).delay(5000).hide(1);
				               //$('#status').append(data);
							    $("#status").show();
                                    setTimeout(function() { $("#status").hide(); }, 5000);
							   
							}
						});
					}));
					
					$('.bbp-user-notable-link').click(function(){
							
						$('#bbp-your-profile').remove();
						$('#bbp-your-notable-feed').show();
						$('#notabalemember').addClass('current');
						 
						$('#feedssubedit').removeClass('current');
					});
					$('.del-notable').click(function(e){
				e.preventDefault();

				console.log($(this).attr('data-id'));
				//var confrm = confirm("Are you sure you want to continue!");
				var notable_member = $(this).attr('data-id');
				var user_id = '<?= get_current_user_id;?>';
              //  alert(favorite_idsss);
				
					$.ajax({
						type: "POST",
						url: ajaxurl,
						data: { action:'user_notable_delete_channel','notable_member': notable_member,'user_id': user_id},
						success : function(data){
							$('.notable'+notable_member).remove();
							//$('.alert-success').attr('style','display:block');
							//location.reload();
							alert(data);
							
						}
					});
				   //}
			     });
					
					
				});
</script>					
