    	<div class="image-holder dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_ID()); ?>">
            <a href="<?php echo apply_filters('dyn_file_download_link', get_the_ID()); ?>">
                <div class="hover-item dyn-file">
                    <i class="fa fa-download"></i>
                </div>
            </a>

        </div>
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    <ul class="stats">
    <li><?php videopress_displayviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>