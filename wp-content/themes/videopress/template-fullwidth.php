<?php /* Template Name: Full Width */ ?>
<?php get_header(); ?>

    <!-- Start Content -->
    <div class="container">

    <?php get_template_part('includes/featured-playlist'); ?>
    
    <!-- Start Entries -->
    <div class="full-width entry">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <!-- Content -->
    <?php
    // Start The Loop
    while (have_posts()) : the_post();
		the_content();
    endwhile;
	// End Of Loop
	?>
    <!-- End Content -->
    <div class="clear"></div>
    
    <?php get_template_part('layouts/layout-builder'); ?>
    
    </div>
    </div>
    <!-- End Entries -->
    
    <div class="clear"></div>
    </div>
    <div class="spacing-20"></div>
    <!-- End Content -->
    
<?php get_footer(); ?>