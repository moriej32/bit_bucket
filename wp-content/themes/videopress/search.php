<?php get_header(); ?>
<?php session_start(); 
 
?>
    <!-- Start Content -->
 <div class="grid-3-4 centre-block" style="background:#f6f6f6 none repeat scroll 0 0">
	    <h1 class="page-title" style="padding: 20px;"><?php printf( __( 'Search Results for : %s', 'twentyfifteen' ), get_search_query() ); ?></h1>

    <div class="cover-twoblocks">
		<div>
		 <form method="post" id="sortvideo-form" action="<?php echo home_url( '/' ).'?s='.get_search_query(); ?>">
			<p>Currently Sorted By :
				<select class="sortvideo" name="sortvideo" onchange="sortpostcharacter()">
					<option value="recent" <?php if ($_REQUEST["sortvideo"] == 'recent') echo 'selected="selected"';?> >Recent</option>
					<option value="views" <?php if ($_REQUEST["sortvideo"] == 'views') echo 'selected="selected"';?> >Views</option>
					<option value="endorsements" <?php if ($_REQUEST["sortvideo"] == 'endorsements') echo 'selected="selected"';?> >Endorsements</option>
					<option value="favourites" <?php if ($_REQUEST["sortvideo"] == 'favourites') echo 'selected="selected"';?> >Favorites</option>
					<option value="comments" <?php if ($_REQUEST["sortvideo"] == 'comments') echo 'selected="selected"';?> >Comments</option>
					<option value="reviews" <?php if ($_REQUEST["sortvideo"] == 'reviews') echo 'selected="selected"';?> >Reviews</option>
					<option value="downloads" <?php if ($_REQUEST["sortvideo"] == 'downloads') echo 'selected="selected"';?> >Downloads</option>
				</select>
			</p>


			</form>
			<div class="sortsearchtype">
			<input name="video" class="chk" onclick="searchpagetypeselect()"  id="chvideo" type="checkbox" value="post" checked="checked" /> Video
			<input name="file" class="chk" onclick="searchpagetypeselect()" id="chfile" type="checkbox" value="dyn_file" checked="checked" />  File
			<input name="book" class="chk"   onclick="searchpagetypeselect()" id="chbook" type="checkbox" value="dyn_book" checked="checked" />   Book
			<input name="product" class="chk" onclick="searchpagetypeselect()"   id="chproduct" type="checkbox" value="product" checked="checked" />   Product
			<input type="hidden" id="nextsearch" value="0">
			<input type="hidden" id="currenttype" value="all">
			<input type="hidden" id="viewstyle" value="list">
			
			</div>

		</div>
			<div class="well well-sm" style="margin-top: -51px;">
				<strong>Views</strong>
				<div class="btn-group">
					<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="container p_90">	
		  <div class="layout-2 defaultheight">
              <div id="searchhtml"></div>
			  <div class="btn btn-info dyn_load_more_search">
			Load More
			</div>
		<div class="row">
 
 <div class="spacing-40"></div>
 
</div></div> </div> </div>
<script type="text/javascript">


$(document).ready(function (e) {

$(document).ready(function() {
    $(window).resize(function() {
        var bodyheight = $(this).height() -150;
          $('.defaultheight').css("min-height", bodyheight + "px" );
        
    }).resize();
}); 

});
 


</script>
    <script>
	  /*$('input.chk').on('change', function() {
              $('input.chk').not(this).prop('checked', false); 		  
        });*/
	   window.onload = function() {
		   var id = 'all';
        selectOnlyThis(id)
       };
	   
	  function searchpagetypeselect()
	   {
		   var idfs = [];
		   
		  // alert($('.chk:checked').serialize());
		    $(".chk:checked").each(function(){
            idfs.push(this.value);
        });
		    selectOnlyThis(idfs)
	   } 
	   
     function sortpostcharacter()
	 {
		 var idfs = [];
		  $("#overlayssss").attr('style','display:block;');
		 var sortype = $('.sortvideo').val();
		// alert(sortype);
		if(sortype == 'views')
		{
			sortype = 1;
		}
		else if(sortype == 'recent'){
			sortype = 2;
		}
		else if(sortype == 'endorsements'){
			sortype = 3;
		}
		else if(sortype == 'favourites'){
			sortype = 4;
		}
		else if(sortype == 'comments'){
			sortype = 5;
		}
		else if(sortype == 'reviews'){
			sortype = 6;
		}
		else if(sortype == 'downloads'){
			sortype = 7;
		}
		var searchIDs = $("#currenttype").val();
		
		 $(".chk:checked").each(function(){
                       idfs.push(this.value);
                    });
					
					var limit= 0;
					 var viewstyle = $('#viewstyle').val();
					 // alert(limit);
					  var searchvalue = '<?php echo $_GET['s']; ?>';
					  
						// alert(searchIDs);
						 
						 console.log();
						 $.ajax({
									url: '<?php echo admin_url('admin-ajax.php'); ?>',
									type: "POST",
                                    dataType: "json",
									data: {
										action: 'load_more_search_home',
									    'limitserach':limit,
										'searchvalue':searchvalue,
										'searchtype':idfs,
										'viewstyle':viewstyle,
										'charactersort':1,
										'viewshort':sortype
									},
									success : function(response){
								   // alert(response.searchcontentsss);	
									$('#searchhtml').html(response.searchcontentsss);
									//$('#checkoutpage').fadeIn();
									$("#nextsearch").val(0);
									 $("#overlayssss").attr('style','display:none;');
								 }
								});	
	 }	 
	 function selectOnlyThis(id) {
		         console.log();
				 $("#overlayssss").attr('style','display:block;');
				  // alert(id);	
                       var optVal= id; 
			          var limit= $("#nextsearch").val();
					 var currenttype = $("#currenttype").val();
					 var viewstyle = $('#viewstyle').val();
					 // alert(limit);
					  var searchvalue = '<?php echo $_GET['s']; ?>';
					  
						// alert(limit);
						 if( currenttype != optVal)
						 {
							 limit = 0;
						 }
						// alert(optVal);
						 $.ajax({
									url: '<?php echo admin_url('admin-ajax.php'); ?>',
									type: "POST",
                                    dataType: "json",
									data: {
										action: 'load_more_search_home',
									    'limitserach':limit,
										'searchvalue':searchvalue,
										'searchtype':optVal,
										'viewstyle':viewstyle
									},
									success : function(response){
								   // alert(response.searchcontentsss);	
									$('#searchhtml').html(response.searchcontentsss);
									//$('#checkoutpage').fadeIn();
									$("#currenttype").val(optVal);
									
									$("#nextsearch").val(response.nextlimit);
									var nextpageaccept = response.nextsearch;
									if(nextpageaccept == 0)
									{
									 $('.dyn_load_more_search').addClass('hidden');	
									}
									$("#overlayssss").attr('style','display:none;');
									 
								 }
								});						
        }
		//dyn_load_more_search
		// $(document).on('click', '.dyn_load_more_search', function(event){
			 $('.dyn_load_more_search').click(function(e){
			  // var id = 'all';
			  var idfs = [];
			  //  var searchIDs = $(".sortsearchtype input:checkbox:checked").val();
			   $("#overlayssss").attr('style','display:block;');
			  var searchIDs = $("#currenttype").val();
			   $(".chk:checked").each(function(){
                       idfs.push(this.value);
                    });
							  
					var limit= $("#nextsearch").val();
					 var viewstyle = $('#viewstyle').val();
					 // alert(limit);
					  var searchvalue = '<?php echo $_GET['s']; ?>';
					  
						// alert(searchIDs);
						 
						 console.log();
						 $.ajax({
									url: '<?php echo admin_url('admin-ajax.php'); ?>',
									type: "POST",
                                    dataType: "json",
									data: {
										action: 'load_more_search_home',
									    'limitserach':limit,
										'searchvalue':searchvalue,
										'searchtype':idfs,
										'viewstyle':viewstyle
									},
									success : function(response){
								   // alert(response.searchcontentsss);	
									$('#searchhtml').append(response.searchcontentsss);
									//$('#checkoutpage').fadeIn();
									$("#nextsearch").val(response.nextlimit);
									var nextpageaccept = response.nextsearch;
									if(nextpageaccept == 0)
									{
									 $('.dyn_load_more_search').addClass('hidden');	
									}
									 $("#overlayssss").attr('style','display:none;');
								 }
								});			  
			  
		 });
		
		 $(document).on('click', '#grid', function(event){

				$('.searchallcontent').addClass('item2  col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center  group-search-grid');
				
                  $('#viewstyle').val('grid');
				$("div.hentry.search-content:gt(11)").show();

				    //FA: show details button
				      if ( $('.bottom-detailsul').hasClass('hidden') ) {
				        $('.bottom-detailsul').removeClass('hidden');
				        $('.bottom-detailsul').addClass('show');
						 
				     }
					 
				event.preventDefault();
			});
         
		 $(document).on('click', '#list', function(event){
					$('.searchallcontent').removeClass('item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center  group-search-grid');
					$('#viewstyle').val('list');
					$('.search-content').addClass('list-search-grid');
					if ( $('.bottom-detailsul').hasClass('show') ) {
				        $('.bottom-detailsul').removeClass('show');
				        $('.bottom-detailsul').addClass('hidden');
						 
				     }
			});
		
         $(document).on('click', '.dyn_open_review_box', function(e){
			$(this).next('.dyn_all_reviews').show();
		});
		
       $(document).on('click', '.dyn_close_review_box', function(e){
			$(this).parent('.dyn_all_reviews').hide();
		});

			
	</script>
<script type="text/javascript">
		$(function(){


             //FA: prepare modal to be appended
			appendthis =  ("<div class='modal-overlayblockdiv'></div>");


               //FA: set default value when page loads
               $("ul.pagination li a").each(function( index ) {
				$(this).attr("data-viewstyle", "" +
						"list");
				});
		 });

		$(document).ready(function(){

			if( /viewstyle=grid/.test(window.location.href) ){

             	$('#grid').click();
             }
             else{
             	$("div.hentry.search-content:gt(11)").hide();
             	$("ul.pagination").eq(0).show();
				$("ul.pagination.gridviewstyle").hide();
             }

        });//end of document ready
		
		
		$(document).on('click', '#countymidssss', function(e) {
var $ = jQuery.noConflict();	
	e.preventDefault();
var postidsss = $(this).attr('data-countymids');
		          $.ajax({
							url: "<?php echo $_ajaxsss; ?>",				
							type: "POST",
							dataType: "JSON",
							data: { 
								action: "shipping_rate_country_by_postid",
								postidsss: postidsss
							 },
							success: function(data) { 
									$('#shippingrateshow').html(data.is_insert);
								
							},
						   complete: function() {
							   
							    $('#moreshipped').fadeIn();
						   }
						   
						});

});
$(document).on('click', 'button.close', function(e) {
	var $ = jQuery.noConflict();
	$('#moreshipped').fadeOut();
});

$(document).on('click', '#liststore', function(e) {
	var $ = jQuery.noConflict();
	  e.preventDefault();
	  $('#productssss .item').removeClass('grid-group-item changeStyleGrid');  
      $('#productssss .item').addClass('list-group-item changeStyleList');
});

$(document).on('click', '#gridstore', function(e) {
	var $ = jQuery.noConflict();
	 e.preventDefault();
     $('#productssss .item').removeClass('list-group-item changeStyleList');
    $('#productssss .item').addClass('grid-group-item changeStyleGrid');
});

$(document).on('click', 'a[data-modal-id]', function(e) {
                
 					 e.preventDefault();
  					$("body").append(appendthis);
  					$(".modal-overlay").fadeTo(500, 0.7);
 					   //$(".js-modalbox").fadeIn(500);
  						  var modalBox = $(this).attr('data-modal-id');

    				$('#'+modalBox + " .layout-title").html('<a href="'+ $(this).data('permalink') + '" title="' + $(this).data('title') + '" style="color:rgb(75, 75, 75)!important; font-size:16px !important; font-weight:600 !important">' + $(this).data('title') + '</a>');
   			 		$('#'+modalBox + " .image-holder a").attr('href', $(this).data('permalink'));
   			 			$('#'+modalBox + " img").attr('src', $(this).data('image'));


    				var thecontent=$(this).data('content').replace( /<p>/g, '' ).replace( /<\/p>/g, '' );

   					 $("#"+modalBox + " .inline-blockright:eq(0)").html(thecontent);
   					 $("#"+modalBox + " .inline-blockright:eq(2)").text($(this).data('author'));
   					 $("#"+modalBox + " .inline-blockright:eq(1)").html($(this).data('ref').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));


					if(modalBox=='productsModal') {
						if($(this).data('image')!='http://www.doityourselfnation.org/bit_bucket/wp-includes/images/media/default.png'){
							$('#'+modalBox + " img").attr('src', $(this).data('image'));
						}else{
							$('#'+modalBox + " img").attr('src','http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg');
						}

						$("#"+modalBox + " .product-regular_price").html($(this).data('regularprice').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
						$('#'+modalBox + " .imgall").html($(this).data('allimg').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
						$('#'+modalBox + " .product-sold-take").text($(this).data('sold'));
						$("#"+modalBox + " .stock-span-grid").text($(this).data('stock'));
						$("#"+modalBox + " #shipped_country").html($(this).data('shippinto')); 
					}else{ }

   					 $('#'+modalBox + " .stats li:eq(0)").text($(this).data('time'));
   					 $('#'+modalBox + " .stats li:eq(1)").text($(this).data('views'));
   					 $('#'+modalBox + " .stats li:eq(2)").text($(this).data('endorse') + ' Endorsments');
  					 $('#'+modalBox + " .stats li:eq(3)").text($(this).data('favr') + ' Favorites');
   				     $('#'+modalBox + " .stats li:eq(4)").text($(this).data('revs'));



    				var comments=$(this).data("comments");
   					 if( (comments==0) || (comments=='No Comments') ){
    				  ncomments="No Comments";
    					}
   					 else{
   				   ncomments=comments + " Comments";
    				}
    				$("#"+modalBox + " .stats li:eq(5)").text(ncomments);

    			if($(this).data("ftype")){

      				$('#'+modalBox + " #img-hld").attr("class", "image-holder filesz dyn-file-sprite " + $(this).data("fclass"));
     				 $('#'+modalBox + " #ftype").text("File Type: " + $(this).data("ftype"));
     			 	if($(this).data("download")!="")
     		    	$('#'+modalBox + " .stats li:last-child").text( $(this).data("download") + " downloads");
      				else
      		 		 $('#'+modalBox + " .stats li:last-child").text("0 downloads");
    			}

    			$('#'+modalBox).fadeIn();

  				});

				
		        $(document).on('click', '.js-modal-close, .modal-overlay', function(e) {	
 				 $(".modal-box, .modal-overlay").fadeOut(500, function() {
   				 $(".modal-overlay").remove();
  				});
				});

				$(window).resize(function() {
 			 	$(".modal-box").css({
   			 	top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
    		 	left: ($(window).width() - $(".modal-box").outerWidth()) / 2
  					});
				// $('.defaultheight').css('min-height:', $(window).height()+'px;');	
				});

			$(window).resize();
        </script>
<style>
.grid-6 {
   
    float: left;
    margin-left: 15px;
    margin-right: 15px;
    width: 234px;
}

.page-numbers, .page-numbers:hover{
	background: #3498db;
    padding: 5px;
    color: #fff;
}
.group-search-grid .grid-6 {
    float: none !important;
    width: 100% !important;
    margin-left: auto;
    margin-right: auto;
}
.do_pagination{margin-left:10px;}
.group-search-grid .dyn_rating_bar,.group-search-grid .review_box,.group-search-grid .video_excerpt_text,
.group-search-grid .layout-2-details span,.group-search-grid .stats,.group-search-grid .layout-2-details{display:none;}
.group-search-grid .layout-2-details{overflow:visible;}
.group-search-grid .grid-6{min-height: 160px;}
.list-search-grid  .bottom-detailsul{ display:none; }
.group-search-grid  .bottom-detailsul{ display:block; }
.group-search-grid {
    display: block;
    margin-bottom: 24px;
    background-color: #F6F6F6;
    padding: 5px 5px 20px 5px !important;
    margin: 0px !important;
    height: auto;
}
.overlaysearch {
    height: 100%;
   
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.2);
    overflow-x: hidden;
    transition: 0.5s;
}

.overlaysearch-content {
    position: relative;
    top: 40%;
   
    text-align: center;
    margin-top: 30px;
    border-radius: 5px;
    padding: 30px;
    background-color: #fff;
    margin: 0 auto;
    width: 50%;
	color: #337ab7;
    opacity: 1;

}
.overlaysearch-content h2{color: #337ab7;}	
</style>

	<div class="modal-box" id="productsModal">
		<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details">
				<div class="image-holder " style="margin 0 auto; display:block; width:200px !important; height:150px !important" >
				
					<a href="">
						<div class="hover-item" style="width:200px !important; height:150px !important"></div>
					</a>
					<img src="" class="layout-2-thumb wp-post-image" alt="" style="height:150px; max-height:100%!important;">
				</div>

				<div class="imgall">

				</div>


				<div class='other-image-product-first'>
					<div class="thumbnails columns-3">

					</div></div>


				<h6 class="layout-title"></h6>
				<div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div>
				<div class="reviews review_box dyn_all_reviews search.php" style="display:none;">

					<!--modal mmm-->
					<div class="btn btn-danger dyn_close_review_box">Close</div> </div>


				<span>(clickable)</span>

				<div class="product-regular_price"></div>

				<div class="stock-grid">
					<h4 class="tittle-h4"><i class="fa fa-smile-o" aria-hidden="true"></i><span class="stock-span-grid">  </span><span style="color:black;font-weight: 400;"> in Stock</span></h4>
				</div>



				<div class="video_excerpt_text one">
					<h4 class="tittle-h4">Description</h4>
					<div class="inline-blockright">okay</div>
				</div>


				<div class="video_excerpt_text two-ref-del"  ><h4 class="tittle-h4"></h4><div class="inline-blockright"></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Sold By</h4><div class="inline-blockright">reference</div></div>
				<div class="video_excerpt_text  shipped-model-home"><h4 class="tittle-h4">Shipped To</h4><div class="inline-blockright" id="shipped_country">Bangladesh</div></div>




				<ul class="stats">

					<li></li>
					<li></li>
					<li><i class="fa fa-wrench"></i> 0 Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
						0 Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> 0 Reviews</li>
					<li></li>
					<li class="product-sold-take"> </li>
				</ul>
				<div class="clear"></div>
				<p></p>
			</div>
			 <div id="shippingrateshow">
           </div>
		</div>
	</div>	
	
	
<div class="modal-box" id="videosModal">
 <div class="modal-body">
  <a class="js-modal-close close">×</a>
  <div class="layout-2-details">
  
							   	       
    <div class="image-holder " style="margin 0 auto; display:block; width:200px !important; height:150px !important" >
    
      <a href="">
        <!--<div class="hover-item" style="width:200px !important; height:150px !important"></div>-->
      </a>
      <img src="" class="layout-2-thumb wp-post-image" alt="" style="height:150px; max-height:100%!important;">
    </div>
    <h6 class="layout-title"></h6>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div>
	  <div class="reviews review_box dyn_all_reviews search.php" style="display:none;">
  <!--modal mmm-->
    <div class="btn btn-danger dyn_close_review_box">Close</div> </div>
	  <span>(clickable)</span> <div class="video_excerpt_text one">
		  <h4 class="tittle-h4">Description</h4>
		  <div class="inline-blockright">okay</div>
	  </div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright">reference</div></div><div class="video_excerpt_text two"><h4 class="tittle-h4">Author: </h4><div class="inline-blockright">reference</div></div>
    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i> 0 Endorsments</li>
      <!-- favourite section -->
      <li><i class="fa fa-heart"></i>
        0 Favorites</li>
        <!-- end favorite -->
        <li><i class="fa fa-star-o"></i> 0 Reviews</li>
        <li></li>
      </ul>
      <div class="clear"></div>
      <p></p>
    </div>
  </div>
</div>	

<div  class="modal-box" id="docs">
 <div class="modal-body">
   <a class="js-modal-close close">×</a>
   <div class="layout-2-details">
    <div id="img-hld" class="">
      <!--<a href="">
        <div class="hover-item dyn-file">
          <i class="fa fa-download"></i>
        </div>
      </a>-->
    </div>
    <h6 class="layout-title"><a href="" title="wow"></a></h6>
    <div id="ftype"></div>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div><div class="reviews review_box dyn_all_reviews eli_search.php" style="display:none;">

    <div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span>
    <div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright">No Description available</div></div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright">No referance </div></div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Author</h4><div class="inline-blockright"></div></div>
    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i>
        0 Endorsments</li>
        <li><i class="fa  fa-heart"></i>
          0 Favorites</li>
          <!--end favorite-->
          <li><i class="fa fa-star-o"></i> 0 Reviews</li>
          <li>No Comments</li>
          <li><i class="fa fa-download"></i> </li>
        </ul>
        <div class="clear"></div>
        <p></p>
      </div>
     </div>
    </div>
	
<div id="overlayssss" class="overlaysearch" style="display:none;">
 
  <div class="overlaysearch-content">
    <h2><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></h2>
  </div>
</div>		
	
<div class="clear"></div>
<?php get_template_part('includes/hover_video_search_page'); ?>
<?php get_template_part('includes/hover_search_page'); ?>

<?php get_footer(); ?>