<?php

// Set Image Path
$themepath = get_template_directory_uri().'/images/';
$vafpresspath = get_template_directory_uri().'/options/public/img/';

// Start Options
return array(
	'title' => __('VideoPress Options', 'textdomain_code'),
	'page' => __('Code Menu', 'textdomain_code'),
	'logo' => '',
	'menus' => array(
	
	
	
			/* =============== Menu - General Settings ====================== */
			array(
			'title' => __('General Settings', 'textdomain_code'),
			'name' => 'menu_1',
			'icon' => 'font-awesome:fa-gears',
			'controls' => array(
			
		// Start Color Schem
		 array(
        'type' => 'radioimage',
        'name' => 'color_scheme',
        'label' => __('Select Color Scheme', 'vp_textdomain'),
        'description' => __('Choose color scheme for the theme', 'vp_textdomain'),
        'item_max_height' => '70',
        'item_max_width' => '70',
        'items' => array(
            array(
                'value' => 'red',
                'label' => __('Red', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/red.png',
            ),
            array(
                'value' => 'blue',
                'label' => __('Blue', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/blue.png',
            ),
            array(
                'value' => 'orange',
                'label' => __('Orange', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/orange.png',
            ),
            array(
                'value' => 'purple',
                'label' => __('Purple', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/purple.png',
            ),
			array(
                'value' => 'green',
                'label' => __('Green', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/green.png',
            ),
			array(
                'value' => 'yellow',
                'label' => __('Yellow', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/yellow.png',
            ),
			array(
                'value' => 'grey',
                'label' => __('Grey', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/gray.png',
            ),
			array(
                'value' => 'black',
                'label' => __('Black', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/black.jpg',
            ),
        ),
        'default' => array(
            'red',
        ),
    ),
		
				// End Color Scheme
				
		
				// Start Logo Upload
					array(
						'type' => 'upload',
						'name' => 'logo',
						'label' => __('Upload Theme Logo', 'textdomain_code'),
						'description' => __('Upload Your Theme Logo', 'textdomain_code'),
						'default' => $themepath.'logo.png',
					),
				// End Logo Upload
				
				// Start Small Logo Upload
					array(
						'type' => 'upload',
						'name' => 'logo_small',
						'label' => __('Upload Small Logo', 'textdomain_code'),
						'description' => __('Upload a small logo', 'textdomain_code'),
						'default' => $themepath.'logo-small.png',
					),
				// End Small Logo Upload
				
				
				// Start Footer Copyright
					array(
						'type' => 'textarea',
						'name' => 'copyright',
						'label' => __('Copyright Text', 'textdomain_code'),
						'description' => __('Set your copyright notice', 'textdomain_code'),
						'default' => '&copy; 2015 VideoPress WordPress Theme. All rights reserved',
					),
				// End Footer Copyright
				
				// Enable Pagination
					array(
						'type' => 'toggle',
						'name' => 'pagination',
						'label' => __('Custom Pagination', 'textdomain_code'),
						'description' => __('Do you want to use custom pagination?', 'textdomain_code'),
						'default' => '1',
					),
				// Enable Pagination
				
				// Enable Pagination
					array(
						'type' => 'toggle',
						'name' => 'show_user',
						'label' => __('Show who uploaded video?', 'textdomain_code'),
						'description' => __('Do you want to show who uploaded the video?', 'textdomain_code'),
						'default' => '0',
					),
				// Enable Pagination

						)),
			/* =============== Menu - General Settings ====================== */
			


			/* =============== Jwplayer Settings ====================== */
			array(
			'title' => __('Video Player Settings', 'textdomain_code'),
			'name' => 'menu_2',
			'icon' => 'font-awesome:fa-play-circle-o',
			'controls' => array(
			
			// Start License Key
					array(
						'type' => 'textbox',
						'name' => 'license_key',
						'label' => __('JWplayer PRO License Key', 'textdomain_code'),
						'description' => __('If you have purchased a license from JWplayer you can enter it here.', 'textdomain_code'),
						'default' => '',
					),
			// End License Key
			
				// Start Player Skin
				array(
					'type' => 'select',
					'name' => 'playerskin',
					'label' => __('Select Player Skins', 'textdomain_code'),
					'items' => array(
						array(
							'value' => 'default_skin',
							'label' => __('Default Skin', 'textdomain_code'),
						),
						array(
							'value' => 'skin_one',
							'label' => __('Skin 1', 'textdomain_code'),
						),
						array(
							'value' => 'skin_two',
							'label' => __('Skin 2', 'textdomain_code'),
						),
						array(
							'value' => 'bekle',
							'label' => __('Bekle(Licensed Only)', 'textdomain_code'),
						),
						array(
							'value' => 'glow',
							'label' => __('Glow(Licensed Only)', 'textdomain_code'),
						),
						array(
							'value' => 'beelden',
							'label' => __('Beelden(Licensed Only)', 'textdomain_code'),
						),
						array(
							'value' => 'stormtrooper',
							'label' => __('Stormtrooper(Licensed Only)', 'textdomain_code'),
						),
						array(
							'value' => 'vapor',
							'label' => __('Vapor(Licensed Only)', 'textdomain_code'),
						),
						array(
							'value' => 'roundster',
							'label' => __('Roundster(Licensed Only)', 'textdomain_code'),
						),
					),
					'default' => array(
						'default_skin',
					),
					'validation' => 'required',
				),
				// End Player Skin
				
				// Start Video Player Logo
					array(
						'type' => 'upload',
						'name' => 'player_logo',
						'label' => __('Player Logo(Licensed Only)', 'textdomain_code'),
						'description' => __('Upload A logo for your video player', 'textdomain_code'),
						'default' => $themepath.'logo-small.png',
					),
				// End Video Player Logo
				
				// Start Player Link
					array(
						'type' => 'textbox',
						'name' => 'player_link',
						'label' => __('Logo Link(Licensed Only)', 'textdomain_code'),
						'description' => __('If you have purchased a license from JWplayer you can enter it here.', 'textdomain_code'),
						'default' => site_url(),
					),
				// End Player Link
				
				// Start About Player
					array(
						'type' => 'textbox',
						'name' => 'player_about',
						'label' => __('About Text(Licensed Only)', 'textdomain_code'),
						'description' => __('If you wish to display an about text when you right click on the player.', 'textdomain_code'),
						'default' => 'About Video Player',
					),
				// End About Player
				
				// Start Player Link
					array(
						'type' => 'textbox',
						'name' => 'player_about_link',
						'label' => __('About Link(Licensed Only)', 'textdomain_code'),
						'description' => __('About Text Link', 'textdomain_code'),
						'default' => site_url(),
					),
				// End Player Link
			
			)),
			
			
			/* =============== Menu - Social Media Settings ====================== */
			array(
			'title' => __('Social Media Settings', 'textdomain_code'),
			'name' => 'menu_3',
			'icon' => 'font-awesome:fa-group',
			'controls' => array(
			
			// Social Button Type
				 array(
        'type' => 'radioimage',
        'name' => 'social_type',
        'label' => __('Type of Social Media Icon', 'vp_textdomain'),
        'item_max_height' => '30',
        'item_max_width' => '30',
        'items' => array(
            array(
                'value' => 'square',
                'label' => __('Squared', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/twitter-square.jpg',
            ),
            array(
                'value' => 'logo',
                'label' => __('Logo Only', 'vp_textdomain'),
                'img' => 'http://theme-titans.com/wp-content/uploads/2015/03/twitter.jpg',
            ),
         
        ),
        'default' => array(
            'value_4',
        ),
    ),
			
			// End social button type
			
			
					
				// Facebook
					array(
						'type' => 'textbox',
						'name' => 'facebook',
						'label' => __('Facebook page', 'textdomain_code'),
						'description' => __('Enter your Facebook URL', 'textdomain_code'),
						'default' => '',
						'validation' => 'url',
					),
				// Facebook
				
				// Twitter
					array(
						'type' => 'textbox',
						'name' => 'twitter',
						'label' => __('Twitter Profile', 'textdomain_code'),
						'description' => __('Enter your Twitter Profile URL', 'textdomain_code'),
						'default' => '',
						'validation' => 'url',
					),
				// Twitter
				
				// Google
					array(
						'type' => 'textbox',
						'name' => 'google',
						'label' => __('Google Plus', 'textdomain_code'),
						'description' => __('Enter your Google Plus URL', 'textdomain_code'),
						'default' => '',
						'validation' => 'url',
					),
				// Google
				
				// Rel=Publisher Google+
				array(
						'type' => 'toggle',
						'name' => 'relpublisher',
						'label' => __('Rel=Publisher', 'textdomain_code'),
						'description' => __('Switch on Rel=Publisher tag in the Google+ link', 'textdomain_code'),
						'default' => '0',
					),
				// Rel=Publisher Google+
				
				// Pinterest
					array(
						'type' => 'textbox',
						'name' => 'pinterest',
						'label' => __('Pinterest', 'textdomain_code'),
						'description' => __('Enter your pinterest profile', 'textdomain_code'),
						'default' => '',
						'validation' => 'url',
					),
				// Pinterest
				
				// Flickr
					array(
						'type' => 'textbox',
						'name' => 'linkedin',
						'label' => __('LinkedIn', 'textdomain_code'),
						'description' => __('Enter your LinkedIn profile', 'textdomain_code'),
						'default' => '',
						'validation' => 'url',
					),
				// Flickr
				
				
					// Open in New Tab
				array(
						'type' => 'toggle',
						'name' => 'targetblank',
						'label' => __('Open in New Window?', 'textdomain_code'),
						'description' => __('Open all social links in new tab?', 'textdomain_code'),
						'default' => '1',
					),
				// Open in New Tab
				
				// Enable RSS Feed
					array(
						'type' => 'toggle',
						'name' => 'enablerss',
						'label' => __('Enable RSS Feed', 'textdomain_code'),
						'description' => __('Turn on RSS Feed?', 'textdomain_code'),
						'default' => '0',
					),
				// Enable RSS Feed
				
				
					)),
			/* =============== Menu - Social Media Settings ====================== */	
			
			
			
			/* =============== Menu - Footer Widgets ====================== */
			array(
			'title' => __('Footer Widgets', 'textdomain_code'),
			'name' => 'menu_4',
			'icon' => 'font-awesome:fa-th',
			'controls' => array(
					

			// Enable RSS Feed
					array(
						'type' => 'toggle',
						'name' => 'enable_footer_widget',
						'label' => __('Enable Footer Widget', 'textdomain_code'),
						'description' => __('Do you want to enable the footer widget?', 'textdomain_code'),
						'default' => '0',
					),
			// Enable RSS Feed

			array(
				'type' => 'radioimage',
				'name' => 'widget_columns',
				'label' => __('Widget Columns', 'textdomain_code'),
				'description' => __('Choose a column for your widget', 'textdomain_code'),
				'item_max_height' => '100',
				'item_max_width' => '100',
				'items' => array(
					array(
						'value' => 'style_one',
						'label' => __('Style One', 'textdomain_code'),
						'img' => $themepath.'sizes/style-1.png',
					),
					array(
						'value' => 'style_two',
						'label' => __('Style Two', 'textdomain_code'),
						'img' => $themepath.'sizes/style-2.png',
					),
					array(
						'value' => 'style_three',
						'label' => __('Style Three', 'textdomain_code'),
						'img' => $themepath.'sizes/style-3.png',
					),
					array(
						'value' => 'one_third',
						'label' => __('One Third', 'textdomain_code'),
						'img' => $themepath.'sizes/one-third.png',
					),
					array(
						'value' => 'one_fourth',
						'label' => __('One Fourth', 'textdomain_code'),
						'img' => $themepath.'sizes/one-fourth.png',
					),
				),
				'default' => array(
					'one_third',
				),
			),

					)),
			/* =============== Menu - Footer Widgets ====================== */	


	)
);

/**
 *EOF
 */