<?php /* Template Name: Login Register*/ 
get_header(); 
?>
<style>
h2{display: none;}
h1{text-align:center;}
.signup-form{width:50%;margin:0 auto;}
.user-forms .user-input {
    outline: none;
    padding: 8px;
    margin: 0px;
        margin-bottom: 0px;
    margin-bottom: 10px;
    border: none;
    border: 2px solid #f5f5f5;
    width: 100%;
    border-radius: 2px;
}
select{
	width: 100%;
padding: 3px;
border: 2px solid #f5f5f5;

}
.col-md-6{text-align: center;}
</style>
 <div class="container" >
 <?php 
  $get = $_GET['q'];
   if($get == 'signup'){
	   $success = 0;
	    if(isset($_POST['trigger_registers']) && !empty($_POST['trigger_registers'])) {
  
  $user_name = $_POST['username'];
  $user_email = $_POST['email'];
  
  $billing_first_name = $_POST['billing_first_name'];
  $billing_last_name = $_POST['billing_last_name'];
  $billing_company = $_POST['billing_company'];
  $billing_address_1 = $_POST['billing_address_1'];
  $billing_address_2 = $_POST['billing_address_2'];
  $billing_city = $_POST['billing_city'];
  $billing_postcode = $_POST['billing_postcode'];
  $billing_country = $_POST['billing_country'];
  $billing_state = $_POST['billing_state'];
  $billing_phone = $_POST['billing_phone'];
  
  $random_password = wp_generate_password( '12', 'true', 'true' );
  
  // Check if username and email exist
  if ( !username_exists( $user_name ) and email_exists($user_email) == false ) {
  	
	// Check Fields
  	if( $user_name == '' ||  $user_email == '' ){
		
		echo '<div class="widget-register-error">Fields cannot be blank</div>';
		
	}else{
	
		// Check if valid email
		if( is_email($user_email) ){
			// Register into database
			$user_id =  wp_create_user( $user_name, $random_password, $user_email);
			
			update_user_meta( $user_id, "billing_first_name", $billing_first_name );
            update_user_meta( $user_id, "billing_last_name", $billing_last_name );
			update_user_meta( $user_id, "billing_company", $billing_company );
            update_user_meta( $user_id, "billing_address_1", $billing_address_1 );
			update_user_meta( $user_id, "billing_address_2", $billing_address_2 );
            update_user_meta( $user_id, "billing_city", $billing_city );
			update_user_meta( $user_id, "billing_postcode", $billing_postcode );
            update_user_meta( $user_id, "billing_country", $billing_country );
			update_user_meta( $user_id, "billing_state", $billing_state );
            update_user_meta( $user_id, "billing_phone", $billing_phone );
			 
			// Send password to email
			$subject = get_bloginfo('name')." Registration";
			$message = 'Password is: '.$random_password;
			$header = 'from: Registration '.get_option('admin_email');
			$send_contact=mail($user_email,$subject,$message,$header);
			
			// Notify Success
			echo '<div class="widget-register-success">Registration Successful, Get Password from email address</div>';
			$success = 1;
		}else{ // If Email not valid
			echo '<div class="widget-register-error">Invalid Email</div>';
			$success = 0;
		} // End Email Validation
		
	} // End Checking of fields

	
  // Else username and email exist
  }else {
	
	echo '<ul class="widget-register-error">';
	
	// If Username Exist
	if( username_exists( $user_name ) != '' ){
		echo '<li>Username Already Exists, Choose another one</li>';
	} // End Username Exist
  	
	
	// If Email Exist
	if( email_exists($user_email) ){
		echo '<li>Email Already Registered, Please Choose another one</li>';
	} // End Email Exist
	
	echo '</ul>';
	
  } // End if check exist username and password
  
  }// End trigger
 
	   ?>
	   <?php if($success == 0) { ?>
	    <div class="signup-form">
	    <form name="loginform" method="post" action="" class="user-forms">
					 <br/>
		          <label> Username </label>
                  <input type="text" name="username" placeholder="Username" class="user-input">
				  <label> Email </label>
                   <input type="text" name="email" placeholder="Email" class="user-input">
				 <label> First Name </label>
				   <input type="text" name="billing_first_name" id="billing_first_name" placeholder="First Name" class="user-input">
				   <label> Last Name </label>
				   <input type="text" name="billing_last_name" id="billing_last_name" placeholder="Last Name" class="user-input">
				   <label> Company </label>
				   <input type="text" name="billing_company" id="billing_company" placeholder="company" class="user-input">
				   <label> Address 1 </label>
				   <input type="text" name="billing_address_1" id="billing_address_1" placeholder="Street address" class="user-input">
				   <input type="text" name="billing_address_2" id="billing_address_2" placeholder="Apartment, suite, unit etc. (optional)" class="user-input">
				   <label> City </label>
				   <input type="text" name="billing_city" id="billing_city" placeholder="city" class="user-input">
				   <label> Post Code </label>
				   <input type="text" name="billing_postcode" id="billing_postcode" placeholder="postcode" class="user-input">
				   <label> Country </label>
				   <select name="billing_country" id="billing_country"  tabindex="-1" title="Country *">
				   <option value="">Select a country…</option><option value="AX">Åland Islands</option><option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="PW">Belau</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option><option value="BQ">Bonaire, Saint Eustatius and Saba</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="VG">British Virgin Islands</option><option value="BN">Brunei</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo (Brazzaville)</option><option value="CD">Congo (Kinshasa)</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CW">CuraÇao</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and McDonald Islands</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran</option><option value="IQ">Iraq</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="CI">Ivory Coast</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JE">Jersey</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Laos</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao S.A.R., China</option><option value="MK">Macedonia</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia</option><option value="MD">Moldova</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="AN">Netherlands Antilles</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="KP">North Korea</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PS">Palestinian Territory</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="QA">Qatar</option><option value="IE">Republic of Ireland</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russia</option><option value="RW">Rwanda</option><option value="ST">São Tomé and Príncipe</option><option value="BL">Saint Barthélemy</option><option value="SH">Saint Helena</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="SX">Saint Martin (Dutch part)</option><option value="MF">Saint Martin (French part)</option><option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option><option value="SM">San Marino</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia/Sandwich Islands</option><option value="KR">South Korea</option><option value="SS">South Sudan</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syria</option><option value="TW">Taiwan</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom (UK)</option><option value="US">United States (US)</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VA">Vatican</option><option value="VE">Venezuela</option><option value="VN">Vietnam</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="WS">Western Samoa</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select>
				   <label> State </label>
				   <input type="text" name="billing_state" id="billing_state" placeholder="state" class="user-input">
				   
				   <label> Phone </label>
				   <input type="text" name="billing_phone" id="billing_phone" placeholder="phone" class="user-input">
				   
                   <input type="submit" value="Register Accounts" class="user-submits btn btn-primary" name="trigger_registers">
				   <br/>
				    <br/>
					 <br/>
			</form>
	   </div>	<?php }else{ ?>
	   <div class="col-md-12">
	         <div class="signup-form">
        <h1>Login</h1>
        <?php echo do_shortcode('[woocommerce_my_account]'); ?>
		   </div>
		   <br/>
				    <br/>
					 <br/>
          </div>
	   <?php
	   }
   } else{ 
 ?>
  <div class="col-md-6">
    <h1>OLD Customer/User</h1>
   <?php echo do_shortcode('[woocommerce_my_account]'); ?>
  </div>
  <div class="col-md-6">
  <h1>New Customer/User</h1>
   <br/>
   <a href="?q=signup" class="btn btn-primary" >Create account</a>
 </div>
  
  					<script>
					$('#createaccountss').click(function(e){
			 
		var $ = jQuery.noConflict();
		$('#model_create').modal();
			 
	});
</script>
  <?php
   }
  // When register button is triggered
 
  
 ?>
										
</div>
<?php get_footer(); ?>


 