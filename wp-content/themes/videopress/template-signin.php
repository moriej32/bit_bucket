<?php /* Template Name: Sign In */ ?>
<?php get_header(); 
include "widgets/users.php";
?>

    <!-- Start Content -->
    <div class="container p_90">

    <?php //get_template_part('includes/featured-playlist'); ?>
    
    <!-- Start Entries -->
    <div class="grid-3-4 centre-block">
    
    <!-- Content -->
    <div class="entry">
    <?php
		// Start The Loop
		while (have_posts()) : the_post();
			    videopress_users_widget();
			the_content();
		endwhile;
		// End Of Loop
	?>
    <div class="clear"></div>
    </div>
    <!-- End Content -->
    
    <?php //get_template_part('layouts/layout-builder'); ?>
    
    </div>
    <!-- End Entries -->
    
    <!-- Widgets -->
    <!-- <?php //get_sidebar(); ?>-->
    <!-- End Widgets -->
    
    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->
    
<?php get_footer(); ?>