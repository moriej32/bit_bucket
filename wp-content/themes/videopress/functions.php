<?php

/*-----------------------------------------------------------------------------------*/
/*	Load Vafpress Framework
/*-----------------------------------------------------------------------------------*/
require_once('vafpress.php');

use GeoIp2\Database\Reader;
//echo $_SERVER['REMOTE_ADDR'];
//$_SERVER['REMOTE_ADDR']='70.248.28.23';
function get_user_country()
{
	$reader = new Reader('GeoIP2-Country_20160830/GeoIP2-Country.mmdb');
	if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
		$record = $reader->country($_SERVER['REMOTE_ADDR']);
		if ($record) {
			return $record->country->name;
		}
	}else{
		return 'United States';
	}
	//return false;
}

function real_ip_checknowsssss()
{
    $header_checks = array(
        'HTTP_CLIENT_IP',
        'HTTP_PRAGMA',
        'HTTP_XONNECTION',
        'HTTP_CACHE_INFO',
        'HTTP_XPROXY',
        'HTTP_PROXY',
        'HTTP_PROXY_CONNECTION',
        'HTTP_VIA',
        'HTTP_X_COMING_FROM',
        'HTTP_COMING_FROM',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_X_CLUSTER_CLIENT_IP',
        'HTTP_FORWARDED_FOR',
        'HTTP_FORWARDED',
        'ZHTTP_CACHE_CONTROL',
        'REMOTE_ADDR'
    );
 
    foreach ($header_checks as $key)
    {
        if (array_key_exists($key, $_SERVER) === true)
        {
            foreach (explode(',', $_SERVER[$key]) as $ipsssss)
            {
                $ipsssss = trim($ipsssss);
                 
                //filter the ip with filter functions
                if (filter_var($ipsssss, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false)
                {
                    return $ipsssss;
                }
            }
        }
    }
}

add_filter ('add_to_cart_redirect', 'redirect_to_checkout_naz');
function redirect_to_checkout_naz() {
    return WC()->cart->get_checkout_url();
}

function get_user_country_isocode_corrent_country()
{
	$ipaddress = real_ip_checknowsssss();
	$reader = new Reader('GeoIP2-Country_20160830/GeoIP2-Country.mmdb');
	if ( $ipaddress ) {
		$record = $reader->country( $ipaddress );
		if ($record) {
			return $record->country->isoCode;
		}
	}else{
		return 'US';
	}
	//return false;
}

function woocommerce_login_check_redirects() {

    // Case1: Non logged user on checkout page (cart empty or not empty)
   /* if ( !is_user_logged_in() && (is_cart() || is_checkout()) )
        wp_redirect('http://www.doityourselfnation.org/bit_bucket/customer-login/');*/

    // Case2: Logged user on my account page with something in cart
    if( is_user_logged_in() && !WC()->cart->is_empty() && is_account_page() )
        wp_redirect( get_permalink( get_option('woocommerce_checkout_page_id') ) );
}
add_action('template_redirect', 'woocommerce_login_check_redirects');

function get_user_country_tr()
{
	$reader = new Reader('GeoIP2-Country_20160830/GeoIP2-Country.mmdb');
	if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
		$record = $reader->country($_SERVER['REMOTE_ADDR']);
		//var_dump($record);
		//echo $record["en"];
		if ($record) {
			return $record->continent->name;
		}
	}
	return false;
}


function user_insert_product_hot_recomend()
{
	if (is_user_logged_in()) {
		global $wpdb;
		$reader = new Reader('GeoIP2-Country_20160830/GeoIP2-Country.mmdb');
		if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
			$record = $reader->country($_SERVER['REMOTE_ADDR']);
			//var_dump($record);
			//echo $record["en"];

			if ($record) {
				$continent = $record->continent->name;

//".$reader->country("91.196.38.29")->continent->name."
				// echo '<pre>';
				$continent_select = $wpdb->get_results("
            SELECT
            t.ID as user,
            SUBSTRING_INDEX(SUBSTRING_INDEX(u.meta_value,';',5),':',-1) AS ip
            FROM wp_users t
            inner  join wp_usermeta u on  t.ID=u.user_id
            where u.meta_key='session_tokens' and t.ID=" . get_current_user_id() . "
           ");

				foreach ($continent_select as $val)
					$continent_name = $reader->country(substr($val->ip, 1, -1))->continent->name;


				if ($continent_name == "Africa") {
					$valueccluster = '1';
				} elseif ($continent_name == "Europe") {
					$valueccluster = '2';
				} elseif ($continent_name == "Asia") {
					$valueccluster = '3';
				} elseif ($continent_name == "North America") {
					$valueccluster = '4';
				} elseif ($continent_name == "South America") {
					$valueccluster = '6';
				} elseif ($continent_name == "Antarctica") {
					$valueccluster = '7';
				} elseif ($continent_name == "Australia") {
					$valueccluster = '8';
				}

				$wpdb->query("
            INSERT INTO `wp_hot_user_product_cluster`(`user_id`, `cluster`)
            SELECT t.ID,'" . $valueccluster . "' FROM wp_users t
            where t.ID=$val->user
            and t.ID NOT IN ( SELECT user_id FROM wp_hot_user_product_cluster)
           ");

				$z = $wpdb->query('
            INSERT INTO `wp_hot_product_cluster`(`post_id`, `cluster`)
            SELECT a.ID,k.cluster FROM wp_posts a
            inner join wp_postmeta b on a.ID=b.post_id
            inner join wp_hot_user_product_cluster k on k.user_id=a.post_author
            and b.meta_key="country"
            and a.post_type="product"
            AND a.post_status="publish"
            AND (SELECT meta_value FROM wp_postmeta where post_id=a.ID and meta_key="total_sales") > 0
            AND (SELECT Count(post_id) FROM wp_favorite where post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_endorsements where post_id=a.ID) > 0
            AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_dyn_review where post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_user_views where  post_id=a.ID) > 0
            AND (SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.ID) > 0
            AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
            AND (a.ID,k.cluster) NOT IN ( SELECT post_id,cluster FROM wp_hot_product_cluster)
           ');


			}
		}

		//insert wp_user_product_recommendation
		$wpdb->query('INSERT INTO wp_user_product_recommendation (post_id, user_id)
SELECT a.ID,t.ID as user FROM wp_users t
inner join wp_posts a
inner join wp_postmeta b on a.ID=b.post_id
and t.ID='.get_current_user_id().'
and b.meta_value > 0  and b.meta_key="total_sales"
and a.post_type="product"
AND a.post_status="publish"
AND (SELECT Count(post_id) FROM wp_favorite where post_id=a.ID) > 0
AND (SELECT Count(post_id) FROM wp_endorsements where post_id=a.ID) > 0
AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
AND (SELECT Count(post_id) FROM wp_dyn_review where post_id=a.ID) > 0
AND (SELECT Count(post_id) FROM wp_user_views where  post_id=a.ID) > 0
AND b.post_id in (SELECT h.post_id FROM wp_user_views h where  h.post_id=b.post_id and h.user_id='.get_current_user_id().')
AND (SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.ID) > 0
AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
AND (a.ID,t.ID) NOT IN ( SELECT post_id, user_id FROM wp_user_product_recommendation)
');

		//tags in user
		$wpdb->query('INSERT INTO wp_user_product_recommendation (post_id, user_id)
SELECT  l.object_id,"'.get_current_user_id().'" FROM  wp_terms
inner join  wp_term_relationships l on l.term_taxonomy_id=term_id
inner join wp_postmeta b on l.object_id=b.post_id
and b.meta_value > 0  and b.meta_key="total_sales"
            AND (SELECT Count(a.post_id) FROM wp_favorite a where a.post_id=l.object_id) > 0
            AND (SELECT Count(a.post_id) FROM wp_endorsements a where a.post_id=l.object_id) > 0
            AND (SELECT Count(a.comment_post_id) FROM wp_comments a where a.comment_post_id=l.object_id) > 0
            AND (SELECT Count(a.post_id) FROM wp_dyn_review a where a.post_id=l.object_id) > 0
            AND (SELECT Count(a.post_id) FROM wp_user_views a where  a.post_id=l.object_id) > 0
            AND (SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=l.object_id) > 0
            AND l.object_id NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")

AND name in (
SELECT  s.name  FROM wp_users t
inner join wp_posts a on a.post_author=t.ID
inner join wp_postmeta b on a.ID=b.post_id
inner join  wp_term_relationships n on b.post_id=n.object_id
inner join  wp_terms s on s.term_id=n.term_taxonomy_id
            and t.ID=' . get_current_user_id() . '
            and b.meta_value > 0  and b.meta_key="total_sales"
            and a.post_type="product"
            AND a.post_status="publish"
            AND (SELECT Count(post_id) FROM wp_favorite where post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_endorsements where post_id=a.ID) > 0
            AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_dyn_review where post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_user_views where  post_id=a.ID) > 0
            AND (SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.ID) > 0
            AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
            )

AND (l.object_id,"'.get_current_user_id().'") NOT IN ( SELECT post_id, user_id FROM wp_user_product_recommendation)
group by l.object_id
');

//end user_product_recommendation


		//insert wp_product_geo_recommendation
		$wpdb->query('INSERT INTO wp_product_geo_recommendation (country,post_id)
            SELECT b.meta_value,a.ID FROM wp_posts a
            inner join wp_postmeta b on a.ID=b.post_id
            and b.meta_key="country"
            and a.post_type="product"
            AND a.post_status="publish"
            AND (SELECT meta_value FROM wp_postmeta where post_id=a.ID and meta_key="total_sales") > 0
            AND (SELECT Count(post_id) FROM wp_favorite where post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_endorsements where post_id=a.ID) > 0
            AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_dyn_review where post_id=a.ID) > 0
            AND (SELECT Count(post_id) FROM wp_user_views where  post_id=a.ID) > 0
            AND (SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.ID) > 0
            AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
            AND (b.meta_value,a.ID) NOT IN ( SELECT country,post_id FROM wp_product_geo_recommendation)
         ');


	}

}


function user_insert_book_hot_recomend()
{
	if (is_user_logged_in()) {
		global $wpdb;
		$reader = new Reader('GeoIP2-Country_20160830/GeoIP2-Country.mmdb');
		if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
			$record = $reader->country($_SERVER['REMOTE_ADDR']);
			//var_dump($record);
			//echo $record["en"];

			if ($record) {
				$continent = $record->continent->name;

//".$reader->country("91.196.38.29")->continent->name."
				// echo '<pre>';
				$continent_select = $wpdb->get_results("
            SELECT
            t.ID as user,
            SUBSTRING_INDEX(SUBSTRING_INDEX(u.meta_value,';',5),':',-1) AS ip
            FROM wp_users t
            inner  join wp_usermeta u on  t.ID=u.user_id
            where u.meta_key='session_tokens' and t.ID=" . get_current_user_id() . "
           ");

				foreach ($continent_select as $val)
					$continent_name = $reader->country(substr($val->ip, 1, -1))->continent->name;


				if ($continent_name == "Africa") {
					$valueccluster = '1';
				} elseif ($continent_name == "Europe") {
					$valueccluster = '2';
				} elseif ($continent_name == "Asia") {
					$valueccluster = '3';
				} elseif ($continent_name == "North America") {
					$valueccluster = '4';
				} elseif ($continent_name == "South America") {
					$valueccluster = '6';
				} elseif ($continent_name == "Antarctica") {
					$valueccluster = '7';
				} elseif ($continent_name == "Australia") {
					$valueccluster = '8';
				}

				$wpdb->query("
            INSERT INTO `wp_hot_user_book_cluster`(`user_id`, `cluster`)
            SELECT t.ID,'" . $valueccluster . "' FROM wp_users t
            where t.ID=$val->user
            and t.ID NOT IN ( SELECT user_id FROM wp_hot_user_book_cluster)
           ");

				$z = $wpdb->query('
            INSERT INTO `wp_hot_book_cluster`(`post_id`, `cluster`)
            SELECT a.ID,k.cluster FROM wp_posts a
            inner join wp_postmeta b on a.ID=b.post_id
            inner join wp_hot_user_book_cluster k on k.user_id=a.post_author
            and b.meta_key="country"
            and a.post_type="dyn_book"
            AND a.post_status="publish"
            AND (SELECT meta_value FROM wp_postmeta where post_id=a.ID and meta_key="popularity_count") > 20
            AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
            AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
            AND (a.ID,k.cluster) NOT IN ( SELECT post_id,cluster FROM wp_hot_book_cluster)
           ');


			}
		}

		//insert wp_user_product_recommendation
		$wpdb->query('INSERT INTO wp_user_book_recommendation (post_id, user_id)
SELECT a.ID,t.ID as user FROM wp_users t
inner join wp_posts a
inner join wp_postmeta b on a.ID=b.post_id
and t.ID='.get_current_user_id().'
and b.meta_value > 0  and b.meta_key="popularity_count"
and a.post_type="dyn_book"
AND a.post_status="publish"
AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
AND b.post_id in (SELECT h.post_id FROM wp_user_views h where  h.post_id=b.post_id and h.user_id='.get_current_user_id().')
AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
AND (a.ID,t.ID) NOT IN ( SELECT post_id, user_id FROM wp_user_book_recommendation)
');

		//tags in user
		$wpdb->query('INSERT INTO wp_user_book_recommendation (post_id, user_id)
SELECT  l.object_id,"'.get_current_user_id().'" FROM  wp_terms
inner join  wp_term_relationships l on l.term_taxonomy_id=term_id
inner join wp_postmeta b on l.object_id=b.post_id
and b.meta_value > 0  and b.meta_key="popularity_count"
            AND (SELECT Count(a.comment_post_id) FROM wp_comments a where a.comment_post_id=l.object_id) > 0
            AND l.object_id NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")

AND name in (
SELECT  s.name  FROM wp_users t
inner join wp_posts a on a.post_author=t.ID
inner join wp_postmeta b on a.ID=b.post_id
inner join  wp_term_relationships n on b.post_id=n.object_id
inner join  wp_terms s on s.term_id=n.term_taxonomy_id
            and t.ID=' . get_current_user_id() . '
            and b.meta_value > 0  and b.meta_key="popularity_count"
            and a.post_type="dyn_book"
            AND a.post_status="publish"
            AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
            AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
            )

AND (l.object_id,"'.get_current_user_id().'") NOT IN ( SELECT post_id, user_id FROM wp_user_book_recommendation)
group by l.object_id
');
 
//end user_product_recommendation


		//insert wp_product_geo_recommendation
		$wpdb->query('INSERT INTO wp_book_geo_recommendation (country,post_id)
            SELECT b.meta_value,a.ID FROM wp_posts a
            inner join wp_postmeta b on a.ID=b.post_id
            and b.meta_key="country"
            and a.post_type="dyn_book"
            AND a.post_status="publish"
            AND (SELECT meta_value FROM wp_postmeta where post_id=a.ID and meta_key="popularity_count") > 0
            AND (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) > 0
            AND a.ID NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"  AND meta_value="private")
            AND (b.meta_value,a.ID) NOT IN ( SELECT country,post_id FROM wp_book_geo_recommendation)
         ');


	}

}
function last_view_recent_action($postid)
{
	global $wpdb;
	$table_name = $wpdb->prefix . 'posts';
    $wpdb->update( 
    	$table_name, 
		       array( 
			      'lastviewfeed' =>  strtotime(current_time('mysql'))
		            ),
				array( 'ID' => $postid ) 
	            ); 
}

function feed_page_more_post_loading($offset=10,$postnumbers=10)
{
		//error_reporting(0);
	$offset = $offset;
$postnumbers = $postnumbers;
	
	global $wpdb;
	$bkstart = !empty($_POST['loadmorefeed'])? $_POST['loadmorefeed']: 0;
	//$bkstart = 0;
	$user_id = get_current_user_id();
	
	$friends_subscribers_qry = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 UNION SELECT c.author_id from '.$wpdb->prefix.'authors_suscribers c WHERE c.suscriber_id = '.$user_id;
	
	
	$friends_subscribers_ids = $wpdb->get_results( $friends_subscribers_qry );
	
	$fs_ids = '';
	
	$count = 0;
	foreach($friends_subscribers_ids as $key=>$value){
		
		if($count == 0)
			$fs_ids .= $value->id;
		else
			$fs_ids .= ','.$value->id;
		
		$count++;	
	}

	if($fs_ids == ''){
		
		//$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
		//$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' AND post_author='$user_id' AND ID NOT IN (SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='privacy-option' AND meta_value='private') ORDER BY post_date DESC LIMIT 50";
        $btquerydafd = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('post','dyn_file','dyn_book','product') AND post_author ='$user_id' AND post_status = 'publish' ORDER BY lastviewfeed DESC,post_date DESC ";
		$btquery = $btquerydafd.' LIMIT  '.$postnumbers." OFFSET ".$offset ;
	}else{
		//$offset = is_numeric($_POST['offset']) ? $_POST['offset'] : 2;
//$postnumbers = is_numeric($_POST['number']) ? $_POST['number'] : 2;
		//~ $btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date LIMIT 50";
		
		//$btquerydafd = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('post','dyn_file','dyn_book','product') AND post_author IN ($fs_ids,$user_id) AND post_status = 'publish' ORDER BY lastviewfeed DESC,post_date DESC ";
		//$btquery = $btquerydafd.' LIMIT  '.$postnumbers." OFFSET ".$offset ;
		/*$btquerydafd = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('post','dyn_file','dyn_book','product') AND post_author IN ($fs_ids,$user_id) AND post_status = 'publish' ORDER BY lastviewfeed DESC,post_date DESC ";
		$btquery = $btquerydafd.' LIMIT  '.$postnumbers." OFFSET ".$offset ;*/
		
		/*$all_acction_query_data = 'SELECT post_id FROM wp_favorite WHERE user_id IN ('. $fs_ids .') UNION SELECT post_id FROM wp_favorite WHERE user_id IN ('. $user_id .')
		  UNION
		  SELECT post_id FROM wp_endorsements WHERE user_id IN ('. $fs_ids .') UNION SELECT post_id FROM wp_endorsements WHERE user_id IN ('. $user_id .')
		  UNION
		  SELECT comment_post_ID as post_id FROM wp_comments WHERE user_id IN ('. $fs_ids .') UNION SELECT comment_post_ID as post_id FROM wp_comments WHERE user_id IN ('. $user_id .')
		  UNION
		  SELECT post_id FROM wp_dyn_review WHERE user_id IN ('. $fs_ids .') UNION SELECT post_id FROM wp_dyn_review WHERE user_id IN ('. $user_id .')';
		 
          $all_acction_query_data_ids = $wpdb->get_results( $all_acction_query_data );		 
		  $acction_id = '';
	
	      $acction_id_count = 0;
	      foreach($all_acction_query_data_ids as $keys=>$values){
		
		     if($acction_id_count == 0)
			  $acction_id .= $values->post_id;
		     else
			 $acction_id .= ','.$values->post_id;
		
		     $acction_id_count++;	
	       }*/
		
		$btquerydafd = "SELECT DISTINCT ID as post_id, post_type
		FROM ".$wpdb->prefix."posts WHERE post_type IN ('post','dyn_file','dyn_book','product') AND post_author IN ($fs_ids,$user_id) AND post_status = 'publish' ORDER BY lastviewfeed DESC, post_date DESC ";
		$btquery = $btquerydafd.' LIMIT  '.$postnumbers." OFFSET ".$offset ;
	}
 $bresult = $wpdb->get_results( $btquery );
$bktotal=$wpdb->query($btquerydafd);
 //$returnsss = feedallcontent();
 //echo  $returnsss;
	return $bresult;
	die();
}
add_action('wp_ajax_feed_page_more_post_loading_feed', 'feed_page_more_post_loading');

function feedallcontent(){
	//error_reporting(0);
			$offset = is_numeric($_POST['offset']) ? $_POST['offset'] : 0;
$postnumbers = is_numeric($_POST['number']) ? $_POST['number'] : 10;
$try = is_numeric($_POST['trys']) ? $_POST['trys'] : 1;
	require_once('dny-feed.php');
	
	return get_feed_data($offset,$postnumbers,$try);
	  wp_die(); 
}
add_action('wp_ajax_feedallcontent_feed', 'feedallcontent');

function book_feed_call($postID){
	$bookshortecode = get_post_meta( $postID, 'bookshortcode', true );
	//$valuetest = '';
	//$bid = strtolower(str_replace(" ", "_", get_the_title($postID)));
			$valuetests =   apply_filters( 'rfbwp_add_book_feed_book', $bookshortecode );
			//if (preg_match('/ERROR:/',$valuetests))
			//{
			// $bid = strtolower(str_replace(" ", "", get_the_title($postID)));	
			//$valuetest =  do_shortcode( '[responsive-flipbook id="'.$bid.'"]' );
			//$valuetest = apply_filters( 'rfbwp_add_book_feed_book', $bid );
			// $variable = do_shortcode($datagfdg)
			 //ob_start();
           //echo do_shortcode($loopContent);
            //   $data = ob_get_clean();
           
			//echo apply_filters( 'rfbwp_add_book_feed', $bid );
			//ob_get_clean();
		//	}
			//else{
			// $valuetest = $valuetests;
			//}
			
	return $valuetests;	
//ob_get_clean();	
}


function feedallcontent_ajax_call(){
	//error_reporting(0);
			$offset = is_numeric($_POST['offset']) ? $_POST['offset'] : 2;
$postnumbers = is_numeric($_POST['number']) ? $_POST['number'] : 2;
$try = is_numeric($_POST['trys']) ? $_POST['trys'] : 1;
	require_once('dny-feed.php');
	
	echo get_feed_data($offset,$postnumbers,$try);
	  wp_die(); 
}
add_action('wp_ajax_feedallcontent_feed_callback', 'feedallcontent_ajax_call');

function feed_playlist_playlerid($offset=0,$postnumbers=2){
	global $wpdb;
	//$bkstart = !empty($_POST['loadmorefeed'])? $_POST['loadmorefeed']: 0;
	//$bkstart = 0;
	$user_id = get_current_user_id();
	
	$friends_subscribers_qry = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 UNION SELECT c.author_id from '.$wpdb->prefix.'authors_suscribers c WHERE c.suscriber_id = '.$user_id;
	
	
	$friends_subscribers_ids = $wpdb->get_results( $friends_subscribers_qry );
	
	$fs_ids = '';
	
	$count = 0;
	foreach($friends_subscribers_ids as $key=>$value){
		
		if($count == 0)
			$fs_ids .= $value->id;
		else
			$fs_ids .= ','.$value->id;
		
		$count++;	
	}
	//$playlist_playerid = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id IN ($fs_ids,$user_id) ");
		$btquerydafd = "SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id IN ($fs_ids,$user_id) ";
		$btquery = $btquerydafd.' LIMIT  '.$postnumbers." OFFSET ".$offset ;
	 $playlist_playerid = $wpdb->get_results( $btquery );
	return $playlist_playerid;
}

function favorite_delete_ajax_call()
{
	global $wpdb;
$post_id = $_POST['favorite_idsss'];
$postautorid = get_post_field( 'post_author', $post_id );
$user_id = get_current_user_id();
if( $user_id == $postautorid)
{
	$wpdb->delete( $wpdb->prefix.'posts', array('post_author'=>$user_id,'ID'=>$post_id));
	echo 'success' ;
}else{
	
 $wpdb->delete( $wpdb->prefix.'favorite', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'success' ;
}
die();
}
add_action('wp_ajax_user_favorite_delete_channel', 'favorite_delete_ajax_call');

function subscribe_channel_from_post_function()
{
 global $wpdb;
 $subscriberid = $_POST['subscriberid'];
 $currentuserid = $_POST['currentuserid']; 
 
 $insert = $wpdb->insert( $wpdb->prefix."authors_suscribers",array(
        		'suscriber_id'	=> $currentuserid,
        		'author_id'		=> $subscriberid,
        		));
 wp_die(); 	
}
add_action('wp_ajax_subscribe_channel_from_post', 'subscribe_channel_from_post_function');

function subscribe_channel_from_post_delete_function()
{
 global $wpdb;
 $subscriberid = $_POST['subscriberid'];
 $currentuserid = $_POST['currentuserid']; 
 
 $delete = $wpdb->delete( $wpdb->prefix."authors_suscribers",array(
        		'suscriber_id'	=> $currentuserid,
        		'author_id'		=> $subscriberid,
        		));
 wp_die(); 	
}
add_action('wp_ajax_subscribe_channel_from_post_delete', 'subscribe_channel_from_post_delete_function');

function playlist_delete_ajax_call()
{
	global $wpdb;
	
$post_id = $_POST['playlist_idsss'];

$user_id = get_current_user_id();

 $wpdb->delete( $wpdb->prefix.'user_playlists', array('user_id'=>$user_id,'id'=>$post_id));
		echo 'success' ;

	die();	
}
add_action('wp_ajax_user_playlist_delete_channel', 'playlist_delete_ajax_call');

function user_notable_delete_channel_ajax_call()
{
	global $wpdb;
$notable_member = $_POST['notable_member'];
//$postautorid = get_post_field( 'post_author', $post_id );
//$user_id = get_current_user_id();

	
 $wpdb->delete( $wpdb->prefix.'notablemember', array('nata_id'=>$notable_member));
		echo 'success' ;
die();

}
add_action('wp_ajax_user_notable_delete_channel', 'user_notable_delete_channel_ajax_call');

add_shortcode('show_ip', 'get_the_user_ip');

function wp_theme_options() {
	global $wp_admin_bar, $wpdb;

	if ( !is_super_admin() || !is_admin_bar_showing() )
		return;
	$theme_options_url = get_admin_url() . 'admin.php?page=vpt_option#_menu_1';


	/* Add the main siteadmin menu item */
	$wp_admin_bar->add_menu( array( 'id' => 'theme_options', 'title' => __( 'Theme Options', 'textdomain' ), 'href' => $theme_options_url ) );
}
add_action( 'admin_bar_menu', 'wp_theme_options', 1000 );
/*-----------------------------------------------------------------------------------*/
/*	Load Includes
/*-----------------------------------------------------------------------------------*/
// Layouts
require_once('layouts/layout-1.php');
require_once('layouts/layout-2.php');
require_once('layouts/layout-3.php');
require_once('layouts/layout-4.php');

// Widgets
require_once('includes/widgets/popular-videos.php');
require_once('includes/widgets/latest-comments.php');
require_once('includes/widgets/ads.php');
require_once('includes/widgets/users.php');


function random_string()
{
	// 8 characters: 7 lower-case alphabets and 1 digit
	$character_set_array = array();
	$character_set_array[] = array('count' => 7, 'characters' => 'abcdefghijklmnopqrstuvwxyz');
	$character_set_array[] = array('count' => 1, 'characters' => '0123456789');
	$temp_array = array();
	foreach ($character_set_array as $character_set) {
		for ($i = 0; $i < $character_set['count']; $i++) {
			$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
		}
	}
	shuffle($temp_array);
	return implode('', $temp_array);
}
//feed endr and fevoriate
function filebookendorsementsssdfshfds()
{
	global $wpdb;
$post_id = $_POST['post_id'];
$user_id = get_current_user_id();

if ($user_id != 0){ // 0 means not logged in

	$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $post_id . ' AND user_id = ' . $user_id);
	
	if (empty($result)){
		$wpdb->insert( 'wp_endorsements', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'endorsed';
	}else{
		$wpdb->delete( 'wp_endorsements', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'deleteend';
	}
}
die();
}
add_action('wp_ajax_filebookendorsementsssss', 'filebookendorsementsssdfshfds');

function filebookfavorite_feed(){
	global $wpdb;

$user_id = get_current_user_id();

if ($user_id != 0){ // 0 means not logged in
	$post_id = $_POST['post_id'];
	$result = $wpdb->get_results('SELECT user_id FROM wp_favorite WHERE post_id = '.$post_id.' AND user_id = '.$user_id);
   // echo $post_id;
	if (empty($result)){
		$wpdb->insert( $wpdb->prefix.'favorite', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'favorite';
	}else{
		$wpdb->delete( $wpdb->prefix.'favorite', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'Un-favorite';
	}
	
}
 die();
}
add_action('wp_ajax_filebookfavoritefeed', 'filebookfavorite_feed');
//end of feed function
add_action('wp_ajax_naz_action', 'naz_action_callback');

function naz_action_callback() {
	global $wpdb; // this is how you get access to the database

	$whatever = intval( $_POST['whatever'] );

	$whatever += 10;

	echo $whatever;

	die(); // this is required to return a proper result
}

/*-----------------------------------------------------------------------------------*/
/*	Load Jquery Files
/*-----------------------------------------------------------------------------------*/
function videopress_theme_scripts() {

	// Load Standard Files
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'JWplayer', get_template_directory_uri() . '/js/jwplayer.js', array());
	wp_enqueue_script( 'Modernizer', get_template_directory_uri() . '/js/modernizr.min.js', array());
	wp_enqueue_script( 'easytabs', get_template_directory_uri() . '/js/easytabs.min.js', array() );
	wp_enqueue_script( 'SlickNav', get_template_directory_uri() . '/js/jquery.slicknav.js', array(), true, true);
	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/script.js', array(), true, true);
	wp_enqueue_script( 'autoplay-video-script', get_template_directory_uri() . '/js/autoplay_video.js', array(), true, true);

	/*
     * custom  btn script
     */

	wp_enqueue_script( 'custom-btn-script', get_template_directory_uri() . '/js/custom-btn.js', array(), true, true);


	// Load Comment Script
	if( is_single() ){ wp_enqueue_script( 'comment-reply' ); }

}
add_action( 'wp_enqueue_scripts', 'videopress_theme_scripts' );






add_action( 'woocommerce_single_product_summary', 'wc_product_sold_count', 11 );
function wc_product_sold_count() {
	global $product;
	$units_sold = get_post_meta( $product->id, 'total_sales', true );
	echo '<span class="units_sold">' . sprintf( __( ' %s', 'woocommerce' ), $units_sold ) . ' Sold</span>';
}




add_action( 'woocommerce_after_shop_loop_item', 'wc_product_rating_overview', 15 );
if ( ! function_exists( 'wc_product_rating_overview' ) ) {
	function wc_product_rating_overview() {
		global $product;
		echo $product->get_rating_html();
	}
}






add_filter( 'default_checkout_country', 'change_default_checkout_country' );
function change_default_checkout_country() {
	return '1';
}










/*-----------------------------------------------------------------------------------*/
/*	Load CSS Files
/*-----------------------------------------------------------------------------------*/
function videopress_theme_styles()  {

	// Load CSS
	wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css', array());
	wp_enqueue_style( 'grids', get_template_directory_uri() . '/css/grid.css', array());
	wp_enqueue_style( 'fontawsome', get_template_directory_uri() . '/css/font-awsome/css/font-awesome.css', array());
	wp_enqueue_style( 'slicknav', get_template_directory_uri() . '/css/slicknav.css', array());
	wp_enqueue_style( 'superfish', get_template_directory_uri() . '/css/superfish.css', array());
	wp_enqueue_style( 'hover-effect', get_template_directory_uri() . '/css/hover-effects.css', array());
	wp_enqueue_style( 'entry', get_template_directory_uri() . '/css/entry.css', array());
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array());

	// Color Scheme
	if( vp_option('vpt_option.color_scheme') == 'blue' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/blue.css', array());
	}elseif( vp_option('vpt_option.color_scheme') == 'orange' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/orange.css', array());
	}elseif( vp_option('vpt_option.color_scheme') == 'purple' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/purple.css', array());
	}elseif( vp_option('vpt_option.color_scheme') == 'green' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/green.css', array());
	}elseif( vp_option('vpt_option.color_scheme') == 'grey' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/gray.css', array());
	}elseif( vp_option('vpt_option.color_scheme') == 'yellow' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/yellow.css', array());
	}elseif( vp_option('vpt_option.color_scheme') == 'black' ){
		wp_enqueue_style( 'color-scheme', get_template_directory_uri() . '/css/colors/black.css', array());
	}

	// Overwrite CSS rules for Buddypress
	wp_enqueue_style( 'buddypress-overwrite', get_template_directory_uri() . '/css/buddypress-overwrite.css', array());

	// Responsive
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array());

}
add_action( 'wp_enqueue_scripts', 'videopress_theme_styles' );




/*-----------------------------------------------------------------------------------*/
/* Start Theme Setup */
/*-----------------------------------------------------------------------------------*/
add_action( 'after_setup_theme', 'videopress_theme_setup' );
function videopress_theme_setup() {

// Widget Hooks
	add_action( 'widgets_init', create_function('', 'return register_widget("videopress_LatestComments");') );
	add_action( 'widgets_init', create_function('', 'return register_widget("videopress_PopularVideos");') );
	add_action( 'widgets_init', create_function('', 'return register_widget("videopress_sidebarads");') );
	add_action( 'widgets_init', create_function('', 'return register_widget("videopress_users");') );

// Wordpress Theme Supports
	if ( ! isset( $content_width ) ) $content_width = 610;
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );

// Register Navigation Menu
	register_nav_menu( 'header_menu','Header Navigation Menu' );
	register_nav_menu( 'top_menu','Top Navigation Menu' );

}
/*-----------------------------------------------------------------------------------*/
/* End Theme Setup */
/*-----------------------------------------------------------------------------------*/



/*...................................................................................*/
/* Excerpt Length */
/*-----------------------------------------------------------------------------------*/
function new_excerpt_length($length) {
	return 15;
}
add_filter('excerpt_length', 'new_excerpt_length');




/*-----------------------------------------------------------------------------------*/
/*	Set Image Sizes
/*-----------------------------------------------------------------------------------*/
add_image_size( 'medium-thumb', 500,270, true );
add_image_size( 'small-thumb', 400,280, true );
add_image_size( 'video-thumb', 690,410, true );
add_image_size( 'blog-post', 610,280, true );



/*-----------------------------------------------------------------------------------*/
/*	Custom Posts
/*-----------------------------------------------------------------------------------*/
// Blog
register_post_type( 'newsblog', /* this can be seen at the URL as a parameter and a unique id for the custom post */
	array(
		'labels' => array(
			'name' => __( 'Blog','textdomain_videopress' ), /* The Label of the custom post */
			'singular_name' => __( "Blog", 'textdomain_videopress' ) /* The Label of the custom post */
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'newsblog'), /* The slug of the custom post */
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ), /* enable basic for text editing */
	)
);



/*-----------------------------------------------------------------------------------*/
/*	Add Sidebars
/*-----------------------------------------------------------------------------------*/


function vdpress_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Chat Widget', 'vdpress' ),
		'id'            => 'sidebar-chat',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'vdpress' ),
		'before_widget' => '<div class="widget-item-group">',
		'after_widget'  => '<div class="clear"></div></div>',
		'before_title'  => '<h6 class="widget-title-group">',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'vdpress_widgets_init' );

/* Homepage Widget */
if ( function_exists('register_sidebar') )
	register_sidebars(1,array(
		'name' => 'Home Widget',
		'before_widget' => '<div class="widget-item">',
		'after_widget'  => '<div class="clear"></div></div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	));

/* Single Page Widget */
if ( function_exists('register_sidebar') )
	register_sidebars(1,array(
		'name' => 'Single Page Widget',
		'before_widget' => '<div class="widget-item">',
		'after_widget'  => '<div class="clear"></div></div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	));

/* Video recommendation sidebar*/
if ( function_exists('register_sidebar') )
	register_sidebars(array(
		'id' => "sidebar-video",
		'name' => 'video recom sidebar',
		'before_widget' => '',//'<div class="widget-item">',
		'after_widget'  => '',//'<div class="clear"></div></div>',
		'before_title'  =>'',// '<h6 class="widget-title">',
		'after_title'   => '',//'</h6>',
	));

/*file recommendation sidebar  */
if ( function_exists('register_sidebar') )
	register_sidebars(array(
		'id' => 'sidebar-file',
		'name' => 'file recom sidebar',
		'before_widget' => '',//'<div class="widget-item">',
		'after_widget'  => '', // '<div class="clear"></div></div>',
		'before_title'  =>'', // '<h6 class="widget-title">',
		'after_title'   =>'', // '</h6>',
	));

// Footer Widget Layout
if( vp_option('vpt_option.widget_columns') == 'style_one' ){
	require_once('includes/footer-columns/style-1.php');
}elseif( vp_option('vpt_option.widget_columns') == 'style_two' ){
	require_once('includes/footer-columns/style-2.php');
}elseif( vp_option('vpt_option.widget_columns') == 'style_three' ){
	require_once('includes/footer-columns/style-3.php');
}elseif( vp_option('vpt_option.widget_columns') == 'one_third' ){
	require_once('includes/footer-columns/one-third.php');
}else{
	require_once('includes/footer-columns/one-fourth.php');
}


/*-----------------------------------------------------------------------------------*/
/*	Add hooks to remove tagcloud font size
/*-----------------------------------------------------------------------------------*/
add_filter('wp_generate_tag_cloud', 'videopress_tagcloud',10,3);

function videopress_tagcloud($tag_string){
	return preg_replace("/style='font-size:.+pt;'/", '', $tag_string);
}


/*-----------------------------------------------------------------------------------*/
/*	Function to Limit words and Filter Tags or elements
/*-----------------------------------------------------------------------------------*/
function videopress_content($videopress_num) {
	$videopress_theContent = get_the_content();
	//$videopress_output = preg_replace('/<a[^>]+./','', $videopress_theContent);
	$videopress_output = preg_replace( '/<a>.*<\/a>/', '', $videopress_output );
	//$videopress_output = preg_replace('/<img[^>]+./','', $videopress_theContent);
	$videopress_output = preg_replace( '/<div>.*<\/div>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<blockquote>.*<\/blockquote>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<h1>.*<\/h1>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<h2>.*<\/h2>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<h3>.*<\/h3>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<h4>.*<\/h4>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<h5>.*<\/h5>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<h6>.*<\/h6>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<span style=".*">.*<\/span>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<table>.*<\/table>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<ul>.*<\/ul>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<li>.*<\/li>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<label for=".*">.*<\/label>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<pre>.*<\/pre>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<dl .*>.*<\/dl>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<blockquote>.*<\/blockquote>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<cite>.*<\/cite>/', '', $videopress_output );
	$videopress_output = preg_replace( '/<a .*>.*<\/a>/', '', $videopress_output );
	$videopress_output = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $videopress_output );
	$videopress_limit = $videopress_num+1;

	$videopress_content = explode(' ', $videopress_output, $videopress_limit);
	array_pop($videopress_content);

	$videopress_content = implode(" ",$videopress_content);
	echo $videopress_content;
}


/*-----------------------------------------------------------------------------------*/
/*	Comments list Function
/*-----------------------------------------------------------------------------------*/

// filter to replace class on reply link
add_filter('comment_reply_link', 'videopress_replace_reply_link_class');
function videopress_replace_reply_link_class($videopress_class){
	$videopress_class = str_replace("class='comment-reply-link", "class='button comment-reply", $videopress_class);
	return $videopress_class;
}

/* Fetch Comments */
function videopress_theme_comment($videopress_comment, $args, $videopress_depth) {
	$GLOBALS['comment'] = $videopress_comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$videopress_tag = 'div';
		$videopress_add_below = 'comment';
	} else {
		$videopress_tag = 'li';
		$videopress_add_below = 'div-comment';
	}
	?>
	<<?php echo $videopress_tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">

	<!-- Start Comment List -->
	<div id="div-comment-<?php comment_ID() ?>">

		<div class="avatar-container">
			<?php if ($args['avatar_size'] != 0) echo get_avatar( $videopress_comment, 60 ); ?>
		</div>

		<div class="comment-header">
			<span><?php printf(__('%s','textdomain_techblog'), get_comment_author_link()) ?></span> &bull; <?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?> <?php edit_comment_link(__('(Edit)','textdomain_techblog'),'  ','' ); ?>
		</div>

		<!-- Comment Text -->
		<div class="comment-text">
			<?php if ($videopress_comment->comment_approved == '0') : ?>
				<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.','textdomain_techblog') ?></em>
				<br /><br />
			<?php endif; ?>

			<?php comment_text() ?>
			<div class="reply-link"><?php comment_reply_link(array_merge( $args, array('add_below' => $videopress_add_below, 'depth' => $videopress_depth, 'max_depth' => $args['max_depth'], 'reply_text' => '<i class="fa fa-mail-reply-all"></i> Reply'))) ?></div>
		</div>
		<!-- End Comment Text -->

	</div>
	<!-- End Comment List -->

<?php }


/*-----------------------------------------------------------------------------------*/
/*	Function for Views Count
/*-----------------------------------------------------------------------------------*/
function videopress_countviews( $postid ){

	$ip = get_user_ip();
	$userid = get_current_user_id();

	global $wpdb;
	$qry = 'SELECT * FROM '.$wpdb->prefix.'user_views WHERE post_id = "'.$postid.'" AND ip = "'.$ip.'" AND user_id = '.$userid;
	$res = $wpdb->get_results($qry);

	$videopress_meta_key = 'popularity_count';
	$videopress_meta_value = get_post_meta( $postid, $videopress_meta_key, true );
	/*---Avoid to increa+se view count for the same person in an ip --*/
	if(empty($res) == true){
		$insert = $wpdb->insert($wpdb->prefix.'user_views',array('post_id'=>$postid,'ip'=>$ip,'user_id'=>$userid));
		update_post_meta( $postid, $videopress_meta_key, $videopress_meta_value+1 );
	}

	/* Determin Weather the topic is hot or not */
	if( $videopress_meta_value == '' ){
		echo 'No Views';
	}else{
		echo number_format($videopress_meta_value) . ' Views';
	}

}

function videopress_countviews_showmore( $postid ){

	$ip = get_user_ip();
	$userid = get_current_user_id();

	global $wpdb;
	$qry = 'SELECT * FROM '.$wpdb->prefix.'user_views WHERE post_id = "'.$postid.'" AND ip = "'.$ip.'" AND user_id = '.$userid;
	$res = $wpdb->get_results($qry);
	$videopress_meta_key = 'popularity_count';
	$videopress_meta_value = get_post_meta( $postid, $videopress_meta_key, true );
	/*---Avoid to increa+se view count for the same person in an ip --*/
	if(empty($res) == true){
		$insert = $wpdb->insert($wpdb->prefix.'user_views',array('post_id'=>$postid,'ip'=>$ip,'user_id'=>$userid));
		update_post_meta( $postid, $videopress_meta_key, $videopress_meta_value+1 );
	}

	/* Determin Weather the topic is hot or not */
	if( $videopress_meta_value == '' || $videopress_meta_value == null){
		return 'No Views';
	}else{
		return number_format($videopress_meta_value) . ' Views';
	}

}

function get_user_ip(){
	if (!empty($_SERVER["HTTP_CLIENT_IP"]))
	{
		//check for ip from share internet
		$ip = $_SERVER["HTTP_CLIENT_IP"];
	}
	elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
	{
		// Check for the Proxy User
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}
	else
	{
		$ip = $_SERVER["REMOTE_ADDR"];
	}

// This will print user's real IP Address
// does't matter if user using proxy or not.
	return $ip;

}


function get_ip_address() {
	$ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
	foreach ($ip_keys as $key) {
		if (array_key_exists($key, $_SERVER) === true) {
			foreach (explode(',', $_SERVER[$key]) as $ip) {
				// trim for safety measures
				$ip = trim($ip);
				// attempt to validate IP
				if (validate_ip($ip)) {
					return $ip;
				}
			}
		}
	}
	return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
	var_dump("expression");	die;
}
/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
	if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
		return false;
	}
	return true;
}


function videopress_displayviews( $postid ){

	$videopress_meta_key = 'popularity_count';
	$videopress_meta_value = get_post_meta( $postid, $videopress_meta_key, true );

	/* Determin Weather the topic is hot or not */
	if( $videopress_meta_value == '' ){
		echo 'No Views';
	}else{
		echo number_format($videopress_meta_value) . ' Views';
	}

}

function save_myvideo_playlist(){
	global $wpdb;
	//global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'playlist_videosmp_song';

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
		id int(9) NOT NULL AUTO_INCREMENT,
		ip varchar(100) NOT NULL,
		playlistid int(30) NOT NULL,
		user_id int(30) NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	$table = $wpdb->prefix."playlist_videosmp_song";

	$data = $wpdb->get_results('SELECT * FROM '.$table.' WHERE playlist_id='.$_POST['videoid']. ' ' );

	if($data){ echo 'Already added To playlist';die(); }

	if(isset($_POST['videoid'])){
		$insert = $wpdb->insert($table,array(
			'ipssss'	=> ip2long($_POST['useripadd']),
			'playlistid'		=> $_POST['videoid'],
			'user_id'  => $_POST['user_id'],
		));
	}

	if($insert)
		echo 'Successfully Added To Playlist';
	die();
}

add_action('wp_ajax_save_myvideo_playlist', 'save_myvideo_playlist');

//end of playlist
function playlistvideourl($video)
{
	global $wpdb;
	$videpresult = $video;
	$getvideoeletent = '';
	//$videpresults = $wpdb->get_results( 'SELECT playlistid FROM '.$wpdb->prefix.'playlist_video_song WHERE user_id = ' . get_current_user_id());
	//$table = $wpdb->prefix."playlist_video_song";
	$video_options = get_post_meta($videpresult, 'video_options', true );
	//

	foreach($video_options as $video_option)
	{
		$relcontent .= $video_option;


	}
	$getvideoeletent = preg_replace('/upload/', '', $relcontent, 1);
	//$videpresultsdfs = $wpdb->get_results('SELECT * FROM '.$table. ' ' );
	/*  foreach($videpresults as $videpresult)
     {
         if($videotext == 1)
         {
            $videpresultid = $videpresult->playlistid;
             $getvideoeletent  = get_the_title($videpresultid) ;
         }
         if($videotext == 2)
         {
            $getvideoeletent .= $videpresult->playlistid . ",";
            //$getvideoeletent = get_the_title($videpresultid);
         }

     } */
	return $getvideoeletent;
}

/*-----------------------------------------------------------------------------------*/
/*	Function for Pagination
/*
/*  Credits goes to Kriesi ( http://www.kriesi.at )
/*-----------------------------------------------------------------------------------*/
function videopress_pagination($pages = '', $range = 2)
{
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '')
	{
		global $wp_query;

		if(is_search()){//FA: make custom for search page

			if(func_num_args()==3)
				$fpgs=func_get_arg(2);
			else
				$fpgs=$wp_query->found_posts;

			//$pages = $wp_query->max_num_pages;

			$pages=ceil($fpgs/12);

			if( isset($_REQUEST["sortvideo"]) )
				$sortvideo=$_REQUEST["sortvideo"];
			else
				$sortvideo="recent";
		}
		else
			$pages = $wp_query->max_num_pages;

		if(!$pages)
		{
			$pages = 1;
		}
	}

	if(1 != $pages)
	{

		if( is_search() && isset($_REQUEST["sortvideo"]) ){//FA: make custom for search page
			echo '<ul class="pagination initial">';
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."&sortvideo=".$sortvideo."' data-sortvideo='".$sortvideo."'>First</a></li>";

			if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."&sortvideo=".$sortvideo."' data-sortvideo='".$sortvideo."'>Prev</a></li>";

			for ($i=1; $i <= $pages; $i++)
			{
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				{
					if( isset($_GET["viewstyle"]) && $_GET["viewstyle"]=="grid" )
						echo ( ($paged * 2)-1 == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'&sortvideo='.$sortvideo.'" data-sortvideo="'.$sortvideo.'">'.$i.'</a></li>';
					else
						echo ($paged == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'&sortvideo='.$sortvideo.'" data-sortvideo="'.$sortvideo.'">'.$i.'</a></li>';
				}
			}

			if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."&sortvideo=".$sortvideo."' data-sortvideo='".$sortvideo."'>Next</a></li>";
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."&sortvideo=".$sortvideo."' data-sortvideo='".$sortvideo."'>Last</a></li>";
			echo "</ul>\n";
		}
		else{
			echo '<ul class="pagination initial">';
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>First</a></li>";

			if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>Prev</a></li>";

			for ($i=1; $i <= $pages; $i++)
			{
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				{
					if( isset($_GET["viewstyle"]) && $_GET["viewstyle"]=="grid" )
						echo ( ($paged * 2)-1 == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'&sortvideo='.$sortvideo.'">'.$i.'</a></li>';
					else
						echo ($paged == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
				}
			}

			if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>Next</a></li>";
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>Last</a></li>";
			echo "</ul>\n";
		}
	}
}




/*-----------------------------------------------------------------------------------*/
// Change Author Slug
/*-----------------------------------------------------------------------------------*/
function change_wp_author_base() {
	global $wp_rewrite;
	$author_slug = 'user'; // change author slug name
	$wp_rewrite->author_base = $author_slug;
}
add_action('init', 'change_wp_author_base');



/*-----------------------------------------------------------------------------------*/
// Add New Fields in user Profile
/*-----------------------------------------------------------------------------------*/
function add_contact_fields($profile_fields) {
	// Adding fields
	$profile_fields['gplus'] = 'Google+';
	$profile_fields['twitter'] = 'Twitter Username';
	$profile_fields['facebook'] = 'Facebook';
	$profile_fields['instagram'] = 'Instagram';
	$profile_fields['linkedin'] = 'LinkedIn';
	$profile_fields['pinterest'] = 'Pinterest';
	$profile_fields['youtube'] = 'Youtube Channel';

	return $profile_fields;
}
// Adding the filter
add_filter('user_contactmethods', 'add_contact_fields');




/*-----------------------------------------------------------------------------------*/
// Custom Player Skin Function
/*-----------------------------------------------------------------------------------*/
function videopress_playerskin(){

	// Player Skin
	if( vp_option('vpt_option.playerskin') == 'default_skin' ){

		// Themed Skin( default )
		if( vp_option('vpt_option.color_scheme') == 'blue' ){
			echo get_template_directory_uri().'/images/skins/theme/blue.xml';
		}elseif( vp_option('vpt_option.color_scheme') == 'green' ){
			echo get_template_directory_uri().'/images/skins/theme/green.xml';
		}elseif( vp_option('vpt_option.color_scheme') == 'grey' ){
			echo get_template_directory_uri().'/images/skins/theme/grey.xml';
		}elseif( vp_option('vpt_option.color_scheme') == 'orange' ){
			echo get_template_directory_uri().'/images/skins/theme/orange.xml';
		}elseif( vp_option('vpt_option.color_scheme') == 'purple' ){
			echo get_template_directory_uri().'/images/skins/theme/purple.xml';
		}elseif( vp_option('vpt_option.color_scheme') == 'red' ){
			echo get_template_directory_uri().'/images/skins/theme/red.xml';
		}elseif( vp_option('vpt_option.color_scheme') == 'yellow' ){
			echo get_template_directory_uri().'/images/skins/theme/yellow.xml';
		} // End if Themed Skin

	}elseif( vp_option('vpt_option.playerskin') == 'skin_one' ){
		echo get_template_directory_uri().'/images/skins/five/five.xml';
	}elseif( vp_option('vpt_option.playerskin') == 'skin_two' ){
		echo get_template_directory_uri().'/images/skins/six/six.xml';
	}elseif( vp_option('vpt_option.playerskin') == 'bekle' ){
		echo 'bekle';
	}elseif( vp_option('vpt_option.playerskin') == 'glow' ){
		echo 'glow';
	}elseif( vp_option('vpt_option.playerskin') == 'beelden' ){
		echo 'beelden';
	}elseif( vp_option('vpt_option.playerskin') == 'stormtrooper' ){
		echo 'stormtrooper';
	}elseif( vp_option('vpt_option.playerskin') == 'vapor' ){
		echo 'vapor';
	}elseif( vp_option('vpt_option.playerskin') == 'roundster' ){
		echo 'roundster';
	} // End If Skin

} // End Function


/*-----------------------------------------------------------------------------------*/
// Custom Function for license key
/*-----------------------------------------------------------------------------------*/
function videopress_license_key(){
	if( vp_option('vpt_option.license_key') != '' ){
		echo '<script type="text/javascript">jwplayer.key="'.vp_option('vpt_option.license_key').'";</script>';
	}

}

/*-----------------------------------------------------------------------------------*/
// Custom Function for Video Player Logo
/*-----------------------------------------------------------------------------------*/
function videopress_player_logo(){
	if( vp_option('vpt_option.license_key') != '' ){

		echo "logo: {
				file: '".vp_option('vpt_option.player_logo')."',
				link: '".vp_option('vpt_option.player_link')."'
			  },";

		if( vp_option('vpt_option.player_about') != '' ){
			echo "abouttext: '".vp_option('vpt_option.player_about')."',";
			echo "aboutlink: '".vp_option('vpt_option.player_about_link')."',";
		}

	}
}

function paginate($reload, $page, $tpages) {
	$adjacents = 2;
	$prevlabel = "&lsaquo; Prev";
	$nextlabel = "Next &rsaquo;";
	$out = "";
	// previous
	if ($page == 1) {
		$out.= "<span>".$prevlabel."</span>\n";
	} elseif ($page == 2) {
		$out.="<li><a class='next' href=\"".$reload."\">".$prevlabel."</a>\n</li>";
	} else {
		$out.="<li><a class='next' href=\"".$reload."?paged=".($page - 1)."\">".$prevlabel."</a>\n</li>";
	}
	$pmin=($page>$adjacents)?($page - $adjacents):1;
	$pmax=($page<($tpages - $adjacents))?($page + $adjacents):$tpages;
	for ($i = $pmin; $i <= $pmax; $i++) {
		if ($i == $page) {
			$out.= "<li class=\"active\"><a href=''>".$i."</a></li>\n";
		} elseif ($i == 1) {
			$out.= "<li><a class='next' href=\"".$reload."\">".$i."</a>\n</li>";
		} else {
			$out.= "<li><a class='next' href=\"".$reload. "?paged=".$i."\">".$i. "</a>\n</li>";
		}
	}

	if ($page<($tpages - $adjacents)) {
		$out.= "<a style='font-size:11px' href=\"" . $reload."?paged=".$tpages."\">" .$tpages."</a>\n";
	}
	// next
	if ($page < $tpages) {
		$out.= "<li><a class='next' href=\"".$reload."?paged=".($page + 1)."\">".$nextlabel."</a>\n</li>";
	} else {
		$out.= "<span style='font-size:11px'>".$nextlabel."</span>\n";
	}
	$out.= "";
	return $out;
}


function dateDiff($date){
	date_default_timezone_set("Asia/Kolkata");
	$mydate= date("Y-m-d H:i:s");

	$datetime1 = date_create($date);
	$datetime2 = date_create($mydate);
	$interval = date_diff($datetime1, $datetime2);

	$min  = $interval->format('%i');
	$sec  = $interval->format('%s');
	$hour = $interval->format('%h');
	$mon  = $interval->format('%m');
	$day  = $interval->format('%d');
	$year = $interval->format('%y');

	if($interval->format('%i%h%d%m%y')=="00000"):
		return $sec." Seconds";
	elseif($interval->format('%h%d%m%y')=="0000"):
		return $min." Minutes";
	elseif($interval->format('%d%m%y')=="000"):
		return $hour." Hours";
	elseif($interval->format('%m%y')=="00"):
		return $day." Days";
	elseif($interval->format('%y')=="0"):
		return $mon." Months";
	else:
		return $year." Years";
	endif;
}


function save_my_playlist(){
	global $wpdb;
	$table = $wpdb->prefix."playlist_songs";

	$data = $wpdb->get_results('SELECT * FROM '.$table.' WHERE playlist_id='.$_POST['plval'].' AND song_id='.$_POST['songid']);

	if($data){ echo 'Already added To playlist';die(); }

	if(isset($_POST['plval'])){
		$insert = $wpdb->insert($table,array(
			'playlist_id'		=> $_POST['plval'],
			'song_id'	=> $_POST['songid'],
		));
	}

	if($insert)
		echo 'Successfully Added To Playlist';
	die();
}

add_action('wp_ajax_save_my_playlist', 'save_my_playlist');
//add_action('wp_ajax_nopriv_save_my_playlist', 'save_my_playlist');

function session_id_playlist(){
	session_start();
	$_SESSION['playlist_id'] =  $_POST['playlist_id'];
	$_SESSION['song_play'] = '';
	die();
}

add_action('wp_ajax_session_id_playlist', 'session_id_playlist');
add_action('wp_ajax_nopriv_session_id_playlist', 'session_id_playlist');

function song_of_playlist(){
	session_start();
	$_SESSION['playlist_id'] =  $_POST['playlist_id'];
	$_SESSION['song_play'] = $_POST['song_play'];
	die();
}

add_action('wp_ajax_song_of_playlist', 'song_of_playlist');
add_action('wp_ajax_nopriv_song_of_playlist', 'song_of_playlist');

function file_download_count(){
	$file_id = $_POST['file_id'];
	global $wpdb;
	$table = $wpdb->prefix."user_files";
	$data = $wpdb->get_row("SELECT * FROM $table WHERE id=$file_id",ARRAY_A);
	$upload_dir = wp_upload_dir();
	//print_r($upload_dir['url']);
	$downloads = $data['downloads']+1;
	$update = $wpdb->update($table,array('downloads' => $downloads),array( 'ID' => $file_id ));
	echo $downloads;
	die();
}

add_action('wp_ajax_file_download_count', 'file_download_count');
add_action('wp_ajax_nopriv_file_download_count', 'file_download_count');

function file_type_save(){

	print_r($_FILES);die('fg');

	if(isset($_FILES) && $_POST['u_f_n']!==''){

		//print_r($_FILES['u_f']['name']);die('hidd');

		$path = $_FILES['u_f']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$exts = array('gif','GIF','png','PNG','jpg','JPG','jpeg','JPEG','doc','DOC','docx','DOCX','pdf','PDF','txt','TXT');

		if(in_array($ext, $exts )){
			$upload_dir = wp_upload_dir();
			//echo '<pre>';print_r($upload_dir['basedir']);echo '</pre>';
			$file = uniqid().'.'.$ext;
			$save_path = $upload_dir['basedir'].'/files/'.$file;
			$download_path = $upload_dir['url'].'/files/'.$file;

			global $wpdb;
			$table = $wpdb->prefix."user_files";
			$insert = $wpdb->insert($table,array(
				'file_name'	=> $_POST['u_f_n'],
				'user_id'	=> get_current_user_id(),
				'file_type'	=> $ext,
				'downloads'	=> '0',
				'file'=>$file
			));
			if($insert){
				$lastid = $wpdb->insert_id;

				move_uploaded_file($_FILES['u_f']['tmp_name'],$save_path);
				$data = array();
				$data['file_active'] = 'active';
				$data['msg_type'] = 'success';
				$data['msg'] = '<div class="alert alert-success">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> File Saved !
				</div>';
				$data['result'] = '';


				//////////////////////////////////////////////
				$data['result'] .= '<tr>';
				$filetype_image = array('png','PNG','jpg','JPG','jpeg','JPEG','gif','GIF');
				$filetype_word = array('doc','DOC','docx','DOCX');
				$filtype_icon = (in_array($ext, $filetype_image))?'file-image-o':
					((in_array($ext, $filetype_word))?'file-word-o':
						(($ext=='txt')?'file-text-o':
							(($ext=='pdf')?'file-pdf-o':'file')));

				$download_link = $upload_dir['url'].'/files/'.$fil->file;
				$data['result'] .= '<td>
							<a class="file-download" href="'. $download_path.'" target="_blank" download data-id="'.$lastid.'">
								<i class="fa fa-'.$filtype_icon.'"></i>
							</a>
						</td>
						<td>
							<a class="file-download" href="'. $download_path.'" target="_blank" download data-id="'.$lastid.'">
								<b>'.$_POST['u_f_n'].'</b>
							</a>
						</td>
						<td class="type">'.$ext.'</td>
						<td class="download-c">0</td>';

				//if($profile_id == get_current_user_id()) {
				$data['result'] .= '<td><a class="u_f_del" href="?delete_file='. $lastid.'"><button type="button" class="btn btn-info">Delete</button></a></td>';
				//}
				$data['result'] .= '</tr>';
				//////////////////////////////////////////////
				$home_active = false;

				echo json_encode($data);
				//echo 'uploaded';
			}
		}
	}
	die();
}

add_action('wp_ajax_file_type_save', 'file_type_save');
add_action('wp_ajax_nopriv_file_type_save', 'file_type_save');

if ( class_exists( 'bbPress' ) ) {
	add_theme_support( 'bbpress' );
}

/* if ( class_exists( 'bbPress' ) ) {
require_once (get_template_directory() . '/bbpress/bbpress-functions.php');
} */

add_filter( 'get_avatar' , 'my_custom_avatar' , 1 , 5 );

function my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
	$user = false;

	if ( is_numeric( $id_or_email ) ) {

		$id = (int) $id_or_email;
		$user = get_user_by( 'id' , $id );

	} elseif ( is_object( $id_or_email ) ) {

		if ( ! empty( $id_or_email->user_id ) ) {
			$id = (int) $id_or_email->user_id;
			$user = get_user_by( 'id' , $id );
		}

	} else {
		$user = get_user_by( 'email', $id_or_email );
	}

	if ( $user && is_object( $user ) ) {
		$user_id2 = $user->data->ID;
		$meta_key2 = 'profile_pic_meta';
		$fileName2 = get_user_meta( $user_id2, $meta_key2, true );
		$fileName3 = site_url().'/profile_pic/'.$fileName2;

		if($fileName2){
			$avatar = "<img alt='#' src='".$fileName3."' class='avatar avatar-size photo' height='35' width='35' />";
		}else{
			$avatar = "<img alt='#' src='http://2.gravatar.com/avatar/2e31f18d72d237a9f4821b52398337ab?s=14&d=mm&r=g' class='avatar avatar-size photo' height='35' width='35' />";
		}
	}

	return $avatar;
}

add_filter( 'bbp_after_get_the_content_parse_args', 'bavotasan_bbpress_upload_media' );
/**
 * Allow upload media in bbPress
 *
 * This function is attached to the 'bbp_after_get_the_content_parse_args' filter hook.
 */
function bavotasan_bbpress_upload_media( $args ) {
	$args['media_buttons'] = true;

	return $args;
}

function upload_video_delete(){
	if(isset($_POST['file_id']) && $_POST['file_id']!=''){
		$profile_id = $_POST['profile_id'];

		if(get_current_user_id() ==  $profile_id){
			$data['msg_type']= 'success';
			$file_id = $_POST['file_id'];
			global $wpdb;
			wp_delete_post($file_id);
			$data['file_id'] = $file_id;
			//die();
		}
		else{
			$data['msg_type']= 'error';
		}

		echo json_encode($data);die();
	}
}

add_action('wp_ajax_upload_video_delete', 'upload_video_delete');
add_action('wp_ajax_nopriv_upload_video_delete', 'upload_video_delete');

function user_playlist_delete(){
	if(isset($_POST['playlist_id']) && $_POST['playlist_id']!=''){
		$playlist_id = $_POST['playlist_id'];
		global $wpdb;
		$table  = $wpdb->prefix.'user_playlists';
		$delete = $wpdb->delete( $table, array( 'id' => $playlist_id ) );
		wp_reset_query();
		$table2  = $wpdb->prefix.'playlist_songs';
		$delete = $wpdb->delete( $table2, array( 'playlist_id' => $playlist_id ) );
	}
}

add_action('wp_ajax_user_playlist_delete', 'user_playlist_delete');
add_action('wp_ajax_nopriv_user_playlist_delete', 'user_playlist_delete');

function user_playsong_delete(){

	if(is_user_logged_in()){
		if(isset($_POST['playlist_id']) && $_POST['playlist_id']!=''){
			$playlist_id = $_POST['playlist_id'];
			$song_id = $_POST['song_id'];

			global $wpdb;
			$table  = $wpdb->prefix.'playlist_songs';
			$delete = $wpdb->delete( $table, array( 'playlist_id' => $playlist_id,'song_id' => $song_id ) );
		}
	}
}

add_action('wp_ajax_user_playsong_delete', 'user_playsong_delete');
add_action('wp_ajax_nopriv_user_playsong_delete', 'user_playsong_delete');

function user_mark_flag(){
	global $wpdb;
	//$recipient = get_option('admin_email');
	$recipient = 'dynation4@gmail.com';
	$subj = 'A user has flagged this post as inappropriate';
	$body = 'Reason For Flag : '.$_POST['reason'].'<br>';
	$body .= 'link For Flag : '.$_POST['url'];

	$array_flags=array(
		'user_ID' =>get_current_user_id(),
		'reason' =>  stripslashes($_POST['reason']),
		'url' => $_POST['url'],
		'flag_date' => date('Y-m-d h:i:s'),
		'flag_ip'=> get_user_ip(),
		'post_id' => $_POST['post_id_flag'],
	);
	$wpdb->insert($wpdb->prefix.'flags',$array_flags);

	wp_mail( $recipient, $subj, $body );
	echo '<p></p><br><h2>&nbsp;&nbsp;Flag request sent !</h2><br><p></p>';exit();
	exit();
}

add_action('wp_ajax_user_mark_flag', 'user_mark_flag');
add_action('wp_ajax_nopriv_user_mark_flag', 'user_mark_flag');

function user_tags_edits(){
	global $wpdb;
	if(isset($_POST['tags_postid'])&& !empty($_POST['tags_postid'])){
		$tags_postid = $_POST['tags_postid'];
		$tagsinput = $_POST['tagsinput'];
		wp_set_post_tags( $tags_postid, $tagsinput , false );
		$settings = rfbwp_get_settings();
		for($i = 0; $i < count($settings['books']); $i++) {
			if($settings['books'][$i]['fbwp_pid']==$tags_postid){
				$settings['books'][$i]['rfbwp_fb_tags']=$tagsinput;
				$wpdb->update( 'wp_options', array("option_value"=> maybe_serialize($settings)), array('option_name'=>'rfbwp_options'));

			}
		}
		// print_r($settings);
	}
	exit();
}

add_action('wp_ajax_user_tags_edits', 'user_tags_edits');
add_action('wp_ajax_nopriv_user_tags_edits', 'user_tags_edits');

function user_category_edits(){
	//$catval = implode(',',$_POST['catval']);
	$catval = $_POST['catval'];
	$post_ID = $_POST['post_id'];
	wp_set_post_categories( $post_ID, $catval, false );
	//wp_set_object_terms( $post_ID, $catval, 'category' );
	exit();
}

add_action('wp_ajax_user_category_edits', 'user_category_edits');
add_action('wp_ajax_nopriv_user_category_edits', 'user_category_edits');

function user_title_edits(){
	$post_title = $_POST['post_title'];
	$post_ID = $_POST['post_id'];

	$my_post = array(
		'ID'           => $post_ID,
		'post_title'   => $post_title,
	);

	wp_update_post( $my_post );
	$settings = rfbwp_get_settings();
	for($i = 0; $i < count($settings['books']); $i++) {
		if($settings['books'][$i]['fbwp_pid']==$post_ID){
			$settings['books'][$i]['rfbwp_fb_name']=$post_title;
			$wpdb->update( 'wp_options', array("option_value"=> maybe_serialize($settings)), array('option_name'=>'rfbwp_options'));

		}
	}
	exit();
}
//function update_flip_book_data($tags_postid,$tagsinput){
//    wp_set_post_tags( $tags_postid, $tagsinput , false );
//                $settings = rfbwp_get_settings();
//                for($i = 0; $i < count($settings['books']); $i++) {
//                    if($settings['books'][$i]['fbwp_pid']==$tags_postid){
//                        $settings['books'][$i]['rfbwp_fb_tags']=$tagsinput;
//                        $wpdb->update( 'wp_options', array("option_value"=> maybe_serialize($settings)), array('option_name'=>'rfbwp_options'));
//
//                    }
//                }
//}

add_action('wp_ajax_user_title_edits', 'user_title_edits');
add_action('wp_ajax_nopriv_user_title_edits', 'user_title_edits');

function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function get_slug_name($no){
	$current_url = current_page_url();
	$slug = explode("/",$current_url);
	return $slug[$no];
}

function get_capability_byUID(){
	global $wpdb;
	$user_ID = get_current_user_id();
	$string_query = $wpdb->get_row("SELECT meta_value FROM ".$wpdb->prefix."usermeta WHERE meta_key='wp_capabilities' AND  user_id=".$user_ID);
	$array_data = unserialize($string_query->meta_value);
	return $array_data;
}

function check_block_pages(){
	$current_pages=get_slug_name(4);
	if(($current_pages == "upload-your-video") || ($current_pages == "channel")){
		$user_capability = get_capability_byUID();
		foreach($user_capability as $key=>$value){
			$list[]= $key;
		}
		return (in_array('bbp_blocked',$list))? true : false;
	}
}

add_filter('relevanssi_modify_wp_query', 'rlv_sort_by_title');
function rlv_sort_by_title($q) {
	$q->set('orderby', 'post_title');
	$q->set('order', 'asc');
	return $q;
}

add_filter('relevanssi_hits_filter','order_the_results');
function order_the_results($hits){
	global $wp_query;

	switch ($wp_query->query_vars['orderby']){
		case 'likes' :
			$likes = array();
			foreach($hits[0] as $hit){
				$likecount = get_post_meta($hit->id,true);
				if(!isset($likes[$likecount]))
					$likes[$likecount] = array();
				array_push($likes[$likecount],$hit);
			}

			if($wp_query->query_vars['order'] == 'asc')
				ksort($likes);
			else
				ksort($likes);

			$sorted_hits = array();
			foreach($likes as $likecount => $year_hits){
				$sorted_hits = array_merge($sorted_hits,$year_hits);
			}
			$hits[0] = $sorted_hits;
			break;

		case 'comments' :
			$comments = array();
			foreach($hits[0] as $hit){
				$commentcount = wp_count_comments($hit->ID)->total_comments;
				if(!isset($comments[$commentcount])) $commentcount[$commentcount] = array();
				array_push($commentcount[$commentcount],$hits);
			}

			if($wp_query->query_vars['order'] == 'asc')
				ksort($comments);
			else
				ksort($comments);

			$sorted_hits = array();
			foreach($comments as $commentcount => $year_hits) {
				$sorted_hits = array_merge($sorted_hits, $year_hits);
			}
			$hits[0] = $sorted_hits;

			break;

		case 'recent' :
			$recents = array();
			break;

		case 'endorsements' :
			$endorsements = array();
			break;

		case 'favourites' :
			$favourites = array();
			break;
	}

	return $hits;
}

function dyn_files_in_query( $query ) {
	if ( $query->is_tag() && $query->is_main_query() ) {
		$query->set( 'post_type', array( 'post', 'dyn_file','dyn_book','product' ) );
	}
}
add_action( 'pre_get_posts', 'dyn_files_in_query' );

/*-----------------------------------------------------------------------------------*/
/*	Custom button by kbihm
/*-----------------------------------------------------------------------------------*/

add_action( 'wp_ajax_endorsement', 'endorsement_handler' );
add_action( 'wp_ajax_nopriv_endorsement', 'endorsement_handler' );

add_action( 'wp_ajax_favorite', 'favorite_handler' );
add_action( 'wp_ajax_nopriv_favorite', 'favorite_handler' );

/*
 * Endorse Btn
 */

function endorsement_btn($post_id,$author_id){
	global $wpdb;
	global $current_user;
	$loginUserID= $current_user->ID;
	$output='';
	$owner='';

	if($loginUserID==$author_id)
	{
		$owner=1;
	}else{
		$owner=0;
	}
	$rs = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' .$post_id . ' AND user_id = ' .$loginUserID);

	if (!empty($rs)) {
		$class = esc_attr( 'endorse_green' );
		$type= esc_attr( 'remove' );
		$title = esc_attr( 'Endorsed!' );
		$text = 'Endorsed!';
	} else {
		$class = esc_attr( 'endorse_black' );
		$title = esc_attr( 'Endorse' );
		$type= esc_attr( 'add' );
		$text = 'Endorse';
	}

	$output = '<a  data-type="'.$type.'" data-owner="'.$owner.'" href="' . admin_url( 'admin-ajax.php').'"
		  data-userId="'.$loginUserID.'"  data-author="'.$author_id.'" id="endorsement_btn_action"
		  class="btn custom_btn btn-default '.$class.'" data-post_id="'.$post_id.'" title="'.$title.'">'.$text.'</a>';
	return $output;

}

/*
* favorite Button
*/

function favorite_btn($post_id,$author_id){
	global $wpdb;
	global $current_user;
	$loginUserID= $current_user->ID;
	$output='';
	$owner='';

	if($loginUserID==$author_id)
	{
		$owner=1;
	}else{
		$owner=0;
	}
	$rs = $wpdb->get_results( 'SELECT user_id FROM `wp_favorite` WHERE post_id = ' .$post_id . ' AND user_id = ' .$loginUserID);

	if (!empty($rs)) {
		$class = esc_attr( 'favorite_green' );
		$type= esc_attr( 'remove' );
		$title = esc_attr( 'Un-Favorite' );
		$text = 'Un-Favorite';
	} else {
		$class = esc_attr( 'favorite_black' );
		$title = esc_attr( 'Favorite' );
		$type= esc_attr( 'add' );
		$text = 'Favorite';
	}

	$output = '<a  data-type="'.$type.'" data-owner="'.$owner.'" href="' . admin_url( 'admin-ajax.php').'"
		  data-userId="'.$loginUserID.'"  data-author="'.$author_id.'" id="favorite_btn_action"
		  class="btn custom_btn btn-default '.$class.'" data-post_id="'.$post_id.'" title="'.$title.'">'.$text.'</a>';
	return $output;

}


function endorsement_handler() {
	// Handle request then generate response using WP_Ajax_Response
	global $wpdb;
	global $current_user;
	$post_id=$_POST['data']['postID'];
	$type=$_POST['data']['type'];
	$user_id=$_POST['data']['userId'];

	if ( $type=="add"){
		$wpdb->insert( 'wp_endorsements', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'endorsed';
	}elseif( $type=="remove"){
		$wpdb->delete( 'wp_endorsements', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'unendorsed';
	}

	exit;
}

function favorite_handler() {
	// Handle request then generate response using WP_Ajax_Response
	global $wpdb;
	global $current_user;
	$post_id=$_POST['data']['postID'];
	$type=$_POST['data']['type'];
	$user_id=$_POST['data']['userId'];

	if ( $type=="add"){
		$wpdb->insert( 'wp_favorite', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'favorite';
	}elseif( $type=="remove"){
		$wpdb->delete( 'wp_favorite', array('user_id'=>$user_id,'post_id'=>$post_id));
		echo 'unfavorite';
	}

	exit;
}

function the_alert($link_video){
	echo '<div class="spacing-40"></div><div class="alert alert-success">
			<span>Video Uploaded successfully. <a href="'.$link_video.'" target="_blank">View Video</a></span>
		 </div>';
}
function notification_subscribers($author_Id,$body){
	global $wpdb;
	$user_ID = get_current_user_id();
	$author  = get_user_by( 'id' , $author_Id );
	$string_query = $wpdb->get_results("SELECT u.ID, u.user_email, u.display_name  FROM
								    ".$wpdb->prefix."users AS u JOIN ".$wpdb->prefix."authors_suscribers AS b ON b.author_id=u.ID
									WHERE u.subscribe='yes' AND b.suscriber_id=".$author_Id);

	require_once ABSPATH . WPINC . '/class-phpmailer.php';
	require_once ABSPATH . WPINC . '/class-smtp.php';
	$phpmailer = new PHPMailer();
	$phpmailer->Host = 'smtp.gmail.com';
	$phpmailer->SMTPAuth = true;
	$phpmailer->Port = 587;
	$phpmailer->SMTPSecure = 'tls';
	$phpmailer->setFrom('dynation4@gmail.com','Do It Yourself Nation');
	$phpmailer->Username = 'theulmotv@gmail.com';
	$phpmailer->Password = 'bismillah!@#';
	$phpmailer->isHTML(true);
	$phpmailer->Subject = $body["subject"];
	$phpmailer->AddAddress( 'dynation4@gmail.com', 'Do It Yourself Nation');

	foreach($string_query as $key=>$value){
		$phpmailer->addBCC($value->user_email, $value->display_name);
	}
	$message = "Hello , <br />".$body["message"];
	$message .= "<hr>";
	$unsubcribelink = home_url()."/unsubscribe_email.php?&Id=".base64_encode($user_ID);
	$message .= "<small><a href='".$unsubcribelink."'>Unsubscribe this notification</a></small>";
	$phpmailer->MsgHTML($message);
	if(!$phpmailer->Send()) {
		echo "Mailer Error: " . $phpmailer->ErrorInfo;
	}

}

function unsubcribe_email($id){
	global $wpdb;
	$table = $wpdb->prefix."users";
	if (is_user_logged_in()){
		$id_=base64_decode($id);
		$update = $wpdb->update($table,array('subscribe' => 'no'),array( 'ID' => $id_ ));
		echo "unsubcribe email notification has successfully";
	}else{
		echo "You have not permission access!";
	}
	wp_redirect( home_url() ); exit;
}

function slug_title($string){
	return strtolower(str_replace(" ","-",$string));
}

function remove_p($string){
	$char_p = array("<p>","</p>");
	return str_replace($char_p,"",$string);
}

add_filter('wpdiscuz_profile_url', 'wpdiscuz_uu_profile_url', 10, 2);
function wpdiscuz_uu_profile_url($profile_url, $user) {
	if ($user && class_exists('XooUserUltra')) {
		global $xoouserultra; $profile_url = $xoouserultra->userpanel->get_user_profile_permalink($user->ID);
	}
	return $profile_url;
}
function disqus_embed($disqus_shortname) {
	global $post;
	wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
	echo '<div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$disqus_shortname.'";
        var disqus_title = "'.$post->post_title.'";
        var disqus_url = "'.get_permalink($post->ID).'";
        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
    </script>';
}

function example_ajax_request() {
	global $post;
	// The $_REQUEST contains all the data sent via ajax
	if ( isset($_REQUEST) ) {

		$fruit = $_REQUEST['postid'];
		//echo $fruit;
		wp_update_post(array(
			'ID'    =>  $fruit,
			'post_status'   =>  'publish'
		));
		//echo 'Your Book has been successfully Submitted';
	}
	// Always die in functions echoing ajax content
	die();
}
add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );

function example_ajax_delete_request() {
	global $post;
	// The $_REQUEST contains all the data sent via ajax
	if ( isset($_REQUEST) ) {
		$fruit = $_REQUEST['postid'];
		//echo $fruit;
		wp_delete_post( $fruit);
		//echo 'Your Book has been successfully deleted';
	}
	// Always die in functions echoing ajax content
	die();
}

add_action( 'wp_ajax_example_ajax_delete_request', 'example_ajax_delete_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_example_ajax_delete_request', 'example_ajax_delete_request' );



add_action( 'add_meta_boxes', 'country_meta_box_add' );
function country_meta_box_add()
{
	$post_types = array ( 'post', 'dyn_book', 'dyn_file' );
	foreach( $post_types as $post_type )
	{
		add_meta_box( 'country_code_id', 'Country code', 'country_meta_box_render', $post_type, 'normal', 'high' );
	}
}

function country_meta_box_render($post)
{
	$values = get_post_custom( $post->ID );
	$ip = get_ip_address();
	$record = geoip_detect2_get_info_from_ip($ip, NULL);
	$code = $record->country->isoCode;
	$text = isset( $values['country_meta_box'] ) ? esc_attr( $values['country_meta_box'][0] ) : $code;
	wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
	?>
	<label for="country_meta_box">Country Code</label>
	<input type="text" name="country_meta_box" id="country_meta_box" value="<?php echo $text; ?>" />
	<?php
}

add_action( 'save_post', 'country_meta_box_save' );
function country_meta_box_save( $post_id )
{
	// Bail if we're doing an auto save
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	// if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;

	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;

	// now we can actually save the data
	$allowed = array(
		'a' => array( // on allow a tags
			'href' => array() // and those anchors can only have href attribute
		)
	);

	// Make sure your data is set before trying to save it
	if( isset( $_POST['country_meta_box'] ) )
		update_post_meta( $post_id, 'country_meta_box', wp_kses( $_POST['country_meta_box'], $allowed ) );
}

function mybackground_add_meta_box() {

	$screens = array('page');

	foreach ( $screens as $screen ) {
		add_meta_box(
			'background_of_box',
			__( 'Background', 'mytheme_textdomain' ),
			'mybackground_meta_box_callback',
			$screen,                                        // Which writing screen ('post','page','dashboard','link','attachment','custom_post_type','comment')
			'side', 'default'
		);

	}
}
add_action( 'add_meta_boxes', 'mybackground_add_meta_box' );

function mybackground_meta_box_callback( $page, $box ) {

	wp_nonce_field( 'mybackground_meta_box_data', 'mybackground_meta_box_nonce' );
	$value = get_post_meta( $page->ID, '_my_background', true );
	wp_enqueue_media();
	?>
	<p>
		<?php if ( isset ( $value ) ) : ?>
			<img src="<?php echo $value ?>" alt="Preview" width="150" height="100" id="background-image-preview">
		<?php endif; ?>
		<input type="hidden" name="background-image" id="background-image" value="<?php echo ( isset ( $value ) ? $value : '' ); ?>" />
		<input type="button" id="background-image-button" class="button" style="margin-bottom: 10px;" value="<?php _e( 'Choose or Upload background', 'mytheme_textdomain' )?>" />
		<input type="button" id="remove-backimage-button" class="button" value="<?php _e( 'Remove background', 'mytheme_textdomain' )?>" />
	</p>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			var background_image_frame;
			var bp = $("#background-image-preview").attr("src");
			if(bp == ""){
				$("#background-image-preview").hide();
				$("#remove-backimage-button").hide();
			}else{
				$("#background-image-preview").show();
				$("#remove-backimage-button").show();
			}
			$('#background-image-button').click(function(e){
				e.preventDefault();
				if ( background_image_frame ) {
					background_image_frame.open();
					return;
				}
				background_image_frame = wp.media.frames.background_image_frame = wp.media({
					title: '<?php _e( 'Background selection', 'mytheme_textdomain' )?>',
					button: { text: '<?php _e( 'Pick background image', 'mytheme_textdomain' )?>' },
					library: { type: 'image' }
				});
				background_image_frame.on('select', function(){
					var media_attachment = background_image_frame.state().get('selection').first().toJSON();
					$('#background-image').val(media_attachment.url);
					$('#background-image-preview').attr('src', media_attachment.url);
				});
				background_image_frame.open();
				$("#background-image-preview").show();
				$("#remove-backimage-button").show();
			});
			$('#remove-backimage-button').click(function(e){
				e.preventDefault();
				$("#background-image-preview").attr("src", "");
				$("#background-image").val("");
				$("#background-image-preview").hide();
				$("#background-image").hide();
				$('#remove-backimage-button').hide();

			});
		});
	</script>
	<?php

}
function mybackground_meta_box_box_data( $page_id, $post )
           {
	if ( ! isset( $_POST['mybackground_meta_box_nonce'] ) ) {
		return;
	}
	if ( ! wp_verify_nonce( $_POST['mybackground_meta_box_nonce'], 'mybackground_meta_box_data' ) ) {
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $page_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $page_id ) ) {
			return;
		}
	}

	if ( ! isset( $_POST['background-image'] ) ) {
		return;
	}
	$my_data = sanitize_text_field( $_POST['background-image'] );
	update_post_meta( $page_id, '_my_background', $my_data );
}
add_action( 'save_post', 'mybackground_meta_box_box_data' );


add_filter( 'redirect_canonical', 'custom_disable_redirect_canonical' );
function custom_disable_redirect_canonical( $redirect_url ) {
	if ( is_paged() && is_singular() ) $redirect_url = false;
	return $redirect_url;
}


//FA: change pagesize for search page
function search_pagesize() {
	global $wp_query;
	if (is_search()) {
		$wp_query->set( 'posts_per_page', 12 );
	}

}

add_action('pre_get_posts','search_pagesize');
/*add_action('admin_init', 'disable_the_plugin');
function disable_the_plugin()
{
	global $current_user;
	if (in_array('adfree_user', $current_user->roles)) {
		deactivate_plugins(
			array(
				'/wp-modal-popup-with-cookie-integration/wp-modal-popup.php'
			),
			true,
			false
		);
	} else { // activate for those than can use it
		activate_plugins(
			array(
				'/wp-modal-popup-with-cookie-integration/wp-modal-popup.php'
			),
			'', // redirect url, does not matter (default is '')
			false, // network wise
			true // silent mode (no activation hooks fired)
		);
	}
}*/



/* add_action( 'gform_subscription_canceled', 'cancelSubscription' );

function cancelSubscription(){

	$user = wp_get_current_user();
	$user->remove_role("adfree_user");
} */


function order_by_comment($postids){

	global $wpdb;


	if(!is_user_logged_in()){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts WHERE ID IN (' . implode(",", array_map("intval", $postids)) . ') AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_status="publish" AND post_type IN ("post", "dyn_file", "dyn_book") ORDER BY comment_count DESC';
	}
	else if( is_user_logged_in() ){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts WHERE ID IN (' . implode(",", array_map("intval", $postids)) . ') AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_status="publish" AND post_type IN ("post", "dyn_file", "dyn_book")
    UNION
    SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_status="publish" AND ID IN (' . implode(",", array_map("intval", $postids)) . ') AND ID IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.get_current_user_id().' AND post_type IN ("post", "dyn_file", "dyn_book")
    UNION
     SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_status="publish" AND ID IN (' . implode(",", array_map("intval", $postids)) . ')  AND ID IN (SELECT post_id
      FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") AND post_type IN ("post", "dyn_file", "dyn_book")
      ORDER BY comment_count DESC';
	}


	return $qry;

}


function order_by_views($postids){

	global $wpdb;

	if(!is_user_logged_in()){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND m.meta_key ="popularity_count" AND p.post_type IN ("post", "dyn_file", "dyn_book") ORDER BY CONVERT( m.meta_value, SIGNED INTEGER ) DESC';
	}
	else if( is_user_logged_in() ){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND m.meta_key ="popularity_count" AND p.post_type IN ("post", "dyn_file", "dyn_book")
    UNION
    SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().'  AND p.post_status="publish" AND m.meta_key ="popularity_count" AND p.post_type IN ("post", "dyn_file", "dyn_book")
    UNION
     SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]")  AND p.post_status="publish" AND m.meta_key ="popularity_count"  AND p.post_type IN ("post", "dyn_file", "dyn_book")
       ORDER BY CONVERT( meta_value, SIGNED INTEGER ) DESC';
	}



	return $qry;

}

function order_by_endorsement($postids){

	global $wpdb;


	if(!is_user_logged_in()){
		$qry = 'SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'endorsements GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book") ORDER BY ecount DESC';
	}

	else if( is_user_logged_in() ){
		$qry = 'SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'endorsements GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
	    UNION
        SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'endorsements GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().' AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
        UNION
        SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'endorsements GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
       ORDER BY ecount DESC';
	}



	return $qry;

}

//FA: functions to set queries and pagination for search.php

function order_by_favourites($postids){

	global $wpdb;


	if(!is_user_logged_in()){
		$qry = 'SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'favorite GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book") ORDER BY ecount DESC';
	}
	else if( is_user_logged_in() ){
		$qry = 'SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'favorite GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
	    UNION
        SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'favorite GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().' AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
        UNION
        SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'favorite GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
         ORDER BY ecount DESC';
	}


	return $qry;
}

function order_by_downloads($postids){

	global $wpdb;


	if(!is_user_logged_in()){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND m.meta_key ="dyn_download_count" AND p.post_type IN ("post", "dyn_file", "dyn_book") ORDER BY CONVERT( m.meta_value, SIGNED INTEGER ) DESC';
	}
	else if( is_user_logged_in() ){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND m.meta_key ="dyn_download_count" AND p.post_type IN ("post", "dyn_file", "dyn_book")
    UNION
    SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().'  AND p.post_status="publish" AND m.meta_key ="dyn_download_count" AND p.post_type IN ("post", "dyn_file", "dyn_book")
    UNION
     SELECT * FROM '.$wpdb->prefix.'posts AS p,'.$wpdb->prefix.'postmeta AS m WHERE p.ID=m.post_id AND p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]")  AND p.post_status="publish" AND m.meta_key ="dyn_download_count" AND p.post_type IN ("post", "dyn_file", "dyn_book")
       ORDER BY CONVERT( meta_value, SIGNED INTEGER ) DESC';
	}


	return $qry;
}

function order_by_reviews($postids){

	global $wpdb;


	if(!is_user_logged_in()){
		$qry = 'SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'dyn_review GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish"  AND p.post_type IN ("post", "dyn_file", "dyn_book") ORDER BY ecount DESC';
	}
	else if( is_user_logged_in() ){
		$qry = 'SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'dyn_review GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
	    UNION
        SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'dyn_review GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().' AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
        UNION
        SELECT p.*, ecount FROM '.$wpdb->prefix.'posts AS p LEFT JOIN (select *,count(*) as ecount from '.$wpdb->prefix.'dyn_review GROUP BY post_id ) AS e ON p.ID=e.post_id WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
         ORDER BY ecount DESC';
	}


	return $qry;

}


function order_by_recent($postids){

	global $wpdb;


	if(!is_user_logged_in()){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts AS p WHERE  p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book") ORDER BY post_date DESC';
	}
	else if( is_user_logged_in() ){
		$qry = 'SELECT * FROM '.$wpdb->prefix.'posts AS p WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID NOT IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
    UNION
    SELECT * FROM '.$wpdb->prefix.'posts AS p WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().'  AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
    UNION
     SELECT * FROM '.$wpdb->prefix.'posts AS p WHERE p.ID IN (' . implode(",", array_map("intval", $postids)) . ') AND p.ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]")  AND p.post_status="publish" AND p.post_type IN ("post", "dyn_file", "dyn_book")
       ORDER BY post_date DESC';
	}

	return $qry;

}


/*-----------------------------------------------------------------------------------*/
/*	Function for GRID Pagination
/*
/*  Credits goes to Kriesi ( http://www.kriesi.at )
/*-----------------------------------------------------------------------------------*/
function videopress_pagination_grid($pages = '', $range = 2)
{
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '')
	{
		global $wp_query;

		if(is_search()){  //FA: make custom for search page

			if(func_num_args()==3)
				$fpgs=func_get_arg(2);
			else
				$fpgs=$wp_query->found_posts;


			$pages=ceil($fpgs/24);


			if( isset($_REQUEST["sortvideo"]) )
				$sortvideo=$_REQUEST["sortvideo"];
			else
				$sortvideo="recent";
		}

		if(!$pages)
		{
			$pages = 1;
		}
	}

	if(1 != $pages)
	{

		if( is_search() && isset($_REQUEST["sortvideo"]) ){//FA: make custom for search page
			echo '<ul class="pagination gridviewstyle initial">';
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."&sortvideo=".$sortvideo."'  data-sortvideo='".$sortvideo."'>First</a></li>";

			if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."&sortvideo=".$sortvideo."'  data-sortvideo='".$sortvideo."'>Prev</a></li>";

			for ($i=1; $i <= $pages; $i++)
			{
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				{
					if( isset($_GET["viewstyle"]) && $_GET["viewstyle"]=="list" )
						echo ( ceil($paged / 2) == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'&sortvideo='.$sortvideo.'" data-sortvideo="'.$sortvideo.'">'.$i.'</a></li>';
					else
						echo ($paged == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'&sortvideo='.$sortvideo.'" data-sortvideo="'.$sortvideo.'">'.$i.'</a></li>';
				}
			}

			if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."&sortvideo=".$sortvideo."' data-sortvideo='".$sortvideo."'>Next</a></li>";
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."&sortvideo=".$sortvideo."' data-sortvideo='".$sortvideo."'>Last</a></li>";
			echo "</ul>\n";
		}
		else{
			echo '<ul class="pagination gridviewstyle initial">';
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>First</a></li>";

			if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>Prev</a></li>";

			for ($i=1; $i <= $pages; $i++)
			{
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				{
					if( isset($_GET["viewstyle"]) && $_GET["viewstyle"]=="list" )
						echo (ceil($paged / 2) == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'&sortvideo='.$sortvideo.'">'.$i.'</a></li>';
					else
						echo ($paged == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
				}
			}

			if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>Next</a></li>";
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>Last</a></li>";
			echo "</ul>\n";
		}

	}
}

/*add_filter('wp_handle_upload_prefilter','tc_handle_upload_prefilter');
function tc_handle_upload_prefilter($file)
{

	$img=getimagesize($file['tmp_name']);
	$minimum = array('width' => '640', 'height' => '480');
	$width= $img[0];
	$height =$img[1];

	if ($img[0] < $minimum['width'] )
		return array("error"=>"Image dimensions are too small. Minimum width is {$minimum['width']}px. Uploaded image width is $width px");

	elseif ($height <  $minimum['height'])
		return array("error"=>"Image dimensions are too small. Minimum height is {$minimum['height']}px. Uploaded image height is $height px");
	else
		return $file;
}
*/

// veena code
/*
function checkIsFriend($chat_user_ID) {
	
	global $wpdb;	
	
	$user_ID = get_current_user_id();
	if($chat_user_ID == ""){
		return false;
	}	
	$query = "
		SELECT *FROM ".$wpdb->prefix."authors_friends_list 
		WHERE (from_friend_id='".$user_ID."' 
		AND to_friend_id='".$chat_user_ID."' 
		AND status=2) 
		OR (from_friend_id='".$chat_user_ID."' 
		AND to_friend_id='".$user_ID."' 
		AND status=2)
	";	
	$result = $wpdb->get_results($query);

	if(count($result) > 0){
		return true;
	}
	
	return false;
}*/

add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );

function keep_me_logged_in_for_1_year( $expirein ) {
    return 31556926; // 1 year in seconds
}


function checkIsFriend($chat_user_ID) {
	global $wpdb;
	$user_ID = get_current_user_id();
	if($chat_user_ID == ""){
		return false;
	}
	$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_friends_list WHERE (from_friend_id='".$user_ID."' AND to_friend_id='".$chat_user_ID."' AND status=2) OR (from_friend_id='".$chat_user_ID."' AND to_friend_id='".$user_ID."' AND status=2) ");

	if(count($result) > 0){
		return true;
	}
	return false;
}


function wc_create_vendor_on_registration( $customer_id, $new_customer_data ) {
	$username = $new_customer_data['user_login'];
	$email    = $new_customer_data['user_email'];

	// Ensure vendor name is unique
	if ( term_exists( $username, 'shop_vendor' ) ) {
		$append     = 1;
		$o_username = $username;

		while ( term_exists( $username, 'shop_vendor' ) ) {
			$username = $o_username . $append;
			$append ++;
		}
	}

	// Create the new vendor
	$return = wp_insert_term(
			$username,
			'shop_vendor',
			array(
					'description' => sprintf( __( 'The vendor %s', 'localization-domain' ), $username ),
					'slug'        => sanitize_title( $username )
			)
	);

	if ( is_wp_error( $return ) ) {
		wc_add_notice( __( '<strong>ERROR</strong>: Unable to create the vendor account for this user. Please contact the administrator to register your account.', 'localization-domain' ), 'error' );
	} else {
		// Update vendor data
		$vendor_data['paypal_email'] = $email; // The email used for the account will be used for the payments
		$vendor_data['commission']   = '50'; // The commission is 50% for each order
		$vendor_data['admins'][]     = $customer_id; // The registered account is also the admin of the vendor

		update_option( 'shop_vendor_' . $return['term_id'], $vendor_data );

		$caps = array(
				"edit_product",
				"read_product",
				"delete_product",
				"edit_products",
				"edit_others_products",
				"delete_products",
				"delete_published_products",
				"delete_others_products",
				"edit_published_products",
				"assign_product_terms",
				"upload_files",
				"manage_bookings",
		);

		$skip_review = get_option( 'woocommerce_product_vendors_skip_review' ) == 'yes' ? true : false;
		if( $skip_review ) {
			$caps[] = 'publish_products';
		}

		$caps = apply_filters( 'product_vendors_admin_caps', $caps );

		$user = new WP_User( $customer_id );
		foreach( $caps as $cap ) {
			$user->add_cap( $cap );
		}
	}
}
add_action( 'woocommerce_created_customer', 'wc_create_vendor_on_registration', 10, 2 );

add_action( 'woocommerce_register_form_start', 'wc_registration_notice_vendors' );
function wc_registration_notice_vendors() {
	echo __( '<p><strong>Warning</strong>: Please register using the same email of your PayPal.com account.</p>', 'localization-domain' );
}


/**
 * Save additional profile fields.
 *
 * @param  int $user_id Current user ID.
 */
 
 add_action( 'user_profile_update_errors', 'validate_extra_info_fields' );
function validate_extra(&$errors, $update = null, &$user  = null)
{

}
 
 
function tm_save_additional_info_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	 return false;
    }
	
	if(isset($_POST['additionalinfog']))
    update_usermeta( $user_id, 'additionalinfog', $_POST['additionalinfog'] );
}

add_action( 'personal_options_update', 'tm_save_additional_info_fields' );
add_action( 'edit_user_profile_update', 'tm_save_additional_info_fields' );


function tm_save_contact_us_info_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	 return false;
    }
	
	if(isset($_POST['additionalinfog']))
	update_usermeta( $user_id, 'cdescriptiong', $_POST['cdescriptiong'] );
	
	if(isset($_POST['additionalinfog']))
    update_usermeta( $user_id, 'conphoneg', json_encode ($_POST['conphoneg']) );
}

add_action( 'personal_options_update', 'tm_save_contact_us_info_fields' );
add_action( 'edit_user_profile_update', 'tm_save_contact_us_info_fields' );


function tm_save_nm_profile_info_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	 return false;
    }
	
	$target_dir = ABSPATH."nm_pic/";

	
	if(isset($_FILES['ne_contact_photo']) && !empty($_FILES['ne_contact_photo']['tmp_name']))
	{
			$target_file =  md5($user_id . time() . $_FILES['ne_contact_photo']['tmp_name'] );
		if(cropPhoto_notable_member($target_dir .$target_file)){
		if(empty($_POST['ne_contact_name']) || empty($_POST['ne_contact_title']) || empty($_POST['ne_contact_link'])){
			$_POST['cust_ne_errors_add'] .= "Notable Member Details Missing.";
		unlink($target_dir .$target_file);
		}
		add_action( 'user_profile_update_errors', 'validate_field_additional_info' );
		$_POST['ne_member_pfdata'][] = array('ne_contact_photo' => $target_file , 'ne_contact_name' => $_POST['ne_contact_name'], 'ne_contact_title' => $_POST['ne_contact_title'], 'ne_contact_link' =>$_POST['ne_contact_link']);
		}else
			add_action( 'user_profile_update_errors', 'validate_field_additional_info' );
	}

	if(isset($_POST['additionalinfog']) && empty($_POST['cust_ne_errors_add'] ))
    update_usermeta( $user_id, 'ne_member_pfdata', json_encode ($_POST['ne_member_pfdata']) );
}

add_action( 'personal_options_update', 'tm_save_nm_profile_info_fields' );
add_action( 'edit_user_profile_update', 'tm_save_nm_profile_info_fields' );



function cropPhoto_notable_member($target_file)
{
	$msg = "";
	$original_size = getimagesize($_FILES["ne_contact_photo"]["tmp_name"]);
			$original_width = $original_size[0];
			$original_height = $original_size[1];
			//$original_height = $original_size[1];
	if($_FILES["ne_contact_photo"]["type"] == "image/gif"){
						$src2 = imagecreatefromgif($_FILES["ne_contact_photo"]["tmp_name"]);
					}elseif($_FILES["ne_contact_photo"]["type"] == "image/jpeg" || $_FILES["ne_contact_photo"]["type"] == "image/pjpeg"){
						$src2 = imagecreatefromjpeg($_FILES["ne_contact_photo"]["tmp_name"]);
					}elseif($_FILES["ne_contact_photo"]["type"] == "image/png"){
						$src2 = imagecreatefrompng($_FILES["ne_contact_photo"]["tmp_name"]);
					}else{
						$_POST['cust_ne_errors_add'] = "There was an error uploading the file. Please upload a .jpg, .gif or .png file. ";
						return false;
					}
					
					$main = imagecreatetruecolor(150,150);
						imagecopyresampled($main,$src2,0, 0, 0, 0,150,150,$original_width,$original_height);
						imagejpeg($main, $target_file, 90);
						
						return true;
}

function validate_field_additional_info(&$errors, $update = null, &$user  = null)
{
	if(!empty($_POST['cust_ne_errors_add']))
        $errors->add('cust_ne_errors_add', "<strong>ERROR</strong>:  ".$_POST['cust_ne_errors_add']);
}

 

// define the bp_core_fetch_avatar callback 
function filter_bp_core_fetch_avatar( $jfb_bp_avatar, $userDetails, $bp_USER ) { 
      		//print_r($userDetails);
			if( "user_has_uploaded_a_local_avatar" ) {
			$meta_key = 'profile_pic_meta';
			$filename = get_user_meta( $userDetails['item_id'], $meta_key);
			if(!empty($filename[0]))
			 $jfb_bp_avatar = "<img alt='{$userDetails['alt']}' src='".site_url()."/profile_pic/".$filename[0] ."' class='avatar user-{$bp_USER}-avatar avatar-{$userDetails['width']} photo' height='{$userDetails['height']}' width='{$userDetails['width']}' />";
			}
        return $jfb_bp_avatar; 
}             
    // add the filter 
    add_filter( 'bp_core_fetch_avatar', 'filter_bp_core_fetch_avatar', 10, 3 ); 


function check_admin_with_role(){	
	if(is_user_logged_in()){
		global $wpdb;
		$adfreeuserid = get_current_user_id();
		$adfreeuser = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."after_subscription WHERE user_id=".$adfreeuserid);
		if($adfreeuser){
			if($adfreeuser->subscription_status =='subscr_signup'){
					$user = wp_get_current_user();
					$user->add_role("adfree_user");
			} else{
				if($adfreeuser->subscription_status =='subscr_cancel'){
					$user = wp_get_current_user();
					$user->remove_role("adfree_user"); 
				}
			}
		}		
	}
}
add_action('admin_init', 'check_admin_with_role', 1 );

 
//require_once('revenue-paypal-section.php');

/* function get-author(){
	$email = $_REQUEST['email'];
	$user = get_user_by( 'email', $email );
	echo 'User is ' . $user->first_name . ' ' . $user->last_name . ' ' . $user->display_name;
}
add_action('wp_ajax_get-author','get-author'); */
?>