<?php 
/* Template Name: Feed Template ghvjhfufui */
get_header(); 

?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/elitevideosource/css/elite.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/elitevideosource/css/elite-font-awesome.css" type="text/css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/elitevideosource/css/jquery.mCustomScrollbar.css" type="text/css">
        
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/froogaloop.js" type="text/javascript"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/jquery.mCustomScrollbar.js" type="text/javascript"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/THREEx.FullScreen.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/videoPlayer.js" type="text/javascript"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/Playlist.js" type="text/javascript"></script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/elitevideosource/js/ZeroClipboard.js"></script>
<style>
.fullmain-container{background-color: #e9ebee;}
.grid-3-4-sidebar.single-page {
    background: #fff;
    border-radius: 2px;
}
 #option-social a.opt-facebook {
    background: #3e5b98;
}
#option-social a.opt-twitter {
    background: #4da7de;
}
#option-social a.opt-googleplus {
    background: #dd4b39;
}
#option-social a.opt-linkedin {
    background: #3371b7;
}
#option-social a.opt-dig {
    background: #1d1d1b;
}
 #option-social a {
    padding: 5px;
    padding-right: 10px;
    padding-left: 10px;
    padding-bottom: 10px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    color: #FFF;
    font-size: 18px;
    text-transform: capitalize;
    margin: 0 10px 10px 0px;
    font-weight: 200;
    text-decoration: none !important;
}
.grid-3-4-sidebar {
    width: 76%;
    float: left;
}
.video-heading.btn-primary {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
    
    left: -46%;
    z-index: unset;
    position: relative;
}

button.button, input.button {
    font-size: 100%;
    margin: 0;
    line-height: 1;
    cursor: pointer;
    position: relative;
    font-family: inherit;
    text-decoration: none;
    overflow: visible;
    padding: .618em 1em;
    font-weight: 700;
    border-radius: 3px;
    left: auto;
    color: #515151;
    background-color: #ebe9eb;
    border: 0;
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
}
 button.button.alt, input.button.alt {
    background-color: #a46497;
    color: #fff;
    -webkit-font-smoothing: antialiased;
}

.form-add .single_add_to_cart_button.button.alt {
    border-radius: 4px;
    display: inline-block;
    line-height: 19px;
    text-align: center;
    height: 34px !important;
    float: left;
}
.quantity{  float: right;}
form.cart {
    margin-bottom: 0%;
}
#product-feed div.thumbnails a {
    float: left;
       width: auto;
    margin-right: 1px;
       margin: 0 auto;
}
#product-feed div.thumbnails a img{
	  width: 100px;
    height: 100px;
}
.woocommerce-main-image img{    max-height: 300px;}
#product-feed span.onsale {
    min-height: 2.236em;
    min-width: 6.236em;
    border-radius: 0px;
    font-weight: bold;
}
#product-feed span.onsale {
    min-height: 3.236em;
    min-width: 3.236em;
    padding: .202em;
    font-weight: 700;
    position: absolute;
    text-align: center;
    line-height: 3.236;
    top: -.5em;
    left: -.5em;
    margin: 0;
    border-radius: 100%;
    background-color: #77a464;
    color: #fff;
    font-size: .857em;
    -webkit-font-smoothing: antialiased;
}
.onsale {
    left: 0px !important;
    top: 0em !important;
    position: relative !important;
    padding: 9px!important;
    padding-left: 25px !important;
    padding-right: 25px !important;
} 
.price del {
    opacity: .5;
}
</style>
	<style>
				.elite_vp_logo{margin-top:6px;}
				.fa-elite-info,.elite_vp_infoBtn,.elite_vp_shareBtn {display:none}
				.fa-elite-share-square-o{display:none}

				#oio-banner-1{
					height:140px;
				}
				.oio-banner-zone .oio-slot a, .oio-banner-zone .oio-slot a:hover, .oio-banner-zone .oio-slot img, .oio-banner-zone .oio-slot object {
					position: absolute !important;
					top: 0 !important;
					left: 0 !important;
					height: 150px !important;
				}

			</style>

    <div class="container" >
		
<?php
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

    $bkstart = ($paged==1) ? 0 : intval($paged-1) * 12;
	
if(!is_user_logged_in()){

	//$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
	wp_redirect(home_url());
}
else if(is_user_logged_in()){
	
	$user_id = get_current_user_id();
	
	$friends_subscribers_qry = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 UNION SELECT c.author_id from '.$wpdb->prefix.'authors_suscribers c WHERE c.suscriber_id = '.$user_id;
	
	
	$friends_subscribers_ids = $wpdb->get_results( $friends_subscribers_qry );
	
	$fs_ids = '';
	
	$count = 0;
	foreach($friends_subscribers_ids as $key=>$value){
		
		if($count == 0)
			$fs_ids .= $value->id;
		else
			$fs_ids .= ','.$value->id;
		
		$count++;	
	}

	if($fs_ids == ''){
		
		//$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
		$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' AND post_author='$user_id' AND ID NOT IN (SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='privacy-option' AND meta_value='private') ORDER BY post_date DESC LIMIT 50";

	}else{
		
		//~ $btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date LIMIT 50";
		
		$btquerydafd = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('post','dyn_file','dyn_book','product') AND post_author IN ($fs_ids,$user_id) AND post_status = 'publish' ORDER BY lastviewfeed DESC,post_date DESC ";
		$btquery = $btquerydafd.' LIMIT  ' . $bkstart .', 150';
	}
	
	
}

$bresult = $wpdb->get_results( $btquery );
$bktotal=$wpdb->query($btquerydafd);
?>
<?php if($bresult){

	foreach ($bresult as $bdata){
		
		if($bdata->post_type == 'dyn_book'){
			$postID = $bdata->post_id; 
			$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
			$type = 'book';
			if(isset($privacyOption[0]) and $privacyOption[0] == "public")
			 {
				//$friendstatus = friend_subs_post($postID,$type);
				//$checkbookstatus = $friendstatus[0];
				//if($checkbookstatus == 1 || $checkbookstatus == '')
				//{
					//show_book_content($bdata); // display book
					endorse_by_feed($postID,$type);
					favorite_by_feed($postID,$type);
					uploaded_by_feed($postID,$type);
					commentedon_by_feed($postID,$type);
					review_by_feed($postID,$type);
					flagged_by_feed($postID,$type);
				//}
			 } else 
			    {
					if($privacyOption[0] == "private")
					{
						 $select_user_status = selected_userlist_feed($postID);	
                        if($select_user_status == 1)
						{
						 //	show_book_content($bdata); // display book
						 endorse_by_feed($postID,$type);
						 favorite_by_feed($postID,$type);
						 uploaded_by_feed($postID,$type);
						 commentedon_by_feed($postID,$type);
					     review_by_feed($postID,$type);
						 flagged_by_feed($postID,$type);
						}
					}
				}
		}
		elseif($bdata->post_type == 'dyn_file'){
			$postID = $bdata->post_id; 
			$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
			$type = 'file';
			if(isset($privacyOption[0]) and $privacyOption[0] == "public")
			 {
				//$friendstatus = friend_subs_post($postID,$type);
				//$checkfilestatus = $friendstatus[0];
				//if($checkfilestatus == 1 || $checkfilestatus == '')
				//{
					endorse_by_feed($postID,$type); // display file
					favorite_by_feed($postID,$type);
					uploaded_by_feed($postID,$type);
					commentedon_by_feed($postID,$type);
					review_by_feed($postID,$type);
					flagged_by_feed($postID,$type);
				//}
			 } else 
			    {
					if($privacyOption[0] == "private")
					{
						$select_user_status = selected_userlist_feed($postID);	
                        if($select_user_status == 1)
						{
						 	endorse_by_feed($postID,$type); // display file
							favorite_by_feed($postID,$type);
							uploaded_by_feed($postID,$type);
							commentedon_by_feed($postID,$type);
					        review_by_feed($postID,$type);
							flagged_by_feed($postID,$type);
						}
					}
				}
		}
		elseif($bdata->post_type == 'post'){
		$postID = $bdata->post_id; 
		$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
			$type = 'video';
			if(isset($privacyOption[0]) and $privacyOption[0] == "public")
			 {
				//$friendstatus = friend_subs_post($postID,$type);
				//$checkvideostatus = $friendstatus[0];
				//if($checkvideostatus == 1 || $checkvideostatus == '')
				//{
					//show_video_feed($bdata); // display file  show_product_feed
					endorse_by_feed($postID,$type);
					favorite_by_feed($postID,$type);
					uploaded_by_feed($postID,$type);
					commentedon_by_feed($postID,$type);
					review_by_feed($postID,$type);
					flagged_by_feed($postID,$type);
				//}
			 } else 
			    {
					if($privacyOption[0] == "private")
					{
						$select_user_status = selected_userlist_feed($postID);	
                        if($select_user_status == 1)
						{
						 	//show_video_feed($bdata); // display file  show_product_feed
							endorse_by_feed($postID,$type);
							favorite_by_feed($postID,$type);
							uploaded_by_feed($postID,$type);
							commentedon_by_feed($postID,$type);
					        review_by_feed($postID,$type);
							flagged_by_feed($postID,$type);
						}
					}
				}
		}
		elseif($bdata->post_type == 'product'){
			$type = 'product';
			$postID = $bdata->post_id; 
			$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
			//if(isset($privacyOption[0]) and $privacyOption[0] == "public")
			// {
				//$friendstatus = friend_subs_post($postID,$type);
				//$checkproductstatus = $friendstatus[0];
				//if($checkproductstatus == 1 || $checkproductstatus == '')
				//{
					//show_product_feed($bdata);
					endorse_by_feed($postID,$type);
					favorite_by_feed($postID,$type);
					uploaded_by_feed($postID,$type);
					commentedon_by_feed($postID,$type);
					review_by_feed($postID,$type);
					flagged_by_feed($postID,$type);
				//}
			// } 
		}

	}
	   
}else{		   
		echo '<div style="width:100%;float:left;" class="main_feed"><div style="width: 60%; background: rgb(242, 242, 242) none repeat scroll 0% 0%; border: 1px solid rgb(225, 225, 225); padding: 10px; margin: 20px auto;" class="box"><p><center>No Recent Activity.</center></p></div></div>';
} ?>

</div></div>
 <?php
 
 function selected_userlist_feed($postID)
{
	 $okay = 0;
	     // $postuserId = get_post_field( 'post_author', $postID );
		 $user_id = get_current_user_id();
	      $selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
          $selectUsersList = explode( ",", $selectUsersList[0] ); 
		   if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
						   $okay = $postuserId;
									 if( in_array($user_id, $selectUsersList) )
									 {
										 $okay = 1;
										
							          }
							   
							   }
							   
		 return $okay;					   
}

 function friend_subs_post($postID, $type='')
 {
	  global $wpdb;
	 $add_play_list_feed = '';
	 $editd_feed = '';
	 $endorse_feed = '';
	 $favorite_feed = '';
	 $donate_feed = '';
	 $falg_feed = '';
	 $review_feed = '';
	 $comment_count_feed = '';
	 $upload_videos_feed = '';
	 //$viewnumber_feed = '';
	 $downloadcount_feed = '';
	 //$posttime_feed = '';
	// $taglist_feed = '';
	 $numberofsale_feed = '';
	 $postuserId = get_post_field( 'post_author', $postID );
	 
	 //$user_id = get_current_user_id();
	
   /* $friends = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'authors_friends_list WHERE `from_friend_id` = '. get_current_user_id().' AND to_friend_id ='. $postuserId .' AND `status` = 2 ORDER BY `from_friend_id` ASC');
	$count_total_friendss = count($friends);
	
	if($count_total_friendss == 0){
		
		$friends = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'authors_friends_list WHERE `to_friend_id` = '. get_current_user_id().' AND from_friend_id ='. $postuserId .' AND `status` = 2 ORDER BY `from_friend_id` ASC');
	
	}*/
	
	 $user_id = get_current_user_id();
    $friends_sql = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 ';
	$count_total_friends = count($friends_sql);
	$friends = $wpdb->get_results( $friends_sql );
	// $friends_subscribers_qry = "SELECT * FROM ".. " WHERE `from_friend_id` = 5 AND `status` = 2 ORDER BY `from_friend_id` ASC';
    // $fs_ids = '';
	$count_total_friends = count($friends);
	 if($count_total_friends != 0)
	 {
		
	    foreach($friends as $value){
			$fs_ids = $value->id;
			if($fs_ids == $postuserId )
			{
				 
				switch($type)
				{
					case 'video':
					       $add_play_list_feed = get_user_meta( $postuserId, '_add_play_listf_feed', true );
						   $editd_feed = get_user_meta( $postuserId, '_editedf_feed', true );
						   $endorse_feed = get_user_meta( $postuserId, '_endorsef_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favoritef_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donatef_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flagf_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_reviewf_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_uploadf_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_commentf_feed', true );
						  // $viewnumber_feed = get_user_meta( $postuserId, '_viewnumberf_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcountf_feed', true );
						  // $posttime_feed = get_user_meta( $postuserId, '_posttimef_feed', true );
						 //  $taglist_feed = get_user_meta( $postuserId, '_taglistf_feed', true );
						   
                     break;
                   case  'file':
				           $add_play_list_feed = get_user_meta( $postuserId, '_add_play_listf_feed', true );
						   $editd_feed = get_user_meta( $postuserId, '_editedf_feed', true );
						  
						   $endorse_feed = get_user_meta( $postuserId, '_endorsef_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favoritef_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donatef_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flagf_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_reviewf_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_uploadf_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_commentf_feed', true );
						  // $viewnumber_feed = get_user_meta( $postuserId, '_viewnumberf_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcountf_feed', true );
						  // $posttime_feed = get_user_meta( $postuserId, '_posttimef_feed', true );
						  // $taglist_feed = get_user_meta( $postuserId, '_taglistf_feed', true );
					break;
					
				   case  'book':
				           $add_play_list_feed = get_user_meta( $postuserId, '_add_play_listf_feed', true );
						   $editd_feed = get_user_meta( $postuserId, '_editedf_feed', true );
						  
						   $endorse_feed = get_user_meta( $postuserId, '_endorsef_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favoritef_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donatef_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flagf_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_reviewf_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_uploadf_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_commentf_feed', true );
						  // $viewnumber_feed = get_user_meta( $postuserId, '_viewnumberf_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcountf_feed', true );
						  // $posttime_feed = get_user_meta( $postuserId, '_posttimef_feed', true );
						  // $taglist_feed = get_user_meta( $postuserId, '_taglistf_feed', true );
					break;
					
				   case  'product':
				           $add_play_list_feed = get_user_meta( $postuserId, '_add_play_listf_feed', true );
						   $editd_feed = get_user_meta( $postuserId, '_editedf_feed', true );
						  
						   $endorse_feed = get_user_meta( $postuserId, '_endorsef_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favoritef_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donatef_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flagf_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_reviewf_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_uploadf_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_commentf_feed', true );
						  // $viewnumber_feed = get_user_meta( $postuserId, '_viewnumberf_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcountf_feed', true );
						   //$posttime_feed = get_user_meta( $postuserId, '_posttimef_feed', true );
						   //$taglist_feed = get_user_meta( $postuserId, '_taglistf_feed', true );
						  
						  $numberofsale_feed = get_user_meta( $postuserId, '_numberofsalef_feed', true );   
					break;
				}
			}
		 
	    }
	 }
	 else{
		 
		 /* $subscriber_feed = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'authors_suscribers WHERE `author_id` = '. get_current_user_id().' AND suscriber_id='. $postuserId .' AND `status` = 1 ORDER BY `suscriber_id` ASC');
	      $count_total_subs = count($subscriber_feed);*/
	       $friends_sql_sub = 'SELECT a.author_id as id from '.$wpdb->prefix.'authors_suscribers a WHERE a.suscriber_id = '.$user_id.' AND a.status = 1 UNION SELECT b.suscriber_id from '.$wpdb->prefix.'authors_suscribers b WHERE b.author_id = '.$user_id.' AND b.status = 1 ';
	      $count_total_subs = count($friends_sql_sub);
	      $subscriber_feed = $wpdb->get_results( $friends_sql_sub );
		  if($count_total_subs != 0)
		  {
			 foreach($subscriber_feed as $key=>$sub_value){
			$sub_ids = $sub_value->suscriber_id;
			if($sub_ids == $postuserId )
			{
				switch($type)
				{
					case 'video':
					       $add_play_list_feed = get_user_meta( $postuserId, '_add_play_list_feed', true );
						   $editd_feed = get_user_meta( $postuserId, '_edited_feed', true );
						   
						   $endorse_feed = get_user_meta( $postuserId, '_endorse_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favorite_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donate_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flag_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_review_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_upload_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_comment_feed', true );
						  // $viewnumber_feed = get_user_meta( $postuserId, '_viewnumber_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcount_feed', true );
						  // $posttime_feed = get_user_meta( $postuserId, '_posttime_feed', true );
						  // $taglist_feed = get_user_meta( $postuserId, '_taglist_feed', true );
						   
                     break;
                   case  'file':
				          $add_play_list_feed = get_user_meta( $postuserId, '_add_play_list_feed', true );
						  $editd_feed = get_user_meta( $postuserId, '_edited_feed', true );
						  
						  $endorse_feed = get_user_meta( $postuserId, '_endorse_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favorite_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donate_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flag_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_review_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_upload_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_comment_feed', true );
						   //$viewnumber_feed = get_user_meta( $postuserId, '_viewnumber_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcount_feed', true );
						   //$posttime_feed = get_user_meta( $postuserId, '_posttime_feed', true );
						   //$taglist_feed = get_user_meta( $postuserId, '_taglist_feed', true );
					break;
					
				   case  'book':
				          $add_play_list_feed = get_user_meta( $postuserId, '_add_play_list_feed', true );
						  $editd_feed = get_user_meta( $postuserId, '_edited_feed', true );
						  
						  $endorse_feed = get_user_meta( $postuserId, '_endorse_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favorite_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donate_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flag_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_review_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_upload_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_comment_feed', true );
						   //$viewnumber_feed = get_user_meta( $postuserId, '_viewnumber_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcount_feed', true );
						  // $posttime_feed = get_user_meta( $postuserId, '_posttime_feed', true );
						  // $taglist_feed = get_user_meta( $postuserId, '_taglist_feed', true );
					break;
					
				   case  'product':
				          $add_play_list_feed = get_user_meta( $postuserId, '_add_play_list_feed', true );
						   $editd_feed = get_user_meta( $postuserId, '_edited_feed', true );
						  
						  $endorse_feed = get_user_meta( $postuserId, '_endorse_feed', true );
						   $favorite_feed = get_user_meta( $postuserId, '_favorite_feed', true );
						   $donate_feed = get_user_meta( $postuserId, '_donate_feed', true );
						   $falg_feed = get_user_meta( $postuserId, '_flag_feed', true );
						   $review_feed = get_user_meta( $postuserId, '_review_feed', true );
						   $upload_videos_feed = get_user_meta( $postuserId, '_upload_feed', true );
						   $comment_count_feed = get_user_meta( $postuserId, '_comment_feed', true );
						 //  $viewnumber_feed = get_user_meta( $postuserId, '_viewnumber_feed', true );
						   $downloadcount_feed = get_user_meta( $postuserId, '_downloadcount_feed', true );
						  // $posttime_feed = get_user_meta( $postuserId, '_posttime_feed', true );
						  // $taglist_feed = get_user_meta( $postuserId, '_taglist_feed', true );
						   
						   $numberofsale_feed = get_user_meta( $postuserId, '_numberofsale_feed', true );   
					break;
				}
			   
			}
		 
	       }  
		 }
		 
	 }
	 
	return array($add_play_list_feed,$endorse_feed,$favorite_feed, $donate_feed,$falg_feed,$review_feed,$upload_videos_feed,$comment_count_feed,$downloadcount_feed,$numberofsale_feed,$editd_feed);
 }
 function friend_subs_post_check($postID)
 {
	 global $wpdb;
	$return_post_check = 'fb'; 
	$postuserId = get_post_field( 'post_author', $postID );
	$user_id = get_current_user_id();
    $friends_sql = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 ';
	$count_total_friends = count($friends_sql);
	$friends = $wpdb->get_results( $friends_sql );
	// $friends_subscribers_qry = "SELECT * FROM ".. " WHERE `from_friend_id` = 5 AND `status` = 2 ORDER BY `from_friend_id` ASC';
    // $fs_ids = '';
	//$count_total_friends = count($friends);
	 if($count_total_friends != 0)
	 {
		
	    foreach($friends as $value){
			$fs_ids = $value->id;
			if($fs_ids == $postuserId )
			{
				$return_post_check = 'fb';
			}
		}
	 } else{
		 
		 /* $subscriber_feed = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'authors_suscribers WHERE `author_id` = '. get_current_user_id().' AND suscriber_id='. $postuserId .' AND `status` = 1 ORDER BY `suscriber_id` ASC');
	      $count_total_subs = count($subscriber_feed);*/
	       $friends_sql_sub = 'SELECT a.author_id as id from '.$wpdb->prefix.'authors_suscribers a WHERE a.suscriber_id = '.$user_id.' AND a.status = 1 UNION SELECT b.suscriber_id from '.$wpdb->prefix.'authors_suscribers b WHERE b.author_id = '.$user_id.' AND b.status = 1 ';
	      $count_total_subs = count($friends_sql_sub);
	      $subscriber_feed = $wpdb->get_results( $friends_sql_sub );
		  if($count_total_subs != 0)
		  {
			 foreach($subscriber_feed as $key=>$sub_value){
			$sub_ids = $sub_value->suscriber_id;
			if($sub_ids == $postuserId )
	           {
				$return_post_check = 'sb';   
			   }
		     }
		   }
		}
	return $return_post_check;	
 }
 function endorse_by_feed($postID,$type)
 {
	global $wpdb;
	//echo $postID ;
	  $endorse_feed = 1;
	  $check_chnnneledit = friend_subs_post_check($postID);
	  $postuserId = get_post_field( 'post_author', $postID );
	  
	   $postby = 'endorsed';
	  $result = $wpdb->get_results( 'SELECT post_id,user_id FROM `wp_endorsements` WHERE post_id = ' . $postID );
	  foreach($result as $value){
		  $postid_feed = $value->post_id;
		  $user_id_feed = $value->user_id;
	  }
	  
	  if($check_chnnneledit == 'fb'){
		   $endorse_feed = get_user_meta( $user_id_feed, '_endorsef_feed', true );
	  }
	  elseif($check_chnnneledit == 'sb')
	  {
		 $endorse_feed = get_user_meta( $user_id_feed, '_endorse_feed', true ); 
	  }
		if($endorse_feed == 1){
			
		 
	 if(!empty($result)){
		 
		 switch($type)
	    {
		 case 'file':
				 show_file( $postid_feed,$postby,$user_id_feed );
				 
			break;
			
		case 'book':
			
				 show_book_content($postid_feed,$postby,$user_id_feed);
			
		    break;
			
		case 'video':
				 show_video_feed($postid_feed,$postby,$user_id_feed);
			
			break;
			
		case 'product':
		
				 show_product_feed($postid_feed,$postby,$user_id_feed);
			
			break;		
	    }
	   }	
	 }  				
 }
 
   function review_by_feed($postID,$type)
 {
	global $wpdb;
	//echo $postID ;
	  $favoritef_feed = 1;
	  $check_chnnneledit = friend_subs_post_check($postID);
	  $postuserId = get_post_field( 'post_author', $postID );
	  $postby = 'reviewed';
	  $result = $wpdb->get_results( 'SELECT post_id,user_id FROM `wp_dyn_review` WHERE post_id = ' . $postID );
	  foreach($result as $value){
		  $postid_feed = $value->post_id;
		  $user_id_feed = $value->user_id;
	  }
	  
	  if($check_chnnneledit == 'fb'){
		   $favoritef_feed = get_user_meta( $user_id_feed, '_reviewf_feed', true );
	  }
	  elseif($check_chnnneledit == 'sb')
	  {
		 $favoritef_feed = get_user_meta( $user_id_feed, '_review_feed', true ); 
	  }
		if($favoritef_feed == 1){
			
		
	 if(!empty($result)){
		 
		 switch($type)
	    {
		 case 'file':
				 show_file( $postid_feed,$postby,$user_id_feed );
				 
			break;
			
		case 'book':
			
				 show_book_content($postid_feed,$postby,$user_id_feed);
			
		    break;
			
		case 'video':
				 show_video_feed($postid_feed,$postby,$user_id_feed);
			
			break;
			
		case 'product':
		
				 show_product_feed($postid_feed,$postby,$user_id_feed);
			
			break;		
	    }
	   }	
	 }  
	    
	 // $postby = 'endorsed';
	//show_file($postID,$postby);				
 }
 
    function flagged_by_feed($postID,$type)
 {
	global $wpdb;
	//echo $postID ;
	  $favoritef_feed = 1;
	  $check_chnnneledit = friend_subs_post_check($postID);
	  $postuserId = get_post_field( 'post_author', $postID );
	  $postby = 'flagged';
	  $result = $wpdb->get_results( 'SELECT post_id,user_ID FROM `wp_flags` WHERE post_id = ' . $postID );
	  foreach($result as $value){
		  $postid_feed = $value->post_id;
		  $user_id_feed = $value->user_ID;
	  }
	  
	  if($check_chnnneledit == 'fb'){
		   $favoritef_feed = get_user_meta( $user_id_feed, '_flagf_feed', true );
	  }
	  elseif($check_chnnneledit == 'sb')
	  {
		 $favoritef_feed = get_user_meta( $user_id_feed, '_flag_feed', true ); 
	  }
		if($favoritef_feed == 1){
			
		
	 if(!empty($result)){
		 
		 switch($type)
	    {
		 case 'file':
				 show_file( $postid_feed,$postby,$user_id_feed );
				 
			break;
			
		case 'book':
			
				 show_book_content($postid_feed,$postby,$user_id_feed);
			
		    break;
			
		case 'video':
				 show_video_feed($postid_feed,$postby,$user_id_feed);
			
			break;
			
		case 'product':
		
				 show_product_feed($postid_feed,$postby,$user_id_feed);
			
			break;		
	    }
	   }	
	 }  
	    
	 // $postby = 'endorsed';
	//show_file($postID,$postby);				
 }
 
  function commentedon_by_feed($postID,$type)
 {
	global $wpdb;
	//echo $postID ;
	  $favoritef_feed = 1;
	  $check_chnnneledit = friend_subs_post_check($postID);
	  $postuserId = get_post_field( 'post_author', $postID );
	  $postby = 'commented on';
	  $result = $wpdb->get_results( 'SELECT comment_post_ID,user_id FROM `wp_comments` WHERE comment_post_ID = ' . $postID );
	  foreach($result as $value){
		  $postid_feed = $value->comment_post_ID;
		  $user_id_feed = $value->user_id;
	  }
	  
	  if($check_chnnneledit == 'fb'){
		   $favoritef_feed = get_user_meta( $user_id_feed, '_commentf_feed', true );
	  }
	  elseif($check_chnnneledit == 'sb')
	  {
		 $favoritef_feed = get_user_meta( $user_id_feed, '_comment_feed', true ); 
	  }
		if($favoritef_feed == 1){
			
		
	 if(!empty($result)){
		 
		 switch($type)
	    {
		 case 'file':
				 show_file( $postid_feed,$postby,$user_id_feed );
				 
			break;
			
		case 'book':
			
				 show_book_content($postid_feed,$postby,$user_id_feed);
			
		    break;
			
		case 'video':
				 show_video_feed($postid_feed,$postby,$user_id_feed);
			
			break;
			
		case 'product':
		
				 show_product_feed($postid_feed,$postby,$user_id_feed);
			
			break;		
	    }
	   }	
	 }  
	    
	 // $postby = 'endorsed';
	//show_file($postID,$postby);				
 }
 
 
  function favorite_by_feed($postID,$type)
 {
	global $wpdb;
	//echo $postID ;
	  $favoritef_feed = 1;
	  $check_chnnneledit = friend_subs_post_check($postID);
	  $postuserId = get_post_field( 'post_author', $postID );
	  $postby = 'favorited';
	  $result = $wpdb->get_results( 'SELECT post_id,user_id FROM `wp_favorite` WHERE post_id = ' . $postID );
	  foreach($result as $value){
		  $postid_feed = $value->post_id;
		  $user_id_feed = $value->user_id;
	  }
	  
	  if($check_chnnneledit == 'fb'){
		   $favoritef_feed = get_user_meta( $user_id_feed, '_favoritef_feed', true );
	  }
	  elseif($check_chnnneledit == 'sb')
	  {
		 $favoritef_feed = get_user_meta( $user_id_feed, '_favorite_feed', true ); 
	  }
		if($favoritef_feed == 1){
			
		
	 if(!empty($result)){
		 
		 switch($type)
	    {
		 case 'file':
				 show_file( $postid_feed,$postby,$user_id_feed );
				 
			break;
			
		case 'book':
			
				 show_book_content($postid_feed,$postby,$user_id_feed);
			
		    break;
			
		case 'video':
				 show_video_feed($postid_feed,$postby,$user_id_feed);
			
			break;
			
		case 'product':
		
				 show_product_feed($postid_feed,$postby,$user_id_feed);
			
			break;		
	    }
	   }	
	 }  
	    
	 // $postby = 'endorsed';
	//show_file($postID,$postby);				
 }
 
 
  function uploaded_by_feed($postID,$type)
 {
	global $wpdb;
	//echo $postID ;
	  $uploadf_feed = 1;
	  $check_chnnneledit = friend_subs_post_check($postID);
	  $postuserId = get_post_field( 'post_author', $postID );
	  $postby = 'uploaded';
	  /*$result = $wpdb->get_results( 'SELECT post_id,user_id FROM `wp_favorite` WHERE post_id = ' . $postID );
	  foreach($result as $value){
		  $postid_feed = $value->post_id;
		  $user_id_feed = $value->user_id;
	  }*/
	  
	  if($check_chnnneledit == 'fb'){
		   $uploadf_feed = get_user_meta( $postuserId, '_uploadf_feed', true );
	  }
	  elseif($check_chnnneledit == 'sb')
	  {
		 $uploadf_feed = get_user_meta( $postuserId, '_upload_feed', true ); 
	  }
		if($uploadf_feed == 1){
			
		
	// if(!empty($result)){
		 
		 switch($type)
	    {
		 case 'file':
				 show_file( $postID,$postby,$postuserId );
				 
			break;
			
		case 'book':
			
				 show_book_content($postID,$postby,$postuserId);
			
		    break;
			
		case 'video':
				 show_video_feed($postID,$postby,$postuserId);
			
			break;
			
		case 'product':
		
				 show_product_feed($postID,$postby,$postuserId);
			
			break;		
	    }
	  // }	
	 }  
	    
	 // $postby = 'endorsed';
	//show_file($postID,$postby);				
 }
 

 ?>
<?php

function show_file($postID,$type,$user_check_id){
	global $wpdb;
	$postby = $type;
	$postID = $postID; 
	$user_action_id = $user_check_id;
	 
				 
		 ?>
		 
<?php
$postId=get_the_ID();
$uploaded_type = get_post_meta( $postID, 'dyn_upload_type', true );
$uploaded_value = get_post_meta( $postID, 'dyn_upload_value', true );
?>
	<style>
		.flag_post
		{
			font-size: 14px !important;
		}
		#endorse_wrapper
		{
			font-size: 14px !important;
		}
		#favorite_wrapper
		{
			font-size: 14px !important;
		}
		.spaceabove {
			margin-top: 4px !important;
		}
		body{    background-color: #e9ebee;}
	</style>
<?php if(!is_user_logged_in()): ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
<?php endif; ?>
<?php



$postdataval = false;
if(isset($_POST['postdataval'.$postID]) && $_POST['postdataval'.$postID]!=''){
	if($_POST['postdataname'.$postID] == 'desc'){
		$postdataval = $_POST['postdataval'.$postID];
		$my_post = array(
			'ID'           => $_POST['postid'.$postID],
			'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'.$postID] == 'refe'){
		update_post_meta($_POST['postid'.$postID], 'video_options_refr', $_POST['postdataval'.$postID]);
	}
}

?>
	<!-- Start Content single-dyn-file -->
	<div class="container p_90">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">
		<!-- Start Entries -->
		<div class="grid-3-4 centre-block">
			<!-- Video part-->
			<div class="grid-3-4-sidebar single-page">
				<?php
				if( isset($_REQUEST['done']) and $_REQUEST['done'] == 1 )
				{
					?>
					<div style="color: blue; font-size: 14pt; font-weight: bold;">
						Thanks for donating the amount.
					</div>
					<?php
				}
				else if( isset($_REQUEST['done']) and $_REQUEST['done'] == 0 )
				{
					?>
					<div style="color: red; font-size: 14pt; font-weight: bold;">
						There is some error in server..Please contact with site administrator
					</div>
					<?php
				}
				else
				{ }
				/* ================================================================== */
				/* Start of Loop */
				/* ================================================================== */
				//while (have_posts()) : the_post();

				// this next bit sets a cookie for related videos
				$first_category = get_the_category();
				$bpost=get_post($postID);
				$title = $post->post_title;
				$post_author     = $bpost->post_author;
				$book_author = get_the_author_meta( 'display_name', $post_author);
	            $userId = get_post_field( 'post_author', $postID );
					$profile_img = get_user_meta( $userId, 'profile_pic_meta', true );
				$authorurl = get_author_posts_url( $userId ); 
				
				$athor_action = get_author_posts_url( $user_action_id ); //$user_action_id
				$profile_img_action = get_user_meta( $user_action_id, 'profile_pic_meta', true );
				$book_author_action = get_the_author_meta( 'display_name', $user_action_id);
				
				$theflContent=$bpost->post_content;
				$titlepo = urlencode(html_entity_decode($title));
				//get_permalink( $postID)
				$pourl = urlencode( get_permalink( $postID) );
		if($profile_img_action){
			//$user_img_src = site_url() . '/profile_pic/' . $profile_img;
			$user_img_src = site_url() . '/profile_pic/' . $profile_img_action;
		}else{
			$user_img_src = get_template_directory_uri().'/images/no-image.png';
		}
			
		//$display_template .= "<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src='". $user_img_src."' class='layout-3-thumb' >";
		
		//$display_template .= "<h4 style='padding-left:10px; color: steelblue;font-size: 15px; font-weight: 600; margin-top:0;margin-bottom:2px;'><span style='color:#222;'>".$book_author." </span><span style='color:#606060;font-weight: 600;'> added</span> <a href='". get_permalink( $postID) ."'> ".$title." </a></h4>";
		
				?>
				<script>
					jQuery(document).ready(function(){
						jQuery.ajax({
							url: "<?php echo get_home_url() ?>/diynation_related_videos_cookie.php",
							type: "POST",
							data: {'category': '<?php echo $first_category[0]->term_id; ?>'}
						}).done(function(data) {
							//console.log("Current category id: " + data);
						});
					});
				</script>
				<?php // end set cookie for related videos ?>


				<?php if($userId == get_current_user_id()){ ?>
				<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" id="title-naz<?php echo $postID; ?>" style='width:80%'><div style="width: auto;float: left;" id="titleafter-edrit<?php echo $postID; ?>"><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby; ?><a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></div>&nbsp;&nbsp;<a href="#" id="title-edit<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i>Edit</a></h6>
				<?php } else {
					?>
					<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" style='width:80%'><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby; ?></span> <a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></h6>
				 
					<?php
				} ?>
				<div id="title-div<?php echo $postID; ?>" style="display:none;">
					<input type="text" name="title-val<?php echo $postID; ?>" value="<?php echo get_the_title($postID);  ?>">
					<input type="hidden" name="post-val<?php echo $postID; ?>" value="<?php echo $postID; ?>">
					<button type="button" class="btn btn-info" id="title-save<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="title-cncl<?php echo $postID; ?>">Cancel</button>
					<br>
				</div>

                <script>
				jQuery(document).ready(function($) {
					$('#title-edit<?php echo $postID; ?>').click(function(){
						$('#title-naz<?php echo $postID; ?>').hide();
						$('#title-div<?php echo $postID; ?>').show();
					});

					$('#title-cncl<?php echo $postID; ?>').click(function(e){
						e.preventDefault();

						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();
					});


					$('#title-save<?php echo $postID; ?>').click(function(e){
						e.preventDefault();


						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();

						var post_title = jQuery('input[name="title-val<?php echo $postID; ?>"]').val();
						var post_id = jQuery('input[name="post-val<?php echo $postID; ?>"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
								$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
							}
						})
					});
					 
				});

			</script>

				<!-- Start Video Player -->
				<?php // get_template_part('includes/file-display'); 
				?>
				<div class="vid-single">

	<div class="file-container">
		<div class="file-logo">
			<a id="gfhjfhgkh" href="<?php echo apply_filters( 'dyn_file_download_link', $postID ); ?>">
				<?php $output = ''; ?>
				<?php echo apply_filters( 'dyn_file_image', $output, $postID ); ?>
			</a>
		</div>
		<div style="width:100%;margin-top:10px;">

			<?php
			$country_id = oiopub_settings::get_user_country_index();

			if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
				$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

			} else {
				$queryyy = " ";
			}
			$tagSelected = wp_get_post_tags($post->ID);
			/*$tagsNameArray = array();
			$tagsSlugArray = array();
			$tagsIDArray = array();
			$tags_str = '';
			foreach($tagSelected as $tag)
			{
				$tags_str .= "'".$tag->name."',";
			}
			$tags_str = substr($tags_str, 0, -1);
						global $wpdb;
			$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
			if($ptags) {
				$ptagname = '';
				$ptagpurchaseid = '';foreach ($ptags as $ptag) {

					$ptagname = $ptag->tag;
					$ptagpurchaseid = $ptag->purchase_id;
				}
				if($ptags)	{$bagfhfa = "AND id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


					global $wpdb;
					$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
					$tagname = '';
					$tagpurchaseid = '';
					foreach ($tags as $tag) {

						$tagname = $tag->tag;
						$tagpurchaseid = $tag->purchase_id;
					}

					if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
}
			}*/
	$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 5 and item_status=1 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


			$bannerurl = '';
			$bannerid = '';
			$countryinbase = '';
			foreach ($banners as $banner) {

				$bannerurl = $banner->item_url;
				$bannerid = $banner->item_id;
			}


			?>
			<?php
			if ($banners) {
				?>	  <a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img id="<?php echo $postID ?>" class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
			<?php }

			?> </div>
		 
	</div>
				<!-- End Video Player -->



				<!-- Start Content -->
				<div class="entry grey-background">

					<p>By <a href="<?php echo $authorurl; ?>">
							<?php echo $book_author; ?>
						</a>
					</p>
					<p>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', $postID ); ?></p>
					<!-- Start Video Heading -->
					<div class="video-heading">

						<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
						 
					<div class="flag_post custom_btn"><span id="addflag-anchor<?php echo $postID; ?>">Flag</span></div>
					 
						<?php
						 
							
						$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID . ' AND user_id = ' . get_current_user_id());
						//print_r($result);
						
						if (empty($result)){
							?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
						<?php }else{ ?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
						<?php }  ?>

						<!-- Section for the favorite button -->
						<?php
						  
						
						$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID . ' AND user_id = ' . get_current_user_id());
						
						if (empty($result)){
							?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
						<?php }else{ ?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
						<?php }  ?>
						<!-- End favorite -->

						<?php
						//Adding a filter for adding Review button
						//This will be added from dyn-review plugin
						 
						
						$output = "";
						echo apply_filters( 'dyn_review_button', $output, $postID, "post" );
						
						 
						?>

						<?php //Start of Section for the Donate Button get_current_user_id()
						 
						
						$return_url = current_page_url()."?done=1";
						$cancel_url = current_page_url()."?done=0";
						$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_current_user_id()." and status=1");
						$paypalacc='';
						if($datapaypal){
							foreach ($datapaypal as $reg_datagen){
								$paypalacc=$reg_datagen->paypal_email;
							}
						}
						if($paypalacc ==''){ ?>
							&nbsp;&nbsp;
							<button type="button" class="btn btn-primary" id="donated<?php echo $postID; ?>" data-toggle="modal" data-target="#model_donate">Donate</button>
							<!--Donate Popup-->
							<div class="modal" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
										</div>
										<div class="modal-body">
											<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="lc" value="US">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<div class="form-group">
													<span for="amount" class="control-label col-sm-4">Amount:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="amount" required >
													</div>
												</div>
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<input type="hidden" name="on1" value="Message" required>
												<div class="form-group">
													<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os1" maxlength="300" size="40">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						<?php } 
						// End Donate Button Section ?>
						<script>
jQuery(document).ready(function($) {
	$('.video-heading').on('click', '#donated<?php echo $postID; ?>', function( e ) {
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not donate Yourself!");
			}else {
		$('div#model_donate').attr('style', 'display:block');
			}
		//$('div#model_donate').removeClass('modal');
	});
	$('.modal-header').on('click', 'button.close', function( e ) {
		$('div#model_donate').attr('style', 'display:none');
		//$('div#model_donate').removeClass('modal');
	});
	
});
</script>

						<div class="clear"></div>
					</div>
					<!-- End Video Heading -->

					<!-- Start Sharer -->
					<div class="clear"></div>

<!-- Modal -->
<div class="modal fade" id="model_flag<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="url<?php echo $postID; ?>" value="<?= get_permalink($postID);?>">
					<textarea rows="10" id="reason<?php echo $postID; ?>" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>

<script>
	var $ = jQuery.noConflict();
	
	/* $(document).ready(function(){
		CKEDITOR.inline( 'reason' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';    
			config.fontSize_defaultLabel = '44px';
		};
	}); */
	
	$('#addflag-submit<?php echo $postID; ?>').live('click',function(e){
		//$('#addflag-form').submit();
		//$("form").serialize()
		var $ = jQuery.noConflict();
		
		var reason = $('#reason<?php echo $postID; ?>').val();
		var url = $('#url<?php echo $postID; ?>').val();
		var post_id_flag = "<?php echo $postID; ?>";
		$.ajax({
			type: "POST",  
			url: "<?php echo admin_url('admin-ajax.php'); ?>",  
			data: {action:'user_mark_flag',reason:reason,url:url,post_id_flag:post_id_fla},
			success : function(data){
				var modelhtml = $('#model_flag<?php echo $postID; ?> .modal-content').html();
				$('#model_flag<?php echo $postID; ?> .modal-content').html(data);
				
				setTimeout(function(){
					$('#model_flag<?php echo $postID; ?>').modal('hide');
					$('#model_flag<?php echo $postID; ?> .modal-content').html(modelhtml);
				},3000)
			}
		});
	
	});
	
	$('#addflag-anchor<?php echo $postID; ?>').click(function(e){
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not flag Yourself.");
			}else {
		var $ = jQuery.noConflict();
		$('#model_flag<?php echo $postID; ?>').modal();
			}
	});
</script>
					<!-- End Sharer -->
					 
					<?php
					$output = "";
					echo apply_filters( 'dyn_display_review', $output, $postID, "post" );
					?>
				 
					<br>
					<div class="panel-group">
						<!-- START About us home page -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#paneldes<?php echo $postID;?>">
									<h4 class="panel-title">Description <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="paneldes<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-des<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="desf-c">
										<?php
										if($postdataval){
											echo $postdataval;
										}
										else{
											if($theflContent==""){
												echo '<div class="content-empty">No Description Provided</div>';
											}else{
												echo $theflContent;
											}
										}

										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelref<?php echo $postID;?>">
									<h4 class="panel-title">Reference <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelref<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-ref<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="ref-c">
										<?php
										$ref = get_post_meta($postID,'video_options_refr',true);
										if($ref){
											echo $ref;
										}else{
											echo 'No referance given';
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelshare<?php echo $postID;?>">
									<h4 class="panel-title">Share <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelshare<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
								<div id="option-social">
								<a class="opt-facebook" href= "https://www.facebook.com/sharer/sharer.php?u=<?php echo $pourl; ?>&t=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-facebook"></span>facebook</a>
								<a class="opt-twitter" href= "http://twitter.com/home?status=<?php echo $titlepo; ?>&nbsp;<?php echo $pourl; ?>" target="_blank"><span class="socicon-twitter"></span>twitter</a>
								<a class="opt-googleplus" href= "https://plus.google.com/share?url=<?php echo $pourl; ?>" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a>
								<a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-linkedin"></span>linkedin</a>
								<a class="opt-dig" href= "http://digg.com/submit?url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-digg"></span>dig</a>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END About us home page -->
				</div>
				<script>
					var $ = jQuery.noConflict();
					$(document).ready(function(){
						var $ = jQuery.noConflict();

						$(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');

						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("hide.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');
						});
						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("show.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-up"></span>');
						});
												$('#edit-des<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();

			e.preventDefault();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Description');
			$('#postdataname<?php echo $postID; ?>').val('desc');

			$('.cke_textarea_inline').html($('#desf-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#edit-ref<?php echo $postID; ?>').click(function(e){
			e.preventDefault();

			var $ = jQuery.noConflict();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Reference');
			$('#postdataname<?php echo $postID; ?>').val('refe');

			$('.cke_textarea_inline').html($('#ref-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#desref-submit<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();
			$('#desref-form<?php echo $postID; ?>').submit();
		});

		$('#endorse_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookendorsementsssss',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								if (data.match(/endorsed/g)){
									//alert(data);
					$('#endorse_button<?php echo $postID; ?>').html('<span>Endorsed!</span>');
					$('#endorse_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
				if (data.match(/deleteend/g)){
					jQuery('#endorse_button<?php echo $postID; ?>').html('<span>Endorse</span>');
					jQuery('#endorse_button<?php echo $postID; ?>').css('background-color','#000');
				}
							}
						})
		     }			
			  
		});
		
		$('#favorite_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not favorite Yourself!");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookfavoritefeed',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								  // alert(data);
								if (data.match(/favorite/g)){
									//alert(data);
									jQuery('#favorite_button<?php echo $postID; ?>').html('<span>Favorite</span>');
					jQuery('#favorite_button<?php echo $postID; ?>').css('background-color','#ff8a8a');
				
				}
				if (data.match(/nsotsafev/g)){
						$('#favorite_button<?php echo $postID; ?>').html('<span>Un-favorite</span>');
					$('#favorite_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
							}
						})
			}
			  
		});
		
		$('#tags-edit<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		$('#tags-val<?php echo $postID; ?>').hide();
		$('.tags-forminput<?php echo $postID; ?>').show();
	});
	$('#tagcancel<?php echo $postID; ?>').click(function(e){
				e.preventDefault();
				$('.tags-forminput<?php echo $postID; ?>').hide();
			});
	$('#tagsave<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		
		var tags_postid = $('#tags_postid<?php echo $postID; ?>').val();
		var tagsinput = $('#tagsinput<?php echo $postID; ?>').val();
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
			}).done(function(data) {
				location.reload();
		});
		
		$('.tags-forminput<?php echo $postID; ?>').hide();
		$('#tags-val<?php echo $postID; ?>').show();
		
	});

					})
				</script>

				<script>
					var $ = jQuery.noConflict();

					$('.ref-more').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.ref-less').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});

					$('.des-more').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.des-less').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});
				</script>

				<div class="clear"></div>
			</div>
			<!-- End Content -->
			<!-- Start Content -->
			<ul class="stats sicon grey-background">
		 
				<li><i class="fa fa-eye"></i> <?php videopress_countviews( $postID);  ?></li>
			 
			 
				<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID);
					echo count($result); ?> Endorsments</li>
			 	
				<!--favourite section -->
				 
				<li><i class="fa  fa-heart"></i>
					<?php
					$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID;
					$result1 = $wpdb->get_results($sql_aux);
					echo count($result1);
					?> Favorites</li>
			 
				<!--end favorite-->
		 
				<li><i class="fa fa-comments"></i> <?php echo get_comments_number( $postID ) ;?> Comment</li>
			 
				 
				<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U',$postID), current_time('timestamp') ) . ' ago'; ?></li>
			 
				 
				<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', $postID, "post"); ?></li>
				 
			 
				<li><i class="fa fa-download"></i><?php echo apply_filters( 'dyn_number_of_download', $postID); ?></li>
				 
			</ul>
			<div class="clear"></div>
           		 
			<?php if( vp_option('vpt_option.show_user') == '1' ){ ?>
				<div class="post-by">
				
					<a href="<?php echo get_author_posts_url( $post_author ); ?>">
					<img src="<?php echo $user_img_src; ?>" class="avatar user-37-avatar avatar-50 photo" width="50" height="50" alt="P"></a>
					<a href="<?php echo get_author_posts_url( $post_author ); ?>" class="post-by-link"><?php echo $book_author; ?></a>
					<?php echo '<div class="post-by-vid-count">'.count_user_posts( $post_author ).' Videos Uploaded</div>'; ?>
					<div class="clear"></div>
				</div>
				<?php }  ?>

			<?php
			// Link Pages Navigation
			$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
			wp_link_pages( $args );


			// Display the Tags
			$tagval = '';?>
			<?php $all_tags = array();?>
			<?php $all_tags = get_the_tags($postID);?>
			<?php if($all_tags){
				$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
			}

			if($userId == get_current_user_id()){
				$before1 = '<span><i class="fa fa-tags"></i>Tags';
				$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit'. $postID .'"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
			}
			$before = '<span id="tags-val">';

			$after = '</span>';


			?>
          	 
			<?php if( is_user_logged_in() ){
				$profile_id = get_current_user_id();
				$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =2  ORDER BY id DESC");
				if($data){ ?>
					<div class="addto_playlist grey-background padding5_10">
						<a herf="#" id="fileplaytest<?php echo $postID; ?>" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
					</div>
			<?php } }  ?>
			<div class="spacer-20"></div><div class="post-tags grey-background padding5_10">
				<?= $before1;?>
				<?php
				$tagSelected = wp_get_post_tags($postID);
				$tagsNameArray = array();
				$tagsSlugArray = array();
				$tagsIDArray = array();
				foreach($tagSelected as $key=>$val)
				{
					$name = $val->name;
					$slug = $val->slug;
					$term_id = $val->term_id;
					$tagsNameArray[] = $name;
					$tagsSlugArray[] = $slug;
					$tagsIDArray[] = $term_id;
				}
				echo get_the_tags( $before,', ',$after );
$tags = get_the_tags($postID); 				?>
				
		 
				<span id="tags-val<?php echo $postID; ?>">

 
      <?php foreach($tags as $tag) :  ?>
 
    
        <a rel="tag"
            href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>">
                  <?php print_r($tag->name); ?>
         </a>   ,

      <?php endforeach; ?>
</span>
		 
			</div>


			<?php

			if($all_tags){
				foreach($all_tags as $tag){
					$tagval .= $tag->name.',';
				};
			}

			rtrim($tagval, ",");

			if($userId == get_current_user_id()){
				?>

				<div class="tags-forminput<?php echo $postID; ?>" style="display:none;">

					<input name="tagsinput" class="tagsinput" id="tagsinput<?php echo $postID; ?>" value="<?= $tagval;?>">
					<input name="userid" type="hidden" id="tags_postid<?php echo $postID; ?>" value="<?= $postID ;?>">
					<button type="button" class="btn btn-success" id="tagsave<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="tagcancel<?php echo $postID; ?>">Cancel</button>
 
				</div>

			<?php } ?>

			<script>
				jQuery(document).ready(function(){
					jQuery('#songids').val('<?php echo $postID;?>')
				});

			</script>
			<!--
    <div class="post-categories">
    <span>
		<i class="fa fa-folder"></i>Categories&nbsp;&nbsp;
		<?php if(get_the_author_id() == get_current_user_id()){ ?>
			<a href="#" id="category-edit"><i class="fa fa-pencil-square-o"></i>Edit</a>
		<?php } ?>
	</span>

	<div id="cat-div">
	    <?php
			$postCats = wp_get_post_categories($postID);
			the_category('&nbsp;,&nbsp;');?>
	</div>

	<div id="cat-editdiv" style="display:none;">

	<?php

			$allcate = get_the_category();
			$post_catar = array();
			foreach($allcate as $cate){
				$post_catar[] = $cate->cat_ID;
			}

			//print_r($post_catar);

			// //$args = array(
			// 	'type'                     => 'post',
			// 	//'child_of'                 => 0,
			// 	//'parent'                   => '',
			// 	'orderby'                  => 'name',
			// 	'order'                    => 'ASC',
			// 	'hide_empty'               => 0,
			// 	'hierarchical'             => 1,
			// 	'exclude'                  => array('1','23'),
			// 	'include'                  => '',
			// 	'number'                   => '',
			// 	'taxonomy'                 => 'category',
			// 	//'pad_counts'               => false

			// );

			$categories = get_categories( $args );

			?>


		<ul>
			<?php foreach($categories as $cat ){ ?>
				<li>
					<input type="checkbox" name="cat_c[]" value="<?= $cat->cat_ID; ?>" <?= (in_array($cat->cat_ID, $post_catar))? 'checked':'';?>>
					<?= $cat->name; ?>
				</li>
			<?php } ?>
		</ul>
		<input type="hidden" name="post_id" value="<?php the_ID(); ?>">

		<button type="button" class="btn btn-info" id="cat-save">Save</button>
		<button type="button" class="btn btn-primary" id="cat-cncl">Cancel</button>
	</div>

    </div>

    -->

			<div class="clear"></div>
			<!-- End Content -->
           
			<?php if(function_exists("display_saic_feeds")) { echo display_saic_feeds($postID);} ?>
			 
			<!-- Start Related Videos -->
			<div class="spacing-40"></div>
			<?php //get_template_part('includes/related-videos'); ?>
			<!-- End Related Videos -->


			<?php
			/* ================================================================== */
			/* End of Loop */
			/* ================================================================== */
			//endwhile;
			?>


		</div>
		<!-- End Video Part -->

		

		
	</div>
	<!-- End Entries -->


	<!-- Widgets -->
	<!--<?php //get_sidebar(); ?>--
    <!-- End Widgets -->

	<div class="clear"></div>
	</div>
	<!--
    <div class="container p_90">
        <div class="grid-3-4 centre-block">
        <div class="grid-3-4-sidebar single-page">

        </div>
        </div>
    </div>
    -->
	<div class="spacing-40"></div>
	<!-- End Content -->
 

	<!-- Modal -->
	<div class="modal fade" id="modeldesref_section<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel<?php echo $postID; ?>">Add Description</h4>
				</div>
				<div class="modal-body">
					<form method="post" id="desref-form<?php echo $postID; ?>">
						<input type="hidden" name="postdataname<?php echo $postID; ?>" id="postdataname<?php echo $postID; ?>">
						<input type="hidden" name="postid<?php echo $postID; ?>" id="postid<?php echo $postID; ?>" value="<?= $postID;?>">
						<div id="">
							<textarea rows="10" id="postdataval<?php echo $postID; ?>" name="postdataval<?php echo $postID; ?>"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="desref-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Lightbox popup -->
	<div id="lightbox_file">
		<?php foreach ($file_recommendation as $single_file) { ?>
			<div id="show_detail_<?php echo $single_file->ID; ?>" class="lightbox" style="display: none;">
				<div class="container">
					<div class="img-represent">
						<?php echo apply_filters( 'dyn_file_image', $output, $single_file->ID ); ?>
						<h4><?php echo $single_file->post_title; ?></h4>
					</div>
					<ul class="detail-info">
						<li><label class="detail-title">By:</label> <span><?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?></span></li>
						<li><?php $output = "";echo apply_filters( 'dyn_display_review', $output, $single_file->ID, "post" );?></li>
						<li><label class="detail-title">Description:</label><?php echo wp_trim_words( $single_file->post_content, 15, '[...]' ); ?></li>
						<li><label class="detail-title">Reference:</label> No reference given</li>
					</ul>
					<ul class="stat">
						<li><i class="fa fa-eye"><?php echo videopress_countviews($single_file->ID); ?></i></li>
						<li><i class="fa fa-wrench"><?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_file->ID);echo count($result); ?> Endorsments</i></li>
						<li><i class="fa fa-heart">
								<?php
								$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_file->ID;
								$result1 = $wpdb->get_results($sql_aux);
								echo count($result1);
								?> Favorites

							</i></li>
						<li><i class="fa fa-star-o">
								<?php echo apply_filters('dyn_number_of_post_review', $single_file->ID, "post"); ?>
							</i></li>
						<li><i class="fa fa-download">
								<?php
								$download = get_post_meta( $single_file->ID, 'dyn_download_count', true);
								if($download == ""){echo '0 Download';}else{echo $download. " Downloads";}
								?>
							</i></li>
					</ul>
					<a class="close-lightbox">Close</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<!-- End Lightbox popup -->
<?php
//$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId  AND type =1 ORDER BY id DESC");
$data_file = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =2 ORDER BY id DESC");
//$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =3 ORDER BY id DESC");

?>	
<!-- file model -->
<div class="modal fade" id="filetest_mod<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Files to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file<?php echo $postID; ?>">
				<label>SELECT Playlist to add Files</label>
				<select name="plval">
				<?php foreach($data_file as $datafile): ?>
					<option value="<?= $datafile->id; ?>"><?= $datafile->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-file<?php echo $postID; ?>').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest<?php echo $postID; ?>').click(function(e){
		jQuery('#filetest_mod<?php echo $postID; ?>').modal();
	});
</script> 

	 
<?php
	
}


function show_book_content($postIDs,$types,$user_check_id){
	global $wpdb;
	$postby = $types;
	$postID = $postIDs; 
	$user_action_id = $user_check_id;
				 
		 ?>
		 
<?php
$postId=get_the_ID();
$uploaded_type = get_post_meta( $postID, 'dyn_upload_type', true );
$uploaded_value = get_post_meta( $postID, 'dyn_upload_value', true );
?>
	<style>
		.flag_post
		{
			font-size: 14px !important;
		}
		#endorse_wrapper
		{
			font-size: 14px !important;
		}
		#favorite_wrapper
		{
			font-size: 14px !important;
		}
		.spaceabove {
			margin-top: 4px !important;
		}
		body{    background-color: #e9ebee;}
	</style>
<?php if(!is_user_logged_in()): ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
<?php endif; ?>
<?php



$postdataval = false;
if(isset($_POST['postdataval'.$postID]) && $_POST['postdataval'.$postID]!=''){
	if($_POST['postdataname'.$postID] == 'desc'){
		$postdataval = $_POST['postdataval'.$postID];
		$my_post = array(
			'ID'           => $_POST['postid'.$postID],
			'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'.$postID] == 'refe'){
		update_post_meta($_POST['postid'.$postID], 'video_options_refr', $_POST['postdataval'.$postID]);
	}
}
?>
	<!-- Start Content single-dyn-file -->
	<div class="container p_90">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">
		<!-- Start Entries -->
		<div class="grid-3-4 centre-block">
			<!-- Video part-->
			<div class="grid-3-4-sidebar single-page">
				<?php
				if( isset($_REQUEST['done']) and $_REQUEST['done'] == 1 )
				{
					?>
					<div style="color: blue; font-size: 14pt; font-weight: bold;">
						Thanks for donating the amount.
					</div>
					<?php
				}
				else if( isset($_REQUEST['done']) and $_REQUEST['done'] == 0 )
				{
					?>
					<div style="color: red; font-size: 14pt; font-weight: bold;">
						There is some error in server..Please contact with site administrator
					</div>
					<?php
				}
				else
				{ }
				/* ================================================================== */
				/* Start of Loop */
				/* ================================================================== */
				//while (have_posts()) : the_post();

				// this next bit sets a cookie for related videos
				$first_category = get_the_category();
				$bpost=get_post($postID);
				$title = $post->post_title;
				$post_author     = $bpost->post_author;
				$book_author = get_the_author_meta( 'display_name', $post_author);
	            $userId = get_post_field( 'post_author', $postID );
				$profile_img = get_user_meta( $userId, 'profile_pic_meta', true );
				
				$athor_action = get_author_posts_url( $user_action_id ); //$user_action_id
				$profile_img_action = get_user_meta( $user_action_id, 'profile_pic_meta', true );
				$book_author_action = get_the_author_meta( 'display_name', $user_action_id);
				
				$authorurl = get_author_posts_url( $userId ); 
				$theflContent=$bpost->post_content;
				$titlepo = urlencode(html_entity_decode($title));
				//get_permalink( $postID)
				$pourl = urlencode( get_permalink( $postID) );
		if($profile_img_action){
			$user_img_src = site_url() . '/profile_pic/' . $profile_img_action;
		}else{
			$user_img_src = get_template_directory_uri().'/images/no-image.png';
		}
			
		//$display_template .= "<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src='". $user_img_src."' class='layout-3-thumb' >";
		
		//$display_template .= "<h4 style='padding-left:10px; color: steelblue;font-size: 15px; font-weight: 600; margin-top:0;margin-bottom:2px;'><span style='color:#222;'>".$book_author." </span><span style='color:#606060;font-weight: 600;'> added</span> <a href='". get_permalink( $postID) ."'> ".$title." </a></h4>";
		
				?>
				<script>
					jQuery(document).ready(function(){
						jQuery.ajax({
							url: "<?php echo get_home_url() ?>/diynation_related_videos_cookie.php",
							type: "POST",
							data: {'category': '<?php echo $first_category[0]->term_id; ?>'}
						}).done(function(data) {
							//console.log("Current category id: " + data);
						});
					});
				</script>
				<?php // end set cookie for related videos ?>


				<?php if($userId == get_current_user_id()){ ?>
				<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" id="title-naz<?php echo $postID; ?>" style='width:80%'><div style="width: auto;float: left;" id="titleafter-edrit<?php echo $postID; ?>"><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby; ?></span> <a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></div>&nbsp;&nbsp;<div id="title-edit<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i>Edit</div></h6>
				<?php } else {
					?>
					<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" style='width:80%'><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby; ?></span> <a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></h6>
				 
					<?php
				} ?>
				<div id="title-div<?php echo $postID; ?>" style="display:none;">
					<input type="text" name="title-val<?php echo $postID; ?>" value="<?php echo get_the_title($postID);  ?>">
					<input type="hidden" name="post-val<?php echo $postID; ?>" value="<?php echo $postID; ?>">
					<button type="button" class="btn btn-info" id="title-save<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="title-cncl<?php echo $postID; ?>">Cancel</button>
					<br>
				</div>

                <script>
				jQuery(document).ready(function($) {
					$('#title-edit<?php echo $postID; ?>').click(function(){
						$('#title-naz<?php echo $postID; ?>').hide();
						$('#title-div<?php echo $postID; ?>').show();
					});

					$('#title-cncl<?php echo $postID; ?>').click(function(e){
						e.preventDefault();

						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();
					});


					$('#title-save<?php echo $postID; ?>').click(function(e){
						e.preventDefault();


						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();

						var post_title = jQuery('input[name="title-val<?php echo $postID; ?>"]').val();
						var post_id = jQuery('input[name="post-val<?php echo $postID; ?>"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
								$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
							}
						})
					});
					 
				});

			</script>

				<!-- Start Video Player -->
				<?php // get_template_part('includes/file-display'); 
				?>
				<div class="vid-single">

	<div class="file-container">
		<div class="image-holder">
			<?php
			$bid = strtolower(str_replace(" ", "_", get_the_title($postID)));
			$valuetest =  do_shortcode( '[responsive-flipbook id="'.$bid.'"]' );
			if (preg_match('/ERROR:/',$valuetest))
			{
			 $bid = strtolower(str_replace(" ", "", get_the_title($postID)));	
			 echo do_shortcode( '[responsive-flipbook id="'.$bid.'"]' );
			}
			else{
				echo $valuetest;
			}
			?>
		</div>
		<div style="width:100%;margin-top:10px;">

			<?php
			$country_id = oiopub_settings::get_user_country_index();

			if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
				$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

			} else {
				$queryyy = " ";
			}
			$tagSelected = wp_get_post_tags($post->ID);
			
	$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 5 and item_status=1 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


			$bannerurl = '';
			$bannerid = '';
			$countryinbase = '';
			foreach ($banners as $banner) {

				$bannerurl = $banner->item_url;
				$bannerid = $banner->item_id;
			}


			?>
			<?php
			if ($banners) {
				?>	  <a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img id="<?php echo $postID ?>" class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
			<?php }

			?> </div>
		 
	</div>
				<!-- End Video Player -->



				<!-- Start Content -->
				<div class="entry grey-background">

					<p>By <a href="<?php echo $authorurl; ?>">
							<?php echo $book_author; ?>
						</a>
					</p>
					 
					<!-- Start Video Heading -->
					<div class="video-heading">

						<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
					 
					<div class="flag_post custom_btn"><span id="addflag-anchor<?php echo $postID; ?>">Flag</span></div>
                     
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID . ' AND user_id = ' . $userId);
						//print_r($result);
						if (empty($result)){
							?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
						<?php }else{ ?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
						<?php }  ?>

						<!-- Section for the favorite button -->
					 
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID . ' AND user_id = ' . $userId);
						if (empty($result)){
							?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
						<?php }else{ ?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
						<?php }  ?>
						<!-- End favorite -->
                     
						<?php
						//Adding a filter for adding Review button
						//This will be added from dyn-review plugin
						$output = "";
						echo apply_filters( 'dyn_review_button', $output, $postID, "post" );
						?>
                      
						 
						<?php //Start of Section for the Donate Button get_current_user_id()
						$return_url = current_page_url()."?done=1";
						$cancel_url = current_page_url()."?done=0";
						$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_current_user_id()." and status=1");
						$paypalacc='';
						if($datapaypal){
							foreach ($datapaypal as $reg_datagen){
								$paypalacc=$reg_datagen->paypal_email;
							}
						}
						if($paypalacc ==''){ ?>
							&nbsp;&nbsp;
							<button type="button" class="btn btn-primary" id="donated<?php echo $postID; ?>" data-toggle="modal" data-target="#model_donate">Donate</button>
							<!--Donate Popup-->
							<div class="modal" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
										</div>
										<div class="modal-body">
											<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="lc" value="US">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<div class="form-group">
													<span for="amount" class="control-label col-sm-4">Amount:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="amount" required >
													</div>
												</div>
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<input type="hidden" name="on1" value="Message" required>
												<div class="form-group">
													<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os1" maxlength="300" size="40">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						<?php } 
						// End Donate Button Section ?>

						<script>
jQuery(document).ready(function($) {
	$('.video-heading').on('click', '#donated<?php echo $postID; ?>', function( e ) {
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not donate Yourself!");
			}else {
		$('div#model_donate').attr('style', 'display:block');
			}
		//$('div#model_donate').removeClass('modal');
	});
	$('.modal-header').on('click', 'button.close', function( e ) {
		$('div#model_donate').attr('style', 'display:none');
		//$('div#model_donate').removeClass('modal');
	});
	
});
</script>
						<div class="clear"></div>
					</div>
					<!-- End Video Heading -->

					<!-- Start Sharer -->
					<div class="clear"></div>

<!-- Modal -->
<div class="modal fade" id="model_flag<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="url<?php echo $postID; ?>" value="<?= get_permalink($postID);?>">
					<textarea rows="10" id="reason<?php echo $postID; ?>" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>

<script>
	var $ = jQuery.noConflict();
	
	/* $(document).ready(function(){
		CKEDITOR.inline( 'reason' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';    
			config.fontSize_defaultLabel = '44px';
		};
	}); */
	
	$('#addflag-submit<?php echo $postID; ?>').live('click',function(e){
		
		//$('#addflag-form').submit();
		//$("form").serialize()
		var $ = jQuery.noConflict();
		
		var reason = $('#reason<?php echo $postID; ?>').val();
		var url = $('#url<?php echo $postID; ?>').val();
		var post_id_flag = "<?php echo $postID; ?>";
		$.ajax({
			type: "POST",  
			url: "<?php echo admin_url('admin-ajax.php'); ?>",  
			data: {action:'user_mark_flag',reason:reason,url:url,post_id_flag:post_id_flag},
			success : function(data){
				var modelhtml = $('#model_flag<?php echo $postID; ?> .modal-content').html();
				$('#model_flag<?php echo $postID; ?> .modal-content').html(data);
				
				setTimeout(function(){
					$('#model_flag<?php echo $postID; ?>').modal('hide');
					$('#model_flag<?php echo $postID; ?> .modal-content').html(modelhtml);
				},3000)
			}
		});
	
	});
	
	$('#addflag-anchor<?php echo $postID; ?>').click(function(e){
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not flag Yourself.");
			}else {
		var $ = jQuery.noConflict();
		$('#model_flag<?php echo $postID; ?>').modal();
			}
	});
</script>
					<!-- End Sharer -->
					 
					<?php
					$output = "";
					echo apply_filters( 'dyn_display_review', $output, $postID, "post" );
					?>
					 
					<br>
					<div class="panel-group">
						<!-- START About us home page -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#paneldes<?php echo $postID;?>">
									<h4 class="panel-title">Description <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="paneldes<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-des<?php echo $postID;?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="desf-c">
										<?php
										if($postdataval){
											echo $postdataval;
										}
										else{
											if($theflContent==""){
												echo '<div class="content-empty">No Description Provided</div>';
											}else{
												echo $theflContent;
											}
										}

										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelref<?php echo $postID;?>">
									<h4 class="panel-title">Reference <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelref<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-ref<?php echo $postID;?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="ref-c">
										<?php
										$ref = get_post_meta($postID,'video_options_refr',true);
										if($ref){
											echo $ref;
										}else{
											echo 'No referance given';
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelshare<?php echo $postID;?>">
									<h4 class="panel-title">Share <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelshare<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
								<div id="option-social">
								<a class="opt-facebook" href= "https://www.facebook.com/sharer/sharer.php?u=<?php echo $pourl; ?>&t=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-facebook"></span>facebook</a>
								<a class="opt-twitter" href= "http://twitter.com/home?status=<?php echo $titlepo; ?>&nbsp;<?php echo $pourl; ?>" target="_blank"><span class="socicon-twitter"></span>twitter</a>
								<a class="opt-googleplus" href= "https://plus.google.com/share?url=<?php echo $pourl; ?>" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a>
								<a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-linkedin"></span>linkedin</a>
								<a class="opt-dig" href= "http://digg.com/submit?url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-digg"></span>dig</a>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END About us home page -->
				</div>
				<script>
					var $ = jQuery.noConflict();
					$(document).ready(function(){
						var $ = jQuery.noConflict();

						$(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');

						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("hide.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');
						});
						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("show.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-up"></span>');
						});
						
									$('#edit-des<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();

			e.preventDefault();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Description');
			$('#postdataname<?php echo $postID; ?>').val('desc');

			$('.cke_textarea_inline').html($('#desf-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#edit-ref<?php echo $postID; ?>').click(function(e){
			e.preventDefault();

			var $ = jQuery.noConflict();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Reference');
			$('#postdataname<?php echo $postID; ?>').val('refe');

			$('.cke_textarea_inline').html($('#ref-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#desref-submit<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();
			$('#desref-form<?php echo $postID; ?>').submit();
		});

		
		$('#endorse_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookendorsementsssss',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								if (data.match(/endorsed/g)){
									//alert(data);
					$('#endorse_button<?php echo $postID; ?>').html('<span>Endorsed!</span>');
					$('#endorse_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
				if (data.match(/deleteend/g)){
					jQuery('#endorse_button<?php echo $postID; ?>').html('<span>Endorse</span>');
					jQuery('#endorse_button<?php echo $postID; ?>').css('background-color','#000');
				}
							}
						})
			}
			  
		});
		
		$('#favorite_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookfavoritefeed',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								  // alert(data);
								if (data.match(/favorite/g)){
									//alert(data);
									jQuery('#favorite_button<?php echo $postID; ?>').html('<span>Favorite</span>');
					jQuery('#favorite_button<?php echo $postID; ?>').css('background-color','#ff8a8a');
				
				}
				if (data.match(/nsotsafev/g)){
						$('#favorite_button<?php echo $postID; ?>').html('<span>Un-favorite</span>');
					$('#favorite_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
							}
						})
			}
			  
		});
		
		$('#tags-edit<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		$('#tags-val<?php echo $postID; ?>').hide();
		$('.tags-forminput<?php echo $postID; ?>').show();
	});
	$('#tagcancel<?php echo $postID; ?>').click(function(e){
				e.preventDefault();
				$('.tags-forminput<?php echo $postID; ?>').hide();
			});
	$('#tagsave<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		
		var tags_postid = $('#tags_postid<?php echo $postID; ?>').val();
		var tagsinput = $('#tagsinput<?php echo $postID; ?>').val();
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
			}).done(function(data) {
				location.reload();
		});
		
		$('.tags-forminput<?php echo $postID; ?>').hide();
		$('#tags-val<?php echo $postID; ?>').show();
		
	});

					})
				</script>

				<script>
					var $ = jQuery.noConflict();

					$('.ref-more').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.ref-less').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});

					$('.des-more').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.des-less').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});
				</script>

				<div class="clear"></div>
			</div>
			<!-- End Content -->
			<!-- Start Content -->
			<ul class="stats sicon grey-background">
			 
				<li><i class="fa fa-eye"></i> <?php videopress_countviews( $postID);  ?></li>
		 
			 
				<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID);
					echo count($result); ?> Endorsments</li>
			 
				<!--favourite section -->
				 
				<li><i class="fa  fa-heart"></i>
					<?php
					$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID;
					$result1 = $wpdb->get_results($sql_aux);
					echo count($result1);
					?> Favorites</li>
			 	
				<!--end favorite-->
			 
				<li><i class="fa fa-comments"></i> <?php echo get_comments_number( $postID ) ;?> Comment</li>
			 
               				
				<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U',$postID), current_time('timestamp') ) . ' ago'; ?></li>
			  
				<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', $postID, "post"); ?></li>
			  
				<li><i class="fa fa-download"></i><?php echo apply_filters( 'dyn_number_of_download', $postID); ?></li>
			 
			</ul>
			<div class="clear"></div>
             
			<?php if( vp_option('vpt_option.show_user') == '1' ){ ?>
				<div class="post-by">
					<a href="<?php echo get_author_posts_url( $post_author ); ?>">
					<img src="<?php echo $user_img_src; ?>" class="avatar user-37-avatar avatar-50 photo" width="50" height="50" alt="P"></a>
					<a href="<?php echo get_author_posts_url( $post_author ); ?>" class="post-by-link"><?php echo $book_author; ?></a>
					<?php echo '<div class="post-by-vid-count">'.count_user_posts( $post_author ).' Videos Uploaded</div>'; ?>
					<div class="clear"></div>
				</div>
		 
            <?php } ?>
			<?php
			// Link Pages Navigation
			$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
			wp_link_pages( $args );


			// Display the Tags
			$tagval = '';?>
			<?php $all_tags = array();?>
			<?php $all_tags = get_the_tags($postID);?>
			<?php if($all_tags){
				$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
			}

			if($userId == get_current_user_id()){
				$before1 = '<span><i class="fa fa-tags"></i>Tags';
				$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit'. $postID .'"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
			}
			$before = '<span id="tags-val">';

			$after = '</span>';


			?>
            
			<?php if( is_user_logged_in() ){
				$profile_id = get_current_user_id();
				$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =2  ORDER BY id DESC");
				if($data){ ?>
					<div class="addto_playlist grey-background padding5_10">
						<a herf="#" id="fileplaytest<?php echo $postID; ?>" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
					</div>
			<?php } }  ?>
			<div class="spacer-20"></div><div class="post-tags grey-background padding5_10">
				<?= $before1;?>
				<?php
				$tagSelected = wp_get_post_tags($postID);
				$tagsNameArray = array();
				$tagsSlugArray = array();
				$tagsIDArray = array();
				foreach($tagSelected as $key=>$val)
				{
					$name = $val->name;
					$slug = $val->slug;
					$term_id = $val->term_id;
					$tagsNameArray[] = $name;
					$tagsSlugArray[] = $slug;
					$tagsIDArray[] = $term_id;
				}
				echo get_the_tags( $before,', ',$after );
 $tags = get_the_tags($postID); ?>
  
	 <span id="tags-val<?php echo $postID; ?>">

 
      <?php foreach($tags as $tag) :  ?>
 
    
        <a rel="tag"
            href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>">
                  <?php print_r($tag->name); ?>
         </a>   ,

      <?php endforeach; ?>
</span>	
 		
			</div>


			<?php

			if($all_tags){
				foreach($all_tags as $tag){
					$tagval .= $tag->name.',';
				};
			}

			rtrim($tagval, ",");

			if($userId == get_current_user_id()){
				?>

				<div class="tags-forminput<?php echo $postID; ?>" style="display:none;">

					<input name="tagsinput" class="tagsinput" id="tagsinput<?php echo $postID; ?>" value="<?= $tagval;?>">
					<input name="userid" type="hidden" id="tags_postid<?php echo $postID; ?>" value="<?= $postID ;?>">
					<button type="button" class="btn btn-success" id="tagsave<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="tagcancel<?php echo $postID; ?>">Cancel</button>

				</div>

			<?php } ?>

			<script>
				jQuery(document).ready(function(){
					jQuery('#songids').val('<?php echo $postID;?>')
				});

			</script>
			<!--
    <div class="post-categories">
    <span>
		<i class="fa fa-folder"></i>Categories&nbsp;&nbsp;
		<?php if(get_the_author_id() == get_current_user_id()){ ?>
			<a href="#" id="category-edit"><i class="fa fa-pencil-square-o"></i>Edit</a>
		<?php } ?>
	</span>

	<div id="cat-div">
	    <?php
			$postCats = wp_get_post_categories($postID);
			the_category('&nbsp;,&nbsp;');?>
	</div>

	<div id="cat-editdiv" style="display:none;">

	<?php

			$allcate = get_the_category();
			$post_catar = array();
			foreach($allcate as $cate){
				$post_catar[] = $cate->cat_ID;
			}

			//print_r($post_catar);

			// //$args = array(
			// 	'type'                     => 'post',
			// 	//'child_of'                 => 0,
			// 	//'parent'                   => '',
			// 	'orderby'                  => 'name',
			// 	'order'                    => 'ASC',
			// 	'hide_empty'               => 0,
			// 	'hierarchical'             => 1,
			// 	'exclude'                  => array('1','23'),
			// 	'include'                  => '',
			// 	'number'                   => '',
			// 	'taxonomy'                 => 'category',
			// 	//'pad_counts'               => false

			// );

			$categories = get_categories( $args );

			?>


		<ul>
			<?php foreach($categories as $cat ){ ?>
				<li>
					<input type="checkbox" name="cat_c[]" value="<?= $cat->cat_ID; ?>" <?= (in_array($cat->cat_ID, $post_catar))? 'checked':'';?>>
					<?= $cat->name; ?>
				</li>
			<?php } ?>
		</ul>
		<input type="hidden" name="post_id" value="<?php the_ID(); ?>">

		<button type="button" class="btn btn-info" id="cat-save">Save</button>
		<button type="button" class="btn btn-primary" id="cat-cncl">Cancel</button>
	</div>

    </div>

    -->

			<div class="clear"></div>
			<!-- End Content -->
          
			<?php if(function_exists("display_saic_feeds")) { echo display_saic_feeds($postID);} ?>
		 
			<!-- Start Related Videos -->
			<div class="spacing-40"></div>
			<?php //get_template_part('includes/related-videos'); ?>
			<!-- End Related Videos -->


			<?php
			/* ================================================================== */
			/* End of Loop */
			/* ================================================================== */
			//endwhile;
			?>


		</div>
		<!-- End Video Part -->

		

		
	</div>
	<!-- End Entries -->


	<!-- Widgets -->
	<!--<?php //get_sidebar(); ?>--
    <!-- End Widgets -->

	<div class="clear"></div>
	</div>
	<!--
    <div class="container p_90">
        <div class="grid-3-4 centre-block">
        <div class="grid-3-4-sidebar single-page">

        </div>
        </div>
    </div>
    -->
	<div class="spacing-40"></div>
	<!-- End Content -->
 

	<!-- Modal -->
	<div class="modal fade" id="modeldesref_section<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel<?php echo $postID; ?>">Add Description</h4>
				</div>
				<div class="modal-body">
					<form method="post" id="desref-form<?php echo $postID; ?>">
						<input type="hidden" name="postdataname<?php echo $postID; ?>" id="postdataname<?php echo $postID; ?>">
						<input type="hidden" name="postid<?php echo $postID; ?>" id="postid" value="<?= $postID;?>">
						<div id="">
							<textarea rows="10" id="postdataval<?php echo $postID; ?>" name="postdataval<?php echo $postID; ?>"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="desref-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Lightbox popup -->
	<div id="lightbox_file">
		<?php foreach ($file_recommendation as $single_file) { ?>
			<div id="show_detail_<?php echo $single_file->ID; ?>" class="lightbox" style="display: none;">
				<div class="container">
					<div class="img-represent">
						<?php echo apply_filters( 'dyn_file_image', $output, $single_file->ID ); ?>
						<h4><?php echo $single_file->post_title; ?></h4>
					</div>
					<ul class="detail-info">
						<li><label class="detail-title">By:</label> <span><?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?></span></li>
						<li><?php $output = "";echo apply_filters( 'dyn_display_review', $output, $single_file->ID, "post" );?></li>
						<li><label class="detail-title">Description:</label><?php echo wp_trim_words( $single_file->post_content, 15, '[...]' ); ?></li>
						<li><label class="detail-title">Reference:</label> No reference given</li>
					</ul>
					<ul class="stat">
						<li><i class="fa fa-eye"><?php echo videopress_countviews($single_file->ID); ?></i></li>
						<li><i class="fa fa-wrench"><?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_file->ID);echo count($result); ?> Endorsments</i></li>
						<li><i class="fa fa-heart">
								<?php
								$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_file->ID;
								$result1 = $wpdb->get_results($sql_aux);
								echo count($result1);
								?> Favorites

							</i></li>
						<li><i class="fa fa-star-o">
								<?php echo apply_filters('dyn_number_of_post_review', $single_file->ID, "post"); ?>
							</i></li>
						<li><i class="fa fa-download">
								<?php
								$download = get_post_meta( $single_file->ID, 'dyn_download_count', true);
								if($download == ""){echo '0 Download';}else{echo $download. " Downloads";}
								?>
							</i></li>
					</ul>
					<a class="close-lightbox">Close</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<!-- End Lightbox popup -->
<?php
//$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId  AND type =1 ORDER BY id DESC");
//$data_file = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =2 ORDER BY id DESC");
$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =3 ORDER BY id DESC");

?>	
<!-- file model -->
<div class="modal fade" id="filetest_mod<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Files to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file<?php echo $postID; ?>">
				<label>SELECT Playlist to add Files</label>
				<select name="plval">
				<?php foreach($data_book as $databook): ?>
					<option value="<?= $databook->id; ?>"><?= $databook->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-file<?php echo $postID; ?>').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest<?php echo $postID; ?>').click(function(e){
		jQuery('#filetest_mod<?php echo $postID; ?>').modal();
	});
</script> 

	 
<?php
	
}

function show_video_feed($postIDs,$types,$user_check_id){
	global $wpdb;
	$postby = $types;
	$postID = $postIDs; 
    $user_action_id = $user_check_id;
	 $getvideoke = get_post_meta( $postID, 'video_options', true );
	 $videourlssroot = $getvideoke['video_upload'];

				 
		 ?>
		 
<?php
$postId=get_the_ID();
$uploaded_type = get_post_meta( $postID, 'dyn_upload_type', true );
$uploaded_value = get_post_meta( $postID, 'dyn_upload_value', true );
?>
	<style>
		.flag_post
		{
			font-size: 14px !important;
		}
		#endorse_wrapper
		{
			font-size: 14px !important;
		}
		#favorite_wrapper
		{
			font-size: 14px !important;
		}
		.spaceabove {
			margin-top: 4px !important;
		}
		body{    background-color: #e9ebee;}
	</style>
<?php if(!is_user_logged_in()): ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
<?php endif; ?>
<?php



$postdataval = false;
if(isset($_POST['postdataval'.$postID]) && $_POST['postdataval'.$postID]!=''){
	if($_POST['postdataname'.$postID] == 'desc'){
		$postdataval = $_POST['postdataval'.$postID];
		$my_post = array(
			'ID'           => $_POST['postid'.$postID],
			'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'.$postID] == 'refe'){
		update_post_meta($_POST['postid'.$postID], 'video_options_refr', $_POST['postdataval'.$postID]);
	}
}
?>
	<!-- Start Content single-dyn-file -->
	<div class="container p_90">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">
		<!-- Start Entries -->
		<div class="grid-3-4 centre-block">
			<!-- Video part-->
			<div class="grid-3-4-sidebar single-page">
				<?php
				if( isset($_REQUEST['done']) and $_REQUEST['done'] == 1 )
				{
					?>
					<div style="color: blue; font-size: 14pt; font-weight: bold;">
						Thanks for donating the amount.
					</div>
					<?php
				}
				else if( isset($_REQUEST['done']) and $_REQUEST['done'] == 0 )
				{
					?>
					<div style="color: red; font-size: 14pt; font-weight: bold;">
						There is some error in server..Please contact with site administrator
					</div>
					<?php
				}
				else
				{ }
				/* ================================================================== */
				/* Start of Loop */
				/* ================================================================== */
				//while (have_posts()) : the_post();

				// this next bit sets a cookie for related videos
				$first_category = get_the_category();
				$bpost=get_post($postID);
				$title = $post->post_title;
				$post_author     = $bpost->post_author;
				$book_author = get_the_author_meta( 'display_name', $post_author);
	            $userId = get_post_field( 'post_author', $postID );
					$profile_img = get_user_meta( $userId, 'profile_pic_meta', true );
				$authorurl = get_author_posts_url( $userId ); 
				
				$athor_action = get_author_posts_url( $user_action_id ); //$user_action_id
				$profile_img_action = get_user_meta( $user_action_id, 'profile_pic_meta', true );
				$book_author_action = get_the_author_meta( 'display_name', $user_action_id);
				
				
				$theflContent=$bpost->post_content;
				$titlepo = urlencode(html_entity_decode($title));
				//get_permalink( $postID)
				$pourl = urlencode( get_permalink( $postID) );
		if($profile_img_action){
			$user_img_src = site_url() . '/profile_pic/' . $profile_img_action;
		}else{
			$user_img_src = get_template_directory_uri().'/images/no-image.png';
		}
			
		//$display_template .= "<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src='". $user_img_src."' class='layout-3-thumb' >";
		
		//$display_template .= "<h4 style='padding-left:10px; color: steelblue;font-size: 15px; font-weight: 600; margin-top:0;margin-bottom:2px;'><span style='color:#222;'>".$book_author." </span><span style='color:#606060;font-weight: 600;'> added</span> <a href='". get_permalink( $postID) ."'> ".$title." </a></h4>";
		
				?>
				<script>
					jQuery(document).ready(function(){
						jQuery.ajax({
							url: "<?php echo get_home_url() ?>/diynation_related_videos_cookie.php",
							type: "POST",
							data: {'category': '<?php echo $first_category[0]->term_id; ?>'}
						}).done(function(data) {
							//console.log("Current category id: " + data);
						});
					});
				</script>
				<?php // end set cookie for related videos ?>


				<?php if($userId == get_current_user_id()){ ?>
				<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" id="title-naz<?php echo $postID; ?>" style='width:80%'><div style="width: auto;float: left;" id="titleafter-edrit<?php echo $postID; ?>"><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby;?></span><a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></div>&nbsp;&nbsp;<div id="title-edit<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i>Edit</div></h6>
				<?php } else {
					?>
					<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" style='width:80%'><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby;?></span> <a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></h6>
				 
					<?php
				} ?>
				<div id="title-div<?php echo $postID; ?>" style="display:none;">
					<input type="text" name="title-val<?php echo $postID; ?>" value="<?php echo get_the_title($postID);  ?>">
					<input type="hidden" name="post-val<?php echo $postID; ?>" value="<?php echo $postID; ?>">
					<button type="button" class="btn btn-info" id="title-save<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="title-cncl<?php echo $postID; ?>">Cancel</button>
					<br>
				</div>

                <script>
				jQuery(document).ready(function($) {
					$('#title-edit<?php echo $postID; ?>').click(function(){
						$('#title-naz<?php echo $postID; ?>').hide();
						$('#title-div<?php echo $postID; ?>').show();
					});

					$('#title-cncl<?php echo $postID; ?>').click(function(e){
						e.preventDefault();

						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();
					});


					$('#title-save<?php echo $postID; ?>').click(function(e){
						e.preventDefault();


						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();

						var post_title = jQuery('input[name="title-val<?php echo $postID; ?>"]').val();
						var post_id = jQuery('input[name="post-val<?php echo $postID; ?>"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
								$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
							}
						})
					});
					 
				});

			</script>

		<!-- Start Video Player -->
				<?php // get_template_part('includes/file-display'); 
				?>
		<div class="vid-single">	
		<div class="videocontainer">
		<?php $country_id = oiopub_settings::get_user_country_index(); ?>
		<div id="feed<?php echo $postID; ?>">
		<?php $getimgsrc = wp_get_attachment_image_src(get_post_thumbnail_id($postID), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $getimgsrc[0]; ?>" class="img-responsive" style="width:100%;height:345px;">
		<video id="feedvideo<?php echo $postID; ?>" loop preload="none" style="position: relative;cursor:pointer;display:none;width:100%;"><source src="<?php echo $videourlssroot; ?>"></video>	
		</div>
		<div id="VideoPlayer<?php echo $postID;?>" style="margin-bottom: 8px;"></div>
		  <style>
			#VideoPlayer<?php echo $postID; ?> {
               position: relative;               
            }
			#feedvideo<?php echo $postID; ?>{ -webkit-transition:all 0.4s ease-in-out; -moz-transition:all 0.4s ease-in-out; -o-transition:all 0.4s ease-in-out; transition:all 0.4s ease-in-out;}
		   </style>		   
		   <?php		   
		if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
			$queryyy = " AND ((`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id') OR country ='all') ";

		} else {
			$queryyy = "";
		}

		$bhtemp= "SELECT item_id,item_url FROM ".$wpdb->prefix. "oiopub_purchases WHERE payment_status = 1 and item_status=1 AND item_channel = 6 $queryyy LIMIT 1";

		//$vhquery = $vhtemp.' LIMIT  ' . $start .', 12';

		$bhresult = $wpdb->get_results( $bhtemp );
		//echo  $wpdb->num_rows . 'Rows Found';

		$bhtotal = $wpdb->query($bhtemp);
		$favs = $wpdb->get_results('SELECT item_id,item_channel,item_url FROM '.$wpdb->prefix."oiopub_purchases WHERE item_channel= 6 and item_status=1 and payment_status = 1  $queryyy  ORDER BY RAND() LIMIT 1");
		
			
		$favsnum = count($favs);
		//	echo "<script>alert($favsnum);</script>";
		$videourl = '';
		$videoid = '';
		foreach($favs as $signvideo)
		{
			//echo $signvideo['item_url'];
			$videourl = $signvideo->item_url;
			$videoid = $signvideo->item_id;
			$videoch = $signvideo->item_channel;
			//echo $favsnum ;
		}
		
		//echo "<br><br>".$videoid " mmm ".$videoch;
		//echo $favsnum ;
		/**/	 
		?>
				
		<style>
				.elite_vp_logo{margin-top:6px;}
				.fa-elite-info {display:none}
				.fa-elite-share-square-o{display:none}

				#oio-banner-1{
					height:140px;
				}
				.oio-banner-zone .oio-slot a, .oio-banner-zone .oio-slot a:hover, .oio-banner-zone .oio-slot img, .oio-banner-zone .oio-slot object {
					position: absolute !important;
					top: 0 !important;
					left: 0 !important;
					height: 150px !important;
				}

			</style>


		<?php
		$user= wp_get_current_user();
		$showad = '';
		if(!(in_array( 'adfree_user', (array) $user->roles )))
			$showad = "yes";
		else $showad = "no";
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT item_id, item_url,item_page FROM wp_oiopub_purchases WHERE (( item_url LIKE "%.jpg" )OR( item_url LIKE "%.jpeg" )OR( item_url LIKE "%.png" )OR( item_url LIKE "%.gif" )) AND item_page != "" AND item_channel= 8 and item_status=1 and payment_status = 1 ORDER BY RAND() LIMIT 1' );

		$item_url 	= "http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/Banner.jpg";
		$item_page	= "http://www.doityourselfnation.org/bit_bucket/";
		if(count($results) > 0){
			foreach($results as $resultsItem){
				$item_url 	= $resultsItem->item_url;
				$item_page	= $resultsItem->item_page;
				$item_id_b	= $resultsItem->item_id;
			}
		}
		
		global $wpdb;
		$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE ( `item_channel` = 8 || `item_channel` = 3 ) and item_status=1 and `payment_status` = 1 and country != '' $queryyy ORDER BY RAND() LIMIT 1");
		
		$bannerurl = '';
		$bannerid = '';
		$countryinbase = '';
		foreach ($banners as $banner) {

			$bannerurl = $banner->item_url;
			$bannerid = $banner->item_id;
		}
		$show = ($banner->item_id) ? 1 : 0;
	

		?>
		 
		<script type="text/javascript" charset="utf-8">
				jQuery(document).ready(function($){
					var figure = $("#feed<?php echo $postID; ?>").hover( hoverVideo, hideVideo );
					function hoverVideo(e) { 
						var Video = $('Video Element')[0];
						$('video',this).parent().find("img").css("display","none");
						$('video',this).css("display","block");
						$('video',this).on('timeupdate',function(e)
						{
							current_time = e.currentTarget.currentTime;
							console.log($(this).get(0).currentTime);
							if(current_time > 5)
							{
								$(this).get(0).pause();
								$(this).get(0).currentTime = 0;
								$(this).get(0).play();
							}
						});
						$('video', this).get(0).play(); 
					}
					function hideVideo(e) {
						$('video',this).parent().find("img").css("display","block");
						$('video',this).css("display","none");
						$('video', this).get(0).pause();
						console.log($('video', this).currentTime);
						$('video',this).on('timeupdate',function(e)
						{
							$(this).currentTime = 0;
						});
					}
					/* jQuery(function(){
						jQuery('#feed<?php echo $postID; ?>').on('mouseenter', function(){
							if( this.paused) this.play();
						}).on('mouseleave', function(){
							if( !this.paused) this.pause();
						});
					}); */
					jQuery("#feed<?php echo $postID; ?>").on("click",function(){
					jQuery(this).css("display", "none");
					var bla = $('#additem_id').val();
					videoPlayer = $("#VideoPlayer<?php echo $postID; ?>").Video({   //ALL PLUGIN OPTIONS
						instanceName:"player1",                      //name of the player instance
						autohideControls:5,                          //autohide HTML5 player controls
						hideControlsOnMouseOut:"No",                 //hide HTML5 player controls on mouse out of the player: "Yes","No"
						videoPlayerWidth:600,                       //fixed total player width
						videoPlayerHeight:320,                       //fixed total player height
						responsive:true,				             //this option will overwrite above videoPlayerWidth/videoPlayerHeight and set player to fit your div (parent) container: true/false
						playlist:"off",                   //choose playlist type: "Right playlist", "Off"
						playlistScrollType:"light",                  //choose scrollbar type: "light","minimal","light-2","light-3","light-thick","light-thin","inset","inset-2","inset-3","rounded","rounded-dots","3d"
						playlistBehaviourOnPageload:"opened",		 //choose playlist behaviour when webpage loads: "closed", "opened" (not apply to Vimeo player)
						autoplay:false,                              //autoplay when webpage loads: true/false
						colorAccent:"#cc181e",                       //plugin colors accent (hexadecimal or RGB value - http://www.colorpicker.com/)
						vimeoColor:"00adef",                         //set "hexadecimal value", default vimeo color is "00adef"
						youtubeControls:"custom controls",			 //choose youtube player controls: "custom controls", "default controls"
						youtubeSkin:"dark",                          //default youtube controls theme: light, dark
						youtubeColor:"red",                          //default youtube controls bar color: red, white
						youtubeQuality:"default",                    //choose youtube quality: "small", "medium", "large", "hd720", "hd1080", "highres", "default"
						youtubeShowRelatedVideos:"No",				 //choose to show youtube related videos when video finish: "Yes", "No" (onFinish:"Stop video" needs to be enabled)
						videoPlayerShadow:"effect1",                 //choose player shadow:  "effect1" , "effect2", "effect3", "effect4", "effect5", "effect6", "off"
						loadRandomVideoOnStart:"No",                 //choose to load random video when webpage loads: "Yes", "No"
						shuffle:"No",				                 //choose to shuffle videos when playing one after another: "Yes", "No" (shuffle button enabled/disabled on start)
						posterImg:"<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>",//player poster image
						onFinish:"Autoplay",                  //"Play next video","Restart video", "Stop video",Autoplay
						nowPlayingText:"Yes",                        //enable disable now playing title: "Yes","No"
						fullscreen:"Fullscreen native",              //choose fullscreen type: "Fullscreen native","Fullscreen browser"
						rightClickMenu:true,                         //enable/disable right click over HTML5 player: true/false
						hideVideoSource:false,						 //option to hide self hosted video sources (to prevent users from download/steal your videos): true/false
						showAllControls:true,						 //enable/disable all HTML5 player controls: true/false
						allowSkipAd:true,                            //enable/disable "Skip advertisement" option: true/false
						infoShow:"no",                              //enable/disable info option: "Yes","No"
						shareShow:"no",                             //enable/disable all share options: "Yes","No"
						facebookShow:"no",                          //enable/disable facebook option individually: "Yes","No"
						twitterShow:"no",                           //enable/disable twitter option individually: "Yes","No"
						mailShow:"no",                              //enable/disable mail option individually: "Yes","No"
						facebookShareName:"",      //first parametar of facebook share in facebook feed dialog is title
						facebookShareLink:"",  //second parametar of facebook share in facebook feed dialog is link below title
						facebookShareDescription:"",
						facebookSharePicture:"", //fourth parametar in facebook feed dialog is picture on left side
						twitterText:"video player",			 //first parametar of twitter share in twitter feed dialog is text
						twitterLink:"", //second parametar of twitter share in twitter feed dialog is link
						twitterHashtags:"wordpressvideoplayer",		 //third parametar of twitter share in twitter feed dialog is hashtag
						twitterVia:"Creative media",				 //fourth parametar of twitter share in twitter feed dialog is via (@)
						googlePlus:"", //share link over Google +
						logoShow:"Yes",                              //"Yes","No"
						logoClickable:"Yes",                         //"Yes","No"
						logoPath:"http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/DYN-logo-white-mini1.png",             //path to logo image
						logoGoToLink:"<?php echo home_url(); ?>",       //redirect to page when logo clicked
						logoPosition:"bottom-right",                  //choose logo position: "bottom-right","bottom-left"
						embedShow:"No",                             //enable/disable embed option: "Yes","No"
						embedCodeSrc:"www.yourwebsite.com/videoplayer/index.html", //path to your video player on server
						embedCodeW:"746",                            //embed player code width
						embedCodeH:"420",                            //embed player code height
						embedShareLink:"www.yourwebsite.com/videoplayer/index.html", //direct link to your site (or any other URL) you want to be "shared"
						youtubePlaylistID:"",                        //automatic youtube playlist ID (leave blank "" if you want to use manual playlist) LL4qbSRobYCjvwo4FCQFrJ4g
						youtubeChannelID:"",                         //automatic youtube channel ID (leave blank "" if you want to use manual playlist) UCHqaLr9a9M7g9QN6xem9HcQ



						//manual playlist123
						videos:[
							{

								show: "<?php echo $show; ?>",
								videoType:"HTML5",
								title:"<?php echo get_the_title($postID); ?>",
								youtubeID:"0dJO0HyE8xE",
								vimeoID:"119641053",
								mp4:"<?php echo $videourlssroot; ?>",
								imageUrl:"<?php echo wp_get_attachment_url( get_post_thumbnail_id($postID) ); ?>",
								imageTimer:4,
								prerollAD:"No",
								prerollGotoLink:"http://www.doityourselfnation.org/bit_bucket/",
								preroll_mp4: "<?php echo $videourlads; ?>",
								prerollSkipTimer:5,
								midrollAD:"<?php echo $showad; ?>",
								midrollAD_displayTime:"00:00",
								midrollGotoLink:"http://www.doityourselfnation.org/bit_bucket/",
								midroll_mp4:"<?php echo $videourl; ?>",
								midrollSkipTimer:5,
								postrollAD:"No",
								postrollGotoLink:"http://www.doityourselfnation.org/bit_bucket/",
								postroll_mp4:"<?php echo $videourl; ?>",
								postrollSkipTimer:5,
								popupImg:"<?php echo $item_url;?>",
								popupAdShow:"<?php echo $showad; ?>",
								popupAdStartTime:"00:05",
								popupAdEndTime:"00:25",
								popupAdGoToLink:"http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $item_id_b; ?>",
								description:"Video description goes here.",
								thumbImg:"<?php echo wp_get_attachment_url( get_post_thumbnail_id($postID) ); ?>",
								info:"Video info goes here.<br>This text can be <i>HTML formatted</i>, <a href='http://codecanyon.net/user/_zac_' target='_blank'><font color='008BFF'>find out more</font></a>.<br>You can disable this info window in player options. <br><br>Lorem ipsum dolor sit amet, eu pri dolores theophrastus. Posidonium vituperatoribus cu mel, cum feugiat nostrum sapientem ne. Vis ea summo persius, unum velit erant in eos, pri ut suas iriure euripidis. Ad augue expetendis sea. Ne usu saperet appetere honestatis, ne qui nulla debitis sententiae."
							}

						]
					});

					});
				});
			</script>
			
			<div style="width:100%;margin-top:10px;">
			<?php			
			
			if ($banners) {	?>	  
				<a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img id="<?php echo $postID ?>" class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
			<?php }

			?> </div>
		 
	</div>
				<!-- End Video Player -->



				<!-- Start Content -->
				<div class="entry grey-background">

					<p>By <a href="<?php echo $authorurl; ?>">
							<?php echo $book_author; ?>
						</a>
					</p>
					 
					<!-- Start Video Heading -->
					<div class="video-heading">
                     
						<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
					<div class="flag_post custom_btn"><span id="addflag-anchor<?php echo $postID; ?>">Flag</span></div>
                     
					 
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID . ' AND user_id = ' . $userId);
						//print_r($result);
						if (empty($result)){
							?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
						<?php }else{ ?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
					   <?php }  ?>

						<!-- Section for the favorite button -->
					 
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID . ' AND user_id = ' . $userId);
						if (empty($result)){
							?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
						<?php }else{ ?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
						  <?php }  ?>
						<!-- End favorite -->
                       
						<?php
						//Adding a filter for adding Review button
						//This will be added from dyn-review plugin
						$output = "";
						echo apply_filters( 'dyn_review_button', $output, $postID, "post" );
						?>
					 
						  
						<?php //Start of Section for the Donate Button get_current_user_id()
						$return_url = current_page_url()."?done=1";
						$cancel_url = current_page_url()."?done=0";
						$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_current_user_id()." and status=1");
						$paypalacc='';
						if($datapaypal){
							foreach ($datapaypal as $reg_datagen){
								$paypalacc=$reg_datagen->paypal_email;
							}
						}
						if($paypalacc !==''){ ?>
							&nbsp;&nbsp;
							<button type="button" class="btn btn-primary" id="donated<?php echo $postID; ?>" data-toggle="modal" data-target="#model_donate">Donate</button>
							<!--Donate Popup-->
							<div class="modal" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
										</div>
										<div class="modal-body">
											<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="lc" value="US">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<div class="form-group">
													<span for="amount" class="control-label col-sm-4">Amount:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="amount" required >
													</div>
												</div>
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<input type="hidden" name="on1" value="Message" required>
												<div class="form-group">
													<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os1" maxlength="300" size="40">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						<?php } 
						// End Donate Button Section ?>
<script>
jQuery(document).ready(function($) {
	$('.video-heading').on('click', '#donated<?php echo $postID; ?>', function( e ) {
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not donate Yourself!");
			}else {
		$('div#model_donate').attr('style', 'display:block');
			}
		//$('div#model_donate').removeClass('modal');
	});
	$('.modal-header').on('click', 'button.close', function( e ) {
		$('div#model_donate').attr('style', 'display:none');
		//$('div#model_donate').removeClass('modal');
	});
	
});
</script>
						<div class="clear"></div>
					</div>
					<!-- End Video Heading -->

					<!-- Start Sharer -->
					<div class="clear"></div>

<!-- Modal -->
<div class="modal fade" id="model_flag<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="url<?php echo $postID; ?>" value="<?= get_permalink($postID);?>">
					<textarea rows="10" id="reason<?php echo $postID; ?>" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>

<script>
	var $ = jQuery.noConflict();
	
	/* $(document).ready(function(){
		CKEDITOR.inline( 'reason' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';    
			config.fontSize_defaultLabel = '44px';
		};
	}); */
	
	$('#addflag-submit<?php echo $postID; ?>').live('click',function(e){
		
		//$('#addflag-form').submit();
		//$("form").serialize()
		var $ = jQuery.noConflict();
		
		var reason = $('#reason<?php echo $postID; ?>').val();
		var url = $('#url<?php echo $postID; ?>').val();
		var post_id_flag = "<?php echo $postID; ?>";
		$.ajax({
			type: "POST",  
			url: "<?php echo admin_url('admin-ajax.php'); ?>",  
			data: {action:'user_mark_flag',reason:reason,url:url,post_id_flag:post_id_flag},
			success : function(data){
				var modelhtml = $('#model_flag<?php echo $postID; ?> .modal-content').html();
				$('#model_flag<?php echo $postID; ?> .modal-content').html(data);
				
				setTimeout(function(){
					$('#model_flag<?php echo $postID; ?>').modal('hide');
					$('#model_flag<?php echo $postID; ?> .modal-content').html(modelhtml);
				},3000)
			}
		});
		
	});
	
	$('#addflag-anchor<?php echo $postID; ?>').click(function(e){
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not flag Yourself.");
			}else {
		       var $ = jQuery.noConflict();
		       $('#model_flag<?php echo $postID; ?>').modal();
			}
	});
</script>
					<!-- End Sharer -->
					<?php
					$output = "";
					echo apply_filters( 'dyn_display_review', $output, $postID, "post" );
					?>
					<br>
					<div class="panel-group">
						<!-- START About us home page -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#paneldes<?php echo $postID;?>">
									<h4 class="panel-title">Description <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="paneldes<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-des<?php echo $postID;?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="desf-c">
										<?php
										if($postdataval){
											echo $postdataval;
										}
										else{
											if($theflContent==""){
												echo '<div class="content-empty">No Description Provided</div>';
											}else{
												echo $theflContent;
											}
										}

										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelref<?php echo $postID;?>">
									<h4 class="panel-title">Reference <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelref<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-ref<?php echo $postID;?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="ref-c">
										<?php
										$ref = get_post_meta($postID,'video_options_refr',true);
										if($ref){
											echo $ref;
										}else{
											echo 'No referance given';
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelshare<?php echo $postID;?>">
									<h4 class="panel-title">Share <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelshare<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
								<div id="option-social">
								<a class="opt-facebook" href= "https://www.facebook.com/sharer/sharer.php?u=<?php echo $pourl; ?>&t=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-facebook"></span>facebook</a>
								<a class="opt-twitter" href= "http://twitter.com/home?status=<?php echo $titlepo; ?>&nbsp;<?php echo $pourl; ?>" target="_blank"><span class="socicon-twitter"></span>twitter</a>
								<a class="opt-googleplus" href= "https://plus.google.com/share?url=<?php echo $pourl; ?>" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a>
								<a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-linkedin"></span>linkedin</a>
								<a class="opt-dig" href= "http://digg.com/submit?url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-digg"></span>dig</a>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END About us home page -->
				</div>
				<script>
					var $ = jQuery.noConflict();
					$(document).ready(function(){
						var $ = jQuery.noConflict();

						$(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');

						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("hide.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');
						});
						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("show.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-up"></span>');
						});
						
									$('#edit-des<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();

			e.preventDefault();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Description');
			$('#postdataname<?php echo $postID; ?>').val('desc');

			$('.cke_textarea_inline').html($('#desf-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#edit-ref<?php echo $postID; ?>').click(function(e){
			e.preventDefault();

			var $ = jQuery.noConflict();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Reference');
			$('#postdataname<?php echo $postID; ?>').val('refe');

			$('.cke_textarea_inline').html($('#ref-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#desref-submit<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();
			$('#desref-form<?php echo $postID; ?>').submit();
		});

		
		$('#endorse_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookendorsementsssss',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								if (data.match(/endorsed/g)){
									//alert(data);
					$('#endorse_button<?php echo $postID; ?>').html('<span>Endorsed!</span>');
					$('#endorse_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
				if (data.match(/deleteend/g)){
					jQuery('#endorse_button<?php echo $postID; ?>').html('<span>Endorse</span>');
					jQuery('#endorse_button<?php echo $postID; ?>').css('background-color','#000');
				}
							}
						})
			}
		});
		
		$('#favorite_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookfavoritefeed',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								  // alert(data);
								if (data.match(/favorite/g)){
									//alert(data);
									jQuery('#favorite_button<?php echo $postID; ?>').html('<span>Favorite</span>');
					jQuery('#favorite_button<?php echo $postID; ?>').css('background-color','#ff8a8a');
				
				}
				if (data.match(/nsotsafev/g)){
						$('#favorite_button<?php echo $postID; ?>').html('<span>Un-favorite</span>');
					$('#favorite_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
							}
						})
			}
			  
		});
		
		$('#tags-edit<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		$('#tags-val<?php echo $postID; ?>').hide();
		$('.tags-forminput<?php echo $postID; ?>').show();
	});
	$('#tagcancel<?php echo $postID; ?>').click(function(e){
				e.preventDefault();
				$('.tags-forminput<?php echo $postID; ?>').hide();
			});
	$('#tagsave<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		
		var tags_postid = $('#tags_postid<?php echo $postID; ?>').val();
		var tagsinput = $('#tagsinput<?php echo $postID; ?>').val();
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
			}).done(function(data) {
				location.reload();
		});
		
		$('.tags-forminput<?php echo $postID; ?>').hide();
		$('#tags-val<?php echo $postID; ?>').show();
		
	});

					})
				</script>

				<script>
					var $ = jQuery.noConflict();

					$('.ref-more').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.ref-less').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});

					$('.des-more').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.des-less').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});
				</script>

				<div class="clear"></div>
			</div>
			<!-- End Content -->
			<!-- Start Content -->
			<ul class="stats sicon grey-background">
			  
				<li><i class="fa fa-eye"></i> <?php videopress_countviews( $postID);  ?></li>
			   
			  
				<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID);
					echo count($result); ?> Endorsments</li>
		 
          		   
				<!--favourite section -->
				<li><i class="fa  fa-heart"></i>
					<?php
					$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID;
					$result1 = $wpdb->get_results($sql_aux);
					echo count($result1);
					?> Favorites</li>
			 
				<!--end favorite-->
				 	
				<li><i class="fa fa-comments"></i> <?php echo get_comments_number( $postID ) ;?> Comment</li>
			 
				 
				<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U',$postID), current_time('timestamp') ) . ' ago'; ?></li>
				 
			 
				<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', $postID, "post"); ?></li>
				 
			 
				<li><i class="fa fa-download"></i><?php echo apply_filters( 'dyn_number_of_download', $postID); ?></li>
				 
			</ul>
			<div class="clear"></div>
            
			<?php if( vp_option('vpt_option.show_user') == '1' ){ ?>
				<div class="post-by">
					<a href="<?php echo get_author_posts_url( $post_author ); ?>">
					<img src="<?php echo $user_img_src; ?>" class="avatar user-37-avatar avatar-50 photo" width="50" height="50" alt="P"></a>
					<a href="<?php echo get_author_posts_url( $post_author ); ?>" class="post-by-link"><?php echo $book_author; ?></a>
					<?php echo '<div class="post-by-vid-count">'.count_user_posts( $post_author ).' Videos Uploaded</div>'; ?>
					<div class="clear"></div>
				</div>
			 <?php } ?>

			<?php
			// Link Pages Navigation
			$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
			wp_link_pages( $args );


			// Display the Tags
			$tagval = '';?>
			<?php $all_tags = array();?>
			<?php $all_tags = get_the_tags($postID);?>
			<?php if($all_tags){
				$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
			}

			if($userId == get_current_user_id()){
				$before1 = '<span><i class="fa fa-tags"></i>Tags';
				$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit'. $postID .'"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
			}
			$before = '<span id="tags-val">';

			$after = '</span>';


			?>
          
			<?php if( is_user_logged_in() ){
				$profile_id = get_current_user_id();
				$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =2  ORDER BY id DESC");
				if($data){ ?>
					<div class="addto_playlist grey-background padding5_10">
						<a herf="#" id="fileplaytest<?php echo $postID; ?>" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
					</div>
			<?php } }  ?>
			<div class="spacer-20"></div><div class="post-tags grey-background padding5_10">
				<?= $before1;?>
				<?php
				$tagSelected = wp_get_post_tags($postID);
				$tagsNameArray = array();
				$tagsSlugArray = array();
				$tagsIDArray = array();
				foreach($tagSelected as $key=>$val)
				{
					$name = $val->name;
					$slug = $val->slug;
					$term_id = $val->term_id;
					$tagsNameArray[] = $name;
					$tagsSlugArray[] = $slug;
					$tagsIDArray[] = $term_id;
				}
				echo get_the_tags( $before,', ',$after );
 $tags = get_the_tags($postID); ?>
 
	 <span id="tags-val<?php echo $postID; ?>">

 
      <?php foreach($tags as $tag) :  ?>
 
    
        <a rel="tag"
            href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>">
                  <?php print_r($tag->name); ?>
         </a>   ,

      <?php endforeach; ?>
</span>	
 	
			</div>


			<?php

			if($all_tags){
				foreach($all_tags as $tag){
					$tagval .= $tag->name.',';
				};
			}

			rtrim($tagval, ",");

			if($userId == get_current_user_id()){
				?>

				<div class="tags-forminput<?php echo $postID; ?>" style="display:none;">

					<input name="tagsinput" class="tagsinput" id="tagsinput<?php echo $postID; ?>" value="<?= $tagval;?>">
					<input name="userid" type="hidden" id="tags_postid<?php echo $postID; ?>" value="<?= $postID ;?>">
					<button type="button" class="btn btn-success" id="tagsave<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="tagcancel<?php echo $postID; ?>">Cancel</button>

				</div>

			<?php } ?>

			<script>
				jQuery(document).ready(function(){
					jQuery('#songids').val('<?php echo $postID;?>')
				});

			</script>
			<!--
    <div class="post-categories">
    <span>
		<i class="fa fa-folder"></i>Categories&nbsp;&nbsp;
		<?php if(get_the_author_id() == get_current_user_id()){ ?>
			<a href="#" id="category-edit"><i class="fa fa-pencil-square-o"></i>Edit</a>
		<?php } ?>
	</span>

	<div id="cat-div">
	    <?php
			$postCats = wp_get_post_categories($postID);
			the_category('&nbsp;,&nbsp;');?>
	</div>

	<div id="cat-editdiv" style="display:none;">

	<?php

			$allcate = get_the_category();
			$post_catar = array();
			foreach($allcate as $cate){
				$post_catar[] = $cate->cat_ID;
			}

			//print_r($post_catar);

			// //$args = array(
			// 	'type'                     => 'post',
			// 	//'child_of'                 => 0,
			// 	//'parent'                   => '',
			// 	'orderby'                  => 'name',
			// 	'order'                    => 'ASC',
			// 	'hide_empty'               => 0,
			// 	'hierarchical'             => 1,
			// 	'exclude'                  => array('1','23'),
			// 	'include'                  => '',
			// 	'number'                   => '',
			// 	'taxonomy'                 => 'category',
			// 	//'pad_counts'               => false

			// );

			$categories = get_categories( $args );

			?>


		<ul>
			<?php foreach($categories as $cat ){ ?>
				<li>
					<input type="checkbox" name="cat_c[]" value="<?= $cat->cat_ID; ?>" <?= (in_array($cat->cat_ID, $post_catar))? 'checked':'';?>>
					<?= $cat->name; ?>
				</li>
			<?php } ?>
		</ul>
		<input type="hidden" name="post_id" value="<?php the_ID(); ?>">

		<button type="button" class="btn btn-info" id="cat-save">Save</button>
		<button type="button" class="btn btn-primary" id="cat-cncl">Cancel</button>
	</div>

    </div>

    -->

			<div class="clear"></div>
			<!-- End Content -->
        
			<?php if(function_exists("display_saic_feeds")) { echo display_saic_feeds($postID);} ?>
		  
			<!-- Start Related Videos -->
			<div class="spacing-40"></div>
			<?php //get_template_part('includes/related-videos'); ?>
			<!-- End Related Videos -->


			<?php
			/* ================================================================== */
			/* End of Loop */
			/* ================================================================== */
			//endwhile;
			?>


		</div>
		<!-- End Video Part -->

		

		
	</div>
	<!-- End Entries -->


	<!-- Widgets -->
	<!--<?php //get_sidebar(); ?>--
    <!-- End Widgets -->

	<div class="clear"></div>
	</div>
	<!--
    <div class="container p_90">
        <div class="grid-3-4 centre-block">
        <div class="grid-3-4-sidebar single-page">

        </div>
        </div>
    </div>
    -->
	<div class="spacing-40"></div>
	<!-- End Content -->
 

	<!-- Modal -->
	<div class="modal fade" id="modeldesref_section<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel<?php echo $postID; ?>">Add Description</h4>
				</div>
				<div class="modal-body">
					<form method="post" id="desref-form<?php echo $postID; ?>">
						<input type="hidden" name="postdataname<?php echo $postID; ?>" id="postdataname<?php echo $postID; ?>">
						<input type="hidden" name="postid<?php echo $postID; ?>" id="postid" value="<?= $postID;?>">
						<div id="">
							<textarea rows="10" id="postdataval<?php echo $postID; ?>" name="postdataval<?php echo $postID; ?>"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="desref-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Lightbox popup -->
	<div id="lightbox_file">
		<?php foreach ($file_recommendation as $single_file) { ?>
			<div id="show_detail_<?php echo $single_file->ID; ?>" class="lightbox" style="display: none;">
				<div class="container">
					<div class="img-represent">
						<?php echo apply_filters( 'dyn_file_image', $output, $single_file->ID ); ?>
						<h4><?php echo $single_file->post_title; ?></h4>
					</div>
					<ul class="detail-info">
						<li><label class="detail-title">By:</label> <span><?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?></span></li>
						<li><?php $output = "";echo apply_filters( 'dyn_display_review', $output, $single_file->ID, "post" );?></li>
						<li><label class="detail-title">Description:</label><?php echo wp_trim_words( $single_file->post_content, 15, '[...]' ); ?></li>
						<li><label class="detail-title">Reference:</label> No reference given</li>
					</ul>
					<ul class="stat">
						<li><i class="fa fa-eye"><?php echo videopress_countviews($single_file->ID); ?></i></li>
						<li><i class="fa fa-wrench"><?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_file->ID);echo count($result); ?> Endorsments</i></li>
						<li><i class="fa fa-heart">
								<?php
								$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_file->ID;
								$result1 = $wpdb->get_results($sql_aux);
								echo count($result1);
								?> Favorites

							</i></li>
						<li><i class="fa fa-star-o">
								<?php echo apply_filters('dyn_number_of_post_review', $single_file->ID, "post"); ?>
							</i></li>
						<li><i class="fa fa-download">
								<?php
								$download = get_post_meta( $single_file->ID, 'dyn_download_count', true);
								if($download == ""){echo '0 Download';}else{echo $download. " Downloads";}
								?>
							</i></li>
					</ul>
					<a class="close-lightbox">Close</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<!-- End Lightbox popup -->
<?php
//$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId  AND type =1 ORDER BY id DESC");
//$data_file = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =2 ORDER BY id DESC");
$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =3 ORDER BY id DESC");

?>	
<!-- file model -->
<div class="modal fade" id="filetest_mod<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Files to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file<?php echo $postID; ?>">
				<label>SELECT Playlist to add Files</label>
				<select name="plval">
				<?php foreach($data_book as $databook): ?>
					<option value="<?= $databook->id; ?>"><?= $databook->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-file<?php echo $postID; ?>').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest<?php echo $postID; ?>').click(function(e){
		jQuery('#filetest_mod<?php echo $postID; ?>').modal();
	});
</script> 

	 
<?php
	
}


function show_product_feed($postIDs,$type,$user_check_id){
	global $wpdb;
	$postby = $type;
	$postID = $postIDs; 
	$user_action_id = $user_check_id;
	
$postId=get_the_ID();
$uploaded_type = get_post_meta( $postID, 'dyn_upload_type', true );
$uploaded_value = get_post_meta( $postID, 'dyn_upload_value', true );
$units_sold = get_post_meta( $postID, 'total_sales', true );
?>
	<style>
		.flag_post
		{
			font-size: 14px !important;
		}
		#endorse_wrapper
		{
			font-size: 14px !important;
		}
		#favorite_wrapper
		{
			font-size: 14px !important;
		}
		.spaceabove {
			margin-top: 4px !important;
		}
		body{    background-color: #e9ebee;}
	</style>
<?php if(!is_user_logged_in()): ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
<?php endif; ?>
<?php



$postdataval = false;
if(isset($_POST['postdataval'.$postID]) && $_POST['postdataval'.$postID]!=''){
	if($_POST['postdataname'.$postID] == 'desc'){
		$postdataval = $_POST['postdataval'.$postID];
		$my_post = array(
			'ID'           => $_POST['postid'.$postID],
			'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'.$postID] == 'refe'){
		update_post_meta($_POST['postid'.$postID], 'video_options_refr', $_POST['postdataval'.$postID]);
	}
}

?>
	<!-- Start Content single-dyn-file -->
	<div class="container p_90">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">
		<!-- Start Entries -->
		<div class="grid-3-4 centre-block">
			<!-- Video part-->
			<div class="grid-3-4-sidebar single-page">
				<?php
				if( isset($_REQUEST['done']) and $_REQUEST['done'] == 1 )
				{
					?>
					<div style="color: blue; font-size: 14pt; font-weight: bold;">
						Thanks for donating the amount.
					</div>
					<?php
				}
				else if( isset($_REQUEST['done']) and $_REQUEST['done'] == 0 )
				{
					?>
					<div style="color: red; font-size: 14pt; font-weight: bold;">
						There is some error in server..Please contact with site administrator
					</div>
					<?php
				}
				else
				{ }
				/* ================================================================== */
				/* Start of Loop */
				/* ================================================================== */
				//while (have_posts()) : the_post();

				// this next bit sets a cookie for related videos
				$first_category = get_the_category();
				$bpost=get_post($postID);
				$title = $post->post_title;
				$post_author     = $bpost->post_author;
				$book_author = get_the_author_meta( 'display_name', $post_author);
	            $userId = get_post_field( 'post_author', $postID );
					$profile_img = get_user_meta( $userId, 'profile_pic_meta', true );
					
				$athor_action = get_author_posts_url( $user_action_id ); //$user_action_id
				$profile_img_action = get_user_meta( $user_action_id, 'profile_pic_meta', true );
				$book_author_action = get_the_author_meta( 'display_name', $user_action_id);
				//$author_obj = get_user_by('id', 1);
				
				$authorurl = get_author_posts_url( $userId ); 
				$theflContent=$bpost->post_content;
				$titlepo = urlencode(html_entity_decode($title));
				//get_permalink( $postID)
				$pourl = urlencode( get_permalink( $postID) );
		if($profile_img_action){
			$user_img_src = site_url() . '/profile_pic/' . $profile_img_action;
		}else{
			$user_img_src = get_template_directory_uri().'/images/no-image.png';
		}
			
		//$display_template .= "<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src='". $user_img_src."' class='layout-3-thumb' >";
		
		//$display_template .= "<h4 style='padding-left:10px; color: steelblue;font-size: 15px; font-weight: 600; margin-top:0;margin-bottom:2px;'><span style='color:#222;'>".$book_author." </span><span style='color:#606060;font-weight: 600;'> added</span> <a href='". get_permalink( $postID) ."'> ".$title." </a></h4>";
		
				?>
				<script>
					jQuery(document).ready(function(){
						jQuery.ajax({
							url: "<?php echo get_home_url() ?>/diynation_related_videos_cookie.php",
							type: "POST",
							data: {'category': '<?php echo $first_category[0]->term_id; ?>'}
						}).done(function(data) {
							//console.log("Current category id: " + data);
						});
					});
				</script>
				<?php // end set cookie for related videos ?>


				<?php if($userId == get_current_user_id()){ ?>
				<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" id="title-naz<?php echo $postID; ?>" style='width:80%'><div style="width: auto;float: left;" id="titleafter-edrit<?php echo $postID; ?>"><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby;?></span><a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></div>&nbsp;&nbsp;<a href="#" id="title-edit<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i>Edit</a></h6>
				<?php } else {
					?>
					<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src="<?php echo $user_img_src; ?>" class='layout-3-thumb' >
					<h6 class="video-title" style='width:80%'><span style='color:#222;'><a href="<?php echo $athor_action; ?>"><?php echo $book_author_action; ?></a></span><span style='color:#606060;font-weight: 600;'> <?php echo $postby;?></span> <a href="<?php echo get_permalink( $postID); ?>"> <?php echo get_the_title($postID); ?> </a></h6>
				 
					<?php
				} ?>
				<div id="title-div<?php echo $postID; ?>" style="display:none;">
					<input type="text" name="title-val<?php echo $postID; ?>" value="<?php echo get_the_title($postID);  ?>">
					<input type="hidden" name="post-val<?php echo $postID; ?>" value="<?php echo $postID; ?>">
					<button type="button" class="btn btn-info" id="title-save<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="title-cncl<?php echo $postID; ?>">Cancel</button>
					<br>
				</div>

                <script>
				jQuery(document).ready(function($) {
					$('#title-edit<?php echo $postID; ?>').click(function(){
						$('#title-naz<?php echo $postID; ?>').hide();
						$('#title-div<?php echo $postID; ?>').show();
					});

					$('#title-cncl<?php echo $postID; ?>').click(function(e){
						e.preventDefault();

						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();
					});


					$('#title-save<?php echo $postID; ?>').click(function(e){
						e.preventDefault();


						$('#title-div<?php echo $postID; ?>').hide();
						$('#title-naz<?php echo $postID; ?>').show();

						var post_title = jQuery('input[name="title-val<?php echo $postID; ?>"]').val();
						var post_id = jQuery('input[name="post-val<?php echo $postID; ?>"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
								$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
							}
						})
					});
					 
				});

			</script>

				<!-- Start Video Player -->
				<?php // get_template_part('includes/file-display'); 
				?>
				<div class="vid-single">

	<div class="file-container">
		<div id="product-feed" class="image-holder" style="text-align: center;">
		<div class="col-md-12  col-sm-12 col-lg-12 col-xs-12" >
		  <div class="col-md-8  col-sm-12" id="images">
		<?php
		$product = new WC_Product($postID);
					if ( ! $product->is_purchasable() ) {
						return;
					}
					if ( has_post_thumbnail($postID) ) {
						$image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
						$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
						$image         = get_the_post_thumbnail( $postID, apply_filters( 'shop_thumbnail', 'shop_single' ), array(
								'title'	=> get_the_title( get_post_thumbnail_id() ),
								 'style' => 'max-height: 300px;',
						) );

						$attachment_count = count( $product->get_gallery_attachment_ids() );

						if ( $attachment_count > 0 ) {
							$gallery = '[product-gallery]';
						} else {
							$gallery = '';
						}

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><a href="%s" itemprop="image" class="woocommerce-main-image zoom"  title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a></div>', $image_link, $image_caption, $image ), $postID );

					} else {

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><img src="%s" alt="%s" / style=" max-height: 300px;"></div>', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $postID );

					}
					?>
					
					
					<?php
					
                
				 
					$attachment_ids = $product->get_gallery_attachment_ids();

					if ( $attachment_ids ) {
						$loop 		= 0;
						$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
						?>
						<div class="thumbnails"><?php

							foreach ( $attachment_ids as $attachment_id ) {

								$classes = array( 'zoom' );

								if ( $loop === 0 || $loop % $columns === 0 )
									$classes[] = 'first';

								if ( ( $loop + 1 ) % $columns === 0 )
									$classes[] = 'last';

								$image_link = wp_get_attachment_url( $attachment_id );

								if ( ! $image_link )
									continue;

								$image_title 	= esc_attr( get_the_title( $attachment_id ) );
								$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

								$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
										'title'	=> $image_title,
										'alt'	=> $image_title,
										'width' => '50px'
								) );

								$image_class = esc_attr( implode( ' ', $classes ) );

								echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s"  data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $postID, $image_class );

								$loop++;
							}

							?></div>
						<?php
					}
					 
					?>
				</div>
			  	<div class="product-group">
		<?php if ( $product->is_on_sale() ) : ?>

    <?php echo '<span class="fgjieryhfe" > ' ; echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . __( 'Sale!', 'woocommerce' ) . '</span>', $post, $product );  echo '</span>';?>

<?php endif; ?>
<script>
    $( document ).ready(function() {
        $('.onsale').css("left", "75em");
        $('.onsale').css("top", "0em");
    });
</script>
	 <div itemprop="offers" class="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price"><?php echo $product->get_price_html(); ?></p>

	<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>
<div itemprop="description" class="short_description">
<?php $text_excerpt = apply_filters('the_excerpt', get_post_field('post_excerpt', $postID)); ?>
	<?php echo apply_filters( 'woocommerce_short_description', $text_excerpt ) ?>
</div>
<p class="stock in-stock"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo $product->get_stock_quantity(); ?> in stock</p>
	 </div>	 </div>	
	
	
	
            
        
	 </div>
		<div style="width:100%;margin-top:10px;">

			<?php
			$country_id = oiopub_settings::get_user_country_index();

			if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
				$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

			} else {
				$queryyy = " ";
			}
			$tagSelected = wp_get_post_tags($post->ID);
			/*$tagsNameArray = array();
			$tagsSlugArray = array();
			$tagsIDArray = array();
			$tags_str = '';
			foreach($tagSelected as $tag)
			{
				$tags_str .= "'".$tag->name."',";
			}
			$tags_str = substr($tags_str, 0, -1);
						global $wpdb;
			$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
			if($ptags) {
				$ptagname = '';
				$ptagpurchaseid = '';foreach ($ptags as $ptag) {

					$ptagname = $ptag->tag;
					$ptagpurchaseid = $ptag->purchase_id;
				}
				if($ptags)	{$bagfhfa = "AND id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


					global $wpdb;
					$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
					$tagname = '';
					$tagpurchaseid = '';
					foreach ($tags as $tag) {

						$tagname = $tag->tag;
						$tagpurchaseid = $tag->purchase_id;
					}

					if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
}
			}*/
	$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 5 and item_status=1 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


			$bannerurl = '';
			$bannerid = '';
			$countryinbase = '';
			foreach ($banners as $banner) {

				$bannerurl = $banner->item_url;
				$bannerid = $banner->item_id;
			}


			?>
			<?php
			if ($banners) {
				?>	  <a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img id="<?php echo $postID ?>" class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
			<?php }

			?> </div>
		 
	</div>
				<!-- End Video Player -->



				<!-- Start Content -->
				<div class="entry grey-background">

					<p>By <a href="<?php echo $authorurl; ?>">
							<?php echo $book_author; ?>
						</a>
					</p>
					 
					<!-- Start Video Heading -->
					<div class="video-heading">
                  
						<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
					<div class="flag_post custom_btn"><span id="addflag-anchor<?php echo $postID; ?>">Flag</span></div>
                
					 
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID . ' AND user_id = ' . get_current_user_id());
						//print_r($result);
						if (empty($result)){
							?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
						<?php }else{ ?>
							<div id="endorse_wrapper"><div id="endorse_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
						<?php } ?>
                
					 
						<!-- Section for the favorite button -->
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID . ' AND user_id = ' . get_current_user_id());
						if (empty($result)){
							?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
						<?php }else{ ?>
							<div id="favorite_wrapper"><div id="favorite_button<?php echo $postID; ?>" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
					  <?php }  ?>
						<!-- End favorite -->
                       
						<?php
						//Adding a filter for adding Review button
						//This will be added from dyn-review plugin
						$output = "";
						echo apply_filters( 'dyn_review_button', $output, $postID, "post" );
						?>
				 
				 
						 
                          <div id="favorite_wrapper" class="form-add"><div>
					<span>
					<?php
					$product = new WC_Product($postID);
					if ( ! $product->is_purchasable() ) {
						return;
					}

					?>

						<?php
						// Availability
						$availability      = $product->get_availability();
						$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

						echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
						?>

						<?php if ( $product->is_in_stock() ) : ?>

							<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

							<form class="cart" method="post" enctype='multipart/form-data'>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

								<button type="submit" class="single_add_to_cart_button button alt custom_btn"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

								<?php
								if ( ! $product->is_sold_individually() ) {
									woocommerce_quantity_input( array(
											'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
											'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
											'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
									) );
								}
								?>

								<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />



								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

						<?php endif; ?>

					</span>
							</div></div>
						<div class="clear"></div>
					</div>
					<!-- End Video Heading -->

					<!-- Start Sharer -->
					<div class="clear"></div>

<!-- Modal -->
<div class="modal fade" id="model_flag<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="url<?php echo $postID; ?>" value="<?= get_permalink($postID);?>">
					<textarea rows="10" id="reason<?php echo $postID; ?>" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>

<script>
	var $ = jQuery.noConflict();
	
	/* $(document).ready(function(){
		CKEDITOR.inline( 'reason' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';    
			config.fontSize_defaultLabel = '44px';
		};
	}); */
	
	$('#addflag-submit<?php echo $postID; ?>').live('click',function(e){
		//$('#addflag-form').submit();
		//$("form").serialize()
		var $ = jQuery.noConflict();
		
		var reason = $('#reason<?php echo $postID; ?>').val();
		var url = $('#url<?php echo $postID; ?>').val();
		var post_id_flag = "<?php echo $postID; ?>";
		
		$.ajax({
			type: "POST",  
			url: "<?php echo admin_url('admin-ajax.php'); ?>",  
			data: {action:'user_mark_flag',reason:reason,url:url,post_id_flag:post_id_flag},
			success : function(data){
				var modelhtml = $('#model_flag<?php echo $postID; ?> .modal-content').html();
				$('#model_flag<?php echo $postID; ?> .modal-content').html(data);
				
				setTimeout(function(){
					$('#model_flag<?php echo $postID; ?>').modal('hide');
					$('#model_flag<?php echo $postID; ?> .modal-content').html(modelhtml);
				},3000)
			}
		});
	});
	
	$('#addflag-anchor<?php echo $postID; ?>').click(function(e){
		var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not flag Yourself.");
			}else {
		var $ = jQuery.noConflict();
		$('#model_flag<?php echo $postID; ?>').modal();
			}
	});
</script>
					<!-- End Sharer -->
				 
					<?php
					$output = "";
					echo apply_filters( 'dyn_display_review', $output, $postID, "post" );
					?>
					 
					<br>
					<div class="panel-group">
						<!-- START About us home page -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#paneldes<?php echo $postID;?>">
									<h4 class="panel-title">Description <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="paneldes<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-des<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="desf-c">
										<?php
										if($postdataval){
											echo $postdataval;
										}
										else{
											if($theflContent==""){
												echo '<div class="content-empty">No Description Provided</div>';
											}else{
												echo $theflContent;
											}
										}

										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelref<?php echo $postID;?>">
									<h4 class="panel-title">Reference <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelref<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if($userId == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-ref<?php echo $postID; ?>"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="ref-c">
										<?php
										$ref = get_post_meta($postID,'video_options_refr',true);
										if($ref){
											echo $ref;
										}else{
											echo 'No referance given';
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelshare<?php echo $postID;?>">
									<h4 class="panel-title">Share <span class="pull-right panel-btn">show</span></h4>
								</a>
							</div>
							<div id="panelshare<?php echo $postID;?>" class="panel-collapse collapse">
								<div class="panel-body">
								<div id="option-social">
								<a class="opt-facebook" href= "https://www.facebook.com/sharer/sharer.php?u=<?php echo $pourl; ?>&t=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-facebook"></span>facebook</a>
								<a class="opt-twitter" href= "http://twitter.com/home?status=<?php echo $titlepo; ?>&nbsp;<?php echo $pourl; ?>" target="_blank"><span class="socicon-twitter"></span>twitter</a>
								<a class="opt-googleplus" href= "https://plus.google.com/share?url=<?php echo $pourl; ?>" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a>
								<a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-linkedin"></span>linkedin</a>
								<a class="opt-dig" href= "http://digg.com/submit?url=<?php echo $pourl; ?>&title=<?php echo $titlepo; ?>" target="_blank"><span class="socicon-digg"></span>dig</a>
								</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END About us home page -->
				</div>
				<script>
					var $ = jQuery.noConflict();
					$(document).ready(function(){
						var $ = jQuery.noConflict();

						$(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');

						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("hide.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');
						});
						$("#panelref<?php echo $postID;?>,#paneldes<?php echo $postID;?>,#panelshare<?php echo $postID;?>").on("show.bs.collapse", function(){
							$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-up"></span>');
						});
												$('#edit-des<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();

			e.preventDefault();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Description');
			$('#postdataname<?php echo $postID; ?>').val('desc');

			$('.cke_textarea_inline').html($('#desf-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#edit-ref<?php echo $postID; ?>').click(function(e){
			e.preventDefault();

			var $ = jQuery.noConflict();

			$('#myModalLabel<?php echo $postID; ?>').html('Add Reference');
			$('#postdataname<?php echo $postID; ?>').val('refe');

			$('.cke_textarea_inline').html($('#ref-c').html());

			$('#modeldesref_section<?php echo $postID; ?>').modal();
		});

		$('#desref-submit<?php echo $postID; ?>').click(function(e){
			var $ = jQuery.noConflict();
			$('#desref-form<?php echo $postID; ?>').submit();
		});

		$('#endorse_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookendorsementsssss',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								if (data.match(/endorsed/g)){
									//alert(data);
					$('#endorse_button<?php echo $postID; ?>').html('<span>Endorsed!</span>');
					$('#endorse_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
				if (data.match(/deleteend/g)){
					jQuery('#endorse_button<?php echo $postID; ?>').html('<span>Endorse</span>');
					jQuery('#endorse_button<?php echo $postID; ?>').css('background-color','#000');
				}
							}
						})
			}
			  
		});
		
		$('#favorite_button<?php echo $postID; ?>').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			var poster_id = "<?php echo $userId; ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			var post_id = <?php echo $postID; ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookfavoritefeed',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								  // alert(data);
								if (data.match(/favorite/g)){
									//alert(data);
									jQuery('#favorite_button<?php echo $postID; ?>').html('<span>Favorite</span>');
					jQuery('#favorite_button<?php echo $postID; ?>').css('background-color','#ff8a8a');
				
				}
				if (data.match(/nsotsafev/g)){
						$('#favorite_button<?php echo $postID; ?>').html('<span>Un-favorite</span>');
					$('#favorite_button<?php echo $postID; ?>').attr('style','background-color:#0F9D58');
				}
							}
						})
			}
			  
		});
		
		$('#tags-edit<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		$('#tags-val<?php echo $postID; ?>').hide();
		$('.tags-forminput<?php echo $postID; ?>').show();
	});
	$('#tagcancel<?php echo $postID; ?>').click(function(e){
				e.preventDefault();
				$('.tags-forminput<?php echo $postID; ?>').hide();
			});
	$('#tagsave<?php echo $postID; ?>').click(function(e){
		e.preventDefault();
		
		var tags_postid = $('#tags_postid<?php echo $postID; ?>').val();
		var tagsinput = $('#tagsinput<?php echo $postID; ?>').val();
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
			}).done(function(data) {
				location.reload();
		});
		
		$('.tags-forminput<?php echo $postID; ?>').hide();
		$('#tags-val<?php echo $postID; ?>').show();
		
	});

					})
				</script>

				<script>
					var $ = jQuery.noConflict();

					$('.ref-more').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.ref-less').click(function(e){
						e.preventDefault();
						$('.content-ref').html('<?= $ref_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});

					$('.des-more').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des; ?>');
						$(this).hide();
						$(this).next().show();
					});

					$('.des-less').click(function(e){
						e.preventDefault();
						$('.content-des').html('<?= $des_min; ?>');
						$(this).hide();
						$(this).prev().show();
					});
				</script>

				<div class="clear"></div>
			</div>
			<!-- End Content -->
			<!-- Start Content -->
			<ul class="stats sicon grey-background">
			 
				<li><i class="fa fa-eye"></i> <?php videopress_countviews( $postID);  ?></li>
          
			 
				<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID);
					echo count($result); ?> Endorsments</li>
			 	
				<!--favourite section -->
			 	
				<li><i class="fa  fa-heart"></i>
					<?php
					$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID;
					$result1 = $wpdb->get_results($sql_aux);
					echo count($result1);
					?> Favorites</li>
				 	
				<!--end favorite-->
			 		
				<li><i class="fa fa-comments"></i> <?php echo get_comments_number( $postID ) ;?> Comment </li>
			 
			 	
				<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U',$postID), current_time('timestamp') ) . ' ago'; ?></li>
			 
            			
				<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', $postID, "post"); ?></li>
			 	
				<!--<li><i class="fa fa-download"></i><?php echo apply_filters( 'dyn_number_of_download', $postID); ?></li>-->
			 	
				<li><i class="fa fa-shopping-bag" aria-hidden="true"></i><span id="units_sold-sapn"><span class="units_sold"> <?php echo $units_sold;?> Sold</span></span></li>
			  
			</ul>
			<div class="clear"></div>
           
			 
			<?php if( vp_option('vpt_option.show_user') == '1' ){ ?>
				<div class="post-by">
					<a href="<?php echo get_author_posts_url( $post_author ); ?>">
					<img src="<?php echo $user_img_src; ?>" class="avatar user-37-avatar avatar-50 photo" width="50" height="50" alt="P"></a>
					<a href="<?php echo get_author_posts_url( $post_author ); ?>" class="post-by-link"><?php echo $book_author; ?></a>
					<?php echo '<div class="post-by-vid-count">'.count_user_posts( $post_author ).' Videos Uploaded</div>'; ?>
					<div class="clear"></div>
				</div>
			 <?php }  ?>

			<?php
			// Link Pages Navigation
			$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
			wp_link_pages( $args );


			// Display the Tags
			$tagval = '';?>
			<?php $all_tags = array();?>
			<?php $all_tags = get_the_tags($postID);?>
			<?php if($all_tags){
				$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
			}

			if($userId == get_current_user_id()){
				$before1 = '<span><i class="fa fa-tags"></i>Tags';
				$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit'. $postID .'"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
			}
			$before = '<span id="tags-val">';

			$after = '</span>';


			?>
           
			<?php if( is_user_logged_in() ){
				$profile_id = get_current_user_id();
				$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =2  ORDER BY id DESC");
				if($data){ ?>
					<div class="addto_playlist grey-background padding5_10">
						<a herf="#" id="fileplaytest<?php echo $postID; ?>" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
					</div>
			<?php } }  ?>
			<div class="spacer-20"></div><div class="post-tags grey-background padding5_10">
				<?= $before1;?>
				<?php
				$tagSelected = wp_get_post_tags($postID);
				$tagsNameArray = array();
				$tagsSlugArray = array();
				$tagsIDArray = array();
				foreach($tagSelected as $key=>$val)
				{
					$name = $val->name;
					$slug = $val->slug;
					$term_id = $val->term_id;
					$tagsNameArray[] = $name;
					$tagsSlugArray[] = $slug;
					$tagsIDArray[] = $term_id;
				}
				echo get_the_tags( $before,', ',$after );
$tags = get_the_tags($postID); 				?>
				
  	
				<span id="tags-val<?php echo $postID; ?>">

 
      <?php foreach($tags as $tag) :  ?>
 
    
        <a rel="tag"
            href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>">
                  <?php print_r($tag->name); ?>
         </a>   ,

      <?php endforeach; ?>
</span>
 
			</div>


			<?php

			if($all_tags){
				foreach($all_tags as $tag){
					$tagval .= $tag->name.',';
				};
			}

			rtrim($tagval, ",");

			if($userId == get_current_user_id()){
				?>

				<div class="tags-forminput<?php echo $postID; ?>" style="display:none;">

					<input name="tagsinput" class="tagsinput" id="tagsinput<?php echo $postID; ?>" value="<?= $tagval;?>">
					<input name="userid" type="hidden" id="tags_postid<?php echo $postID; ?>" value="<?= $postID ;?>">
					<button type="button" class="btn btn-success" id="tagsave<?php echo $postID; ?>">Save</button>
					<button type="button" class="btn btn-primary" id="tagcancel<?php echo $postID; ?>">Cancel</button>
 
				</div>

			<?php } ?>

			<script>
				jQuery(document).ready(function(){
					jQuery('#songids').val('<?php echo $postID;?>')
				});

			</script>
			<!--
    <div class="post-categories">
    <span>
		<i class="fa fa-folder"></i>Categories&nbsp;&nbsp;
		<?php if(get_the_author_id() == get_current_user_id()){ ?>
			<a href="#" id="category-edit"><i class="fa fa-pencil-square-o"></i>Edit</a>
		<?php } ?>
	</span>

	<div id="cat-div">
	    <?php
			$postCats = wp_get_post_categories($postID);
			the_category('&nbsp;,&nbsp;');?>
	</div>

	<div id="cat-editdiv" style="display:none;">

	<?php

			$allcate = get_the_category();
			$post_catar = array();
			foreach($allcate as $cate){
				$post_catar[] = $cate->cat_ID;
			}

			//print_r($post_catar);

			// //$args = array(
			// 	'type'                     => 'post',
			// 	//'child_of'                 => 0,
			// 	//'parent'                   => '',
			// 	'orderby'                  => 'name',
			// 	'order'                    => 'ASC',
			// 	'hide_empty'               => 0,
			// 	'hierarchical'             => 1,
			// 	'exclude'                  => array('1','23'),
			// 	'include'                  => '',
			// 	'number'                   => '',
			// 	'taxonomy'                 => 'category',
			// 	//'pad_counts'               => false

			// );

			$categories = get_categories( $args );

			?>


		<ul>
			<?php foreach($categories as $cat ){ ?>
				<li>
					<input type="checkbox" name="cat_c[]" value="<?= $cat->cat_ID; ?>" <?= (in_array($cat->cat_ID, $post_catar))? 'checked':'';?>>
					<?= $cat->name; ?>
				</li>
			<?php } ?>
		</ul>
		<input type="hidden" name="post_id" value="<?php the_ID(); ?>">

		<button type="button" class="btn btn-info" id="cat-save">Save</button>
		<button type="button" class="btn btn-primary" id="cat-cncl">Cancel</button>
	</div>

    </div>

    -->

			<div class="clear"></div>
			<!-- End Content -->
          
			<?php if(function_exists("display_saic_feeds")) { echo display_saic_feeds($postID);} ?>
			 
			<!-- Start Related Videos -->
			<div class="spacing-40"></div>
			<?php //get_template_part('includes/related-videos'); ?>
			<!-- End Related Videos -->


			<?php
			/* ================================================================== */
			/* End of Loop */
			/* ================================================================== */
			//endwhile;
			?>


		</div>
		<!-- End Video Part -->

		

		
	</div>
	<!-- End Entries -->


	<!-- Widgets -->
	<!--<?php //get_sidebar(); ?>--
    <!-- End Widgets -->

	<div class="clear"></div>
	</div>
	<!--
    <div class="container p_90">
        <div class="grid-3-4 centre-block">
        <div class="grid-3-4-sidebar single-page">

        </div>
        </div>
    </div>
    -->
	<div class="spacing-40"></div>
	<!-- End Content -->
 

	<!-- Modal -->
	<div class="modal fade" id="modeldesref_section<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel<?php echo $postID; ?>">Add Description</h4>
				</div>
				<div class="modal-body">
					<form method="post" id="desref-form<?php echo $postID; ?>">
						<input type="hidden" name="postdataname<?php echo $postID; ?>" id="postdataname<?php echo $postID; ?>">
						<input type="hidden" name="postid<?php echo $postID; ?>" id="postid<?php echo $postID; ?>" value="<?= $postID;?>">
						<div id="">
							<textarea rows="10" id="postdataval<?php echo $postID; ?>" name="postdataval<?php echo $postID; ?>"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="desref-submit<?php echo $postID; ?>" class="btn btn-primary">Submit changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Lightbox popup -->
	<div id="lightbox_file">
		<?php foreach ($file_recommendation as $single_file) { ?>
			<div id="show_detail_<?php echo $single_file->ID; ?>" class="lightbox" style="display: none;">
				<div class="container">
					<div class="img-represent">
						<?php echo apply_filters( 'dyn_file_image', $output, $single_file->ID ); ?>
						<h4><?php echo $single_file->post_title; ?></h4>
					</div>
					<ul class="detail-info">
						<li><label class="detail-title">By:</label> <span><?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?></span></li>
						<li><?php $output = "";echo apply_filters( 'dyn_display_review', $output, $single_file->ID, "post" );?></li>
						<li><label class="detail-title">Description:</label><?php echo wp_trim_words( $single_file->post_content, 15, '[...]' ); ?></li>
						<li><label class="detail-title">Reference:</label> No reference given</li>
					</ul>
					<ul class="stat">
						<li><i class="fa fa-eye"><?php echo videopress_countviews($single_file->ID); ?></i></li>
						<li><i class="fa fa-wrench"><?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_file->ID);echo count($result); ?> Endorsments</i></li>
						<li><i class="fa fa-heart">
								<?php
								$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_file->ID;
								$result1 = $wpdb->get_results($sql_aux);
								echo count($result1);
								?> Favorites

							</i></li>
						<li><i class="fa fa-star-o">
								<?php echo apply_filters('dyn_number_of_post_review', $single_file->ID, "post"); ?>
							</i></li>
						<li><i class="fa fa-download">
								<?php
								$download = get_post_meta( $single_file->ID, 'dyn_download_count', true);
								if($download == ""){echo '0 Download';}else{echo $download. " Downloads";}
								?>
							</i></li>
					</ul>
					<a class="close-lightbox">Close</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<!-- End Lightbox popup -->
<?php
//$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId  AND type =1 ORDER BY id DESC");
$data_file = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =2 ORDER BY id DESC");
//$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =3 ORDER BY id DESC");

?>	
<!-- file model -->
<div class="modal fade" id="filetest_mod<?php echo $postID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Files to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file<?php echo $postID; ?>">
				<label>SELECT Playlist to add Files</label>
				<select name="plval">
				<?php foreach($data_file as $datafile): ?>
					<option value="<?= $datafile->id; ?>"><?= $datafile->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-file<?php echo $postID; ?>').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest<?php echo $postID; ?>').click(function(e){
		jQuery('#filetest_mod<?php echo $postID; ?>').modal();
	});
</script> 

	 
<?php
	
}

?>
	<?php
$big = 999999999; // need an unlikely integer
 
echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => ($bktotal/150)
) );
?>
<?php get_footer(); ?>