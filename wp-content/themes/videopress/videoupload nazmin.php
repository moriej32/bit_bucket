<?php error_reporting(0); /* Template Name: video upload nazmin */ ?>
<?php
get_header();
   /* echo "<pre>";
   print_r($_POST);
   print_r($_FILES);
   print_r($_REQUEST);
   echo "</pre>"; */
//error_reporting(0);
//error_reporting(E_ALL & ~E_NOTICE);
   ?>
<style>
/* progress bar */
#progress-wrp {
	
	height: 20px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #eee;
    box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
}
#progress-wrp .progress-bar{
	height: 20px;
    border-radius: 3px;
    background-color: #0099CC;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
	margin-bottom: 10px;
}
#progress-wrp .status{
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #000000;
}
#title {
    border: 1px solid #cccccc;
    border-radius: 4px;
    height: 40px;
    margin: 10px 0;
    width: 100%;
    
	font-size: 18px;
}
  .custom-post-type {
   clear: both;
   margin-bottom: 18px;
   margin-top: 20px;
   padding: 10px;
   width: 100%;
 
   }
   .tabs-panel{
   clear: both;
   }
 #tabsn{background: #f8f8f8;
    max-width: 80%;
    margin: 0 auto;
    padding: 15px;
    border: 1px solid #eeeeee;
    border-radius: 4px;}  
	
	#curtain {
	z-index: 100;
	display: none;
	position: absolute;
	top: 0;
	bottom: 0;
	right: 0;
	left: 0;
	opacity: 0;
	background: rgba(0,0,0,0.2);
}
#curtain .fb-spinner {
	position: fixed;
    width: 40px;
	height: 40px;
	top: 50%;
	margin-top: -20px;
}
#curtain .fb-spinner.not-fixed {
	position: absolute;
	left: 50% !important;
	margin-left: -20px;
}
#curtain .double-bounce1, #curtain .double-bounce2 {
	width: 100%;
	height: 100%;
	border-radius: 50%;
	background-color: #333;
	opacity: 0.6;
	position: absolute;
	top: 0;
	left: 0;

	-webkit-animation: bounce 2.0s infinite ease-in-out;
	animation: bounce 2.0s infinite ease-in-out;
}
#curtain .double-bounce2 {
	-webkit-animation-delay: -1.0s;
	animation-delay: -1.0s;
}
.revnue table td,
.revnue table th {
    border: 1px solid #333;
    padding: 10px;
}
.revnue table td input{
	width:161px;
}
.revnue table td a{
	color: #e00000;
}
#error-msg{
	color: red;
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.css" />
<?php 
	$current_user = wp_get_current_user();
	global $wpdb;	
	$check_marchant_paypal = $wpdb->get_row("SELECT paypal_email FROM ".$wpdb->prefix . "users_merchant_accounts WHERE user_id=".$current_user->ID);
	$check_marchant_revenue = $wpdb->get_row("SELECT revenue_status FROM ".$wpdb->prefix . "users_revenue_status WHERE user_id=".$current_user->ID);
	/* $check_revenue_table = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ad_revenue_share");
	foreach($check_revenue_table as $data){
		echo $data->postType.'<br/>';
		echo $data->authorID.'<br/>';
		echo $data->getPercentageUserID.'<br/>';
		echo $data->getPercentageValue.'<br/>';
		echo $data->postID.'<br/>';
	} */
?>
 <div class="container p_90">  
      <!-- Start Related Videos -->
   <div class="spacing-40"></div>
   
   <!-- Start Content -->
   <div class="container">
  
      <div class="col-md-12 col-sm-12">
         <div id="tabsn" style="min-height: 406px;">
             
			  <div id="output"><!-- error or success results --></div>
   <form method="post" action="<?php echo get_stylesheet_directory_uri();?>/videoupload.php" enctype="multipart/form-data" class="custom-post-type" id="uploadForm" >
               <div class="form-group">
                  <label>Post Title</label><br/>
                  <input type="text" id="title" value=""  size="40" class="form-control" name="post_title"  required />
                  <br/>
                  <span class="description">Description<br/>
				  <textarea class="form-control" id="descriptions"  placeholder="video description"  name="post_content"  rows="13"  ></textarea>
                  </span>
               </div>
			   <div class="form-group">
           
                  <b>Video Reference</b>
                  <textarea name="ref" id="ref" class="form-control" rows="3" placeholder="video reference text"></textarea>
                  <br>
                  
               </div>
              <div class="form-group">
               <label>Add Tags</label><br/>
               <input name="tagsinput" class="form-control" class="tagsinput" id="tagsinput" >
              
			   </div>
			   <div class="form-group">
               <label>Add Video File</label><br/>
                  <input class="vp-js-upload vp-button button" name="file" id="file" type="file" value="Choose File">
                  <br />
                  <div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>
                  <div id="output"><!-- error or success results --></div>

              </div>
              <div class="form-group">
                  <label for="dyn-thumbnail">Thumbnail:</label>
                  <div class="checkbox">
                     <label>Default
                     <input type="radio" name="dyn_thumbnail" class="dyn_thumbnail_checkbox" value="thumbnail_default" checked="checked" ></label>
                     <label>Custom Thumbnail
                     <input type="radio" name="dyn_thumbnail" class="dyn_thumbnail_checkbox" value="thumbnail_custom" ></label>
                  </div>
               </div>
               <div id="thumbnail_upload"></div>
               <div class="form-group">
                  <label for="dyn-tags"><br/>Add Background:</label>
                  <div class="checkbox">
                     <label>White Background
                     <input type="radio" name="dyn_background" class="dyn_checkbox" value="background_no" checked="checked" ></label>
                     <label>Profile Background
                     <input type="radio" name="dyn_background" class="dyn_checkbox" value="background_user" ></label>
                     <label>New Background
                     <input type="radio" name="dyn_background" class="dyn_checkbox" value="background_new" ></label>
                  </div>
               </div>
               <div id="upload_container"></div>
			   <?php if($check_marchant_paypal && $check_marchant_revenue->revenue_status==1){?>
			   <div class="form-group">
                  <label for="dyn-ad-revenue">Ad Revenue Share:</label>
                  <div class="checkbox">
                     <label>Yes
                     <input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox" value="yes"></label>
                     <label>No
                     <input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox" value="no" checked="checked"></label>
                  </div>
				  <div class="revnue" style="display:none">
				  <label for="revenue-paypal-email">Add Paypal Email</label>
				  <input type="email" required name="revenue-paypal-email" class="revenue-paypal-email" id="revenue-paypal-email">
				  <input type="button" value="Add" class="revenue-paypal">
				  <div id="error-msg"></div>
				  <div id="loading" style="display:none"><img id="loadingImage" src="<?php echo get_template_directory_uri();?>/images/ajax-loader.gif"></div>
				  <table id="author-lists" class="author-lists"><tr><th></th><th><b>Users</b></th><th><b> % of Revenue</b></th></tr><tr><td></td><td><p><?php echo $current_user->display_name;?></p></td><td><input id="chnl-perc-val" type="text" value="100" readonly />%</td></tr></table>
				  </div>
               </div>
			   <input type="hidden" name="counterval" id="counterval"/>
			   <?php } ?>
               <script type="text/javascript">
                  jQuery(document).ready(function() {
                  	jQuery("#upload_container").html('');
                  	jQuery("#thumbnail_upload").html('');
					jQuery("#error-msg").html('');
                  	jQuery("#revenue-table").html('');
                  	jQuery(".dyn_checkbox").click( function() {
                  		var test = $(this).val();
                  		if( test == 'background_new' ){
                  			jQuery("#upload_container").html('<div class="form-group"><label for="dyn_bg">Upload File:</label><input type="file" id="dyn_bg" name="dyn_bg" class="dyn_bg" accept="image/*"></div>');
                  		}else{
                  			jQuery("#upload_container").html('');
                  		}
                  	});					
                  	jQuery(".dyn_thumbnail_checkbox").click( function() {
                  		var thumbval = $(this).val();
                  		if( thumbval == 'thumbnail_custom' ){
                  			jQuery("#thumbnail_upload").html('<div class="form-group"><label for="dyn_thumb_file">Upload Thumbnail:</label><input type="file" id="dyn_thumb_file" name="dyn_thumb_file" class="dyn_thumb_file" accept="image/*"></div>');
                  		}else{
                  			jQuery("#thumbnail_upload").html('');
                  		}
                  	});
					var counter = 1;
					jQuery('.dyn_ad_revenue_checkbox').click(function(){
						var revenueval = $(this).val();
						if(revenueval == 'yes'){
							jQuery('.revnue').css('display','block');
							jQuery('.revenue-paypal').click(function(){
								email = $('.revenue-paypal-email').val();
								var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
								var result = pattern .test(email);
								if(result){
								jQuery('.revenue-paypal-email').val('');
								$("#loading").css('display','block');
								$("#author-lists").css('display','none');
								setTimeout(function(){											
								jQuery.post("<?php echo get_template_directory_uri();?>/get-author.php",{email:email,counter:counter},function(data){	
									if(data['author']){
										if(data['marchant_paypal'] && data['marchant_revenue']){	
										var newRow = jQuery('<tr id="row'+data['counter']+'"><td><a class="remove'+data['counter']+'">Remove</a></td><td><p id="authorbtn'+data['counter']+'">Author Name</p><input type="hidden" name="revusers'+data['counter']+'" class="authorid" value="'+data['authorID']+'"/></td><td><input type="number" class="num" id="numbersOnly'+data['counter']+ '" name="percentage-revenue'+data['counter']+ '" max="" min=""/>%<input type="hidden" id="numval'+data['counter']+'" name="perceval'+data['counter']+'"/></td></tr>');
										jQuery('table.author-lists').append(newRow);
										jQuery("#error-msg").html('');
										jQuery("#counterval").val(data['counter']);
										}else{
											jQuery("#error-msg").html("User is not allow for revenue share.");
										}
									}else{
										jQuery("#error-msg").html("No user Found. Please try again.");
									}
									jQuery("#authorbtn"+data['counter']).html(data['author']);
									jQuery("#numbersOnly"+data['counter']).attr('min',1);
									jQuery("#numbersOnly"+data['counter']).attr('max',80);
									jQuery(".remove"+data['counter']).click(function(){
										$("#row"+data['counter']).remove();
										checksum();
									});
									jQuery("#author-lists").on('change', '#numbersOnly'+data['counter'], function(){
										jQuery('#numval'+data['counter']).val(this.value);
										checksum();
									});
									function checksum(){
										var sum = 0;
										jQuery('.num').each(function(){
											sum += Number(jQuery(this).val());	
										});										
										jQuery('#chnl-perc-val').val(100 - sum);
									}
								},'json');
								$("#loading").css('display','none');
								$("#author-lists").css('display','block');
								},5000);								
								counter++;									
								}else{
									jQuery("#error-msg").html("Please enter valid Email address.");
								}
							});
						}else{
							jQuery('.revnue').css('display','none');
							jQuery("#error-msg").html('');
						}
					});					
                  });
               </script>
               <div class="form-group">
                  <label for="dyn-tags"><br/>Privacy Options:</label>
                  <div class="checkbox">
                     <label>Private
                     <input type="radio" id="privacy-radio" name="privacy-option" class="dyn-select-file" class="form-control" value="private">
                     </label>
                     <label>Public
                     <input type="radio" id="privacy-radio1" name="privacy-option" class="dyn-select-file" class="form-control" value="public">
                     </label>
                  </div>
                  <div style="display:none;" class="select-box">
                     <?php
                        echo '<label for="dyn-tags">Please select the User you want to give access</label><br><select id="tokenize" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
                         $args1 = array(
                          'role' => 'free_user',
                          'orderby' => 'id',
                          'order' => 'desc'
                          );
                         $subscribers = get_users($args1);
                         foreach ($subscribers as $user) {
                             if(get_current_user_id() == $user->id)
                          { }
                          else
                          {
                        	  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
                          }
                         }
                        echo '</select>';
                        ?>
                  </div>
                  <script type="text/javascript">
                     $(document).ready(function(){
                      $("#privacy-radio").click(function(){
                      $(".select-box").slideDown();
                      });
                      $("#privacy-radio1").click(function(){
                      $(".select-box").slideUp();
                      //$('select#select_users_list option').removeAttr("selected");
                      });
                     });
                     $('#tokenize').tokenize();
                     datas: "bower.json.php"
                  </script>
                  <style>.tokenize-sample { width: 300px }</style>
               </div>
               <input type="submit" value="Publish" tabindex="8" id="submit" name="upload_video" />
			    <div id="curtain" style="display:none"></div>
         </div>
         <input type="hidden" name="action" value="my_post_type" />
		
         </form>
      </div>
    <div class="clear"></div>
   </div>
   <div class="spacing-40"></div>
   <!-- bottom section -->
   <!-- RANDOM CATEGORIES -->
   <!-- end random categories section -->
   <!-- End Content -->
   <div class="clear"></div>
</div>
<div class="spacing-40"></div>
</div><!-- end 90 percent wrapper -->
  
 
<script type="text/javascript">
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';    
//configuration
var max_file_size 			= 2048576; //allowed file size. (1 MB = 1048576)
var allowed_file_types 		= ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg']; //allowed file types
var result_output 			= '#output'; //ID of an element for response output
var my_form_id 				= '#uploadForm'; //ID of an element for response output
var progress_bar_id 		= '#progress-wrp'; //ID of an element for response output
var total_files_allowed 	= 3; //Number files allowed to upload



//on form submit
$(my_form_id).on( "submit", function(event) { 
	event.preventDefault();
	var proceed = true; //set proceed flag
	var error = [];	//errors
	var total_files_size = 0;
	
	var percentageUserIDs = [];
	$('.authorid').each(function() {
		percentageUserIDs.push($(this).val());
	});
	
	$('#curtain').css( { display: 'block'} );
			$('#curtain').css('visibility', 'visible');
			$('#curtain').stop( true ).animate({ 'opacity': 1 }, 300);
		$('#curtain').find('.fb-spinner').addClass( 'not-fixed' );	
	//reset progressbar
	$(progress_bar_id +" .progress-bar").css("width", "0%");
	$(progress_bar_id + " .status").text("0%");
							
	if(!window.File && window.FileReader && window.FileList && window.Blob){ //if browser doesn't supports File API
		error.push("Your browser does not support new File API! Please upgrade."); //push error text
	}else{
		var total_selected_files = this.elements['file'].files.length; //number of files
		//alert(total_selected_files);
		//limit number of files allowed
		if(total_selected_files > total_files_allowed){
			error.push( "You have selected "+total_selected_files+" file(s), " + total_files_allowed +" is maximum!"); //push error text
			proceed = false; //set proceed flag to false
		}
		 //iterate files in file input field
		$(this.elements['file'].files).each(function(i, ifile){
			if(ifile.value !== ""){ //continue only if file(s) are selected
				/*if(allowed_file_types.indexOf(ifile.type) === -1){ //check unsupported file
					error.push( "<b>"+ ifile.name + "</b> is unsupported file type!"); //push error text
					proceed = false; //set proceed flag to false
				}*/

				total_files_size = total_files_size + ifile.size; //add file size to total size
			}
		});
		
		//if total file size is greater than max file size
		/*if(total_files_size > max_file_size){ 
			error.push( "You have "+total_selected_files+" file(s) with total size "+total_files_size+", Allowed size is " + max_file_size +", Try smaller file!"); //push error text
			proceed = false; //set proceed flag to false
		}*/
		
		var submit_btn  = $(this).find("input[type=submit]"); //form submit button	
		
		//if everything looks good, proceed with jQuery Ajax
		if(proceed){
			//submit_btn.val("Please Wait...").prop( "disabled", true); //disable submit button
			var form_data = new FormData(this); //Creates new FormData object
			var post_url = $(this).attr("action"); //get action URL of form			   
			form_data.append('action', 'videofil_upload'); 
			//form_data.append('percentageUserIDs', percentageUserIDs);  
			//jQuery Ajax to Post form data
$.ajax({
	url : '<?php echo admin_url( 'admin-ajax.php' ); ?>',
	//action: 'videofil_upload',
	type: "POST",
	data : form_data,
	contentType: false,
	cache: false,
	processData:false,
	xhr: function(){
		//upload Progress
		var xhr = $.ajaxSettings.xhr();
		if (xhr.upload) {
			xhr.upload.addEventListener('progress', function(event) {
				var percent = 0;
				var position = event.loaded || event.position;
				var total = event.total;
				if (event.lengthComputable) {
					percent = Math.ceil(position / total * 100);
				}
				//update progressbar
				$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
				$(progress_bar_id + " .status").text(percent +"%");
			}, true);
		}
		return xhr;
	},
	mimeType:"multipart/form-data"
}).done(function(res){ //
	$(my_form_id)[0].reset(); //reset form
	$(result_output).html(res); //output response from server
	submit_btn.val("Publish").prop( "disabled", false); //enable submit button once ajax is done
	$(my_form_id).attr('style','display:none');
	$('#curtain').css('visibility', 'hidden');
				$('#curtain').css('display', 'none');
});
			
		}
	}
	
	$(result_output).html(""); //reset output 
	$(error).each(function(i){ //output any error to output element
		$(result_output).append('<div class="error">'+error[i]+"</div>");
	});
		
});
</script>
<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.validate.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri();?>/js/additional-methods.min.js"></script>
<script language="javascript" type="text/javascript">
  // CKEDITOR.inline( 'description' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
  // CKEDITOR.inline( 'ref' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});

   	    /* jquery('#categories_container').jscroll({
   			loadingHtml: '<img src="<?php echo get_template_directory_uri();?>/images/loading.gif" alt="Loading" /> Loading...',
   			padding: 20,
   			nextSelector: '#nav-below li a',
   			contentSelector: '#categories_container div.post'
   		});
   			  */
   			$("#uploadForm" ).validate({
   				rules: {
   					'file': {
   					  required: true,
   					  extension: 'ogg|ogv|avi|mpe?g|mov|wmv|flv|mp4'
   					}
   				}
   			});

   		  $(document).ready(function(){

   			var $ = jQuery.noConflict();
   			$('input#tagsinput').tagsinput({
   			  confirmKeys: [32]
   			});

   		  })
</script>
<?php get_footer(); ?>