<?php /* Template Name: Home Page */ ?>
<?php get_header(); ?>

<!-- start homepage header -->
<div class="container homepage-header">

<div class='homepage_main_video'><?php get_template_part('includes/home-page-videos/home-page-video-1'); ?></div>

	<div class='homepage_videos_wrapper'>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-2'); ?></div>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-3'); ?></div>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-4'); ?></div>
		<div class='homepage_video'><?php get_template_part('includes/home-page-videos/home-page-video-5'); ?></div>
	</div>

</div>
<script>
jQuery("video").each(function(){
jQuery(this).click(function() {
 if(this.paused)
   this.play();
  else
    this.pause();
});
});
</script>
<!-- end homepage header -->


<div class="container p_90"><!-- start 90 precent wrapper -->
 <?php
	/* ================================================================== */
	/* Start of Loop */
	/* ================================================================== */
	while (have_posts()) : the_post();
	?>

    <!-- Start Video Player -->
    <?php //get_template_part('includes/videoplayer'); ?>
    <!-- End Video Player -->

    <!-- Start Video Heading -->
    <!--<div class="video-heading">-->
    <!-- Start Sharer -->
    <?php //get_template_part('includes/sharer'); ?>
    <!-- End Sharer -->
    <!--<h6 class="video-title"><?php //the_title(); ?></h6>
    <div class="clear"></div>
    </div>-->
    <!-- End Video Heading -->

    <!-- Start Content -->
    <ul class="stats">
        <li><?php //videopress_countviews( get_the_ID() ); ?></li>
        <li><?php //comments_number() ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>

    <?php if (1 == 0){//if( vp_option('vpt_option.show_user') == '1' ){ ?>
    <div class="post-by">
    <a href="<?php echo get_author_posts_url( $post->post_author ); ?>"><?php echo get_avatar( $post->post_author, 50 ); ?></a>
    <a href="<?php echo get_author_posts_url( $post->post_author ); ?>" class="post-by-link"><?php the_author(); ?></a>
    <?php echo '<div class="post-by-vid-count">'.count_user_posts( $post->post_author ).' Videos Uploaded</div>'; ?>
    <div class="clear"></div>
    </div>
    <?php } ?>

    <!-- Start Content -->
    <div class="entry">
    <?php
    // Display The Content
    if($post->post_content==""){
    		echo '<div class="content-empty">No Description</div>';
		}else{
			the_content();
		}
	?>
    <div class="clear"></div>
    </div>
    <!-- End Content -->


	<?php
	// Link Pages Navigation
	$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
    wp_link_pages( $args );

	// Display the Tags
	$before = '<div class="spacer-20"></div>
			   <div class="post-tags">
			   <span><i class="fa fa-tags"></i>Tags</span>';

	$after = '</div>';
	the_tags( $before,', ',$after );
	?>

    <!--<div class="post-categories">
    <span><i class="fa fa-folder"></i>Categories</span>-->
	<?php //the_category(', '); ?>
    <!--</div>-->


    <div class="clear"></div>
    <!-- End Content -->

    <!-- Start Related Videos -->
    <div class="spacing-40"></div>
    <?php //get_template_part('includes/related-videos'); ?>
    <!-- End Related Videos -->


    <?php
	/* ================================================================== */
	/* End of Loop */
	/* ================================================================== */
	endwhile;
	?>


    <!-- Start Content -->
    <div class="container">



    <!-- Start Entries -->
    <div class="grid-3-4 homepage-layout">

	<!-- FEATURED VIDEOS -->
	 <div id="featured_videos_container">
	 <?php get_template_part('includes/featured-playlist-home'); ?>
	 </div>
	 <hr/>

	<!-- POPULAR VIDEOS -->
	 <div id="popular_videos_container">
	 <?php //the_widget('videopress_PopularVideos'); ?>
     <?php get_template_part('includes/popular-videos-home'); ?>
	 </div>
	 <hr/>

	<!-- RELATED VIDEOS -->
	 <div id="related_videos_container">
	 <?php get_template_part('includes/related-videos'); ?>
	 </div>
	 <hr/>

	 <!-- POPULAR CHANNELS -->
	 <div id="popular_channels_container">
	 <?php get_template_part('includes/popular-channels'); ?>
	 </div>
	 <hr/>



    <!-- Content -->
    <!--<div class="entry">

    <div class="clear"></div>
    </div>-->
    <!-- End Content -->

    <?php get_template_part('layouts/layout-builder'); ?>

    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <?php get_sidebar(); ?>
    <!-- End Widgets -->


    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>

     <!-- bottom section -->
     <!-- CATEGORIES -->
	 <div id="categories_container">
	 <?php get_template_part('includes/category-videos'); ?>
	 </div>
	 <hr/>
    <!-- end bottom section -->
    <!-- End Content -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>

</div><!-- end 90 percent wrapper -->
<div class="sign_up_area">
<div class="sign_up_title">Join the Nation Today</div>
<div class="sign_up_text"><span class="icon-quote-left"></span> Where Anything is Possible - Just Press Play ! <span class="icon-quote-right"></span></div>

		<div class="fifty-percent alignleft">
			<a href="<?php echo home_url(); ?>/sign-in/#login"><div class="login_button">LOG IN</div></a>
		</div>

		<div class="fifty-percent alignright">
			<a href="<?php echo home_url(); ?>/sign-in/#signup"><div class="sign_up_button">SIGN UP</div></a>
		</div>

</div>
<?php get_footer(); ?>