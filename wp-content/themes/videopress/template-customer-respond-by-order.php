<?php /* Template Name: Respond contact us*/ 
get_header(); 
?>
<style>
.contactus{
    width: 76%;
    margin: 0 auto;
	padding: 3px 20px 50px 20px;
	}
h1{
	margin-bottom: 29px;
}
</style>
<script type="text/javascript">

var jquery = jQuery.noConflict();
$(document).ready(function (e) {
$('#contactpage').css("min-height", $(window).height()-190 + "px" );
 
});
var $ = jQuery.noConflict();


</script>
 <div class="container" id="contactpage" >
 
 <?php 
 global $wpdb, $woocommerce;
   $orderid = $_GET['orderid'];
    $status = apply_filters( 'contact_us_by_order_id', $orderid ); 
	$message = '';
  
   
   
	    if($_POST['subject']) {
          
		 $subject = $_POST['subject'];
		 $message = $_POST['message'];
		 
		  $order 		= new WC_Order( $orderid ); 
          $comment =  $subject . $message;
		 if ( is_object( $order ) ) {
			//add_filter( 'woocommerce_new_order_note_data', array( __CLASS__, 'filter_order_note' ), 10, 2 );
			$order->add_order_note( $comment, 1 );
			//remove_filter( 'woocommerce_new_order_note_data', array( __CLASS__, 'filter_order_note' ), 10, 2 );
			//wc_add_notice( __( 'The customer has been notified.', 'wcvendors-pro' ), 'success' );
			$mails = $woocommerce->mailer()->get_emails();
			
			if ( !empty( $mails ) ) {
				$mails[ 'WC_Email_Notify_Vendor_By_Customer' ]->trigger( $orderid, $comment );
			}
			$message = 1;
		 }
		
         }// End trigger
 
	   ?>
	  	   
  
  <div class="col-md-12">
     <div class="contactus">
	 <?php if($status == 1) { ?>
	 <?php if( $message == 1) { ?>
	 <div class="alert alert-success">
             <strong>Success!</strong> Thank You message sent.
           </div>
	 <?php } ?>
  <h1>Message your seller</h1>
    
    <form method="post" >
		<div class="form-group">  
		 <label> What is your Problem? </label>
                      <input type="text" required name="subject" placeholder="What is your Problem?" class="form-control">
				   </div>
				   <div class="form-group">
				      <label> Describes about your problem </label>
                        <textarea oninvalid="invalidComment(this);" onblur="setbg('white')"  rows="10" name="message" class="form-control" required></textarea> 
				
					</div>
					 <button type="submit" name="sendmessage" class="btn btn-success">Submit</button>
					 	</form>
	 <?php } else { ?>
	 <script type="text/javascript">

var jquery = jQuery.noConflict();
$(document).ready(function (e) {
$('#contactpage').css("padding-top", "20px" );
 
});
var $ = jQuery.noConflict();


</script>
	       <div class="alert alert-info">
             <strong>Sorry!</strong> Not found your order or order may be complete.
           </div>
        <?php } ?>	 
		</div>	 
 </div>
  										
</div>
<?php get_footer(); ?>