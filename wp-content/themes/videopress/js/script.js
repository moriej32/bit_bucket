jQuery(document).ready(function(){

  jQuery(".widget-profile-controls").hide();
  jQuery(".tab-container").hide();
    
   jQuery(".tab-menu> .log-in").on("click",function(){
	  var hasclass = $(".login").hasClass("activess");
	if(hasclass == false)
	{
	  //alert(hasclass);
		jQuery(".signup").addClass("hide");
		jQuery(".login").removeClass("hide");
		jQuery(".login").addClass("activess");
		 jQuery(".signup").removeClass("activess");
  		jQuery(".tab-container").slideDown("slow");  
   } else
	{
		 //alert(hasclass);
		 
		//jQuery(".tab-menu").show("slow");
		jQuery(".tab-container").slideUp("slow");
		jQuery(".login").removeClass("activess");
  
	}		
  });
	
  jQuery(".tab-menu> .sign-up").on("click",function(){
	  var hasclass = $(".signup").hasClass("activess");
	if(hasclass == false)
	{
		jQuery(".login").addClass("hide");
		jQuery(".signup").removeClass("hide");
		jQuery(".signup").addClass("activess");
		jQuery(".login").removeClass("activess");
		jQuery('#signup').attr('style', 'display:block');
		jQuery(".tab-container").slideDown("slow");
		} else
	{
		 //alert(hasclass);
		 jQuery(".signup").removeClass("activess");
		//jQuery(".tab-menu").show("slow");
		jQuery(".tab-container").slideUp("slow");
		jQuery('#signup').attr('style', 'display:none');
		
  
	}	
		
  });
  
    jQuery(".signup-right").on("click",function(e){
	 	
		e.preventDefault();
		$('#signupright').fadeIn();
  });
  
   jQuery('#loginformnew').on( "submit", function(e) { 
   
     e.preventDefault();
	 var acceptall = false;
 var fname = $('#fname').val();
 var lname = $('#lname').val();
 var username_reg  = $('#username_reg').val();
 var password_reg  = $('#password_reg').val();
 var cpassword_reg = $('#cpassword_reg').val();
 var email_reg    = $('#email_reg').val();
 var address1     = $('#address1').val();
 var address2     = $('#address2').val();
 var country12    = $('#country12').val();
 
 var paypalemail_reg    = $('#paypalemail_reg').val();
 var citytown_reg = $('#citytown_reg').val();
 var zip_reg      = $('#zip_reg').val();
 var phone_reg    = $('#phone_reg').val();
 var storename_reg    = $('#storename_reg').val();
 var revnueshare_reg = $('#revnueshare_reg:checked').val();
 var tearmcodition_reg = $('#tearmcodition_reg:checked').val();
   if(revnueshare_reg != 1)
   {
	   revnueshare_reg = 0;
   }
   if(address2 == 'undefined')
   {
	   address2 = '';
   }
   //undefined
 //alert(tearmcodition_reg);
      if(tearmcodition_reg == 'on'){
		  $('.termerror').html('');
		  acceptall = true;
		 }else{
			 acceptall = false;
			$('.termerror').html('Required Accept Terms of Service');
		}
		
		var checkpassword = valid_password_new(password_reg, cpassword_reg);
		//alert(checkpassword);
		if(checkpassword == 1)
		{
			acceptall = true;
		}else{
			acceptall = false;
		}
		
		 if (grecaptcha.getResponse() == ""){
             acceptall = false;
			 $('.captcherror').html('Required Captcha');
          }
		
		if(acceptall == true)
		{
			console.log();
			//alert('okay');
			$("#overlayssss").fadeIn();
                            $.ajax({
									url: ajaxurl,
									type: "POST",
                                    dataType: "json",
									data: {
										action: 'register_new_account_wp',
									    'username_reg':username_reg,
										'email_reg':email_reg,
										'fname':fname,
										'lname':lname,
										'password_reg':password_reg,
										'address1':address1,
										'address2':address2,
										'country12':country12,
										'paypalemail_reg':paypalemail_reg,
										'citytown_reg':citytown_reg,
										'zip_reg':zip_reg,
										'phone_reg':phone_reg,
										'storename_reg':storename_reg,
										'revnueshare_reg':revnueshare_reg,
									},
									success : function(response){
										//alert(response.newaccount_success);
										$("#overlayssss").fadeOut();
									var newaccount_success = response.newaccount_success;
									if(newaccount_success == 1){
										$('#loginformnew').fadeOut();
										$('#successdiv').fadeIn();
										//successdiv
										$('.notify_success').html(response.notify_success);
									}else{
											
								     alert(response.username_error);	
									 $('.usernameerror').html(response.username_error);
									 $('.email_error').html(response.email_error);
									 
									}
									
									//$('#loginformnew').fadeOut(); newaccount_success
									
									
								  }
								});
		}
		
	 
   });
  
 /* jQuery(".widget-item").mouseleave(function(){
		//jQuery(".tab-menu").show("slow");
		jQuery(".tab-container").slideUp("slow");
		
  });*/
  

  
  
  jQuery(".user-image").hover(function(){
     jQuery(".widget-profile-controls").slideDown("fast");
      jQuery(".widget-profile-controls").css({"z-index" : 1});
	    jQuery(".widget-profile-controls").css({"z-index" : -1});
  });
  
  jQuery(".widget-item").mouseleave(function(){
		jQuery(".widget-profile-controls").slideUp("medium");
  });
  
});



jQuery(document).ready(function($) {
	$('#top-nav').slicknav({
		prependTo:'#responsive-top-nav'
	});
});

jQuery(document).ready(function($) {
	$('.sf-menu').slicknav({
		prependTo:'.sf-responsives'
	});
});
jQuery(document).ready(function($) {
	$('.widget-profile-controls').slicknav({
		prependTo:'.rights',
		label: '',
	});
});

jQuery(document).ready(function($) {
	$('.widget-profile-controls').slicknav({
		prependTo:'.rightsss',
		label: '',
	});
});
jQuery(document).ready(function($) {
$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
  });
};
});
