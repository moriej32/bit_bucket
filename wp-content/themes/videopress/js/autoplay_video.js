$("input#autoplay-checkbox").prop('checked', true);
$(".sidebar-autoblock .autoplay-bar input.checked").val("true");	
$("#autoplay-checkbox").click(function(){
	if($("input#autoplay-checkbox").is(':checked')){
		$("input#autoplay-checkbox").prop('checked', true);
		$("#autoplay-checkbox-label").addClass("background_checkbox_change");		
		$(".sidebar-autoblock .autoplay-bar input.checked").val("true");		
	}else{
		$("input#autoplay-checkbox").prop('checked', false);
		$("#autoplay-checkbox-label").removeClass("background_checkbox_change");
		$(".sidebar-autoblock .autoplay-bar input.checked").val("false");		
	}

});