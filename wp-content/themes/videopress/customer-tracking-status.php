<?php /* Template Name: Customer Tracking status*/ 
get_header(); 
?>
<style>
.contactus{
    width: 76%;
    margin: 0 auto;
	padding: 3px 20px 50px 20px;
	}
h1{
	margin-bottom: 29px;
}
#statusdiv{ 
max-width: 100%; 
margin: 0 auto;
padding: 19px;
 border-radius: 5px;
background: #f1f1f1;
}
#statusdiv span{    margin-left: 10px; text-transform: capitalize;}
.titele-title {
    font-size: 18px;
    margin-bottom: 5px;
    display: block;
    text-transform: uppercase;
    width: 100%;
    margin-bottom: 15px;
}
</style>
<script type="text/javascript">

var jquery = jQuery.noConflict();
$(document).ready(function (e) {
$('#contactpage').css("min-height", $(window).height()/1.2 + "px" );
 
});
var $ = jQuery.noConflict();


</script>
 <div class="container" id="contactpage" >
 
 <?php 
 global $wpdb;
   $carrier_code = $_GET['carrier_code'];
   $track_id     = $_GET['track_id'];
   $status = 0;
   $getresponse = '';
	    if($carrier_code !='' & $track_id != '') {
		     $getresponse = get_tracking_details_by_tracking_id($carrier_code, $track_id);
			 
			 $status = 1;
         }// End trigger
 
	   ?>
	  	   
  
  <div class="col-md-12">
     <div class="contactus">
	 <?php if($status == 1) { ?>
  <h6 class="titele-title">Shipping Tracking Status</h6>
                
				<div id="statusdiv">
				  <div class="row"> <div class="col-md-12"><strong>Shipping Status:</strong><span style="color: #3498db; font-weight: 800;"><?php echo $getresponse ; ?></span></div></div>
				  <div class="row">
				     <div class="col-md-6"><strong>Tracking Number:</strong><span><?php echo $track_id  ; ?></div>
				     <div class="col-md-6"><strong>Carrier:</strong><span><?php echo $carrier_code ; ?></div>
				  </div>
				</div>
	 <?php } else { ?>
	       <div class="alert alert-info">
             <strong>Sorry!</strong> Not found your carrier or tracking ID.
           </div>
        <?php } ?>			 
		</div>	 
 </div>
  										
</div>
<?php 

function get_tracking_details_by_tracking_id( $carrier_code, $track_id ){
          libxml_use_internal_errors( true);
		  $messageofdelevery = '';
		  
		  switch($carrier_code)
		  {
			  case 'newgistics':
			                    //example 725101142101250801204513394341
			                   $temurl = $track_id;
							   $fullurl = 'http://tracking.newgistics.com/?trackingValue='. $temurl .'?'; 
			                   // $fileconent =  file_get_contents( $fullurl );
							   $request = wp_remote_get( $fullurl);
							   $fileconent = wp_remote_retrieve_body( $request );
							  
							   if(preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                     {
				                   $messageofdelevery = 'No tracking results available for this Barcode';
								    $messageofdelevery = 0;
			                    }else{
									 $doc = new DOMDocument;
									 $doc->loadHTML( $fileconent); 
									 $xpath = new DOMXpath( $doc);
									 $staus = $xpath->query('//*[@id="_ctl0_middle_trackingSummary__ctl2_statusLabel"]');
									 $messageofdelevery = $staus->item(0)->nodeValue;  
								 }
								// $messageofdelevery  = $fileconent;
								 
			               break;
						   
			  case 'collectplus':
							  //example 8UWK10012723A301 Ready for Collection
							  // worng result empty
							  $temurl = $track_id;
							   $fullurl = 'https://track.aftership.com/collectplus/'. $temurl .'?'; 
			                 // $fileconent =  file_get_contents( $fullurl );
							 $request = wp_remote_get( $fullurl);
							 $fileconent = wp_remote_retrieve_body( $request );
							  
							   if(preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                     {
				                  // $messageofdelevery = 'No tracking results available for this Barcode';
								   $messageofdelevery = 0;
			                    }else{
									 $doc = new DOMDocument;
									 $doc->loadHTML( $fileconent); 
									 $xpath = new DOMXpath( $doc);
									 $staus = $xpath->query('//*[@id="page"]/div/div/div[2]/div/div[3]/div/p');
									 $messageofdelevery = $staus->item(0)->nodeValue;  
								 }
								 //$messageofdelevery = $fileconent;
			               break;
						   
			  case 'expeditors':
			                  //http://expo.expeditors.com/expo/SQGuest?SearchType=shipmentSearch&TrackingNumber=4812094003&action=SQGuest
							  //example 4812094003 Ready for Delivery
							  //example 4812094004 Services Completed: Delivered
							  //example In Transit - #4812094024
							  // worng result empty
							  $temurl = $track_id;
							   $fullurl = 'http://expo.expeditors.com/expo/SQGuest?SearchType=shipmentSearch&TrackingNumber='. $temurl .'&action=SQGuest'; 
			                  // $fileconent =  file_get_contents( $fullurl );
							  $request = wp_remote_get( $fullurl );
							  $fileconent = wp_remote_retrieve_body( $request );
							  
							   if(preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                     {
				                  // $messageofdelevery = 'No tracking results available for this Barcode';
								  $messageofdelevery = 0;
			                    }else{
									 $doc = new DOMDocument;
									 $doc->loadHTML( $fileconent); 
									 $xpath = new DOMXpath( $doc);
									 $staus = $xpath->query('//*[@id="sqgDetailSummary"]//div[@class="statusRef"]//span[@class="status"]');
									 $messageofdelevery = $staus->item(0)->nodeValue;  
								 }
								// $messageofdelevery = $fileconent;
			               break;
						   
			  case   'canpar':
			                   // example D434035600000008023001
							   
			                   $temurl = 'https://www.canpar.com/en/track/TrackingAction.do?locale=en&type=0&reference=';
							   $fullurl = $temurl . $track_id; 
			                   $fileconent =  file_get_contents( $fullurl );
							   if(preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                     {
				                  // $messageofdelevery = 'No tracking results available for this Barcode';
								   $messageofdelevery = 0;
			                    }else{
									 $doc = new DOMDocument;
									 $doc->loadHTML( $fileconent); 
									 $xpath = new DOMXpath( $doc);
									 $staus = $xpath->query('//*[@class="centreContent"]//table[@id="table0"]/tr[3]/td[1]');
									 $messageofdelevery = $staus->item(0)->nodeValue;  
								 }
			                   break;				
							 
			   default:
			                   $temurl = $track_id;
							   $fullurl = 'https://track.aftership.com/'. $carrier_code . '/'. $temurl .'?'; 
			                 // $fileconent =  file_get_contents( $fullurl );
							 $request = wp_remote_get( $fullurl);
							 $fileconent = wp_remote_retrieve_body( $request );
							  
							   if(preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                     {
				                  // $messageofdelevery = 'No tracking results available for this Barcode';
								   $messageofdelevery = 0;
			                    }else{
									 $doc = new DOMDocument;
									 $doc->loadHTML( $fileconent); 
									 $xpath = new DOMXpath( $doc);
									$staus = $xpath->query('//*[@id="page"]/div/div/div[2]/div/div[3]/div/p');
									// $staus = $xpath->query('//*[@id="page"]/div/div/div[2]/div/div[2]/div/p');
									 $messageofdelevery = $staus->item(0)->nodeValue;  
								 }
								 //$messageofdelevery = $fileconent;
                         //99038585357								 
			          
		  }
			
				 
			return $messageofdelevery;
			
			// exmaple fedex: 776641097654
		
	 }
?>
<?php get_footer(); ?>