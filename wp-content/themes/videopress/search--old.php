<?php get_header(); ?>
<?php session_start(); ?>
    <!-- Start Content -->

      <div id="ex1" style="display:none;">
    <p>Thanks for clicking.  That felt good.  <a href="#" rel="modal:close">Close</a> or press ESC</p>
  </div>


    <!-- Start Entries -->
    <div class="grid-3-4 centre-block" style="background:#f6f6f6 none repeat scroll 0 0">
	    <h1 class="page-title" style="padding: 20px;"><?php printf( __( 'Search Results for : %s', 'twentyfifteen' ), get_search_query() ); ?></h1>

    <div class="cover-twoblocks">
		<div>
		 <form method="post" id="sortvideo-form" action="<?php echo home_url( '/' ).'?s='.get_search_query(); ?>">
			<p>Currently Sorted By :
				<select class="sortvideo" name="sortvideo" onchange="this.form.submit()">
					<option value="recent" <?php if ($_REQUEST["sortvideo"] == 'recent') echo 'selected="selected"';?> >Recent</option>
					<option value="views" <?php if ($_REQUEST["sortvideo"] == 'views') echo 'selected="selected"';?> >Views</option>
					<option value="endorsements" <?php if ($_REQUEST["sortvideo"] == 'endorsements') echo 'selected="selected"';?> >Endorsements</option>
					<option value="favourites" <?php if ($_REQUEST["sortvideo"] == 'favourites') echo 'selected="selected"';?> >Favorites</option>
					<option value="comments" <?php if ($_REQUEST["sortvideo"] == 'comments') echo 'selected="selected"';?> >Comments</option>
					<option value="reviews" <?php if ($_REQUEST["sortvideo"] == 'reviews') echo 'selected="selected"';?> >#Reviews</option>
					<option value="downloads" <?php if ($_REQUEST["sortvideo"] == 'downloads') echo 'selected="selected"';?> >#Downloads</option>
				</select>
			</p>


			</form>
			<input name="video" id="chvideo" type="checkbox" value="video" checked="checked" /> Video
			<input name="file" id="chfile" type="checkbox" value="file" checked="checked"/>  File
			<input name="book" id="chbook" type="checkbox" value="book"checked="checked"/>   Book
			<input name="product" id="chproduct" type="checkbox" value="product"checked="checked"/>   Product

		</div>
			<div class="well well-sm" style="margin-top: -51px;">
				<strong>Views</strong>
				<div class="btn-group">
					<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="container p_90">

	    <!-- Start Layout 2-->
	    <div class="layout-2">

		<div class="row">

		<!-- <script src="wp-content/themes/videopress/js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>-->
		<script type="text/javascript">
		$(function(){


             //FA: prepare modal to be appended
			appendthis =  ("<div class='modal-overlayblockdiv'></div>");


			var chvideoChecked = sessionStorage.getItem("chvideo");  //$.cookie("chvideo");
			var chfileChecked =  sessionStorage.getItem("chfile"); //$.cookie("chfile");
			var chbookChecked = sessionStorage.getItem("chbook"); //$.cookie("chbook");
			var chproductChecked = sessionStorage.getItem("chproduct"); //$.cookie("chproduct");

			//video chkbox
			console.log("chvideoChecked "+chvideoChecked);
			console.log("chfileChecked "+chfileChecked);
			console.log("chbookChecked "+chbookChecked);
			console.log("chproductChecked "+chproductChecked);


			if(chvideoChecked == "false"){
				$("#chvideo").prop('checked', false);
			 	$(".post").hide();
			 	console.log("video: false");
			}else{
			 	$(".post").show();
			    $("#chvideo").prop('checked', true);
			    console.log("video: true");
			    console.log(chvideoChecked);
			}


			if(chproductChecked == "false"){
				$("#chproduct").prop('checked', false);
				$(".post").hide();
				console.log("product: false");
			}else{
				$(".post").show();
				$("#chproduct").prop('checked', true);
				console.log("product: true");
				console.log(chproductChecked);
			}





			if(chfileChecked == "false"){
				$(".dyn_file").hide();
			 	$("#chfile").prop('checked', false);
			 	console.log("FIle: false");

			}else{
				$(".dyn_file").show();
			    $("#chfile").prop('checked', true);
			    console.log("File: true");
			    console.log(chfileChecked);

			}

			if(chbookChecked == "false"){
				$(".dyn_book").hide();
			 	$("#chbook").prop('checked', false);
			 	console.log("Book: false");
			}else{
			 	$(".dyn_book").show();
			    $("#chbook").prop('checked', true);
			    console.log("Book: true");
			    console.log(chbookChecked);
			}



               //FA: set default value when page loads
               $("ul.pagination li a").each(function( index ) {
				$(this).attr("data-viewstyle", "" +
						"list");
				});




			$('#list').click(function(event){
					$('.status-publish').removeClass('item2 col-sm-4 col-md-2 text-center post-1427 group-search-grid');


					$("div.hentry.status-publish:gt(11)").hide();


	//				$(".layout-2-details").show();
					$(".dyn_rating_bar").show();
					//
					$(".video_excerpt_text").show();
					$(".stats").show();
					$(".layout-2-details span").show();


					$(".pagination").attr("style","");
					$(".layout-2-details :nth-child(2)").show();
					$(".layout-2-details").css("overflow","hidden");
					event.preventDefault();

					 //FA: hide details	button
				     if ( $('.bottom-detailsul').hasClass('show') ) {
				       $('.bottom-detailsul').removeClass('show');
				       $('.bottom-detailsul').addClass('hidden');
				     }

				     var $formaction=$("#sortvideo-form").attr("action");
				     if( /&viewstyle=grid/.test($formaction) ){
				     	$formaction= $formaction.replace("&viewstyle=grid", "&viewstyle=list");
				     $("#sortvideo-form").attr("action", $formaction );
				     }

              		$("ul.pagination li a").each(function( index ) {
					var $href=$(this).attr("href");
					if( /&viewstyle=grid/.test($href) ){
						$href=$href.replace("&viewstyle=grid","&viewstyle=list");
						$(this).attr("href", $href);
						$(this).attr("data-viewstyle", "list");
					}
					else if(!/&viewstyle=list/.test($href)){
						$(this).attr("href", $href+"&viewstyle=list");
						$(this).attr("data-viewstyle", "list");
					}
					else
						$(this).attr("data-viewstyle", "list");

				   });

              	$("ul.pagination").eq(0).show();
				$("ul.pagination.gridviewstyle").hide();

			});

		$('#grid').click(function(event){

				$('.status-publish').addClass('item2  col-sm-4 col-md-2 text-center post-1427 group-search-grid');

				$("div.hentry.status-publish:gt(11)").show();

				    //FA: show details button
				      if ( $('.bottom-detailsul').hasClass('hidden') ) {
				        $('.bottom-detailsul').removeClass('hidden');
				        $('.bottom-detailsul').addClass('show');
				     }

				//$(".layout-2-wrapper").css("min-height","176px");
				$(".pagination").attr("style","position: relative; float: left; clear: both; margin-left: 9px;")
				$(".group-search-grid .dyn_rating_bar").hide();
				$(".group-search-grid .review_box").hide();
				$(".group-search-grid .video_excerpt_text").hide();
				$(".group-search-grid .stats").hide();
				$(".group-search-grid .layout-2-details span").hide();
				$(".group-search-grid .layout-2-details").css("overflow","visible");
				$(".group-search-grid .layout-2-details :nth-child(2)").hide();
				$(".grid-6").css("min-height","160px");



				var $formaction=$("#sortvideo-form").attr("action");
				 if( /&viewstyle=list/.test($formaction) ){
				     	$formaction= $formaction.replace("&viewstyle=list", "&viewstyle=grid");
				     $("#sortvideo-form").attr("action", $formaction );
				     }
				 else if(! /&viewstyle=grid/.test($formaction) )
				$("#sortvideo-form").attr("action", $formaction + "&viewstyle=grid");

				$("ul.pagination li a").each(function( index ) {
					var $href=$(this).attr("href");
					if( /&viewstyle=list/.test($href) ){
						$href=$href.replace("&viewstyle=list","&viewstyle=grid");
						$(this).attr("href", $href);
						$(this).attr("data-viewstyle", "grid");
					}
					else if(!/&viewstyle=grid/.test($href) ){
						$(this).attr("href", $href+"&viewstyle=grid");
						$(this).attr("data-viewstyle", "grid");
					}
					else
						$(this).attr("data-viewstyle", "grid");
				});

				$("ul.pagination").eq(0).hide();
				$("ul.pagination.gridviewstyle").show();
				/*
				$(".bottom-detailsul").click(function(){
					console.log("Info11 Click");
					$(".review_box").show();
					$(".review_box").empty();
					$(".review_box").prepend($(this).parent().find(".layout-title").html())

					$.ajax({
					    type: 'POST',
					    url: 'fetchdata.php',
					    data: { id: "387"},
					    dataType: 'json',
					    success: function (data) {
					        $.each(data, function(index, element) {

						        	$(".review_box").prepend('<div class="layout-2-details"><div class="image-holder"><a href="http://www.doityourselfnation.org/bit_bucket/fasdfsdaf/"><div class="hover-item"></div></a><img style="max-height:100%!important;" alt="" class="layout-2-thumb wp-post-image" src="http://www.doityourselfnation.org/bit_bucket/wp-includes/images/media/default.png"></div><h6 class="layout-title"><a style="color:rgb(75, 75, 75)!important; font-size:16px !important; font-weight:600 !important" title="'+element.post_title+'" href="http://www.doityourselfnation.org/bit_bucket/fasdfsdaf/">'+element.post_title+'</a></h6><div data-rating_type="post" data-post_id="" class="dyn_rating_bar dyn_open_review_box"><div style="width:0%" class="dyn_rating"></div></div><div style="display:none;" class="reviews review_box dyn_all_reviews"><div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span> <div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"></div></div><div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright">'+element.post_content+'</div></div><div class="video_excerpt_text two"><h4 class="tittle-h4">Author: </h4><div class="inline-blockright">Morris Pearson</div></div><ul class="stats"><li>9 months ago</li><li>650 Views</li><li>0 Endorsments</li><!-- favourite section --><li>0  Favorites</li><!-- end favorite --><li>1 Review</li><li>No Comments</li></ul><div class="clear"></div><p></p></div>')

					        });

					    }
					});

					$(".review_box").append('<div class="btn btn-danger dyn_close_review_box">Close</div>');
					$(".dyn_close_review_box").click(function(){
						$(".dyn_close_review_box").parent().hide();

				});
				});
				*/


				event.preventDefault();
			});



				$('.layout-2').on("click", 'a[data-modal-id]', function(e) {

 					 e.preventDefault();
  					$("body").append(appendthis);
  					$(".modal-overlay").fadeTo(500, 0.7);
 					   //$(".js-modalbox").fadeIn(500);
  						  var modalBox = $(this).attr('data-modal-id');

    				$('#'+modalBox + " .layout-title").html('<a href="'+ $(this).data('permalink') + '" title="' + $(this).data('title') + '" style="color:rgb(75, 75, 75)!important; font-size:16px !important; font-weight:600 !important">' + $(this).data('title') + '</a>');
   			 		$('#'+modalBox + " .image-holder a").attr('href', $(this).data('permalink'));
   			 			$('#'+modalBox + " img").attr('src', $(this).data('image'));


    				var thecontent=$(this).data('content').replace( /<p>/g, '' ).replace( /<\/p>/g, '' );

   					 $("#"+modalBox + " .inline-blockright:eq(0)").html(thecontent);
   					 $("#"+modalBox + " .inline-blockright:eq(2)").text($(this).data('author'));
   					 $("#"+modalBox + " .inline-blockright:eq(1)").html($(this).data('ref').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));


					if(modalBox=='productsModal') {
						if($(this).data('image')!='http://www.doityourselfnation.org/bit_bucket/wp-includes/images/media/default.png'){
							$('#'+modalBox + " img").attr('src', $(this).data('image'));
						}else{
							$('#'+modalBox + " img").attr('src','http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg');
						}

						$("#"+modalBox + " .product-regular_price").html($(this).data('regularprice').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
						$('#'+modalBox + " .imgall").html($(this).data('allimg').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
						$('#'+modalBox + " .product-sold-take").text($(this).data('sold'));
						$("#"+modalBox + " .stock-span-grid").text($(this).data('stock'));
						$("#"+modalBox + " #shipped_country").html($(this).data('shippinto')); 
					}else{ }

   					 $('#'+modalBox + " .stats li:eq(0)").text($(this).data('time'));
   					 $('#'+modalBox + " .stats li:eq(1)").text($(this).data('views'));
   					 $('#'+modalBox + " .stats li:eq(2)").text($(this).data('endorse') + ' Endorsments');
  					 $('#'+modalBox + " .stats li:eq(3)").text($(this).data('favr') + ' Favorites');
   				     $('#'+modalBox + " .stats li:eq(4)").text($(this).data('revs'));



    				var comments=$(this).data("comments");
   					 if( (comments==0) || (comments=='No Comments') ){
    				  ncomments="No Comments";
    					}
   					 else{
   				   ncomments=comments + " Comments";
    				}
    				$("#"+modalBox + " .stats li:eq(5)").text(ncomments);

    			if($(this).data("ftype")){

      				$('#'+modalBox + " #img-hld").attr("class", "image-holder filesz dyn-file-sprite " + $(this).data("fclass"));
     				 $('#'+modalBox + " #ftype").text("File Type: " + $(this).data("ftype"));
     			 	if($(this).data("download")!="")
     		    	$('#'+modalBox + " .stats li:last-child").text( $(this).data("download") + " downloads");
      				else
      		 		 $('#'+modalBox + " .stats li:last-child").text("0 downloads");
    			}

    			$('#'+modalBox).fadeIn();

  				});


				$(".js-modal-close, .modal-overlay").click(function() {
 				 $(".modal-box, .modal-overlay").fadeOut(500, function() {
   				 $(".modal-overlay").remove();
  				});
				});

				$(window).resize(function() {
 			 	$(".modal-box").css({
   			 	top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
    		 	left: ($(window).width() - $(".modal-box").outerWidth()) / 2
  					});
				});

			$(window).resize();


		 });

		$(document).ready(function(){

			if( /viewstyle=grid/.test(window.location.href) ){

             	$('#grid').click();
             }
             else{
             	$("div.hentry.status-publish:gt(11)").hide();
             	$("ul.pagination").eq(0).show();
				$("ul.pagination.gridviewstyle").hide();
             }

			$("#chvideo").click(function(){
				//console.log($("#chvideo").prop( "checked" ));
				var res = $("#chvideo").prop( "checked" );
				if(res){
					//$(".post").show();
					//$.cookie("chvideo", "#chvideo");
					sessionStorage.setItem("chvideo", "#chvideo");
				}else{
					//$(".post").hide();
					//$.cookie("chvideo","false");
					sessionStorage.setItem("chvideo","false");
				}
				changeView();
			});

			$("#chfile").click(function(){
				//console.log($("#chfile").prop( "checked" ));
				var res = $("#chfile").prop( "checked" );
				if(res){
					//$(".dyn_file").show();
					//$.cookie("chfile", "#chfile");
					sessionStorage.setItem("chfile", "#chfile");
				}else{
					//$(".dyn_file").hide();
					//$.cookie("chfile","false");
					sessionStorage.setItem("chfile","false");
				}
				changeView();
			});




			$("#chbook").click(function(){
				var res = $("#chbook").prop( "checked" );
				if(res){
					//$(".dyn_book").show();
					//$.cookie("chbook", "#chbook");
					sessionStorage.setItem("chbook", "#chbook");
				}else{
					//$(".dyn_book").hide();
					//$.cookie("chbook","false");
					sessionStorage.setItem("chbook","false");
				}
				changeView();
			});
			$("#chproduct").click(function(){
				//console.log($("#chfile").prop( "checked" ));
				var res = $("#chproduct").prop( "checked" );
				if(res){
					//$(".dyn_file").show();
					//$.cookie("chfile", "#chfile");
					sessionStorage.setItem("chproduct", "#chproduct");
				}else{
					//$(".dyn_file").hide();
					//$.cookie("chfile","false");
					sessionStorage.setItem("chproduct","false");
				}
				changeView();
			});
			var isloggedin = "<?PHP echo is_user_logged_in(); ?> ";
			if(isloggedin == " ")
			{
				$(".del-video").remove();
			}

			if(sessionStorage.getItem("chvideo")=="false" || sessionStorage.getItem("chfile")=="false" || sessionStorage.getItem("chbook")=="false"  || sessionStorage.getItem("chproduct")=="false"  )
			{
				changeView();
			}



        });//end of document ready
		
		
		$(document).on('click', '#countymidssss', function(e) {
var $ = jQuery.noConflict();	
	e.preventDefault();
var postidsss = $(this).attr('data-countymids');
		          $.ajax({
							url: "<?php echo $_ajaxsss; ?>",				
							type: "POST",
							dataType: "JSON",
							data: { 
								action: "shipping_rate_country_by_postid",
								postidsss: postidsss
							 },
							success: function(data) { 
									$('#shippingrateshow').html(data.is_insert);
								
							},
						   complete: function() {
							   
							    $('#moreshipped').fadeIn();
						   }
						   
						});

});
$(document).on('click', 'button.close', function(e) {
	var $ = jQuery.noConflict();
	$('#moreshipped').fadeOut();
});

$(document).on('click', '#liststore', function(e) {
	var $ = jQuery.noConflict();
	  e.preventDefault();
	  $('#productssss .item').removeClass('grid-group-item changeStyleGrid');  
      $('#productssss .item').addClass('list-group-item changeStyleList');
});

$(document).on('click', '#gridstore', function(e) {
	var $ = jQuery.noConflict();
	 e.preventDefault();
     $('#productssss .item').removeClass('list-group-item changeStyleList');
    $('#productssss .item').addClass('grid-group-item changeStyleGrid');
});
        </script>
	    <br />
	    <?php
		global $wpdb;


		//FA the posts_per_page has been set to -1 @funtions.php L1642 to get all posts that match search query
		// and filter them according to option in "sort by"

		$postids=array();

		//print_r($wp_query->query_vars);
		//error_reporting(E_ALL);

		if($posts != null){

			    global $paged;
			global $wpdb;

				if($paged>0)
				 $start = ($paged==1) ? 0 : intval($paged-1) * 12;
   				else
   				 $start=0;

                /*
   				if( isset($_GET["viewstyle"]) && $_GET["viewstyle"]=="grid" )
   				 	$lmt=' LIMIT  ' . $start*2 .', 24';
   				else
				 	$lmt=' LIMIT  ' . $start .', 24'; //FA: get 24 but show only first 12 in list view = don't change the $start. + see @L 285, 108,163
				*/


				 foreach ($posts as $temp) {

					array_push($postids, $temp->ID);
				}

	            ///FA: set the query depending on the view
	        if( isset($_REQUEST["sortvideo"]) ) {

	            switch($_REQUEST['sortvideo']) {
   				case "comments":
   				$qry=order_by_comment($postids);
    				break;
   				case "views":
   				$qry=order_by_views($postids);
   				break;
   				case "endorsements":
   				 $qry=order_by_endorsement($postids);
   				 break;
   				 case "favourites":
   				 $qry=order_by_favourites($postids);
   				 break;
   				 case "downloads":
   				 $qry=order_by_downloads($postids);
   				 break;
   				 case "reviews":
   				 $qry=order_by_reviews($postids);
   				 break;
   				 default:
                 $qry=order_by_recent($postids);
             	}

             }else{
             	$qry=order_by_recent($postids);
            	 }


                  //FA: get all the posts

			//echo $qry ; die;
			//$posts = $wpdb->get_results($qry);

		//	echo '<pre>';
		//	print_r($posts);die;
            	 //FA: get the total number of posts from custom $qry
            	 $postlist=count($posts);

			/*FA check results:  print_r($posts); */

//echo json_encode($posts);

			if( isset($_GET["viewstyle"]) && $_GET["viewstyle"]=="grid" )
   				// $thePage=array_slice($posts, $start*2, 12);
			     $thePage=array_slice($posts, $start, 12);
   			else
				 //FA: get 24 but show only first 12 in list view = don't change the $start
				 $thePage=array_slice($posts, $start, 12);


			foreach ($thePage as $post) {


				$user_ID = get_current_user_id();
				$res = get_user_meta( $user_ID);



				if($res!=null && $post->post_type == 'user'){
				//	var_dump($post);die;


					 $avatar = get_user_meta( $user_ID, 'profile_pic_meta', true );
					if($avatar != ""){
						$avatar1 = site_url().'/profile_pic/'.$avatar;
					}else{
						$avatar1 = get_stylesheet_directory_uri().'/images/no_avatar.jpg';
					}
					?>
					<div class="user">
						<div class="layout-2-wrapper solid-bg overflow_y">
							<div class="grid-6 first">
								<div class="image-holder ">
								
								<a href="<?php echo $post->link; ?>" target="_blank">
									<img src="<?php echo $avatar1; ?>" style="width: 100%" />
								</a>
								</div>
							</div>
							<div class="layout-2-details">
								<h6 class="layout-title"><a href="<?php echo $post->link; ?>" target="_blank"><?php echo $post->post_title; ?></a></h6>
								<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo wp_trim_words( $post->post_content, 120, ' [...]' ); ?></div></div>
							</div>
						</div>
					</div>
					<?php
				}
				else
				{
						?>
					<script>

						/*setInterval(function(){

							if ($('input#chproduct').is(':checked')) {
								ktcraca = 5;
							} else  ktcraca = 6; console.log(ktcraca);


						}, 300);*/
</script>
<?php
 					$contentTemp  = get_template_part( 'content' , get_post_type()  );
$qyzy ;
					 if(get_post_type() == 'product') {

?>
						<script> $(".search-everything-highlight-color").css("background-color", ""); $("a").css("background-color", ""); </script>
						 <?php
						 }


							/*					var deb=5;
							$( "#chproduct" ).change(function() {
								var $input = $( this );
							if(deb ==5) {	$( "#chproduct" ).setAttribute("class", "ktcracchi"); } else $( "#chproduct" ).setAttribute("class", "ktcraca");
								if($('#chproduct').attr('class') == 'ktcracchi')  {deb = 111;}
 							}).change();


						</script>*/








				}


			}

 		}else{

	    ?>

	    <h6>NOTHING FOUND!</h6>
	    <p>Sorry Nothing found, please try again..</p>

	    <?php
		/* ================================================================== */
		/* End If */
		/* ================================================================== */
		}

		echo '<input type="hidden" id="hiddeninput" value="'.$postlist.'"></input>';
		echo "<input type='hidden' value='' id='newCount'></input>";
		?>
		</div> <!--end of row -->

	    </div>
    	<!-- End Layout 2 -->

    <!-- Start Pagination -->
    <?php
	//if($current || $selected):


		if( vp_option('vpt_option.pagination') == '1'){
			echo '<div class="do_pagination">';
			videopress_pagination($pages = '', $range = 2, $postlist); // Use Custom Pagination
			videopress_pagination_grid($pages = '', $range = 2, $postlist); // Hidden pagination for Grid View - Use Custom Pagination - see @L 295-269
			echo'</div>';
		}else{
			echo '<div class="post_pagination">';
			posts_nav_link();
			echo'</div>';
		}
	?>
    <!-- End Pagination -->

    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <!-- <?php //get_sidebar(); ?>-->
    <!-- End Widgets -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
	<!-- description product -->
<div class="modal-box" id="videosModal">
 <div class="modal-body">
  <a class="js-modal-close close">×</a>
  <div class="layout-2-details">
  
							   	       
    <div class="image-holder " style="margin 0 auto; display:block; width:200px !important; height:150px !important" >
    
      <a href="">
        <!--<div class="hover-item" style="width:200px !important; height:150px !important"></div>-->
      </a>
      <img src="" class="layout-2-thumb wp-post-image" alt="" style="height:150px; max-height:100%!important;">
    </div>
    <h6 class="layout-title"></h6>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div>
	  <div class="reviews review_box dyn_all_reviews search.php" style="display:none;">
  <!--modal mmm-->
    <div class="btn btn-danger dyn_close_review_box">Close</div> </div>
	  <span>(clickable)</span> <div class="video_excerpt_text one">
		  <h4 class="tittle-h4">Description</h4>
		  <div class="inline-blockright">okay</div>
	  </div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright">reference</div></div><div class="video_excerpt_text two"><h4 class="tittle-h4">Author: </h4><div class="inline-blockright">reference</div></div>
    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i> 0 Endorsments</li>
      <!-- favourite section -->
      <li><i class="fa fa-heart"></i>
        0 Favorites</li>
        <!-- end favorite -->
        <li><i class="fa fa-star-o"></i> 0 Reviews</li>
        <li></li>
      </ul>
      <div class="clear"></div>
      <p></p>
    </div>
  </div>
</div>



	<div class="modal-box" id="productsModal">
		<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details">
				<div class="image-holder " style="margin 0 auto; display:block; width:200px !important; height:150px !important" >
				
					<a href="">
						<div class="hover-item" style="width:200px !important; height:150px !important"></div>
					</a>
					<img src="" class="layout-2-thumb wp-post-image" alt="" style="height:150px; max-height:100%!important;">
				</div>

				<div class="imgall">

				</div>


				<div class='other-image-product-first'>
					<div class="thumbnails columns-3">

					</div></div>


				<h6 class="layout-title"></h6>
				<div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div>
				<div class="reviews review_box dyn_all_reviews search.php" style="display:none;">

					<!--modal mmm-->
					<div class="btn btn-danger dyn_close_review_box">Close</div> </div>


				<span>(clickable)</span>

				<div class="product-regular_price"></div>

				<div class="stock-grid">
					<h4 class="tittle-h4"><i class="fa fa-smile-o" aria-hidden="true"></i><span class="stock-span-grid">  </span><span style="color:black;font-weight: 400;"> in Stock</span></h4>
				</div>



				<div class="video_excerpt_text one">
					<h4 class="tittle-h4">Description</h4>
					<div class="inline-blockright">okay</div>
				</div>


				<div class="video_excerpt_text two-ref-del"  ><h4 class="tittle-h4"></h4><div class="inline-blockright"></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Sold By</h4><div class="inline-blockright">reference</div></div>
				<div class="video_excerpt_text  shipped-model-home"><h4 class="tittle-h4">Shipped To</h4><div class="inline-blockright" id="shipped_country">Bangladesh</div></div>




				<ul class="stats">

					<li></li>
					<li></li>
					<li><i class="fa fa-wrench"></i> 0 Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
						0 Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> 0 Reviews</li>
					<li></li>
					<li class="product-sold-take"> </li>
				</ul>
				<div class="clear"></div>
				<p></p>
			</div>
			 <div id="shippingrateshow">
           </div>
		</div>
	</div>


<div  class="modal-box" id="docs">
 <div class="modal-body">
   <a class="js-modal-close close">×</a>
   <div class="layout-2-details">
    <div id="img-hld" class="">
      <a href="">
        <div class="hover-item dyn-file">
          <i class="fa fa-download"></i>
        </div>
      </a>
    </div>
    <h6 class="layout-title"><a href="" title="wow"></a></h6>
    <div id="ftype"></div>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div><div class="reviews review_box dyn_all_reviews eli_search.php" style="display:none;">

    <div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span>
    <div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright">No Description available</div></div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright">No referance </div></div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Author</h4><div class="inline-blockright"></div></div>
    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i>
        0 Endorsments</li>
        <li><i class="fa  fa-heart"></i>
          0 Favorites</li>
          <!--end favorite-->
          <li><i class="fa fa-star-o"></i> 0 Reviews</li>
          <li>No Comments</li>
          <li><i class="fa fa-download"></i> </li>
        </ul>
        <div class="clear"></div>
        <p></p>
      </div>
     </div>
    </div>

    <!-- End Content -->
<script>
$(".do_pagination").on("click", "ul.pagination li a", function(e){
				e.preventDefault();
				createModal();

				var $viewstyle=($(this).data("viewstyle"))?$(this).data("viewstyle"):"list";
				var $page_id=$(this).attr("href").match(/\/page\/(\d+)\//)[1];
				var $sortvideo= ($(this).data("sortvideo"))?$(this).data("sortvideo"):"recent";

				var $start="";

				var excluded={};
				excluded.post= sessionStorage.getItem("chvideo");
				excluded.dyn_file=sessionStorage.getItem("chfile");
				excluded.dyn_book=sessionStorage.getItem("chbook");
				excluded.product=sessionStorage.getItem("chproduct");


				var allposts={al:<?php echo json_encode($posts); ?>};




  var exclude=function(el,indx){

	for(x in excluded){

	 	if (excluded[x]=="false"){
			if (el.post_type==x){

				return false;
			}
         }
     }

     return true;
};

var filtered = allposts.al.filter(exclude);



			if($page_id>0) //FA NOTE: if exclude is present $page_id posted = 0
				 $start = ($page_id==1) ? 0 : eval($page_id-1) * 12;
   			else
   				 $start=0;

   		     if( $viewstyle=="grid")
   				 	//var $posts=filtered.slice($start*2, ($start*2)+24);
				   var $posts=filtered.slice($start, $start+24);
   			 else
				 	var $posts=filtered.slice($start, $start+24);


$(".layout-2 .row input[id='newCount']").val(filtered.length);

        		$.ajax({
					  type: 'POST',
					  url: 'fetchsearch.php',
					  data: { "page_id": $page_id, "viewstyle":$viewstyle, "sortvideo": $sortvideo, "exclude":excluded, "posts":$posts },
					  dataType: 'html',
					  success: function (data) {

					   $(".layout-2 .row div.status-publish").remove();

					   $(".layout-2 .row").append(data);


					   $('html, body').animate({scrollTop : 0},800);
					    document.querySelector("#themeModal").style.display = "none";

			}//ajax success

			}).done(function(){


	 videopress_pagination_js($page_id,$sortvideo,$viewstyle);
	videopress_pagination_js_grid($page_id,$sortvideo,$viewstyle);

              if($viewstyle=="grid")
			   $('#grid').click();
			   else if($viewstyle=="list")
			   $('#list').click();


	});//end of ajax

});// end of pagination link click


changeView();
//FA: function when checkboxes are clicked
 function changeView(){

				createModal();

				// $( ".do_pagination" ).empty();
				 //$( ".do_pagination" ).html('<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif">');

				var $viewstyle=($("ul.pagination li a:eq(0)").data("viewstyle"))?$("ul.pagination li a:eq(0)").data("viewstyle"):"list";
				var $page_id=1;
				var $sortvideo= ($("#sortvideo-form select option:selected").val()) ? $("#sortvideo-form select option:selected").val() : "recent";

				var $start="";

				var excluded={};
				excluded.post= sessionStorage.getItem("chvideo");
				excluded.dyn_file=sessionStorage.getItem("chfile");
				excluded.dyn_book=sessionStorage.getItem("chbook");
	 			excluded.product=sessionStorage.getItem("chproduct");


	 var allposts={al:<?php echo json_encode($posts); ?>};





  var exclude=function(el,indx){

	for(x in excluded){

	 	if (excluded[x]=="false"){
			if (el.post_type==x){

				return false;
			}
         }
     }

     return true;
};

var filtered = allposts.al.filter(exclude);



			if($page_id>0) //FA NOTE: if exclude is present $page_id posted = 0
				 $start = ($page_id==1) ? 0 : eval($page_id-1) * 12;
   			else
   				 $start=0;

   		      if( $viewstyle=="grid")
   				 	//var $posts=filtered.slice($start*2, ($start*2)+24);
				    var $posts=filtered.slice($start, $start+24);
   			  else
				 	var $posts=filtered.slice($start, $start+24);


$(".layout-2 .row input[id='newCount']").val(filtered.length);

        		$.ajax({
					  type: 'POST',
					  url: 'fetchsearch.php',
					  data: { "page_id": $page_id, "viewstyle":$viewstyle, "sortvideo": $sortvideo, "exclude":excluded, "posts":$posts },
					  dataType: 'html',
					  success: function (data) {

					   $(".layout-2 .row div.status-publish").remove();

					   $(".layout-2 .row").append(data);


					   $('html, body').animate({scrollTop : 0},800);
					    document.querySelector("#themeModal").style.display = "none";

			}//ajax success

			}).done(function(){


	 videopress_pagination_js($page_id,$sortvideo,$viewstyle);
	 videopress_pagination_js_grid($page_id,$sortvideo,$viewstyle);

              if($viewstyle=="grid")
			   $('#grid').click();
			   else if($viewstyle=="list")
			   $('#list').click();


	});//end of ajax

}// end of checkbox click





//FA create pagination for AJAX

	function videopress_pagination_js(pgs,srt,grdview)
        {

            var range=2;
            var showitems = (range * 2)+1;


            if( $(".layout-2 .row input[id='newCount']") ) {
            var val=$(".layout-2 .row input[id='newCount']").val()/12;
              }
            else
            var val=$("#hiddeninput").val()/12;

            var pages=Math.ceil(val);

            var ap="";
            var pgs=( grdview=="grid" ) ? (pgs * 2)-1 : pgs;

            var next=( grdview=="grid" ) ? eval(pgs+1) : eval(Number(pgs)+1);
            var prev=( grdview=="grid" ) ? eval(Number(pgs)-1) : eval(Number(pgs)-1);

         var pagination="";
       pagination+='<ul class="pagination">';
        if(pgs > 2 && pgs > range+1 && showitems < pages) pagination+= "<li><a href='/page/1/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='list'>First</a></li>";
        if(pgs > 1 && showitems < pages) pagination += "<li><a href='/page/"+prev+"/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='list'>Prev</a></li>";

        for (var i=1; i <= pages; i++)
            {
            	if (1 != pages &&( !(i >= pgs+range+1 || i <= pgs-range-1) || pages <= showitems ))
            {
            /*if( grdview=="grid" )
          ap+=( (pgs * 2)-1 == i)?'<li class="current"><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="grid">'+i+'</a></li>':'<li><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="grid">'+i+'</a></li>';
             else*/
          ap+=(pgs == i)?'<li class="current"><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="list">'+i+'</a></li>':'<li><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="list">'+i+'</a></li>';

            }

        }
        pagination+=ap;

        if (pgs < pages && showitems < pages) pagination+= "<li><a href='/page/"+next+"/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='list'>Next</a></li>";
        if (pgs < pages-1 &&  pgs+range-1 < pages && showitems < pages) pagination+= "<li><a href='/page/"+pages+"/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='list'>Last</a></li>";
        pagination += "</ul>\n";

            $("ul.pagination").remove();
            $( ".do_pagination" ).prepend( pagination );


    }

function videopress_pagination_js_grid(pgs,srt,grdview)
        {

            var range=2;
            var showitems = (range * 2)+1;

            if( $(".layout-2 .row input[id='newCount']") )
            var val=$(".layout-2 .row input[id='newCount']").val()/12;
            else
            var val=$("#hiddeninput").val()/12;
            var pages=Math.ceil(val);
            var ap="";

            //if(Number(pgs)>pages){pgs=pages;}

            var pgs=( grdview=="list" ) ? Math.ceil(eval((Number(pgs)/2)) ) : pgs;

            var next=( grdview=="list" ) ? eval(pgs+1) : eval(Number(pgs)+1);
            var prev=( grdview=="list" ) ? eval(pgs-1) : eval(Number(pgs)-1);


         var pagination="";
       pagination+='<ul class="pagination gridviewstyle">';
        if(pgs > 2 && pgs > range+1 && showitems < pages) pagination+= "<li><a href='/page/1/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='grid'>First</a></li>";
        if(pgs > 1 && showitems < pages) pagination += "<li><a href='/page/"+prev+"/&sortvideo="+srt+"' data-sortvideo='"+srt+"'  data-viewstyle='grid'>Prev</a></li>";
        if(pages ==1 && pgs==2 ) pagination+= "<li><a href='/page/1/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='grid'>First</a></li><li class='current'><a href='/page/2/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='grid'>2</a></li>";


        for (var i=1; i <= pages; i++)
            {
            	if (1 != pages &&( !(i >= pgs+range+1 || i <= pgs-range-1) || pages <= showitems ))
            {
            /*if( grdview=="list" )
          ap+=( Math.ceil(pgs / 2) == i)? '<li class="current"><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="list">'+i+'</a></li>':'<li><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="list">'+i+'</a></li>';
             else*/
          ap+=(pgs == i)? '<li class="current"><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="grid">'+i+'</a></li>':'<li><a href="/page/'+i+'/&sortvideo='+srt+'" data-sortvideo="'+srt+'" data-viewstyle="grid">'+i+'</a></li>';

            }
        }
         pagination+=ap;

        if (pgs < pages && showitems < pages) pagination+= "<li><a href='/page/"+next+"/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='grid'>Next</a></li>";
        if (pgs < pages-1 &&  pgs+range-1 < pages && showitems < pages) pagination+= "<li><a href='/page/"+pages+"/&sortvideo="+srt+"' data-sortvideo='"+srt+"' data-viewstyle='grid'>Last</a></li>";
        pagination += "</ul>\n";

           $("ul.pagination.initial").remove();
           $( ".do_pagination" ).append( pagination );


    }
       function createModal(){
               var appendix='<div id="themeModal" style="display: none; position: fixed; z-index: 9999999; left: 0; top: 0; width: 100%; height: 100%; overflow: auto; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">'+
               '<div style="background-color: #fefefe; margin: 15% auto; padding: 10px; border: 1px solid #888; width: 400px;">'+
               '<h3 style="text-align:center; padding:10px 10px 0px 10px;margin:0px; text-transform:lowercase !important">Loading Results<br><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></h3><br>'+
               '</div>'+
               '</div>';
               	 
               jQuery("body").prepend(appendix);

               document.querySelector("#themeModal").style.display = "block";

            }

//FA end
</script>

<?php get_footer(); ?>