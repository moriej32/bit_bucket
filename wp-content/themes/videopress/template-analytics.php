<?php /* Template Name: analytics*/ ?>

<style type="text/css">
.footer-container .container.p_90 {
  
    bottom: 0;
    background-color: #000;
    padding: 10px;
	padding--bottom: 0px;
}
.footer-container.nopadding{
	background-color: #fff;	
}
@charset "utf-8";
/* CSS Document */
.shadow {
	box-shadow: 0px 1px 4px 1px rgba(0,0,0,0.3);
}
.section1 {
	padding: 50px 0px 50px 0px;
}
.section2 {
	padding: 50px 0px 50px 0px;
}
.section3 {
	padding: 50px 0px 50px 0px;
}
.videos {
	position: relative;
	padding-top: 10px;
	padding-bottom: 10px;
}
/*
.tab-content {   
  width: 30% !important;    
} */
.vidoes-pic {
	position: absolute;
	left: 0;
}
.vidoes-pic img {
    height: 100px;
	width: 100px;
}
.video-text {
	margin-left: 116px;
	min-height: 92px;
}
.revenue {
	padding-top: 50px;
	text-align: center;
}
.videos .video-text p {
	margin: 0 0 0em;
	    font-size: 13px;
}
.sort-button {
	padding-top: 10px;
}
.chart {
	padding-top: 25px;
}
p {
    text-align: left;
}
.footer-container {
   
    padding-bottom: 0px;
 
}
.highcharts-container {
    width:100% !important;
    height:100% !important;
}
.footer-container {
    padding-top: 40px;
    padding-bottom: 0px !important;
    background: #000000;
    color: #666666;
}
</style>
<!-- Bootstrap -->
<link href="https://bootswatch.com/paper/bootstrap.min.css" rel="stylesheet">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<?php
get_header();
for($j=1; $j<=date('t'); $j++)
{
	$days[] = "$j";	
	$data_days[] = 0;
}
global $paged;
$limit = false;
$per_page = 10;

$vpage = isset( $_GET['vpage'] ) ? abs( (int) $_GET['vpage'] ) : 1;
if ($vpage > 1) {
    $voffset = $vpage * $per_page - $per_page;
} else {
    $voffset = 0;
}

$fpage = isset( $_GET['fpage'] ) ? abs( (int) $_GET['fpage'] ) : 1;
if ($fpage > 1) {
    $foffset = $fpage * $per_page - $per_page;
} else {
    $foffset = 0;
}

$bpage = isset( $_GET['bpage'] ) ? abs( (int) $_GET['bpage'] ) : 1;
if ($bpage > 1) {
    $boffset = $bpage * $per_page - $per_page;
} else {
    $boffset = 0;
}

$ppage = isset( $_GET['ppage'] ) ? abs( (int) $_GET['ppage'] ) : 1;
if ($ppage > 1) {
    $poffset = $ppage * $per_page - $per_page;
} else {
    $poffset = 0;
}

if (!isset($_GET['vpage']) && !isset($_GET['bpage']) && !isset($_GET['fpage']) && !isset($_GET['ppage']))
	$_GET['vpage'] = 1;

//$time_zone = getTimeZoneFromIpAddress();
//echo 'Your Time Zone is '.$time_zone;

function getTimeZoneFromIpAddress(){
    $clientsIpAddress = get_client_ip();

    $clientInformation = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$clientsIpAddress));

    $clientsLatitude = $clientInformation['geoplugin_latitude'];
    $clientsLongitude = $clientInformation['geoplugin_longitude'];
    $clientsCountryCode = $clientInformation['geoplugin_countryCode'];

    $timeZone = get_nearest_timezone($clientsLatitude, $clientsLongitude, $clientsCountryCode) ;

    return $timeZone;

}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function get_nearest_timezone($cur_lat, $cur_long, $country_code = '') {
    $timezone_ids = ($country_code) ? DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country_code)
        : DateTimeZone::listIdentifiers();

    if($timezone_ids && is_array($timezone_ids) && isset($timezone_ids[0])) {

        $time_zone = '';
        $tz_distance = 0;

        //only one identifier?
        if (count($timezone_ids) == 1) {
            $time_zone = $timezone_ids[0];
        } else {

            foreach($timezone_ids as $timezone_id) {
                $timezone = new DateTimeZone($timezone_id);
                $location = $timezone->getLocation();
                $tz_lat   = $location['latitude'];
                $tz_long  = $location['longitude'];

                $theta    = $cur_long - $tz_long;
                $distance = (sin(deg2rad($cur_lat)) * sin(deg2rad($tz_lat)))
                    + (cos(deg2rad($cur_lat)) * cos(deg2rad($tz_lat)) * cos(deg2rad($theta)));
                $distance = acos($distance);
                $distance = abs(rad2deg($distance));
                // echo '<br />'.$timezone_id.' '.$distance;

                if (!$time_zone || $tz_distance > $distance) {
                    $time_zone   = $timezone_id;
                    $tz_distance = $distance;
                }

            }
        }
        return  $time_zone;
    }
    return 'unknown';
}

?>
<div class="container p_90">
<div class="grid-3-4 centre-block single-page">
<div class="entry" style="text-align:center;background-color:#ececec;">            

<?php if(1) { 
		//is_user_logged_in()
		$userId = get_current_user_id();
		//$userId = 5;
		$table_name = $wpdb->prefix."oio_custom_clicks_count";		
		$getadfreeuser=$wpdb->get_row("SELECT sum(cost) as clickR FROM $table_name WHERE author_id=".$userId);
		$banner_clicks_total = $getadfreeuser->clickR;		
		
		$getadfreeusery=$wpdb->get_row("SELECT sum(cost) as clickY FROM $table_name WHERE YEAR(date) = YEAR(NOW()) and author_id=".$userId);
		$banner_clicks_year = $getadfreeusery->clickY;
		
		$getadfreeuserm=$wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and author_id=".$userId);
		$banner_clicks_month = $getadfreeuserm->clickM;
		
		$getadfreeuserd=$wpdb->get_row("SELECT sum(cost) as clickD FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW()) and author_id=".$userId);
		$banner_clicks_day = $getadfreeuserd->clickD;
	
		$table_name2 = $wpdb->prefix."oio_custom_views_count";		
		$getadfreeuser2=$wpdb->get_row("SELECT sum(cost) as viewR FROM $table_name2 WHERE author_id=".$userId);
		$video_views_total = $getadfreeuser2->viewR;		
			
		$getadfreeusery2=$wpdb->get_row("SELECT sum(cost) as clickY FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) and author_id=".$userId);
		$video_views_year = $getadfreeusery2->clickY;

		$getadfreeuserm2=$wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and author_id=".$userId);
		$video_views_month = $getadfreeuserm2->clickM;

		$getadfreeuserd2=$wpdb->get_row("SELECT sum(cost) as clickD FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW()) and author_id=".$userId);
		$video_views_day = $getadfreeuserd2->clickD;
		
		$grand_total = $banner_clicks_total + $video_views_total;		
		$year_total  = $banner_clicks_year + $video_views_year;
		$month_total = $banner_clicks_month + $video_views_month;
		$day_total = $banner_clicks_day + $video_views_day;
		
		$dta = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$q = "select sum(cost) as ccost, month(date) as cmonth from $table_name WHERE YEAR(date) = YEAR(NOW()) and author_id=".$userId." group by month(date)";
		$res = $wpdb->get_results( $q );
		if($res)
		{
			foreach ($res as $d)
			{
				$dta[$d->cmonth-1] = (float) $d->ccost; 
			}
		}
		
		$q2 = "select sum(cost) as ccost, month(date) as cmonth from $table_name2 WHERE YEAR(date) = YEAR(NOW()) and author_id=".$userId." group by month(date)";
		$res2 = $wpdb->get_results( $q2 );			
		if($res2)
		{
			foreach ($res2 as $d2)
			{
				$dta[$d2->cmonth-1] =  $dta[$d2->cmonth-1] + $d2->ccost; 
			}
		}
		
		$q3 = "select sum(cost) as ccost, day(date) as cday from $table_name WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) and author_id=".$userId." group by day(date)";
		$res3 = $wpdb->get_results( $q3 );
		if($res3)
		{
			foreach ($res3 as $d3)
			{
				$data_days[$d3->cday-1] = (float) $d3->ccost; 
			}
		}
		
		$q4 = "select sum(cost) as ccost, day(date) as cday from $table_name2 WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) and author_id=".$userId." group by day(date)";
		$res4 = $wpdb->get_results( $q4 );
		if($res4)
		{
			foreach ($res4 as $d4)
			{
				$data_days[$d4->cday-1] =  $data_days[$d4->cday-1] + $d4->ccost; 
			}
		}
		
		$days_h = array("1 AM","2 AM","3 AM","4 AM","5 AM","6 AM","7 AM","8 AM","9 AM","10 AM","11 AM","12 PM","13 PM","14 PM","15 PM","16 PM","17 PM","18 PM","19 PM","20 PM","21 PM","22 PM","23 PM","24 AM");
		$data_hours = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$q5 = "select sum(cost) as ccost, hour(time) as chour from $table_name WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) AND DAY(date) = DAY(NOW()) and author_id=".$userId." group by hour(time)";		
		
		$res5 = $wpdb->get_results( $q5 );
		if($res5)
		{
			foreach ($res5 as $d5)
			{
				$data_hours[$d5->chour-1] = (float) $d5->ccost; 
			}
		}
		
		$q6 = "select sum(cost) as ccost, hour(time) as chour from $table_name2 WHERE YEAR(date) = YEAR(NOW()) and MONTH(date)=MONTH(NOW()) AND DAY(date) = DAY(NOW()) and author_id=".$userId." group by hour(time)";		
		
		$res6 = $wpdb->get_results( $q6 );
		if($res6)
		{
			foreach ($res6 as $d6)
			{
				$data_hours[$d6->chour-1] =  $data_hours[$d6->chour-1] + $d6->ccost; 				 
			}
		}				
				
?>
	
	<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
		<button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Graph Filter<span class="caret"></span></button>
		<ul class="dropdown-menu dropdown-menu-left" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#"  onClick = "createGraphM(2);">Day</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#"  onClick = "createGraphM(1);">Month</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#"  onClick = "createGraphM(3);">Year</a></li>
		</ul>
	</div>

  <div class="row">
    <div class="col-lg-8 col-md-8">
      <div class="section1">
        <div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		<div id="chart1" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>
		<div id="chart2" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="row revenue">
        <div class="col-md-8 col-xs-8">
          <p style="font-size: 13px;">Total Revenue Today : </p>
          <p style="font-size: 13px;">Total Revenue This Month : </p>
          <p style="font-size: 13px;">Total Revenue This Year : </p>
          <p style="font-size: 13px;">Total Revenue All Time : </p>
        </div>
        <div class="col-md-4 col-xs-4">
          <p style="font-size: 13px;">$ <?php echo $day_total; ?></p>
          <p style="font-size: 13px;">$ <?php echo $month_total; ?></p>
          <p style="font-size: 13px;">$ <?php echo $year_total; ?></p>
          <p style="font-size: 13px;">$ <?php echo $grand_total; ?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="section2">
    <div role="tabpanel">
      <ul class="nav nav-tabs" role="tablist" style="margin-left: 0px;">
        <li class="<?php if (isset($_GET['vpage'])) echo "active";?>"><a href="#vidoes" data-toggle="tab" role="tab">Videos</a></li>
        <li class="<?php if (isset($_GET['fpage'])) echo "active";?>"><a href="#files" data-toggle="tab" role="tab">Files</a></li>
        <li class="<?php if (isset($_GET['bpage'])) echo "active";?>"><a href="#books" data-toggle="tab" role="tab">Books</a></li>
        <li class="<?php if (isset($_GET['ppage'])) echo "active";?>"><a href="#products" data-toggle="tab" role="tab">Products</a></li>
      </ul>
      <div id="tabContent1" class="tab-content">
	  <!-- Videos Tab Start -->
        <div class="tab-pane fade <?php if (isset($_GET['vpage'])) echo "in active";?>" id="vidoes" >
			<!--<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
				<button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
				<ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Alphabetical</a></li>
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest Earned</a></li>
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Lowest Earned</a></li>					
				</ul>      
			</div>-->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php
			
			$tquery= 'SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_status="publish" and post_type="post" AND post_author='.$userId.' ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC limit '.$voffset.",".$per_page;
	
			$result = $wpdb->get_results( $tquery );
				
			if ($result)
			{			
				foreach ($result as $data)
				{					
					$postID 		 = $data->ID;
					$post_url 		 = $data->guid;	
					$image = wp_get_attachment_url( get_post_thumbnail_id($postID));
					if(!$image)
						$image = get_stylesheet_directory_uri()."/images/no-image-thumb.png";					
					
					$qry1= 'SELECT count(*) as viewC FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
					$result1 = $wpdb->get_results($qry1);					
					$viewcount = $result1[0]->viewC;
					
					$qry2= 'SELECT count(*) as clickC FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
					$result2 = $wpdb->get_results($qry2);					
					$clickcount = $result2[0]->clickC;
					
					$qry3= 'SELECT sum(cost) as clickCost FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
					$result3 = $wpdb->get_results($qry3);					
					$clickcost = $result3[0]->clickCost;
					
					$qry4= 'SELECT sum(cost) as viewCost FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
					$result4 = $wpdb->get_results($qry4);					
					$viewcost = $result4[0]->viewCost;
					
					$postRevenue = $clickcost + $viewcost;
					
					?>
					
					<div class="videos">
						<div class="vidoes-pic">								
							<a href="analytics-detail/?id=<?php echo $postID; ?>"><img src="<?php echo $image;?>" height="100px" width="100px"></a>
						</div>	
						<div class="video-text">
						  <p>Title: <a href="analytics-detail/?id=<?php echo $postID; ?>"><?php echo $data->post_title; ?></a></p>
						  <p>Counted Video Ads Views: <?php echo $viewcount; ?></p>
						  <p>Counted Banner Ads Clicks: <?php echo $clickcount; ?></p>
						  <p>Revenue Earned: <?php echo $postRevenue; ?></p>
						</div>
					</div>			
			
				<?php 		
				}								
				$vtotal = $wpdb->get_var("SELECT count(*) FROM ".$wpdb->prefix."posts WHERE post_status='publish' and post_type='post' AND post_author =".$userId);
				$paginatio = "<div class='clearfix'></div><div class='paginate_division'>".paginate_links(array(
					'format' => '?vpage=%#%',
					'prev_text' => __('&laquo;'),
					'next_text' => __('&raquo;'),
					'total' => ceil($vtotal / $per_page),
					'current' => $vpage
				))."</div>";
				$paginatio = str_replace("bpage=","",$paginatio);
				$paginatio = str_replace("fpage=","",$paginatio);
				$paginatio = str_replace("ppage=","",$paginatio);
				echo $paginatio;
			}
				
			?>
			</div>
		</div>
		<!-- Videos Tab End -->
		
		<!-- Files Tab Start -->
        <div class="tab-pane fade <?php if (isset($_GET['fpage'])) echo "in active";?>" id="files">
			<!--<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
			  <button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
			  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Alphabetical</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest Earned</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Lowest Earned</a></li>                
			  </ul>
			</div>-->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php			
				$tquery= 'SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_status="publish" and post_type="dyn_file" AND post_author='.$userId.' ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC limit '.$foffset.",".$per_page;

				$result = $wpdb->get_results( $tquery );
				//echo "<pre>";
				//print_r($result);
				//echo "</pre>";
				
				if ($result)
				{			
					foreach ($result as $data)
					{
						$postID 		= $data->ID;
						$post_url 		 = $data->guid;	
						//apply_filters( 'dyn_file_image', $output, $postID );
						$uploaded_imgssss = get_post_meta( $postID, 'dyn_thumbaniimg_src', true );
						$type 			= apply_filters( 'dyn_file_type_by_mime', $postID );
						if($type == "Document")
							$cls = "dyn-doc";
						elseif($type == "AVI")
							$cls = "dyn-avi";
						elseif($type == "Photoshop")
							$cls = "dyn-psd";
						elseif($type == "Powerpoint")
							$cls = "dyn-ppt";
						elseif($type == "GIF")
							$cls = "dyn-gif";
						elseif($type == "FLV")
							$cls = "dyn-flv";
						elseif($type == "Excel")
							$cls = "dyn-xls";
						elseif($type == "WMV")
							$cls = "dyn-wmv";
						elseif($type == "Zip")
							$cls = "dyn-zip";
						elseif($type == "Image")
							$cls = "dyn-jpg";
						elseif($type == "PDF")
							$cls = "dyn-pdf";	
						elseif($type == "Audio")
							$cls = "dyn-mp3";
						elseif($type == "video/audio")
							$cls = "dyn-mp4";		
						else
							$cls = "dyn-unknown";	
						
						$qry1= 'SELECT count(*) as viewC FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
						$result1 = $wpdb->get_results($qry1);					
						$viewcount = $result1[0]->viewC;
						
						$qry2= 'SELECT count(*) as clickC FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
						$result2 = $wpdb->get_results($qry2);					
						$clickcount = $result2[0]->clickC;
						
						$qry3= 'SELECT sum(cost) as clickCost FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
						$result3 = $wpdb->get_results($qry3);					
						$clickcost = $result3[0]->clickCost;
						
						$qry4= 'SELECT sum(cost) as viewCost FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
						$result4 = $wpdb->get_results($qry4);					
						$viewcost = $result4[0]->viewCost;
						
						$postRevenue = $clickcost + $viewcost;						
						
			?>			
						<div class="videos">
                            <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
							
							 <div class="vidoes-pic">
							    <a href="analytics-detail/?id=<?php echo $postID; ?>">
							      <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>">
							    </a>
							 </div>
							 
							<?php } else { ?>							
							<a href="analytics-detail/?id=<?php echo $postID; ?>"><div class="vidoes-pic <?php echo $cls; ?>" style="width: 100px; height: 100px;"></div></a>	
							<?php } ?>
							<div class="video-text">								
								<p>Title: <a href="analytics-detail/?id=<?php echo $postID; ?>"><?php echo $data->post_title; ?></a></p>
								<p>Counted Video Ads Views: <?php echo $viewcount; ?></p>
								<p>Counted Banner Ads Clicks: <?php echo $clickcount; ?></p>
								<p>Revenue Earned: <?php echo $postRevenue; ?></p>
							</div>
						</div>			
				
					<?php 		
					}
								
					$ftotal = $wpdb->get_var("SELECT count(*) FROM ".$wpdb->prefix."posts WHERE post_status='publish' and post_type='dyn_file' AND post_author =".$userId);
					$paginatio = "<div class='clearfix'></div><div class='paginate_division'>".paginate_links(array(
						'format' => '?fpage=%#%',
						'prev_text' => __('&laquo;'),
						'next_text' => __('&raquo;'),
						'total' => ceil($ftotal / $per_page),
						'current' => $fpage
					))."</div>";
					$paginatio = str_replace("bpage=","",$paginatio);
					$paginatio = str_replace("vpage=","",$paginatio);
					$paginatio = str_replace("ppage=","",$paginatio);
					echo $paginatio;
				}
						?> 
			</div>
        </div>
		<!-- Files Tab End -->
		
		<!-- Books Tab Start -->
        <div class="tab-pane fade <?php if (isset($_GET['bpage'])) echo "in active";?>" id="books">
          <!--<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
              <button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Alphabetical</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest Earned</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Lowest Earned</a></li>
             
              </ul>
          </div>-->
		  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <?php			
				$tquery= 'SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_status="publish" and post_type="dyn_book" AND post_author='.$userId.' ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC limit '.$boffset.",".$per_page;

				$result = $wpdb->get_results( $tquery );
				//echo "<pre>";
				//print_r($result);
				//echo "</pre>";
				
				if ($result)
				{			
					foreach ($result as $data)
					{
						$postID 		= $data->ID;
						//$post_url 		 = $data->guid;	
						 $post_url 		 = get_post_permalink( $postID );
						$image = wp_get_attachment_url( get_post_thumbnail_id($postID));
						if(!$image)
							$image = get_stylesheet_directory_uri()."/images/no-image-thumb.png";
						
						$qry1= 'SELECT count(*) as viewC FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
						$result1 = $wpdb->get_results($qry1);					
						$viewcount = $result1[0]->viewC;
						
						$qry2= 'SELECT count(*) as clickC FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
						$result2 = $wpdb->get_results($qry2);					
						$clickcount = $result2[0]->clickC;
						
						$qry3= 'SELECT sum(cost) as clickCost FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
						$result3 = $wpdb->get_results($qry3);					
						$clickcost = $result3[0]->clickCost;
						
						$qry4= 'SELECT sum(cost) as viewCost FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
						$result4 = $wpdb->get_results($qry4);					
						$viewcost = $result4[0]->viewCost;
						
						$postRevenue = $clickcost + $viewcost;
						
				?>			
						<div class="videos">							
							<div class="vidoes-pic">								
								<a href="analytics-detail/?id=<?php echo $postID; ?>"><img src="<?php echo $image;?>" height="100px" width="100px"></a>
							</div>	
							<div class="video-text">								
								<p>Title: <a href="analytics-detail/?id=<?php echo $postID; ?>"><?php echo $data->post_title; ?></a></p>
								<p>Counted Video Ads Views: <?php echo $viewcount; ?></p>
								<p>Counted Banner Ads Clicks: <?php echo $clickcount; ?></p>
								<p>Revenue Earned: <?php echo $postRevenue; ?></p>
							</div>
						</div>			
				
				<?php 		
					}
					$btotal = $wpdb->get_var("SELECT count(*) FROM ".$wpdb->prefix."posts WHERE post_status='publish' and post_type='dyn_book' AND post_author =".$userId);
					$paginatio = "<div class='clearfix'></div><div class='paginate_division'>".paginate_links(array(
						'format' => '?bpage=%#%',
						'prev_text' => __('&laquo;'),
						'next_text' => __('&raquo;'),
						'total' => ceil($btotal / $per_page),
						'current' => $bpage,
						'add_args' => false
					))."</div>";
					$paginatio = str_replace("vpage=","",$paginatio);
					$paginatio = str_replace("fpage=","",$paginatio);
					$paginatio = str_replace("ppage=","",$paginatio);
					echo $paginatio;
				}
					
				?> 
		  </div>
        </div>
		<!-- Books Tab End -->
		
		<!-- Products Tab Start -->
        <div class="tab-pane fade <?php if (isset($_GET['ppage'])) echo "in active";?>" id="products">
            <!--<div class="sort-button col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown" >
              <button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" style="float: left; display: block">Sort By <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="list-style-position: outside; list-style-type: none;">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Alphabetical</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest Earned</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Lowest Earned</a></li>
              </ul>
            </div>-->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  <?php			
				$tquery= 'SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_status="publish" and post_type="product" AND post_author='.$userId.' ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC limit '.$poffset.",".$per_page;

				$result = $wpdb->get_results( $tquery );
				//echo "<pre>";
				//print_r($result);
				//echo "</pre>";
				
				if ($result)
				{			
					foreach ($result as $data)
					{
						$postID 		= $data->ID;
						$post_url 		 = $data->guid;	
						$image = wp_get_attachment_url( get_post_thumbnail_id($postID));
						if(!$image)
							$image = get_stylesheet_directory_uri()."/images/no-image-thumb.png";
						
						$qry1= 'SELECT count(*) as viewC FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
						$result1 = $wpdb->get_results($qry1);					
						$viewcount = $result1[0]->viewC;
						
						$qry2= 'SELECT count(*) as clickC FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
						$result2 = $wpdb->get_results($qry2);					
						$clickcount = $result2[0]->clickC;
						
						$qry3= 'SELECT sum(cost) as clickCost FROM '.$wpdb->prefix.'oio_custom_clicks_count WHERE contentUrl="'.$post_url.'"';
						$result3 = $wpdb->get_results($qry3);					
						$clickcost = $result3[0]->clickCost;
						
						$qry4= 'SELECT sum(cost) as viewCost FROM '.$wpdb->prefix.'oio_custom_views_count WHERE contentUrl="'.$post_url.'"';
						$result4 = $wpdb->get_results($qry4);					
						$viewcost = $result4[0]->viewCost;
						
						$postRevenue = $clickcost + $viewcost;
						
				?>			
						<div class="videos">							
							<div class="vidoes-pic">								
								<a href="analytics-detail/?id=<?php echo $postID; ?>"><img src="<?php echo $image;?>" height="100px" width="100px"></a>
							</div>	
							<div class="video-text">								
								<p>Title: <a href="analytics-detail/?id=<?php echo $postID; ?>"><?php echo $data->post_title; ?></a></p>
								<!--<p>Counted Video Ads Views: <?php //echo $viewcount; ?></p>-->
								<p>Counted Banner Ads Clicks: <?php echo $clickcount; ?></p>
								<p>Revenue Earned: <?php echo $postRevenue; ?></p>
							</div>
						</div>			 
				
				<?php 		
					}
					$ptotal = $wpdb->get_var("SELECT count(*) FROM ".$wpdb->prefix."posts WHERE post_status='publish' and post_type='product' AND post_author =".$userId);
					$paginatio = "<div class='clearfix'></div><div class='paginate_division'>".paginate_links(array(
						'format' => '?ppage=%#%',
						'prev_text' => __('&laquo;'),
						'next_text' => __('&raquo;'),
						'total' => ceil($ptotal / $per_page),
						'current' => $ppage
					))."</div>";
					$paginatio = str_replace("vpage=","",$paginatio);
					$paginatio = str_replace("fpage=","",$paginatio);
					$paginatio = str_replace("bpage=","",$paginatio);
					echo $paginatio;
					
				}
					
				?> 
			</div>
        </div>
		<!-- Products Tab Ends -->
      </div>
    </div>
  </div>

	
<?php 
	}
else{ 
?>
	<h2>You need to login to access this page.</h2>
<?php }?>
<div class="clear"></div>
</div>
</div>
</div>
<div class="spacing-40"></div>
<?php get_footer();?>
<script
  src="https://code.jquery.com/jquery-1.11.2.js"
  integrity="sha256-WMJwNbei5YnfOX5dfgVCS5C4waqvc+/0fV7W2uy3DyU="
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>

/* $( window ).load(function() {
  $("#chart1").hide();
  $("#chart2").hide();
}) */

function createGraphM (id)
{
	if(id==3)
	{
		document.getElementById('chart1').style.display = "none";
		document.getElementById('chart2').style.display = "none";	
		document.getElementById('chart').style.display = "block"; 
	}
	else if(id==1)
	{
        $('#chart1').show(function () {
			chart1();
        });
		//document.getElementById('chart1').style.display = "block";
		document.getElementById('chart2').style.display = "none";	
		document.getElementById('chart').style.display = "none"; 
	}
	else if(id==2)
	{
		
        $('#chart2').show(function () {
			chart2();
        });
		document.getElementById('chart1').style.display = "none";
		//document.getElementById('chart2').style.display = "block";	
		document.getElementById('chart').style.display = "none"; 
	}
			
	
}  

	
$(function () {
	var dataR = [];
	var dataR = <?php echo json_encode($dta); ?>;
    Highcharts.chart('chart', {
        title: {
            text: 'This Year Monthly Revenue',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: 'Months'
            },
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: ' ',
            data: dataR
        }]
    });
});

//$(function () {
	function chart1(){
	var days_m = [];
	var days_m = <?php echo json_encode($days); ?>;
	var data_days = [];
	var data_days = <?php echo json_encode($data_days); ?>;
    Highcharts.chart('chart1', {
        title: {
            text: 'This Month Daily Revenue',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: 'Dates'
            },
            categories: days_m
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: ' ',
            data: data_days
        }]
    });
}


//$(function () {
	function chart2(){
	var days_h = [];
	var days_h = <?php echo json_encode($days_h); ?>;
	var data_hours = [];
	var data_hours = <?php echo json_encode($data_hours); ?>;
    Highcharts.chart('chart2', {
        title: {
            text: 'Today Hourly Revenue',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
			title: {
                text: 'Hours'
            },
            categories: days_h
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
		credits:{
			enabled: false
		},
        series: [{
            name: ' ',
            data: data_hours
        }]
    });
};
</script>