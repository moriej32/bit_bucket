<?php
global $wpdb;

$uploaded_type = get_post_meta( get_the_ID(), 'dyn_upload_type', true );
$bookshortecode = get_post_meta( get_the_ID(), 'bookshortcode', true );

$sqlssss = '
			SELECT *
			FROM wp_book_page
			WHERE   book ='. $bookshortecode; 
		
		
	// $totatpage_books = $wpdb->get_results( $sqlssss );
	 
	$pagesbv =  $wpdb->query( $sqlssss );
	
?>
 <script>
  function stopaudioid(id)
	 {
		 var $this = $(this),
                     stopaudioclass,
					 stopaudiofle;
					 
		            stopaudioclass = "bookmyAudio"+ id;
					stopaudiofle = document.getElementById(stopaudioclass); 
					if(stopaudiofle != null)
					{
					 stopaudiofle.pause();		
					}
	 }
	 
 jQuery(document).ready(function($) {
	 
	 var totalpagebook = '<?php echo $pagesbv; ?>';
	 
	 //alert($('#flipbook-container-37').turn('page')); 
	 
	 $("#flipbook-container-<?php echo $bookshortecode; ?>").bind("turning", function(event, page, view) {
       //alert("Turning the page to: "+page);
	   var $this = $(this),
				leftaudio,
				leftaudioclass,
				rightaudio,
				rightaudioclass,
				leftaudiofile,
				rightaudiofile,
				stopleftaudio,
				stoprightaudio;
				
	   if(page%2 == 0)
	   {
 		 
          leftaudio = 	page;
          leftaudioclass = "bookmyAudio"+ leftaudio;
		  rightaudio = parseInt(page) + 1;
		  rightaudioclass = "bookmyAudio"+ rightaudio;
		  
		  leftaudiofile = document.getElementById(leftaudioclass); 
		  rightaudiofile = document.getElementById(rightaudioclass); 
		  
		  if(leftaudiofile != null)
		   {
			 $('#leftsideaudio').fadeIn(); 
			 $('.leftaudiotext').html('<i class="fa fa-volume-off leftaudiooffnow" aria-hidden="true"></i>');  
			 
			        stopleftaudio = $('#lefttoright').val();
					stopleftaudio = parseInt(stopleftaudio);
					
					stopaudioid(stopleftaudio);
		            
		   }else{
			   
			  $('#leftsideaudio').fadeOut();
			  
                   stopleftaudio = $('#lefttoright').val();
					stopleftaudio = parseInt(stopleftaudio);
					
					stopaudioid(stopleftaudio);
		            		 
		   }
		   
		   
		   if(rightaudiofile != null)
		   {
			  $('#rightsideaudio').fadeIn(); 
			  $('.rightaudiotext').html('<i class="fa fa-volume-off rightaudiooffnow" aria-hidden="true"></i>');
			  
			     stoprightaudio = $('#lefttoright').val();
					stoprightaudio = parseInt(stoprightaudio) + 1;
					
					stopaudioid(stoprightaudio);
					
		   }else{
			   
			  $('#rightsideaudio').fadeOut(); 
			  
			        stoprightaudio = $('#lefttoright').val();
					stoprightaudio = parseInt(stoprightaudio) + 1;
					
					stopaudioid(stoprightaudio);
					
		   }
		   
	     
		 $('#lefttoright').val( page ); 
         $('#righttoleft').val(0); 
		 
	   }else{
		  
		  
		  leftaudio = 	parseInt(page) - 1; 
          leftaudioclass = "bookmyAudio"+ leftaudio;
		  rightaudio = page;
		  rightaudioclass = "bookmyAudio"+ rightaudio;
		  
		  leftaudiofile = document.getElementById(leftaudioclass); 
		  rightaudiofile = document.getElementById(rightaudioclass); 
		  
		  if(leftaudiofile != null)
		   {
			 $('#leftsideaudio').fadeIn(); 
			 $('.leftaudiotext').html('<i class="fa fa-volume-off leftaudiooffnow" aria-hidden="true"></i>');  
			 
                 stopleftaudio = $('#righttoleft').val();
					stopleftaudio = parseInt(stopleftaudio) - 1;
					
					stopaudioid(stopleftaudio);
					
		   }else{
			   
			  $('#leftsideaudio').fadeOut();  
			        stopleftaudio = $('#righttoleft').val();
					stopleftaudio = parseInt(stopleftaudio) - 1;
					
					stopaudioid(stopleftaudio);
		   }
		   
		   
		   if(rightaudiofile != null)
		   {
			  $('#rightsideaudio').fadeIn(); 
			  $('.rightaudiotext').html('<i class="fa fa-volume-off rightaudiooffnow" aria-hidden="true"></i>');
			  
			      stopleftaudio = $('#righttoleft').val();
					stopleftaudio = parseInt(stopleftaudio);
					
					stopaudioid(stopleftaudio);
			 
		   }else{
			   
			  $('#rightsideaudio').fadeOut(); 
			  
			        stopleftaudio = $('#righttoleft').val();
					stopleftaudio = parseInt(stopleftaudio);
					
					stopaudioid(stopleftaudio);
		   }
		  
		  
		 $('#righttoleft').val( page );  
		  $('#lefttoright').val( 0 );  
	   }
	 
      });
	  
	  
	  $('.main-nav').on('click', 'i.leftaudiooffnow', function( e ) {
		 var currentpagenum = $('#lefttoright').val();
		 
		 var url = parseInt(currentpagenum);
		 
		 if(currentpagenum == 0)
		 {
			currentpagenum = $('#righttoleft').val(); 
			url = parseInt(currentpagenum) -1;
		 }
		 
		  
		  
		  var leftsideaudioid = url;
		  
		  var lefaudioclass = "bookmyAudio"+ leftsideaudioid;
		  
		  var leftaudioreal = document.getElementById(lefaudioclass); 
		   
		  leftaudioreal.play();
	   
	    $('.leftaudiotext').html('<i class="fa fa-volume-up leftaudioupfnow" aria-hidden="true"></i>');
	 });
	 
	 
	 $('.main-nav').on('click', 'i.leftaudioupfnow', function( e ) {
		 
		 var currentpagenum = $('#lefttoright').val();
		 
		 var url = parseInt(currentpagenum);
		 
		 if(currentpagenum == 0)
		 {
			currentpagenum = $('#righttoleft').val(); 
			url = parseInt(currentpagenum) -1;
		 }
		 
		  
		  
		  var leftsideaudioid = url;
		  
		  var lefaudioclass = "bookmyAudio"+ leftsideaudioid;
		  
		  var leftaudioreal = document.getElementById(lefaudioclass); 
		  
		  $('.leftaudiotext').html('<i class="fa fa-volume-off leftaudiooffnow" aria-hidden="true"></i>');
		  leftaudioreal.pause();
	 });
	 
	 
	 $('.main-nav').on('click', 'i.rightaudioupfnow', function( e ) {
		 
		 var currentpagenum = $('#righttoleft').val();
		 
		 var url = parseInt(currentpagenum);
		 
		  if(currentpagenum == 0)
		  {
			currentpagenum = $('#lefttoright').val();
			url =  parseInt(currentpagenum)+ 1; 
		  }
		  
		  var rightsideaudioid = url;
		  
		 // alert(rightsideaudioid);
		  
		  var rightaudioclass = "bookmyAudio"+ rightsideaudioid;
		  
		  var rightaudioreal = document.getElementById(rightaudioclass); 
		  
		 // alert(rightaudioreal);
		  
		  rightaudioreal.pause();
	   
	    $('.rightaudiotext').html('<i class="fa fa-volume-off rightaudiooffnow" aria-hidden="true"></i>');
	 });
	 
	  $('.main-nav').on('click', 'i.rightaudiooffnow', function( e ) {
		  
		 var currentpagenum = $('#righttoleft').val();
		 
		 var url = parseInt(currentpagenum);
		 
		  if(currentpagenum == 0)
		  {
			currentpagenum = $('#lefttoright').val();
			url =  parseInt(currentpagenum)+ 1; 
		  }
		  
		  var rightsideaudioid = url;
		  //alert(rightsideaudioid);
		  var rigtaudioclass = "bookmyAudio"+ rightsideaudioid;
		  
		  var rightaudioreal = document.getElementById(rigtaudioclass); 
		  
		   $('.rightaudiotext').html('<i class="fa fa-volume-up rightaudioupfnow" aria-hidden="true"></i>');
		  rightaudioreal.play();
		  
	 });
	 
	 
 });
 
 </script>
 <script>

function toggleCheckbox(element)
 {

      var x = document.getElementById("bookmyAudio");
	  //alert(xt);
      //var x = $('.zoomed#myAudio').value();
	 //element.checked = !element.checked;  zoomed
	 if(element.checked)
	 {
	   //var x = document.getElementById("myAudio");
		 
	    x.play();
		$('.audiotext').html('<i class="fa fa-volume-off" aria-hidden="true"></i>');
	 }
	 if(!element.checked){
	 x.pause();
	 
	   $('.audiotext').html('<i class="fa fa-volume-up" aria-hidden="true"></i>');
	 }
 }
 
 
</script>
<style>
.actives{ 
opacity: 1;
    display: inline-block;
    visibility: visible;
}
.flipbook-container .fb-nav .alternative-nav li.actives, .flipbook-container .fb-nav .alternative-nav li.actives i{ 
     opacity: 1;
    display: inline-block;
    visibility: visible;
}
</style>
<div class="vid-single">

	<div class="file-container">

		<div class="image-holder">
			<?php
			//echo do_shortcode('[dyn-book-popup id='. get_the_ID() . ']<div class="hover-item"></div>[/dyn-book-popup]');
			//$bid = str_replace(" ", "_", $book_name);
             echo do_shortcode( '[responsive-flipbook id="'. $bookshortecode .'"]' );
			/*$bid = strtolower(str_replace(" ", "_", get_the_title()));
			// echo do_shortcode( '[flipbook-popup id="'.$bid.'"]' );
			//if(strlen($book_name)>10){
			$valuetest =  do_shortcode( '[responsive-flipbook id="'.$bid.'"]' );
			if (preg_match('/ERROR:/',$valuetest))
			{
			 $bid = strtolower(str_replace(" ", "", get_the_title()));	
			 echo do_shortcode( '[responsive-flipbook id="'.$bid.'"]' );
			}
			else{
				echo $valuetest;
			}*/
			//                          } else {
			//                            echo do_shortcode( '[flipbook-popup id="'.$bid.'"]'.$book_name.'[/flipbook-popup]' );
			//
			//       flipbook-popup                   }
			/*if (has_post_thumbnail()) {
				the_post_thumbnail('medium-thumb', array( 'class' => 'layout-2-thumb' ) );
			} else {
				$the_ID = get_the_ID();
				$dyn_book_options = dyn_get_options_mpcrf($the_ID);
				//book cover
				$img_url = $dyn_book_options['books'][$the_ID]['rfbwp_fb_hc_fco'];
				if ($img_url!='') {
					echo "<img src=\"$img_url\" class=\"layout-2-thumb wp-post-image\" width=\"500\" height=\"270\" \>";
				} else {
					//first words
					echo $dyn_book_options['books'][$the_ID]['pages'][0]['rfbwp_page_html'];
				}
			}*/
			?>
			<input type="hidden" id="lefttoright" value="0">
			<input type="hidden" id="righttoleft" value="0">
		</div>
		<div style="width:100%;margin-top:10px;">

			<?php
			$country_id = oiopub_settings::get_user_country_index();

			if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
				// Old code
				//$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";
               // new code
			    $queryyy = " AND country IN ($country_id,'all')  ";    
			   
			} else {
				$queryyy = " ";
			}
			$tagSelected = wp_get_post_tags($post->ID);
			$tagsNameArray = array();
			$tagsSlugArray = array();
			$tagsIDArray = array();
			$tags_str = '';
			foreach($tagSelected as $tag)
			{
				$tags_str .= "'".$tag->name."',";
			}
			$tags_str = substr($tags_str, 0, -1);
			$bagfhfa = ''; $bagfhf = '';
			global $wpdb;
			$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
			if($ptags) {
				$ptagname = '';
				$ptagpurchaseid = '';foreach ($ptags as $ptag) {

					$ptagname = $ptag->tag;
					$ptagpurchaseid = $ptag->purchase_id;
				}
				if($ptags)	{$bagfhfa = "AND item_id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


					global $wpdb;
					$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
					$tagname = '';
					$tagpurchaseid = '';
					foreach ($tags as $tag) {

						$tagname = $tag->tag;
						$tagpurchaseid = $tag->purchase_id;
					}

					if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND item_id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
				}
			}

			$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 5 and item_status=1 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


			$bannerurl = '';
			$bannerid = '';
			$countryinbase = '';
			foreach ($banners as $banner) {

				$bannerurl = $banner->item_url;
				$bannerid = $banner->item_id;
			}


			?>
			<?php
			if ($banners) {


				?>	  <a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
			<?php } ?> </div>
		<div class="google-ads" style="position: relative;display: block;right:-5%;bottom: 180px;background-color: #bcd5e6;text-align: center;width: 90%; height: 30%;display:none">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- Doityourselfnationn -->
			<ins class="adsbygoogle"
				 style="display:block"
				 data-ad-client="ca-pub-4835145942281602"
				 data-ad-slot="4268322948"
				 data-ad-format="auto"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>