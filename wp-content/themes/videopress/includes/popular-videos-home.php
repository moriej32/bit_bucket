<h6 class="title-bar"><span>Popular Videos</span></h6>
<div class="spacer-dark"></div>

    <!-- Start Categorized post -->
    <div class="layout-3 related-videos">
    <div class="homepage-video-holder" data-slick='{"slidesToShow": 5, "slidesToScroll": 1}'>
    <?php
	$videopress_gridcounter = 0;
	$postcounter_1 = 0;
    $args = array(
		'meta_key' => 'popularity_count',
		'orderby'  => 'meta_value_num',
		'order'    => 'date',
		'posts_per_page' => 4,
    );
	$postslist = get_posts( $args );
	foreach($postslist as $post) :  setup_postdata($post);

	$videopress_gridcounter++;

	?>
    
    <!--<div class="grid-one-fourth <?php if( $videopress_gridcounter == '1' ){ echo ' first'; }elseif( $videopress_gridcounter == '4' ){ echo ' last'; } ?>">-->
    <div class="home-page-scroller <?php if( $videopress_gridcounter == '1' ){ echo ' first-homepage-video'; }elseif( $videopress_gridcounter == '6' ){ echo ' last'; } ?>">
    
    	<div class="image-holder">
        <a href="<?php echo get_permalink(); ?>">
        <div class="hover-item"></div>
        </a>
        
    	<?php
		if( has_post_thumbnail() ){
        	the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
		}else{
			echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
		}
		?>
        
        </div>
		
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    <ul class="stats">
    <li><?php videopress_displayviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    
    </div>
    
    <?php endforeach; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    </div><!-- end -->
    <div class="scroll-left scroll-left_2"><span class="icon-arrow-left"> </span></div>
    <div class="scroll-right scroll-right_2"><span class="icon-arrow-right"> </span></div>
    <div class="spacing-40"></div>