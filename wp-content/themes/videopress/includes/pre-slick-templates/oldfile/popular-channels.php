<?php
function pre($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}
?>
<h6 class="title-bar"><span>Popular Channels</span></h6>
<div class="spacer-dark"></div>

    <!-- Start Categorized post -->
    <div class="layout-3 related-videos">
    <div class="homepage-video-holder">
    <?php
	
	// Get all users posts, add up their total popularity, and sort in that order, then show their most popular video!
	// START USERS
	global $wpdb;

	$results = $wpdb->get_results( 'SELECT `post_author`,`meta_key`,`meta_value` FROM wp_posts LEFT OUTER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id WHERE `meta_key` = "popularity_count" ORDER BY `meta_value` DESC', ARRAY_A );
	
	//
	function post_author_cmp(array $a, array $b) {
		if ($a['post_author'] < $b['post_author']) {
			return -1;
		} else if ($a['post_author'] > $b['post_author']) {
			return 1;
		} else {
			return 0;
		}
	}
	//
	usort($results,'post_author_cmp');
	
	$result_counter = 0;
	
	for ($result_counter; $result_counter < count($results); $result_counter++){
		 if ($results[$result_counter]['post_author'] == $results[$result_counter + 1]['post_author']){
			 $sum = array('post_author' => $results[$result_counter]['post_author'], 'views' => $results[$result_counter]['meta_value'] + $results[$result_counter + 1]['meta_value']);
		 }else{
			 if ($result_counter == 0){
				$sum_array[$result_counter] = array('post_author' => $results[$result_counter]['post_author'], 'views' => $results[$result_counter]['meta_value']);
			 }else{
			 	$sum_array[$result_counter] = $sum;
			 }
		 }
	}
	
	//
	function views_cmp(array $a, array $b) {
		if ($a['views'] > $b['views']) {
			return -1;
		} else if ($a['views'] < $b['views']) {
			return 1;
		} else {
			return 0;
		}
	}
	//
	usort($sum_array,'views_cmp');
	
	$i = 0;
	foreach($sum_array as $popular_user){
		//require __DIR__ . '/user/profile-home.php';
		$result_2 = $wpdb->get_row( 'SELECT * FROM wp_posts WHERE `post_author` = "'.$popular_user['post_author'].'" AND `post_type` = "post" AND `post_status` = "publish" ORDER BY `id` ASC', ARRAY_A );
		$results_array[$i] = $result_2['ID'];
		$i++;
	}
	
	//pre($results_array);
	
	$videopress_gridcounter = 0;
	$postcounter_1 = 0;
    $args = array(
		'numberposts' => 6, 'post__in' => $results_array, 'order'=> 'post__in'
    );
	$postslist = get_posts( $args );
	
	//pre($postslist);
	
	foreach($postslist as $post) :  setup_postdata($post);
	$videopress_gridcounter++;

	?>
    
    <div class="home-page-scroller <?php if( $videopress_gridcounter == '1' ){ echo ' first-homepage-video'; }elseif( $videopress_gridcounter == '6' ){ echo ' last'; } ?>">
    
    	<div class="image-holder">
        <a href="<?php echo get_permalink(); ?>">
        <div class="hover-item"></div>
        </a>
        
    	<?php
		if( has_post_thumbnail() ){
        	the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
		}else{
			echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
		}
		?>
        
        </div>
		
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    <ul class="stats">
    <li><?php videopress_displayviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    
    </div>
    
    <?php endforeach; wp_reset_query(); ?>
    
    <?php
	
	// END USERS
	
	/*
	if( isset( $videopress_category[0]->term_taxonomy_id ) == '' ){
		// Set Default category to 1 if none
		$videopress_category_id = '';
	}else{
		// else assign value to a category
		$videopress_category_id = $videopress_category[0]->term_taxonomy_id;
	}
	
	$args = array( 'numberposts' => 4, 'order'=> 'DESC', 'category'=> $videopress_category_id, 'exclude'=> $post->ID );
	$postslist = get_posts( $args );
	foreach($postslist as $post) :  setup_postdata($post);
	$videopress_gridcounter++;
	?>
    
    <div class="grid-one-fourth <?php if( $videopress_gridcounter == '1' ){ echo ' first'; }elseif( $videopress_gridcounter == '4' ){ echo ' last'; } ?>">
    
    	<div class="image-holder">
        <a href="<?php echo get_permalink(); ?>">
        <div class="hover-item"></div>
        </a>
        
    	<?php
		if( has_post_thumbnail() ){
        	the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
		}else{
			echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
		}
		?>
        
        </div>
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    <ul class="stats">
    <li><?php videopress_displayviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    
    </div>
    
    <?php endforeach; wp_reset_query(); ?>
	*/ ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    </div> <!-- end -->
    <div class="scroll-left scroll-left_4"><span class="icon-arrow-left"> </span></div>
    <div class="scroll-right scroll-right_4"><span class="icon-arrow-right"> </span></div>
    <div class="spacing-40"></div>