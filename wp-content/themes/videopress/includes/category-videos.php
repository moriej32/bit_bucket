<h6 class="title-bar"><span>Featured Categories</span></h6>
<div class="spacer-dark"></div>

    <!-- Start Categorized post -->
    <div class="layout-3 related-videos">
	<div class="homepage-video-holder">
    <?php
	// Set Variable
	$videopress_gridcounter = 0;
	$videopress_category = '23'; //get_the_category(); 
	
	if( isset( $videopress_category[0]->term_taxonomy_id ) == '' ){
		// Set Default category to 1 if none
		$videopress_category_id = '23';
	}else{
		// else assign value to a category
		$videopress_category_id = '23';
		//$videopress_category_id = $videopress_category[0]->term_taxonomy_id;
	}
	
	$args = array( 'numberposts' => 6, 'order'=> 'DESC', 'category'=> $videopress_category_id, 'exclude'=> $post->ID );
	$postslist = get_posts( $args );
	foreach($postslist as $post) :  setup_postdata($post);
	$videopress_gridcounter++;
	?>
    
    <!--<div class="grid-one-fifth <?php if( $videopress_gridcounter == '1' ){ echo ' first-homepage-video'; }elseif( $videopress_gridcounter == '6' ){ echo ' last'; } ?>">-->
    <div class="home-page-scroller <?php if( $videopress_gridcounter == '1' ){ echo ' first-homepage-video'; }elseif( $videopress_gridcounter == '6' ){ echo ' last'; } ?>">
    
    	<div class="image-holder">
        <a href="<?php echo get_permalink(); ?>">
        <div class="hover-item"></div>
        </a>
        
    	<?php
		if( has_post_thumbnail() ){
        	the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
		}else{
			echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
		}
		?>
        
        </div>
		
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    <ul class="stats">
    <li><?php videopress_displayviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    
    </div>
    
    <?php endforeach; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    </div> <!-- end -->
    <div class="scroll-left scroll-left_1"><span class="icon-arrow-left"> </span></div>
    <div class="scroll-right scroll-right_1"><span class="icon-arrow-right"> </span></div>
    <div class="spacing-40"></div>