<div class="clear"></div>

<!-- Modal -->
<div class="modal-box" id="model_flag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-bodys">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="url" value="<?= get_permalink();?>">
					<textarea rows="10" id="reason" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>
 
<script>
           var $ = jQuery.noConflict();
           
            jQuery('#addflag-submit').live('click',function(e){
                    //$('#addflag-form').submit();
                    //$("form").serialize()
                    var reason = jQuery('#reason').val();
                    var url = jQuery('#url').val();
                    var check_id = "<?php echo get_current_user_id(); ?>";
                    
                    if (check_id == 0){
		alert("You must be logged in to endorse this channel.");	
                    }else{
                        jQuery.ajax({
                                type: "POST",  
                                url: "<?php echo admin_url('admin-ajax.php'); ?>",  
                                data: {action:'user_mark_flag',reason:reason,url:url},
                                success : function(data){
                                        var modelhtml = jQuery('#model_flag .modal-content').html();
                                        jQuery('#model_flag .modal-content').html(data);

                                        setTimeout(function(){
                                                jQuery('#model_flag').modal('hide');
                                                jQuery('#model_flag .modal-content').html(modelhtml);
                                        },3000)
                                }
                        });
                    }     
            });
            
            
	jQuery('#addflag-anchor').click(function(e){
		jQuery('#model_flag').modal();
	});
</script>