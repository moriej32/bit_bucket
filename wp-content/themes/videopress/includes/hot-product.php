<?php
$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;

$post='';

$cat=isset($_GET['cat_type']) ? $_GET['cat_type'] : '';

if($cat=='new-products')
    $bkstart = ($paged==1) ? 0 : intval($paged-1) * 12;
else
    $bkstart=0;

//create custom loop for products
if(!is_user_logged_in()){
    $bktemp= 'SELECT * FROM ' .$wpdb->prefix.'posts
     WHERE post_type="product" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta
     WHERE meta_key="privacy-option" AND meta_value="private")
     ORDER BY   STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC' ;
     $bktquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
}
else if(is_user_logged_in()){
    $bktemp = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="product" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
UNION
SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="product" AND post_status="publish" AND ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.get_current_user_id().'
UNION
SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="product" AND post_status="publish" AND ID IN (SELECT post_id
FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY  STR_TO_DATE(post_date_gmt,"%Y-%m-%d %H:%i:%s")  DESC';

    $bktquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
}

$bkresult = $wpdb->get_results( $bktquery );
//echo  $wpdb->num_rows . 'Rows Found';

$bktotal=$wpdb->query($bktemp);


?>

<div class="list-group">
    <?php echo '<a href="#new-products" id="new-products"><h5><b>NEW PRODUCTS</b></h5></a>'; ?>

    <div class="row">
        <?php
        if($bkresult)
        {
            $bkri=0;

            foreach ($bkresult as $bkdata){
                $postID = $bkdata->ID;
                $post = get_post($postID);
                $privacyOption   = get_post_meta( $postID, 'privacy-option' );
//echo "<pre>";

//print_r ($privacyOption);

//echo "</pre>";

                $selectUsersList = get_post_meta( $postID, 'select_users_list' );
                $post_author     = $bkdata->post_author;
                $user_ID         = get_current_user_id();
                $selectUsersList = explode( ",", $selectUsersList[0] );

                $refr = get_post_meta($postID,'video_options_refr',true);
                if($refr){}
                else{$refr= 'No reference given'; }

                $thumbnail_id=get_post_thumbnail_id($postID);
                $thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
                $imgsrc= $thumbnail_url [0];

                $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");

                $endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
                $endorsnum=count($endors);


                $favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
                $favsnum=count($favs);

                $book_author=get_the_author_meta('display_name', $bkdata->post_author);
                
				$spacicecountry = get_post_meta( $postID, '_spacicecountry' );
				$spacicecountrys = explode( ",", $spacicecountry[0] );

                $current_country = get_user_country_isocode_corrent_country();
				
				$allcountry_accept = get_post_meta( $postID, '_countries_shippinf_op', true );
				
				$shipping_countiess = apply_filters( 'woocommerce_shipping_country_full_by_postid', $postID);

                $thumbnail_id=get_post_thumbnail_id($postID);
                $thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
                $imgsrc= $thumbnail_url [0];
                $product_id=$postID;
                $_product = wc_get_product( $product_id );
                $attachment_ids = $_product->get_gallery_attachment_ids();
                $units_sold = get_post_meta( $postID, 'total_sales', true );
				
				$_productvideofile = get_post_meta( $postID, '_productvideofile', true );
				$_productvideoimage = get_post_meta( $postID, '_productvideoimage', true );
				
				 $imagesdcva = wp_get_attachment_url( get_post_thumbnail_id($postID));
				 
				$imageRow = '';
				$videoproduct = 0;
				if(!empty($_productvideoimage)){
					$imgsrc = $_productvideoimage; 
					$videoproduct = 1;
				}
				
				$popup_poroduct_class = ''; 
				
				 if( $_productvideofile != '' ){
					$popup_poroduct_class = 'video-clase';  
					} else{
					$popup_poroduct_class = 'galleryImgCont';  
				} 

				 $showaccept = 0;
				if( in_array($current_country, $spacicecountrys) )
				{
					$showaccept = 1;
				}
				else if( $allcountry_accept == 'all')
				{
				  $showaccept = 1;	
				}
				$count_total_productss = 0;
				if( $showaccept == 1 )
				{
					$count_total_productss++;
					
                 if(!is_user_logged_in()) {  // case where user is not logged in
                    if(isset($privacyOption[0]) and ($privacyOption[0] != "private")) {
                        //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
                        ?>
                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

                            <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                               <div class="galleryOverlay" style="display:none;">

                                   <?php 
                                         $viddeoData = get_post_meta($postID, $key = '_product_image_gallery');
                                            foreach ($viddeoData as $row) {
                                               //echo $row;
                                                $data = explode(",", $row);
                                                foreach ($data as $row2) {
                                                    //echo $row2."<br>";
                                                   // $imageRow = wp_get_attachment_url( $row2 );
												   $totatthum = count($data);
													if( $totatthum < 2 )
													{
													  $imageRow = 	 $imagesdcva ;
													}else{
														
														 $imageRow = wp_get_attachment_url( $row2 );
													}
													
                                                    ?>

                                                    <img src="<?php echo $imageRow; ?>" style=" width:100%;height: 100%; top: 0px; display: block;">
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="galleryDots">
                                            <?php
                                            $i=1;
                                              $count_dot = count($data) > 5 ? 5 : count($data);
                                                while($i<=$count_dot)
                                              {
                                            ?>
                                              <span class="">•</span>
                                            <?php
                                                $i++;
                                              }
                                            ?>
                                         </div>
                                   </div>

                                    <div class="image-holder <?php echo $popup_poroduct_class; ?>">
									
									 <?php if($_productvideofile != '')
										{ ?>
									     <video class="thevideo" loop preload="none" style="display:none;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/<?php echo $_productvideofile; ?>" type="video/mp4">  
                                           </video>
										<?php } ?>	
                                    <a href="<?php echo get_permalink($postID); ?>">
                                        <!--  <div class="hover-item"></div>-->
                                    </a>

                                    <?php
									
									if($_productvideoimage != ''){
										
										  echo '<img src="'. $_productvideoimage .'" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                               
									}else{
                                    if( has_post_thumbnail($postID) ){
                                        echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
                                    }else{
                                        echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                                    }
									}
									
									?>


                                </div>
                                <div class="layout-title-box text-center" style="display:block;">
                                    <h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
                                        <?php
                                        preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                                        if ( count($matches[0]) && count($matches[1]) >4){
                                            ?>
                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                            <?php
                                        }
                                        else if(strlen($bkdata->post_title)>30){
                                            ?>
                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                                            <?php
                                        }
                                        else {
                                            ?>
                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                                        <?php } ?>
                                    </h6>
                                    <!-- <ul class="stats">
                                            <li ><?php videopress_displayviews( $postID ); ?></li>
                                            <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                            <li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>

                                        </ul>
                                        -->
                                    <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                        <li><a class="detailsblock-btn" data-modal-id="products"
                                               data-author="<?php echo $book_author; ?>"

                                               data-regularprice='<?php echo $_product->get_price_html();?>'

                                               data-shippinto='<?php echo $shipping_countiess;?>'
                                               data-stock="<?php echo $_product->get_total_stock(); ?>"
                                               data-sold="<?php echo  sprintf( __( ' %s', 'woocommerce' ), $units_sold ); ?> Sold"
                                               data-allimg="<?php  foreach( $attachment_ids as $attachment_id ){ echo '<img src='.wp_get_attachment_url( $attachment_id ).'>'; }  ?>"
                                               data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>"
                                               data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>"
                                               data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>"
                                               data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>"
                                               data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>"
                                               data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <?php
                        $bkri++;
                    }
                    else
                    {
                        if(!isset($privacyOption[0]))
                        {
                            //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

                                <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                    <div class="galleryOverlay" style="display:none;">

                                   <?php 
                                         $viddeoData = get_post_meta($postID, $key = '_product_image_gallery');
                                            foreach ($viddeoData as $row) {
                                               //echo $row;
                                                $data = explode(",", $row);
                                                foreach ($data as $row2) {
                                                    //echo $row2."<br>";
                                                  //  $imageRow = wp_get_attachment_url( $row2 );
												  $totatthum = count($data);
													if( $totatthum < 2 )
													{
													  $imageRow = 	 $imagesdcva;
													}else{
														
														 $imageRow = wp_get_attachment_url( $row2 );
													}
                                                    ?>

                                                    <img src="<?php echo $imageRow; ?>" style=" width:100%;height: 100%; top: 0px; display: block;">
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="galleryDots">
                                            <?php
                                            $i=1;
                                              $count_dot = count($data) > 5 ? 5 : count($data);
                                                while($i<=$count_dot)
                                              {
                                            ?>
                                              <span class="">•</span>
                                            <?php
                                                $i++;
                                              }
                                            ?>
                                         </div>
                                   </div>

                                    <div class="image-holder <?php echo $popup_poroduct_class; ?>">
									
									<?php if($_productvideofile != '')
										{ ?>
									     <video class="thevideo" loop preload="none" style="display:none;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/<?php echo $_productvideofile; ?>" type="video/mp4">  
                                           </video>
										<?php } ?>
										
                                        <a href="<?php echo get_permalink($postID); ?>">
                                            <!--  <div class="hover-item"></div>-->
                                        </a>

                                        <?php
										
                                        if($_productvideoimage != ''){
										
										  echo '<img src="'. $_productvideoimage .'" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                               
									       }else{
                                           if( has_post_thumbnail($postID) ){
											   
                                             echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
                                         }else{
                                            echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                                          }
									    }
										
										?>


                                    </div>
                                    <div class="layout-title-box text-center" style="display:block;">
                                        <h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
                                            <?php
                                            preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                                            if ( count($matches[0]) && count($matches[1]) >4){;
                                                ?>
                                                <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                <?php
                                            }
                                            else if(strlen($bkdata->post_title)>30){
                                                ?>
                                                <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                                                <?php
                                            }
                                            else {
                                                ?>
                                                <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                                            <?php } ?>
                                        </h6>
                                        <!-- <ul class="stats">
                                            <li ><?php videopress_displayviews( $postID ); ?></li>
                                            <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                            <li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>

                                        </ul>
                                        -->


                                        <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                            <li><a class="detailsblock-btn" data-modal-id="products"
                                                   data-author="<?php echo $book_author; ?>"

                                                   data-regularprice='<?php echo $_product->get_price_html();?>'

                                                   data-shippinto='<?php echo $shipping_countiess;?>'
                                                   data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                   data-sold="<?php echo  sprintf( __( ' %s', 'woocommerce' ), $units_sold ); ?> Sold"
                                                   data-allimg="<?php  foreach( $attachment_ids as $attachment_id ){ echo '<img src='.wp_get_attachment_url( $attachment_id ).'>'; }  ?>"
                                                   data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>"
                                                   data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>"
                                                   data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>"
                                                   data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>"
                                                   data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>"
                                                   data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <?php
                            $bkri++;
                        }
                    }
                }
                else
                {  // case where user is logged in
                    if($post_author == $user_ID)
                    {    // Case where logged in User is same as Video Author User
                        //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
                        ?>
                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

                            <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                <div class="galleryOverlay" style="display:none;">

                                   <?php 
                                         $viddeoData = get_post_meta($postID, $key = '_product_image_gallery');
                                            foreach ($viddeoData as $row) {
                                               //echo $row;
                                                $data = explode(",", $row);
                                                foreach ($data as $row2) {
                                                    //echo $row2."<br>";
                                                  //  $imageRow = wp_get_attachment_url( $row2 );
												  
												  $totatthum = count($data);
													if( $totatthum < 2 )
													{
														
													  $imageRow = 	 $imagesdcva;
													  
													}else{
														
														 $imageRow = wp_get_attachment_url( $row2 );
													}
													
                                                    ?>

                                                    <img src="<?php echo $imageRow; ?>" style=" width:100%;height: 100%; top: 0px; display: block;">
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="galleryDots">
                                            <?php
                                            $i=1;
                                              $count_dot = count($data) > 5 ? 5 : count($data);
                                                while($i<=$count_dot)
                                              {
                                            ?>
                                              <span class="">•</span>
                                            <?php
                                                $i++;
                                              }
                                            ?>
                                         </div>
                                   </div>

                                    <div class="image-holder <?php echo $popup_poroduct_class; ?>">
									
									 <?php if($_productvideofile != '')
										{ ?>
									     <video class="thevideo" loop preload="none" style="display:none;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/<?php echo $_productvideofile; ?>" type="video/mp4">  
                                           </video>
										<?php } ?>
										
										
                                    <a href="<?php echo get_permalink($postID); ?>">
                                        <!--  <div class="hover-item"></div>-->
                                    </a>

                                    <?php
									
									    if($_productvideoimage != ''){
										
										  echo '<img src="'. $_productvideoimage .'" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                               
									       }else{
                                           if( has_post_thumbnail($postID) ){
											   
                                             echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
                                         }else{
                                            echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                                          }
									    }
									
									?>


                                </div>
                                <div class="layout-title-box text-center" style="display:block;">
                                    <h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
                                        <?php
                                        preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                                        if ( count($matches[0]) && count($matches[1]) >4){;
                                            ?>
                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                            <?php
                                        }
                                        else if(strlen($bkdata->post_title)>30){
                                            ?>
                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                                            <?php
                                        }
                                        else {
                                            ?>
                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                                        <?php } ?>
                                    </h6>
                                    <!-- <ul class="stats">
                                            <li ><?php videopress_displayviews( $postID ); ?></li>
                                            <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                            <li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>

                                        </ul>
                                        -->

                                    <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                        <li><a class="detailsblock-btn" data-modal-id="products"
                                               data-author="<?php echo $book_author; ?>"

                                               data-regularprice='<?php echo $_product->get_price_html();?>'

                                               data-shippinto='<?php echo $shipping_countiess;?>'
                                               data-stock="<?php echo $_product->get_total_stock(); ?>"
                                               data-sold="<?php echo  sprintf( __( ' %s', 'woocommerce' ), $units_sold ); ?> Sold"
                                               data-allimg="<?php  foreach( $attachment_ids as $attachment_id ){ echo '<img src='.wp_get_attachment_url( $attachment_id ).'>'; } ?>"
                                               data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>"
                                               data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>"
                                               data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>"
                                               data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>"
                                               data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>"
                                               data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <?php
                        $bkri++;
                    }
                    else
                    {    // Case where logged in User is not same as Video Author User
                        if(isset($privacyOption[0]) and $privacyOption[0] == "public")
                        {
                            //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

                                <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

                                   <div class="galleryOverlay" style="display:none;">

                                   <?php 
                                         $viddeoData = get_post_meta($postID, $key = '_product_image_gallery');
                                            foreach ($viddeoData as $row) {
                                               //echo $row;
                                                $data = explode(",", $row);
                                                foreach ($data as $row2) {
                                                    //echo $row2."<br>";
                                                    //$imageRow = wp_get_attachment_url( $row2 );
													
													$totatthum = count($data);
													if( $totatthum < 2 )
													{
													  $imageRow = 	 $imagesdcva ;
													}else{
														
														 $imageRow = wp_get_attachment_url( $row2 );
													}
													
                                                    ?>

                                                    <img src="<?php echo $imageRow; ?>" style=" width:100%;height: 100%; top: 0px; display: block;">
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="galleryDots">
                                            <?php
                                            $i=1;
                                              $count_dot = count($data) > 5 ? 5 : count($data);
                                                while($i<=$count_dot)
                                              {
                                            ?>
                                              <span class="">•</span>
                                            <?php
                                                $i++;
                                              }
                                            ?>
                                         </div>
                                   </div>

                                    <div class="image-holder <?php echo $popup_poroduct_class; ?>">
									
									   <?php if($_productvideofile != '')
										{ ?>
									     <video class="thevideo" loop preload="none" style="display:none;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/<?php echo $_productvideofile; ?>" type="video/mp4">  
                                           </video>
										<?php } ?>
										
										
                                        <a href="<?php echo get_permalink($postID); ?>">
                                          <!--  <div class="hover-item"></div>-->
                                        </a>

                                        <?php
										
                                       if($_productvideoimage != ''){
										
										  echo '<img src="'. $_productvideoimage .'" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                               
									       }else{
                                           if( has_post_thumbnail($postID) ){
											   
                                             echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
                                         }else{
                                            echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                                          }
									    }
										
										?>


                                    </div>
                                    <div class="layout-title-box text-center" style="display:block;">
                                        <h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
                                            <?php
                                            preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                                            if ( count($matches[0]) && count($matches[1]) >4){;
                                                ?>
                                                <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                <?php
                                            }
                                            else if(strlen($bkdata->post_title)>30){
                                                ?>
                                                <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>
                                                <?php
                                            }
                                            else {
                                                ?>
                                                <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                                            <?php } ?>
                                        </h6>
                                        <!-- <ul class="stats">
                                            <li ><?php videopress_displayviews( $postID ); ?></li>
                                            <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                            <li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>

                                        </ul>
                                        -->
                                        <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                            <li><a class="detailsblock-btn" data-modal-id="products"
                                                   data-author="<?php echo $book_author; ?>"

                                                   data-regularprice='<?php echo $_product->get_price_html();?>'

                                                   data-shippinto='<?php echo $shipping_countiess;?>'
                                                   data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                   data-sold="<?php echo  sprintf( __( ' %s', 'woocommerce' ), $units_sold ); ?> Sold"
                                                   data-allimg="<?php foreach( $attachment_ids as $attachment_id ){ echo '<img src='.wp_get_attachment_url( $attachment_id ).'>'; } ?>"
                                                   data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>"
                                                   data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>"
                                                   data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>"
                                                   data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>"
                                                   data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>"
                                                   data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <?php
                            $bkri++;
                        }
                        else
                        {
                            if(!isset($privacyOption[0])) {
                                //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
                                ?>
                                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

                                    <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">
                                         
                                         <div class="galleryOverlay" style="display:none;">

                                   <?php 
                                         $viddeoData = get_post_meta($postID, $key = '_product_image_gallery');
                                            foreach ($viddeoData as $row) {
                                               //echo $row;
                                                $data = explode(",", $row);
                                                foreach ($data as $row2) {
                                                    //echo $row2."<br>";
                                                   // $imageRow = wp_get_attachment_url( $row2 );
												   $totatthum = count($data);
													if( $totatthum < 2 )
													{
													  $imageRow = 	 $imagesdcva ;
													}else{
														
														 $imageRow = wp_get_attachment_url( $row2 );
													}
													
                                                    ?>

                                                    <img src="<?php echo $imageRow; ?>" style=" width:100%;height: 100%; top: 0px; display: block;">
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="galleryDots">
                                            <?php
                                            $i=1;
                                              $count_dot = count($data) > 5 ? 5 : count($data);
                                                while($i<=$count_dot)
                                              {
                                            ?>
                                              <span class="">•</span>
                                            <?php
                                                $i++;
                                              }
                                            ?>
                                         </div>
                                   </div>

                                    <div class="image-holder <?php echo $popup_poroduct_class; ?>">

                                           <?php if($_productvideofile != '')
										{ ?>
									     <video class="thevideo" loop preload="none" style="display:none;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/<?php echo $_productvideofile; ?>" type="video/mp4">  
                                           </video>
										<?php } ?>
										
                                            <a href="<?php echo get_permalink($postID); ?>">
                                               <!--  <div class="hover-item"></div>-->
                                            </a>

                                            <?php
											
                                            if($_productvideoimage != ''){
										
										  echo '<img src="'. $_productvideoimage .'" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                               
									       }else{
                                             if( has_post_thumbnail($postID) ){
											   
                                             echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
                                              }else{
                                              echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                                            }
									      }
											
											?>


                                        </div>
                                        <div class="layout-title-box text-center" style="display:block;">
                                            <h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
                                                <?php
                                                preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                                                if ( count($matches[0]) && count($matches[1]) >4){;
                                                    ?>
                                                    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                    <?php
                                                }
                                                else if(strlen($bkdata->post_title)>30){
                                                    ?>
                                                    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                                                    <?php
                                                }
                                                else {
                                                    ?>
                                                    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                                                <?php } ?>
                                            </h6>
                                            <!-- <ul class="stats">
                                            <li ><?php videopress_displayviews( $postID ); ?></li>
                                            <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                            <li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>

                                        </ul>
                                        -->
                                            <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                                <li><a class="detailsblock-btn" data-modal-id="products"
                                                       data-author="<?php echo $book_author; ?>"

                                                       data-regularprice='<?php echo $_product->get_price_html();?>'

                                                       data-shippinto='<?php echo $shipping_countiess;?>'
                                                       data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                       data-sold="<?php echo  sprintf( __( ' %s', 'woocommerce' ), $units_sold ); ?> Sold"
                                                       data-allimg="<?php foreach( $attachment_ids as $attachment_id ){ echo '<img src='.wp_get_attachment_url( $attachment_id ).'>'; } ?>"
                                                       data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>"
                                                       data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>"
                                                       data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>"
                                                       data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>"
                                                       data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>"
                                                       data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <?php
                                $bkri++;
                            } else  {
                                if( is_array($selectUsersList) and count($selectUsersList) > 0 )
                                {   // case where user access list is available
                                    if( in_array($user_ID, $selectUsersList) )
                                    {
                                        //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
                                        ?>
                                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

                                            <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                                <div class="galleryOverlay" style="display:none;">

                                   <?php 
                                         $viddeoData = get_post_meta($postID, $key = '_product_image_gallery');
                                            foreach ($viddeoData as $row) {
                                               //echo $row;
                                                $data = explode(",", $row);
                                                foreach ($data as $row2) {
                                                    //echo $row2."<br>";
                                                   // $imageRow = wp_get_attachment_url( $row2 );
												   
												   $totatthum = count($data);
													if( $totatthum < 2 )
													{
													  $imageRow = 	 $imagesdcva ;
													}else{
														
														 $imageRow = wp_get_attachment_url( $row2 );
													}
													
                                                    ?>

                                                    <img src="<?php echo $imageRow; ?>" style=" width:100%;height: 100%; top: 0px; display: block;">
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="galleryDots">
                                            <?php
                                            $i=1;
                                              $count_dot = count($data) > 5 ? 5 : count($data);
                                                while($i<=$count_dot)
                                              {
                                            ?>
                                              <span class="">•</span>
                                            <?php
                                                $i++;
                                              }
                                            ?>
                                         </div>
                                   </div>

                                    <div class="image-holder <?php echo $popup_poroduct_class; ?>">
									
									        <?php if($_productvideofile != '')
										{ ?>
									     <video class="thevideo" loop preload="none" style="display:none;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/<?php echo $_productvideofile; ?>" type="video/mp4">  
                                           </video>
										<?php } ?>
										
                                                    <a href="<?php echo get_permalink($postID); ?>">
                                                       <!-- <div class="hover-item"></div>-->
                                                    </a>

                                                    <?php
													
													
                                                    if($_productvideoimage != ''){
										
										  echo '<img src="'. $_productvideoimage .'" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                               
									       }else{
                                             if( has_post_thumbnail($postID) ){
											   
                                             echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
                                              }else{
                                              echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:100%;">';
                                            }
									      }
													
													
													
													?>


                                                </div>
                                                <div class="layout-title-box text-center" style="display:block;">
                                                    <h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
                                                        <?php
                                                        preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                                                        if ( count($matches[0]) && count($matches[1]) >4){;
                                                            ?>
                                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                            <?php
                                                        }
                                                        else if(strlen($bkdata->post_title)>30){
                                                            ?>
                                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                                                            <?php
                                                        }
                                                        else {
                                                            ?>
                                                            <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                                                        <?php } ?>
                                                    </h6>
                                                    <!-- <ul class="stats">
                                            <li ><?php videopress_displayviews( $postID ); ?></li>
                                            <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                            <li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>

                                        </ul>
                                        -->
                                                    <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                                        <li><a class="detailsblock-btn" data-modal-id="products"
                                                               data-author="<?php echo $book_author; ?>"

                                                               data-regularprice='<?php echo $_product->get_price_html();?>'

                                                               data-shippinto='<?php echo $shipping_countiess;?>'
                                                               data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                               data-sold="<?php echo  sprintf( __( ' %s', 'woocommerce' ), $units_sold ); ?> Sold"
                                                               data-allimg="<?php  foreach( $attachment_ids as $attachment_id ){ echo '<img src='.wp_get_attachment_url( $attachment_id ).'>'; }  ?>"
                                                               data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>"
                                                               data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>"
                                                               data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>"
                                                               data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>"
                                                               data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>"
                                                               data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                        $bkri++;
                                    }
                                    else
                                    {  // case where user is not in access list

                                    }
                                }
                                else
                                {  }
                            }
                        }
                    }
                }
				
				} // end of country check
            }}?>



    </div>
    <div class="row" data-anchor="new-products">
        <div class="col-sm-12">
            <nav class="pull-right">

                <?php
				
                  $bktotal =  $count_total_productss;
				  
                if($cat=='new-products'){
                    $pcurrent=max( 1, get_query_var('page') );
                }
                else{
                    $pcurrent=1;
                }

                $paginate=paginate_links( array(
                    'format' => 'page/%#%/',
                    'current' => $pcurrent,
                    'type' => 'array',
                    'end_size' =>1,
                    'mid_size'=>4,
                    'total' => ($bktotal/12)
                ) );



                $pcount=count($paginate);


                $maxp=ceil(($bktotal/12)/2);

                if( $pcurrent<= ceil($bktotal / 12)  )
                {
                    if( ceil($bktotal / 12) <5 ) {
                        foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
                    }

                    else if( ceil($bktotal / 12) >= 5 ){


                        if ($pcurrent<=6)
                        {
                            if ($pcurrent>2)
                            {echo $paginate[0].'&nbsp;';}
                            if($paginate[$pcurrent-2]){
                                echo $paginate[$pcurrent-2].'&nbsp;';
                            }
                            echo $paginate[$pcurrent-1].'&nbsp;';
                            echo $paginate[$pcurrent].'&nbsp;';
                            if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
                            if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

                        }

                        else if ($pcurrent>=7)
                        {   echo $paginate[0].'&nbsp;';
                            echo $paginate[5].'&nbsp;';
                            echo $paginate[6].'&nbsp;';
                            echo $paginate[7].'&nbsp;';
                            if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
                            if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

                        }


                        if( $pcurrent <= (ceil($bktotal/12)-1) ){
                            echo $paginate[$pcount-1];
                        }

                    }

                }
                ?>
            </nav>
        </div>
    </div>
</div>
<div>
 <?php get_template_part('includes/hover_product'); ?>
</div>
