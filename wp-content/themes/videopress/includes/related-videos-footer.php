<h6 class="title-bar"><span>Related Videos</span></h6>

    <!-- Start Categorized post -->
    <div class="layout-3 related-videos">
    <?php
	// Set Variable
	$videopress_gridcounter = 0;
	$videopress_category = get_the_category(); 
	
	if( isset( $videopress_category[0]->term_taxonomy_id ) == '' ){
		// Set Default category to 1 if none
		$videopress_category_id = '1';
	}else{
		// else assign value to a category
		$videopress_category_id = $videopress_category[0]->term_taxonomy_id;
	}
	
	$args = array( 'numberposts' => 4, 'order'=> 'DESC', 'category'=> $videopress_category_id, 'exclude'=> $post->ID );
	$postslist = get_posts( $args );
	foreach($postslist as $post) :  setup_postdata($post);
	$videopress_gridcounter++;
	?>
    
    <div class="grid-one-fourth <?php if( $videopress_gridcounter == '1' ){ echo ' first'; }elseif( $videopress_gridcounter == '4' ){ echo ' last'; } ?>">
    
    	<div class="image-holder">
        <a href="<?php echo get_permalink(); ?>">
        <div class="hover-item"></div>
        </a>
        
    	<?php
		if( has_post_thumbnail() ){
        	the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
		}else{
			echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
		}
		?>
        
        </div>
    
    <ul class="stats">
    <li><?php videopress_displayviews( get_the_ID() ); ?></li>
    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
    </div>
    
    <?php endforeach; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    <div class="spacing-40"></div>