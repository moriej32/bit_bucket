<?php if( vp_metabox( 'playlist.enable_playlist' ) == '1' ){ // If Playlist is enabled on the page ?>

<!-- Playlist -->
<div class="full-width">
<?php
// Assign Meta Values to Variables
$videopress_videopress_playlist_category = vp_metabox('playlist.playlist_group.0.playlist_category');
$videopress_videopress_playlist_count = vp_metabox('playlist.playlist_group.0.playlist_count');
$videopress_playlist_type = vp_metabox('playlist.playlist_group.0.full_width');
?>



<!-- ============================================ Desktop View ============================================ -->
<div class="desktop-view">
	<div id="DesktopPlaylist"><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="loading" /></div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
jwplayer("DesktopPlaylist").setup({
	// Settings
	height: 390,
	<?php if($videopress_playlist_type != '1'){ ?>
		listbar: {
			position: 'right',
			size: 240,//220
			layout: 'extended',
		},
	<?php } ?>
    width: '100%',
	skin: '<?php videopress_playerskin(); ?>',
	<?php videopress_player_logo(); ?>
	
    playlist: [
		// Start Loop
		<?php
		// Set Args Parameters
		$args = array(
		'orderby'  => 'date',
		'order'    => 'DESC',
		'posts_per_page' => $videopress_videopress_playlist_count,
		'cat'	=> $videopress_videopress_playlist_category,
		);
		
		// Start Loop
		query_posts($args);
		while ( have_posts() ) : the_post();
		
		// Show only if video is self hoster(uploaded) or from YouTube
		if( vp_metabox('video_options.video_type') == 'upload' or vp_metabox('video_options.video_type') == 'youtube_url' ){
		
		// Video File
		if( vp_metabox('video_options.video_type') == 'upload' ){
			$videopress_video = vp_metabox('video_options.video_upload');
		}elseif( vp_metabox('video_options.video_type') == 'youtube_url' ){
			$videopress_video = vp_metabox('video_options.youtube_url');
		}
		
		// Thumbnail
		$videopress_videothumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?>
		
		{
			image: "<?php echo $videopress_videothumb; ?>",
			file: "<?php echo $videopress_video; ?>",
			title: "<?php the_title(); ?>",
			description: "<?php echo videopress_content('240'); ?>"
		},
		<?php
		} // End of Condition
		
		// Reset the variable
		unset($videopress_video); 
		unset($videopress_videothumb);
		
		endwhile; // End of Loop
		wp_reset_query(); // Reset Queries
		?>
		// End Loop
	]
});
});
</script>
<!-- ============================================ Desktop View ============================================ -->




<!-- ============================================ Responsive View ============================================ -->
<div class="responsive-view">
	<div id="MobilePlaylist"><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="loading" /></div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
jwplayer("MobilePlaylist").setup({
	// Settings
	height: 390,
		listbar: {
			position: 'bottom',
			size: 100,//220
			layout: 'basic',
		},
    width: '100%',
	skin: '<?php videopress_playerskin(); ?>',
	<?php videopress_player_logo(); ?>
	
    playlist: [
		// Start Loop
		<?php
		// Set Args Parameters
		$args = array(
		'orderby'  => 'date',
		'order'    => 'DESC',
		'posts_per_page' => $videopress_videopress_playlist_count,
		'cat'	=> $videopress_videopress_playlist_category,
		);
		
		// Start Loop
		query_posts($args);
		while ( have_posts() ) : the_post();
		
		// Show only if video is self hoster(uploaded) or from YouTube
		if( vp_metabox('video_options.video_type') == 'upload' || vp_metabox('video_options.video_type') == 'youtube_url' ){
		
		// Video File
		if( vp_metabox('video_options.video_type') == 'upload' ){
			$videopress_video = vp_metabox('video_options.video_upload');
		}elseif( vp_metabox('video_options.video_type') == 'youtube_url' ){
			$videopress_video = vp_metabox('video_options.youtube_url');
		}
		
		// Thumbnail
		$videopress_videothumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?>
		
		{
			image: "<?php echo $videopress_videothumb; ?>",
			file: "<?php echo $videopress_video; ?>",
			title: "<?php the_title(); ?>",
			description: "<?php echo videopress_content('240'); ?>"
		},
		<?php
		} // End of Condition
		
		// Reset the variable
		unset($videopress_video); 
		unset($videopress_videothumb);
		
		endwhile; // End of Loop
		wp_reset_query(); // Reset Queries
		?>
		// End Loop
	]
});
});
</script>
<!-- ============================================ Responsive View ============================================ -->



<div class="spacing-40"></div>
</div>
<!-- Playlist -->

<?php } // End If ?>