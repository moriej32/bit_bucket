<style>

.galleryOverlay {
    background-color: #fff;
    border: 1px solid #999;
    box-shadow: 0 0 7px #666;
    height: 170px;
    position: absolute;
    max-width: 91%;
    width: 250px;
    z-index: 99;
    transform: scale(1.1);
    z-index: 99;
}
.galleryImgCont {
   /* background-color: #fff;
    border: 0 solid #999;
    height: 200px;
    position: relative;
    width: 200px;*/
}

.galleryDots {
    bottom: 0;
    left: 0;
    position: absolute;
    text-align: center;
    width: 100%;
}
.galleryDots span {
    color: #999;
    font-size: 14pt;
    margin: 2px;
    text-decoration: none;
}
.galleryDots span.active {
    color: #333;
}
</style>

<script>
if (!window.matchMedia('(max-device-width: 480px)').matches) {
        jQuery(document).ready(function($){
          var mySlide;
          var currentImg;
          $(document).on('mouseenter', '.galleryImgCont', function(e) {
              if (mySlide) clearInterval(mySlide);
             // $(".galleryImgCont").hide();
              $(".galleryOverlay:visible").hide();
              var myOverlay = $(this).prev(".galleryOverlay");
              var myImages = myOverlay.find("img");
              myImages.hide().eq(0).show();
              var myDots = myOverlay.find(".galleryDots span");
              myDots.removeClass("active").eq(0).addClass("active");
              myOverlay.fadeIn("fast");
              var nbImages = myImages.length;
              currentImg = 0;
              if (nbImages>1){
                mySlide = setInterval(function(){
                  myImages.eq(currentImg).hide();
                  if (currentImg == nbImages -1){
                      currentImg = 0;
                  } else {
                      currentImg++;
                  }
                  myImages.eq(currentImg).show();
                  myDots.removeClass("active").eq(currentImg).addClass("active");
                }, 1500);
              }
            }
          );
		  
		 $(document).on('mouseleave', '.galleryOverlay', function(e) {
              $(this).hide();
              $(".galleryImgCont").show();
              clearInterval(mySlide);
              currentImg = 0;
            
        });
		
		 });
      }

</script>