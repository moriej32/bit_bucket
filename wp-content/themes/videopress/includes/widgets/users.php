<?php ob_start(); ?>

<?php

class videopress_users extends WP_Widget
{
  function videopress_users()
  {
    $widget_ops = array('classname' => 'videopress_users', 'description' => 'Displays a the login and registration form' );
    $this->WP_Widget('videopress_users', 'VideoPress: Users', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'widget_title' => '' ) );
	$widget_title = $instance['widget_title'];
?>

  <p><label for="<?php echo $this->get_field_id('widget_title'); ?>">Widget Title: <input class="widefat" id="<?php echo $this->get_field_id('widget_title'); ?>" name="<?php echo $this->get_field_name('widget_title'); ?>" type="text" value="<?php echo esc_attr($widget_title); ?>" /></label></p>
  
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
	$instance['widget_title'] = $new_instance['widget_title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
	$widget_title = empty($instance['widget_title']) ? ' ' : apply_filters('widget_title', $instance['widget_title']);
 
    if (!empty($imageurl))
      echo $before_title . $widget_title . $after_title;
 
    // WIDGET CODE GOES HERE
    videopress_users_widget();
 
    echo $after_widget;
  }
 
}

?>





<?php function videopress_users_widget(){ ?>
	 <?php   //echo $wpmobile;
	 if( wp_is_mobile () )
	 {
		 if( !is_user_logged_in()  ){
			 usertopsignuplogin();
		 }
	 }else{
		 if( is_user_logged_in() ) // If user is logged in
		 {
			 userdeshbordmenu();
		 }
		 else{
			usertopsignuplogin(); 
		 }
		 
	 }
}
	 ?>
<?php function usertopsignuplogin(){ ?> 

 <ul class='tab-menu'>
    <li class='tab log-in'><a href="#">Log-in <i class="fa fa-user"></i></a></li>
    <li class='tab signup-right'><a href="#">Sign-up <i class="fa fa-pencil"></i></a></li>
  </ul>
  
  <div id="tab-container" class="tab-container">
 
  
  <!-- Login Tab -->
  <div class="tab-content login" id="login">
  <form name="loginform" method="post" action="<?php echo site_url(); ?>/wp-login.php?redirect_to=<?php echo site_url(); ?>" class="user-forms">
  <input type="text" name="log" placeholder="Username" class="user-input">
  <input type="password" name="pwd" placeholder="password" class="user-input">
  <input type="submit" value="Sign-in" class="user-submit"> <br />
  <a href="<?php echo wp_lostpassword_url(); ?>">Forgot Password?</a>
  </form>
  </div>

  <!-- End Login Tab -->
  
  
  <!-- Signup Tab -->
  <div class="tab-content signup" id="signup" style="display:none;">
  <?php
  // When register button is triggered
  if(isset($_POST['trigger_register']) && !empty($_POST['trigger_register'])) {
  
  $user_name = $_POST['username'];
  $user_email = $_POST['email'];
  //$random_password = wp_generate_password( '12', 'true', 'true' );
    $random_password = wp_generate_password(12, false);
  
  // Check if username and email exist
  if ( !username_exists( $user_name ) and email_exists($user_email) == false ) {
  	
	// Check Fields
  	if( $user_name == '' ||  $user_email == '' ){
		
		echo '<div class="widget-register-error">Fields cannot be blank</div>';
		
	}else{
	
		// Check if valid email
		if( is_email($user_email) ){
			// Register into database
			$user_id = wp_create_user( $user_name, $random_password, $user_email );
			
			// Send password to email
			$subject = get_bloginfo('name')." Registration";
			$message = sprintf( __( 'Congratulations %s!', 'testmessage' ),  $user_name ) . "\n\n";
			$message .= sprintf( __( 'Username %s', 'testmessage' ),  $user_name ) . "\n\n";
			$message .= sprintf( __( 'Password is:  %s', 'testmessage' ),  $random_password ) . "\n\n";
			
			$header = 'from: Registration '.get_option('admin_email');
			//$send_contact=mail($user_email,$subject,$message,$header);
			$newaccountcreate  = new Affiliate_WP_Emails;
			$newaccountcreate->send( $user_email, $subject, $message );
			
			 update_user_meta($user_id, 'userstatusreg', 1);
			 
			  $usernewnowz = new WP_User($user_id);

             $usernewnowz->set_role('contributor');
			
			// Notify Success
			echo '<div class="widget-register-success">Registration Successful, Get Password from email address</div>';
			
		}else{ // If Email not valid
			echo '<div class="widget-register-error">Invalid Email</div>';
		} // End Email Validation
		
	} // End Checking of fields

	
  // Else username and email exist
  }else {
	
	echo '<ul class="widget-register-error">';
	
	// If Username Exist
	if( username_exists( $user_name ) != '' ){
		echo '<li>Username Already Exists, Choose another one</li>';
	} // End Username Exist
  	
	
	// If Email Exist
	if( email_exists($user_email) ){
		echo '<li>Email Already Registered, Please Choose another one</li>';
	} // End Email Exist
	
	echo '</ul>';
	
  } // End if check exist username and password
  
  }// End trigger
  ?>
  
  <form name="loginform" method="post" action="#signup" class="user-forms">
  <input type="text" name="username" placeholder="Username" class="user-input">
  <input type="text" name="email" placeholder="Email" class="user-input">
  <input type="submit" value="Register Account" class="user-submit" name="trigger_register">
  </form>
  </div>

  <!-- End Signin Tab -->
  
   </div>
   <!-- Start signup pop up -->
					<?php get_template_part('includes/sign-up-popup'); ?>
	<!-- End signup -->
<?php  } ?>
  <?php function userdeshbordmenu() {  // If user is logged in ?>
 
 
  
   <!-- Logged In -->
  <div class="widget-user-profile">
    <div class="user-images left">
      <?php global $current_user; ?>
      <?php
      $current_user = wp_get_current_user();
      $user_id = get_current_user_id();
//      $cover_meta = 'cover_pic_meta';
      $profile_meta = 'profile_pic_meta';
//      $cover_img = get_user_meta( $user_id, $cover_meta, true );
      $profile_img = get_user_meta( $user_id, $profile_meta, true );
      if($profile_img){
          $img2show = site_url() . '/profile_pic/' . $profile_img;
          echo "<img src={$img2show} height='35' width='35' / >";
      }else{
//          $img2show = "http://2.gravatar.com/avatar/" . md5($current_user->user_email) . "?s=14&d=mm&r=g";
          echo get_avatar( $current_user->ID, '35' );
      }
      ?>
	</div>
 <!--<div class="profile-header">

	  <p class="welcome">Welcome back, <strong><?php echo $current_user->user_login; ?></strong></p>
	  <div class="clear"></div>
	  <a href="<?php echo home_url(); ?>/your-profile/">Edit Profile</a>
	 </div> -->
    <div class="rights">
	<ul class="widget-profile-controls">
      <!--<li><a href="<?php echo get_author_posts_url( $current_user->ID ); ?>?activity=updateprofile"><i class="fa fa-pencil"></i> Update Account</a></li> -->
		<?php if( current_user_can( 'publish_posts' ) ) {

		  echo '<li><a href="'.home_url().'/upload-your-video/"><i class="fa fa-upload"></i> Upload Video</a></li>';  
		  echo '<li><a href="'.home_url().'/upload-your-file/"><i class="fa fa-upload"></i> Upload File</a></li>';
		  echo '<li><a href="'.home_url().'/upload-your-book/"><i class="fa fa-upload"></i> Upload Book</a></li>';
		  
		}?>
      <li><a href="<?php echo get_author_posts_url( $current_user->ID ); ?>"><i class="fa fa-user"></i> View Profile</a></li>
 <li><a onclick="return chat_menu_section();"><i class="fa fa-comments"></i> Chat</a></li>
      <li><a href="<?php echo get_author_posts_url( $current_user->ID ); ?>?noti=noti"><i class="fa fa-bell"></i> Notifications</a></li>
        <li><a href="<?php home_url() ?> /bit_bucket/dashboard/"><i class="fa fa-upload"></i> Store Dashboard</a></li>
        <!-- <li><a href="<?php home_url() ?> /bit_bucket/my-account/"><i class="fa fa-upload"></i> Store Settings</a></li> -->
		<li><a href="<?php home_url() ?> /bit_bucket/analytics/"><i class="fa fa-bar-chart-o"></i> Ad Analytics</a></li>
        <li><a href="<?php echo wp_logout_url( site_url() ); ?>"><i class="fa fa-sign-in"></i> Logout</a></li>
   </ul>
   </div>
  </div>
  
<script>
function chat_menu_section(){
	$(".mainChatBoxArea .close.on").trigger("click");
	$(".slicknav_nav").hide(0);
}
</script>
  <!-- End Logged In -->
  
  <?php }  ?>
   
 

<?php ob_end_clean(); ?>
