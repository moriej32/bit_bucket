<?php ob_start(); ?>

<?php

class videopress_PopularVideos extends WP_Widget
{
  function videopress_PopularVideos()
  {
    $widget_ops = array('classname' => 'videopress_PopularVideos', 'description' => 'Display the Most Popular Videos' );
    $this->WP_Widget('videopress_PopularVideos', 'VideoPress: Popular Videos', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'post_count' => '', 'widget_title' => '' ) );
    $post_count = $instance['post_count'];
	$widget_title = $instance['widget_title'];
?>

  <p><label for="<?php echo $this->get_field_id('widget_title'); ?>">Widget Title: <input class="widefat" id="<?php echo $this->get_field_id('widget_title'); ?>" name="<?php echo $this->get_field_name('widget_title'); ?>" type="text" value="<?php echo esc_attr($widget_title); ?>" /></label></p>

  <p><label for="<?php echo $this->get_field_id('post_count'); ?>">Number of Posts: <input class="widefat" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>" type="text" value="<?php echo esc_attr($post_count); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['post_count'] = $new_instance['post_count'];
	$instance['widget_title'] = $new_instance['widget_title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $post_count = empty($instance['post_count']) ? ' ' : apply_filters('post_count', $instance['post_count']);
	$widget_title = empty($instance['widget_title']) ? ' ' : apply_filters('widget_title', $instance['widget_title']);
 
    if (!empty($post_count))
      echo $before_title . $widget_title . $after_title;
 
    // WIDGET CODE GOES HERE
    videopress_popular_posts( 5 );//$post_count );
 
    echo $after_widget;
  }
 
}

?>


<?php function videopress_popular_posts( $postcount ){ ?>
<div class="layout-3 popular-videos">
<!--<ul class="widget-popular">-->
	<?php
	$postcounter_1 = 0;
    $args = array(
		'meta_key' => 'popularity_count',
		'orderby'  => 'meta_value_num',
		'order'    => 'date',
		'posts_per_page' => $postcount,
    );
    query_posts($args);
    while ( have_posts() ) : the_post(); 
    ?>
    <!--<li>-->
	<?php if ($postcounter_1 == 0){ ?>
	<div class="grid-one-fourth first">
	<?php }else{ ?>
	<div class="grid-one-fourth">
	<?php } ?>
     
          <!-- Image Holder -->
          <div class="image-holder">
          <a href="<?php echo get_permalink(); ?>"><?php if(has_post_thumbnail() ){
		  	 the_post_thumbnail('thumbnail', array( 'class' => 'post-thumbnail' ) );

		   }else{
		  	 echo '<img src="'.get_template_directory_uri().'/images/no-image-thumb.png" class="post-thumbnail">';

		   } ?>
           </a>
          </div>
          <!-- Image Holder -->
          
          <div class="popular-details">
          <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
              <ul class="stats">
			  <li><?php videopress_countviews( get_the_ID() ); ?></li>
              <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>            
              </ul>
          </div>
        
        <div class="clear"></div>
        <!--</li>--></div>
    
    <?php 
	$postcounter_1++;
	endwhile; wp_reset_query(); 
	?>
    <!--</ul>--></div>
    <?php } ?>

<?php ob_end_clean(); ?>