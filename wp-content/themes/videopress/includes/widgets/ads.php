<?php ob_start(); ?>

<?php

class videopress_sidebarads extends WP_Widget
{
  function videopress_sidebarads()
  {
    $widget_ops = array('classname' => 'videopress_sidebarads', 'description' => 'Display the Most Popular Videos' );
    $this->WP_Widget('videopress_sidebarads', 'VideoPress: Banner Ads', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'imageurl' => '', 'linktoads' => '', 'widget_title' => '' ) );
    $imageurl = $instance['imageurl'];
	$linktoads = $instance['linktoads'];
	$widget_title = $instance['widget_title'];
?>

  <p><label for="<?php echo $this->get_field_id('widget_title'); ?>">Widget Title: <input class="widefat" id="<?php echo $this->get_field_id('widget_title'); ?>" name="<?php echo $this->get_field_name('widget_title'); ?>" type="text" value="<?php echo esc_attr($widget_title); ?>" /></label></p>

  <p><label for="<?php echo $this->get_field_id('imageurl'); ?>">Image URL: <input class="widefat" id="<?php echo $this->get_field_id('imageurl'); ?>" name="<?php echo $this->get_field_name('imageurl'); ?>" type="text" value="<?php echo esc_attr($imageurl); ?>" /></label></p>
  
  <p><label for="<?php echo $this->get_field_id('linktoads'); ?>">Link To Ads: <input class="widefat" id="<?php echo $this->get_field_id('linktoads'); ?>" name="<?php echo $this->get_field_name('linktoads'); ?>" type="text" value="<?php echo esc_attr($linktoads); ?>" /></label></p>
  
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['imageurl'] = $new_instance['imageurl'];
	$instance['linktoads'] = $new_instance['linktoads'];
	$instance['widget_title'] = $new_instance['widget_title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $imageurl = empty($instance['imageurl']) ? ' ' : apply_filters('imageurl', $instance['imageurl']);
	$linktoads = empty($instance['linktoads']) ? ' ' : apply_filters('linktoads', $instance['linktoads']);
	$widget_title = empty($instance['widget_title']) ? ' ' : apply_filters('widget_title', $instance['widget_title']);
 
    if (!empty($imageurl))
      echo $before_title . $widget_title . $after_title;
 
    // WIDGET CODE GOES HERE
    videopress_show_ads( $imageurl, $linktoads );
 
    echo $after_widget;
  }
 
}

?>


<?php function videopress_show_ads( $imageurl, $linktoads ){ ?>
	<div class="widget-ads">
    <a href="<?php echo $linktoads; ?>"><img src="<?php echo $imageurl; ?>" alt="Widget Ads" /></a>
    </div>
<?php } ?>

<?php ob_end_clean(); ?>