<?php ob_start(); ?>

<?php

class videopress_LatestComments extends WP_Widget
{
  function videopress_LatestComments()
  {
    $widget_ops = array('classname' => 'videopress_LatestComments', 'description' => 'Display the Most Recent Comments' );
    $this->WP_Widget('videopress_LatestComments', 'VideoPress: Recent Comments', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'post_count' => '', 'widget_title' => '' ) );
    $post_count = $instance['post_count'];
	$widget_title = $instance['widget_title'];
?>

  <p><label for="<?php echo $this->get_field_id('widget_title'); ?>">Widget Title: <input class="widefat" id="<?php echo $this->get_field_id('widget_title'); ?>" name="<?php echo $this->get_field_name('widget_title'); ?>" type="text" value="<?php echo esc_attr($widget_title); ?>" /></label></p>

  <p><label for="<?php echo $this->get_field_id('post_count'); ?>">Number of Posts: <input class="widefat" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>" type="text" value="<?php echo esc_attr($post_count); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['post_count'] = $new_instance['post_count'];
	$instance['widget_title'] = $new_instance['widget_title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $post_count = empty($instance['post_count']) ? ' ' : apply_filters('post_count', $instance['post_count']);
	$widget_title = empty($instance['widget_title']) ? ' ' : apply_filters('widget_title', $instance['widget_title']);
 
    if (!empty($post_count))
      echo $before_title . $widget_title . $after_title;
 
    // WIDGET CODE GOES HERE
    videopress_latest_comments( $post_count );
 
    echo $after_widget;
  }
 
}

?>




<?php function videopress_latest_comments( $commentcount ){ // Create Function for Latest Comments ?>

<ul class="comment-widget">
<?php
// The Query
$comments_query = new WP_Comment_Query();
$comments = $comments_query->query( array('number' => $commentcount) );

// Comment Loop
if ( $comments ) {
	foreach ( $comments as $comment ) {
?>

<li>
    	<div class="comment-avatar"><?php echo get_avatar( $comment->comment_author_email, 50); ?></div>
        
        <div class="comment-text">
       <a href="<?php echo get_permalink( $comment->comment_post_ID ); ?>"><?php echo get_the_title( $comment->comment_post_ID ); ?></a>
        <p><?php echo implode(' ', array_slice(explode(' ', $comment->comment_content ), 0, 5)); ?></p>
        </div>
    <div class="clear"></div>
    </li>
    
<?php
} // End of foreach loop
	} else { 
		echo 'No comments found.';
} // End if
?>

</ul>

<?php } // End Of Function ?>

<?php ob_end_clean(); ?>