<div class="update-profile-form">


<?php

	// Set Author ID
	global $current_user;
	$author = get_user_by( 'login', get_query_var( 'author_name' ) );
	
	// Check Variables
	if( !empty($_POST['url']) || !empty($_POST['nickname']) || !empty($_POST['desc']) || !empty($_POST['gplus']) || !empty($_POST['twitter']) || !empty($_POST['facebook']) || !empty($_POST['instagram']) || !empty($_POST['linkedin']) || !empty($_POST['pinterest']) || !empty($_POST['youtube']) || !empty($_POST['email']) || !empty($_POST['password']) ){
		$url = $_POST['url'];
		$nickname = $_POST['nickname'];
		$desc = $_POST['desc'];
		$gplus = $_POST['gplus'];
		$twitter = $_POST['twitter'];
		$facebook = $_POST['facebook'];
		$instagram = $_POST['instagram'];
		$linkedin = $_POST['linkedin'];
		$pinterest = $_POST['pinterest'];
		$youtube = $_POST['youtube'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		
	}else{
		
		$nickname = '';
		$url = '';
		$desc = '';
		$gplus = '';
		$twitter = '';
		$facebook = '';
		$instagram = '';
		$linkedin = '';
		$pinterest = '';
		$youtube = '';
		$email = '';
		$password = get_the_author_meta('user_pass', $author->ID);
	} // End of checking
	
	$userdata = array(
		'ID' => $author->ID,
		'nickname' => $nickname,
		'description' => $desc,
		'user_url' => $url,
		'gplus' => $gplus,
		'twitter' => $twitter,
		'facebook' => $facebook,
		'instagram' => $instagram,
		'linkedin' => $linkedin,
		'pinterest' => $pinterest,
		'youtube' => $youtube,
		'user_email' => $email,
		);
		
	$userpassword_data = array(
		'ID' => $author->ID,
		'user_pass' => $password,
	);
		
	if(isset($_POST['trigger_update']) && !empty($_POST['trigger_update'])) {
    		
			wp_update_user( $userdata );
			
			if( $password == $_POST['r-password'] && !empty($password) ){
				wp_set_password( $password, $author->ID );
			}elseif( $password != $_POST['r-password'] ){
				echo '<div class="update-error">Error: Password Did not match</div>';
			}
		
	}
	
?>

	<?php
    // Security Check
	// If Logged in user trying to edit other profile will cause an error
	if( $author->ID == $current_user->ID ){
	?>
    
	<form method="post" action="?activity=updateprofile">
	<h2>About yourself</h2>
    <h6>Nickname</h6><br>
    <input type="text" name="nickname" class="user-input" value="<?php echo get_the_author_meta('nickname', $author->ID); ?>">
    <h6>Bio</h6><br>
    <label>
    <textarea class="user-input" name="desc"><?php echo get_the_author_meta('description', $author->ID); ?></textarea>
    </label>
    <div class="spacing-30"></div>
    
    <h6>Social Media</h6>
    <label><i class="fa fa-link"></i>
    <input type="text" name="url" class="user-input" value="<?php echo get_the_author_meta('url', $author->ID); ?>" placeholder="http://example.com">
    </label>
    
    <label>
    <i class="fa fa-google-plus"></i>
    <input type="text" name="gplus" class="user-input" value="<?php echo get_the_author_meta('gplus', $author->ID); ?>" placeholder="http://plus.google.com">
    </label>
    
    <label>
    <i class="fa fa-twitter"></i>
    <input type="text" name="twitter" class="user-input" value="<?php echo get_the_author_meta('twitter', $author->ID); ?>" placeholder="http://twitter.com">
    </label>
    
    <label>
    <i class="fa fa-facebook"></i>
    <input type="text" name="facebook" class="user-input" value="<?php echo get_the_author_meta('facebook', $author->ID); ?>" placeholder="http://facebook.com">
    </label>
    
    <label>
    <i class="fa fa-instagram"></i>
    <input type="text" name="instagram" class="user-input" value="<?php echo get_the_author_meta('instagram', $author->ID); ?>" placeholder="http://instagram.com">
    </label>
    
    <label>
    <i class="fa fa-linkedin"></i>
    <input type="text" name="linkedin" class="user-input" value="<?php echo get_the_author_meta('linkedin', $author->ID); ?>" placeholder="http://linkedin.com">
    </label>
    
    <label>
    <i class="fa fa-pinterest"></i>
    <input type="text" name="pinterest" class="user-input" value="<?php echo get_the_author_meta('pinterest', $author->ID); ?>" placeholder="http://pinterest.com">
    </label>
    
    <label>
    <i class="fa fa-youtube-play"></i>
    <input type="text" placeholder="http://youtube.com" name="youtube" class="user-input" value="<?php echo get_the_author_meta('youtube', $author->ID); ?>" >
    </label>
    
    <div class="spacing-30"></div>
    
    <h6>Account Settings</h6>
	<label>
    <i class="fa fa-envelope"></i>
    <input type="text" name="email" class="user-input" value="<?php echo get_the_author_meta('email', $author->ID); ?>">
    </label>
    
    <label>
    <i class="fa fa-lock"></i>
    <input type="password" name="password" class="user-input" placeholder="Password">
    </label>
    
    <label>
    <i class="fa fa-lock"></i>
    <input type="password" name="r-password" class="user-input" placeholder="Repeat Password">
    </label>
    
    <div class="spacing-20"></div>
    
    <input type="submit" value="Update" class="user-submit" name="trigger_update">
	</form>
    
    <?php }else{ // If security check not matched display a notice ?>
	
    	<h6>Error:</h6>
        <p>You are trying to attempt to edit someone else's profile</p>
    
    <?php } ?>
    
</div>