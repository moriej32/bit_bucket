<script>
var $ = jQuery.noConflict();


$(document).on('click', 'button.close', function(e) {
	//var $ = jQuery.noConflict();
	$('#signupright').fadeOut();
});
$(document).ready(function(){
	
	/*$('.signup-right').click(function(e){
						e.preventDefault();
						$('#signupright').fadeIn();
});

$('#loginformnew').on( "submit", function(e) { 
 e.preventDefault();
 var acceptall = false;
 var fname = $('#fname').val();
 var lname = $('#lname').val();
 var username_reg  = $('#username_reg').val();
 var password_reg  = $('#password_reg').val();
 var cpassword_reg = $('#cpassword_reg').val();
 var email_reg    = $('#email_reg').val();
 var address1     = $('#address1').val();
 var address2     = $('#address2').val();
 var country12    = $('#country12').val();
 
 var paypalemail_reg    = $('#paypalemail_reg').val();
 var citytown_reg = $('#citytown_reg').val();
 var zip_reg      = $('#zip_reg').val();
 var phone_reg    = $('#phone_reg').val();
 var storename_reg    = $('#storename_reg').val();
 var revnueshare_reg = $('#revnueshare_reg:checked').val();
 var tearmcodition_reg = $('#tearmcodition_reg:checked').val();
 var donations_reg = $('#donations_reg:checked').val();
   if(revnueshare_reg != 1)
   {
	   revnueshare_reg = 0;
   }
   
   if(donations_reg != 1)
   {
	   donations_reg = 0;
   }
   
   if(address2 == 'undefined')
   {
	   address2 = '';
   }
   //undefined
 //alert(tearmcodition_reg);
      if(tearmcodition_reg == 'on'){
		  $('.termerror').html('');
		  acceptall = true;
		 }else{
			 acceptall = false;
			$('.termerror').html('Required Accept Terms of Service');
		}
		
		var checkpassword = valid_password_new(password_reg, cpassword_reg);
		//alert(checkpassword);
		if(checkpassword == 1)
		{
			acceptall = true;
		}else{
			acceptall = false;
		}
		
		 if (grecaptcha.getResponse() == ""){
             acceptall = false;
			 $('.captcherror').html('Required Captcha');
          }
		
		if(acceptall == true)
		{
			console.log();
			 $("#overlayssss").fadeIn();
                            $.ajax({
									url: '<?php echo admin_url('admin-ajax.php'); ?>',
									type: "POST",
                                    dataType: "json",
									data: {
										action: 'register_new_account_wp',
									    'username_reg':username_reg,
										'email_reg':email_reg,
										'fname':fname,
										'lname':lname,
										'password_reg':password_reg,
										'address1':address1,
										'address2':address2,
										'country12':country12,
										'paypalemail_reg':paypalemail_reg,
										'citytown_reg':citytown_reg,
										'zip_reg':zip_reg,
										'phone_reg':phone_reg,
										'storename_reg':storename_reg,
										'revnueshare_reg':revnueshare_reg,
										'donations_reg':donations_reg
									},
									success : function(response){
										$("#overlayssss").fadeOut();
									var newaccount_success = response.newaccount_success;
									if(newaccount_success == 1){
										$('#loginformnew').fadeOut();
										$('#successdiv').fadeIn();
										//successdiv
										$('.notify_success').html(response.notify_success);
									}else{
											
								     alert(response.username_error);	
									 $('.usernameerror').html(response.username_error);
									 $('.email_error').html(response.email_error);
									 
									}
									
									//$('#loginformnew').fadeOut(); newaccount_success
									
									
								  }
								});
		}
						

});*/
});

function valid_password_new(password_reg, cpassword_reg){
	var correctvalue = 0;
	var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/; 
	
	if( password_reg != cpassword_reg ){
		  
		   correctvalue = 0;
			$('.passworderror').html('Password not same');
			
		 }else{
			 
			//$('.passworderror').html('');
		   if(password_reg.match(passw)){
			   correctvalue = 1;
			   $('.passworderror').html('');
		   }else{
			   correctvalue = 0;
			 $('.passworderror').html('Password 6 to 20 characters which contain at least one numeric digit, uppercase and lowercase letter');
  
		   }
		    
		  
		}
		
	return correctvalue;
}
		
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="modal-box" id="signupright" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog" style="max-width: 100% !important;">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 style="text-align: left;" class="modal-title" id="myModalLabel">Create New Account</h4>
										</div>
										<div class="modal-bodys">
										<div id="successdiv" style="display:none;">
										    <h1>Congratulations!</h1>
                                           <p class="notify_success"></p>
                                         </div> 
										 
										<form id="loginformnew" method="post">
										  <div class="form-group">
										     <div class="half-width firasthalf">
										     <label>First Name</label><br/>
											<input type="text" id="fname" name="fname" placeholder="First Name" class="form-control">
											</div>
											 <div class="half-width">
											 <label>Last Name</label><br/>
											<input type="text" id="lname" name="lname" placeholder="Last Name" class="form-control">
											</div>
										 </div>
										 
										 <div class="form-group">
										     <label>Username<abbr class="required" title="required">*</abbr></label><br/>
											<input type="text" id="username_reg" name="username" placeholder="Username" class="form-control" required>
											<p class="usernameerror"></p>
										 </div>
										 
                                          <div class="form-group">
										     <div class="half-width firasthalf">
										         <label>Password<abbr class="required" title="required">*</abbr></label><br/>
											     <input type="password" id="password_reg" name="password" placeholder="Password"class="form-control" required>
											  </div>
											 <div class="half-width">
											     <label>Confirm Password<abbr class="required" title="required">*</abbr></label><br/>
											     <input type="password" id="cpassword_reg" name="password" placeholder="Password"class="form-control" required>
											 </div>
											 <p class="passworderror"></p>
										  </div>
										  
										  <div class="form-group">
										     <label>Email<abbr class="required" title="required">*</abbr></label><br/>
											<input type="text" id="email_reg" name="email" placeholder="Email Address"class="form-control" required>
										      <p class="email_error"></p>
										  
										  </div>
										  
										  <div class="form-group">
										     <label>Address<abbr class="required" title="required">*</abbr></label><br/>
											<input type="text" id="address1" name="address1" placeholder="Address" class="form-control" required>
											<br/>
											<input type="text" id="address2"  name="address2" placeholder="Apartment, suite, unit etc. (optional)"class="form-control">
										
										  </div>
										  
										  <div class="form-group">
										     <div class="half-width firasthalf">
										     <label>Country</label><br/>
											 <select name="country12" id="country12" class="form-control">
											 <option value="">Select a country…</option><option value="AX">Åland Islands</option>
											 <option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option>
											 <option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option>
											 <option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option>
											 <option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="PW">Belau</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option>
											 <option value="BQ">Bonaire, Saint Eustatius and Saba</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="VG">British Virgin Islands</option><option value="BN">Brunei</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo (Brazzaville)</option><option value="CD">Congo (Kinshasa)</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CW">CuraÇao</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and McDonald Islands</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran</option><option value="IQ">Iraq</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="CI">Ivory Coast</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JE">Jersey</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Laos</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao S.A.R., China</option><option value="MK">Macedonia</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia</option><option value="MD">Moldova</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="AN">Netherlands Antilles</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="KP">North Korea</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PS">Palestinian Territory</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="QA">Qatar</option><option value="IE">Republic of Ireland</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russia</option><option value="RW">Rwanda</option>
											 <option value="ST">São Tomé and Príncipe</option>
											 <option value="BL">Saint Barthélemy</option><option value="SH">Saint Helena</option>
											 <option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option>
											 <option value="SX">Saint Martin (Dutch part)</option><option value="MF">Saint Martin (French part)</option>
											 <option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option>
											 <option value="SM">San Marino</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option>
											 <option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option>
											 <option value="SG">Singapore</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option>
											 <option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option>
											 <option value="GS">South Georgia/Sandwich Islands</option><option value="KR">South Korea</option><option value="SS">South Sudan</option>
											 <option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option>
											 <option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option>
											 <option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syria</option><option value="TW">Taiwan</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom (UK)</option><option value="US">United States (US)</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VA">Vatican</option><option value="VE">Venezuela</option><option value="VN">Vietnam</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="WS">Western Samoa</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select>
											 </div>
											 <div class="half-width">
											  <label>Town / City</label><br/>
											<input type="text" name="citytown" id="citytown_reg" placeholder="Town / City"class="form-control">	
											</div>
										  </div>
										  
										  <div class="form-group">
										     <div class="half-width firasthalf">
										     <label>Postcode / ZIP</label><br/>
											<input type="text" name="zip" id="zip_reg" placeholder="Postcode / ZIP"class="form-control">
											</div>
											 <div class="half-width">
											 <label>Phone</label><br/>
											  <input type="number" name="phone" id="phone_reg" placeholder="Phone number"class="form-control">
											  </div>
										  </div>
										  
										  <div class="form-group">
										     <label>Store Name<abbr class="required" title="required">*</abbr></label><br/>
											<input type="text" id="storename_reg" name="storename" placeholder="Store Name"class="form-control" required>
										  </div>
										  
                                          <div class="form-group">
										     <label>Paypal Email<abbr class="required" title="required">*</abbr></label><br/>
											<input type="text" id="paypalemail_reg" name="paypalemail" placeholder="Paypal Email"class="form-control" required>
											 <p class="tips"><a href="https://www.paypal.com/us/home">What is Paypal</a></p>
										  </div><br/>
										  <div class="form-group">
										  
										  <div class="g-recaptcha" name="recaptcha" id="recaptcha"   data-sitekey="6LeQlS8UAAAAAARkp3gtqjiGvjOegleyMzKaFnGC" required></div>
										  
										   <p class="captcherror"></p>
										  </div>
                                          <div class="checkbox">
                                             <label><input id="revnueshare_reg" value="1" type="checkbox"> Do you want to earn money from Advertisments? </label><br/>
											 <label><input id="donations_reg" value="1" type="checkbox"> Do you want to earn money through Donations? </label><br/>
											 <label><input id="tearmcodition_reg" type="checkbox"> I accept terms of Service. <a href="http://www.doityourselfnation.org/bit_bucket/terms-of-service/">View Term and Condition Here</a></label>
                                               <p class="termerror"></p>
										   </div>
										   
											<input type="submit"  value="Register Account" class="btn btn-success">
										</form>
											
										</div>
									</div>
								</div>
<div id="overlayssss" class="overlay" style="display:none;">
 
  <div class="overlay-content">
     <h2><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></h2>
  </div>
</div>	
</div>
<style >
#signupright .modal-bodys {
    padding: 2em 2em;
	text-align: left;
}
#signupright .half-width{width:49%;float:left;}
#signupright .firasthalf{margin-right:10px;}
#signupright label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;
    margin-top: 10px;
}
#signupright .form-group{
	margin-bottom: 0px;
}
#signupright .btn-success{
    color: #fff;
    background-color: #3498db;
    border-color: #3498db;
}
#signupright .btn-success:hover {
    color: #fff;
    background-color: #21648F;
    border-color: #21648F;
}
#signupright abbr.required {
    color: red;
}
#signupright .email_error, #signupright .usernameerror, #signupright .passworderror, #signupright .termerror, #signupright .captcherror{ 
    font-size: 10px;
    color: red;
    font-weight: bold;
}
#signupright .tips{
	font-size: 10px;
    color: #000;
    font-weight: bold;
	}
#signupright #successdiv p{
padding-bottom: 20px;
    color: #000;
	}
#signupright #successdiv h1{
     color: #3498db !important;
    text-align: center !important;
    margin-top: 1px !important;
    margin-bottom: 10px !important;
    margin-bottom: 26px !important;

	}
@media only screen and (max-width: 768px){
#signupright .half-width{width:100%;float:left;}
#signupright .firasthalf{margin-right:0px;}
}

#signupright .overlay {
    height: 100%;
   
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.2);
    overflow-x: hidden;
    transition: 0.5s;
}

#signupright .overlay-content {
    position: relative;
    top: 25%;
   
    text-align: center;
    margin-top: 30px;
    border-radius: 5px;
    padding: 30px;
    background-color: #fff;
    margin: 0 auto;
    width: 50%;
	color: #337ab7;
    opacity: 1;

}
#signupright .overlay-content h2{color: #337ab7;}	
</style>
