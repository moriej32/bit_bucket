<ul class="sharer">

<!--<li>

<div class="dropdown">
    <button type="button" class="dropdown-toggle black" data-toggle="dropdown" >
      <i class="fa fa-ellipsis-v"></i> 
    </button>
    <ul class="dropdown-menu">
		<li><a class="black" id="addflag-anchor"><i class="fa fa-flag"></i> Mark as Flag</a></li>
    </ul>
</div>

</li>

<li><a class="facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>&t=<?php the_title(); ?>"><i class="fa fa-facebook"></i><span>Facebook</span></a></li>

<li><a class="twitter" target="_blank" href="http://twitter.com/home?status=<?php the_title(); ?>&nbsp;<?php echo get_permalink(); ?>"><i class="fa fa-twitter"></i><span>Twitter</span></a></li>

<li><a class="google" target="_blank" href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>"><i class="fa fa-google-plus"></i><span>Google Plus</span></a></li>

-->

</ul>
<div class="clear"></div>

<!-- Modal -->
<div class="modal-box" id="model_flag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-bodys">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="url" value="<?= get_permalink();?>">
					<input type="hidden" name="postidflag" id="postidflag" value="<?php get_the_id();?>">
					<textarea rows="10" id="reason" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>

<script>
	var $ = jQuery.noConflict();
	
	/* $(document).ready(function(){
		CKEDITOR.inline( 'reason' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';    
			config.fontSize_defaultLabel = '44px';
		};
	}); */
	
	$('#addflag-submit').live('click',function(e){
		//$('#addflag-form').submit();
		//$("form").serialize()
		var $ = jQuery.noConflict();
		
		var reason = $('#reason').val();
		var url = $('#url').val();
		var postidflag = $('#postidflag').val();
		
		$.ajax({
			type: "POST",  
			url: "<?php echo admin_url('admin-ajax.php'); ?>",  
			data: {action:'user_mark_flag',reason:reason,url:url,post_id_flag:postidflag},
			success : function(data){
				var modelhtml = $('#model_flag .modal-content').html();
				$('#model_flag .modal-content').html(data);
				
				setTimeout(function(){
					$('#model_flag').modal('hide');
					$('#model_flag .modal-content').html(modelhtml);
				},3000)
			}
		});
	});
	
	$('#addflag-anchor').click(function(e){
		var $ = jQuery.noConflict();
		$('#model_flag').modal();
	});
</script>