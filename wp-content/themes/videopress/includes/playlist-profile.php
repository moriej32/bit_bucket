 <?php 
 global $wpdb;
 $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$profile_id = $author->ID;

?>
 <div class="modal-box" id="myModalplaylist">
					<div class="modal-body">
								<a class="js-modal-close close">×</a>
						 <form class="form-inline" id="playlist-form" method="post"  enctype="multipart/form-data">
			     <div class="form-group playlistform">
					<label for="playlist_type">Type:</label>
					<select class="form-control" name="playlist_type" required>
					  <option value="0">Select Any One</option>
					  <option value="1">Videos</option>
					  <option value="2">Files</option>
					  <option value="3">Books</option>
					  <option value="4">Products</option>
					</select>

				</div>
				<div class="form-group playlistform">
					<label for="playlist_title">Title:</label>
					<input class="form-control" type="text" name="playlist_title" class="playlist_title" class="form-control" id="playlist_title" placeholder="title" required>
				</div>
				<div class="form-group playlistform">
				<label for="playlist_title">Thumbnail:</label>
				 <br/>
				 <div class="checkbox">
                     <label>Default
                     <input type="radio" name="dyn_playlist" class="playlist_cus_th" value="playlist_no" checked="checked" ></label>
                     <label>Custom Thumbnail
                     <input type="radio" name="dyn_playlist" class="playlist_cus_th" value="playlist_new" ></label>
                  </div>
				<div class="form-group playlistform">  
				  <div id="playlistcusth"></div>
				</div>
                </div>
				<button type="submit" name="submit" class="btn btn-success" id="palylistform-submit">Submit</button>
			</form>		
								
				     </div>
					 <style>
					   .playlistform{width:100%; margin-bottom: 8px !important;}
					   
					   #playlist-form .form-control{width:100%}
					   
					   .bottom-detailsul li .del-playlist {
    background: #C9302C;
    color: #fff;
    padding: 3px 14px;
    border-radius: 2px;
    display: block;
    line-height: normal;
    margin-top: 0px;
    text-transform: capitalize !important;
}
					 </style>
			 </div>
			<!-- Video playlist -->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Videos</label>
					</div>
				</div>
			</div>
			<?php
			$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 1  ORDER BY id DESC");
			?>
            <?php  if ( wp_is_mobile() ) {
                    /* Display and echo mobile specific stuff here */
					  $mobi = "auto";
                 }
				 else{
					 $mobi = "23.794%";
				 }

				 ?>
			<?php if($data): ?>
				<div class="playlistdivs">

					<?php foreach($data as $single_data): ?>
						<div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-430 grid-group-item" id="lay-<?= $single_data->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$playlistsongid = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data->id);

								?>

								  <?php  $thumtype = $single_data->thumnitype; if($playlistsongid[0]->c != 0 & $thumtype == 0) {  ?>
							        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($playlistsongid[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php  } if($thumtype == 1) {  ?>
								  <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data->customthum; ?>" class="playlistview" width="100%" >

								<?php } if($playlistsongid[0]->c == 0 & $thumtype == 0) { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php } ?>
                                  
								   <?php
								$playlist_playliayer_id = ($playlistsongid[0]->c)?$single_data->id:false;
								$encode_playlist_playliayer_id = str_replace("=",'',base64_encode($playlist_playliayer_id));
								//echo base64_decode($t);
								?>
								  
					              <div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								    <?php if($playlistsongid[0]->c == 0) { ?>
									<h6 class="layout-title" style="min-height: 29px;margin-top: 21px;max-height: 29px;overflow: hidden;"><?php echo $single_data->title; ?></h6>
								   <?php } else { ?>
									<h6 class="layout-title" style="min-height: 29px;margin-top: 21px;max-height: 29px;overflow: hidden;"><a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $encode_playlist_playliayer_id; ?>&ih=<?php echo $playlistsongid[0]->song_id; ?>&ptype=videos" title="<?php echo $single_data->title; ?>"><?php echo $single_data->title; ?></a></h6>
								   <?php } ?>
									<ul class="bottom-detailsul">
										<?php if($profile_id == get_current_user_id()){ ?>
										<li><a href="#" class="del-playlist" data-id="<?= $single_data->id; ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
										<?php } ?>
										<li><a class="detailsblock-btn" data-modal-id="myModal-<?= $single_data->id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
										<?php if($profile_id == get_current_user_id()){ ?>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?= $single_data->id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
								
									</ul>
								</div>

 <?php if($playlistsongid[0]->c != 0) { 
  echo privacy_user_select_from_playlist($single_data->id);
 } ?>
								<div class="modal-box" id="myModal-<?= $single_data->id; ?>">
								<div class="modal-body">
								<a class="js-modal-close close">×</a>
								<div class="layout-2-details layout-2-details-<?php $single_data->id; ?>">
								<div class="image-holder">
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $playlistsongid[0]->song_id; ?>&ptype=videos">
										<div class="hover-item"></div>
									</a>
									
									  <?php  $thumtype = $single_data->thumnitype; if($playlistsongid[0]->c != 0 & $thumtype == 0) {  ?>
							        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($playlistsongid[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php  } if($thumtype == 1) {  ?>
								  <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data->customthum; ?>" class="playlistview" width="100%" >

								<?php } if($playlistsongid[0]->c == 0 & $thumtype == 0) { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php } ?>
								

								</div>
								 <?php if($playlistsongid[0]->c == 0) { ?>
									<h6 class="layout-title"><?php echo $single_data->title; ?></h6>
								   <?php } else { ?>
									<h6 class="layout-title"><a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $encode_playlist_playliayer_id; ?>&ih=<?php echo $playlistsongid[0]->song_id; ?>&ptype=videos" title="<?php echo $single_data->title; ?>"><?php echo $single_data->title; ?></a></h6>
								   <?php } ?>
								<?php echo apply_filters( 'dyn_display_review', $output, $single_data->id, "post" ); ?>
								<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
								<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta($single_data->id,'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
								<ul class="stats">
									<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
									<li><?php videopress_countviews( $single_data->id ); ?></li>
									<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_data->id);
										echo count($result); ?> Endorsments</li>
									<!--favourite section -->
									<li><i class="fa fa-heart"></i>
									<?php
									  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_data->id;
									  $result1 = $wpdb->get_results($sql_aux);
									  echo count($result1);
									?> Favorites</li>
									<!-- end favorite -->
									<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', $single_data->id, "post"); ?></li>
									<li><?php comments_number(); ?></li>
								</ul>
								<div class="clear"></div>
								<p><?php echo videopress_content('240'); ?></p>
								<?php if($profile_id == get_current_user_id()){ ?>
								<p><a href="#" class="del-playlist" data-id="<?= $single_data->id; ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
								<?php } ?>
							</div></div></div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
			<!-- End playlist -->
			<br>
			<br>
			<!-- File playlist -->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Files</label>
					</div>
				</div>
				<!--<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>-->
			</div>
			<?php
			$datafile = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 2  ORDER BY id DESC");
			?>

			<?php if($datafile): ?>
				<div class="playlistdivs">

					<?php foreach($datafile as $single_data_file): ?>
						<div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-430 grid-group-item" id="lay-<?= $single_data_file->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$data_file = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data_file->id);

								?>



								<?php $output = ''; ?>
				                <!--<?php echo apply_filters( 'dyn_file_image', $output, $data_file[0]->song_id ); ?>

								<p><?php echo $data_file[0]->c;?> files
									<?php if($profile_id == get_current_user_id()){ ?>
									<span class="pull-right">
										<a href="#" title="Delete Playlist" data-id="<?= $single_data_file->id;?>" class="button_p_d"><i class="fa fa-trash-o"></i></a>
									</span>
									<?php } ?>
								</p>
								<p>
								<?php
								$ts = ($data_file[0]->c)?$single_data_file->id:false;
								$t = str_replace("=",'',base64_encode($ts));
								//echo base64_decode($t);
								?>
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_file[0]->song_id; ?>&ptype=files" class="playlist-url" data-playlist="<?= ($data_file[0]->c)?$single_data_file->id:false;?>" >
										<?php echo $single_data_file->title; ?>
									</a>
								</p>
								<p>updated : <?php echo dateDiff($single_data_file->updated); ?></p>-->
								
								
                                <?php
								 
								 $typethum = $single_data_file->thumnitype;
								 if( $typethum == 0){
								
                                 $uploaded_imgssss = get_post_meta( $data_file[0]->song_id, 'dyn_thumbaniimg_src', true );
								 
								if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  
								  
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 <a href="<?php echo get_permalink($postID); ?>">
								 <div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>
								 
								   </a> 
								  </div>
								  
								  <?php } else { ?>
								<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', $data_file[0]->song_id); ?>">
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_file[0]->song_id; ?>&ptype=files">
										<div class="hover-item dyn-file">
					                        <i class="fa fa-download"></i>
					                    </div>
									</a>
								</div>
								  <?php }
                                    
									} 
									if( $typethum == 1){
								  ?>
								   <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data_file->customthum; ?>" class="playlistview" width="100%" >
									<?php } ?>
								  
								<div class="layout-title-box layout-title-box-<?php echo $data_file[0]->song_id; ?>" style="display:block;">
									 <?php if($data_file[0]->c == 0) { ?>
									<h6 class="layout-title" style="min-height: 29px;margin-top: 21px;max-height: 29px;overflow: hidden;"><?php echo ctpreview( $content = $single_data_file->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></h6>
									 <?php } else { ?>
									 <h6 class="layout-title" style="min-height: 29px;margin-top: 21px;max-height: 29px;overflow: hidden;"><a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_file[0]->song_id; ?>&ptype=files" title="<?php echo $single_data_file->title; ?>"><?php echo ctpreview( $content = $single_data_file->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								
									 <?php } ?>
									<ul class="bottom-detailsul">
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><p><a href="#" class="del-playlist" data-id="<?= $single_data_file->id; ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
									<?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?= $data_file[0]->song_id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?= $single_data_file->id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
									</ul>
								</div>

							</div>
						</div>

						<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>


		
		
 <?php if($data_file[0]->c != 0) { 
  echo privacy_user_select_from_playlist($single_data_file->id);
 } ?>
           <?php if($data_file[0]->c != 0) { ?>
			<div class="modal-box" id="myModal-<?=$data_file[0]->song_id; ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?=$data_file[0]->song_id; ?>">
			
				<?php
								 
								 $typethum = $single_data_file->thumnitype;
								 if( $typethum == 0){
								
                                 $uploaded_imgssss = get_post_meta( $data_file[0]->song_id, 'dyn_thumbaniimg_src', true );
								 
								if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  
								  
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 <a href="<?php echo get_permalink($postID); ?>">
								 <div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>
								 
								   </a> 
								  </div>
								  
								  <?php } else { ?>
								<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', $data_file[0]->song_id); ?>">
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_file[0]->song_id; ?>&ptype=files">
										<div class="hover-item dyn-file">
					                        <i class="fa fa-download"></i>
					                    </div>
									</a>
								</div>
								  <?php }
                                    
									} 
									if( $typethum == 1){
								  ?>
								   <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data_file->customthum; ?>" class="playlistview" width="100%" >
									<?php } ?>
									
					
					
				 <?php if($data_file[0]->c == 0) { ?>
				<h6 class="layout-title"><?php echo $single_data_file->title; ?></h6>
				 <?php } else { ?>
				 <h6 class="layout-title"><a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_file[0]->song_id; ?>&ptype=files" title="<?php echo $single_data_file->title; ?>"><?php echo $single_data_file->title; ?></a></h6>
				
				 <?php } ?>
				<?php echo apply_filters( 'dyn_display_review', $output, $data_file[0]->song_id, "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta($data_file[0]->song_id,'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( $data_file[0]->song_id ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $data_file[0]->song_id);
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $data_file[0]->song_id;
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', $data_file[0]->song_id, "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if(get_the_author_id() == get_current_user_id()){ ?>
					<!--<p><a href="#" class="del-video" data-id="<?=$data_file[0]->song_id; ?>"><i class="fa fa-trash-o"></i> Delete</a></p>-->
					<?php } ?>


			</div></div></div><?php } ?>

					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
			<!-- End File playlist -->
			<br>
			<br>
			<!-- Book playlist -->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Books</label>
					</div>
				</div>
				 
			</div>
			<?php
			$databook = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 3  ORDER BY id DESC");
			?>

			<?php if($databook): ?>
				<div class="playlistdivs">

					<?php foreach($databook as $single_data_book): ?>
						<div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-430 grid-group-item" id="lay-<?= $single_data_book->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$data_book = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data_book->id);

								?>

								  <?php  $thumtype = $single_data_book->thumnitype; if($data_book[0]->c != 0 & $thumtype == 0) {  ?>
							        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($data_book[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php  } if($thumtype == 1) {  ?>
								  <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data_book->customthum; ?>" class="playlistview" width="100%" >

								<?php } if($data_book[0]->c == 0 & $thumtype == 0) { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php } ?>
								
								 
								<?php
								$ts = ($data_book[0]->c)?$single_data_book->id:false;
								$t = str_replace("=",'',base64_encode($ts));
								//echo base64_decode($t);
								?>
								

								<div class="layout-title-box layout-title-box-<?php echo $single_data_book->id; ?>" style="display:block;">
									<?php if($data_book[0]->c != 0) { ?>
									<h6 class="layout-title" style="min-height: 29px;margin-top: 21px;max-height: 29px;overflow: hidden;" ><a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_book[0]->song_id; ?>&ptype=books" title="<?php echo $single_data_book->title; ?>"><?php echo ctpreview( $content = $single_data_book->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
									<?php } else { ?>
									<h6 class="layout-title" style="min-height: 29px;margin-top: 21px;max-height: 29px;overflow: hidden;" ><?php echo ctpreview( $content = $single_data_book->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></h6>
								    <?php } ?>
									<ul class="bottom-detailsul">
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><p><a href="#" class="del-playlist" data-id="<?= $single_data_book->id; ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
									<?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?= $data_book[0]->song_id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?= $single_data_book->id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
									</ul>
								</div> 
							</div>
							
	 <?php if($data_book[0]->c != 0) { 
             echo privacy_user_select_from_playlist($single_data_book->id);
             } ?>	
             <?php if($data_book[0]->c != 0) { ?>
			<div class="modal-box" id="myModal-<?=$data_book[0]->song_id; ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?=$data_book[0]->song_id; ?>">
				<div class="image-holder">
				
					<?php  $thumtype = $single_data_book->thumnitype; if($data_book[0]->c != 0 & $thumtype == 0) {  ?>
							        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($data_book[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php  } if($thumtype == 1) {  ?>
								  <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data_book->customthum; ?>" class="playlistview" width="100%" >

								<?php } if($data_book[0]->c == 0 & $thumtype == 0) { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php } ?>
								
					</div>
				 <?php if($data_book[0]->c == 0) { ?>
				<h6 class="layout-title"><?php echo $single_data_book->title; ?></h6>
				 <?php } else { ?>
				 <h6 class="layout-title"><a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_book[0]->song_id; ?>&ptype=files" title="<?php echo $single_data_file->title; ?>"><?php echo $single_data_file->title; ?></a></h6>
				
				 <?php } ?>
				<?php echo apply_filters( 'dyn_display_review', $output, $data_book[0]->song_id, "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta($data_book[0]->song_id,'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( $data_book[0]->song_id ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $data_book[0]->song_id);
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $data_book[0]->song_id;
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', $data_book[0]->song_id, "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if(get_the_author_id() == get_current_user_id()){ ?>
					<!--<p><a href="#" class="del-video" data-id="<?=$data_file[0]->song_id; ?>"><i class="fa fa-trash-o"></i> Delete</a></p>-->
					<?php } ?>


			</div></div></div><?php } ?>			 
						</div>
						
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
			<!-- End Book playlist -->
            <!-- Start Products Playlist-->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Products</label>
					</div>
				</div>
				 
			</div>
			<?php
			$product_playlist_query = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 4  ORDER BY id DESC");
			?>

			<?php if($product_playlist_query): ?>
				<div class="playlistdivs">

					<?php foreach($product_playlist_query as $single_data_product_playlist): ?>
						<div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-430 grid-group-item" id="lay-<?= $single_data_product_playlist->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$data_product_playlist = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data_product_playlist->id);

								?>

								
								  <?php  $thumtype = $single_data_product_playlist->thumnitype; if($data_product_playlist[0]->c != 0 & $thumtype == 0) {  ?>
							        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($data_product_playlist[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php  } if($thumtype == 1) {  ?>
								  <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data_product_playlist->customthum; ?>" class="playlistview" width="100%" >

								<?php } if($data_product_playlist[0]->c == 0 & $thumtype == 0) { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php } ?>

								 
								<?php
								$ts = ($data_product_playlist[0]->c)?$single_data_product_playlist->id:false;
								$t = str_replace("=",'',base64_encode($ts));
								//echo base64_decode($t);
								?>
								

								<div class="layout-title-box layout-title-box-<?php echo $single_data_product_playlist->id; ?>" style="display:block;">
									<?php if($data_product_playlist[0]->c != 0) { ?>
									<h6 class="layout-title"  style="min-height: 29px;margin-top: 21px; max-height: 29px;overflow: hidden;" ><a href="<?php echo get_permalink($data_product_playlist[0]->song_id); ?>?opt=<?php echo $t; ?>&ih=playlists&ptype=products" title="<?php echo $single_data_product_playlist->title; ?>"><?php echo ctpreview( $content = $single_data_product_playlist->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								<?php } else { ?>
									<h6 class="layout-title" style="min-height: 29px;margin-top: 21px; max-height: 29px;overflow: hidden;" ><?php echo ctpreview( $content = $single_data_product_playlist->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></h6>
								    <?php } ?>
									<ul class="bottom-detailsul">
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><p><a href="#" class="del-playlist" data-id="<?= $single_data_product_playlist->id; ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
									<?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?= $single_data_product_playlist->id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?= $single_data_product_playlist->id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
									</ul>
								</div> 
							</div>
							
	 <?php if($data_product_playlist[0]->c != 0) { 
             echo privacy_user_select_from_playlist($single_data_product_playlist->id);
             } ?>	
             <?php if($data_product_playlist[0]->c != 0) { ?>
			<div class="modal-box" id="myModal-<?= $single_data_product_playlist->id; ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?=$data_product_playlist[0]->song_id; ?>">
				<div class="image-holder">
				
				 <?php  $thumtype = $single_data_product_playlist->thumnitype; if($data_product_playlist[0]->c != 0 & $thumtype == 0) {  ?>
							        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($data_product_playlist[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php  } if($thumtype == 1) {  ?>
								  <img src="http://www.doityourselfnation.org/bit_bucket/<?php echo $single_data_product_playlist->customthum; ?>" class="playlistview" width="100%" >

								<?php } if($data_product_playlist[0]->c == 0 & $thumtype == 0) { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php } ?>	
								
					</div>
				
				 <div class='other-image-product-first'>
				 <div class="thumbnails columns-3">
					<?php
   $product_id_playlist = $data_product_playlist[0]->song_id;
   $_product_playlist = wc_get_product( $product_id_playlist );
 $attachment_ids = $_product_playlist->get_gallery_attachment_ids();foreach( $attachment_ids as $attachment_id ){  ?>
 <img width="180" height="180" src="<?php echo wp_get_attachment_url( $attachment_id ); ?>" class="other-image-product attachment-shop_thumbnail" alt="Chrysanthemum" title="Chrysanthemum">
 <?php } ?>
				</div></div>
				
				 <?php if($data_product_playlist[0]->c == 0) { ?>
				<h6 class="layout-title"><?php echo $single_data_product_playlist->title; ?></h6>
				 <?php } else { ?>
					<!--<h6 class="layout-title"><a href="<?php echo get_permalink($data_product_playlist[0]->song_id); ?>?opt=<?php echo $t; ?>&ih=playlists&ptype=products" title="<?php echo $single_data_product_playlist->title; ?>"><?php echo ctpreview( $content = $single_data_product_playlist->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>-->
					<h6 class="layout-title"><a href="<?php echo get_permalink($data_product_playlist[0]->song_id); ?>?opt=<?php echo $t; ?>&ih=playlists&ptype=products" title="<?php echo $single_data_product_playlist->title; ?>"><?php echo ctpreview( $content = $single_data_product_playlist->title , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
									
				 <?php } ?>
				 
				<?php echo apply_filters( 'dyn_display_review', $output, $data_product_playlist[0]->song_id, "post" ); ?>
					<h4 class="tittle-h4">Price:<?php
				$product_id_playlist= $data_product_playlist[0]->song_id;
				$_product_playlist = wc_get_product( $product_id_playlist );
				?><span class="price-product-modal">
				<?php if($_product_playlist->get_regular_price()!== ''){ if ($_product_playlist->get_sale_price() !=='') { ?> <del><span class="amount"><?php echo $_product_playlist->get_regular_price(); ?>$</span></del> <?php }else{?>
				<span class="amount"><?php echo $_product_playlist->get_regular_price(); ?>$</span>  <?php }} ?>


<?php if ($_product_playlist->get_sale_price() !=='') {
?>
<ins><span class="amount"><?php echo $_product_playlist->get_sale_price(); ?>$</span></ins> <?php } ?></span>
 <span class="fgjieryhfe"> <span class="onsale" style="left: 75em; top: 0em;">Sale!</span></span>
 </h4>

<div class="stock-grid">
<h4 class="tittle-h4"><i class="fa fa-smile-o" aria-hidden="true"></i><span class="stock-span-grid"><?php
echo $_product_playlist->get_total_stock();
?> in Stock</span></h4>
</div>




				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>


                <h4 class="tittle-h4">Sold by: <span class="stock-span-grid"><?php
	                echo get_the_author();
				?></span></h4>
				
				  <h4 class="tittle-h4">Shipped To: <span class="stock-span-grid"><?php
	               
					echo more_country_shipping_gets( $data_product_playlist[0]->song_id );
				?></span></h4>


				
				
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( $data_product_playlist[0]->song_id ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $data_product_playlist[0]->song_id);
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $data_product_playlist[0]->song_id;
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', $data_book[0]->song_id, "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if(get_the_author_id() == get_current_user_id()){ ?>
					<!--<p><a href="#" class="del-video" data-id="<?=$data_file[0]->song_id; ?>"><i class="fa fa-trash-o"></i> Delete</a></p>-->
					<?php } ?>


			</div>
			<?php echo more_country_pop_up_models( $data_product_playlist[0]->song_id  ); ?>
			</div></div><?php } ?>			 
						</div>
						
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
	

		<script>
			jQuery(document).ready(function(){
				var $ = jQuery.noConflict();

				jQuery('#playlist_create').click(function(e){
					e.preventDefault();
					//jQuery(this).hide();
					jQuery('#playlist-form').show();
					var appendthisss =  ("<div class='modal-overlayblockdiv'></div>");

                  $("body").append(appendthisss);
                  $(".modal-overlay").fadeTo(500, 0.7);
                  //$(".js-modalbox").fadeIn(500);
		
		            $('#myModalplaylist').fadeIn($(this).data());
	
				} );
				
				jQuery(".playlist_cus_th").click( function() {
                  		var test = $(this).val();
                  		if( test == 'playlist_new' ){
                  			jQuery("#playlistcusth").html('<div class="form-group"><label for="dyn_bg_play">Upload Thumbnail:</label><input type="file" id="dyn_bg_play" name="dyn_bg_play" class="dyn_bg_play" accept=".jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG"></div>');
                  		}else{
                  			jQuery("#playlistcusth").html('');
                  		}
                  	});	

				jQuery('#palylistform-cancel').click(function(e){
					e.preventDefault();
					jQuery('.playlist_title').val('');
					jQuery('#playlist-form').hide();
					jQuery('#playlist_create').show();
					
				} );

				jQuery('.playlist-url').click(
					function(e){
						e.preventDefault();
						var playlist_id = $(this).attr('data-playlist');
						if(playlist_id){
							var hrefs =  $(this).attr('href');
							jQuery.ajax({
								type: "POST",
								url: "<?php echo admin_url('admin-ajax.php'); ?>",
								data: { action:'session_id_playlist',playlist_id: playlist_id,},
								success : function(data){
								//localStorage["playlist_id"] = playlist_id;
								window.location.href = hrefs;
							}
						});
						}
					}
					);
					
							
				$('.del-playlist').click(function(e){
				e.preventDefault();

				console.log($(this).attr('data-id'));
				//var confrm = confirm("Are you sure you want to continue!");
				var playlist_idsss = $(this).attr('data-id');
				var user_id = '<?= get_current_user_id;?>';
              //  alert(favorite_idsss);
				
					$.ajax({
						type: "POST",
						url: ajaxurl,
						data: { action:'user_playlist_delete_channel','playlist_idsss': playlist_idsss,'user_id': user_id},
						success : function(data){
							//$('.'+favorite_idsss).remove();
							//$('.alert-success').attr('style','display:block');
							//
							alert(data);
							location.reload();
							
						}
					});
				//}
			});	

			});
			
		</script>
		
	 <?php function more_country_shipping_gets( $postid ){ 

 $allcountry_accept = get_post_meta( $postid, '_countries_shippinf_op', true );
					      $shipping_country = '';
						  $morecountrys_shipp = '';
                           if($allcountry_accept == 'all')
						   {
							   $shipping_country = 'All Countries';
						   }else{
							   
							   $spacicecountry = get_post_meta( $postid, '_spacicecountry');
				               $spacicecountrys = explode( ",", $spacicecountry[0] ); 
							   $countss = 0;
							   $tocountss = 0;
							   $tocountss  = count($spacicecountrys);
							   
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
										
									 if($countss < 2 && $tocountss != 1)
							             {
								            $shipping_country .=  $country_full_name . ', ';   
							            }
									 $countss++; 
							        }
								if($tocountss > 2)
								{
									$morecountrys_shipp = '<a href="#morecountry" id="morecountry'. $postid .'"> View More</a>';
								}									
						    }
						  
					 echo $shipping_country . ' ' .$morecountrys_shipp; 
					 } 
	?>
<?php function more_country_pop_up_models( $postid ){ ?>	
<div class="modal fade" id="moreshipped<?php echo $postid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Shipped To </h4>
					</div>
					<div class="modal-bodys">
					<?php 
					$storename = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );
					?>
					<div class="sold-by"> 	<span class="titlesoldby">Sold By<span class="snamesoldby">
								<?php if(!empty($storename)) { 
								 echo $storename;
								} else {
								?>
								<?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?>
								<?php } ?>
							</span></span>
						 
					</div>
					<div class="well well-sm">
			           <strong>Views</strong>
			           <div class="btn-group">
				          <a href="#" id="liststoressss" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
				        </span>List</a> <a href="#" id="gridstoresss" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
			            </div>
		            </div>
					 <div id="productssss<?php echo $postid; ?>" class="list-group">
						 <div class="shipped-by">
						 
					<?php  
					      $shipping_country = '';
						  $spacicecountry = get_post_meta( $postid , '_spacicecountry');
				          $spacicecountrys = explode( ",", $spacicecountry[0] ); 
						  $shipping_details = get_post_meta( $postid , '_wcv_shipping_rates',  true ); 
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										
										
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
									    
								        $shipping_country =  $country_full_name; 
										
                                        if($spacicecountryss == 'GB')
										{
										 $spacicecountryss = 'UK';	
										}										
										$shipping_rate    = '';
										//$rates = 0;
		                                foreach ( $shipping_details as $shipping_rate ) {
			                             if($spacicecountryss == $shipping_rate['country'])
			                               {
				                              $shipping_rate =  $shipping_rate['fee'];
			                                
                        ?>										
							            <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 grid-group-item changeStyleGrid">
										<div class="layout-2-wrapper">
										   <h4>Country</h4>
										<?php	echo $shipping_country;	?>
										  <h4>Shipping Rate</h4>
										  <span class="amount" ><?php	echo '$' . $shipping_rate;	?></span>
										  <div class="listview" style="display:none">
										   <h4>Shipping Policy</h4>
										   </div>
										</div></div>
						
						<?php	} } }	?>
					   </div>
					<style>.shipped-counties{color: #337ab7;font-weight: bold; font-size: 13px;}
					 .titlesoldby{font-size: 22px !important;
                     padding: 6px !important;
					 text-transform: uppercase;
					 } 
					 .snamesoldby{color: #337ab7;}
					 #productssss<?php echo $postid; ?> .changeStyleGrid {
                            min-height: 130px !important;
                           text-align: center;
						   }
					 #productssss<?php echo $postid; ?> .changeStyleList { height: 139px !important;}	  
                     @media (min-width: 768px){
                          #moreshipped<?php echo $postid; ?> .modal-dialog {
                            width: 700px; margin: 30px auto;
							}	
					 }
                   .modal-bodys{					 
                     position: relative;
                     padding: 15px;	
					}					 
				   </style>
					</div>
					 <h2> &nbsp; &nbsp; </h2>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		
		$('#morecountry<?php echo $postid; ?>').click(function(e){
				e.preventDefault();
                 //#morecountry  moreshipped
				var $ = jQuery.noConflict();
				
				$('#moreshipped<?php echo $postid; ?>').modal();
			})
			
			$('#moreshipped<?php echo $postid; ?> #liststoressss').click(function(event){
    	               event.preventDefault();
					 $('#productssss<?php echo $postid; ?> .item').removeClass('grid-group-item changeStyleGrid');  
    	             $('#productssss<?php echo $postid; ?> .item').addClass('list-group-item changeStyleList');
					 

                 });
	   
	        $('#moreshipped<?php echo $postid; ?> #gridstoresss').click(function(event){
    	            event.preventDefault();
    	           $('#productssss<?php echo $postid; ?> .item').removeClass('list-group-item changeStyleList');
    	           $('#productssss<?php echo $postid; ?> .item').addClass('grid-group-item changeStyleGrid');
               });
			   
		</script>
<?php } ?>						