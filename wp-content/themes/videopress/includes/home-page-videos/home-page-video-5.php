<?php
$filename = __FILE__;
$page_number = reset(array_filter(preg_split("/\D+/", $filename)));

$dir = 'homepage_videos/' . $page_number; 
$video = array_diff(scandir($dir), array('..', '.'));
?>
<video style='width: 100%;' class='homepage_video'>
<source type="video/mp4" src="<?php echo home_url(); ?>/homepage_videos/<?php echo $page_number; ?>/<?php echo $video[2]; ?>">
</video>