<div class="videocontainer">
	<div id="VideoPlayer"><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="loading" /></div>
    <div class="clear"></div>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			jwplayer("VideoPlayer").setup({
				file: "<?php echo home_url(); ?>/homepage_videos/1/homepage_video.mp4",
				image: "<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>",
				width: '100%',
				aspectratio: "16:9",
				skin: '<?php videopress_playerskin(); ?>',
				<?php //videopress_player_logo(); ?>
			});
		});
        </script>
	<div class="spacing-10"></div>
</div>