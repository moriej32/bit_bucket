<div class="list-group" id="new-book-tab">
<?php echo '<a href="#new-books" id="new-books"><h5><b>NEW BOOKS</b></h5></a>'; ?>
<!-- Sort block -->	

<div class="alert alert-success v-d" style="display:none;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> book is Deleted.
	</div>
<div class="cover-twoblocks">
    <div class="sortvideo-div">
        <form method="get" id="sortbook-form">
            <p><label>Currently Sorted By : </label>
                <select class="sortbook" name="sortbook">
                    <option value="recent" <?= ($selected)?'selected':'';?>>Recent</option>
                    <option value="views" <?= ($views)?'selected':'';?>>Views</option>  
                    <option value="endorsements" <?= ($endorsements)?'selected':'';?>>Endorsements</option>
                    <option value="favourites" <?= ($favourites)?'selected':'';?>>Favorites</option>
                    <option value="comments" <?= ($comments)?'selected':'';?>>comments</option>
                </select>
            </p>
        </form>
    </div>
    
    <div class="well well-sm">
        <strong>Views</strong>
        <div class="btn-group">
            <a href="" id="list-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List
            </a>
            <a href="" id="grid-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
</div>

	<div class="row">
<?php
global $wpdb, $paged;	
$paged = get_query_var( 'paged' );
//echo get_query_var('author');
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$profile_id = $author->ID;
	          $args = array(
			        'post_type' => 'dyn_book',
					'paged' => $paged,
					'post_status' => 'publish',
					'author' => $profile_id,
					'posts_per_page' => 12
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
				
				//echo $postID = the_title() ."<br>";
				$postID = get_the_ID();
                $bkdata = get_post($postID);
				
				$post = get_post($postID);
				$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
				$selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
				$post_author     = $bkdata->post_author;
				$user_ID         = get_current_user_id(); 
				$selectUsersList = explode( ",", $selectUsersList[0] ); 
				$currentuser = 0;
				
				if(!is_user_logged_in()) {
					
					if(isset($privacyOption[0]) and $privacyOption[0] == "public") { 
					 echo html_model_view( $postID, $currentuser);
					}
					
					
				}
				
				if( $profile_id == $user_ID ) {
					$currentuser = 1;
					echo html_model_view( $postID, $currentuser );
				}
				
				if( $profile_id != $user_ID ) {
					echo html_model_view( $postID, $currentuser);
				}
				
				if( is_array($selectUsersList) and count($selectUsersList) > 0 ){
					if( in_array($user_ID, $selectUsersList) ){
						echo html_model_view( $postID, $currentuser);
					}
				}
				
				endwhile;
				echo paginate_links(array('add_args' => array('newbooks' => 'newbooks')));
				endif;
				wp_reset_postdata();
				
function html_model_view( $postID, $currentuser ){
	global $wpdb, $paged;
	            $bkdata = get_post($postID);
				$post = get_post($postID);
				
				$post_author     = $bkdata->post_author;
				$user_ID         = get_current_user_id(); 
				
	            $refr = get_post_meta($postID,'video_options_refr',true);
				if($refr){}else{$refr= 'No reference given'; }
				$thumbnail_id = get_post_thumbnail_id($postID);
				$thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
				$imgsrc= $thumbnail_url [0];
				$revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");
				$endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
				$endorsnum=count($endors);
				$favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
				$favsnum=count($favs);
				
				$book_author=get_the_author_meta('display_name', $post_author);
				?>
				<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center <?php echo $postID; ?>" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">

								    <div class="galleryOverlay" style="display:none;">
								   <?php 
								       // global $mpcrf_options;
									//	$mpcrf_optionsss = get_option('rfbwp_options',true);
										 
										 $bookshortecode = get_post_meta( $postID, 'bookshortcode', true );
										 
										echo $valuetest =  do_shortcode( '[responsive-flipbooksss id="'. $bookshortecode .'"]' );
                                         
                                            ?>
											
								  </div>
							   	   <div class="image-holder galleryImgCont">
								   <a href="<?php echo get_permalink($postID); ?>">
								   
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:100%;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                    
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<?php if($currentuser == 1) { ?>
										<li><a href="#" class="del-booksid" data-id="<?php echo $postID; ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								        <?php } ?>
										<li><a class="detailsblock-btn" data-modal-id="myModal-<?php echo $postID; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
										<?php if($currentuser == 1) { ?>
									   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php echo $postID; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							             <?php } ?>
									   </ul>
									   </div>
									   
								 </div>
							   </div>
							   
							   
							    <!-- Modal box detail for books --> 
								
								
        <div class="modal-box" id="myModal-<?php echo $postID; ?>">                  
            <div class="modal-body">
                <a class="js-modal-close close">×</a>
                <div class="layout-2-details layout-2-details-<?php echo $postID; ?>"> 
				
                     <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:100%">';
											}?>

											
								   </div>
								   
                    <h6 class="layout-title">  
                    <?php
                        preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                        if ( count($matches[0]) && count($matches[1]) >4){;
                        ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                        <?php 
                        }
                        else if(strlen($bkdata->post_title)>30){ 
                        ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                       <?php
                        }
                        else {
                                ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                        <?php } ?>              
                    </h6>
                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                    <div class="video_excerpt_text one"><h4 class="tittle-h4 ">Description</h4>
                        <div class="inline-blockright">
                            <?php if($bkdata->post_content ==""){
                                echo "No description available.";}else{
                                echo $bkdata->post_content;
                            }?>
                        </div>
                    </div>                                          
                    <ul class="stats">
                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                        <li><?php videopress_countviews($postID); ?></li>
                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                        <li><?php comments_number(); ?></li>
                    </ul> 
                    <div class="clear"></div>
                </div>
            </div>
        </div> 
<div class="modal-box" id="myModalPrivacy-<?php echo $postID; ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php echo $postID; ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php echo $postID; ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) and $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php echo $postID; ?>" name="privacy-option-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php echo $postID; ?>" name="privacy-option-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php echo $postID; ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php echo $postID; ?>" name="privacy-option-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php echo $postID; ?>" name="privacy-option2-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php echo $postID; ?>">
													<?php
													  echo '<select id="tokenize-'.$postID.'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo $postID; ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php echo $postID; ?>">
													<?php
													  echo '<select id="tokenize-'.$postID.'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo $postID; ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php echo $postID; ?>">
												<?php
												  echo '<select id="tokenize-'.$postID.'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo $postID; ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php echo $postID; ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php echo $postID; ?>" name="save-privacy-option-<?php echo $postID; ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo $postID; ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php echo $postID; ?>").click(function(){
							 $("#privacy-radio1-<?php echo $postID; ?>").prop( "checked", false );
						     $(".select-box-<?php echo $postID; ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php echo $postID; ?>").click(function(){
							 $("#privacy-radio-<?php echo $postID; ?>").prop( "checked", false );
						     $(".select-box-<?php echo $postID; ?>").slideUp();
							 $('select#select_users_list-<?php echo $postID; ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php echo $postID; ?>").click(function(){
							 $('#msgLoader-<?php echo $postID; ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php echo $postID; ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php echo $postID; ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php echo $postID; ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php echo $postID; ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php echo $postID; ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php echo $postID; ?>, #privacy-radio-<?php echo $postID; ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php echo $postID; ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>	
							   <!-- end of mode div> -->
				<?php
	
}
?>
</div></div>

 <script type="text/javascript">
        $('#new-book-tab .bookitem').addClass('grid-group-item');   
        $("#grid-book").click(function(event){
            event.preventDefault();
            $('#new-book-tab .bookitem').removeClass('list-group-item changeStyleList').addClass('grid-group-item changeStyleGrid');
            $('.layout-title-box .layout-2-details').fadeOut();
            $('.detailsblock-btn').show();
        });
        $("#list-book").click(function(event){
            event.preventDefault();
            $('#new-book-tab .bookitem').removeClass('grid-group-item changeStyleGrid').addClass('list-group-item changeStyleList');
            $('.layout-title-box .layout-2-details').fadeIn();
            $('.detailsblock-btn').show();
			$(".bookitem").attr("style",'padding: 5px 5px 5px 5px !important; margin:0px !important; height:330px;');
        });
        //end list/grid view
        // sort book
        $(".sortbook").on("change", function(){
            this.form.submit();
        });
        
    </script>
	<script>
		jQuery(document).ready(function(){
			var $ = jQuery.noConflict();

			$('.del-booksid').click(function(e){
				e.preventDefault();

				console.log($(this).attr('data-id'));
				var confrm = confirm("Are you sure you want to continue!");
				var book_id = $(this).attr('data-id');
				var user_id = '<?= get_current_user_id;?>';

				if(confrm){
					$.ajax({
						type: "POST",
						url: ajaxurl,
						data: { action:'user_book_delete_book','book_id': book_id},
						success : function(data){
							$('.'+book_id).remove();
							$('.alert-success').attr('style','display:block');
						}
					});
				}
			})

		})

	</script>