<?php 
$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
$post='';

$cat=isset($_GET['cat_type']) ? $_GET['cat_type'] : '';

if($cat=='new-files')
$fstart = ($paged==1) ? 0 : intval($paged-1) * 12;
else
$fstart=0;

//create custom loop for books
if(!is_user_logged_in()){
$ftemp= 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_file" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
 ORDER BY   STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC' ;
$ftquery = $ftemp.' LIMIT  ' . $fstart .', 12';
}
else if(is_user_logged_in()){
$ftemp = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_file" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
UNION
SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_file" AND post_status="publish" AND ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.get_current_user_id().'
UNION
SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_file" AND post_status="publish" AND ID IN (SELECT post_id
FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY  STR_TO_DATE(post_date_gmt,"%Y-%m-%d %H:%i:%s")  DESC';

$ftquery = $ftemp.' LIMIT  ' . $fstart .', 12';
}

$fresult = $wpdb->get_results( $ftquery ); 
	 //echo  $wpdb->num_rows . 'Rows Found'; 

$ftotal=$wpdb->query($ftemp);


?>
<div class="list-group">
<?php echo '<a href="#new-files" id="new-files"><h5 style="display:inline;"><b>NEW FILES</b></h5></a>'; ?>
	
<div class="row">
			 
<?php if($fresult){
	$fri=0;

  
foreach ($fresult as $fdata){
$postID = $fdata->ID; 
$post = get_post($postID);
$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
$selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
$post_author     = $fdata->post_author;
$user_ID         = get_current_user_id(); 
$selectUsersList = explode( ",", $selectUsersList[0] ); 

$theflContent=$fdata->post_content;
$file_author = get_the_author_meta( 'display_name', $fdata->post_author );

$refr = get_post_meta($postID,'video_options_refr',true);
	if($refr){}
	else{$refr= 'No reference given'; }

$key_value = get_post_meta( $postID, 'dyn_upload_value', true );
$fext=get_post($key_value)->post_mime_type; 

 
 
$ftype=apply_filters( 'dyn_file_type_by_mime', get_the_ID() );

 
$imgClass = apply_filters('dyn_class_by_mime', $postID);
 

$download_count=get_post_meta($postID, 'dyn_download_count', true);

$revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post"); 

$endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
$endorsnum=count($endors);


$favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
$favsnum=count($favs);

$uploaded_imgssss = get_post_meta( $postID, 'dyn_thumbaniimg_src', true );
						
 if(!is_user_logged_in())
		{  // case where user is not logged in 
	             if(isset($privacyOption[0]) and ($privacyOption[0] != "private"))
				  { 
					      //if( ($fri % 6) == 0){echo '</div><div class="row">'; }  ;
					  ?>
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;"> 
							 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px"> 

                                  <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>									

									
									<div class="layout-title-box text-center" style="display:block;">
									<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
									<?php
									preg_match_all('/(\w+\s)/', $fdata->post_title, $matches);

									if (count($matches[1]) >4){
									?>
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
									<?php 
										}
									else if(strlen($fdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $fdata->post_title ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($fdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }	
									else {
											?>
									   <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $fdata->post_title; ?></a>

									<?php } ?>
									  </h6>
									 
									<!-- <ul class="stats">
										<li ><?php videopress_displayviews( get_the_ID() ); ?></li>
										<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
										<li>Author: <font color="red"><?php //echo get_the_author(); ?></font></li>

									</ul>
									-->
									<ul class="bottom-detailsul" style="vertical-align:baseline;">
									<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $fdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $fdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Details
									</a></li>
								   </ul>
								   </div>
								  
							 </div>
						   </div>
                      <?php
                       $fri++;					  
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							   //if( ($fri % 6) == 0){echo '</div><div class="row">'; }  ;
					  ?>
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;"> 
							 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px"> 

                                <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 
								 <!--<div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>-->
								 
								   
								  </div></a> 
								  
								  <?php } else { ?>
								   <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important;">
								  </div></a> 				   
																		
								  <?php } ?>
									
									<div class="layout-title-box text-center" style="display:block;">
									<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
									<?php
									preg_match_all('/(\w+\s)/', $fdata->post_title, $matches);

									if (count($matches[1]) >4){
									?>
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
									<?php 
										}
									else if(strlen($fdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $fdata->post_title ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($fdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
									else {
											?>
									   <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $fdata->post_title; ?></a>

									<?php } ?>
									  </h6>
									 
									<!-- <ul class="stats">
										<li ><?php videopress_displayviews( get_the_ID() ); ?></li>
										<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
										<li>Author: <font color="red"><?php //echo get_the_author(); ?></font></li>

									</ul>
									-->
									<ul class="bottom-detailsul" style="vertical-align:baseline;">
									<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $fdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $fdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Details
									</a></li>
								   </ul>
								   </div>
								  
							 </div>
						   </div>
                      <?php
                       $fri++;					
						}
				  }
		} 
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)	
			 {    // Case where logged in User is same as Video Author User
				   //if( ($fri % 6) == 0){echo '</div><div class="row">'; }  ;
					  ?>
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;"> 
							 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px"> 

                                 <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  <a href="<?php echo get_permalink($postID); ?>">
								  
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 
								 <!--<div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>-->
								 
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important;">
								 
								 <!--<div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>-->
								 
								   
								  </div></a> 				   
																		
								  <?php } ?>
									
									<div class="layout-title-box text-center" style="display:block;">
									<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
									<?php
									preg_match_all('/(\w+\s)/', $fdata->post_title, $matches);

									if (count($matches[1]) >4){
									?>
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
									<?php 
										}
									else if(strlen($fdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $fdata->post_title ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($fdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
									else {
											?>
									   <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $fdata->post_title; ?></a>

									<?php } ?>
									  </h6>
									 
									<!-- <ul class="stats">
										<li ><?php videopress_displayviews( get_the_ID() ); ?></li>
										<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
										<li>Author: <font color="red"><?php //echo get_the_author(); ?></font></li>

									</ul>
									-->
									<ul class="bottom-detailsul" style="vertical-align:baseline;">
									<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $fdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $fdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Details
									</a></li>
								   </ul>
								   </div>
								  
							 </div>
						   </div>
                      <?php
                       $fri++;						
					   
			 }	
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					    // if( ($fri % 6) == 0){echo '</div><div class="row">'; }  ;
					  ?>
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;"> 
							 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px"> 

                                  <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 
								 <!--<div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>-->
								 
								   
								  </div></a> 
								  
								  <?php } else { ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								   
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important;">
								
								 <!--<div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>-->
								 
								   
								  </div></a> 				   
																		
								  <?php } ?>
									
									<div class="layout-title-box text-center" style="display:block;">
									<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
									<?php
									preg_match_all('/(\w+\s)/', $fdata->post_title, $matches);

									if (count($matches[1]) >4){
									?>
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
									<?php 
										}
									else if(strlen($fdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $fdata->post_title ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($fdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
									else {
											?>
									   <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $fdata->post_title; ?></a>

									<?php } ?>
									  </h6>
									 
									<!-- <ul class="stats">
										<li ><?php videopress_displayviews( get_the_ID() ); ?></li>
										<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
										<li>Author: <font color="red"><?php //echo get_the_author(); ?></font></li>

									</ul>
									-->
									<ul class="bottom-detailsul" style="vertical-align:baseline;">
									<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $fdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $fdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Details
									</a></li>
								   </ul>
								   </div>
								  
							 </div>
						   </div>
                      <?php
                       $fri++;					
									
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						    //if( ($fri % 6) == 0){echo '</div><div class="row">'; }  ;
					  ?>
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;"> 
							 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px"> 

                               <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  <a href="<?php echo get_permalink($postID); ?>">
								  
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 
								  </div></a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important;">
								 
								 
								   
								  </div>	</a> 			   
																		
								  <?php } ?>
									
									<div class="layout-title-box text-center" style="display:block;">
									<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
									<?php
									preg_match_all('/(\w+\s)/', $fdata->post_title, $matches);

									if (count($matches[1]) >4){
									?>
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
									<?php 
									}
									else if(strlen($fdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $fdata->post_title ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($fdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
									else {
											?>
									   <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $fdata->post_title; ?></a>

									<?php } ?>
									  </h6>
									 
									<!-- <ul class="stats">
										<li ><?php videopress_displayviews( get_the_ID() ); ?></li>
										<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
										<li>Author: <font color="red"><?php //echo get_the_author(); ?></font></li>

									</ul>
									-->
									<ul class="bottom-detailsul" style="vertical-align:baseline;">
									<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $fdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $fdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Details
									</a></li>
								   </ul>
								   </div>
								  
							 </div>
						   </div>
                      <?php
                       $fri++;						
						
					  }	
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										   //if( ($fri % 6) == 0){echo '</div><div class="row">'; }  ;
					  ?>
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;"> 
							 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px"> 

                               <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								  <a href="<?php echo get_permalink($postID); ?>">
								  
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								 <a href="<?php echo get_permalink($postID); ?>">
								 <!--<div class="hover-item dyn-file">
									<i class="fa fa-download"></i>
								 </div>-->
								 
								   </a> 
								  </div>
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
								  
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important;">
								 
								  
								  </div> </a> 				   
																		
								  <?php } ?>
									
									<div class="layout-title-box text-center" style="display:block;">
									<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
									<?php
									preg_match_all('/(\w+\s)/', $fdata->post_title, $matches);

									if (count($matches[1]) >4){
									?>
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
									<?php 
										}
									else if(strlen($fdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $fdata->post_title ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($fdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
									else {
											?>
									   <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $fdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $fdata->post_title; ?></a>

									<?php } ?>
									  </h6>
									 
									<!-- <ul class="stats">
										<li ><?php videopress_displayviews( get_the_ID() ); ?></li>
										<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
										<li>Author: <font color="red"><?php //echo get_the_author(); ?></font></li>

									</ul>
									-->
									<ul class="bottom-detailsul" style="vertical-align:baseline;">
									<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $fdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $fdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Details
									</a></li>
								   </ul>
								   </div>
								  
							 </div>
						   </div>
                      <?php
                       $fri++;					
						
									 }
							        else
									 { // case where user is not in access list


									 }
							   }
							   else
							   {  }
					  }	 
				  }
			 }	
		}
    }

  }
  ?>  


		</div>
		<div class="row" data-anchor="new-files">
		<div class="col-sm-12">
		<nav class="pull-right">
        <?php
      if($cat=='new-files'){
         $pcurrent=max( 1, get_query_var('page') );
        }
       else{
         $pcurrent=1;	
        }

     $paginate = paginate_links( array(	
	'format' => 'page/%#%/',
	'current' => $pcurrent,
	'type'=>'array',
	'end_size'=>1,
	'mid_size'=>4,
	'total' => ceil($ftotal / 12)
           ) );

 $pcount=count($paginate);
 

 $maxp=ceil(($ftotal/12)/2);
 
if( $pcurrent<= ceil($ftotal / 12)  )
{ 
 if( ceil($ftotal / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($ftotal / 12) >= 5 ){


 if ($pcurrent<=6)
 {
 	if ($pcurrent>2)
 	{echo $paginate[0].'&nbsp;';}
     if($paginate[$pcurrent-2]){
 	echo $paginate[$pcurrent-2].'&nbsp;';
        }
 	echo $paginate[$pcurrent-1].'&nbsp;';
 	echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
 	echo $paginate[5].'&nbsp;';
 	echo $paginate[6].'&nbsp;';
 	echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($ftotal/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}
                        
                ?>
    </nav>
    </div>
   </div>
</div>
