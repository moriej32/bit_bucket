<?php


$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
//$start = ($paged==1) ? 0 : $paged * 12;
$post='';
$cat=isset($_GET['cat_type']) ? $_GET['cat_type'] : '';

if(!is_user_logged_in()){

$ftemp= 'SELECT *,
(SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = a.ID) as countfavorite,
(SELECT Count(s2.post_id) FROM wp_user_views s2  where  s2.post_id=a.ID) as countview,
(SELECT Count(s3.post_id) FROM wp_endorsements s3 WHERE s3.post_id = a.ID) as countendorsements,
(SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) as countcoment,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.ID) as rating,
(SELECT Count(post_id) FROM wp_dyn_review where  post_id=a.ID) as countreview

 FROM ' .$wpdb->prefix.'posts a  WHERE post_type="dyn_file" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
 ORDER BY  countfavorite DESC,countview DESC,countendorsements DESC,countcoment DESC,rating DESC,countreview DESC, a.ID DESC' ;
//$ftquery = $ftemp.' LIMIT  ' . $fstart .', 12';
$fhresult = $wpdb->get_results( $ftemp );

}


else if(is_user_logged_in())
{
/*
$fhtemp= 'SELECT post_id, value FROM '.$wpdb->prefix. 'hot_file_cluster as vid
INNER JOIN
'.$wpdb->prefix. 'hot_user_file_cluster as clst
ON vid.cluster = clst.cluster
WHERE clst.user_id='.get_current_user_id().' AND vid.value>0
AND vid.post_id NOT IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
UNION 
SELECT post_id, value
FROM '.$wpdb->prefix. 'hot_file_cluster, '.$wpdb->prefix. 'posts
WHERE value >0
AND cluster = (SELECT cluster FROM '.$wpdb->prefix. 'hot_user_file_cluster WHERE user_id='.get_current_user_id().')
AND ID = post_id
AND ID
IN (
SELECT post_id
FROM '.$wpdb->prefix. 'postmeta
WHERE meta_key = "privacy-option"
AND meta_value = "private"
)
AND post_author ='.get_current_user_id().'
UNION
SELECT  post_id, value
FROM '.$wpdb->prefix. 'hot_file_cluster as vid
INNER JOIN
'.$wpdb->prefix. 'hot_user_file_cluster as clst
ON vid.cluster = clst.cluster
WHERE clst.user_id='.get_current_user_id().' AND vid.value>0
AND  vid.post_id
IN (SELECT post_id
FROM '.$wpdb->prefix. 'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]")
ORDER BY value  DESC';

//$vhquery = $vhtemp.' LIMIT  ' . $start .', 12';
$fhresult = $wpdb->get_results( $fhtemp ); 
$fhttempotal=$wpdb->query($fhtemp);

if ($fhttempotal==0)
{
$fhtemp= 'SELECT ROUND(AVG( value ),0) AS average, post_id
FROM '.$wpdb->prefix. 'hot_file_cluster
WHERE value >0
AND post_id  NOT IN (SELECT post_id FROM '.$wpdb->prefix. 'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
GROUP BY post_id
UNION 
SELECT ROUND( AVG( value ) , 0 ) AS average, post_id
FROM '.$wpdb->prefix. 'hot_file_cluster, '.$wpdb->prefix. 'posts
WHERE value >0
AND ID = post_id
AND ID
IN (
SELECT post_id
FROM '.$wpdb->prefix. 'postmeta
WHERE meta_key = "privacy-option"
AND meta_value = "private"
)
AND post_author ='.get_current_user_id().'
GROUP BY post_id
UNION
SELECT ROUND( AVG( value ) , 0 ) AS average, post_id
FROM '.$wpdb->prefix. 'hot_file_cluster
WHERE value >0
AND  post_id
IN (SELECT post_id
FROM '.$wpdb->prefix. 'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]")
GROUP BY post_id
ORDER BY average  DESC';

//$vhquery = $vhtemp.' LIMIT  ' . $start .', 12';
$fhresult = $wpdb->get_results( $fhtemp ); 
//$fhtotal=$wpdb->query($fhtemp);
}
*/

$ftemp= 'SELECT *,
(SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = a.ID) as countfavorite,
(SELECT Count(s2.post_id) FROM wp_user_views s2  where  s2.post_id=a.ID) as countview,
(SELECT Count(s3.post_id) FROM wp_endorsements s3 WHERE s3.post_id = a.ID) as countendorsements,
(SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.ID) as countcoment,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.ID) as rating,
(SELECT Count(post_id) FROM wp_dyn_review where  post_id=a.ID) as countreview

 FROM ' .$wpdb->prefix.'posts a  WHERE post_type="dyn_file" AND post_status="publish" 
 ORDER BY  countfavorite ASC,countview ASC,countendorsements ASC,countcoment ASC,rating ASC,countreview ASC, a.ID ASC' ;
//$ftquery = $ftemp.' LIMIT  ' . $fstart .', 12';
$fhresult = $wpdb->get_results( $ftemp );

}

$ftemparray=array();

foreach($fhresult as $ftempdata){
	//get the numeric ids
	$file_check=get_post(intval($ftempdata->ID));
	if( ($file_check->post_type =="dyn_file") && ($file_check->post_status =="publish") ) {
    array_push($ftemparray, intval($ftempdata->ID));
      }      
}

$fhtotal=count($ftemparray);
$fallarray=$ftemparray;

//order by id;
rsort($ftemparray);


//get total of 2 * floor($fhtotal / 12) ids from the top of the sorted array to place on every page
if(count($fallarray)>12)
$ftopIds=array_slice($ftemparray, 0, 2*(floor($fhtotal / 12)));
else
$ftopIds=array_slice($ftemparray, 0, 2);
//remove topids from original all array
foreach($ftopIds as $fremId){	
$findx=array_search($fremId, $fallarray);
unset($fallarray[$findx]);
}

$fcnt=0;
$fcar=0;

//add 2 topids at the beginning and every 12 values of original array
do{
	array_splice($fallarray,$fcar,0,array($ftopIds[$fcnt], $ftopIds[$fcnt+1]));
	$fcar+=12;
	$fcnt+=2;
}
while(!empty($ftopIds[$fcnt]));


// cut into chunks of 12
if(count($fallarray)<12)
$fallres=array_chunk($fallarray, count($fallarray));
else
$fallres=array_chunk($fallarray, 12 );


 ?>
<div class="list-group">
<?php echo '<a href="#hot-files" id="hot-files"><h5><b>HOT FILES</b></h5></a>'; ?>
	
	<div class="row">				 
<?php if($fhresult){
	$ri=0;

if($cat=='hot-files')
$paglink=( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
else
$paglink=1;
  
foreach ($fallres[$paglink-1] as $fhdata){   
$postID = $fhdata; 
$post=get_post($postID);

$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
$selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
$post_author     = $post->post_author;
$user_ID         = get_current_user_id();  
$selectUsersList = explode( ",", $selectUsersList[0] ); 
 //$post_id=$data->ID;
$theflContent=$post->post_content; 
$file_author = get_the_author_meta( 'display_name', $post->post_author );

 $refr = get_post_meta($postID,'video_options_refr',true);
	if($refr){}
	else{$refr= 'No reference given'; }

$ftype=apply_filters( 'dyn_file_type_by_mime', get_the_ID() );

 
$imgClass = apply_filters('dyn_class_by_mime', $postID);
 

$download_count=get_post_meta($postID, 'dyn_download_count', true);

$revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post"); 

$endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
$endorsnum=count($endors);

$favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
$favsnum=count($favs);

$uploaded_imgssss = get_post_meta( $postID, 'dyn_thumbaniimg_src', true );

       if(!is_user_logged_in())
		{  // case where user is not logged in 
	             if(isset($privacyOption[0]) and ($privacyOption[0] != "private"))
				  {
					  //if( ($ri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	    <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>	
								  
								  
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $post->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($post->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink($postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($post->post_title, 0, 30) .' [...]'; ?></a>
									    <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $post->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>" data-post_id="<?php echo $postID;?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									     <div id="review-post-id-<?php echo $postID;?>" style="display:none">
									   <?php $output = "";
		echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
									   </div>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $ri++;
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	  
								      <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>
								  
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $post->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($post->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink($postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($post->post_title, 0, 30) .' [...]'; ?></a>
									    <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $post->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>" data-post_id="<?php echo $postID;?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									     <div id="review-post-id-<?php echo $postID;?>" style="display:none">
									   <?php $output = "";
		echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
									   </div>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $ri++;
						}
				  }
		} 
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)	
			 {    // Case where logged in User is same as Video Author User
		          //if( ($ri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	     <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>
								  
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $post->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($post->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink($postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($post->post_title, 0, 30) .' [...]'; ?></a>
									    <?php
									    }	
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $post->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>" data-post_id="<?php echo $postID;?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									     <div id="review-post-id-<?php echo $postID;?>" style="display:none">
									   <?php $output = "";
		echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
									   </div>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $ri++;
			 }	
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) and $privacyOption[0] == "public")
				  {
					   //if( ($ri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	     
                                      <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>
								  
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $post->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($post->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink($postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($post->post_title, 0, 30) .' [...]'; ?></a>
									    <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $post->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>" data-post_id="<?php echo $postID;?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									     <div id="review-post-id-<?php echo $postID;?>" style="display:none">
									   <?php $output = "";
		echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
									   </div>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $ri++;
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						   //if( ($ri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	      <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>
								  
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $post->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($post->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink($postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($post->post_title, 0, 30) .' [...]'; ?></a>
									    <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $post->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>" data-post_id="<?php echo $postID;?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									     <div id="review-post-id-<?php echo $postID;?>" style="display:none">
									   <?php $output = "";
		echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
									   </div>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $ri++;
					  }	
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   
                                      <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
								   <a href="<?php echo get_permalink($postID); ?>">
								  <div class="image-holder filesz dyn-file-sprite" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');min-height:140px; max-width:100% !important;  background-repeat: no-repeat;background-position: center;">
								
								  
								  </div> </a> 
								  
								  <?php } else { ?>
								  <a href="<?php echo get_permalink($postID); ?>">
							   <div class="image-holder filesz dyn-file-sprite <?php echo $imgClass ?>" style="min-height:140px; max-width:100% !important; ">
								  </div></a> 				   
								  <?php } ?>

								  
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $post->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($post->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink($postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($post->post_title, 0, 30) .' [...]'; ?></a>
									    <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $post->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $post->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="docs" data-author="<?php echo $file_author; ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($theflContent); ?>" data-image="<?php echo $imgsrc ?>" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>" data-post_id="<?php echo $postID;?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									     <div id="review-post-id-<?php echo $postID;?>" style="display:none">
									   <?php $output = "";
		echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
									   </div>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $ri++;
									 }
									 else
									 {  // case where user is not in access list
									 	 
									 }
							   }
							   else
							   {  }
					  }	 
				  }
			 }	
		} 

	   

	  } //foreach

	 } //if result

	 ?>
	     

		</div>
        <div class="row" data-anchor="hot-files">
		<div class="col-sm-12">
		<nav class="pull-right">
        <?php

       if($cat=='hot-files'){
         $pcurrent=max( 1, get_query_var('page') );
        }
       else{
         $pcurrent=1;	
        }

     $paginate = paginate_links( array(	
	'format' => 'page/%#%/',
	'current' => $pcurrent,
	'type'=>'array',
	'end_size'=>1,
	'mid_size'=>4,
	'total' => ceil($fhtotal / 12)
           ) );
       
$pcount=count($paginate);
 

 $maxp=ceil(($fhtotal/12)/2);
 
if( $pcurrent<= ceil($fhtotal / 12)  )
{ 
 if( ceil($fhtotal / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($fhtotal / 12) >= 5 ){


 if ($pcurrent<=6)
 {
 	if ($pcurrent>2)
 	{echo $paginate[0].'&nbsp;';}
      if($paginate[$pcurrent-2]){
 	echo $paginate[$pcurrent-2].'&nbsp;';
      }
 	echo $paginate[$pcurrent-1].'&nbsp;';
 	echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
 	echo $paginate[5].'&nbsp;';
 	echo $paginate[6].'&nbsp;';
 	echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($fhtotal/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}


                ?>
      </nav>
    </div>
   </div>
</div>



