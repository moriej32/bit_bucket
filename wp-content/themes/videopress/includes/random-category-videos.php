<!-- Start Categorized post -->
    <?php
	$posts_per_page = 3;
	$page = (isset($_GET['paged'])) ? $_GET['paged'] : 1;
	$offset = ( $page - 1 );
	$args = array(
		'order_by' => 'id',
		'exclude' => array(1,23),
	);
	$categories = get_categories( $args );	
	//echo count($categories);
	$tpages = ceil(count($categories)/$posts_per_page);
	echo '<p id="noofpages" style="display:none;" class="'.$tpages.'">'.$tpages.'</p>';
	$i = 0;
	foreach($categories as $category){ 
	if($i < ( $offset + 1 ) * $posts_per_page && $i>=$offset*$posts_per_page){
		$args = array( 'numberposts' => 6, 'order'=> 'DESC', 'category'=> $category->term_id, 'exclude'=> $post->ID,'post_type'=>'post','suppress_filters'=>true );
		$postslist = get_posts( $args );
		echo '<div class="post">';
		echo '<a href="'.get_category_link($category->term_id) .'"><h5>'.$category->name.'</h5></a>';?>
		<div class="layout-3 related-videos">
			<div class="homepage-video-holder"><?php
				foreach($postslist as $post) :  setup_postdata($post);
					$videopress_gridcounter++;?> 
					<div class="home-page-scroller <?php if( $videopress_gridcounter == '1' ){ echo ' first-homepage-video'; }elseif( $videopress_gridcounter == '6' ){ echo ' last'; } ?>">
						<div class="image-holder">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item"></div>
							</a><?php
                            if( has_post_thumbnail() ){
									the_post_thumbnail('medium-thumb', array( 'class' => 'layout-3-thumb' ) );
							}else{
									echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb">';
							}?>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
						<ul class="stats">
							<li><?php videopress_displayviews( get_the_ID() ); ?></li>
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
						</ul>
						<div class="clear"></div>
					</div>
			<?php endforeach; wp_reset_query(); ?>
			<div class="clear"></div>
			</div><!-- End Categorized Post -->
		</div> <!-- end -->
		<div class="scroll-left scroll-left_1"><span class="icon-arrow-left"> </span></div>
		<div class="scroll-right scroll-right_1"><span class="icon-arrow-right"> </span></div>
		<div class="spacing-40"></div>
		</div>
	<?php }
		$i++;
	} 
	unset( $category );
	echo '<div id="nav-below">';
	echo paginate(site_url(),$page,$tpages);
	echo '</div>';