<script>
$(document).ready(function()
{
var figure = $(".video-clase").hover( hoverVideo, hideVideo );

function hoverVideo(e) { 
	var Video = $('Video Element')[0];
	$('video',this).parent().find("img").css("display","none");
	$('video',this).css("display","block");
	$('video',this).on('timeupdate',function(e)
	{
		current_time = e.currentTarget.currentTime;
		console.log($(this).get(0).currentTime);
		if(current_time > 5)
		{
			$(this).get(0).pause();
			$(this).get(0).currentTime = 0;
			$(this).get(0).play();
		}
	});
    $('video', this).get(0).play(); 
}
function hideVideo(e) {
	$('video',this).parent().find("img").css("display","inline-block");
	$('video',this).css("display","none");
    $('video', this).get(0).pause();
    console.log($('video', this).currentTime);
    $('video',this).on('timeupdate',function(e)
	{
    	$(this).currentTime = 0;
	});
}
});

</script>
<style >
	
	.thevideo{

		width: 250px;
		height: 165px;
	}
	.thevideo{ -webkit-transition:all 0.4s ease-in-out; -moz-transition:all 0.4s ease-in-out; -o-transition:all 0.4s ease-in-out; transition:all 0.4s ease-in-out;}
	.video-clase .thevideo{ transform:scale(0); }
	.video-clase:hover .thevideo{ transform:scale(1.09); box-shadow:0 0 7px #666; z-index: 3;}
</style>
