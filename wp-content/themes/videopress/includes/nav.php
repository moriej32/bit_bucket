<?php if( has_nav_menu('header_menu') ){

	wp_nav_menu( array(
		  	'theme_location' => 'header_menu',
			'container' => 'false',
			'items_wrap' => '<ul class="sf-menu">%3$s</ul>',
	));

}else{ ?>

<p style="float:left; margin-top:15px; margin-right:15px; color:#FFFFFF; text-transform:uppercase">Please Setup your nav menu</p>

<?php } ?>