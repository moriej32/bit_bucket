<?php 

// Profile Photo

 //profile id
	$profile_id = 12345;

/***********************************************************
	0 - Remove The Temp image if it exists
***********************************************************/
	/* if (!isset($_POST['x']) && !isset($_FILES['image']['name']) ){
		//Delete users temp image
			$temppath = '/var/www/html/bitbucket/profile_pic/'.$profile_id.'_temp.jpeg';
			if (file_exists ($temppath)){ @unlink($temppath); }
	}  */

if (isset($_POST['profileType']) && $_POST['profileType'] == '111' )
{
	
	

if(isset($_FILES['image']['name'])){	  
	/***********************************************************
		1 - Upload Original Image To Server
	***********************************************************/	
		//Get Name | Size | Temp Location		    
			$ImageName = $_FILES['image']['name'];
			$ImageSize = $_FILES['image']['size'];
			$ImageTempName = $_FILES['image']['tmp_name'];
		//Get File Ext   
			$ImageType = @explode('/', $_FILES['image']['type']);
			$type = $ImageType[1]; //file type	
		//Set Upload directory    
			$uploaddir = '/var/www/html/bitbucket/profile_pic';
		//Set File name	
			$file_temp_name = $profile_id.'_original.'.md5(time()).'n.'.$type; //the temp file name
			$fullpath = "$uploaddir/".$file_temp_name; // the temp file path
			$file_name = $profile_id.'_temp.jpeg'; //$profile_id.'_temp.'.$type; // for the final resized image
			$fullpath_2 = "$uploaddir/".$file_name; //for the final resized image
		//Move the file to correct location
			$move = move_uploaded_file($ImageTempName ,$fullpath) ; 
			chmod($fullpath, 0777);  
   		//Check for valid uplaod
			if (!$move) { 
				die ('File didnt upload');
			} else { 
				$imgSrc= 'http://182.75.35.84:83/bitbucket/profile_pic/'.$file_name.'?x='.rand(); // the image to display in crop area
				$msg= "Upload Complete!";  	//message to page
				$src = $file_name;	 		//the file name to post from cropping form to the resize		
			} 

	/***********************************************************
		2  - Resize The Image To Fit In Cropping Area
	***********************************************************/		
			//get the uploaded image size	
				clearstatcache();				
				$original_size = getimagesize($fullpath);
				$original_width = $original_size[0];
				$original_height = $original_size[1];	
			// Specify The new size
				$main_width = 500; // set the width of the image
				$main_height = $original_height / ($original_width / $main_width);	// this sets the height in ratio									
			//create new image using correct php func			
				if($_FILES["image"]["type"] == "image/gif"){
					$src2 = imagecreatefromgif($fullpath);
				}elseif($_FILES["image"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/pjpeg"){
					$src2 = imagecreatefromjpeg($fullpath);
				}elseif($_FILES["image"]["type"] == "image/png"){ 
					$src2 = imagecreatefrompng($fullpath);
				}else{ 
					$msg .= "There was an error uploading the file. Please upload a .jpg, .gif or .png file. <br />";
				}
			//create the new resized image
				$main = imagecreatetruecolor($main_width,$main_height);
				imagecopyresampled($main,$src2,0, 0, 0, 0,$main_width,$main_height,$original_width,$original_height);
			//upload new version
				$main_temp = $fullpath_2;
				imagejpeg($main, $main_temp, 90);
				chmod($main_temp,0777);
			//free up memory
				imagedestroy($src2);
				imagedestroy($main);
				@imagedestroy($fullpath);
				@ unlink($fullpath); // delete the original upload					
									
}//ADD Image 	

}
	


/***********************************************************
	3- Cropping & Converting The Image To Jpg
***********************************************************/
if (isset($_POST['profileCropType']) && $_POST['profileCropType'] == '222' )
	
{
	

	if (isset($_POST['x1']) && $_POST['x1']) {
		
		//echo 'Hello Ok this is testing'; die("Fdfsfsdf");
		
		//the file type posted
			$type = $_POST['type1'];	
		//the image src
						
			$src = '/var/www/html/bitbucket/profile_pic/'.$_POST['src1'];	
								
			$finalname = md5(time());	
		
		if($type == 'jpg' || $type == 'jpeg' || $type == 'JPG' || $type == 'JPEG'){	
		
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;
			//create a cropped copy of the image
				$img_r = imagecreatefromjpeg($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],
				$targ_w,$targ_h,$_POST['w1'],$_POST['h1']);
			//save the new cropped version				
			
				$user_id = get_current_user_id(); 
				$meta_key = 'profile_pic_meta';	
				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg'); 
							
				imagejpeg($dst_r, "/var/www/html/bitbucket/profile_pic/".$finalname."n.jpeg", 90); 	
					
				
		}else if($type == 'png' || $type == 'PNG'){
			
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;
			//create a cropped copy of the image
				$img_r = imagecreatefrompng($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );		
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],
				$targ_w,$targ_h,$_POST['w1'],$_POST['h1']);
				
				$user_id = get_current_user_id(); 
				$meta_key = 'profile_pic_meta';	
				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg'); 
					
			//save the new cropped version
				imagejpeg($dst_r, "/var/www/html/bitbucket/profile_pic/".$finalname."n.jpeg", 90); 	
				
				
						
		}else if($type == 'gif' || $type == 'GIF'){
			
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;
			//create a cropped copy of the image
				$img_r = imagecreatefromgif($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );		
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],
				$targ_w,$targ_h,$_POST['w1'],$_POST['h1']);
				
				
				$user_id = get_current_user_id(); 
				$meta_key = 'profile_pic_meta';	
				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg'); 
				
			//save the new cropped version
				imagejpeg($dst_r, "/var/www/html/bitbucket/profile_pic/".$finalname."n.jpeg", 90); 	
			
		}
			//free up memory
				imagedestroy($img_r); // free up memory
				imagedestroy($dst_r); //free up memory
				@ unlink($src); // delete the original upload					
			
			//return cropped image to page	
			$displayname ="http://182.75.35.84:83/bitbucket/profile_pic/".$finalname."n.jpeg";
															
	} // post x
	
}
?>


<script>
 
//JCrop Bits
  jquery(function(){
	  
    jquery('#jcrop_target').Jcrop({
      aspectRatio: 1,
	  setSelect:   [ 200,200,37,49 ],
      onSelect: updateCoords
    });

  });

  function updateCoords(c)
  {
    jquery('#x').val(c.x);
    jquery('#y').val(c.y);
    jquery('#w').val(c.w);
    jquery('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt(jquery('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  }; 
//End JCrop Bits

	function cancelCrop(){
		//Refresh page				
		top.location = 'index.php?cancel';
		return false;
	}
	

	jQuery(document).ready(function(){
		jQuery("#image").on("change", function() {		
			jQuery("#profileForm").submit();		
		});
		

});
</script>


<div id="Overlay" style=" width:100%; height:100%; background-color:rgba(0,0,0,.5); border:0px #990000 solid; position:absolute; top:0px; left:0px; z-index:2000; display:none;"></div>


<div id="wrapper" style="width: 917px; padding-right:10px; text-area:center; position:relative; margin:0px auto; border:1px #444 solid; background-color:#3498db;">
 
 
 <?php
		$user_id = get_current_user_id(); 
		$meta_key = 'profile_pic_meta';	
	 	$fileName = get_user_meta( $user_id, $meta_key, true ); 	
  	    $fileName1 = site_url().'/profile_pic/'.$fileName;			
	
?>	 
     
 <img src="<?php echo $fileName1; ?>" style="position:relative; float:right; width:150px; height:150px;"  />
	 


<div id="formExample">
	
    <p><b> <?php echo @$msg; ?> </b></p>
    
    <form action="" method="post" id="profileForm" enctype="multipart/form-data">
        Upload something<br /><br />
        <input type="file" id="image" name="image" style="width:200px; height:30px; background-color:#FFFFFF;" /><br />
		<input type="hidden" name="profileType" value="111" />
        <input type="submit" value="submit" style="width:85px; height:25px;" />
    
    </form><br /><br />
    
</div> <!-- Form-->  


<?php  if(isset($imgSrc)){ //if an image has been uploaded display cropping area?>
    <script>
    	jquery('#Overlay').show();
		jquery('#formExample').hide();
    </script>
    <div id="CroppingContainer" style="width:800px; max-height:600px; background-color:#FFF; position:relative; overflow:hidden; border:2px #666 solid; margin:50px auto; z-index:2001; padding-bottom:0px;">  
    
        <div id="CroppingArea" style="width:500px; max-height:400px; position:relative; overflow:hidden; margin:40px 0px 40px 40px; border:2px #666 solid; float:left;">	
            <img src="<?php echo $imgSrc;?>" border="0" id="jcrop_target" style="border:0px #990000 solid; position:relative; margin:0px 0px 0px 0px; padding:0px; " />
        </div>  


            <div id="CropImageForm" style="width:100px; height:30px; float:left; margin:10px 0px 0px 40px;" >  
                <form method="post" onsubmit="return checkCoords();">
                    <input type="hidden" id="x" name="x1" />
                    <input type="hidden" id="y" name="y1" />
                    <input type="hidden" id="w" name="w1" />
                    <input type="hidden" id="h" name="h1" />
                    <input type="hidden" value="jpeg" name="type1"/>
                    <input type="hidden" value="<?php echo $src; ?>" name="src1"/>
					<input type="hidden" name="profileCropType" value="222" />
                    <input type="submit" value="CropImage" style="width:100px; height:30px;" />
                </form>
            </div>
            <div id="CropImageForm2" style="width:100px; height:30px; float:left; margin:10px 0px 0px 40px;" >  
                <form action="" method="post" onsubmit="return cancelCrop();">
                    <input type="submit" value="Cancel Crop" style="width:100px; height:30px;" />
					
                </form>
            </div>            
            
    </div><!-- CroppingContainer -->
	<?php } ?>
	
 
</div>


<!-- Cover Photo -->
	
	
<?php 

 //profile id
	$profile_id1 = 123456;

/***********************************************************
	0 - Remove The Temp image if it exists
***********************************************************/
	/* if (!isset($_POST['x']) && !isset($_FILES['image']['name']) ){
		//Delete users temp image
			$temppath = '/var/www/html/bitbucket/profile_pic/'.$profile_id.'_temp.jpeg';
			if (file_exists ($temppath)){ @unlink($temppath); }
	}  */

if (isset($_POST['coverType']) && $_POST['coverType'] == '333' )
	
{
	
if(isset($_FILES['image']['name'])){	  
	/***********************************************************
		1 - Upload Original Image To Server
	***********************************************************/	
		//Get Name | Size | Temp Location		    
			$ImageName = $_FILES['image']['name'];
			$ImageSize = $_FILES['image']['size'];
			$ImageTempName = $_FILES['image']['tmp_name'];
		//Get File Ext   
			$ImageType = @explode('/', $_FILES['image']['type']);
			$type = $ImageType[1]; //file type	
		//Set Upload directory    
			$uploaddir = '/var/www/html/bitbucket/cover_pic';
		//Set File name	
			$file_temp_name = $profile_id1.'_original.'.md5(time()).'n.'.$type; //the temp file name
			$fullpath = "$uploaddir/".$file_temp_name; // the temp file path
			$file_name = $profile_id1.'_temp.jpeg'; //$profile_id.'_temp.'.$type; // for the final resized image
			$fullpath_2 = "$uploaddir/".$file_name; //for the final resized image
		//Move the file to correct location
			$move = move_uploaded_file($ImageTempName ,$fullpath) ; 
			chmod($fullpath, 0777);  
   		//Check for valid uplaod
			if (!$move) { 
				die ('File didnt upload');
			} else { 
				$imgSrc1= 'http://182.75.35.84:83/bitbucket/cover_pic/'.$file_name.'?x='.rand(); // the image to display in crop area
				$msg= "Upload Complete!";  	//message to page
				$src = $file_name;	 		//the file name to post from cropping form to the resize		
			} 

	/***********************************************************
		2  - Resize The Image To Fit In Cropping Area
	***********************************************************/		
			//get the uploaded image size	
				clearstatcache();				
				$original_size = getimagesize($fullpath);
				$original_width = $original_size[0];
				$original_height = $original_size[1];	
			// Specify The new size
				$main_width = 500; // set the width of the image
				$main_height = $original_height / ($original_width / $main_width);	// this sets the height in ratio									
			//create new image using correct php func			
				if($_FILES["image"]["type"] == "image/gif"){
					$src2 = imagecreatefromgif($fullpath);
				}elseif($_FILES["image"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/pjpeg"){
					$src2 = imagecreatefromjpeg($fullpath);
				}elseif($_FILES["image"]["type"] == "image/png"){ 
					$src2 = imagecreatefrompng($fullpath);
				}else{ 
					$msg .= "There was an error uploading the file. Please upload a .jpg, .gif or .png file. <br />";
				}
			//create the new resized image
				$main = imagecreatetruecolor($main_width,$main_height);
				imagecopyresampled($main,$src2,0, 0, 0, 0,$main_width,$main_height,$original_width,$original_height);
			//upload new version
				$main_temp = $fullpath_2;
				imagejpeg($main, $main_temp, 90);
				chmod($main_temp,0777);
			//free up memory
				imagedestroy($src2);
				imagedestroy($main);
				@imagedestroy($fullpath);
				@ unlink($fullpath); // delete the original upload					
									
}//ADD Image 	

}	


/***********************************************************
	3- Cropping & Converting The Image To Jpg
***********************************************************/
if (isset($_POST['coverCropType']) && $_POST['coverCropType'] == '444' )
	
{

	if (isset($_POST['x1']) && $_POST['x1']) {
		
		//echo 'Hello Ok this is testing'; die("Fdfsfsdf");
		
		//the file type posted
			$type = $_POST['type1'];	
		//the image src
						
			$src = '/var/www/html/bitbucket/cover_pic/'.$_POST['src1'];	
								
			$finalname = md5(time());	
		
		if($type == 'jpg' || $type == 'jpeg' || $type == 'JPG' || $type == 'JPEG'){	
		
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;
			//create a cropped copy of the image
				$img_r = imagecreatefromjpeg($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],
				$targ_w,$targ_h,$_POST['w1'],$_POST['h1']);
			//save the new cropped version				
			
				$user_id = get_current_user_id(); 
				$meta_key = 'cover_pic_meta';	
				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg'); 
							
				imagejpeg($dst_r, "/var/www/html/bitbucket/cover_pic/".$finalname."n.jpeg", 90); 	
					
				
		}else if($type == 'png' || $type == 'PNG'){
			
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;
			//create a cropped copy of the image
				$img_r = imagecreatefrompng($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );		
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],
				$targ_w,$targ_h,$_POST['w1'],$_POST['h1']);
				
				$user_id = get_current_user_id(); 
				$meta_key = 'cover_pic_meta';	
				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg'); 
					
			//save the new cropped version
				imagejpeg($dst_r, "/var/www/html/bitbucket/cover_pic/".$finalname."n.jpeg", 90); 	
				
				
						
		}else if($type == 'gif' || $type == 'GIF'){
			
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;
			//create a cropped copy of the image
				$img_r = imagecreatefromgif($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );		
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],
				$targ_w,$targ_h,$_POST['w1'],$_POST['h1']);
				
				
				$user_id = get_current_user_id(); 
				$meta_key = 'cover_pic_meta';	
				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg'); 
				
			//save the new cropped version
				imagejpeg($dst_r, "/var/www/html/bitbucket/cover_pic/".$finalname."n.jpeg", 90); 	
			
		}
			//free up memory
				imagedestroy($img_r); // free up memory
				imagedestroy($dst_r); //free up memory
				@ unlink($src); // delete the original upload					
			
			//return cropped image to page	
			$displayname ="http://182.75.35.84:83/bitbucket/cover_pic/".$finalname."n.jpeg";
															
	} // post x
	
}
?>


<script>
 
//JCrop Bits
  jquery(function(){
	  
    jquery('#jcrop_target').Jcrop({
      aspectRatio: 1,
	  setSelect:   [ 200,200,37,49 ],
      onSelect: updateCoords
    });

  });

  function updateCoords(c)
  {
    jquery('#x').val(c.x);
    jquery('#y').val(c.y);
    jquery('#w').val(c.w);
    jquery('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt(jquery('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  }; 
//End JCrop Bits

	function cancelCrop(){
		//Refresh page				
		top.location = 'index.php?cancel';
		return false;
	}
	
	
	jQuery(document).ready(function(){
		jQuery("#image1").on("change", function() {		
			jQuery("#coverForm").submit();		
		});
	});

</script>

<div id="Overlay" style=" width:100%; height:100%; background-color:rgba(0,0,0,.5); border:0px #990000 solid; position:absolute; top:0px; left:0px; z-index:2000; display:none;"></div>


<div id="wrapper1"  style="width: 917px; padding-right:10px; text-area:center; position:relative; margin:0px auto; border:1px #444 solid; background-color:#3498db;">
 
 
 <?php
		$user_id2 = get_current_user_id(); 
		$meta_key2 = 'cover_pic_meta';	
	 	$fileName2 = get_user_meta( $user_id2, $meta_key2, true ); 	
  	    $fileName3 = site_url().'/cover_pic/'.$fileName2;			
	
?>	 
     
 <img src="<?php echo $fileName3; ?>" style="position:relative; float:right; width:150px; height:150px;"  />
	 


<div id="formExample">
	
    <p><b> <?php echo @$msg; ?> </b></p>
    
    <form action="" method="post" id="coverForm" enctype="multipart/form-data">
        Upload something<br /><br />
        <input type="file" id="image1" name="image" style="width:200px; height:30px; background-color:#FFFFFF;" /><br /><br />
		<input type="hidden" name="coverType" value="333" />
        <input type="submit" value="submit" style="width:85px; height:25px;" />
    
    </form><br /><br />
    
</div> <!-- Form-->  


<?php  if(isset($imgSrc1)){ //if an image has been uploaded display cropping area?>
    <script>
    	jquery('#Overlay').show();
		jquery('#formExample').hide();
    </script>
    <div id="CroppingContainer" style="width:800px; max-height:600px; background-color:#FFF; position:relative; overflow:hidden; border:2px #666 solid; margin:50px auto; z-index:2001; padding-bottom:0px;">  
    
        <div id="CroppingArea" style="width:500px; max-height:400px; position:relative; overflow:hidden; margin:40px 0px 40px 40px; border:2px #666 solid; float:left;">	
            <img src="<?php echo $imgSrc1;?>" border="0" id="jcrop_target" style="border:0px #990000 solid; position:relative; margin:0px 0px 0px 0px; padding:0px; " />
        </div>  


            <div id="CropImageForm" style="width:100px; height:30px; float:left; margin:10px 0px 0px 40px;" >  
                <form method="post" onsubmit="return checkCoords();">
                    <input type="hidden" id="x" name="x1" />
                    <input type="hidden" id="y" name="y1" />
                    <input type="hidden" id="w" name="w1" />
                    <input type="hidden" id="h" name="h1" />
                    <input type="hidden" value="jpeg" name="type1"/>
                    <input type="hidden" value="<?php echo $src; ?>" name="src1"/>
					<input type="hidden" name="coverCropType" value="444" />
                    <input type="submit" value="CropImage" style="width:100px; height:30px;" />
                </form>
            </div>
            <div id="CropImageForm2" style="width:100px; height:30px; float:left; margin:10px 0px 0px 40px;" >  
                <form action="" method="post" onsubmit="return cancelCrop();">
                    <input type="submit" value="Cancel Crop" style="width:100px; height:30px;"   />
                </form>
            </div>            
            
    </div><!-- CroppingContainer -->
	<?php } ?>
	
 
</div>

