<!doctype html>

<html lang="en">
<head>
<meta charset="utf-8">

<title>How-To Videos, Do It Yourself Videos, DIY Videos | Do It Yourself Nation</title>
<?php wp_head(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
<link href='<?php echo get_template_directory_uri(); ?>/css/css.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id='all'>
        <!-- header -->
        <header>
            <div id="header-container-wrapper">
                <div id="header-container">
                    <div class="spacing-40"></div>
                    <div class="container">

                        <!-- Logo -->
                        <div class="grid-3 logo-container">
                            <a href="http://doityourselfnation.org/">
                                <img alt="Logo" src="<?php echo get_template_directory_uri(); ?>/DYN-logo-white-small.png">
                            </a>
                        </div>
                        <!-- End Logo -->

                        <div class="social_media_icons">
                           <div class="social_icon"><a target="_blank" href="https://www.facebook.com/pages/Do-It-Yourself-Nation/886634901416743"><img src="<?php echo get_template_directory_uri(); ?>/social_media_icons/instagram.png"></a></div>
                           <div class="social_icon"><a target="_blank" href="https://www.facebook.com/pages/Do-It-Yourself-Nation/886634901416743"><img src="<?php echo get_template_directory_uri(); ?>/social_media_icons/facebook.png"></a></div>
                           <div class="social_icon"><a target="_blank" href="https://www.facebook.com/pages/Do-It-Yourself-Nation/886634901416743"><img src="<?php echo get_template_directory_uri(); ?>/social_media_icons/linkedin.png"></a></div>
                           <div class="social_icon"><a target="_blank" href="https://www.facebook.com/pages/Do-It-Yourself-Nation/886634901416743"><img src="<?php echo get_template_directory_uri(); ?>/social_media_icons/circle.png"></a></div>
                           <div class="social_icon"><a target="_blank" href="https://www.facebook.com/pages/Do-It-Yourself-Nation/886634901416743"><img src="<?php echo get_template_directory_uri(); ?>/social_media_icons/twitter.png"></a></div>
                           <div class="social_icon"><a target="_blank" href="https://www.facebook.com/pages/Do-It-Yourself-Nation/886634901416743"><img src="<?php echo get_template_directory_uri(); ?>/social_media_icons/youtube.png"></a></div>
                       </div>

                       <div class="clear"></div>
                       <div class="spacing-50"></div>
                   </div>
               </div> <!-- end header container -->
           </div>
       </header>
       <!-- header -->
       <!-- content -->
       <div class='p_90' style='min-height: 700px; margin: auto;'>
           <h1>NEW SITE COMING SOON. ALL NEW VIDEOS.</h1>
           <h2>Want to be the first in line to contribute?</h2>
           <h3>Sign up below and be included in our beta access!</h3>
           <h4>Special offers for early adopters.</h4>
           <div style='width:50%; margin: auto; padding-top: 100px; padding-bottom: 100px;'>
            <?php echo do_shortcode("[ninja_forms id=5]")?>
        </div>

    </div>
    <!-- content -->
    <!-- footer -->
    <footer>
        <div class="footer-container">
            <div class="container p_90">
                <div class="clear"></div>    
                <!-- Footer Copyright -->
                <div id="copyright" class="full-width">
                   <div id="copyright-text">
                     &copy; 2015 Do It Yourself Nation. All rights reserved        </div> 

                     <div id="footer-logo">
                       <a href="http://wpdev.mainstreethost.com/doityourselfnation.org/">
                        <img alt="small logo" src="<?php echo get_template_directory_uri(); ?>/DYN-logo-white-mini1.png">
                    </a>
                </div>

                <div class="clear"></div>
            </div>
            <!-- End Footer Copyright -->

            <div class="clear"></div>
        </div>
    </div>
    <?php wp_footer(); ?>
</footer>
<!-- footer -->

</div>
</body>
</html>