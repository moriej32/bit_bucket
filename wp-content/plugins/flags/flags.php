<?php
/*
Plugin Name: Display flag as inappropriate
Description: This plugin to controls flag as inappropriate video. 
Version: 1.0
Author: Wahyu Widodo
Author URI: http://www.javathemes.com
License: GPL2
*/

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

function list_table_page()
{    
    $exampleListTable = new Content_List_Table();
    echo '<div class="wrap"><h2>List Flag as Inappropriate</h2>'; 
    $exampleListTable->prepare_items();
    echo '<form id="movies-filter" method="post">';
    echo '<input type="hidden" name="page" value="'.$_REQUEST['page'].'" />';
    $exampleListTable->display();
    echo '</form>';
    echo '</div>';     
}

function my_add_menu_items(){
    add_menu_page( 'Display Flag as Inappropriate', 'Flag as Inappropriate', 'activate_plugins', 'display_flag_inappropriate', 'list_table_page' );
}
add_action( 'admin_menu', 'my_add_menu_items' );


class Content_List_Table extends WP_List_Table
{
	
    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
 
        $data = $this->table_data();
        usort( $data, array( &$this, 'sort_data' ) );
 
        $perPage = 100;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);
 
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
 
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
 
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();
        $this->items = $data;
    }
 
    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'cb'        => '<input type="checkbox">',
            'flag_ID'	=> 'ID',
            'reason'	=> 'Reason',
            'url'	=> 'Video URL',
            'flag_date'  => 'Flag Date',
            'flag_ip'    => 'IP Address'
        );
 
        return $columns;
    }
	
    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }
 
    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('flag_ID' => array('flag_ID', true), 
                    'reason' => array('reason', true),  
                    'flag_date' => array('flag_date', true));
    }
 
    /**
     * Get the table data
     *
     * @return Array
     */
    public function table_data()
    {
	global $wpdb;
           $data = array();
	$query = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."flags ORDER BY flag_ID DESC");		
	foreach($query as $myrow){
                $data[] = array(
                    'flag_ID'        	=> $myrow->flag_ID,
                    'reason'     		=> $myrow->reason,
                    'url' 		=> '<a href="'.$myrow->url.'" target="_blank" > '.$myrow->url.'</a>',
                    'flag_date' 		=> $myrow->flag_date,
                    'flag_ip'                   => $myrow->flag_ip
                );
	}		
           return $data;
    }
 
    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'flag_ID':
            case 'reason':
            case 'url':
            case 'flag_date': 
            case 'flag_ip':    
                return $item[ $column_name ];
            default:
                return print_r( $item, true ) ;
        }
    }
 
    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'flag_ID';
        $order = 'desc';
 
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
 
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
 
 
        $result = strnatcmp( $a[$orderby], $b[$orderby] );
 
        if($order === 'asc')
        {
            return $result;
        }
 
        return -$result;
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="dataid[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],
            /*$2%s*/ $item['flag_ID']
        );
    }


    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {        
      global $wpdb; 
      if(isset($_POST['dataid'])){
            if( 'delete' === $this->current_action() ) { 
               foreach($_POST['dataid'] as $id) {
                   $id = absint($id);                   
                   $wpdb->query("DELETE FROM ".$wpdb->prefix."flags WHERE flag_ID=$id");
               }
            }
      }  
    }
    
    
}


?>
