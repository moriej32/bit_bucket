<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.doityourselfnation.org
 * @since             1.0.0
 * @package           Dyn_Review
 *
 * @wordpress-plugin
 * Plugin Name:       DYN Reveiw
 * Plugin URI:        http://www.doityourselfnation.org
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            papul
 * Author URI:        http://www.doityourselfnation.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dyn-review
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dyn-review-activator.php
 */
function activate_dyn_review() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dyn-review-activator.php';
	Dyn_Review_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dyn-review-deactivator.php
 */
function deactivate_dyn_review() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dyn-review-deactivator.php';
	Dyn_Review_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dyn_review' );
register_deactivation_hook( __FILE__, 'deactivate_dyn_review' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dyn-review.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dyn_review() {

	$plugin = new Dyn_Review();
	$plugin->run();

}
run_dyn_review();
