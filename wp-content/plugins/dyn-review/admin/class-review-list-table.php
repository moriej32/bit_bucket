<?php
/**
 * Wordpress table to display reviews
 */
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
class Review_List_Table extends WP_List_Table {

   /**
    * Constructor, we override the parent to pass our own arguments
    * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
    */
    function __construct() {
       	parent::__construct( array(
    		'singular'=> __('Review', 'dyn-review'), //Singular label
      		'plural' => __('Reviews', 'dyn-review'), //plural label, also this well be one of the table css class
      		'ajax'   => false //We won't support Ajax for this table
      	) );
      	require_once dirname(__FILE__) . '/../includes/class-dyn-review-db.php';

    }

    public static function delete_review($id){
    	$db = new Dyn_Review_DB();
    	$db->delete_reviwe($id);
    }

    public static function publish_review($id){
    	$db = new Dyn_Review_DB();
    	$db->publish_review($id);
    }

    public static function pending_review($id){
    	$db = new Dyn_Review_DB();
    	$db->pending_review($id);
    }

    /**
	 * Retrieve reviews from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
	public static function get_reviews( $per_page = 5, $page_number = 1 ) {

		global $wpdb;

		$db = new Dyn_Review_DB();
		$sql = "SELECT * FROM {$wpdb->prefix}dyn_review";

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}

		$sql .= " LIMIT $per_page";

		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


		$results = $wpdb->get_results( $sql, 'ARRAY_A' );
		return $results;
	}

	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;

		$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}dyn_review";

		return $wpdb->get_var( $sql );
	}

	/**
	 * Render a column when no column specific method exists.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'review_id':
			case 'review':
			case 'user_name':
			case 'review_status':
				return $item[ $column_name ];
			case 'rating':
				$output = "<div class='dyn_rating_bar'>";
				$output .= "<div class='dyn_rating' style='width:" . $item['total_rating'] . "%'>";
				$output .= "</div>";
				$output .= "</div>";
				return $output;
			default:
			return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}

	/** Text displayed when no Review is available */
	public function no_items() {
		_e( 'No Reviews avaliable.', 'dyn-review' );
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-change[]" value="%s" />', $item['review_id']
		);
	}

    /**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
		//$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'reviews_per_page', 5 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );


		$this->items = self::get_reviews( $per_page, $current_page );
	}

    /**
	 * Decide which columns to activate the sorting functionality on
	 * @return array $sortable, the array of columns that can be sorted by the user
	 */
	public function get_sortable_columns() {
		return $sortable = array(
			'review_id' => array('review_id', true),
			'review_status' => array('review_status', true),
			'user_name' => array('user_name', true)
		);
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'Delete',
			'bulk-publish' => 'Mark as Published',
			'bulk-pending' => 'Mark as Pending'
		];

		return $actions;
	}

	public function process_bulk_action() {
		//Detect when a bulk action is being triggered..
		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) ) || ( isset( $_POST['action2'] ) ) ) {
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );
			if ( ! wp_verify_nonce( $nonce, 'bulk-' . $this->_args['plural'] ) ) {
		      	die( 'Unauthorized attempt to change the data' );
		    }
			if( $_POST['action'] == 'bulk-delete' || $_POST['action2'] == 'bulk-delete') {
				$delete_ids = esc_sql( $_POST['bulk-change'] );

				// loop over the array of record IDs and delete them
				foreach ( $delete_ids as $id ) {
				  self::delete_review( $id );

				}

			}

			if( $_POST['action'] == 'bulk-publish' || $_POST['action2'] == 'bulk-publish') {
				$publish_ids = esc_sql( $_POST['bulk-change'] );

				// loop over the array of record IDs and delete them
				foreach ( $publish_ids as $id ) {
				  self::publish_review( $id );

				}

			}

			if( $_POST['action'] == 'bulk-pending' || $_POST['action2'] == 'bulk-pending') {
				$pending_ids = esc_sql( $_POST['bulk-change'] );

				// loop over the array of record IDs and delete them
				foreach ( $pending_ids as $id ) {
				  self::pending_review( $id );
				}

				//wp_redirect( esc_url( add_query_arg() ) );
				//exit;
			}

		}
	}

    /**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
			'review_id'    => __( 'Review Id', 'dyn-review' ),
			'review' => __( 'review', 'dyn-review' ),
			'user_name'    => __( 'User Name', 'dyn-review' ),
			'review_status' => __( 'Review Status', 'dyn-review'),
			'rating' => __( 'Rating', 'dyn-review')
		];

		return $columns;
	}

 //    function extra_tablenav( $which ) {
	//    if ( $which == "top" ){
	//       //The code that goes before the table is here
	//       echo"Hello, I'm before the table";
	//    }
	//    if ( $which == "bottom" ){
	//       //The code that goes after the table is there
	//       echo"Hi, I'm after the table";
	//    }

	// }
}