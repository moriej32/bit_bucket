(function( $ ) {
	'use strict';

	$(document).ready(function(){
		function rating_optimizer(rating){
			var divider = rating/20;
			divider = Math.ceil(divider);
			rating = divider * 20;
			
			return rating;
		}

		function get_data_single(submit){
			var review_box = submit.parent('.review_box');
			var data = {};
			data.rating_type = review_box.data('type');
			data.post_id = review_box.data('id');
			data.review = review_box.find(".review_text").val();
			data.errortext = "";
			data.user_id = review_box.find(".review_user_info").data("user_id");
			if(data.user_id == 0){
				data.user_name = review_box.find(".review_user_info .review_user_name").val();
				if(!$.trim(data.user_name)){
					data.errortext = "Please Enter your name";
					console.log(data);
					return data;
				}
			}
			var rating = review_box.find('.review_single .dyn_rating_bar').data('rating');
			if(rating<=0){
				data.errortext = "Please Enter all the rating";
				return data;
			}
			data.rating = rating;

	 		return data;
	 	}

		function get_data(submit){
			var review_box = submit.parent('.review_box');
			var data = {};
			data.rating_type = review_box.data('type');
			data.post_id = review_box.data('id');
			data.review = review_box.find(".review_text").val();
			data.ratings = [];
			data.errortext = "";
			data.user_id = review_box.find(".review_user_info").data("user_id");
			if(data.user_id == 0){
				data.user_name = review_box.find(".review_user_info .review_user_name").val();
				if(!$.trim(data.user_name)){
					data.errortext = "Please Enter your name";
					console.log(data);
					return data;
				}
			}
			review_box.find('.review_category .category_rating').each(function(){
				var rating = {};
				rating.cat_name = $(this).children('.dyn_rating_bar').data('category');
				rating.rating = $(this).children('.dyn_rating_bar').data('rating');
				if(rating.rating <= 0){
					data.errortext = "Please Enter all the rating";
					return data;
				}
				data.ratings.push(rating);
			});

	 		return data;
	 	}

	 	/**** Helpful Button Section Start ****/
	 	$(".dyn_review_positive, .dyn_review_negative").click(function(){
	 		var data = {};
	 		var helpful = $(this);
	 		data.review_id = $(this).parents(".dyn_review").data("review_id");
	 		if ($(this).is('.dyn_review_positive') )
	 			data.helpful = 1;
	 		else
	 			data.helpful = 0;
	 		data.action = 'dyn_save_review_helpful';
	 		$.post(ajax_object.ajax_url, data, function(response) {
				if(response.status == 'success'){
					var count_element = helpful.find(".helpful-count");
					var count = count_element.data("count");
					count = count+1;
					count_element.html("(" + count + ")")
					count_element.data('count', count);
					helpful.parents(".review_helpful").children(".dyn_review_positive, .dyn_review_negative").each(function(){
						$(this).attr('disabled','disabled');
					});
				}
				else{
					alert(response.message);
				}
				//helpful.parents(".review_helpful").addClass("alert alert-success").html("Thanks for your input");
			}, 'json');
	 	});

	 	/**** Helpful Button Section End ****/

		$(".review_submit").click(function(){
			//If all the categories instead of single review, use get_data() instead
			var review_data = get_data_single($(this));
			if(review_data.errortext){
				$(".review_error").html(review_data.errortext).show();
				return;
			}
			var data = {
				'action' : 'dyn_submit_review',
				'review_data' : review_data
			};
			var box = $(this).closest('.review_box');
			box.html("<div class='alert alert-info'>Submitting your review. Please wait</div>");
			$.post(ajax_object.ajax_url, data, function(response) {
				box.prev('.add_dyn_review').hide();
				box.html("<div class='alert alert-success'>Your review has been submitted</div>");
				setTimeout(function(){
					box.remove();
				}, 2000);
			});
		});

		$(".add_dyn_review").click(function(){
		 	//$("body").addClass("overlay");
		 	$(this).next('.review_box').show();
		});

		$('.review_close').click(function(){
			$(this).closest('.review_box').hide();
			$("body").removeClass("overlay");
		});

		$('.category_rating .dyn_rating_bar, .review_single .dyn_rating_bar').click(function(e){
			var offset = $(this).offset();
			var relativeX = e.pageX - offset.left;
			var width = (relativeX*100)/$(this).width();
			width = rating_optimizer(width);
			var style = width + "%";
			$(this).children('.dyn_rating').css('width', style);
			$(this).data('rating', width);
		});

		/*$(".review_sort_form .review_sort_type").change(function(){
			$(this).parents('.review_sort_form').submit();
		});*/

		$(".dyn_open_review_box").click(function(){
			$(this).next('.dyn_all_reviews').show();
		});

		$(".dyn_close_review_box").click(function(){
			$(this).parent('.dyn_all_reviews').hide();
		})

		$(".dyn_load_more").click(function(){
			var data = {
				'offset' : $(this).data('offset'),
				'post_id' : $(this).data('post_id'),
				'rating_type' : $(this).data('rating_type'),
				'action' : 'dyn_load_more'
			}
			var load_object = $(this);

			$.post(ajax_object.ajax_url, data, function(response) {
				load_object.before(response.reviews);
				if(response.have_more=="true"){
					load_object.data('offset', response.offset);
				}
				else{
					load_object.removeClass();
					load_object.html("No more Reviews");
				}
			}, 'json');
		});
		 
		 $(document).on('change', '.review_sort_form .review_sort_type', function() {
			//$(this).parents('.review_sort_form').submit();
			console.log();
			 var optVal= $("#review_sort_type option:selected").val();
			 var nextlimit = 6;
			 // alert(optVal);
			 $.ajax({
									url: admin_ajax,
									type: "POST",
									data: {action : 'dyn_review_by_type','review_sort_type':optVal},
									success : function(response){
								   // alert(data);	
									$('#allreview').html(response);
									$("#numreview").val(nextlimit);
									$('.dyn_load_more_home').fadeIn();
								 }
								})	
			
			
		});
		
		$(".dyn_load_more_home").click(function(){
			var optVal= $("#review_sort_type option:selected").val();
			var limit= $("#numreview").val();
			var data = {
				'offset' : limit,
				'review_sort_type' : optVal,
				'action' : 'load_more_reviews_home'
			}
			var load_object = $(this);

			$.post(ajax_object.ajax_url, data, function(response) {
				$('#allreview').append(response.reviews);
				if(response.have_more=="true"){
					
					$("#numreview").val( response.nextlimit);
				}
				else{
					//load_object.removeClass();
					//dyn_load_more_home
					$('.dyn_load_more_home').fadeOut();
					//$('#allreview').after("No more Reviews");
				}
			}, 'json');
		});
		
		
	});



})( jQuery );
