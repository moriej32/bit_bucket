<?php
class Dyn_Review_DB{
	/**
	 * Review Table
	 **/
	private $review_table;

	/**
	 * Rating Table
	 **/
	private $rating_table;

	/**
	 * Category Table
	 **/
	private $category_table;

	/**
	 * Initialize Table Names
	 *
	 **/
	function __construct() {
		global $wpdb;
		include_once dirname(__FILE__) . '/class-dyn-review-helper.php';

		$this->review_table = $wpdb->prefix . 'dyn_review';
		$this->rating_table = $wpdb->prefix . 'dyn_rating';
		$this->category_table = $wpdb->prefix . 'dyn_rating_category';
		$this->helpful_table = $wpdb->prefix . 'dyn_review_helpful';
	}

	public function get_post_id_from_review_id($review_id) {
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare(
					"SELECT post_id FROM {$this->review_table}
					 WHERE review_id = %d
					",
					$review_id
			));
	}

	public function count_positive_helpful($review_id){
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare(
					"SELECT COUNT(*) FROM {$this->helpful_table}
					 WHERE review_id = %d
					 AND helpful = 1
					",
					$review_id
			));
	}

	public function count_negative_helpful($review_id){
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare(
					"SELECT COUNT(*) FROM {$this->helpful_table}
					 WHERE review_id = %d
					 AND helpful = 0
					",
					$review_id
			));
	}

	public function save_review_helpful($review_id, $user_id, $helpful){
		global $wpdb;

		$data = array(
					"user_id" => $user_id,
					"review_id" => $review_id,
					"helpful" => $helpful,
					"date" => date('Y-m-d H:i:s')
				);
		$format = array(
					"%d",
					"%d",
					"%d",
					"%s"
				);
		if( $id = $this->review_helpful_exist($review_id, $user_id) ){
			return false;
		}

		return $wpdb->insert($this->helpful_table, $data, $format);
	}

	public function review_helpful_exist($review_id, $user_id){
		global $wpdb;

		if($user_id == 0)
			return false;

		$id = $wpdb->get_var( $wpdb->prepare( 
				"
					SELECT review_helpful_id 
					FROM {$this->helpful_table} 
					WHERE review_id = %d
					AND user_id = %d
				",
				$review_id,
				$user_id
				) );

		if( $id )
			return $id;
		return false;
	}

	public function number_of_post_review($post_id, $rating_type='post') {
		global $wpdb;

		return $wpdb->get_var($wpdb->prepare(
					"SELECT COUNT(*) FROM {$this->review_table}
					 WHERE post_id = %d
					 AND rating_type = %d
					 AND review_status = 'published'
					",
					$post_id,
					DYN_REVIEW_HELPER::rating_type($rating_type)
			));
	}

	function get_raw_rating($post_id, $rating_type='post', $rating='five'){
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare(
									"SELECT review_id, review, user_name, review_status FROM {$this->review_table}
									WHERE rating_type= %d
									AND post_id= %d
									",
									DYN_REVIEW_HELPER::rating_type($rating_type),
									$post_id			
								)
					);
	}

	public function get_ratings($review_id) {
		global $wpdb;

		return $wpdb->get_results($wpdb->prepare(
						"
							SELECT * FROM {$this->rating_table}
							LEFT JOIN {$this->category_table}
							ON cat_id = rating_category_id
							WHERE review_id = %d
						",
						$review_id
					));
	}

	function get_combined_rating_for_post($post_id, $rating_type) {
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare(
						"
						SELECT rating from {$this->rating_table}
						LEFT JOIN {$this->review_table}
						ON {$this->rating_table}.review_id = {$this->review_table}.review_id
						WHERE post_id = %d
						AND rating_type = %d
						",
						$post_id,
						DYN_REVIEW_HELPER::rating_type($rating_type)
					));
		$count = count($results);
		$sum = 0;
		foreach($results as $result){
			$sum += $result->rating;
		}
		return ($sum/$count);
	}

	public function get_reviews($post_id, $rating_type, $sort_type, $limit, $offset) {
		global $wpdb;

		if($sort_type == 'recent'){
			$orderby = 'date';
		}
		else{
			$orderby = 'total_rating';
		}

		$results = $wpdb->get_results( $wpdb->prepare(
						"
						SELECT * from {$this->review_table}
						WHERE post_id = %d
						AND rating_type = %d
						AND review_status = 'published'
						ORDER BY $orderby desc
						LIMIT %d
						OFFSET %d
						",
						$post_id,
						DYN_REVIEW_HELPER::rating_type($rating_type),
						$limit,
						$offset
					));

		return $results;
	}

	public function get_review_count($post_id, $rating_type) {
		global $wpdb;

		$results = $wpdb->get_var( $wpdb->prepare(
						"
						SELECT COUNT(*) from {$this->review_table}
						WHERE post_id = %d
						AND rating_type = %d
						AND review_status = 'published'
						",
						$post_id,
						DYN_REVIEW_HELPER::rating_type($rating_type)
					));

		return $results;
	}

	public function get_rating_for_post($post_id, $rating_type){
		global $wpdb;
		$results = $wpdb->get_results( $wpdb->prepare(
						"
						SELECT * from {$this->review_table}
						WHERE post_id = %d
						AND rating_type = %d
						AND review_status = 'published'
						",
						$post_id,
						DYN_REVIEW_HELPER::rating_type($rating_type)
					));
		$count = 0;
		$sum = 0;
		foreach($results as $review){
			$sum += $review->total_rating;
			$count++;
		}
		if($count == 0)
			return 0;
		return ceil($sum/$count);
	}

	public function get_rating_count($profile_id, $sort_type) {
		global $wpdb;
		
		$type = '';
		if($sort_type == 'post'){
			$type = 'AND post_type = "post"';
		}
		elseif($sort_type == 'books'){
			$type = 'AND post_type = "dyn_book"';
		}
		elseif($sort_type == 'product'){
			$type = 'AND post_type = "product"';
		}
		elseif($sort_type == 'file'){
			$type = 'AND post_type = "dyn_file"';
		}
		
		$results = $wpdb->get_results( $wpdb->prepare(
						"
							(SELECT COUNT(*) as count FROM {$this->review_table}
							LEFT JOIN {$wpdb->prefix}user_files ON post_id = id 
							WHERE {$wpdb->prefix}user_files.user_id = %d
							AND review_status = 'published'
							)
							UNION
							(SELECT COUNT(*) as count FROM {$this->review_table}
							LEFT JOIN {$wpdb->posts} ON post_id = {$wpdb->posts}.ID
							WHERE post_author = %d
							AND review_status = 'published'
							$type
							)
						",
						$profile_id,
						$profile_id
				));
		$count = 0;
		foreach($results as $result){
			$count += $result->count;
		}

		return $count;
	}

	public function get_sort_reviews($profile_id, $sort_type, $limit=5, $offset=0 ) {
		global $wpdb;

		/*if($sort_type == 'recent'){
			$orderby = 'date';
			$ascdesc = 'desc';
		}
		elseif($sort_type == 'low_rated'){
			$orderby = 'total_rating';
			$ascdesc = 'asc';
		}
		else{
			$orderby = 'total_rating';
			$ascdesc = 'desc';
		}*/
		
		 $type = '';
		if($sort_type == 'post'){
			$orderby = 'date';
			$ascdesc = 'desc';
			$type = 'AND post_type = "post"';
		}
		elseif($sort_type == 'books'){
			$orderby = 'total_rating';
			$ascdesc = 'asc';
			$type = 'AND post_type = "dyn_book"';
		}
		elseif($sort_type == 'product'){
			$orderby = 'total_rating';
			$ascdesc = 'desc';
			$type = 'AND post_type = "product"';
		}
		elseif($sort_type == 'file'){
			$orderby = 'total_rating';
			$ascdesc = 'desc';
			$type = 'AND post_type = "dyn_file"';
		}

		$result = $wpdb->get_results( $wpdb->prepare(
						"
							(SELECT {$this->review_table}.* FROM {$this->review_table}
							LEFT JOIN {$wpdb->prefix}user_files ON post_id = id 
							WHERE {$wpdb->prefix}user_files.user_id = %d
							AND review_status = 'published'
							)
							UNION
							(SELECT {$this->review_table}.* FROM {$this->review_table}
							LEFT JOIN {$wpdb->posts} ON post_id = {$wpdb->posts}.ID
							WHERE post_author = %d
							AND review_status = 'published'
							$type
							)
							ORDER BY $orderby $ascdesc
							LIMIT %d
							OFFSET %d

						",
						$profile_id,
						$profile_id,
						$limit,
						$offset
				));

		return $result;
	}
	
	public function get_sort_reviews_channel_home_by($profile_id, $sort_type, $limit=5 ) {
		global $wpdb;

		
		 $type = '';
		if($sort_type == 'post'){
			$orderby = 'date';
			$ascdesc = 'desc';
			$type = 'AND post_type = "post"';
		}
		elseif($sort_type == 'books'){
			$orderby = 'total_rating';
			$ascdesc = 'asc';
			$type = 'AND post_type = "dyn_book"';
		}
		elseif($sort_type == 'product'){
			$orderby = 'total_rating';
			$ascdesc = 'desc';
			$type = 'AND post_type = "product"';
		}
		elseif($sort_type == 'file'){
			$orderby = 'total_rating';
			$ascdesc = 'desc';
			$type = 'AND post_type = "dyn_file"';
		}

		$result = $wpdb->get_results( $wpdb->prepare(
						"
							(SELECT {$this->review_table}.* FROM {$this->review_table}
							LEFT JOIN {$wpdb->prefix}user_files ON post_id = id 
							WHERE {$wpdb->prefix}user_files.user_id = %d
							AND review_status = 'published'
							)
							UNION
							(SELECT {$this->review_table}.* FROM {$this->review_table}
							LEFT JOIN {$wpdb->posts} ON post_id = {$wpdb->posts}.ID
							WHERE post_author = %d
							AND review_status = 'published'
							 $type
							)
							ORDER BY $orderby $ascdesc
							LIMIT %d, 5

						",
						$profile_id,
						$profile_id,
						$limit
				));

		return $result;
	}
	
	public function get_rating_count_channel_by_ajax($profile_id, $sort_type) {
		global $wpdb;
		 $type = '';
		if($sort_type == 'post'){
			$type = 'AND post_type = "post"';
		}
		elseif($sort_type == 'books'){
			$type = 'AND post_type = "dyn_book"';
		}
		elseif($sort_type == 'product'){
			$type = 'AND post_type = "product"';
		}
		elseif($sort_type == 'file'){
			$type = 'AND post_type = "dyn_file"';
		}
		$results = $wpdb->get_results( $wpdb->prepare(
						"
							(SELECT COUNT(*) as count FROM wp_dyn_review
							LEFT JOIN wp_user_files ON post_id = id 
							WHERE wp_user_files.user_id = %d
							AND review_status = 'published'
							)
							UNION
							(SELECT COUNT(*) as count FROM wp_dyn_review
							LEFT JOIN {$wpdb->posts} ON post_id = {$wpdb->posts}.ID
							WHERE post_author = %d
							AND review_status = 'published'
							$type
							)
						",
						$profile_id,
						$profile_id
				));
		$count = 0;
		foreach($results as $result){
			$count += $result->count;
		}

		return $count;
	}
	
	public function get_sort_reviews_channel($profile_id, $sort_type, $limit=5, $offset=0 ) {
		global $wpdb;

		if($sort_type == 'recent'){
			$orderby = 'date';
			$ascdesc = 'desc';
		}
		elseif($sort_type == 'low_rated'){
			$orderby = 'total_rating';
			$ascdesc = 'asc';
		}
		else{
			$orderby = 'total_rating';
			$ascdesc = 'desc';
		}
		

		$result = $wpdb->get_results( $wpdb->prepare(
						"
							(SELECT {$this->review_table}.* FROM {$this->review_table}
							LEFT JOIN {$wpdb->prefix}user_files ON post_id = id 
							WHERE {$wpdb->prefix}user_files.user_id = %d
							AND review_status = 'published'
							)
							UNION
							(SELECT {$this->review_table}.* FROM {$this->review_table}
							LEFT JOIN {$wpdb->posts} ON post_id = {$wpdb->posts}.ID
							WHERE post_author = %d
							AND review_status = 'published'
							)
							ORDER BY $orderby $ascdesc
							LIMIT %d
							OFFSET %d

						",
						$profile_id,
						$profile_id,
						$limit,
						$offset
				));
		return $result;
	}

	public function get_file($post_id) {
		global $wpdb;

		return $wpdb->get_row($wpdb->prepare(
						"
							SELECT * FROM {$wpdb->prefix}user_files
							WHERE id = %d
						",
						$post_id
				));
	}

	public function save_review($review){
		global $wpdb;

		//Save the review first
		$review_id = $this->insert_review($review);
		//Then save the ratings with this id
		$count = 0;
		$sum = 0;
		foreach($review['ratings'] as $rating){
			$sum += $rating['rating'];
			$count++;
			$this->insert_rating($rating, $review_id);
		}
		$rating_sum = ceil($sum/$count);
		$this->update_total_rating($review_id, $rating_sum);
	}

	public function update_total_rating($review_id, $rating) {
		global $wpdb;

		$data = array( 'total_rating' => $rating );
		$format = array( '%d' );
		$where = array( 
					'review_id' => $review_id 
				);
		$where_format = array( '%d' );

		$wpdb->update($this->review_table, $data, $where, $format, $where_format);
	}

	public function delete_review($review_id) {
		global $wpdb;

		$where = array('review_id' => $review_id);
		$where_format = array( '%d' );
		$wpdb->delete($this->rating_table, $where, $where_format);
		$wpdb->delete($this->review_table, $where, $where_format);
	}

	public function publish_review($review_id){
		global $wpdb;

		$data = array( 'review_status' => 'published' );
		$format = array( '%s' );
		$where = array( 'review_id' => $review_id );
		$where_format = array( '%d' );

		$wpdb->update($this->review_table, $data, $where, $format, $where_format);
	}

	public function pending_review($review_id){
		global $wpdb;

		$data = array( 'review_status' => 'pending' );
		$format = array( '%s' );
		$where = array( 'review_id' => $review_id );
		$where_format = array( '%d' );

		$wpdb->update($this->review_table, $data, $where, $format, $where_format);
	}

	public function insert_rating($rating, $review_id){
		global $wpdb;

		$data = array(
					"cat_id" => $this->get_category_id($rating['cat_name']),
					"review_id" => $review_id,
					"rating" => $rating['rating']
				);
		$format = array(
					"%d",
					"%d",
					"%d"
				);
		$wpdb->insert($this->rating_table, $data, $format);
		return $wpdb->insert_id;
	}

	public function insert_review($review) {
		global $wpdb;

		$data = array(
					"review" => $review['review'],
					"post_id" => $review['post_id'],
					"user_id" => $review['user_id'],
					"user_name" => $review['user_name'],
					"date" => date('Y-m-d H:i:s'),
					"review_status" => $review['review_status'],
					"rating_type" => DYN_REVIEW_HELPER::rating_type($review['rating_type'])
				);
		$format = array(
					"%s",
					"%d",
					"%d",
					"%s",
					"%s",
					"%s",
					"%d"
				);
		$wpdb->insert($this->review_table, $data, $format);
		return $wpdb->insert_id;
	}

	public function get_categories(){
		global $wpdb;

		return $wpdb->get_results(
					"
					SELECT * FROM {$this->category_table}
					"
				);
	}

	public function get_category_id($cat_name){
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare(
							"
								SELECT rating_category_id
								FROM {$this->category_table}
								WHERE name = %s
							",
							$cat_name
						)
				);
	}

	public function insert_category($name){
		global $wpdb;

		if( $id = $this->category_exists( $name ))
			return $id;
		$data = array(
					"name" => $name,
				);
		$format = array(
					"%s",
				);
		$wpdb->insert($this->category_table, $data, $format);
		return $wpdb->insert_id;
	}

	public function category_exists($name){
		global $wpdb;

		$id = $wpdb->get_var( $wpdb->prepare( 
				"
					SELECT rating_category_id 
					FROM {$this->category_table} 
					WHERE name = %s
				",
				$name
				) );

		if( $id )
			return $id;
		return false;
	}

	public function user_already_reviewed($post_id, $rating_type, $user_id){
		global $wpdb;

		$result = $wpdb->query($wpdb->prepare(
						"SELECT * FROM {$this->review_table}
						WHERE post_id = %d
						AND rating_type = %d
						AND user_id = %d
						",
						$post_id,
						DYN_REVIEW_HELPER::rating_type($rating_type),
						$user_id
				));

		return $result;
	}

};