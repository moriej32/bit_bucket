<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Review
 * @subpackage Dyn_Review/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dyn_Review
 * @subpackage Dyn_Review/includes
 * @author     papul <28papul@gmail.com>
 */
class Dyn_Review_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
					
		if (function_exists('is_multisite') && is_multisite()) {
			// check if it is a network activation - if so, run the activation function for each blog id
			if ($networkwide) {
				$old_blog = $wpdb->blogid;
				// Get all blog ids
				$blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);
					self::activate_per_site();
				}
				switch_to_blog($old_blog);
				return;
			}	
		}
		self::activate_per_site();
	}

	public static function activate_per_site(){
		self::create_review_table();
		self::create_category_table();
		self::create_rating_table();
		self::create_review_positive_negative_table();
		//self::create_cache_table();
		self::default_options();
	}

	public static function default_options(){
		$plugin_name = 'dyn-review';
		include_once dirname(__FILE__) . '/class-dyn-review-helper.php';
		include_once dirname(__FILE__) . '/class-dyn-review-db.php';
		$db = new Dyn_Review_DB();

		$categories = array(
						"Visual Quality",
						"Easy to Follow",
						"Duplication",
						"Content Quality",
						"Description"
					);

		foreach($categories as $category){
			$db->insert_category($category);
		}

		$review = array(
				"post_id" => 1,
				"review" => "This is a good review",
				"user_id" => 1,
				"user_name" => get_userdata(1)->user_nicename,
				"rating_type" => "post",
				"review_status" => "published",
				"ratings" => array(
					array(
						"cat_name" => "Visual Quality",
						"rating" => 100
					),
					array(
						"cat_name" => "Easy to Follow",
						"rating" => 80
					),
					array(
						"cat_name" => "Duplication",
						"rating" => 100
					),
					array(
						"cat_name" => "Content Quality",
						"rating" => 60
					),
					array(
						"cat_name" => "Description",
						"rating" => 100
					)
				)
			);
		$db->save_review($review);

		$review = array(
				"post_id" => 1,
				"review" => "This is a good review",
				"user_id" => 0,
				"user_name" => "Guest User",
				"rating_type" => "post",
				"review_status" => "published",
				"ratings" => array(
					array(
						"cat_name" => "Visual Quality",
						"rating" => 80
					),
					array(
						"cat_name" => "Easy to Follow",
						"rating" => 100
					),
					array(
						"cat_name" => "Duplication",
						"rating" => 100
					),
					array(
						"cat_name" => "Content Quality",
						"rating" => 90
					),
					array(
						"cat_name" => "Description",
						"rating" => 90
					)
				)
			);
		$db->save_review($review);
	}

	public static function create_review_positive_negative_table(){
		include_once dirname(__FILE__) . '/class-dyn-review-helper.php';
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$type = 'review_helpful';
		$table_name = $wpdb->prefix . 'dyn_' . $type;

		$fields = array(
			array(
				"name" =>  "{$type}_id",
				"description" => "bigint(20) NOT NULL AUTO_INCREMENT",
			),
			array(
				"name" =>  "user_id",
				"description" => "bigint(20) NOT NULL",
			),
			array(
				"name" =>  "review_id",  //file/video(which is a post) id
				"description" => "bigint(20) NOT NULL",
			),
			array(
				"name" =>  "helpful",
				"description" => "TINYINT(3) UNSIGNED DEFAULT NULL",
			),
			array(
				"name" => "date",
				"description" => "DATETIME NOT NULL"
			)
		);

		$sql = DYN_REVIEW_HELPER::create_table_syntax($table_name, $fields, '', DYN_REVIEW_HELPER::get_character_set() );
		dbDelta($sql);
	}

	public static function create_category_table(){
		include_once dirname(__FILE__) . '/class-dyn-review-helper.php';
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$type = 'rating_category';
		$table_name = $wpdb->prefix . 'dyn_' . $type;

		$fields = array(
			array(
				"name" =>  "{$type}_id",
				"description" => "bigint(20) NOT NULL AUTO_INCREMENT",
			),
			array(
				"name" =>  "name",
				"description" => "varchar(255) DEFAULT NULL",
			)
		);

		$sql = DYN_REVIEW_HELPER::create_table_syntax($table_name, $fields, '', DYN_REVIEW_HELPER::get_character_set() );
		dbDelta($sql);
	}

	public static function create_rating_table(){
		include_once dirname(__FILE__) . '/class-dyn-review-helper.php';
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$type = 'rating';
		$table_name = $wpdb->prefix . 'dyn_' . $type;

		$fields = array(
			array(
				"name" =>  "{$type}_id",
				"description" => "bigint(20) NOT NULL AUTO_INCREMENT",
			),
			array(
				"name" =>  "cat_id",
				"description" => "bigint(20) NOT NULL",
			),
			array(
				"name" =>  "review_id",  //file/video(which is a post) id
				"description" => "bigint(20) NOT NULL",
			),
			array(
				"name" =>  "rating",
				"description" => "TINYINT(3) UNSIGNED DEFAULT NULL",
			)
		);

		$sql = DYN_REVIEW_HELPER::create_table_syntax($table_name, $fields, '', DYN_REVIEW_HELPER::get_character_set() );
		dbDelta($sql);
	}

	public static function create_review_table(){
		include_once dirname(__FILE__) . '/class-dyn-review-helper.php';
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$type = 'review';
		$table_name = $wpdb->prefix . 'dyn_' . $type;

		$fields = array(
			array(
				"name" =>  "{$type}_id",
				"description" => "bigint(20) NOT NULL AUTO_INCREMENT",
			),
			array(
				"name" =>  "post_id",  //file/video(which is a post) id
				"description" => "bigint(20) NOT NULL",
			),
			array(
				"name" =>  "review",
				"description" => "TEXT DEFAULT NULL",
			),
			array(
				"name" => "user_id",
				"description" => "bigint(20) NOT NULL"
			),
			array(
				"name" => "user_name",
				"description" => "VARCHAR(255) DEFAULT NULL" //Needed only in case it's a guest
			),
			array(
				"name" => "rating_type",
				"description" => "TINYINT(3) UNSIGNED DEFAULT NULL"
			),
			array(
				"name" => "date",
				"description" => "DATETIME NOT NULL"
			),
			array(
				"name" => "review_status",
				"description" => "VARCHAR(10) DEFAULT NULL"
			),
			array(
				"name" => "total_rating",
				"description" => "TINYINT(3) UNSIGNED DEFAULT NULL"
			)
		);

		$sql = DYN_REVIEW_HELPER::create_table_syntax($table_name, $fields, '', DYN_REVIEW_HELPER::get_character_set() );
		dbDelta($sql);
	}

	// public static function create_cache_table(){
	// 	include_once dirname(__FILE__) . '/class-dyn-review-helper.php';
	// 	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	// 	global $wpdb;
	// 	$type = 'cache';
	// 	$table_name = $wpdb->prefix . 'dyn_' . $type;

	// 	$fields = array(
	// 		array(
	// 			"name" =>  "{$type}_id",
	// 			"description" => "bigint(20) NOT NULL AUTO_INCREMENT",
	// 		),
	// 		array(
	// 			"name" =>  "post_id",
	// 			"description" => "bigint(20) NOT NULL",
	// 		),
	// 		array(
	// 			"name" =>  "rating",
	// 			"description" => "TINYINT(3) UNSIGNED DEFAULT NULL",
	// 		),
	// 		array(
	// 			"name" => "rating_type",
	// 			"description" => "TINYINT(3) UNSIGNED DEFAULT NULL"
	// 		)
	// 	);

	// 	$sql = DYN_REVIEW_HELPER::create_table_syntax($table_name, $fields, '', DYN_REVIEW_HELPER::get_character_set() );
	// 	dbDelta($sql);
	// }

}
