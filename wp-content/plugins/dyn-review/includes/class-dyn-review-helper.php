<?php

class DYN_REVIEW_HELPER{
	/**
	 *Rating type to integer conversion function
	 *
	 */
	public static function rating_type($type){
		/*if($type=='post')
			return 1;
		else
			return 2;*/
		switch($type)
		{
			case 'post':
			     return 1;
				 break;
				 
		    case 'productreview':	
                return 1;
				 break;
				 
            default:
			    return 2;
				
              			
		}
		
	}

	public static function create_table_syntax( $table_name, $fields, $uniques, $charset_collate ) {
		$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (";

		foreach( $fields as $field ) {
		  	$sql .= "{$field['name']} {$field['description']}";
		  	$sql .= ",";
		}
		$sql .= "PRIMARY KEY({$fields[0]['name']}),";
		if( is_array( $uniques ) ) {
			$sql .= "UNIQUE KEY(";
			foreach ( $uniques as $unique ) {
				$sql .= "{$unique},";
			}
			$sql = rtrim( $sql, ',' );
			$sql .= ")";
		}

		$sql = rtrim( $sql, ',' );
		$sql .= ")";
		$sql .= "{$charset_collate};";
		return $sql;
	}

	public static function get_character_set() {
		global $wpdb;
		$charset_collate = '';
		if (!empty ($wpdb->charset))
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		if (!empty ($wpdb->collate))
			$charset_collate .= " COLLATE {$wpdb->collate}";

		return $charset_collate;
	}

	public static function humanTiming ($time)
	{
	    $time = time() - $time; // to get the time since that moment
	    $time = ($time<1)? 1 : $time;
	    $tokens = array (
	        31536000 => 'year',
	        2592000 => 'month',
	        604800 => 'week',
	        86400 => 'day',
	        3600 => 'hour',
	        60 => 'minute',
	        1 => 'second'
	    );

	    foreach ($tokens as $unit => $text) {
	        if ($time < $unit) continue;
	        $numberOfUnits = floor($time / $unit);
	        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
	    }

	}
};