<?php $ptype = $_GET['ptype'] ;
     if($ptype =='products')
	 {
	   playlist_product();	 
	 }else{
		  else_playlist_product();	
	 }
?>
<?php function playlist_product() { ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php
$postId=get_the_ID();

$uploaded_type = get_post_meta( get_the_ID(), 'dyn_upload_type', true );
$uploaded_value = get_post_meta( get_the_ID(), 'dyn_upload_value', true );
?>

<?php if(!is_user_logged_in()){ ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
	<?php

}

$arg = get_current_user_id();
$path = "http://ec2-54-200-193-165.us-west-2.compute.amazonaws.com/app/video_prediction?user_id=".$arg;
$response = file_get_contents($path);


global $wpdb;
$postdataval = false;
if(isset($_POST['postdataval']) && $_POST['postdataval']!=''){
	if($_POST['postdataname'] == 'desc'){
		$postdataval = $_POST['postdataval'];
		$my_post = array(
				'ID'           => $_POST['postid'],
				'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'] == 'refe'){
		update_post_meta($_POST['postid'], 'video_options_refr', $_POST['postdataval']);
	}
}

?>

<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
///echo get_the_ID();
?>

<style>
 video::-internal-media-controls-download-button {
                    display:none;
                  }	
                   video::-webkit-media-controls-enclosure {
                   overflow:hidden;
                    }
 
                 video::-webkit-media-controls-panel {
                 width: calc(100% + 30px); /* Adjust as needed */
                   }
	 #overlyeffect{ position: absolute;
    top: 0px;
    bottom: 50%;
    left: 0px;
    right: 0px;
    opacity: 0;
    cursor: pointer;
    display: block;
   }			   
</style>
<div class="container-fluid grid-3-4 centre-block">
	<div class="row" id="product-first">
		<div class="col-md-12  col-sm-12 col-lg-12 col-xs-12" id="images" style="max-height:420px;padding-left:0px;overflow: hidden; padding-top: 0px;">
			<div class="rows">
			   <div class="col-md-8  col-sm-12 col-lg-8 col-xs-12" id="images">
				<div class="col-md-12 col-lg-8 col-sm-12" style="padding-left: 0px;">
					<?php
					
					 $_productvideofile = get_post_meta(  $post->ID, '_productvideofile', true );
				     $_productvideoimage = get_post_meta(  $post->ID, '_productvideoimage', true );
					 if($_productvideofile != ''){
						/* echo '<div class="img-product-page image-holder" style="cursor: pointer;"><div class="hover-item"></div><video id="thevideo" autoplay preload="yes">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads'. $_productvideofile. '" type="video/mp4">  
                                           </video></div>';*/
										   
						echo '<div class="img-product-page"><video id="thevideo" controls="controls" poster="'. $_productvideoimage .'" autoplay="true" preload="yes" style="display:block;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads'. $_productvideofile. '" type="video/mp4">  
                                           </video>
										    <i id="overlyeffect" class="fa fa-pause " aria-hidden="true"></i>
											 <i id="overlyeffect"  class="fa fa-play-circle" style="display:none" aria-hidden="true"></i>
										   </div>';				   
						
					 }else{
					if ( has_post_thumbnail() ) {
						$image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
						$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
						$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
								'title'	=> get_the_title( get_post_thumbnail_id() )
						) );

						$attachment_count = count( $product->get_gallery_attachment_ids() );

						if ( $attachment_count > 0 ) {
							$gallery = '[product-gallery]';
						} else {
							$gallery = '';
						}

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a></div>', $image_link, $image_caption, $image ), $post->ID );

					} else {

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><img src="%s" alt="%s" /></div>', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

					}
					 }
					?>
					<?php
					/**
					 * Single Product Thumbnails
					 *
					 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
					 *
					 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
					 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
					 * as little as possible, but it does happen. When this occurs the version of the template file will.
					 * be bumped and the readme will list any important changes.
					 *
					 * @see 	    http://docs.woothemes.com/document/template-structure/
					 * @author 		WooThemes
					 * @package 	WooCommerce/Templates
					 * @version     2.3.0
					 */

					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}
					global $post, $product, $woocommerce;

					$attachment_ids = $product->get_gallery_attachment_ids();

					if ( $attachment_ids ) {
						$loop 		= 0;
						$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
						?>
						<div class="thumbnails <?php echo 'columns-' . $columns; ?>  " style="width: 268px"><?php

							foreach ( $attachment_ids as $attachment_id ) {

								$classes = array( 'zoom' );

								if ( $loop === 0 || $loop % $columns === 0 )
									$classes[] = 'first';

								if ( ( $loop + 1 ) % $columns === 0 )
									$classes[] = 'last';

								$image_link = wp_get_attachment_url( $attachment_id );

								if ( ! $image_link )
									continue;

								$image_title 	= esc_attr( get_the_title( $attachment_id ) );
								$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

								$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
										'title'	=> $image_title,
										'alt'	=> $image_title,
										'width' => '65px'
								) );

								$image_class = esc_attr( implode( ' ', $classes ) );

								echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s"  data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

								$loop++;
							}

							?></div>
						<?php
					}
					//$country_id = oiopub_settings::get_user_country_index();

					if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
						$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

					} else {
						$queryyy = " ";
					}
					$tagSelected = wp_get_post_tags($post->ID);
					$tagsNameArray = array();
					$tagsSlugArray = array();
					$tagsIDArray = array();
					$tags_str = '';
					foreach($tagSelected as $tag)
					{
						$tags_str .= "'".$tag->name."',";
					}
					$tags_str = substr($tags_str, 0, -1);
					$bagfhfa = ''; $bagfhf = '';
					global $wpdb;
					$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
					if($ptags) {
						$ptagname = '';
						$ptagpurchaseid = '';foreach ($ptags as $ptag) {

							$ptagname = $ptag->tag;
							$ptagpurchaseid = $ptag->purchase_id;
						}
						if($ptags)	{$bagfhfa = "AND item_id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


							global $wpdb;
							$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
							$tagname = '';
							$tagpurchaseid = '';
							foreach ($tags as $tag) {

								$tagname = $tag->tag;
								$tagpurchaseid = $tag->purchase_id;
							}

							if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND item_id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
						}
					} ?>


				</div>
				<div class="col-md-12 col-lg-4 col-sm-12" id="addproduct">
					<div class="product-group">
						<!--Donate Popup-->
						<?php
						include 'sale-flash2.php';
						do_action( 'woocommerce_single_product_summary' );
						?>
						<?php
						$post_id = get_the_ID();
						$author_id=$post->post_author;
						if(is_user_logged_in() && $author_id == get_current_user_id())
						{

							echo '<div  id="tools-edit"><strong>Tools :<a href="#" id="title-edit"><i class="fa fa-pencil-square-o"></i>EDIT</a></strong>';
							echo '</div>';

						}

						?>

						<div id="title-div" style="display:none;">
							<input type="text" name="title-val" value="<?= the_title();?>">
							<input type="hidden" name="post_id" value="<?= get_the_id();?>">

							<button type="button" class="btn btn-info" id="title-save">Save</button>
							<button type="button" class="btn btn-primary" id="title-cncl">Cancel</button>
							<br>
						</div>



					</div>
					<div class="ships-from"></div>
				</div>
                  </div>
				  <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12" id="sidebar-product" style="padding-left: 25px;padding-right: 0px;" >
	                      
 <style>
 
.mobilediv{width:100%;height:50px;background-color:#000;}
.leftmobilediv{width:84%;float:left}
.rightmobilediv{width:10%;float:left;padding:11px;}
.rightmobilediv>.fa{
	       font-size: 26px;
    color: #fff;
}
.playlisttitle{
	margin-top: 5px;
    font-size: 19px;
    margin-bottom: 3px;
    padding-left: 20px;
    color: #fff;
}
.mobilediv span{
	color: #fff;
	 font-size: 19px;
	  padding-left: 20px;
}
.activeplaylist{
	    background: #1B1B1B;
		min-height: 78px;
}
.countplaylistnow{margin-right:-10px;}
.autoplay-bar{
    scrollbar-face-color: #000;
    scrollbar-shadow-color: #FFFFFF;
    scrollbar-highlight-color: #FFFFFF;
    scrollbar-3dlight-color: #FFFFFF;
    scrollbar-darkshadow-color: #FFFFFF;
    scrollbar-track-color: #FFFFFF;
    scrollbar-arrow-color: #FFFFFF;
}

/* Let's get this party started */
.autoplay-bar::-webkit-scrollbar {
    width: 5px;
}
 
/* Track */
.autoplay-bar::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
.autoplay-bar::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: #fff; 
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
@media (max-width: 1116px) {
			.grid-1-4-sidebar {
				width: 100%;
			}
}
#content{  padding-top: 0px;}
 #product-first {
    margin-bottom: 0px;
} 
#images .woocommerce-main-image.zoom img {
    max-width: 100%;
    vertical-align: middle;
    max-height: 500px;
    max-height: 309px;
}
</style>
     <?php
	 global $wpdb;
	   //  echo get_ip_address();
	     /*$idd = get_the_ID($post->ID);
	   $ips = ip2long(get_ip_address());
         $videpresultstrue = $wpdb->get_results( 'SELECT playlistid FROM '.$wpdb->prefix.'playlist_videosmp_song WHERE ipssss = ' . $ips );
		 $videpresults=$wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'playlist_videosmp_song WHERE playlistid = '. $idd .' and ipssss = '. $ips .' ORDER BY playlistid');
		$sf = count($videpresults);
		if( $sf > 0){ */
		$players = $_GET['opt'].'==';
             $player = base64_decode($players);
			  $playlisturl = $_GET['opt'];
			 $nowplaylisturl = get_the_ID($post->ID);
		//$videpresultstrue = $wpdb->get_results( 'SELECT playlistid FROM '.$wpdb->prefix.'playlist_video_song ' );
		 $nextid = $wpdb->get_results("SELECT song_id  FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = $player AND  song_id != $nowplaylisturl ORDER BY RAND() LIMIT 1" );

		$videpresultstrue = $wpdb->get_results("SELECT song_id  FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$player);
			
			?>
			 
             
			  <?php 
 $players = $_GET['opt'].'==';
  $player = base64_decode($players);
 $videpresultstrue = $wpdb->get_results("SELECT song_id  FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$player);
 ?>
 <style>
 .hideplaylistmob{display:none;}
 .black-background{background-color:#000;}
 .contextcolor{   
 color: rgb(204, 24, 30); 
  font-size: 17px;
    font-weight: 600;
    text-decoration: none !important;}
 elite_vp_title {
    font-size: 17px;
    font-weight: 600;
}
.hoverclor:hover{ background: #1B1B1B;}
.hite-background{background: #fff;}
 </style>

<?php
if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
    $queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

} else {
    $queryyy = " ";
}
$tagSelected = wp_get_post_tags($post->ID);
$tagsNameArray = array();
$tagsSlugArray = array();
$tagsIDArray = array();
$tags_str = '';
foreach($tagSelected as $tag)
{
    $tags_str .= "'".$tag->name."',";
}
$tags_str = substr($tags_str, 0, -1);
$bagfhfa = ''; $bagfhf = '';
global $wpdb;
 
$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 7 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


$bannerurl = '';
$bannerid = '';
$countryinbase = '';
foreach ($banners as $banner) {

    $bannerurl = $banner->item_url;
    $bannerid = $banner->item_id;
}

?>
<div class="grid-1-4-sidebar sidebar-autoblock black-background padding5_10 playlistsidebar">
       
<?php 
global $wpdb;
$datass = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE id = $player ");
$videpresultstrue = $wpdb->get_results("SELECT song_id  FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = $player ");
$totalplylist = count($videpresultstrue);
 ?>
 
<div class="mobilediv hideplaylistmob">
 <div class="leftmobilediv">
 <?php foreach($datass as $datas) 
 { ?>
 <h2 class="playlisttitle"> <?php echo $datas->title;  ?></h2>
 <span class="countplaylistnow"></span><span>/<?php  echo $totalplylist; ?> </span>
 <?php } ?>
</div>

<div class="rightmobilediv">
 <i class="fa fa-sort-desc upplaylist" aria-hidden="true"></i>
  <i class="fa fa-sort-asc downplaylist hideplaylistmob" aria-hidden="true"></i>
</div>
</div>   
		<div class="watch-sidebar-section watch-sidebar-section-playlist"     style="height: 100%;padding: 0px;margin-bottom: 0px; margin-top: 0px;">
			
		    <div class="autoplay-bar autoplay-bar-playlist" style="height: 470px; overflow: hidden; overflow: scroll; overflow-x: hidden;">
		      	 
			    
		        <div class="watch-sidebar-body">
			      	<ul class="video-list">			      		
			      		<?php 
			      		 $icout = 1;
			      		foreach ($videpresultstrue as $single_video) {		
			      			 
				      		?>			      		
					        <li class="video video-list-item related-list-item hoverclor active<?php echo $single_video->song_id; ?>" style="height: 100px;  margin-bottom: 8px;">
					        	<input type="hidden" class="video-guid" value="<?php echo $icout; ?>"/>	
					        	<div class="content-wrapper contextcolor">
					        		<a href="<?php echo get_permalink($single_video->song_id); ?>?opt=<?php echo $_GET['opt']; ?>&ih=playlist&ptype=products" class="contextcolor"><?php echo get_the_title($single_video->song_id); ?></a>
					        		<div class="video-item-des">
					        		<?php  
					        			echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') )
					        		?>

					        		</div>
					        		
									 <a class="detailsblock-btn" data-modal-id="myModalinfos<?php echo $single_video->song_id; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a>
								
					        	</div>
					        	<div class="thumb-wrapper">
					        		<a href="<?php echo get_permalink($single_video->song_id); ?>?opt=<?php echo $_GET['opt']; ?>&ih=playlist&ptype=products">
					        			<span class="yt-uix-simple-thumb-wrap yt-uix-simple-thumb-related">
					        				<?php 
					        				$video_image = wp_get_attachment_url( get_post_thumbnail_id($single_video->song_id));
					        				if($video_image != null){
					        					?>
					        					<img src="<?php echo $video_image; ?>" class="video-image" style="width: 120px; height: 90px;"/>
					        					<?php 	
					        				}else{
					        					?>
												<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-thumb.png" class="video-image" style="width: 120px; height: 100%;">
					        					<?php
					        				}
					        				?>			        				
					        			</span>
					        		</a>
					        	</div>				        	
					        </li>
					        <!-- Insert for first <li class="line"></li>-->
					        <?php 
					    	 $icout++;
					        }					      
						    ?>
			      	</ul>		      	
		    	</div>
				
						<?php foreach ($videpresultstrue as $single_videos) { 
						$single_video = get_post( $single_videos->song_id );
						?>
		<div class="modal-box" id="myModalinfos<?php echo $single_video->ID; ?>">
		    <div class="modal-dialog">
			  <div class="modal-content">
			   <div class="modal-bodys">
			    <a class="js-modal-close close">×</a>
			
			        
					<div class="img-represent">
                    <?php
                    $video_image = wp_get_attachment_url( get_post_thumbnail_id($single_video->ID));
                    if($video_image != null){
                        ?>
                        <img src="<?php echo $video_image; ?>" class="video-image" style="width: 120px;"/>
                        <?php
                    }else{
                        ?>
                        <img src="<?php echo get_site_url() ;  ?>/wp-content/plugins/woocommerce/assets/images/placeholder.png" class="video-image" style="width: 120px; height: 100%;">
                        <?php
                    }
                    ?>
                 
                </div>
				
				 <div class='other-image-product-first'>
                    <div class="thumbnails columns-3">
                        <?php
                        $product_id=$single_video->ID;
                        $_product = wc_get_product( $product_id );
                        $attachment_ids = $_product->get_gallery_attachment_ids();foreach( $attachment_ids as $attachment_id ){  ?>
                            <img width="180" height="180" src="<?php echo wp_get_attachment_url( $attachment_id ); ?>" class="other-image-product attachment-shop_thumbnail" alt="Chrysanthemum" title="Chrysanthemum">
                        <?php } ?>
                    </div></div>
					
					 <h4 class="layout-title"><a href="" style="color: rgb(75, 75, 75)!important;font-size: 16px !important;font-weight: 600 !important;"><?php echo $single_video->post_title; ?></a></h4>
				
				
				     <ul class="detail-info">
                    <li><?php $output = "";echo apply_filters( 'dyn_display_review', $output, $single_video->ID, "post" );?></li>

                    <li><label class="detail-title">Price:</label><?php
                        $product_id=$single_video->ID;
                        $_product = wc_get_product( $product_id );
                        ?><span class="price-product-modal">
				<?php if($_product->get_regular_price()!== ''){ if ($_product->get_sale_price() !=='') { ?> <del><span class="amount"><?php echo $_product->get_regular_price(); ?>$</span></del> <?php }else{?>
                    <span class="amount"><?php echo $_product->get_regular_price(); ?>$</span>  <?php }} ?>


                            <?php if ($_product->get_sale_price() !=='') {
                                ?>
                                <ins><span class="amount"><?php echo $_product->get_sale_price(); ?>$</span></ins> <?php } ?></span>
                        <span class="fgjieryhfe"> <span class="onsale" style="left: 75em; top: 0em;">Sale!</span></span>
                    </li>

                    <div class="stock-grid">
                        <h4 class="tittle-h4" style="    color: #000;" ><i class="fa fa-smile-o" aria-hidden="true"></i><span class="stock-span-grid"><?php
                                echo $_product->get_total_stock();
                                ?> in Stock</span></h4></div>


                    <li><label class="detail-title">Description:</label><?php echo wp_trim_words( $single_file->post_content); ?></li>
                    <li><label class="detail-title">Sold By:</label> <span><?php $storenamess = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );  
					  if(!empty( $storenamess )) { echo $storenamess; } else { 
							 echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); } ?></span></li>
                    <li><?php $allcountry_accept = get_post_meta( $single_video->ID, '_countries_shippinf_op', true );
					      $shipping_country = '';
						  $morecountrys_shipp = '';
                           if($allcountry_accept == 'all')
						   {
							   $shipping_country = 'All Countries';
						   }else{
							   
							   $spacicecountry = get_post_meta( $single_video->ID, '_spacicecountry');
				               $spacicecountrys = explode( ",", $spacicecountry[0] ); 
							   $countss = 0;
							   $tocountss  = count($spacicecountrys);
							   
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
										
									 if($countss < 2 && $tocountss != 1)
							             {
								            $shipping_country .=  $country_full_name . ', ';   
							            }
									 $countss++; 
							        }
								if($tocountss > 2)
								{
									$morecountrys_shipp = '<a href="#morecountry" id="morecountry"> View More</a>';
								}									
						    }
						  
					?>
					<label class="detail-title">Shipped To:</label><span class="shipped-counties"><?php echo $shipping_country ; echo $morecountrys_shipp; ?></span>
					<style>.shipped-counties{color: #337ab7;font-weight: bold; font-size: 13px;}</style></li>
				</ul>
				
					<ul class="stats">
					    <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
						<li><?php echo videopress_countviews($single_video->ID); ?></li>
						<li><?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_video->ID);echo count($result); ?> Endorsments</li>
						<li>
								<?php
								$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_video->ID;
								$result1 = $wpdb->get_results($sql_aux);
								echo count($result1);
								?> Favorites</li>
						<li><?php echo apply_filters('dyn_number_of_post_review', $single_video->ID, "post"); ?></li>
						<li>
								<?php
								$download = get_post_meta( $single_video->ID, 'dyn_download_count', true);
								if($download == ""){echo '0 Download';}else{echo $download. " Downloads";}
								?></li>
						<li><?php 
						$fdata = get_post($single_video->ID);
						$commentstotal = $fdata->comment_count;
						if($commentstotal == 0) { echo 'No Comments'; } else{ echo $commentstotal ." Comments"; }
						  ?></li>	
                        <li>
                        <?php
                        $units_sold = get_post_meta($single_video->ID, 'total_sales', true );
                        if($units_sold){
                            echo '<span>' . sprintf( __( ' %s', 'woocommerce' ), $units_sold ) . ' Sold</span>';}
                        else{echo '<span>0 Sold</span>';}
                        ?>
                    </li>
					
					</ul>
					 <br/>
					  <br/>
					   <br/>
			</div>
			 <div class="modal-footer">
					<button type="button" class="btn btn-default js-modal-close" data-dismiss="modal">Close</button>
				</div>
			</div></div></div>
			
		<?php } ?>
	 <style>
	   .modal-box .modal-bodys {
              padding: 2em 1.5em;
         }
		 
		 .modal-bodys ul.stats li {
    background: #e7e7e7;
    margin: 0 10px 10px 0;
    padding: 5px 15px;
    color: #212121;
}
.modal-dialog {
    max-width: 500px!important;
    box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
    
}
.modelboxaa .fa-download{display:none;}
.reviewssss{margin-top:5px;margin-bottom: 5px;}
 .detail-title{ color: #3498db; }
 .detail-title:hover{ color: #000; }
   .ftype{ margin-bottom: 5px; } 

	 </style>

	    	</div>
  		</div>


</div>

<script type="text/javascript">
$(function(){ 

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>





<?php
				  
              if ( wp_is_mobile() ) {
                    /* Display and echo mobile specific stuff here */
					  $mobi = 1;
                 }
				 else{
					 $mobi = 0;
				 }
               ?>
            </div>
			<script>
	jQuery(document).ready(function(){
	//	var height = this.parent.width()/(16/5.8234);	
	       var height = $(window).width()/(16/3.1234);
         //  jQuery(".Svid-single").width('100%').height(height);	
          var mql = window.matchMedia("(orientation: portrait)");
          //if(mql.matches) {
			  var mobile = "<?php echo $mobi; ?>";
		  if( mobile == 1){
			   jQuery(".watch-sidebar-section-playlist").addClass("hideplaylistmob");
			   jQuery(".mobilediv").removeClass("hideplaylistmob");
			    jQuery(".autoplay-bar-playlist").width('100%').height('600px');
				  jQuery(".centre-block").addClass("hite-background");
		  }	
		  else{
			  jQuery(".filehightnow").height('auto'); 
		  }

      jQuery(".fa-sort-desc").on("click",function(){
			jQuery(".watch-sidebar-section-playlist").removeClass("hideplaylistmob");
			  jQuery(".rightmobilediv>.downplaylist").removeClass("hideplaylistmob");
			 jQuery(".rightmobilediv>.upplaylist").addClass("hideplaylistmob");  
			 jQuery(".bannerimage").addClass("hideplaylistmob");
			  jQuery(".contentblock").addClass("hideplaylistmob");
			   jQuery(".saic-wrappersss").addClass("hideplaylistmob"); //contentsecondblock contentthrdblock contentfourthdblock sidebarmaincontent activedivplay
			   jQuery(".contentsecondblock").addClass("hideplaylistmob");
			    jQuery(".contentthrdblock").addClass("hideplaylistmob");
				 jQuery(".contentfourthdblock").addClass("hideplaylistmob");
				  jQuery(".saic-wrapper").addClass("hideplaylistmob");
				  jQuery(activedivplay).addClass("activeplaylist");
				  jQuery(".spacing-40").addClass("hideplaylistmob");
			
			});
		  jQuery(".fa-sort-asc").on("click",function(){
			 jQuery(".watch-sidebar-section-playlist").addClass("hideplaylistmob");
			  jQuery(".rightmobilediv>.upplaylist").removeClass("hideplaylistmob");
			 jQuery(".rightmobilediv>.downplaylist").addClass("hideplaylistmob");
			  jQuery(".bannerimage").removeClass("hideplaylistmob");
			    jQuery(".contentblock").removeClass("hideplaylistmob");
			   jQuery(".saic-wrapper").removeClass("hideplaylistmob");
			   jQuery(".contentsecondblock").removeClass("hideplaylistmob");
			    jQuery(".contentthrdblock").removeClass("hideplaylistmob");
				 jQuery(".contentfourthdblock").removeClass("hideplaylistmob");
				  jQuery(".saic-wrapper").removeClass("hideplaylistmob");
			  
			 
			});	
              var activedivplay = ".active<?php echo $nowplaylisturl; ?>";
		  var mob = "<?php echo $mobile; ?>";
		  var video_ird = " input.video-guid";
		  var videook = activedivplay + video_ird;
		 // var video_guid = $("ul li.video-list-item:first-child input.video-guid").val();
		 var video_guid = jQuery(videook).val();
        	 jQuery(".countplaylistnow").text(video_guid);	
			
         var r = "<?php echo $nextid[0]->song_id; ?>"
		 // var next_guid = jQuery(p).val();
		  var playlisttitle = "<?php echo $_GET['opt']; ?>";
        jQuery(".single_add_to_cart_button").on("click",function(){
			 window.setTimeout(function(){
			    location.href = "<?php echo get_permalink($nextid[0]->song_id); ?>?opt=" + playlisttitle + "&ih=playlists&ptype=products";
				}, 2000);
			});	
			
  });
  </script>


                   </div>
			</div>  <!-- end row -->
             
			<script>
				jQuery(document).ready(function($) {
					$('#title-edit').click(function(){
						$('#tools-edit').hide();
					});

					$('#title-cncl').click(function(e){
						e.preventDefault();

						$('#title-div').hide();
						$('#tools-edit').show();
					});
                   
				  /* $('.image-holder').click(function(e){
						e.preventDefault();

						var video = document.getElementById("thevideo"); 
						$('#thevideo').show();
						$('#image-div').hide();
						
						if (video.paused == true) {
                               video.play();
                              }
                                  else{
                              video.pause();
                              }	
					});*/
					
					$('i.fa-pause').click(function(e){
						e.preventDefault();

						var x = document.getElementById("thevideo"); 
						$('i.fa-play-circle').show();
						$('i.fa-pause').hide();
						//$('.img-product-page').append();
									//x.play();
									x.pause();
					});
					
					$('i.fa-play-circle').click(function(e){
						e.preventDefault();

						var x = document.getElementById("thevideo"); 
						$('i.fa-pause').show();
						$('i.fa-play-circle').hide();
						//$('.img-product-page').append();
									x.play();
									//x.pause();
					});
					
					/*$('.image-holder').mouseout(function(e){
						e.preventDefault();

						var x = document.getElementById("thevideo"); 
						$('#thevideo').show();
						$('#image-div').hide();
									x.pause();
					});*/

					$('#title-save').click(function(e){
						e.preventDefault();


						$('#title-div').hide();
						$('#tools-edit').show();

						var post_title = jQuery('input[name="title-val"]').val();
						var post_id = jQuery('input[name="post_id"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
								location.reload();
								//alert('ok');
							}
						})
					});
					$('.sold-by .wcvendors_ships_from span').prependTo(".ships-from");
					$('<i class="fa fa-smile-o" aria-hidden="true"></i>').prependTo('.stock.in-stock');
				});

			</script>
			
              
			</div>
         <div class="col-md-8  col-sm-12 col-lg-8 col-xs-12" id="descript">
			<div class="col-md-12 col-lg-12 col-sm-12 product-attributes-all">

				<?php


				$user = wp_get_current_user();
				if(!(in_array( 'adfree_user', (array) $user->roles ))) {
					$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 7 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


					$bannerurl = '';
					$bannerid = '';
					$countryinbase = '';
					foreach ($banners as $banner) {

						$bannerurl = $banner->item_url;
						$bannerid = $banner->item_id;
					}

					?>

					<?php
					if ($banners) {

						?>
						<a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
					<?php } } ?>


				<?php
				if ( ! defined( 'ABSPATH' ) ) {
					exit; // Exit if accessed directly
				}
				global $post, $product;
				$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
				$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
				?>



				<!-- Button trigger modal -->


         


				<div class="entry grey-background group-sort-tags-des">
					<?php
					$author_id=$post->post_author;
					$naxlogin = get_userdata($author_id);
					$storenamess = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );
					?>
					<div class="sold-by"> 	<p>Sold By <a href="<?php echo home_url(); ?>/user/<?php echo $naxlogin->user_nicename;?>/?store" >
					          <?php if(!empty( $storenamess )) { echo $storenamess; } else { ?>
							  <?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); } ?>
							</a>
						</p>
					</div>
					<div class="shipped-by">
					<?php $allcountry_accept = get_post_meta( get_the_ID(), '_countries_shippinf_op', true );
					      $shipping_country = '';
						  $morecountrys_shipp = '';
                           if($allcountry_accept == 'all')
						   {
							   $shipping_country = 'All Countries';
						   }else{
							   
							   $spacicecountry = get_post_meta( get_the_ID(), '_spacicecountry');
				               $spacicecountrys = explode( ",", $spacicecountry[0] ); 
							   $countss = 0;
							   $tocountss  = count($spacicecountrys);
							   
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
										
									 if($countss < 2 && $tocountss != 0)
							             {
								            $shipping_country .=  $country_full_name . ', ';   
							            }
									 $countss++; 
							        }
								if($tocountss > 2)
								{
									$morecountrys_shipp = '<a href="#morecountry" id="morecountry"> View More</a>';
								}									
						    }
							
							 $user_IDnow = get_current_user_id();
					if ( $user_IDnow != $author_id ){
						//check if the current user is suscribed to the list
						$susriptiondata = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."authors_suscribers WHERE author_id =". $author_id ." and suscriber_id =".$user_IDnow );
						
						$suscribedstat = 0;
						if( $susriptiondata ){
							foreach($susriptiondata as $susriptiondata_sing){
								$suscribedstat = $susriptiondata_sing->status;
							}
						}
					}
						  
					?>
					<p>Shipped To <span class="shipped-counties"><?php echo $shipping_country ; echo $morecountrys_shipp; ?></span></p>
					<style>.shipped-counties{color: #337ab7;font-weight: bold; font-size: 13px;}</style>
					</div>




					<div class="video-heading">

						<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
						<div class="flag_post custom_btn"><span id="addflag-anchor">Flag</span></div>

						<?php
						$postiddd = get_the_ID();
						$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postiddd . ' AND user_id = ' . get_current_user_id());
						//print_r($result);
						if (empty($result)){
							?>
							<div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
						<?php }else{ ?>
							<div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
						<?php } ?>

						<!-- Section for the favorite button -->
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postiddd . ' AND user_id = ' . get_current_user_id());
						if (empty($result)){
							?>
							<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
						<?php }else{ ?>
							<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
						<?php } ?>
						<!-- End favorite -->
						<!--Section for the subscribe button -->
					
						<div id="favorite_wrapper"><div id="susription_submit" class="endorse_video_no custom_btn" style="background-color:#3498db;<?php echo $statusdd = ($suscribedstat== 0)? "display:inline-block;": "display:none;"; ?>"><span>Suscribe</span></div></div>
					
						<div id="favorite_wrapper"><div id="unsusription_submit" class="endorse_video_no custom_btn" style="background-color:#0F9D58;<?php echo $statusdd = ($suscribedstat== 1)? "display:inline-block;": "display:none;"; ?>"><span>Unsuscribe</span></div></div>
					
					<!-- End subscribe -->

						<?php
						//Adding a filter for adding Review button
						//This will be added from dyn-review plugin
						//$output = "";
						//echo apply_filters( 'dyn_review_button', $output, $postiddd, "post" );
						?>

						<?php //Start of Section for the Donate Button get_current_user_id()
						$return_url = current_page_url()."?done=1";
						$cancel_url = current_page_url()."?done=0";
						$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_the_author_meta('ID')." and status=1");
						$paypalacc='';

						if($datapaypal){
							foreach ($datapaypal as $reg_datagen){
								$paypalacc=$reg_datagen->paypal_email;
							}
						}
						if($paypalacc!==''){ ?>

							<!--Donate Popup-->
							<div class="modal fade" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
										</div>
										<div class="modal-body">
											<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="lc" value="US">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<div class="form-group">
													<span for="amount" class="control-label col-sm-4">Amount:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="amount" required >
													</div>
												</div>
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<input type="hidden" name="on1" value="Message" required>
												<div class="form-group">
													<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os1" maxlength="300" size="40">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						<?php }
						?>


						<?php  //do_action( 'woocommerce_after_single_product_summary'); ?>



						<!--
                                        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                        <script>
                       /*
                                            $( function() {
                                                $( "#dialog" ).dialog({
                                                    autoOpen: false,
                                                    width:900,
                                                    show: {
                                                        effect: "blind",
                                                        duration: 1000
                                                    },
                                                    hide: {
                                                        effect: "explode",
                                                        duration: 1000
                                                    }
                                                });

                                                $( "#opener" ).on( "click", function() {
                                                    $( "#dialog" ).dialog( "open" );
                                                });

                                            } );
                                            */
                                        </script>
                    -->
						<!--

                                    <div id="dialog" title="Review">
                                        <div class="dialog-content-open"></div>
                                    </div>
                -->




						<script>
							/*	jQuery(document).ready(function($) {
							 $('#tab-reviews').prependTo(".dialog-content-open");
							 });
							 */
						</script>
						<!--
                        <div id="favorite_wrapper" class="review">
                            <button type="submit" id="opener" class="btn btn-primary">Review</button>
                        </div>
             -->




						<!--add to cart-->
						<div id="favorite_wrapper" class="form-add"><div>
					<span>
					<?php
					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}

					global $product;

					if ( ! $product->is_purchasable() ) {
						return;
					}

					?>

						<?php
						// Availability
						$availability      = $product->get_availability();
						$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

						echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
						?>

						<?php if ( $product->is_in_stock() ) : ?>

							<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

							<form class="cart" method="post" enctype='multipart/form-data'>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

								<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

								<?php
								if ( ! $product->is_sold_individually() ) {
									woocommerce_quantity_input( array(
											'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
											'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
											'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
									) );
								}
								?>

								<input type="hidden" class="addtocart" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />



								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

						<?php endif; ?>

					</span>
							</div></div>

						<div class="clear"></div>
					</div>

					<?php get_template_part('includes/sharer'); ?>
					<!-- End Sharer -->
					<?php
					$output = "";
					//echo apply_filters( 'dyn_display_review', $output, $postiddd, "post" );
					$bpost=get_post(get_the_ID());
			     //	$title = $bpost->post_title;
				    $post_author     = $bpost->post_author;
					?>
					<script>
						var check_id = "<?php echo get_current_user_id(); ?>";
						var poster_id = "<?php echo $post_author; ?>";

						// endorse video / un-endorse
						/*jQuery('#endorse_button').on('click',function(){
							if (check_id == poster_id){
								alert("You must be logged in to endorse a product.");
							}else{
								url = "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now();
								console.log(url);
								jQuery.ajax({
									url: "<?php echo get_home_url() ?>/endorse_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
									type: "GET",
								}).done(function(data) {
									//console.log(data);
									if (data == "endorsed"){
										jQuery('#endorse_button').html('<span>Endorsed!</span>');
										jQuery('#endorse_button').css('background-color','#0F9D58');
									}
									if (data == "unendorsed"){
										jQuery('#endorse_button').html('<span>Endorse</span>');
										jQuery('#endorse_button').css('background-color','#000');
									}
								});

							}
						});*/
						
						$('#endorse_button').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			
			var post_id = <?php echo get_the_ID(); ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookendorsementsssss',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								if (data.match(/endorsed/g)){
									//alert(data);
					$('#endorse_button').html('<span>Endorsed!</span>');
					$('#endorse_button').attr('style','background-color:#0F9D58');
				}
				if (data.match(/deleteend/g)){
					jQuery('#endorse_button').html('<span>Endorse</span>');
					jQuery('#endorse_button').css('background-color','#000');
				}
							}
						})
		     }			
			  
		});


						// favorite video / non-favorite
						/*jQuery('#favorite_button').on('click',function(){
							if (check_id == 0){
								alert("You must be logged in to set a product as favorite.");
							}else{
								url = "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now();
								console.log(url);
								jQuery.ajax({
									url: "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
									type: "GET",
								}).done(function(data) {
									alert(data);
									if (data == "favorite"){
										jQuery('#favorite_button').html('<span>Un-favorite!</span>');
										jQuery('#favorite_button').css('background-color','#0F9D58');
									}
									if (data == "notfavorite"){
										jQuery('#favorite_button').html('<span>Favorite</span>');
										jQuery('#favorite_button').css('background-color','#ff8a8a');
									}
								});

							}
						});*/
						
						$('#favorite_button').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			
			if(check_id == poster_id)
			{
				alert("You can not favorite Yourself!");
			}else {
			var post_id = <?php echo get_the_ID(); ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookfavoritefeed',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								  // alert(data);
								if (data.match(/favorite/g)){
									alert('Favorite');
									jQuery('#favorite_button').html('<span>Favorite</span>');
					jQuery('#favorite_button').css('background-color','#ff8a8a');
				
				}
				if (data.match(/Un-favorite/g)){
					alert('Un-favorite');
						$('#favorite_button').html('<span>Un-favorite</span>');
					$('#favorite_button').attr('style','background-color:#0F9D58');
				}
							}
						})
			}
			  
		});

						// flag inappropriate posts
						function flag_post(){
							//if (  localStorage["logged_in"] == 1 ){
							if (  1 == 1 ){
								// flag ajax code goes here
								jQuery.ajax({
									url: "<?php echo get_home_url() ?>/flag_email.php",
									type: "POST",
									data: {flagged_page : window.location.pathname}
								}).done(function(data) {
									console.log(data);
								});
								alert('This post has been flagged for moderation. Thank you.');
							}else{
								alert('Please log in to flag this as inappropriate. Thank you.');
							}
						}

						/* jQuery('.flag_post').on('click',function(){
						 if (check_id == 0){
						 alert("You must be logged in to flag a video.");
						 }else{
						 //flag_post();
						 }
						 });
						 */


					</script>

					<!-- Modal -->



					<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
						<!--<span class="sku_wrapper">
			<?php _e( 'SKU:', 'woocommerce' ); ?>
							<span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>
		</span>-->

					<?php endif; ?>

					<?php  //do_action( 'woocommerce_after_single_product_summary' );
					?>

					<?php if(WC()->cart->get_cart_contents_count()){ ?>
						<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
							<i class="fa fa-shopping-cart" aria-hidden="true"><sup>
									<?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>
								</sup></i> <bdi> - </bdi> <?php echo WC()->cart->get_cart_total(); ?>
						</a>
					<?php } ?>

					<!--<script>
						jQuery(document).ready(function($) {
							$('.cart-contents').prependTo(".nav-container-wrapper .nav-container");
						});

					</script>-->

					<div class="panel-group">
						<!-- START About us home page -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#paneldes">
									<h4 class="panel-title">Description <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
								</a>
							</div>
							<div id="paneldes" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-des"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="desf-c">
										<?php
										if($postdataval){
											echo $postdataval;
										}
										else{
											if($post->post_content==""){
												echo '<div class="content-empty">No Description Provided</div>';
											}else{
												the_content();
											}
										}

										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelref">
									<h4 class="panel-title">Additional info <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
								</a>
							</div>
							<div id="panelref" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-ref"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="ref-c">
										<?php
										$ref = get_post_meta(get_the_ID(),'video_options_refr',true);
										// $shipping_details = get_user_meta(get_the_author_id(), '_wcv_shipping',true);
										// $shipping_policy = ( is_array( $shipping_details ) && array_key_exists( 'shipping_policy', $shipping_details ) ) ? $shipping_details[ 'shipping_policy' ] : ''; 
										 //$shipping_policy   =   array_column($shipping_policys, 'shipping_policy');
										//$return_policy = ( is_array( $shipping_details ) && array_key_exists( 'return_policy', $shipping_details ) ) ? $shipping_details[ 'return_policy' ] : ''; 
										$return_policy = get_post_meta(get_the_ID(),'_wcv_shipping_return_policy',true);
										$shipping_policy = get_post_meta(get_the_ID(),'_wcv_shipping_policy',true);
										 
										if($ref){
											echo $ref;
										}else{
											echo 'No additional info given';
										}
										
										if($shipping_policy){
											echo '<h2> Shipping Policy </h2>';
											echo $shipping_policy;
										}
										
										if($return_policy){
											echo '<h2> Return Policy </h2>';
											echo $return_policy;
										}
										
										?>
									
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelshare">
									<h4 class="panel-title">Share <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
								</a>
							</div>
							<div id="panelshare" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="block-general">
										<div id="option-social">
											<div class="social-share-left"><a class="opt-facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;t=4th+Test" target="_blank"><span class="socicon-facebook"></span>facebook</a></div>
											<div class="social-share-left"><a class="opt-twitter" href="http://twitter.com/home?status=4th+Test�http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F" target="_blank"><span class="socicon-twitter"></span>twitter</a></div>
											<div class="social-share-left"><a class="opt-googleplus" href="https://plus.google.com/share?url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a></div>
											<div class="social-share-left"><a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;title=4th+Test" target="_blank"><span class="socicon-linkedin"></span>linkedin</a></div>
											<div class="social-share-left"><a class="opt-dig" href="http://digg.com/submit?url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;title=4th+Test" target="_blank"><span class="socicon-digg"></span>dig</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<ul class="stats sicon grey-background">
					<?php global $wpdb ; ?>
					<li><i class="fa fa-eye"></i> <?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa  fa-heart"></i>
						<?php
						$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
						$result1 = $wpdb->get_results($sql_aux);
						echo count($result1);
						?> Favorites</li>
					<!--end favorite -->
					<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><i class="fa fa-shopping-bag" aria-hidden="true"></i><span id="units_sold-sapn"></span></li>
				</ul>
				<script>
					jQuery(document).ready(function($) {
						$('.units_sold').prependTo("#units_sold-sapn");
						//$('.units_sold').css({'display':'inline-block'});
					});

				</script>



				<div class="entry grey-background group-sort-tags-des">
					<div id="category-tags">
						<?php //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

						<?php //echo $product->get_tags( ', ', '<i class="fa fa-tags" aria-hidden="true"></i><span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', ' </span>' ); ?>
						<?php
						// Link Pages Navigation
						$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
						wp_link_pages( $args );


						// Display the Tags
						$tagval = '';?>
						<?php $all_tags = array();?>
						<?php $all_tags = get_the_terms( get_the_ID(), 'product_tag' ); ?>
						<?php if($all_tags){
							$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
						}

						if(get_the_author_id() == get_current_user_id()){
							$before1 = '<span><i class="fa fa-tags"></i>Tags';
							$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
						}
						$before = '<span id="tags-val">';

						$after = '</span>';


						?>

						<?php if( is_user_logged_in() ){
							$profile_id = get_current_user_id();
							$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id  ORDER BY id DESC");
							//if($data){ ?>
							<!--<div class="addto_playlist grey-background padding5_10">
								<a herf="#" id="videostest" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
							</div> -->
							<?php // }
						} ?>
						<div class="post-tags grey-background padding5_10">
							<?= $before1;?>
							<?php
							$tagSelected = get_the_terms( get_the_ID(), 'product_tag' );
							$tagsNameArray = array();
							$tagsSlugArray = array();
							$tagsIDArray = array();
							foreach($tagSelected as $key=>$val)
							{
								$name = $val->name;
								$slug = $val->slug;
								$term_id = $val->term_id;
								$tagsNameArray[] = $name;
								$tagsSlugArray[] = $slug;
								$tagsIDArray[] = $term_id;
							}
							//the_tags( $before,', ',$after );
							
							 echo '<span id="tags-val">';
								if ($all_tags) {
									foreach($all_tags as $tagss) {
											echo '<a href="">' . $tagss->name . ',</a>'; 
									}
								}
								echo '</span>';

							?>
						</div>


						<?php

						if($all_tags){
							foreach($all_tags as $tag){
								$tagval .= $tag->name.',';
							};
						}

						rtrim($tagval, ",");

						if(get_the_author_id() == get_current_user_id()){
							?>

							<div class="tags-forminput" style="display:none;">
								<input name="tagsinput" class="tagsinput" id="tagsinput" value="<?= $tagval;?>">
								<input name="userid" type="hidden" id="tags_postid" value="<?= get_the_id();?>">
								<button type="button" class="btn btn-success" id="tagsave">Save</button>
								<button type="button" class="btn btn-primary" id="tagcancel">Cancel</button>
							</div>

						<?php }  ?>


					</div>
					<?php comments_template(); ?>
					<?php  do_action( 'woocommerce_product_meta_end' ); ?>
				</div>
			</div>

		</div>

		<div class="modal fade" id="modeldesref_section" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Add Description</h4>
					</div>
					<div class="modal-body">
						<form method="post" id="desref-form">
							<input type="hidden" name="postdataname" id="postdataname">

							<input type="hidden" name="postid" id="postid" value="<?= get_the_ID();?>">
							<div id="">
								<textarea rows="10" id="postdataval" name="postdataval"></textarea>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="desref-submit" class="btn btn-primary">Submit changes</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="moreshipped" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Shipped To </h4>
					</div>
					<div class="modal-body">
					<?php 
					$storename = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );
					$author_id=$post->post_author;
					$naxlogin = get_userdata($author_id);
					//$storenamess = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );
					?>
					<div class="sold-by"> 	<span class="titlesoldby">Sold By<span class="snamesoldby">
								<a href="<?php echo home_url(); ?>/user/<?php echo $naxlogin->user_nicename;?>/?store" ><?php if(!empty($storename)) { 
								 echo $storename;
								} else {
								?>
								<?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?>
								<?php } ?>
							</a></span></span>
						 
					</div>
					<div class="well well-sm">
			           <strong>Views</strong>
			           <div class="btn-group">
				          <a href="#" id="liststore" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
				        </span>List</a> <a href="#" id="gridstore" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
			            </div>
		            </div>
					 <div id="productssss" class="list-group">
						 <div class="shipped-by">
						 
					<?php  
					      $shipping_country = '';
						  $spacicecountry = get_post_meta( get_the_ID(), '_spacicecountry');
				          $spacicecountrys = explode( ",", $spacicecountry[0] ); 
						  $shipping_details = get_post_meta( get_the_ID(), '_wcv_shipping_rates',  true ); 
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										
										
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
									    
								        $shipping_country =  $country_full_name; 
										
                                        if($spacicecountryss == 'GB')
										{
										 $spacicecountryss = 'UK';	
										}										
										$shipping_rate    = '';
										//$rates = 0;
		                                foreach ( $shipping_details as $shipping_rate ) {
			                             if($spacicecountryss == $shipping_rate['country'])
			                               {
				                              $shipping_rate =  $shipping_rate['fee'];
			                                
                        ?>										
							            <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 grid-group-item changeStyleGrid">
										<div class="layout-2-wrapper">
										   <h4>Country</h4>
										<?php	echo $shipping_country;	?>
										  <h4>Shipping Rate</h4>
										  <span class="amount" ><?php	echo '$' . $shipping_rate;	?></span>
										  <div class="listview" style="display:none">
										   <h4>Shipping Policy</h4>
										   </div>
										</div></div>
						
						<?php	} } }	?>
					   </div>
					<style>.shipped-counties{color: #337ab7;font-weight: bold; font-size: 13px;}
					 .titlesoldby{font-size: 22px !important;
                     padding: 6px !important;
					 text-transform: uppercase;
					 } 
					 .snamesoldby{color: #337ab7;}
					 #productssss .changeStyleGrid {
                            min-height: 130px !important;
                           text-align: center;
						   }
					 #productssss .changeStyleList { height: 139px !important;}	  
                     @media (min-width: 768px){
                          #moreshipped .modal-dialog {
                            width: 700px; margin: 30px auto;
							}	
					 }
                    #productssss .changeStyleList .layout-2-wrapper {
                         margin-bottom: 20px;
                         border: 1px solid #EEEEEE;
                         padding: 15px;
                         box-shadow: 0 0 10px 1px #666;
                         border-radius: 5px;
						min-height: auto;
                     }	
                    #productssss .changeStyleGrid .layout-2-wrapper {
                         margin-bottom: 20px;
                         border: 1px solid #EEEEEE;
                         padding: 15px;
                         box-shadow: 0 0 10px 1px #666;
                         border-radius: 5px;
						min-height: 156px !important;
                     }						 
				   </style>
					</div>
					 <h2> &nbsp; &nbsp; </h2>
					</div>
				</div>
			</div>
		</div>
		<!-- Lightbox popup -->



		<script>

			$(document).ready(function(){
				var $ = jQuery.noConflict();

				CKEDITOR.inline( 'postdataval' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
				CKEDITOR.editorConfig = function( config ) {
					config.font_defaultLabel = 'Arial';
					config.fontSize_defaultLabel = '44px';
				};

				var $ = jQuery.noConflict();
				$('input#tagsinput').tagsinput({
					confirmKeys: [32]
				});

				$('#tagcancel').click(function(e){
					e.preventDefault();

					$('.tags-forminput').hide();
					$('#tags-val').show();
					$('#tagsinput').tagsinput('removeAll');
					$('#tagsinput').tagsinput('add', '<?= $tagval;?>', {preventPost: true});
				})

			})

			var $ = jQuery.noConflict();


			$('#tags-edit').click(function(e){
				e.preventDefault();
				$('#tags-val').hide();
				$('.tags-forminput').show();
			});

			$('#tagcancel').click(function(e){
				e.preventDefault();
				$('.tags-forminput').hide();
			})

			/*$('#tagsave').click(function(e){
				e.preventDefault();

				var tags_postid = $('#tags_postid').val();
				var tagsinput = $('#tagsinput').val();

				$.ajax({
					url: admin_ajax,
					type: "POST",
					data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
				}).done(function(data) {
					location.reload();
				});

				$('.tags-forminput').hide();
				$('#tags-val').show();

			});*/
			$('#tagsave').click(function(e){
				e.preventDefault();

				var tags_postid = $('#tags_postid').val();
				var tagsinput = $('#tagsinput').val();

				$.ajax({
					url: admin_ajax,
					type: "POST",
					data: {action : 'user_product_tags_edits',tags_postid: tags_postid,product_tags: tagsinput}
				}).done(function(data) {
					alert(data);
					location.reload();
				});

				$('.tags-forminput').hide();
				$('#tags-val').show();

			});



			$('#edit-des').click(function(e){
				var $ = jQuery.noConflict();

				e.preventDefault();

				$('#myModalLabel').html('Add Description');
				$('#postdataname').val('desc');

				$('.cke_textarea_inline').html($('#desf-c').html());

				$('#modeldesref_section').modal();
			})


			$('#edit-ref').click(function(e){
				e.preventDefault();

				var $ = jQuery.noConflict();

				$('#myModalLabel').html('Add Reference');
				$('#postdataname').val('refe');

				$('.cke_textarea_inline').html($('#ref-c').html());

				$('#modeldesref_section').modal();
			})

			$('#desref-submit').click(function(e){
				var $ = jQuery.noConflict();
				$('#desref-form').submit();
			})
			
			
			$('#morecountry').click(function(e){
				e.preventDefault();
                 //#morecountry  moreshipped
				var $ = jQuery.noConflict();
				
				$('#moreshipped').modal();
			})
       
	   
	        $('#liststore').click(function(event){
    	               event.preventDefault();
					 $('#productssss .item').removeClass('grid-group-item changeStyleGrid');  
    	             $('#productssss .item').addClass('list-group-item changeStyleList');
					 

                 });
	   
	        $('#gridstore').click(function(event){
    	            event.preventDefault();
    	           $('#productssss .item').removeClass('list-group-item changeStyleList');
    	           $('#productssss .item').addClass('grid-group-item changeStyleGrid');
               });

		</script>

		<?php do_action( 'woocommerce_product_thumbnails' ); ?>


	</div>
						<?php } ?>
						
<?php function else_playlist_product () { ?>	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php
$postId=get_the_ID();

$uploaded_type = get_post_meta( get_the_ID(), 'dyn_upload_type', true );
$uploaded_value = get_post_meta( get_the_ID(), 'dyn_upload_value', true );
?>

<?php if(!is_user_logged_in()){ ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
	<?php

}

$arg = get_current_user_id();
$path = "http://ec2-54-200-193-165.us-west-2.compute.amazonaws.com/app/video_prediction?user_id=".$arg;
$response = file_get_contents($path);


global $wpdb;
$postdataval = false;
if(isset($_POST['postdataval']) && $_POST['postdataval']!=''){
	if($_POST['postdataname'] == 'desc'){
		$postdataval = $_POST['postdataval'];
		$my_post = array(
				'ID'           => $_POST['postid'],
				'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'] == 'refe'){
		update_post_meta($_POST['postid'], 'video_options_refr', $_POST['postdataval']);
	}
}

?>

<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
///echo get_the_ID();
?>

<style>
 video::-internal-media-controls-download-button {
                    display:none;
                  }	
                   video::-webkit-media-controls-enclosure {
                   overflow:hidden;
                    }
 
                 video::-webkit-media-controls-panel {
                 width: calc(100% + 30px); /* Adjust as needed */
                   }
	 #overlyeffect{ position: absolute;
    top: 0px;
    bottom: 50%;
    left: 0px;
    right: 0px;
    opacity: 0;
    cursor: pointer;
    display: block;
   }			   
</style>				   
<div class="container-fluid grid-3-4 centre-block">
	<div class="row" id="product-first">
		<div class="col-md-8  col-sm-12 col-lg-8 col-xs-12" id="images">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<?php
					
					 $_productvideofile = get_post_meta(  $post->ID, '_productvideofile', true );
				     $_productvideoimage = get_post_meta(  $post->ID, '_productvideoimage', true );
					 if($_productvideofile != ''){
						 /* echo '<div class="img-product-page image-holder" style="cursor: pointer;"><div class="hover-item"></div><video controls id="thevideo" autoplay preload="yes" style="width: 100%;">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads'. $_productvideofile. '" type="video/mp4">  
                                           </video></div>';*/
										   
						echo '<div class="img-product-page"><video id="thevideo" controls="controls" poster="'. $_productvideoimage .'" autoplay="true" preload="yes" style="display:block;width:100%">
                                          <source src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads'. $_productvideofile. '" type="video/mp4">  
                                           </video>
										    <i id="overlyeffect" class="fa fa-pause " aria-hidden="true"></i>
											 <i id="overlyeffect"  class="fa fa-play-circle" style="display:none" aria-hidden="true"></i>
										   </div>';					   
										   
						 $attachementidss = get_post_thumbnail_id();
						insert_thumbnail_id_video_product($_productvideoimage,  $attachementidss, $post->ID) ;
					 }else{
					if ( has_post_thumbnail() ) {
						$image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
						$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
						$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
								'title'	=> get_the_title( get_post_thumbnail_id() )
						) );

						$attachment_count = count( $product->get_gallery_attachment_ids() );

						if ( $attachment_count > 0 ) {
							$gallery = '[product-gallery]';
						} else {
							$gallery = '';
						}

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a></div>', $image_link, $image_caption, $image ), $post->ID );

					} else {

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><img src="%s" alt="%s" /></div>', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

					}
					 }
					?>
					<?php
					/**
					 * Single Product Thumbnails
					 *
					 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
					 *
					 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
					 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
					 * as little as possible, but it does happen. When this occurs the version of the template file will.
					 * be bumped and the readme will list any important changes.
					 *
					 * @see 	    http://docs.woothemes.com/document/template-structure/
					 * @author 		WooThemes
					 * @package 	WooCommerce/Templates
					 * @version     2.3.0
					 */

					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}
					global $post, $product, $woocommerce;

					$attachment_ids = $product->get_gallery_attachment_ids();

					if ( $attachment_ids ) {
						$loop 		= 0;
						$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
						?>
						<div class="thumbnails <?php echo 'columns-' . $columns; ?>  " style="width: 268px"><?php

							foreach ( $attachment_ids as $attachment_id ) {

								$classes = array( 'zoom' );

								if ( $loop === 0 || $loop % $columns === 0 )
									$classes[] = 'first';

								if ( ( $loop + 1 ) % $columns === 0 )
									$classes[] = 'last';

								$image_link = wp_get_attachment_url( $attachment_id );

								if ( ! $image_link )
									continue;

								$image_title 	= esc_attr( get_the_title( $attachment_id ) );
								$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

								$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
										'title'	=> $image_title,
										'alt'	=> $image_title,
										'width' => '65px'
								) );

								$image_class = esc_attr( implode( ' ', $classes ) );

								echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s"  data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

								$loop++;
							}

							?></div>
						<?php
					}
					//$country_id = oiopub_settings::get_user_country_index();

					if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
						$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

					} else {
						$queryyy = " ";
					}
					$tagSelected = wp_get_post_tags($post->ID);
					$tagsNameArray = array();
					$tagsSlugArray = array();
					$tagsIDArray = array();
					$tags_str = '';
					foreach($tagSelected as $tag)
					{
						$tags_str .= "'".$tag->name."',";
					}
					$tags_str = substr($tags_str, 0, -1);
					$bagfhfa = ''; $bagfhf = '';
					global $wpdb;
					$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
					if($ptags) {
						$ptagname = '';
						$ptagpurchaseid = '';foreach ($ptags as $ptag) {

							$ptagname = $ptag->tag;
							$ptagpurchaseid = $ptag->purchase_id;
						}
						if($ptags)	{$bagfhfa = "AND item_id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


							global $wpdb;
							$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
							$tagname = '';
							$tagpurchaseid = '';
							foreach ($tags as $tag) {

								$tagname = $tag->tag;
								$tagpurchaseid = $tag->purchase_id;
							}

							if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND item_id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
						}
					} ?>


				</div>
				<div class="col-md-12 col-lg-4 col-sm-12" id="addproduct">
					<div class="product-group">
						<!--Donate Popup-->
						<?php
						include 'sale-flash2.php';
						do_action( 'woocommerce_single_product_summary' );
						?>
						<?php
						$post_id = get_the_ID();
						$author_id=$post->post_author;
						if(is_user_logged_in() && $author_id == get_current_user_id())
						{

							echo '<div  id="tools-edit"><strong>Tools :<a href="#" id="title-edit"><i class="fa fa-pencil-square-o"></i>EDIT</a></strong>';
							echo '</div>';

						}

						?>

						<div id="title-div" style="display:none;">
							<input type="text" name="title-val" value="<?= the_title();?>">
							<input type="hidden" name="post_id" value="<?= get_the_id();?>">

							<button type="button" class="btn btn-info" id="title-save">Save</button>
							<button type="button" class="btn btn-primary" id="title-cncl">Cancel</button>
							<br>
						</div>



					</div>
					<div class="ships-from"></div>
				</div>

			</div>

			<script>
				jQuery(document).ready(function($) {
					
					
					
					$('#title-edit').click(function(){
						$('#tools-edit').hide();
					});

					$('#title-cncl').click(function(e){
						e.preventDefault();

						$('#title-div').hide();
						$('#tools-edit').show();
					});
                  
				   /* $('.image-holder').click(function(e){
						e.preventDefault();

						var video = document.getElementById("thevideo"); 
						$('#thevideo').show();
						$('#image-div').hide();
								//	x.play();
						if (video.paused == true) {
                               
							    video.pause();
                              }
                                  else{
                              video.play();
                              }			
					});*/
					
					$('i.fa-pause').click(function(e){
						e.preventDefault();

						var x = document.getElementById("thevideo"); 
						$('i.fa-play-circle').show();
						$('i.fa-pause').hide();
						//$('.img-product-page').append();
									//x.play();
									x.pause();
					});
					
					$('i.fa-play-circle').click(function(e){
						e.preventDefault();

						var x = document.getElementById("thevideo"); 
						$('i.fa-pause').show();
						$('i.fa-play-circle').hide();
						//$('.img-product-page').append();
									x.play();
									//x.pause();
					});
					
					/*$('.image-holder').click(function(e){
						e.preventDefault();

						var x = document.getElementById("thevideo"); 
						$('#thevideo').show();
						$('#image-div').hide();
									x.pause();
					});*/

					$('#title-save').click(function(e){
						e.preventDefault();


						$('#title-div').hide();
						$('#tools-edit').show();

						var post_title = jQuery('input[name="title-val"]').val();
						var post_id = jQuery('input[name="post_id"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
								ocation.reload();
								//alert('ok');
							}
						})
					});
					$('.sold-by .wcvendors_ships_from span').prependTo(".ships-from");
					$('<i class="fa fa-smile-o" aria-hidden="true"></i>').prependTo('.stock.in-stock');
				});
              
			  jQuery(function(){
					$('i.fa-play-circle').trigger('click');
				});
			</script>

			<div class="col-md-12 col-lg-12 col-sm-12 product-attributes-all">

				<?php


				$user = wp_get_current_user();
				if(!(in_array( 'adfree_user', (array) $user->roles ))) {
					$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 7 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


					$bannerurl = '';
					$bannerid = '';
					$countryinbase = '';
					foreach ($banners as $banner) {

						$bannerurl = $banner->item_url;
						$bannerid = $banner->item_id;
					}

					?>

					<?php
					if ($banners) {

						?>
						<a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
					<?php } } ?>


				<?php
				if ( ! defined( 'ABSPATH' ) ) {
					exit; // Exit if accessed directly
				}
				global $post, $product;
				$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
				$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
				?>



				<!-- Button trigger modal -->





				<div class="entry grey-background group-sort-tags-des">
					<?php
					$author_id=$post->post_author;
					$naxlogin = get_userdata($author_id);
					$storenamess = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );
					?>
					<div class="sold-by"> 	<p>Sold By <a href="<?php echo home_url(); ?>/user/<?php echo $naxlogin->user_nicename; ?>/?store" >
					          <?php if(!empty( $storenamess )) { echo $storenamess; } else { ?>
							  <?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); } ?>
							</a>
						</p>
					</div>
                    <div class="shipped-by">
					<?php $allcountry_accept = get_post_meta( get_the_ID(), '_countries_shippinf_op', true );
					      $shipping_country = '';
						  $morecountrys_shipp = '';
                           if($allcountry_accept == 'all')
						   {
							   $shipping_country = 'All Countries';
						   }else{
							   
							   $spacicecountry = get_post_meta( get_the_ID(), '_spacicecountry');
				               $spacicecountrys = explode( ",", $spacicecountry[0] ); 
							   $countss = 0;
							   $tocountss  = count($spacicecountrys);
							   
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
										
									 if($countss < 2 && $tocountss != 0)
							             {
								            $shipping_country .=  $country_full_name . ', ';   
							            }
									 $countss++; 
							        }
								if($tocountss > 2)
								{
									$morecountrys_shipp = '<a href="#morecountry" id="morecountry"> View More</a>';
								}									
						    }
							
							 $user_IDnow = get_current_user_id();
					if ( $user_IDnow != $author_id ){
						//check if the current user is suscribed to the list
						$susriptiondata = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."authors_suscribers WHERE author_id =". $author_id ." and suscriber_id =".$user_IDnow );
						
						$suscribedstat = 0;
						if( $susriptiondata ){
							foreach($susriptiondata as $susriptiondata_sing){
								$suscribedstat = $susriptiondata_sing->status;
							}
						}
					}
						  
					?>
					<p>Shipped To <span class="shipped-counties"><?php echo $shipping_country ; echo $morecountrys_shipp; ?></span></p>
					<style>.shipped-counties{color: #337ab7;font-weight: bold; font-size: 13px;}</style>
					</div>



					<div class="video-heading">

						<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
						<div class="flag_post custom_btn"><span id="addflag-anchor">Flag</span></div>

						<?php
						$postiddd = get_the_ID();
						$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postiddd . ' AND user_id = ' . get_current_user_id());
						//print_r($result);
						if (empty($result)){
							?>
							<div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
						<?php }else{ ?>
							<div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
						<?php } ?>

						<!-- Section for the favorite button -->
						<?php
						$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postiddd . ' AND user_id = ' . get_current_user_id());
						if (empty($result)){
							?>
							<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
						<?php }else{ ?>
							<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
						<?php } ?>
						<!-- End favorite -->
						
						<!--Section for the subscribe button -->
					
						<div id="favorite_wrapper"><div id="susription_submit" class="endorse_video_no custom_btn" style="background-color:#3498db;<?php echo $statusdd = ($suscribedstat== 0)? "display:inline-block;": "display:none;"; ?>"><span>Suscribe</span></div></div>
					
						<div id="favorite_wrapper"><div id="unsusription_submit" class="endorse_video_no custom_btn" style="background-color:#0F9D58;<?php echo $statusdd = ($suscribedstat== 1)? "display:inline-block;": "display:none;"; ?>"><span>Unsuscribe</span></div></div>
					
					<!-- End subscribe -->

						<?php
						//Adding a filter for adding Review button
						//This will be added from dyn-review plugin
						//$output = "";
						//echo apply_filters( 'dyn_review_button', $output, $postiddd, "post" );
						?>

						<?php //Start of Section for the Donate Button get_current_user_id()
						$return_url = current_page_url()."?done=1";
						$cancel_url = current_page_url()."?done=0";
						$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_the_author_meta('ID')." and status=1");
						$paypalacc='';

						if($datapaypal){
							foreach ($datapaypal as $reg_datagen){
								$paypalacc=$reg_datagen->paypal_email;
							}
						}
						if($paypalacc!==''){ ?>

							<!--Donate Popup-->
							<div class="modal fade" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
										</div>
										<div class="modal-body">
											<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="lc" value="US">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<div class="form-group">
													<span for="amount" class="control-label col-sm-4">Amount:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="amount" required >
													</div>
												</div>
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<input type="hidden" name="on1" value="Message" required>
												<div class="form-group">
													<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os1" maxlength="300" size="40">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						<?php }
						?>


						<?php  //do_action( 'woocommerce_after_single_product_summary'); ?>



						<!--
                                        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                        <script>
                       /*
                                            $( function() {
                                                $( "#dialog" ).dialog({
                                                    autoOpen: false,
                                                    width:900,
                                                    show: {
                                                        effect: "blind",
                                                        duration: 1000
                                                    },
                                                    hide: {
                                                        effect: "explode",
                                                        duration: 1000
                                                    }
                                                });

                                                $( "#opener" ).on( "click", function() {
                                                    $( "#dialog" ).dialog( "open" );
                                                });

                                            } );
                                            */
                                        </script>
                    -->
						<!--

                                    <div id="dialog" title="Review">
                                        <div class="dialog-content-open"></div>
                                    </div>
                -->




						<script>
							/*	jQuery(document).ready(function($) {
							 $('#tab-reviews').prependTo(".dialog-content-open");
							 });
							 */
						</script>
						<!--
                        <div id="favorite_wrapper" class="review">
                            <button type="submit" id="opener" class="btn btn-primary">Review</button>
                        </div>
             -->




						<!--add to cart-->
						<div id="favorite_wrapper" class="form-add"><div>
					<span>
					<?php
					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}

					global $product;

					if ( ! $product->is_purchasable() ) {
						return;
					}

					?>

						<?php
						// Availability
						$availability      = $product->get_availability();
						$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

						echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
						?>

						<?php if ( $product->is_in_stock() ) : ?>

							<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

							<form class="cart" method="post" enctype='multipart/form-data'>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

								<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

								<?php
								if ( ! $product->is_sold_individually() ) {
									woocommerce_quantity_input( array(
											'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
											'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
											'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
									) );
								}
								?>

								<input type="hidden" class="addtocart" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />



								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

						<?php endif; ?>

					</span>
							</div></div>

						<div class="clear"></div>
					</div>

					<?php get_template_part('includes/sharer'); ?>
					<!-- End Sharer -->
					<?php
					$output = "";
					//echo apply_filters( 'dyn_display_review', $output, $postiddd, "post" );
					$bpost=get_post(get_the_ID());
			     //	$title = $bpost->post_title;
				    $post_author     = $bpost->post_author;
					?>
					<script>
						var check_id = "<?php echo get_current_user_id(); ?>";
                        var poster_id = "<?php echo $post_author; ?>";
						// endorse video / un-endorse
						/*jQuery('#endorse_button').on('click',function(){
							if (check_id == 0){
								alert("You must be logged in to endorse a product.");
							}else{
								if(check_id == poster_id){
									alert("You can not endorse Yourself!");
								} else{
								url = "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now();
								console.log(url);
								jQuery.ajax({
									url: "<?php echo get_home_url() ?>/endorse_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
									type: "GET",
								}).done(function(data) {
									//console.log(data);
									alert(data)
									if (data == "endorsed"){
										jQuery('#endorse_button').html('<span>Endorsed!</span>');
										jQuery('#endorse_button').css('background-color','#0F9D58');
									}
									if (data == "unendorsed"){
										jQuery('#endorse_button').html('<span>Endorse</span>');
										jQuery('#endorse_button').css('background-color','#000');
									}
								});
							 }
							}
						});



						// favorite video / non-favorite
						jQuery('#favorite_button').on('click',function(){
							if (check_id == 0){
								alert("You must be logged in to set a product as favorite.");
							}else{
								if(check_id == poster_id){
									alert("You can not favorite Yourself!");
								} else{
								url = "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now();
								console.log(url);
								jQuery.ajax({
									url: "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
									type: "GET",
								}).done(function(data) {
									alert(data);
									if (data == "favorite"){
										jQuery('#favorite_button').html('<span>Un-favorite!</span>');
										jQuery('#favorite_button').css('background-color','#0F9D58');
									}
									if (data == "notfavorite"){
										jQuery('#favorite_button').html('<span>Favorite</span>');
										jQuery('#favorite_button').css('background-color','#ff8a8a');
									}
								});
								}
							}
						});  */
						
						
							$('#endorse_button').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			if(check_id == poster_id)
			{
				alert("You can not endorse Yourself.");
			}else {
			
			var post_id = <?php echo get_the_ID(); ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookendorsementsssss',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								//location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								if (data.match(/endorsed/g)){
									//alert(data);
					$('#endorse_button').html('<span>Endorsed!</span>');
					$('#endorse_button').attr('style','background-color:#0F9D58');
				}
				if (data.match(/deleteend/g)){
					jQuery('#endorse_button').html('<span>Endorse</span>');
					jQuery('#endorse_button').css('background-color','#000');
				}
							}
						})
		     }			
			  
		});
		
		$('#favorite_button').click(function(e){
			var check_id = "<?php echo get_current_user_id(); ?>";
			
			if(check_id == poster_id)
			{
				alert("You can not favorite Yourself!");
			}else {
			var post_id = <?php echo get_the_ID(); ?>;
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'filebookfavoritefeed',post_id : post_id},
							success : function(data){
								//$('#titleafter-edrit<?php echo $postID; ?>').html(post_title);
								location.reload();
								//alert('ok');title-edit
								   // var res = str.match(/ain/g);
								  // alert(data);
								if (data.match(/favorite/g)){
									alert('Favorite');
									jQuery('#favorite_button').html('<span>Favorite</span>');
					jQuery('#favorite_button').css('background-color','#ff8a8a');
				
				}
				if (data.match(/Un-favorite/g)){
					alert('Un-favorite');
						$('#favorite_button').html('<span>Un-favorite</span>');
					$('#favorite_button').attr('style','background-color:#0F9D58');
				}
							}
						})
			}
			  
		});

						// flag inappropriate posts
						function flag_post(){
							//if (  localStorage["logged_in"] == 1 ){
							if (  1 == 1 ){
								// flag ajax code goes here
								jQuery.ajax({
									url: "<?php echo get_home_url() ?>/flag_email.php",
									type: "POST",
									data: {flagged_page : window.location.pathname}
								}).done(function(data) {
									console.log(data);
								});
								alert('This post has been flagged for moderation. Thank you.');
							}else{
								alert('Please log in to flag this as inappropriate. Thank you.');
							}
						}

						/* jQuery('.flag_post').on('click',function(){
						 if (check_id == 0){
						 alert("You must be logged in to flag a video.");
						 }else{
						 //flag_post();
						 }
						 });
						 */


					</script>

					<!-- Modal -->



					<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
						<!--<span class="sku_wrapper">
			<?php _e( 'SKU:', 'woocommerce' ); ?>
							<span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>
		</span>-->

					<?php endif; ?>

					<?php  //do_action( 'woocommerce_after_single_product_summary' );
					?>

					<?php if(WC()->cart->get_cart_contents_count()){ ?>
						<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
							<i class="fa fa-shopping-cart" aria-hidden="true"><sup>
									<?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>
								</sup></i> <bdi> - </bdi> <?php echo WC()->cart->get_cart_total(); ?>
						</a>
					<?php } ?>

					<!--<script>
						jQuery(document).ready(function($) {
							$('.cart-contents').prependTo(".nav-container-wrapper .nav-container");
						});

					</script>-->

					<div class="panel-group">
						<!-- START About us home page -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#paneldes">
									<h4 class="panel-title">Description <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
								</a>
							</div>
							<div id="paneldes" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-des"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="desf-c">
										<?php
										if($postdataval){
											echo $postdataval;
										}
										else{
											if($post->post_content==""){
												echo '<div class="content-empty">No Description Provided</div>';
											}else{
												the_content();
											}
										}

										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelref">
									<h4 class="panel-title">Additional info <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
								</a>
							</div>
							<div id="panelref" class="panel-collapse collapse">
								<div class="panel-body">
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
										<span class="pull-right sec-form" id="edit-ref"><i class="fa fa-pencil-square-o"></i></span>
									<?php } ?>
									<div id="ref-c">
										<?php
										$ref = get_post_meta(get_the_ID(),'video_options_refr',true);
										// $shipping_details = get_user_meta(get_the_author_id(), '_wcv_shipping',true);
										 
										// $shipping_policy = ( is_array( $shipping_details ) && array_key_exists( 'shipping_policy', $shipping_details ) ) ? $shipping_details[ 'shipping_policy' ] : ''; 
										 //$shipping_policy   =   array_column($shipping_policys, 'shipping_policy');
										// $return_policy = ( is_array( $shipping_details ) && array_key_exists( 'return_policy', $shipping_details ) ) ? $shipping_details[ 'return_policy' ] : ''; 
									   
									    $return_policy = get_post_meta(get_the_ID(),'_wcv_shipping_return_policy',true);
										$shipping_policy = get_post_meta(get_the_ID(),'_wcv_shipping_policy',true);
										
										$_weight = get_post_meta(get_the_ID(),'_weight',true);
										$_length = get_post_meta(get_the_ID(),'_length',true);
										$_width = get_post_meta(get_the_ID(),'_width',true);
										$_height = get_post_meta(get_the_ID(),'_height',true);
										
										if($ref){
											echo $ref;
										}else{
											echo 'No additional info given';
										}
										 
										
										 
										?>
										 
										
									</div>
									<?php
                                 if($_weight){
									 echo '<h6 style="font-weight: bold;">Weight </h6><span>'; 
									 echo $_weight . 'kg</span>';
									 }
									 
								 if($_length || $_width || $_height){
									 
									 echo '<h6 style="font-weight: bold;">Dimensions </h6>'; 
									 
									 if($_length){
									 echo '<span style="font-size: 18px;">Length: '.$_length . 'cm, </span>';
									 }
									 
									 if($_width){
									 echo '<span style="font-size: 18px;">Width: '.$_width . 'cm, </span>';
									 }
									 
									 if($_height){
									 echo '<span style="font-size: 18px;">Height: '.$_height . 'cm </span>';
									 }
									 
									 
									 }	 
									 
                             if($shipping_policy){
											echo '<h6 style="font-weight: bold;"> Shipping Policy </h6>';
											echo $shipping_policy;
										}
										
										if($return_policy){
											echo '<h6 style="font-weight: bold;"> Return Policy </h6>';
											echo $return_policy;
										}
										 
							?>
								</div>
							</div>
							
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a data-toggle="collapse" href="#panelshare">
									<h4 class="panel-title">Share <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
								</a>
							</div>
							<div id="panelshare" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="block-general">
										<div id="option-social">
											<div class="social-share-left"><a class="opt-facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;t=4th+Test" target="_blank"><span class="socicon-facebook"></span>facebook</a></div>
											<div class="social-share-left"><a class="opt-twitter" href="http://twitter.com/home?status=4th+Test�http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F" target="_blank"><span class="socicon-twitter"></span>twitter</a></div>
											<div class="social-share-left"><a class="opt-googleplus" href="https://plus.google.com/share?url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a></div>
											<div class="social-share-left"><a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;title=4th+Test" target="_blank"><span class="socicon-linkedin"></span>linkedin</a></div>
											<div class="social-share-left"><a class="opt-dig" href="http://digg.com/submit?url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;title=4th+Test" target="_blank"><span class="socicon-digg"></span>dig</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<ul class="stats sicon grey-background">
					<?php global $wpdb ; ?>
					<li><i class="fa fa-eye"></i> <?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa  fa-heart"></i>
						<?php
						$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
						$result1 = $wpdb->get_results($sql_aux);
						echo count($result1);
						?> Favorites</li>
					<!--end favorite -->
					<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><i class="fa fa-shopping-bag" aria-hidden="true"></i><span id="units_sold-sapn"></span></li>
				</ul>
				<script>
					jQuery(document).ready(function($) {
						$('.units_sold').prependTo("#units_sold-sapn");
						//$('.units_sold').css({'display':'inline-block'});
					});

				</script>

              <!-- button for product playlist by nazmin-->      
                 <?php if( is_user_logged_in() ){
				$profile_id = get_current_user_id();
				$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =4  ORDER BY id DESC");
				if($data){ ?>
					<div class="addto_playlist grey-background padding5_10">
						<a herf="#" id="fileplaytest" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
					</div>
			<?php } }  ?>
				<div class="entry grey-background group-sort-tags-des">
					<div id="category-tags">
						<?php //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

						<?php //echo $product->get_tags( ', ', '<i class="fa fa-tags" aria-hidden="true"></i><span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', ' </span>' ); ?>
						<?php
						// Link Pages Navigation
						$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
						wp_link_pages( $args );


						// Display the Tags
						$tagval = '';?>
						<?php $all_tags = array();?>
						<?php $all_tags = get_the_terms( get_the_ID(), 'product_tag' ); ?>
						<?php if($all_tags){
							$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
						}

						if(get_the_author_id() == get_current_user_id()){
							$before1 = '<span><i class="fa fa-tags"></i>Tags';
							$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
						}
						$before = '<span id="tags-val">';

						$after = '</span>';


						?>

						<?php if( is_user_logged_in() ){
							$profile_id = get_current_user_id();
							$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id  ORDER BY id DESC");
							//if($data){ ?>
							<!--<div class="addto_playlist grey-background padding5_10">
								<a herf="#" id="videostest" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
							</div> -->
							<?php // }
						} ?>
						<div class="post-tags grey-background padding5_10">
							<?= $before1;?>
							<?php
							$tagSelected = get_the_terms( get_the_ID(), 'product_tag' );
							$tagsNameArray = array();
							$tagsSlugArray = array();
							$tagsIDArray = array();
							foreach($tagSelected as $key=>$val)
							{
								$name = $val->name;
								$slug = $val->slug;
								$term_id = $val->term_id;
								$tagsNameArray[] = $name;
								$tagsSlugArray[] = $slug;
								$tagsIDArray[] = $term_id;
							}
							//the_tags( $before,', ',$after );
							 echo '<span id="tags-val">';
								if ($all_tags) {
									foreach($all_tags as $tagss) {
											echo '<a href="">' . $tagss->name . ',</a>'; 
									}
								}
								echo '</span>';
								
							?>
						</div>


						<?php

						if($all_tags){
							foreach($all_tags as $tag){
								$tagval .= $tag->name.',';
							};
						}

						rtrim($tagval, ",");

						if(get_the_author_id() == get_current_user_id()){
							?>

							<div class="tags-forminput" style="display:none;">
								<input name="tagsinput" class="tagsinput" id="tagsinput" value="<?= $tagval;?>">
								<input name="userid" type="hidden" id="tags_postid" value="<?= get_the_id();?>">
								<button type="button" class="btn btn-success" id="tagsave">Save</button>
								<button type="button" class="btn btn-primary" id="tagcancel">Cancel</button>
							</div>

						<?php }  ?>


					</div>
					<?php comments_template(); ?>
					<?php  do_action( 'woocommerce_product_meta_end' ); ?>
				</div>
			</div>

		</div>

		<div class="modal fade" id="modeldesref_section" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Add Description</h4>
					</div>
					<div class="modal-body">
						<form method="post" id="desref-form">
							<input type="hidden" name="postdataname" id="postdataname">

							<input type="hidden" name="postid" id="postid" value="<?= get_the_ID();?>">
							<div id="">
								<textarea rows="10" id="postdataval" name="postdataval"></textarea>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="desref-submit" class="btn btn-primary">Submit changes</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="moreshipped" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Shipped To </h4>
					</div>
					<div class="modal-body">
					<?php 
					$storename = get_the_author_meta( 'pv_shop_name' , get_the_author_meta('ID') );
					$author_id=$post->post_author;
					$naxlogin = get_userdata($author_id);
					?>
					<div class="sold-by"> 	<span class="titlesoldby">Sold By<span class="snamesoldby">
								<a href="<?php echo home_url(); ?>/user/<?php echo $naxlogin->user_nicename; ?>/?store" ><?php if(!empty($storename)) { 
								 echo $storename;
								} else {
								?>
								<?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?>
								<?php } ?>
							</a></span></span>
						 
					</div>
					<div class="well well-sm">
			           <strong>Views</strong>
			           <div class="btn-group">
				          <a href="#" id="liststore" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
				        </span>List</a> <a href="#" id="gridstore" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
			            </div>
		            </div>
					 <div id="productssss" class="list-group">
						 <div class="shipped-by">
						 
					<?php  
					      $shipping_country = '';
						  $spacicecountry = get_post_meta( get_the_ID(), '_spacicecountry');
				          $spacicecountrys = explode( ",", $spacicecountry[0] ); 
						  $shipping_details = get_post_meta( get_the_ID(), '_wcv_shipping_rates',  true ); 
								   foreach($spacicecountrys as $spacicecountryss)
							        {
										//woocommerce_shipping_country_full_by_isocode
										
										
										$country_full_name = apply_filters( 'woocommerce_shipping_country_full_by_isocode', $spacicecountryss);
									    
								        $shipping_country =  $country_full_name; 
										
                                        if($spacicecountryss == 'GB')
										{
										 $spacicecountryss = 'UK';	
										}										
										$shipping_rate    = '';
										//$rates = 0;
		                                foreach ( $shipping_details as $shipping_rate ) {
			                             if($spacicecountryss == $shipping_rate['country'])
			                               {
				                              $shipping_rate =  $shipping_rate['fee'];
			                                
                        ?>										
							            <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 grid-group-item changeStyleGrid">
										<div class="layout-2-wrapper">
										   <h4>Country</h4>
										<?php	echo $shipping_country;	?>
										  <h4>Shipping Rate</h4>
										  <span class="amount" ><?php	echo '$' . $shipping_rate;	?></span>
										  <div class="listview" style="display:none">
										   <h4>Shipping Policy</h4>
										   </div>
										</div></div>
						
						<?php	} } }	?>
					   </div>
					<style>.shipped-counties{color: #337ab7;font-weight: bold; font-size: 13px;}
					 .titlesoldby{font-size: 22px !important;
                     padding: 6px !important;
					 text-transform: uppercase;
					 } 
					 .snamesoldby{color: #337ab7;}
					 #productssss .changeStyleGrid {
                            min-height: 130px !important;
                           text-align: center;
						   }
					 #productssss .changeStyleList { height: 139px !important;}	  
                     @media (min-width: 768px){
                          #moreshipped .modal-dialog {
                            width: 700px; margin: 30px auto;
							}	
					 }	
                    #productssss .changeStyleList .layout-2-wrapper {
                         margin-bottom: 20px;
                         border: 1px solid #EEEEEE;
                         padding: 15px;
                         box-shadow: 0 0 10px 1px #666;
                         border-radius: 5px;
						min-height: auto;
                     }	
                    #productssss .changeStyleGrid .layout-2-wrapper {
                         margin-bottom: 20px;
                         border: 1px solid #EEEEEE;
                         padding: 15px;
                         box-shadow: 0 0 10px 1px #666;
                         border-radius: 5px;
						min-height: 156px !important;
                     }	
                  				  
				   </style>
					</div>
					 <h2> &nbsp; &nbsp; </h2>
					</div>
				</div>
			</div>
		</div>
		<!-- Lightbox popup -->



		<script>

			$(document).ready(function(){
				var $ = jQuery.noConflict();

				CKEDITOR.inline( 'postdataval' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
				CKEDITOR.editorConfig = function( config ) {
					config.font_defaultLabel = 'Arial';
					config.fontSize_defaultLabel = '44px';
				};

				var $ = jQuery.noConflict();
				$('input#tagsinput').tagsinput({
					confirmKeys: [32]
				});

				$('#tagcancel').click(function(e){
					e.preventDefault();

					$('.tags-forminput').hide();
					$('#tags-val').show();
					$('#tagsinput').tagsinput('removeAll');
					$('#tagsinput').tagsinput('add', '<?= $tagval;?>', {preventPost: true});
				})

			})

			var $ = jQuery.noConflict();


			$('#tags-edit').click(function(e){
				e.preventDefault();
				$('#tags-val').hide();
				$('.tags-forminput').show();
			});

			$('#tagcancel').click(function(e){
				e.preventDefault();
				$('.tags-forminput').hide();
			})

			/*$('#tagsave').click(function(e){
				e.preventDefault();

				var tags_postid = $('#tags_postid').val();
				var tagsinput = $('#tagsinput').val();

				$.ajax({
					url: admin_ajax,
					type: "POST",
					data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
				}).done(function(data) {
					location.reload();
				});

				$('.tags-forminput').hide();
				$('#tags-val').show();

			});*/
			
			$('#tagsave').click(function(e){
				e.preventDefault();

				var tags_postid = $('#tags_postid').val();
				var tagsinput = $('#tagsinput').val();

				$.ajax({
					url: admin_ajax,
					type: "POST",
					data: {action : 'user_product_tags_edits',tags_postid: tags_postid,product_tags: tagsinput}
				}).done(function(data) {
					alert(data);
					location.reload();
				});

				$('.tags-forminput').hide();
				$('#tags-val').show();

			});



			$('#edit-des').click(function(e){
				var $ = jQuery.noConflict();

				e.preventDefault();

				$('#myModalLabel').html('Add Description');
				$('#postdataname').val('desc');

				$('.cke_textarea_inline').html($('#desf-c').html());

				$('#modeldesref_section').modal();
			})


			$('#edit-ref').click(function(e){
				e.preventDefault();

				var $ = jQuery.noConflict();

				$('#myModalLabel').html('Add Reference');
				$('#postdataname').val('refe');

				$('.cke_textarea_inline').html($('#ref-c').html());

				$('#modeldesref_section').modal();
			})

			$('#desref-submit').click(function(e){
				var $ = jQuery.noConflict();
				$('#desref-form').submit();
			})
			
			$('#morecountry').click(function(e){
				e.preventDefault();
                 //#morecountry  moreshipped
				var $ = jQuery.noConflict();
				
				$('#moreshipped').modal();
			})
       
	   
	        $('#liststore').click(function(event){
    	               event.preventDefault();
					 $('#productssss .item').removeClass('grid-group-item changeStyleGrid');  
    	             $('#productssss .item').addClass('list-group-item changeStyleList');
					 

                 });
	   
	        $('#gridstore').click(function(event){
    	            event.preventDefault();
    	           $('#productssss .item').removeClass('list-group-item changeStyleList');
    	           $('#productssss .item').addClass('grid-group-item changeStyleGrid');
               });

		</script>
		<?php
//$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId  AND type =1 ORDER BY id DESC");
$profile_id_product = get_current_user_id();
$data_products = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id_product AND type =4 ORDER BY id DESC");
//$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $userId AND type =3 ORDER BY id DESC");

?>	
<!-- prodyct playlist model by nazmin -->
<div class="modal fade" id="filetest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Product to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file<?php echo $countproduct; ?>">
				<label>Select Playlist to add Products</label>
				<select name="plval">
				<?php foreach($data_products as $dataproduct): ?>
					<option value="<?= $dataproduct->id; ?>"><?= $dataproduct->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value="<?php echo get_the_ID(); ?>" name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
<!-- <script>
	jQuery('#form-add-playlist-file').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest').click(function(e){
		jQuery('#filetest_mod').modal();
	});
</script> -->

		<?php do_action( 'woocommerce_product_thumbnails' ); ?>


	</div>

<?php } ?>	

<!-- checkout pop up -->
<script>

			$(document).ready(function(){
				var $ = jQuery.noConflict();
				 $(".video-heading").submit(function(e){
							e.preventDefault();
							var productcal = $(".addtocart").val();
		
							$.ajax({
								url: admin_ajax,
								type: "POST",
								data: {action : 'adtocart_call',productid : productcal},
								success : function(data){
								 $.ajax({
									url: admin_ajax,
									type: "POST",
									data: {action : 'checkout_call'},
									success : function(response){
								 //alert(data);	
									$('#checkoutcontent').html(response);
									$('#checkoutpage').fadeIn();
								 }
								})							
					        }
			           })
                  });
				  
				  $("#js-modal-closecheck").click(function() {
					   var productcal = $(".addtocart").val();
					   
						$(".modal-box, .modal-overlay").fadeOut(500, function() {
						
						
						 $.ajax({
								url: admin_ajax,
								type: "POST",
								data: {action : 'checkout_menu_popup'},
								success : function(response){
								 $('.cart-contents').html(response);
								 $(".modal-overlay").remove();
								}
						   });//checkout_menu_popup
						})
						
						

					});
			});
</script>

<style>
.woocommerce-checkout{display:none;}
</style>
<script>

$(document).ready(function(){
	$('#buynowproduct').click(function(e){
		e.preventDefault();
			$('.woocommerce-checkout').fadeIn();	
      	});	

           $('#susription_submit').click(function(e){
	  
   var currentuserid = "<?php echo get_current_user_id(); ?>";
   var subscriberid = "<?php echo $author_id; ?>";
   
   if (currentuserid == 0){
			alert("You must be logged in to subscribe this user channel.");
		}else{
			
   if(currentuserid == subscriberid)
   {
       alert("You can not subscribe Yourself.");
	   
   }else {
	   //alert(subscriberid);
          $.ajax({
          url: admin_ajax,
          type: "POST",
          data:{
			    action:'subscribe_channel_from_post',
		        subscriberid:subscriberid,
				currentuserid:currentuserid
				},
          success: function(responce){
		     alert('Successfully subscribe to this user channel');
			 $('#susription_submit').fadeOut();
			 $('#unsusription_submit').fadeIn();
           }
         });
       }   
	}
  });
  
  
  $('#unsusription_submit').on('click',function(e){
	  
   var currentuserid = "<?php echo get_current_user_id(); ?>";
   var subscriberid = "<?php echo $author_id; ?>";
    if (currentuserid == 0){
			alert("You must be logged in to unsubscribe this user channel.");
		}else{
   if(currentuserid == subscriberid)
   {
       alert("You can not unsubscribe Yourself.");
	   
   }else {
	   //alert(subscriberid);
          $.ajax({
          url: admin_ajax,
          type: "POST",
          data:{
			    action:'subscribe_channel_from_post_delete',
		        subscriberid:subscriberid,
				currentuserid:currentuserid
				},
          success: function(responce){
		     alert('Successfully unsubscribe to this user channel');
			 $('#unsusription_submit').fadeOut();
			 $('#susription_submit').fadeIn();
			 
           }
         });
       }   
	}
  });
  
   });
</script>				
		<style>
		  .modal-box .modal-body {
					width: 95%!important;
					max-width: 100% !important;
					border-bottom: none;
					border-radius: 4px;
					box-shadow: none;
					border: none;
					background: #fff;
					margin: 30px auto;
			}
			
			#checkoutpage .modal-dialog {
				width: 900px;
				margin: 30px auto;
				max-width: 100% !important;
				}
			
			#checkoutpage .woocommerce .col2-set .col-2, .woocommerce-page .col2-set .col-2 {
				float: right;
				width: 100%;
			}
			#checkoutpage .woocommerce .col2-set .col-1, .woocommerce-page .col2-set .col-1 {
				float: left;
				width: 100%;
			}
			.shipping_address, .create-account{ display:none; }
			.woocommerce #payment #place_order, .woocommerce-page #payment #place_order {
				float: right;
				background: #3498db;
				}
			.woocommerce .woocommerce-error:before, .woocommerce .woocommerce-info:before, .woocommerce .woocommerce-message:before {
              left: 0em !important;
             }	
			 
			 #messagetext {
				padding: 1em 1em 1em 1em!important;
				margin: 0 0 2em!important;
				position: relative;
				background-color: #f7f6f7;
				color: #515151;
				border-top: 3px solid #1e85be;
				list-style: none!important;
				width: auto;
				word-wrap: break-word;
				}
			 
		</style>
		<div class="modal-box" id="checkoutpage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="js-modal-closecheck" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					    <h2 style="color: #1e85be;" class="modal-title">Checkout</h2>
					</div>
					<div class="modal-body">
					  <div id="messagetext">
		                    <h3><a id="buynowproduct" href="">Buy Now</a> or <a id="js-modal-close" href="">Add to Cart</a></h3>
		               </div>
					<div id="checkoutcontent" >
					</div>
				 <h2> &nbsp; &nbsp; </h2>
				</div></div>
			</div>
		</div>	
<?php 
function insert_thumbnail_id_video_product($_productvideoimage,  $attachementidss, $postid) {
	
if($attachementidss == ''){
$pid = 	$postid;
require_once(ABSPATH . "wp-admin" . '/includes/image.php');
require_once(ABSPATH . "wp-admin" . '/includes/file.php');
require_once(ABSPATH . "wp-admin" . '/includes/media.php');	

$upload_dir = wp_upload_dir(); 
$image_data = file_get_contents($_productvideoimage); // Get image data
           $filename   = basename($_productvideoimage); // Create image file name

            // Check folder permission and define file location
            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }

            // Create the image  file on the server
            file_put_contents( $file, $image_data );
      			// Check image file type
      			$wp_filetype = wp_check_filetype( $filename, null );
      			// Set attachment data
      			$attachment = array(
      				'post_mime_type' => $wp_filetype['type'],
      				'post_title'     => sanitize_file_name( $filename ),
      				'post_content'   => '',
      				'post_status'    => 'inherit'
      			);
      			// Create the attachment
      			$attach_id = wp_insert_attachment( $attachment, $file, $pid );
				
				 // Include image.php
            require_once(ABSPATH . 'wp-admin/includes/image.php');
      			// Define attachment metadata
      		 $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

      			// Assign metadata to attachment
      			wp_update_attachment_metadata( $attach_id, $attach_data );

      			// And finally assign featured image to post
      			set_post_thumbnail( $pid, $attach_id );
				
				//return "test";
}

}
?>		