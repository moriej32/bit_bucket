<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

?>
<script>
var jquery = jQuery.noConflict();
$(document).ready(function (e) {
	
$('.single-page').css("min-height", $(window).height()-230 + "px" );
$('.single-page').css("padding-top","50px" );

});
</script>
<style>
.woocommerce a.button, .woocommerce a.button:hover{
    font-size: 100%;
    margin: 0;
    line-height: 1;
    cursor: pointer;
    position: relative;
    font-family: inherit;
    text-decoration: none;
    overflow: visible;
    padding: .618em 1em;
    font-weight: 700;
    border-radius: 3px;
    left: auto;
    color: #fff;
    background-color: #21648F;
    border: 0;
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
}
</style>
<p class="cart-empty">
	<?php _e( 'Your checkout is currently empty.', 'woocommerce' ) ?>
</p>

<?php do_action( 'woocommerce_cart_is_empty' ); ?>

<p class="return-to-shop">
	<a class="button wc-backward" href="http://www.doityourselfnation.org/bit_bucket/<?php /* echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) );*/ ?>"> 
		<?php _e( 'Return To Home', 'woocommerce' ) ?>
	</a>
</p>
