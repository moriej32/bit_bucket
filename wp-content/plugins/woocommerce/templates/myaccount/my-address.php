<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">
<style>.shop_table.shop_table_responsive.my_account_orders {width: 100%} .page-template-default .entry table {  margin: 0px !important; }.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }</style>

<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) {
	$page_title = apply_filters( 'woocommerce_my_account_my_address_title', __( 'My Addresses', 'woocommerce' ) );
	$get_addresses    = apply_filters( 'woocommerce_my_account_get_addresses', array(
		'billing' => __( 'Billing Address', 'woocommerce' ),
		'shipping' => __( 'Shipping Address', 'woocommerce' )
	), $customer_id );
} else {
	$page_title = apply_filters( 'woocommerce_my_account_my_address_title', __( 'My Address', 'woocommerce' ) );
	$get_addresses    = apply_filters( 'woocommerce_my_account_get_addresses', array(
		'billing' =>  __( 'Billing Address', 'woocommerce' )
	), $customer_id );
}

$col = 1;
?>
<style>
	header.title{
		padding-bottom: 25px;
		padding-top: 20px;
	}
.myaccount_address{    color: #43454b;
	font-weight: 500;}
header.title h3:first-child{
font-size: 1.618em;
font-weight: 600;    width: 90%;
	float: left;    margin-top: 0px;}

	header.title a i{    color: #96588a;}

	.col-1.address{
		width: 46.6666666667%;
		float: left;
		margin-right: 6.6666666667%;
	}
	.col-2.address{
		width: 46.6666666667%;
		float: right;
	}
</style>

<!--<h2><?php // echo $page_title; ?></h2> -->

<p class="myaccount_address">
	<?php echo apply_filters( 'woocommerce_my_account_my_address_description', __( 'The following addresses will be used on the checkout page by default.', 'woocommerce' ) ); ?>
</p>

<?php if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) echo '<div class="col2-set addresses">'; ?>

<?php foreach ( $get_addresses as $name => $title ) : ?>

	<div class="col-<?php echo ( ( $col = $col * -1 ) < 0 ) ? 1 : 2; ?> address">
		<header class="title">
			<h3><?php echo $title; ?></h3>
			<a href="<?php echo site_url().'/edit_address_'.$name; ?>" class="edit"><?php _e( '<i class="fa fa-pencil-square-o"></i>', 'woocommerce' ); ?></a>
		</header>
		<address>
			<?php
				$address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
					'first_name'  => get_user_meta( $customer_id, $name . '_first_name', true ),
					'last_name'   => get_user_meta( $customer_id, $name . '_last_name', true ),
					'company'     => get_user_meta( $customer_id, $name . '_company', true ),
					'address_1'   => get_user_meta( $customer_id, $name . '_address_1', true ),
					'address_2'   => get_user_meta( $customer_id, $name . '_address_2', true ),
					'city'        => get_user_meta( $customer_id, $name . '_city', true ),
					'state'       => get_user_meta( $customer_id, $name . '_state', true ),
					'postcode'    => get_user_meta( $customer_id, $name . '_postcode', true ),
					'country'     => get_user_meta( $customer_id, $name . '_country', true )
				), $customer_id, $name );

				$formatted_address = WC()->countries->get_formatted_address( $address );

				if ( ! $formatted_address )
					_e( 'You have not set up this type of address yet.', 'woocommerce' );
				else
					echo $formatted_address;
			?>
		</address>
	</div>

<?php endforeach; ?>

<?php if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) echo '</div>'; ?>
<script>
	$('.wcv-navigation ul li:first-child').removeClass('active');
	$('#dashboard-menu-item-addresses').addClass('active');
</script>
