<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">
<style>.shop_table.shop_table_responsive.my_account_orders {width: 100%} .page-template-default .entry table {  margin: 0px !important; }.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }</style>


<?php
/**
 * My Orders
 *
 * Shows recent orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( $downloads = WC()->customer->get_downloadable_products() ) : ?>

	<?php do_action( 'woocommerce_before_available_downloads' ); ?>

	<!--<h2><?php /// echo apply_filters( 'woocommerce_my_account_my_downloads_title', __( 'Available Downloads', 'woocommerce' ) ); ?></h2> -->



	<table class="shop_table shop_table_responsive my_account_orders">
		<thead>
		<tr  class="order">
			<th class="order-number"><span class="nobr">File</span></th>
			<th class="order-date"><span class="nobr">Remaining</span></th>
			<th class="order-status"><span class="nobr">Expires</span></th>
			<th class="order-actions"><span class="nobr">&nbsp;</span></th>
		</tr>	</thead>

		<?php foreach ( $downloads as $download ) : ?>
		<tbody><tr>

					<?php
					do_action( 'woocommerce_available_download_start', $download );

					if ( is_numeric( $download['downloads_remaining'] ) )
						?>
			        <td class="order-number" data-title="Order">
						<?php echo $download['download_name']; ?>
					</td>
						<td class="order-date" data-title="Date">
							<?php
						echo apply_filters( 'woocommerce_available_download_count', '<span class="count">' . sprintf( _n( '%s download remaining', '%s', $download['downloads_remaining'], 'woocommerce' ), $download['downloads_remaining'] ) . '</span> ', $download );
                        ?>
						</td>
		      	<td class="order-status" data-title="Status">
					Never
				</td>
			    <td class="order-actions" data-title="&nbsp;">
					<?php
					echo apply_filters( 'woocommerce_available_download_link', '<a id="download-link-product" href="' . esc_url( $download['download_url'] ) . '">Download</a>', $download );
					?>
				</td>
			<?php
			do_action( 'woocommerce_available_download_end', $download );
			?>
		</tr></tbody>
		<?php endforeach; ?>
      </table>












	<?php do_action( 'woocommerce_after_available_downloads' ); ?>

<?php endif; ?>
<script>
	$('.wcv-navigation ul li:first-child').removeClass('active');
	$('#dashboard-menu-item-downloads').addClass('active');
</script>
