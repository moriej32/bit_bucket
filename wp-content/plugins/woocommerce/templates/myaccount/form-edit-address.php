<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">

<style>p input {
		width: 50%;
	}.woocommerce form .form-row .select2-container {
		 width: 100%;
		 line-height: 2em;
	 }select#billing_country {
		  width: 50%;
		  background: red;
		  background-color: #f2f2f2;
		  border-radius: 2px;
		  border: 1px solid #f2f2f2;    box-shadow: inset 0 1px 1px rgba(0,0,0,.125);
		  height: 44px;
	  }select#billing_country {
		width: 50%;
	}select#shipping_country {
		  width: 50%;
		  background: red;
		  background-color: #f2f2f2;
		  border-radius: 2px;
		  border: 1px solid #f2f2f2;    box-shadow: inset 0 1px 1px rgba(0,0,0,.125);
		  height: 44px;
	  }select#shipping_country {
		width: 50%;
	}input.button {
		 line-height: 31px;
	 }abbr.required {
		color: red;
	}label {
		display: inline-block;
		max-width: 100%;
		margin-bottom: 5px;
		font-weight: 700;    width: 100%;
	}.input-text, .select2-choice {
		padding: .618em;
		background-color: #f2f2f2;
		color: #43454b;
		outline: 0;
		border: 0;
		-webkit-appearance: none;
		border-radius: 2px;
		box-sizing: border-box;
		font-weight: 400;
		box-shadow: inset 0 1px 1px rgba(0,0,0,.125);
	} .shop_table.shop_table_responsive.my_account_orders {width: 100%} .page-template-default .entry table {  margin: 0px !important; }.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }</style>

<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$page_title   = ( $load_address === 'billing' ) ? __( 'Billing Address', 'woocommerce' ) : __( 'Shipping Address', 'woocommerce' );
?>

<?php wc_print_notices(); ?>

<?php if ( ! $load_address ) : ?>

	<?php wc_get_template( 'myaccount/my-address.php' ); ?>

<?php else : ?>

	<form method="post">

		<h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title ); ?></h3>

		<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

		<?php foreach ( $address as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

		<?php endforeach; ?>

		<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

		<p>
			<input type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save Address', 'woocommerce' ); ?>" />
			<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
			<input type="hidden" name="action" value="edit_address" />
		</p>

	</form>

<?php endif; ?>
<script>
	$('.wcv-navigation ul li:first-child').removeClass('active');
	$('#dashboard-menu-item-addresses').addClass('active');
</script>
