{% variable containerClasses %}
	wcContainer 
	{% if showMessageSubmitButton %} wcControlsButtonsIncluded {% endif showMessageSubmitButton %}
	{% if enableImagesUploader %} wcControlsButtonsIncluded {% endif enableImagesUploader %}
	{% if enableAttachmentsUploader %} wcControlsButtonsIncluded {% endif enableAttachmentsUploader %}
    {% if showEmoticonInsertButton %} wcControlsButtonsIncluded {% endif showEmoticonInsertButton %}
	{% if showUsersList %} wcUsersListIncluded wcUsersListEnabled {% endif showUsersList %}
	{% if sidebarMode %} wcSidebarMode {% endif sidebarMode %}
{% endvariable containerClasses %}

<link rel='stylesheet' id='wise_chat_theme_{{ chatId }}-css' href='{{ themeStyles }}' type='text/css' media='all' />
	<script>
jQuery(document).ready(function($) {
	$('.wcCustomizations').on('click', '.creatgroupButtonss', function( e ) {
		$('#chatgroupmodels').fadeIn();
	});
	$('.modal-header').on('click', 'button.close', function( e ) {
		 
		$('#chatgroupmodels').fadeOut();
	});
	
	$('.wcCustomizations').on('click', '.addorremovememeber', function( e ) {
		$('#addorremovememeber').fadeIn();
	});
	$('.modal-header').on('click', 'button.close', function( e ) {
		 
		$('#addorremovememeber').fadeOut();
	});
	
});
</script>

<div id='{{ chatId }}' class='{{ containerClasses }}' {% if sidebarMode %}style="display: none"{% endif sidebarMode %}>
	{% if showWindowTitle %}
		<div class='wcWindowTitle {% if sidebarMode %}wcInvisible{% endif sidebarMode %}'>
            {% if sidebarMode %}<sup class="wcUnreadMessagesFlag">*</sup>{% endif sidebarMode %}
            {{ windowTitle }}&#160;
            {% if sidebarMode %}<a href="javascript://" class="wcWindowTitleMinMaxLink"></a>{% endif sidebarMode %}
        </div>
	{% endif showWindowTitle %}

    {% if allowToReceiveMessages %}
        <div class="wcTopControls wcInvisible">
            <a href="javascript://" class="wcTopControlsButton wcUserListToggle wcInvisible"></a>
        </div>

        {% if inputControlsBottomLocation %}
            {% if allowPrivateMessages %}
                <div class='wcMessagesContainersTabs wcInvisible'> </div>
            {% endif allowPrivateMessages %}
            
            <div class='wcMessages wcMessages{{ channelId }} {% if sidebarMode %}wcInvisible{% endif sidebarMode %}'>{{ messages }}</div>
            
            {% if showUsersList %}
                <div class='wcUsersList'>{{ usersList }}</div>
            {% endif showUsersList %}

            {% if showUsersCounter %}
                <div class='wcUsersCounter'>
                    {{ messageTotalUsers }}: <span>{{ totalUsers }}{% if channelUsersLimit %}&nbsp;/&nbsp;{{ channelUsersLimit }} {% endif channelUsersLimit %}</span>
                </div>
            {% endif showUsersCounter %}
        {% endif inputControlsBottomLocation %}
    {% endif allowToReceiveMessages %}

    {% if allowToSendMessages %}
        <div class="wcOperationalSection">
            <div class="wcControls wcControls{{ channelId }} {% if sidebarMode %}wcInvisible{% endif sidebarMode %}">
                {% if showUserName %}
                    <span class='wcCurrentUserName'>{{ currentUserName }}{% if isCurrentUserNameNotEmpty %}:{% endif isCurrentUserNameNotEmpty %}</span>
                {% endif showUserName %}

                {% if showMessageSubmitButton %}
                    <input type='button' class='wcSubmitButton' value='{{ messageSubmitButtonCaption }}' />
                {% endif showMessageSubmitButton %}

                {% if enableAttachmentsUploader %}
                    <a href="javascript://" class="wcToolButton wcAddFileAttachment" title="{{ messageAttachFileHint }}"><input type="file" accept="{{ attachmentsExtensionsList }}" class="wcFileUploadFile" title="{{ messageAttachFileHint }}" /></a>
                {% endif enableAttachmentsUploader %}

                {% if enableImagesUploader %}
                    <a href="javascript://" class="wcToolButton wcAddImageAttachment" title="{{ messagePictureUploadHint }}"><input type="file" accept="image/*;capture=camera" class="wcImageUploadFile" title="{{ messagePictureUploadHint }}" /></a>
                {% endif enableImagesUploader %}

                {% if showEmoticonInsertButton %}
                    <a href="javascript://" class="wcToolButton wcInsertEmoticonButton" title="{{ messageInsertEmoticon }}"></a>
                {% endif showEmoticonInsertButton %}

                <div class='wcInputContainer'>
                    {% if multilineSupport %}
                        <textarea class='wcInput' maxlength='{{ messageMaxLength }}' placeholder='{{ hintMessage }}'></textarea>
                    {% endif multilineSupport %}
                    {% if !multilineSupport %}
                        <input class='wcInput' type='text' maxlength='{{ messageMaxLength }}' placeholder='{{ hintMessage }}' title="{{ messageInputTitle }} " />
                    {% endif multilineSupport %}

                    <progress class="wcMainProgressBar" max="100" value="0" style="display: none;"> </progress>
                </div>

                {% if enableAttachmentsPanel %}
                    <div class="wcMessageAttachments" style="display: none;">
                        <img class="wcImageUploadPreview" style="display: none;" />
                        <span class="wcFileUploadNamePreview" style="display: none;"></span>
                        <a href="javascript://" class="wcAttachmentClear"><img src='{{ baseDir }}/gfx/icons/x.png' class='wcIcon' /></a>
                    </div>
                {% endif enableAttachmentsPanel %}
            </div>

            {% if showCustomizationsPanel %}
                <div class='wcCustomizations'>
                    <a href='javascript://' class='wcCustomizeButton'>{{ messageCustomize }}</a>
					<a href='#'  class='creatgroupButtonss'>Create Group</a>
					<a href='#'  class='addorremovememeber'>Add/Remove Group Member</a>
                    <div class='wcCustomizationsPanel' style='display:none;'>
                        {% if allowChangeUserName %}
                            <div class="wcCustomizationsProperty">
                                <label>{{ messageName }}: <input class='wcUserName' type='text' value='{{ currentUserName }}' required /></label>
                                <input class='wcUserNameApprove' type='button' value='{{ messageSave }}' />
                            </div>
                        {% endif allowChangeUserName %}
                        {% if allowMuteSound %}
                            <div class="wcCustomizationsProperty">
                                <label>{{ messageMuteSounds }} <input class='wcMuteSound' type='checkbox' value='1' {% if muteSounds %} checked="1" {% endif muteSounds %} /></label>
                            </div>
                        {% endif allowMuteSound %}
                        {% if allowChangeTextColor %}
                            <div class="wcCustomizationsProperty">
                                <label>{{ messageTextColor }}: <input class='wcTextColor' type='text' value="{{ textColor }}" /></label>
                                <input class='wcTextColorReset' type='button' value='{{ messageReset }}' />
                            </div>
                        {% endif allowChangeTextColor %}
                    </div>
                </div>
            {% endif showCustomizationsPanel %}
        </div>
    {% endif allowToSendMessages %}

    {% if allowToReceiveMessages %}
        {% if inputControlsTopLocation %}
            {% if allowPrivateMessages %}
                <div class='wcMessagesContainersTabs wcInvisible'> </div>
            {% endif allowPrivateMessages %}

            <div class='wcMessages wcMessages{{ channelId }} {% if sidebarMode %}wcInvisible{% endif sidebarMode %}'>{{ messages }}</div>

            {% if showUsersList %}
                <div class='wcUsersList'>{{ usersList }}</div>
            {% endif showUsersList %}
            {% if showUsersCounter %}
                <div class='wcUsersCounter'>
                    {{ messageTotalUsers }}: <span>{{ totalUsers }}{% if channelUsersLimit %}&nbsp;/&nbsp;{{ channelUsersLimit }} {% endif channelUsersLimit %}</span>
                </div>
            {% endif showUsersCounter %}
        {% endif inputControlsTopLocation %}
    {% endif allowToReceiveMessages %}

    <div class="wcVisualLogger wcInvisible">
        <div class="wcVisualLoggerInner"> </div>
    </div>

    {% if sidebarMode %}
        <div class="wcSidebarModeMobileNavigation">
            <a href="javascript://" class="wcSidebarModeMobileNavigationButton wcSidebarModeUsersListToggler">&#160;</a>
            <a href="javascript://" class="wcSidebarModeMobileNavigationButton wcSidebarModeWindowsNavigationRight wcInvisible">&#160;</a>
            <a href="javascript://" class="wcSidebarModeMobileNavigationButton wcSidebarModeWindowsNavigationLeft wcInvisible">&#160;</a>
            <br class="wcClear" />
        </div>
    {% endif sidebarMode %}
</div>

{{ cssDefinitions }}
{{ customCssDefinitions }}

<script type='text/javascript'>
    {% if redirectURL %}
        window.location.href = '{{ redirectURL }}';
    {% endif redirectURL %}

    (function() {
        var messages = jQuery('#{{ chatId }} .wcMessages');
        if (messages.length > 0 && '{{ messagesOrder }}' == 'ascending') {
            setTimeout(function() {
                messages.scrollTop(messages[0].scrollHeight);
            }, 100);
        }
    })();

	jQuery(window).load(function() {
        wisechat.core.Instance.getInstance(
            {{ jsOptions }}
        );
	}); 
</script>
