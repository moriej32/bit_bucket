{% variable messageClasses %}
	wcMessage {% if isAuthorWpUser %} wcWpMessage {% endif isAuthorWpUser %} {% if isAuthorCurrentUser %} wcCurrentUserMessage {% endif isAuthorCurrentUser %} {% if hidden %} wcMessageHidden {% endif hidden %} {% if !allowedToGetTheContent %} wcInvisible {% endif allowedToGetTheContent %}
{% endvariable messageClasses %}

<div class="{{ messageClasses }}" data-id="{{ messageId }}" data-chat-user-id="{{ messageChatUserId }}">
	{% if avatarUrl %}
		<img class="wcMessageAvatar" src="{{ avatarUrl }}" />
	{% endif avatarUrl %}

	<div class="wcMessageContentContainer">
		<span class="wcMessageContent" {% if isTextColorSet %}style="color:{{ textColor }}"{% endif isTextColorSet %}>
			<span class="wcMessageUser" {% if isTextColorSet %}style="color:{{ textColor }}"{% endif isTextColorSet %}>
				{{ renderedUserName }}:
			</span>

			{{ messageContent }}
		</span>
		<br class='wcClear' />

		<span class="wcMessageFooter">
			<a href="javascript://" class="wcAdminAction wcMessageApproveButton wcInvisible" data-id="{{ messageId }}" title="Approve the message"><img src='{{ baseDir }}/gfx/icons/approve.png' class='wcIcon' /></a>
			<a href="javascript://" class="wcAdminAction wcMessageDeleteButton wcInvisible" data-id="{{ messageId }}" title="Delete the message"><img src='{{ baseDir }}/gfx/icons/x.png' class='wcIcon' /></a>
			<a href="javascript://" class="wcAdminAction wcUserBanButton wcInvisible" data-id="{{ messageId }}" title="Ban this user"><img src='{{ baseDir }}/gfx/icons/block.png' class='wcIcon' /></a>
			<span class="wcMessageTime" data-utc="{{ messageTimeUTC }}"></span>
		</span>
		<br class='wcClear' />
	</div>
</div>