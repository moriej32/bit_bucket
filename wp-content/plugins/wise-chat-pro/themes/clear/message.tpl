{% variable messageClasses %}
	wcMessage {% if isAuthorWpUser %} wcWpMessage {% endif isAuthorWpUser %} {% if isAuthorCurrentUser %} wcCurrentUserMessage {% endif isAuthorCurrentUser %} {% if hidden %} wcMessageHidden {% endif hidden %} {% if !allowedToGetTheContent %} wcInvisible {% endif allowedToGetTheContent %}
{% endvariable messageClasses %}

<div class="{{ messageClasses }}" data-id="{{ messageId }}" data-chat-user-id="{{ messageChatUserId }}">
	{% if avatarUrl %}
		<span class="wcMessageAvatarContainer">
			<img class="wcMessageAvatar" src="{{ avatarUrl }}" />
		</span>
	{% endif avatarUrl %}
	<span class="wcMessageUser" {% if isTextColorSet %}style="color:{{ textColor }}"{% endif isTextColorSet %}>
		{{ renderedUserName }}
	</span>
	<span class="wcMessageContent" {% if isTextColorSet %}style="color:{{ textColor }}"{% endif isTextColorSet %}>
		{{ messageContent }}

		<span class="wcMessageTime" data-utc="{{ messageTimeUTC }}" {% if isTextColorSet %}style="color:{{ textColor }}"{% endif isTextColorSet %}></span>
		<a href="javascript://" class="wcAdminAction wcMessageApproveButton wcInvisible" data-id="{{ messageId }}" title="Approve the message"><img src='{{ baseDir }}/gfx/icons/approve.png' class='wcIcon' /></a>
		<a href="javascript://" class="wcAdminAction wcMessageDeleteButton wcInvisible" data-id="{{ messageId }}" title="Delete the message"><img src='{{ baseDir }}/gfx/icons/x.png' class='wcIcon' /></a>
		<a href="javascript://" class="wcAdminAction wcUserBanButton wcInvisible" data-id="{{ messageId }}" title="Ban this user"><img src='{{ baseDir }}/gfx/icons/block.png' class='wcIcon' /></a>
		<br class='wcClear' />
	</span>
</div>