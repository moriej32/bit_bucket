/**
 * Wise Chat Private Messages namespace.
 *
 * @author Kainex <contact@kaine.pl>
 * @see http://kaine.pl/projects/wp-plugins/wise-chat-pro
 */

var wisechat = wisechat || {};
wisechat.pm = {};

/**
 * Manages the list of ignored users. The list is stored in Local Storage.
 *
 * @param {String} channelId
 * @constructor
 */
wisechat.pm.IgnoreList = function(channelId) {
	var LOCAL_STORAGE_IGNORE_LIST_KEY = "WiseChatIgnoreList";
	var fallbackStorage = {};

	/**
	 * Adds user to the ignore list.
	 *
	 * @param {String} hash ID of the user
	 */
	function addUser(hash) {
		var data = getData();
		data[hash] = true;
		saveData(data);
	}

	/**
	 * Removes user from the ignore list.
	 *
	 * @param {String} hash ID of the user
	 */
	function removeUser(hash) {
		var data = getData();
		delete data[hash];
		saveData(data);
	}

	/**
	 * Checks if the user is in the ignore list.
	 *
	 * @param {String} hash ID of the user
	 */
	function isIgnored(hash) {
		var data = getData();

		return typeof data[hash] !== "undefined";
	}

	/**
	 * Returns the ignore list for current channel.
	 *
	 * @returns {Object}
	 */
	function getData() {
		var ignoredList = null;
		if (typeof(Storage) !== "undefined") {
			var data = window.localStorage.getItem(LOCAL_STORAGE_IGNORE_LIST_KEY);
			if (data != null) {
				try {
					var parsedData = JSON.parse(data);
					if (jQuery.isPlainObject(parsedData)) {
						ignoredList = parsedData;
					}
				} catch (e) { }
			}
		}

		if (ignoredList === null) {
			ignoredList = fallbackStorage;
		}

		return typeof ignoredList[channelId] !== 'undefined' && jQuery.isPlainObject(ignoredList[channelId]) ? ignoredList[channelId] : {};
	}

	/**
	 * Saves the ignore list for current channel.
	 *
	 * @param {Object} data
	 */
	function saveData(data) {
		if (typeof(Storage) !== "undefined") {
			var currentData = window.localStorage.getItem(LOCAL_STORAGE_IGNORE_LIST_KEY);
			if (currentData != null) {
				try {
					currentData = JSON.parse(currentData);
				} catch (e) { }
			}

			if (!jQuery.isPlainObject(currentData)) {
				currentData = {};
			}

			if (typeof currentData[channelId] === 'undefined') {
				currentData[channelId] = {};
			}

			currentData[channelId] = data;

			if (jQuery.isEmptyObject(data)) {
				delete currentData[channelId];
			}

			window.localStorage.setItem(LOCAL_STORAGE_IGNORE_LIST_KEY, JSON.stringify(currentData));
		} else {
			if (typeof fallbackStorage[channelId] === 'undefined') {
				fallbackStorage[channelId] = {};
			}

			fallbackStorage[channelId] = data;

			if (jQuery.isEmptyObject(data)) {
				delete fallbackStorage[channelId];
			}
		}
	}

	// public API:
	this.addUser = addUser;
	this.removeUser = removeUser;
	this.isIgnored = isIgnored;
};

/**
 * Manages the list of invitations.
 *
 * @constructor
 */
wisechat.pm.InvitationList = function() {
	var inProgressInvitations = {};

	/**
	 * Checks if the user is in the invitation process.
	 *
	 * @param {String} hash ID of the user
	 */
	function isInvitationInProgress(hash) {
		return typeof inProgressInvitations[hash] !== "undefined";
	}

	/**
	 * Starts invitation for the user.
	 *
	 * @param {String} hash ID of the user
	 */
	function startInvitation(hash) {
		inProgressInvitations[hash] = hash;
	}

	/**
	 * Ends invitation for the user.
	 *
	 * @param {String} hash ID of the user
	 */
	function endInvitation(hash) {
		delete inProgressInvitations[hash];
	}

	// public API:
	this.isInvitationInProgress = isInvitationInProgress;
	this.startInvitation = startInvitation;
	this.endInvitation = endInvitation;
};

/**
 * Represents a single conversation.
 *
 * @param {String} publicId User public ID
 * @param {String} hash User ID
 * @param {String} name User name
 * @param {wisechat.ui.Window} window Messages window
 * @constructor
 */
wisechat.pm.Conversation = function(publicId, hash, name, window) {
	var invitationEnabled = true;

	// public api:
	this.getPublicId = function() {
		return publicId;
	};
	this.getHash = function() {
		return hash;
	};
	this.getName = function() {
		return name;
	};
	this.getWindow = function() {
		return window;
	};
	this.setInvitationEnabled = function(enabled) {
		invitationEnabled = enabled;
	};
	this.isInvitationEnabled = function() {
		return invitationEnabled;
	};
};

/**
 * Manages the list of saved conversations.
 *
 * @param {String} channelId
 * @constructor
 */
wisechat.pm.SavedConversations = function(channelId) {
	var LOCAL_STORAGE_KEY = "WiseChatOpenConversations";

	/**
	 * Marks conversation as minimized.
	 *
	 * @param {String} hash
	 */
	function markMinimized(hash) {
		var data = getData();

		if (typeof data[channelId] === 'undefined') {
			data[channelId] = {};
		}
		data[channelId]['__m__' + hash] = true;

		saveData(data);
	}

	/**
	 * Check if the conversation is minimized.
	 *
	 * @param {String} hash User ID
	 * @return {Boolean}
	 */
	function isMinimized(hash) {
		var data = getData();

		return typeof data[channelId] !== 'undefined' && typeof data[channelId]['__m__' + hash] !== 'undefined';
	}

	/**
	 * Clears minimize state of the conversation.
	 *
	 * @param {String} hash User ID
	 */
	function unmarkMinimized(hash) {
		var data = getData();

		if (typeof data[channelId] !== 'undefined' && typeof data[channelId]['__m__' + hash] !== 'undefined') {
			delete data[channelId]['__m__' + hash];
			if (jQuery.isEmptyObject(data[channelId])) {
				delete data[channelId];
			}
			saveData(data);
		}
	}

	/**
	 * Marks conversation as active.
	 *
	 * @param {String} hash
	 */
	function markActive(hash) {
		var data = getData();
		data['__active__'] = hash;

		saveData(data);
	}

	/**
	 * Returns active conversation.
	 *
	 * @return {String}
	 */
	function getActive() {
		var data = getData();

		if (typeof data['__active__'] !== 'undefined') {
			return data['__active__'];
		}

		return null;
	}

	/**
	 * Marks conversation as open.
	 *
	 * @param {String} hash User ID
	 */
	function markOpen(hash) {
		var data = getData();

		if (typeof data[channelId] === 'undefined') {
			data[channelId] = {};
		}
		data[channelId][hash] = true;

		saveData(data);
	}

	/**
	 * Removes the conversation from the list.
	 *
	 * @param {String} hash User ID
	 */
	function clear(hash) {
		var data = getData();

		if (typeof data[channelId] !== 'undefined' && typeof data[channelId][hash] !== 'undefined') {
			delete data[channelId][hash];
			delete data[channelId]['__m__' + hash];
			if (jQuery.isEmptyObject(data[channelId])) {
				delete data[channelId];
			}
			saveData(data);
		}
	}

	/**
	 * Check if the conversation is on the list.
	 *
	 * @param {String} hash User ID
	 * @return {Boolean}
	 */
	function isOpen(hash) {
		var data = getData();

		return typeof data[channelId] !== 'undefined' && typeof data[channelId][hash] !== 'undefined';
	}

	function getData() {
		var data = {};
		if (typeof(Storage) !== "undefined") {
			var encodedData = window.localStorage.getItem(LOCAL_STORAGE_KEY);
			if (encodedData != null) {
				try {
					data = JSON.parse(encodedData);
				} catch (e) { }
			}
		}

		return data;
	}

	function saveData(data) {
		if (typeof(Storage) !== "undefined") {
			window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(data));
		}
	}

	// public API:
	this.markOpen = markOpen;
	this.clear = clear;
	this.isOpen = isOpen;
	this.markActive = markActive;
	this.getActive = getActive;
	this.markMinimized = markMinimized;
	this.isMinimized = isMinimized;
	this.unmarkMinimized = unmarkMinimized;
};

/**
 * Private messages support. It initializes wisechat.ui.Window objects that represents each individual private conversation.
 * Additionally it manages ignoring list.
 *
 * @param {Object} options Plugin's global options
 * @param {wisechat.ui.UsersList} usersList
 * @param {wisechat.core.MessagesTransceiver} transceiver
 * @param {wisechat.maintenance.MaintenanceExecutor} maintenance
 * @param {wisechat.settings.Settings} settings
 * @param {wisechat.utils.Notifier} notifier
 * @param {wisechat.ui.VisualLogger} logger
 * @param {jQuery} controlsTemplate
 * @param {jQuery} dialogsContainer
 * @constructor
 */
wisechat.pm.PrivateMessages = function(
	options, usersList, transceiver, maintenance, settings, notifier, logger, controlsTemplate, dialogsContainer
) {
	var $this = jQuery(this);
	var ignoreList = new wisechat.pm.IgnoreList(options.channelId);
	var invitationList = new wisechat.pm.InvitationList();
	var conversations = [];

	/**
	 * Opens a conversation when an user is clicked on the users list.
	 *
	 * @param {Event} event
	 * @param {String} publicId
	 * @param {String} hash
	 * @param {String} name
	 * @param {Boolean} isCurrent
	 */
	function onUserListUserClick(event, publicId, hash, name, isCurrent) {
		if (isCurrent) {
			return;
		}

		var conversation = getConversationByHash(hash);
		if (conversation == null) {
			conversation = createConversation(publicId, hash, name);
		}

		// if the user is in the ignore list then display confirmation dialog:
		if (ignoreList.isIgnored(hash)) {
			var confirmDialog = new wisechat.ui.Dialog('confirm', dialogsContainer, {
				title: options.messages.messageInformation,
				text: options.messages.messageInfo1,
				buttons: [
					{
						label: options.messages.messageNo,
						onClickClose: true
					},
					{
						label: options.messages.messageYes,
						onClick: function () {
							$this.trigger('open', [conversation]);
							ignoreList.removeUser(hash);
							confirmDialog.close();
						}
					}
				]
			});
			confirmDialog.show();
		} else {
			$this.trigger('open', [conversation]);
		}
	}

	/**
	 * Detects new private messages. Supports restoring private messages (during chat initialization).
	 *
	 * @param {Event} event
	 * @param {Object} response
	 */
	function onMessagesArrived(event, response) {
		if (typeof options.userData === 'undefined') {
			return;
		}

		var restoreMode = typeof response.restorePrivateConversations !== 'undefined' && response.restorePrivateConversations === true;
		for (var x = 0; x < response.result.length; x++) {
			var message = response.result[x];
			if (typeof message.isPrivate === 'undefined' || !message.isPrivate) {
				continue;
			}

			if (restoreMode) {
				restoreConversation(message, response.nowTime);
			} else {
				processMessage(message, response.nowTime);
			}
		}

		// restored conversations can be handled by UI:
		if (restoreMode) {
			$this.trigger('restore', [conversations]);
		}
	}

	/**
	 * Restores conversation based on the message.
	 *
	 * @param {Object} message
	 * @param {String} time
	 * @returns {null}
	 */
	function restoreConversation(message, time) {
		var userHash = options.userData.hash;
		var senderHash = message.senderHash;
		var recipientHash = message.recipientHash;
		var targetId = null;
		var targetHash = null;
		var targetName = null;

		// sent by me:
		if (userHash == senderHash) {
			targetId = message.recipientId;
			targetHash = message.recipientHash;
			targetName = message.recipientName;
		}

		// sent by someone else:
		if (userHash == recipientHash) {
			if (ignoreList.isIgnored(senderHash)) {
				return null;
			}
			targetId = message.senderId;
			targetHash = message.senderHash;
			targetName = message.senderName;
		}

		// create and open a new conversation if it does not exist yet:
		if (targetId !== null) {
			var conversation = getConversationByHash(targetHash);
			if (conversation === null) {
				conversation = createConversation(targetId, targetHash, targetName);
				conversation.setInvitationEnabled(false);
			}

			// ask for disabling notifications for this message:
			message.noNotifications = true;

			// resend message in order to be handled by the regular messages processing:
			transceiver.$.trigger('messagesArrived', [{
				result: [message],
				nowTime: time
			}]);
		}
	}

	/**
	 * Handles a regular message.
	 *
	 * @param {Object} message
	 * @param {String} time
	 * @returns {null}
	 */
	function processMessage(message, time) {
		var userHash = options.userData.hash;
		var senderHash = message.senderHash;
		var recipientHash = message.recipientHash;

		// sent by me:
		if (userHash == senderHash) {
			return null; // do nothing
		}

		// sent by someone else:
		if (userHash == recipientHash) {
			if (ignoreList.isIgnored(senderHash)) {
				return null;
			}

			// create new conversation if it does not exist already:
			var conversation = getConversationByHash(senderHash);
			if (conversation === null) {
				conversation = createConversation(message.senderId, message.senderHash, message.senderName);

				// resend message, it can be caught by the window and shown:
				transceiver.$.trigger('messagesArrived', [{
					result: [message],
					nowTime: time
				}]);
			}

			if (!conversation.isInvitationEnabled() || invitationList.isInvitationInProgress(senderHash)) {
				return null;
			}

			invitationList.startInvitation(senderHash);

			// show invitation dialog:
			var confirmDialog = new wisechat.ui.Dialog('confirm', dialogsContainer, {
				title: options.messages.messageInvitation,
				text: message.senderName + " " + options.messages.messageInfo2,
				buttons: [
					{
						label: options.messages.messageIgnoreUser,
						onClick: function () {
							ignoreList.addUser(message.senderHash);
							invitationList.endInvitation(message.senderHash);
							confirmDialog.close();
						}
					},
					{
						label: options.messages.messageNo,
						onClick: function () {
							invitationList.endInvitation(message.senderHash);
							confirmDialog.close();
						}
					},
					{
						label: options.messages.messageYes,
						onClick: function () {
							$this.trigger('open', [conversation]);
							invitationList.endInvitation(message.senderHash);
							confirmDialog.close();
						}
					}
				]
			});
			confirmDialog.show();
		}
	}

	/**
	 * Creates new wisechat.ui.Window and wisechat.pm.Conversation objects.
	 *
	 * @param {String} publicId User public ID
	 * @param {String} hash User ID
	 * @param {String} name User name
	 * @returns {wisechat.pm.Conversation}
	 */
	function createConversation(publicId, hash, name) {
		// create required DOM elements for wisechat.ui.Window:
		var windowContainer = jQuery('<div />')
			.append(jQuery('<div />').addClass('wcMessages wcInvisible wcMessages' + hash))
			.append(controlsTemplate.clone().addClass('wcInvisible wcControls' + hash));

		// create window:
		var window = new wisechat.ui.Window(options, windowContainer, transceiver, maintenance, usersList, settings, notifier, logger);
		window.setMessageCustomParameters({
			privateMessage: publicId
		});
		window.setMessagesFilter(function(message) {
			if (typeof message.isPrivate === 'undefined' || !message.isPrivate || typeof options.userData === 'undefined') {
				return false;
			}

			var userHash = options.userData.hash;

			// sent by me:
			if (userHash == message.senderHash) {
				return hash == message.recipientHash;
			}

			// sent by someone:
			if (userHash == message.recipientHash) {
				return hash == message.senderHash;
			}

			return false;
		});

		// create conversation:
		var conversation = new wisechat.pm.Conversation(publicId, hash, name, window);
		conversations.push(conversation);

		return conversation;
	}

	/**
	 * Get conversation by user ID.
	 *
	 * @param {String} hash User ID
	 * @returns {wisechat.pm.Conversation}
	 */
	function getConversationByHash(hash) {
		var key = -1;
		jQuery.each(conversations, function(index, element) {
			if (element.getHash() == hash) {
				key = index;
			}
		});

		return key >= 0 ? conversations[key] : null;
	}

	// external events:
	usersList.$.bind('userClick', onUserListUserClick);
	transceiver.$.bind('messagesArrived', onMessagesArrived);

	// public API:
	this.$ = $this;
	this.getConversationByHash = getConversationByHash;
};

/**
 * Tabbed view for private messages.
 *
 * @param {Object} options Plugin's global options
 * @param {wisechat.pm.PrivateMessages} privateMessages
 * @param {wisechat.ui.Window} channelWindow
 * @param {jQuery} tabsContainer
 * @param {jQuery} controlsContainer
 * @param {wisechat.maintenance.MaintenanceExecutor} maintenance
 * @constructor
 */
wisechat.pm.TabbedUI = function(options, privateMessages, channelWindow, tabsContainer, controlsContainer, maintenance) {
	var savedConversations = new wisechat.pm.SavedConversations(options.channelId);
	var windows = {};
	var leftNavButton = null;
	var rightNavButton = null;

	function createTabsContainer() {
		tabsContainer.append(
			jQuery('<span />')
				.data('hash', options.channelId)
				.addClass('wcMessagesContainerTab wcChannelTab')
				.append(
					jQuery('<sup class="wcTabAlert" />')
						.html('*')
						.hide()
				)
				.append(
					jQuery('<a />')
						.addClass('wcMessagesContainerTabLink wcTab' + options.channelId)
						.data('hash', options.channelId)
						.attr('href', 'javascript://')
						.html(options.channelName)
				)
		);
		leftNavButton = jQuery('<a />')
			.attr('href', 'javascript://')
			.addClass('wcPmNavigationButton wcLeftButton')
			.text('<');
		rightNavButton = jQuery('<a />')
			.attr('href', 'javascript://')
			.addClass('wcPmNavigationButton wcRightButton')
			.text('>');
		tabsContainer.append(leftNavButton);
		tabsContainer.append(rightNavButton);
	}

	function createTab(hash, name) {
		var tab = jQuery('<span />')
			.data('hash', hash)
			.addClass('wcMessagesContainerTab wcInvisible')
			.append(
				jQuery('<sup class="wcTabAlert" />')
					.html('*')
					.hide()
			)
			.append(
				jQuery('<a />')
					.addClass('wcMessagesContainerTabLink wcTab' + hash + ' wcUserName' + hash)
					.data('hash', hash)
					.attr('href', 'javascript://')
					.html(name)
			)
			.append(
				jQuery('<a />')
					.addClass('wcMessagesContainerTabCloseLink')
					.data('hash', hash)
					.attr('href', 'javascript://')
					.html('x')
			);
		tabsContainer.append(tab);

		return tab;
	}

	function getActiveConversationHash() {
		var tab = tabsContainer.find('.wcMessagesContainerTabActive a');

		return tab.length > 0 ? tab.data('hash') : null;
	}

	function getNextVisibleTab(hash, includeChannelTab) {
		var channelTabClassSelector = includeChannelTab ? '.wcChannelTabNonExisting' : '.wcChannelTab';
		var tabs = getTabByHash(hash).nextAll('.wcMessagesContainerTab').not(channelTabClassSelector).not('.wcInvisible');

		return tabs.length > 0 ? tabs.slice(0, 1).data('hash') : null;
	}

	function getPreviousVisibleTab(hash, includeChannelTab) {
		var channelTabClassSelector = includeChannelTab ? '.wcChannelTabNonExisting' : '.wcChannelTab';
		var tabs = getTabByHash(hash).prevAll('.wcMessagesContainerTab').not(channelTabClassSelector).not('.wcInvisible');

		return tabs.length > 0 ? jQuery(tabs[0]).data('hash') : null;
	}

	function getTabByHash(hash) {
		return tabsContainer.find('.wcTab' + hash).parent();
	}

	function getPrivateConversationsCount() {
		return tabsContainer.find('.wcMessagesContainerTab').not('.wcChannelTab').not('.wcInvisible').length;
	}

	function hideAlertInTab(hash) {
		getTabByHash(hash).find('.wcTabAlert').hide();
	}

	function showAlertInTab(hash) {
		getTabByHash(hash).find('.wcTabAlert').show();
	}

	function onMessagesContainerTabClick(event) {
		focusConversation(jQuery(this).data('hash'));
	}

	function onMessagesContainerTabCloseClick() {
		var hash = jQuery(this).data('hash');
		var activeConversationHash = getActiveConversationHash();

		hideConversation(hash);
		if (activeConversationHash == hash) {
			var focusTabHash = getNextVisibleTab(hash, false);
			if (focusTabHash === null) {
				focusTabHash = getPreviousVisibleTab(hash, false);
			}

			focusConversation(focusTabHash !== null ? focusTabHash : options.channelId);
		}
	}

	function onMessagesContainerLeftButtonClick() {
		var hash = getActiveConversationHash();
		if (hash !== null) {
			var previousHash = getPreviousVisibleTab(hash, true);
			if (previousHash !== null) {
				focusConversation(previousHash);
			}
		}
	}

	function onMessagesContainerRightButtonClick() {
		var hash = getActiveConversationHash();
		if (hash !== null) {
			var nextHash = getNextVisibleTab(hash, true);
			if (nextHash !== null) {
				focusConversation(nextHash);
			}
		}
	}

	function attachEventListeners() {
		tabsContainer.on('click', 'a.wcMessagesContainerTabLink', onMessagesContainerTabClick);
		tabsContainer.on('click', 'a.wcMessagesContainerTabCloseLink', onMessagesContainerTabCloseClick);
		tabsContainer.on('click', 'a.wcLeftButton', onMessagesContainerLeftButtonClick);
		tabsContainer.on('click', 'a.wcRightButton', onMessagesContainerRightButtonClick);
	}

	/**
	 * Shows conversation: its window, controls and the tab. It also refreshes the window and hides alert indicator in the tab.
	 *
	 * @param {String} hash Conversation ID
	 */
	function focusConversation(hash) {
		var conversation = privateMessages.getConversationByHash(hash);
		if (conversation === null && hash != options.channelId) {
			return;
		}

		// show messages container:
		tabsContainer.siblings('.wcMessages').addClass('wcInvisible');
		tabsContainer.siblings('.wcMessages' + hash).removeClass('wcInvisible');

		// show controls container:
		controlsContainer.parent().find('.wcControls').addClass('wcInvisible');
		controlsContainer.parent().find('.wcControls' + hash).removeClass('wcInvisible');

		// activate tab:
		tabsContainer.find('.wcMessagesContainerTab').removeClass('wcMessagesContainerTabActive');
		tabsContainer.find('.wcTab' + hash).parent().addClass('wcMessagesContainerTabActive').removeClass('wcInvisible');

		// show tabs only if private conversation is being activated:
		if (hash != options.channelId) {
			tabsContainer.removeClass('wcInvisible');
		}

		 var MESSAGES_REFRESH_TIMEOUT = 1000;
		setInterval(gethistorymessage(hash), MESSAGES_REFRESH_TIMEOUT);
		
		// refresh window:
		if (conversation !== null) {
			conversation.getWindow().refresh();
		} else if (hash == options.channelId) {
			channelWindow.refresh();
		}

		// hide alert mark in the tab:
		hideAlertInTab(hash);

		// mark active:
		savedConversations.markActive(hash);

		// set navigation buttons:
		leftNavButton.toggleClass('wcPmNavigationButtonDisabled', getPreviousVisibleTab(hash, true) === null);
		rightNavButton.toggleClass('wcPmNavigationButtonDisabled', getNextVisibleTab(hash, true) === null);
	}
	
	function gethistorymessage(hash){
		var messageshistory = options.apiEndpointBase + '?action=messageshistory';
		console.log(hash);
		tabsContainer.siblings('.wcMessages' + hash).html();
		jQuery.ajax({
							url: messageshistory,
							type: "POST",
							data: { idss : hash},
							success : function(data){
								tabsContainer.siblings('.wcMessages' + hash).html(data);
								 
							}
						});
						
						
	}

	/**
	 * @param {String} hash
	 */
	function hideConversation(hash) {
		tabsContainer.siblings('.wcMessages' + hash).addClass('wcInvisible');
		controlsContainer.parent().find('.wcControls' + hash).addClass('wcInvisible');
		tabsContainer.find('.wcTab' + hash).parent().removeClass('wcMessagesContainerTabActive').addClass('wcInvisible');

		// hide tabs if there is only channel window left:
		if (getPrivateConversationsCount() === 0) {
			tabsContainer.addClass('wcInvisible');
		}

		// enable invitations when the tab is not visible:
		var conversation = privateMessages.getConversationByHash(hash);
		if (conversation !== null) {
			conversation.setInvitationEnabled(true);
		}

		// mark conversation as closed:
		savedConversations.clear(hash);
	}

	/**
	 * Shows alert in the tab if the tab is not active and tabs are visible.
	 *
	 * @param {String} hash Conversation ID
	 */
	function showAlertIfNotActive(hash) {
		if (!tabsContainer.hasClass('wcInvisible') && getActiveConversationHash() != hash) {
			showAlertInTab(hash);
		}
	}

	/**
	 * Opens conversation.
	 *
	 * @param {Event} event
	 * @param {wisechat.pm.Conversation} conversation
	 */
	function onConversationOpen(event, conversation) {
		// insert the window and create the tab:
		if (typeof windows[conversation.getHash()] === 'undefined') {
			var conversationWindow = conversation.getWindow();

			// insert window into DOM and create corresponding tab:
			conversationWindow.insertAfter(tabsContainer, controlsContainer);
			createTab(conversation.getHash(), conversation.getName());

			conversationWindow.$.bind('messageShow', function(event, message) {
				showAlertIfNotActive(conversation.getHash());
			});
			windows[conversation.getHash()] = conversationWindow;
		}

		// show window and tabs:
		focusConversation(conversation.getHash());

		// disable invitations when the tab is visible:
		conversation.setInvitationEnabled(false);

		// mark conversation as open:
		savedConversations.markOpen(conversation.getHash());
	}

	/**
	 * Executed when conversation are being restored after chat initialization.
	 *
	 * @param {Event} event
	 * @param {Array} conversations
	 */
	function onConversationsRestore(event, conversations) {
		var activeConversationHash = savedConversations.getActive();
		for (var x = 0; x < conversations.length; x++) {
			var conversation = conversations[x];

			if (savedConversations.isOpen(conversation.getHash())) {
				onConversationOpen(event, conversation);
			} else {
				conversation.setInvitationEnabled(true);
			}
		}

		if (activeConversationHash !== null) {
			focusConversation(activeConversationHash);
		}
	}

	// events:
	privateMessages.$.bind('open', onConversationOpen);
	privateMessages.$.bind('restore', onConversationsRestore);
	channelWindow.$.bind('messageShow', function(event, message) {
		showAlertIfNotActive(options.channelId);
	});

	// maintenance events:
	maintenance.$.bind('refreshPlainUserNameByHash', function(event, data) {
		tabsContainer.find('.wcUserName' + data.hash).html(data.name); // refresh tabs when user name changes
	});

	// initializations:
	createTabsContainer();
	attachEventListeners();
};

/**
 * Sidebar view for private messages.
 *
 * @param {Object} options Plugin's global options
 * @param {jQuery} container
 * @param {wisechat.pm.PrivateMessages} privateMessages
 * @param {wisechat.ui.Window} channelWindow
 * @param {jQuery} tabsContainer
 * @param {jQuery} controlsContainer
 * @param {wisechat.maintenance.MaintenanceExecutor} maintenance
 * @constructor
 */
wisechat.pm.SidebarUI = function(options, container, privateMessages, channelWindow, tabsContainer, controlsContainer, maintenance) {
	var $this = jQuery(this);
	var savedConversations = new wisechat.pm.SavedConversations(options.channelId);
	/**
	 * @type {Array.<wisechat.ui.Window>}
	 */
	var windows = {};

	function getTabByHash(hash) {
		return tabsContainer.find('.wcTab' + hash).parent();
	}

	function onMessagesContainerTabMinMaxLinkClick(event) {
		event.stopPropagation();

		var hash = jQuery(this).data('hash');
		if (typeof windows[hash] === 'undefined') {
			return;
		}
		var conversationWindow = windows[hash];
		var isMinimize = false;

		if (conversationWindow.getMessages().isVisible()) {
			conversationWindow.getMessages().hide();
			conversationWindow.getControls().hide();
			savedConversations.markMinimized(hash);
			getTabByHash(hash).addClass('wcMessagesContainerTabMinimized');
			getTabByHash(hash).find('.wcMessagesContainerTabMinMaxLink').attr('title', options.messages.messageMaximize);
			isMinimize = true;
		} else {
			conversationWindow.getMessages().show();
			conversationWindow.getControls().show();
			savedConversations.unmarkMinimized(hash);
			conversationWindow.hideUnreadMessagesFlag();
			getTabByHash(hash).removeClass('wcMessagesContainerTabMinimized');
			getTabByHash(hash).find('.wcMessagesContainerTabMinMaxLink').attr('title', options.messages.messageMinimize);
		}

		// arrange windows positions on the screen:
		positionWindows();

		if (isMinimize) {
			$this.trigger('windowMinimized', [conversationWindow]);
		} else {
			$this.trigger('windowMaximized', [conversationWindow]);
		}
	}

	/**
	 * @param {String} hash
	 */
	function hideConversation(hash) {
		tabsContainer.siblings('.wcMessages' + hash).addClass('wcInvisible');
		controlsContainer.parent().find('.wcControls' + hash).addClass('wcInvisible');
		tabsContainer.find('.wcTab' + hash).parent().addClass('wcInvisible');

		// enable invitations when the tab is not visible:
		var conversation = privateMessages.getConversationByHash(hash);
		if (conversation !== null) {
			conversation.setInvitationEnabled(true);
		}

		// mark conversation as closed:
		savedConversations.clear(hash);

		// arrange windows positions on the screen:
		positionWindows();

		$this.trigger('windowHide', [windows[hash]]);
	}

	function onMessagesContainerTabCloseLinkClick() {
		hideConversation(jQuery(this).data('hash'));
	}

	function onMessagesContainerTabClick() {
		var hash = jQuery(this).data('hash');
		if (typeof windows[hash] !== 'undefined') {
			windows[hash].focus();
		}
	}

	function attachEventListeners() {
		tabsContainer.on('click', 'a.wcMessagesContainerTabMinMaxLink', onMessagesContainerTabMinMaxLinkClick);
		tabsContainer.on('click', 'a.wcMessagesContainerTabCloseLink', onMessagesContainerTabCloseLinkClick);
		tabsContainer.on('click', '.wcMessagesContainerTab', onMessagesContainerTabClick);
	}

	function createTab(hash, name) {
		var tab = jQuery('<span />')
			.data('hash', hash)
			.addClass('wcMessagesContainerTab wcInvisible')
			.append(
				jQuery('<sup class="wcUnreadMessagesFlag" />')
					.html('*')
					.hide()
			)
			.append(
				jQuery('<a />')
					.addClass('wcMessagesContainerTabLink wcTab' + hash + ' wcUserName' + hash)
					.data('hash', hash)
					.attr('href', 'javascript://')
					.html(name)
			)
			.append(
				jQuery('<a />')
					.addClass('wcMessagesContainerTabMinMaxLink')
					.data('hash', hash)
					.attr('href', 'javascript://')
					.attr('title', options.messages.messageMinimize)
					.html('')
			)
			.append(
				jQuery('<a />')
					.addClass('wcMessagesContainerTabCloseLink')
					.data('hash', hash)
					.attr('href', 'javascript://')
					.attr('title', options.messages.messageClose)
					.html('')
			);
		tabsContainer.append(tab);

		return tab;
	}

	/**
	 * Shows conversation: its window, controls and the tab. It also refreshes the window and hides alert indicator in the tab.
	 *
	 * @param {String} hash Conversation ID
	 * @param {Boolean} maintainMinimized
	 */
	function focusConversation(hash, maintainMinimized) {
		var conversation = privateMessages.getConversationByHash(hash);
		if (conversation === null) {
			return;
		}

		if (!savedConversations.isMinimized(hash) || maintainMinimized !== true) {
			// show messages container:
			tabsContainer.siblings('.wcMessages' + hash).removeClass('wcInvisible');

			// show controls container:
			controlsContainer.parent().find('.wcControls' + hash).removeClass('wcInvisible');

			getTabByHash(hash).removeClass('wcMessagesContainerTabMinimized');

			savedConversations.unmarkMinimized(hash);
		} else {
			getTabByHash(hash).addClass('wcMessagesContainerTabMinimized');
			getTabByHash(hash).find('.wcMessagesContainerTabMinMaxLink').attr('title', options.messages.messageMaximize);
		}

		// activate tab:
		tabsContainer.find('.wcTab' + hash).parent().removeClass('wcInvisible');
		tabsContainer.removeClass('wcInvisible');

		conversation.getWindow().refresh();
		conversation.getWindow().focus();
		conversation.getWindow().hideUnreadMessagesFlag();
	}

	/**
	 * Opens conversation.
	 *
	 * @param {Event} event
	 * @param {wisechat.pm.Conversation} conversation
	 * @param {Boolean} maintainMinimized
	 */
	function onConversationOpen(event, conversation, maintainMinimized) {
		var hash = conversation.getHash();

		// insert the window and create the tab:
		if (typeof windows[hash] === 'undefined') {
			var conversationWindow = conversation.getWindow();
			conversationWindow.setTitleContainer(createTab(hash, conversation.getName()));

			// insert window into DOM and create corresponding tab:
			conversationWindow.insertAfter(tabsContainer, controlsContainer);

			conversationWindow.$.bind('messageShow', function(event, message) {
				if (!conversationWindow.isActive()) {
					conversationWindow.showUnreadMessagesFlag();
				}
			});
			conversationWindow.$.bind('windowFocus', function(event, originalEvent) {
				conversationWindow.hideUnreadMessagesFlag();
			});
			conversationWindow.$.bind('clickInside', function(event, originalEvent) {
				conversationWindow.hideUnreadMessagesFlag();
				conversationWindow.setActive();
			});
			conversationWindow.$.bind('clickOutside', function(event, originalEvent) {
				var tab = getTabByHash(hash);

				if (jQuery(originalEvent.target).closest(tab).length > 0 && !savedConversations.isMinimized(hash) || conversationWindow.isFocused()) {
					conversationWindow.setActive();
				} else {
					conversationWindow.setInactive();
				}
			});

			windows[hash] = conversationWindow;
		}

		// show window and tabs:
		focusConversation(hash, maintainMinimized);

		// disable invitations when the tab is visible:
		conversation.setInvitationEnabled(false);

		// mark conversation as open:
		savedConversations.markOpen(hash);

		// arrange windows positions on the screen:
		positionWindows();

		$this.trigger('windowOpen', [windows[hash]]);
	}

	/**
	 * Executed when conversation are being restored after chat initialization.
	 *
	 * @param {Event} event
	 * @param {Array} conversations
	 */
	function onConversationsRestore(event, conversations) {
		for (var x = 0; x < conversations.length; x++) {
			var conversation = conversations[x];

			if (savedConversations.isOpen(conversation.getHash())) {
				onConversationOpen(event, conversation, true);
			} else {
				conversation.setInvitationEnabled(true);
			}
		}

		$this.trigger('windowsRestored', []);
	}

	function positionWindows() {
		var channelWindowLeft = channelWindow.getMessagesContainer().offset().left;
		if (channelWindowLeft == 0) {
			// messages container may be hidden:
			channelWindowLeft = channelWindow.getMessagesContainer().siblings('.wcWindowTitle').offset().left
		}
		var right = jQuery(window).width() - channelWindowLeft;
		var width = channelWindow.getMessagesContainer().outerWidth();

		for (var hash in windows) {
			var conversationWindow = windows[hash];
			var messages = conversationWindow.getMessages();
			var controls = conversationWindow.getControls();
			var tabContainer = getTabByHash(hash);

			// omit invisible windows:
			if (tabContainer.hasClass('wcInvisible')) {
				continue;
			}

			var mobileNavigation = container.find('.wcSidebarModeMobileNavigation');
			var mobileNavigationHeight = mobileNavigation.is(":visible") && mobileNavigation.outerHeight() > 0 ? mobileNavigation.outerHeight() : 0;

			if (!savedConversations.isMinimized(hash)) {
				// safe refresh:
				conversationWindow.refresh();

				// position of controls container:
				controls.setWidth(width);
				controls.setRight(right);
				controls.setBottom(mobileNavigationHeight);
				controls.refresh();

				// position of messages container:
				messages.setWidth(width);
				messages.setRight(right);
				messages.setBottom((controls.getHeight() > 0 ? controls.getHeight() : 0) + mobileNavigationHeight);
			}

			tabContainer.css({
				width: width,
				right: right,
				bottom: (messages.isVisible() ? messages.getHeight() + controls.getHeight() : 0) + mobileNavigationHeight
			});

			right += width;
		}
	}

	function getWindows() {
		return windows;
	}

	// events:
	privateMessages.$.bind('open', onConversationOpen);
	privateMessages.$.bind('restore', onConversationsRestore);
	channelWindow.$.bind('messageShow', function(event, message) {
		if (!channelWindow.isActive()) {
			channelWindow.showUnreadMessagesFlag();
		}
	});
	channelWindow.$.bind('windowFocus', function(event, originalEvent) {
		channelWindow.hideUnreadMessagesFlag();
	});

	// maintenance events:
	maintenance.$.bind('refreshPlainUserNameByHash', function(event, data) {
		tabsContainer.find('.wcUserName' + data.hash).html(data.name); // refresh tabs when user name changes
	});

	// initializations:
	attachEventListeners();

	// public API:
	this.$ = $this;
	this.getWindows = getWindows;
};