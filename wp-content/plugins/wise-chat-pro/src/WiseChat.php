<?php

/**
 * WiseChat core class.
 *
 * @author Kainex <contact@kaine.pl>
 */
class WiseChat {
	
	/**
	* @var WiseChatOptions
	*/
	private $options;
	
	/**
	* @var WiseChatUsersDAO
	*/
	private $usersDAO;
	
	/**
	* @var WiseChatUserSettingsDAO
	*/
	private $userSettingsDAO;
	
	/**
	* @var WiseChatChannelUsersDAO
	*/
	private $channelUsersDAO;
	
	/**
	* @var WiseChatActionsDAO
	*/
	private $actionsDAO;
	
	/**
	* @var WiseChatRenderer
	*/
	private $renderer;
	
	/**
	* @var WiseChatCssRenderer
	*/
	private $cssRenderer;
	
	/**
	* @var WiseChatBansService
	*/
	private $bansService;
	
	/**
	* @var WiseChatUserService
	*/
	private $userService;
	
	/**
	* @var WiseChatMessagesService
	*/
	private $messagesService;
	
	/**
	* @var WiseChatService
	*/
	private $service;
	
	/**
	* @var WiseChatAttachmentsService
	*/
	private $attachmentsService;

	/**
	 * @var WiseChatAuthentication
	 */
	private $authentication;

	/**
	 * @var WiseChatExternalAuthentication
	 */
	private $externalAuthentication;

	/**
	 * @var WiseChatHttpRequestService
	 */
	private $httpRequestService;
	
	/**
	* @var array
	*/
	private $shortCodeOptions;
	
	public function __construct() {
		$this->options = WiseChatOptions::getInstance();
		$this->usersDAO = WiseChatContainer::get('dao/user/WiseChatUsersDAO');
		$this->userSettingsDAO = WiseChatContainer::get('dao/user/WiseChatUserSettingsDAO');
		$this->channelUsersDAO = WiseChatContainer::get('dao/WiseChatChannelUsersDAO');
		$this->actionsDAO = WiseChatContainer::get('dao/WiseChatActionsDAO');
		$this->renderer = WiseChatContainer::get('rendering/WiseChatRenderer');
		$this->cssRenderer = WiseChatContainer::get('rendering/WiseChatCssRenderer');
		$this->bansService = WiseChatContainer::get('services/WiseChatBansService');
		$this->userService = WiseChatContainer::get('services/user/WiseChatUserService');
		$this->messagesService = WiseChatContainer::get('services/WiseChatMessagesService');
		$this->service = WiseChatContainer::get('services/WiseChatService');
		$this->attachmentsService = WiseChatContainer::get('services/WiseChatAttachmentsService');
		$this->authentication = WiseChatContainer::getLazy('services/user/WiseChatAuthentication');
		$this->externalAuthentication = WiseChatContainer::getLazy('services/user/WiseChatExternalAuthentication');
		$this->httpRequestService = WiseChatContainer::getLazy('services/WiseChatHttpRequestService');
		WiseChatContainer::load('WiseChatCrypt');
		WiseChatContainer::load('WiseChatThemes');
		WiseChatContainer::load('rendering/WiseChatTemplater');
		
		$this->userService->initMaintenance();
		$this->shortCodeOptions = array();
	}
	
	/*
	* Enqueues all necessary resources (scripts or styles).
	*/
	public function registerResources() {
		$pluginBaseURL = $this->options->getBaseDir();

		wp_enqueue_script('wisechat_utils', $pluginBaseURL.'js/wisechat/utils.js', array());
		wp_enqueue_script('wisechat_engines', $pluginBaseURL.'js/wisechat/engines.js', array());
		wp_enqueue_script('wisechat_settings', $pluginBaseURL.'js/wisechat/settings.js', array());
		wp_enqueue_script('wisechat_pm', $pluginBaseURL.'js/wisechat/pm.js', array());
		wp_enqueue_script('wisechat_ui_core', $pluginBaseURL.'js/wisechat/ui-core.js', array());
		wp_enqueue_script('wisechat_ui_controls', $pluginBaseURL.'js/wisechat/ui-controls.js', array());
		wp_enqueue_script('wisechat_ui_sidebar', $pluginBaseURL.'js/wisechat/ui-sidebar.js', array());
		wp_enqueue_script('wisechat_maintenance', $pluginBaseURL.'js/wisechat/maintenance.js', array());
		wp_enqueue_script('wisechat_core', $pluginBaseURL.'js/wisechat/core.js', array());

		if ($this->options->isOptionEnabled('allow_change_text_color')) {
			wp_enqueue_script('wise_chat_3rdparty_jscolorPicker', $pluginBaseURL.'js/3rdparty/jquery.colorPicker.min.js', array());
			wp_enqueue_style('wise_chat_3rdparty_jscolorPicker', $pluginBaseURL.'css/3rdparty/colorPicker.css');
		}
	}

	/**
	 * Shortcode backend function: [wise-chat]
	 *
	 * @param array $attributes
	 * @return string
	 */
	public function getRenderedShortcode($attributes) {
		if (!is_array($attributes)) {
			$attributes = array();
		}
		$attributes['channel'] = $this->service->getValidChatChannelName(
			array_key_exists('channel', $attributes) ? $attributes['channel'] : ''
		);
		
		$this->options->replaceOptions($attributes);
		$this->shortCodeOptions = $attributes;
   
		return $this->getRenderedChat($attributes['channel']);
	}

	/**
	 * Returns chat HTML for given channel.
	 *
	 * @param string|null $channelName
	 *
	 * @return string
	 * @throws Exception
	 */
	public function getRenderedChat($channelName = null) {
		$this->requireComposerClassLoader();

		$channel = $this->service->createAndGetChannel($this->service->getValidChatChannelName($channelName));

		if ($this->service->isChatRestrictedForAnonymousUsers()) {
			return $this->renderer->getRenderedAccessDenied(
				$channel, $this->options->getOption('message_error_4', 'Only logged in users are allowed to enter the chat'), 'wcAccessDenied'
			);
		}

		if ($this->service->isChatRestrictedForCurrentUserRole()) {
			return $this->renderer->getRenderedAccessDenied(
				$channel, $this->options->getOption('message_error_11', 'You are not allowed to enter the chat.'), 'wcAccessDenied'
			);
		}
		
		if (!$this->service->isChatOpen()) {
			return $this->renderer->getRenderedAccessDenied(
				$channel, $this->options->getOption('message_error_5', 'The chat is closed now'), 'wcChatClosed'
			);
		}
		
		if ($this->service->isChatChannelFull($channel)) {
			return $this->renderer->getRenderedAccessDenied(
				$channel, $this->options->getOption('message_error_6', 'The chat is full now. Try again later.'), 'wcChatFull'
			);
		}
		
		if ($this->service->isChatChannelsLimitReached($channel)) {
			return $this->renderer->getRenderedAccessDenied(
				$channel, $this->options->getOption('message_error_10', 'You cannot enter the chat due to the limit of channels you can participate simultaneously.'), 'wcChatChannelLimitFull'
			);
		}

		$redirectURL = null;
		if ($this->service->hasUserToBeForcedToEnterName()) {
			if ($this->getPostParam('wcUserNameSelection') !== null) {
				try {
					$this->authentication->authenticate($this->getPostParam('wcUserName'));
				} catch (Exception $e) {
					return $this->renderer->getRenderedUserNameForm($channel, $e->getMessage());
				}
			} else {
				return $this->renderer->getRenderedUserNameForm($channel);
			}
		} else if ($this->service->hasUserToBeAuthenticatedExternally()) {
			if ($this->getParam('wcExternalLogin')) {
				try {
					$redirectURL = $this->externalAuthentication->authenticate($this->getParam('wcExternalLogin'));
				} catch (Exception $e) {
					return $this->renderer->getRenderedExternalAuthentication($channel, $e->getMessage());
				}
			} else if ($this->getParam('wcAnonymousLogin') && $this->options->isOptionEnabled('anonymous_login_enabled', true)) {
				try {
					$this->authentication->authenticateAnonymously();
					$redirectURL = $this->httpRequestService->getCurrentURLWithoutParameters(array('wcAnonymousLogin'));
				} catch (Exception $e) {
					return $this->renderer->getRenderedExternalAuthentication($channel, $e->getMessage());
				}
			} else {
				return $this->renderer->getRenderedExternalAuthentication($channel);
			}
		}
		
		if ($this->service->hasUserToBeAuthorizedInChannel($channel)) {
			if ($this->getPostParam('wcChannelAuthorization') !== null) {
				if (!$this->service->authorize($channel, $this->getPostParam('wcChannelPassword'))) {
					return $this->renderer->getRenderedPasswordAuthorization($channel, $this->options->getOption('message_error_9', 'Invalid password.'));
				}
			} else {
				return $this->renderer->getRenderedPasswordAuthorization($channel);
			}
		}

		$chatId = $this->service->getChatID();
		
		$this->userService->startUpMaintenance($channel);
		$this->bansService->startUpMaintenance();
		$this->messagesService->startUpMaintenance($channel);

		$messages = $this->messagesService->getAllByChannelNameAndOffset($channel->getName());
		
		$renderedMessages = '';
		 
		$lastId = 0;
		foreach ($messages as $message) {
			// omit non-admin messages:
			//if ($message->isAdmin() && !$this->usersDAO->isWpUserAdminLogged()) {
				//continue;
			//}
			//if (!($message->getRecipientId() > 0)) {
				$renderedMessages .= $this->renderer->getRenderedMessage($message, $this->authentication->getUserIdOrNull(), $message->getRecipientId());
		 
			   
			//} 
			
			if ($lastId < $message->getId()) {
				$lastId = $message->getId();
			}
		}
		
		$lastAction = $this->actionsDAO->getLast();
		$jsOptions = array(
			'chatId' => $chatId,
			'channelId' => $channel->getId(),
			'channelName' => $channel->getName(),
			'nowTime' => gmdate('c', time()),
			'theme' => $this->options->getEncodedOption('theme', ''),
			'lastId' => 130,
			'checksum' => $this->getCheckSum(),
			'lastActionId' => $lastAction !== null ? $lastAction->getId() : 0,
			'baseDir' => $this->options->getBaseDir(),
            'emoticonsBaseURL' => $this->options->getEmoticonsBaseURL(),
			'apiEndpointBase' => $this->getEndpointBase(),
			'apiWPEndpointBase' => $this->getWPEndpointBase(),
			'sidebarMode' => $this->options->getIntegerOption('mode', 0) === 1,
			'messagesRefreshTime' => intval($this->options->getEncodedOption('messages_refresh_time', 3000)),
			'messagesOrder' => $this->options->getEncodedOption('messages_order', '') == 'descending' ? 'descending' : 'ascending',
			'enableTitleNotifications' => $this->options->isOptionEnabled('enable_title_notifications'),
			'enablePrivateMessages' => $this->options->isOptionEnabled('enable_private_messages'),
			'soundNotification' => $this->options->getEncodedOption('sound_notification'),
			'messagesTimeMode' => $this->options->getEncodedOption('messages_time_mode'),
			'channelUsersLimit' => $this->options->getIntegerOption('channel_users_limit', 0),
			'messages' => array(
				'message_sending' => $this->options->getEncodedOption('message_sending', 'Sending ...'),
				'hint_message' => $this->options->getEncodedOption('hint_message'),
				'messageSecAgo' => $this->options->getEncodedOption('message_sec_ago', 'sec. ago'),
				'messageMinAgo' => $this->options->getEncodedOption('message_min_ago', 'min. ago'),
				'messageYesterday' => $this->options->getEncodedOption('message_yesterday', 'yesterday'),
				'messageUnsupportedTypeOfFile' => $this->options->getEncodedOption('message_error_7', 'Unsupported type of file.'),
				'messageSizeLimitError' => $this->options->getEncodedOption('message_error_8', 'The size of the file exceeds allowed limit.'),
				'messageInputTitle' => $this->options->getEncodedOption('message_input_title', 'Use Shift+ENTER in order to move to the next line.'),
				'messageYes' => $this->options->getEncodedOption('message_yes', 'Yes'),
				'messageNo' => $this->options->getEncodedOption('message_no', 'No'),
				'messageIgnoreUser' => $this->options->getEncodedOption('message_ignore_user', 'Ignore this user'),
				'messageInformation' => $this->options->getEncodedOption('message_information', 'Information'),
				'messageInvitation' => $this->options->getEncodedOption('message_invitation', 'Invitation'),
				'messageMaximize' => $this->options->getEncodedOption('message_maximize', 'Maximize'),
				'messageMinimize' => $this->options->getEncodedOption('message_minimize', 'Minimize'),
				'messageClose' => $this->options->getEncodedOption('message_close', 'Close'),
				'messageInfo1' => $this->options->getEncodedOption('message_info_1', 'This user is ignored by you. Would you like to stop ignoring this user?'),
				'messageInfo2' => $this->options->getEncodedOption('message_info_2', 'invites you to the private chat. Do you accept it?'),
				'messageInfo3' => $this->options->getEncodedOption('message_info_3', 'The message has been posted, but first it must be approved by the administrator.'),
			),
			'userSettings' => $this->userSettingsDAO->getAll(),
			'attachmentsValidFileFormats' => $this->attachmentsService->getAllowedFormats(),
			'attachmentsSizeLimit' => $this->attachmentsService->getSizeLimit(),
			'imagesSizeLimit' => $this->options->getIntegerOption('images_size_limit', 3145728),
			'autoHideUsersList' => $this->options->isOptionEnabled('autohide_users_list', false),
			'autoHideUsersListWidth' => $this->options->getIntegerOption('autohide_users_list_width', 300),
			'showUsersList' => $this->options->isOptionEnabled('show_users'),
			'multilineSupport' => $this->options->isOptionEnabled('multiline_support'),
			'messageMaxLength' => $this->options->getIntegerOption('message_max_length', 100),
			'debugMode' => $this->options->isOptionEnabled('enabled_debug', false),
			'emoticonsSet' => $this->options->getIntegerOption('emoticons_enabled', 1),
			/*'rights' => array(
				'deleteMessages' => $this->options->isOptionEnabled('enable_message_actions') && $this->usersDAO->hasCurrentWpUserRight('delete_message'),
				'banUsers' => $this->options->isOptionEnabled('enable_message_actions') && $this->usersDAO->hasCurrentWpUserRight('ban_user')
			),*/
			'allowToReceiveMessages' => !$this->options->isOptionEnabled('write_only', false),
			'enableApprovalConfirmation' => $this->options->isOptionEnabled('enable_approval_confirmation', true),
			'approvingMessagesMode' => $this->options->getIntegerOption('approving_messages_mode', 1),
		);
		
		$templater = new WiseChatTemplater($this->options->getPluginBaseDir());
		$templater->setTemplateFile(WiseChatThemes::getInstance()->getMainTemplate());
		$data = array(
			'chatId' => $chatId,
			'channelId' => $channel->getId(),
			'baseDir' => $this->options->getBaseDir(),
			'redirectURL' => $redirectURL,
			'messages' => $renderedMessages,
			'themeStyles' => $this->options->getBaseDir().WiseChatThemes::getInstance()->getCss(),
			'sidebarMode' => $this->options->getIntegerOption('mode', 0) === 1,
			'showMessageSubmitButton' => $this->options->isOptionEnabled('show_message_submit_button'),
            'showEmoticonInsertButton' => $this->options->isOptionEnabled('show_emoticon_insert_button', true),
			'messageSubmitButtonCaption' => $this->options->getEncodedOption('message_submit_button_caption', 'Send'),
			'showUsersList' => $this->options->isOptionEnabled('show_users'),
			'usersList' => $this->options->isOptionEnabled('show_users') ? $this->renderer->getRenderedUsersList($channel) : '',
			'showUsersCounter' => $this->options->isOptionEnabled('show_users_counter'),
			'channelUsersLimit' => $this->options->getIntegerOption('channel_users_limit', 0),
			'totalUsers' => $this->channelUsersDAO->getAmountOfUsersInChannel($channel->getId()),
			'showUserName' => $this->options->isOptionEnabled('show_user_name'),
			'currentUserName' => htmlentities($this->authentication->getUserNameOrEmptyString(), ENT_QUOTES, 'UTF-8'),
			'isCurrentUserNameNotEmpty' => $this->authentication->isAuthenticated(),
			'allowPrivateMessages' => $this->options->isOptionEnabled('enable_private_messages', false),
			
			'inputControlsTopLocation' => $this->options->getEncodedOption('input_controls_location') == 'top',
			'inputControlsBottomLocation' => $this->options->getEncodedOption('input_controls_location') == '',
			
			'showCustomizationsPanel' => 
				$this->options->isOptionEnabled('allow_change_user_name') && !$this->usersDAO->isWpUserLogged() && !$this->authentication->isAuthenticatedExternally() ||
				$this->options->isOptionEnabled('allow_mute_sound') && strlen($this->options->getEncodedOption('sound_notification')) > 0 || 
				$this->options->isOptionEnabled('allow_change_text_color'),
				
			'allowChangeUserName' => $this->options->isOptionEnabled('allow_change_user_name') && !$this->usersDAO->isWpUserLogged() && !$this->authentication->isAuthenticatedExternally(),
			'allowMuteSound' => $this->options->isOptionEnabled('allow_mute_sound') && strlen($this->options->getEncodedOption('sound_notification')) > 0,
			'allowChangeTextColor' => $this->options->isOptionEnabled('allow_change_text_color'),

            'allowToSendMessages' => $this->userService->isSendingMessagesAllowed() || $this->authentication->isAuthenticatedExternally(),
            'allowToReceiveMessages' => !$this->options->isOptionEnabled('write_only', false),

			'messageCustomize' => $this->options->getEncodedOption('message_customize', 'Customize'),
			'messageName' => $this->options->getEncodedOption('message_name', 'Name'),
			'messageSave' => $this->options->getEncodedOption('message_save', 'Save'),
			'messageReset' => $this->options->getEncodedOption('message_reset', 'Reset'),
			'messageMuteSounds' => $this->options->getEncodedOption('message_mute_sounds', 'Mute sounds'),
			'messageTextColor' => $this->options->getEncodedOption('message_text_color', 'Text color'),
			'messageTotalUsers' => $this->options->getEncodedOption('message_total_users', 'Total users'),
			'messagePictureUploadHint' => $this->options->getEncodedOption('message_picture_upload_hint', 'Upload a picture'),
			'messageAttachFileHint' => $this->options->getEncodedOption('message_attach_file_hint', 'Attach a file'),
            'messageInsertEmoticon' => $this->options->getEncodedOption('message_insert_emoticon', 'Insert an emoticon'),
			'messageInputTitle' => $this->options->getEncodedOption('message_input_title', 'Use Shift+ENTER in order to move to the next line.'),
            'windowTitle' => $this->options->getEncodedOption('window_title', ''),
			'showWindowTitle' => strlen($this->options->getEncodedOption('window_title', '')) > 0 || $this->options->getIntegerOption('mode', 0) === 1,
            
            'enableAttachmentsPanel' => $this->options->isOptionEnabled('enable_images_uploader') || $this->options->isOptionEnabled('enable_attachments_uploader'),
            'enableImagesUploader' => $this->options->isOptionEnabled('enable_images_uploader'),
            'enableAttachmentsUploader' => $this->options->isOptionEnabled('enable_attachments_uploader'),
            'attachmentsExtensionsList' => $this->attachmentsService->getAllowedExtensionsList(),

            'multilineSupport' => $this->options->isOptionEnabled('multiline_support'),
            'hintMessage' => $this->options->getEncodedOption('hint_message'),
            'messageMaxLength' => $this->options->getIntegerOption('message_max_length', 100),

			'jsOptions' => json_encode($jsOptions),
            'messagesOrder' => $this->options->getEncodedOption('messages_order', '') == 'descending' ? 'descending' : 'ascending',
			'cssDefinitions' => $this->cssRenderer->getCssDefinition($chatId),
			'customCssDefinitions' => $this->cssRenderer->getCustomCssDefinition()
		);
		
		$data = array_merge($data, $this->userSettingsDAO->getAll());
		if ($this->authentication->isAuthenticated()) {
			$data = array_merge($data, $this->authentication->getUser()->getData());
		}

		return $templater->render($data);
	}

    /**
     * @return string
     */
    private function getCheckSum() {
		$checkSumData = is_array($this->shortCodeOptions) ? $this->shortCodeOptions : array();
		$checkSumData['_sid'] = session_id();
		$checkSumData['_sn'] = session_name();
		if ($this->options->isOptionEnabled('enable_buddypress', false) && function_exists("bp_is_group") && bp_is_group()) {
			$checkSumData['_bpg'] = bp_get_group_id();
		}

        return base64_encode(WiseChatCrypt::encrypt(serialize($checkSumData)));
    }

    /**
     * @return string
     */
	private function getEndpointBase() {
		$endpointBase = get_site_url().'/wp-admin/admin-ajax.php';
		if ($this->options->getEncodedOption('ajax_engine', null) === 'lightweight') {
			$endpointBase = get_site_url().'/wp-content/plugins/wise-chat-pro/src/endpoints/';
		}
		
		return $endpointBase;
	}

 	/**
     * @return string
     */
	private function getWPEndpointBase() {
		return get_site_url().'/wp-admin/admin-ajax.php';
	}

    /**
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     */
	private function getPostParam($name, $default = null) {
		return array_key_exists($name, $_POST) ? $_POST[$name] : $default;
	}

	/**
	 * @param string $name
	 * @param mixed|null $default
	 * @return mixed
	 */
	private function getParam($name, $default = null) {
		return array_key_exists($name, $_GET) ? $_GET[$name] : $default;
	}

	/**
	 * Loads Composer class loader only if necessary.
	 */
	private function requireComposerClassLoader() {
		if ($this->service->isExternalLoginEnabled()) {
			require_once(dirname(__DIR__).'/vendor/autoload.php');
		}
	}
}