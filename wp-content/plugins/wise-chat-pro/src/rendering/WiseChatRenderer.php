<?php

/**
 * Wise Chat message rendering class.
 *
 * @author Kainex <contact@kaine.pl>
 */
class WiseChatRenderer {
	
	/**
	* @var WiseChatMessagesService
	*/
	private $messagesService;

	/**
	 * @var WiseChatService
	 */
	private $service;
	
	/**
	* @var WiseChatUsersDAO
	*/
	private $usersDAO;
	
	/**
	* @var WiseChatChannelUsersDAO
	*/
	private $channelUsersDAO;

	/**
	 * @var WiseChatAuthentication
	 */
	private $authentication;

	/**
	 * @var WiseChatExternalAuthentication
	 */
	private $externalAuthentication;

	/**
	 * @var WiseChatHttpRequestService
	 */
	private $httpRequestService;
	
	/**
	* @var WiseChatOptions
	*/
	private $options;
	
	/**
	* @var WiseChatTemplater
	*/
	private $templater;

	/**
	 * @var WiseChatCssRenderer
	 */
	private $cssRenderer;
	
	public function __construct() {
		$this->options = WiseChatOptions::getInstance();
		$this->messagesService = WiseChatContainer::get('services/WiseChatMessagesService');
		$this->service = WiseChatContainer::getLazy('services/WiseChatService');
		$this->usersDAO = WiseChatContainer::get('dao/user/WiseChatUsersDAO');
		$this->channelUsersDAO = WiseChatContainer::get('dao/WiseChatChannelUsersDAO');
		$this->authentication = WiseChatContainer::getLazy('services/user/WiseChatAuthentication');
		$this->externalAuthentication = WiseChatContainer::getLazy('services/user/WiseChatExternalAuthentication');
		$this->httpRequestService = WiseChatContainer::getLazy('services/WiseChatHttpRequestService');
		$this->cssRenderer = WiseChatContainer::get('rendering/WiseChatCssRenderer');
		WiseChatContainer::load('WiseChatThemes');
		WiseChatContainer::load('rendering/WiseChatTemplater');
		WiseChatContainer::load('services/user/WiseChatUserService');

		$this->templater = new WiseChatTemplater($this->options->getPluginBaseDir());
	}

    /**
     * Returns rendered password authorization page.
     *
	 * @param WiseChatChannel $channel
     * @param string|null $authorizationError
     *
     * @return string HTML source
     * @throws Exception
     */
	public function getRenderedPasswordAuthorization($channel, $authorizationError = null) {
		$this->templater->setTemplateFile(WiseChatThemes::getInstance()->getPasswordAuthorizationTemplate());
		$chatId = $this->service->getChatID();

		$data = array(
			'chatId' => $chatId,
			'channelId' => $channel->getId(),
			'isDefaultTheme' => strlen($this->options->getEncodedOption('theme', '')) === 0,
			'themeStyles' => $this->options->getBaseDir().WiseChatThemes::getInstance()->getCss(),
			'windowTitle' => $this->options->getEncodedOption('window_title', ''),
			'sidebarMode' => $this->options->getIntegerOption('mode', 0) === 1,
			'showWindowTitle' => strlen($this->options->getEncodedOption('window_title', '')) > 0 || $this->options->getIntegerOption('mode', 0) === 1,
			'messageChannelPasswordAuthorizationHint' => $this->options->getEncodedOption(
				'message_channel_password_authorization_hint', 'This channel is protected. Enter your password:'
			),
			'messageLogin' => $this->options->getEncodedOption('message_login', 'Log in'),
			'authorizationError' => $authorizationError,
			'cssDefinitions' => $this->cssRenderer->getCssDefinition($chatId),
			'customCssDefinitions' => $this->cssRenderer->getCustomCssDefinition()
		);
		
		return $this->templater->render($data);
	}

	/**
	 * Returns rendered external authorization page.
	 *
	 * @param WiseChatChannel $channel
	 * @param string|null $authenticationError
	 *
	 * @return string HTML source
	 * @throws Exception
	 */
	public function getRenderedExternalAuthentication($channel, $authenticationError = null) {
		$this->templater->setTemplateFile(WiseChatThemes::getInstance()->getExternalAuthenticationTemplate());
		$chatId = $this->service->getChatID();

		$facebookRedirectURL = null;
		$twitterRedirectURL = null;
		$googleRedirectURL = null;
		try {
			if ($this->options->isOptionEnabled('facebook_login_enabled', false)) {
				$facebookRedirectURL = $this->externalAuthentication->getFacebookRedirectLoginURL();
			}
			if ($this->options->isOptionEnabled('twitter_login_enabled', false)) {
				$twitterRedirectURL = $this->externalAuthentication->getTwitterRedirectLoginURL();
			}
			if ($this->options->isOptionEnabled('google_login_enabled', false)) {
				$googleRedirectURL = $this->externalAuthentication->getGoogleRedirectLoginURL();
			}
		} catch (Exception $e) {
			$authenticationError = $e->getMessage();
		}

		$data = array(
			'chatId' => $chatId,
			'channelId' => $channel->getId(),
			'baseDir' => $this->options->getBaseDir(),
			'isDefaultTheme' => strlen($this->options->getEncodedOption('theme', '')) === 0,
			'themeStyles' => $this->options->getBaseDir().WiseChatThemes::getInstance()->getCss(),
			'windowTitle' => $this->options->getEncodedOption('window_title', ''),
			'sidebarMode' => $this->options->getIntegerOption('mode', 0) === 1,
			'showWindowTitle' => strlen($this->options->getEncodedOption('window_title', '')) > 0 || $this->options->getIntegerOption('mode', 0) === 1,
			'loginUsing' => $this->options->getEncodedOption('message_login_using', 'Log in using'),
			'loginAnonymously' => $this->options->getEncodedOption('message_login_anonymously', 'Log in anonymously'),

			'anonymousLogin' => $this->options->isOptionEnabled('anonymous_login_enabled', true),
			'anonymousLoginURL' => $this->httpRequestService->getCurrentURLWithParameter('wcAnonymousLogin', 'an'),

			'facebook' => $this->options->isOptionEnabled('facebook_login_enabled', false),
			'facebookRedirectURL' => $facebookRedirectURL,

			'twitter' => $this->options->isOptionEnabled('twitter_login_enabled', false),
			'twitterRedirectURL' => $twitterRedirectURL,

			'google' => $this->options->isOptionEnabled('google_login_enabled', false),
			'googleRedirectURL' => $googleRedirectURL,

			'authenticationError' => $authenticationError,

			'cssDefinitions' => $this->cssRenderer->getCssDefinition($chatId),
			'customCssDefinitions' => $this->cssRenderer->getCustomCssDefinition()
		);

		return $this->templater->render($data);
	}
	
	/**
	* Returns rendered access-denied page.
	*
	* @param WiseChatChannel $channel
	* @param object $errorMessage
	* @param object $cssClass
	*
	* @return string HTML source
	*/
	public function getRenderedAccessDenied($channel, $errorMessage, $cssClass) {
		$this->templater->setTemplateFile(WiseChatThemes::getInstance()->getAccessDeniedTemplate());
		$chatId = $this->service->getChatID();

		$data = array(
			'chatId' => $chatId,
			'channelId' => $channel->getId(),
			'isDefaultTheme' => strlen($this->options->getEncodedOption('theme', '')) === 0,
			'themeStyles' => $this->options->getBaseDir().WiseChatThemes::getInstance()->getCss(),
			'windowTitle' => $this->options->getEncodedOption('window_title', ''),
			'sidebarMode' => $this->options->getIntegerOption('mode', 0) === 1,
			'showWindowTitle' => strlen($this->options->getEncodedOption('window_title', '')) > 0 || $this->options->getIntegerOption('mode', 0) === 1,
			'errorMessage' => $errorMessage,
			'cssClass' => $cssClass,
			'cssDefinitions' => $this->cssRenderer->getCssDefinition($chatId),
			'customCssDefinitions' => $this->cssRenderer->getCustomCssDefinition()
		);
		
		return $this->templater->render($data);
	}

	/**
	 * Returns the form which allows to enter username.
	 *
	 * @param WiseChatChannel $channel
	 * @param string|null $errorMessage
	 *
	 * @return string HTML source
	 * @throws Exception
	 */
	public function getRenderedUserNameForm($channel, $errorMessage = null) {
		$this->templater->setTemplateFile(WiseChatThemes::getInstance()->getUserNameFormTemplate());
		$chatId = $this->service->getChatID();
		$data = array(
			'chatId' => $chatId,
			'channelId' => $channel->getId(),
			'isDefaultTheme' => strlen($this->options->getEncodedOption('theme', '')) === 0,
			'themeStyles' => $this->options->getBaseDir().WiseChatThemes::getInstance()->getCss(),
			'windowTitle' => $this->options->getEncodedOption('window_title', ''),
			'sidebarMode' => $this->options->getIntegerOption('mode', 0) === 1,
			'showWindowTitle' => strlen($this->options->getEncodedOption('window_title', '')) > 0 || $this->options->getIntegerOption('mode', 0) === 1,
			'errorMessage' => $errorMessage,
			'messageLogin' => $this->options->getEncodedOption('message_login', 'Log in'),
			'messageEnterUserName' => $this->options->getEncodedOption('message_enter_user_name', 'Enter your username'),
			'cssDefinitions' => $this->cssRenderer->getCssDefinition($chatId),
			'customCssDefinitions' => $this->cssRenderer->getCustomCssDefinition()
		);

		return $this->templater->render($data);
	}

	/**
	 * Returns rendered message for specified user.
	 *
	 * @param WiseChatMessage $message
	 * @param integer|null $userId
	 *
	 * @return string HTML source
	 * @throws Exception
	 */
	 public function getRenderehistorydMessage($message, $userId, $reciveid='') {
		global $wpdb;
		 
		
		  $sqlsss = "SELECT * FROM wp_wise_chat_messages WHERE chat_recipient_id = $reciveid ORDER BY id DESC";
	
		  $messagesRaws = $wpdb->get_results($sqlsss);
		  
		  
		return 'test message';
	}

	
	 
	 public function getRenderedMessage($message, $userId, $reciveid='') {
		global $wpdb;
		
		$this->templater->setTemplateFile(WiseChatThemes::getInstance()->getMessageTemplate());
		$allowedToGetTheContent = $this->isUserAllowedToSeeTheContentOfMessage($message);
		
		  $sqlsss = "SELECT * FROM wp_wise_chat_messages WHERE chat_recipient_id = $reciveid ORDER BY id DESC";
	
		  $messagesRaws = $wpdb->get_results($sqlsss);
		  
		  $rowcunt = count( $sqlsss );
		  
		  foreach($messagesRaws as $messagesRaw){
			  
           $consaasf  =  $messagesRaw->text; 
		
		$data = array(
			'baseDir' => $this->options->getBaseDir(),
			'messageId' => $message->getId(),
			'messageUser' => $message->getUserName(),
			'messageChatUserId' => $message->getUserId(),
			'isAuthorWpUser' => $this->usersDAO->getWpUserByID($message->getWordPressUserId()) !== null,
			'isAuthorCurrentUser' => $userId == $message->getUserId(),
			'messageTimeUTC' => gmdate('c', $message->getTime()),
			'renderedUserName' => $this->getRenderedUserName($message),
			'avatarUrl' => $this->getUserAvatarForMessage($message, $this->options->isOptionEnabled('show_avatars', false)),
			'allowedToGetTheContent' => $allowedToGetTheContent,
			'hidden' => $message->isHidden(),
			'messageContent' => $consaasf,
			
			'isTextColorSet' => $this->options->isOptionEnabled('allow_change_text_color') &&
								$message->getUser() !== null &&
								strlen($message->getUser()->getDataProperty('textColor')) > 0,
			'textColor' => $message->getUser() !== null ? $message->getUser()->getDataProperty('textColor') : ''
		);
		
		return $this->templater->render($data);
		}
	}
	
	public function getallhistorymessage($reciveid){
		
		global $wpdb;
		 
		
		  $sqlsss = "SELECT * FROM wp_wise_chat_messages WHERE chat_recipient_id = $reciveid ORDER BY id DESC";
	
		  $messagesRaws = $wpdb->get_results($sqlsss);
		  
		 // $rowcunt = count( $sqlsss );
		  $data = array();
		  foreach($messagesRaws as $messagesRaw){
			
		$messageId = $messagesRaw->id; 
		
		$messageTimeUTC= gmdate('c', $messagesRaw->time); 
		
		$messageChatUserId = $messagesRaw->chat_user_id; 
		$avatarUrl = $messagesRaw->avatar_url; 
		$messagecontent = $messagesRaw->text;
		
		$message[] = $messagecontent;
		 
		}
		
		return implode('<br />', $message);
	}
	 
	/**
	 * Checks if the current user can get the message content.
	 *
	 * @param WiseChatMessage $message
	 *
	 * @return boolean
	 */
	private function isUserAllowedToSeeTheContentOfMessage($message) {
		if ($this->options->isOptionEnabled('new_messages_hidden', false) === false) {
			return true;
		}

		if (!$message->isHidden()) {
			return true;
		}

		$wpUser = $this->usersDAO->getCurrentWpUser();
		if ($wpUser !== null) {
			$targetRoles = (array) $this->options->getOption("show_hidden_messages_roles", 'administrator');
			if ((is_array($wpUser->roles) && count(array_intersect($targetRoles, $wpUser->roles)) > 0)) {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * @param WiseChatMessage $message
	 * @param boolean $enabled
	 *
	 * @return string|null
	 */
	private function getUserAvatarForMessage($message, $enabled) {
		if ($enabled) {
			if (strlen($message->getAvatarUrl()) > 0) {
				return $message->getAvatarUrl();
			} else {
				return $this->getUserAvatar($message->getUser(), $enabled, $message->getWordPressUserId());
			}
		}

		return null;
	}

	/**
	 * @param WiseChatUser $user
	 * @param boolean $enabled
	 * @param integer $priorityWordPressId
	 *
	 * @return string|null
	 */
	private function getUserAvatar($user, $enabled, $priorityWordPressId = null) {
		$imageSrc = null;

		if ($enabled) {
			if ($user !== null && strlen($user->getExternalId()) > 0) {
				$imageSrc = $user->getAvatarUrl();
			} else if ($priorityWordPressId > 0 || ($user !== null && $user->getWordPressId() !== null)) {
				$imageTag = $priorityWordPressId > 0 ? get_avatar($priorityWordPressId) : get_avatar($user->getWordPressId());
				
				$doc = new DOMDocument();
				$doc->loadHTML($imageTag);
				$imageTags = $doc->getElementsByTagName('img');
				foreach($imageTags as $tag) {
					$imageSrc = $tag->getAttribute('src');
				}
			} else {
				$imageSrc = $this->options->getIconsURL().'user.png';
			}
		}

		return $imageSrc;
	}
	
	/**
	* Returns rendered users list in the given channel.
	*
	* @param WiseChatChannel $channel
	*
	* @return string HTML source
	*/
	public function getRenderedUsersList($channel) {
		$hideRoles = $this->options->getOption('users_list_hide_roles', array());
		$channelUsers = $this->channelUsersDAO->getAllActiveByChannelId($channel->getId());
		$isCurrentUserPresent = false;
		$userId = $this->authentication->getUserIdOrNull();

		$usersList = array();
		foreach ($channelUsers as $channelUser) {
			if ($channelUser->getUser() == null) {
				continue;
			}			

			// do not render anonymous users:
			if ($this->service->isChatAllowedForWPUsersOnly() && !($channelUser->getUser()->getWordPressId() > 0)) {
				continue;
			}
			// veena code
			/*if (checkIsFriend($channelUser->getUser()->getWordPressId()) == false){
				continue;
			}*/
			$sdgfghf = WiseChatRenderer::chatgroup_chat($channelUser->getUserId());
			$seleceted_user_list_from = get_user_meta($channelUser->getUserId(), '_user_selected',true); 
			$sdgfghf = WiseChatRenderer::chatgroup_chat($channelUser->getUserId());
			$whocreategroup = get_user_meta($channelUser->getUserId(), '_who_create_selected',true);
			
			 $selectUsersList = explode( ",", $seleceted_user_list_from);
			 
			 $searchselectuser = count($selectUsersList);
			 
              if($whocreategroup != get_current_user_id())
			     {
					if( $searchselectuser != 1 )
				        {
						 $current_user_id = get_current_user_id();
						 if(!in_array($current_user_id, $selectUsersList) )
				            {
				             continue;  
			               }
						}
					else{
				          if (checkIsFriend($channelUser->getUser()->getWordPressId()) == false){
				          continue;
						  }
			            } 
				}
			// veena code
			/*$status_color = 'rgb(0, 128, 0)';
			$user_status = @get_user_meta($channelUser->getUser()->getWordPressId(), '_user_status', true);
			if($user_status){
				$status_color = 'rgb(122, 131, 140)';
			}*/
			// new code by nazmin
			
			$status_color = 'rgb(0, 128, 0)';
			$user_status = get_user_meta($channelUser->getUser()->getWordPressId(), '_user_statusnaz', true);
			if($user_status == 1){
				$status_color = 'rgb(0, 128, 0)';
			}else{
				$status_color =  'rgb(255,165,0)';
			}

			// hide chosen roles:
			if (is_array($hideRoles) && count($hideRoles) > 0 && $channelUser->getUser()->getWordPressId() > 0) {
				$wpUser = $this->usersDAO->getWpUserByID($channelUser->getUser()->getWordPressId());
				if (is_array($wpUser->roles) && count(array_intersect($hideRoles, $wpUser->roles)) > 0) {
					continue;
				}
			}

			// do not render anonymous users if it is not WP user and externally logged in:
			if ($this->options->isOptionEnabled('users_list_hide_anonymous', false) &&
				!($channelUser->getUser()->getWordPressId() > 0) &&
				strlen($channelUser->getUser()->getExternalType()) == 0
			) {
				continue;
			}

			// text color feature:
			$styles = '';
			if ($this->options->isOptionEnabled('allow_change_text_color')) {
				$textColor = $channelUser->getUser()->getDataProperty('textColor');
				if (strlen($textColor) > 0) {
					$styles = sprintf('style="color: %s"', $textColor);
				}
			}
			
			// veena code
			$avatarHtml = '';
			if ($this->options->isOptionEnabled('show_users_list_avatars', false)) {
				$avatarHtml = sprintf('<img src="%s" class="wcUserListAvatar" /><span style="background-color: '.$status_color.';border-radius: 10px;display: inline-block;height: 10px;width: 10px;vertical-align:middle;"></span>&nbsp;&nbsp;', $this->getUserAvatar($channelUser->getUser(), true));
			}

			$currentUserClassName = '';
			if ($userId == $channelUser->getUserId()) {
				if($sdgfghf == 0){
					$isCurrentUserPresent = true;
				    $currentUserClassName = 'wcCurrentUser';
				}
			}

            $flag = '';
            if ($this->options->isOptionEnabled('collect_user_stats', true) && $this->options->isOptionEnabled('show_users_flags', false)) {
                $countryCode = $channelUser->getUser()->getDataProperty('countryCode');
                $country = $channelUser->getUser()->getDataProperty('country');
                if (strlen($countryCode) > 0) {
                    $flagURL = $this->options->getFlagURL(strtolower($countryCode));
                    $flag = " <img src='{$flagURL}' class='wcUsersListFlag wcIcon' alt='{$countryCode}' title='{$country}'/>";
                }
            }
            $cityAndCountry = '';
            if ($this->options->isOptionEnabled('collect_user_stats', true) && $this->options->isOptionEnabled('show_users_city_and_country', false)) {
                $cityAndCountryArray = array();
                $city = $channelUser->getUser()->getDataProperty('city');
                if (strlen($city) > 0) {
                    $cityAndCountryArray[] = $city;
                }

                $countryCode = $channelUser->getUser()->getDataProperty('countryCode');
                if (strlen($countryCode) > 0) {
                    $cityAndCountryArray[] = $countryCode;
                }

                if (count($cityAndCountryArray) > 0) {
                    $cityAndCountry = ' <span class="wcUsersListCity">'.implode(', ', $cityAndCountryArray).'</span>';
                }
            }

			$publicID = $this->getUserPublicIdForChannel($channelUser->getUser(), $channel);
			$userIdHash = WiseChatUserService::getUserHash($channelUser->getUser()->getId());
			
			/*$gropidsnn = WiseChatRenderer::get_group_id_chats($channelUser->getUserId());
			if($gropidsnn == 0)
			{
				$userIdHash = WiseChatUserService::getUserHash($channelUser->getUser()->getId());
			}else{
				$userIdHash = WiseChatUserService::getUserHash($gropidsnn);
			}*/
			
			$encodedName = WiseChatRenderer::chatgroup($channelUser->getUserId());
			if($encodedName == 0){
				$encodedName = htmlspecialchars($channelUser->getUser()->getName(), ENT_QUOTES, 'UTF-8');
			}
			if ($this->options->isOptionEnabled('enable_private_messages', false) || !$this->options->isOptionEnabled('users_list_linking', false)) {
				if($currentUserClassName != 'wcCurrentUser'){
				$usersList[] = sprintf(
					'<a href="javascript://" data-public-id="%s" data-hash="%s" data-name="%s" class="wcUserInChannel %s" %s>%s %s</a>',
					$publicID, $userIdHash, $encodedName, $currentUserClassName, $styles, $avatarHtml . $encodedName,
					$flag . $cityAndCountry
				);
				}
			} else if ($this->options->isOptionEnabled('users_list_linking', false)) {
				$usersList[] = $this->getRenderedUserNameInternal(
					$encodedName, $channelUser->getUser()->getWordPressId(), $channelUser->getUser(), 'wcUserInChannel ' . $currentUserClassName,
					$avatarHtml . $encodedName . ' ' . $flag . $cityAndCountry, true
				);
			}
		}
		
		if (!$isCurrentUserPresent && $this->authentication->isAuthenticated()) {
			/* $hidden = false;
			if (is_array($hideRoles) && count($hideRoles) > 0 && $this->authentication->getUser()->getWordPressId() > 0) {
				$wpUser = $this->usersDAO->getWpUserByID($this->authentication->getUser()->getWordPressId());
				if (is_array($wpUser->roles) && count(array_intersect($hideRoles, $wpUser->roles)) > 0) {
					$hidden = true;
				}
			}

			if (!$hidden && (
					!$this->options->isOptionEnabled('users_list_hide_anonymous', false) ||
					$this->authentication->getUser()->getWordPressId() > 0 ||
					$this->authentication->isAuthenticatedExternally()
				)
			) {
				$publicID = $this->getUserPublicIdForChannel($this->authentication->getUser(), $channel);
				$userIdHash = WiseChatUserService::getUserHash($this->authentication->getUser()->getId());
				array_unshift(
					$usersList, sprintf(
						'<a href="javascript://" data-public-id="%s" data-hash="%s" data-name="%s" class="wcUserInChannel wcCurrentUser">%s</a>',
						$publicID, $userIdHash, $this->authentication->getUserNameOrEmptyString(), $this->authentication->getUserNameOrEmptyString()
					)
				);
			} */
		}
		
		return implode('<br />', $usersList);
	}
	
	public function get_group_id_chats($chatid){
		global $wpdb;
		
         $returnvalue = 0;
		 
		$table = WiseChatInstaller::getChannelUsersTable();
		$sql = sprintf(
			'SELECT group_id FROM %s WHERE user_id = %d ', $table, intval($chatid)
		);
		$results = $wpdb->get_results($sql);
		if (is_array($results) && count($results) > 0) {
			foreach($results as $result){
				$returnvalue = $result->group_id;
			}
			
			 
		}
       // $returnvalue = $chatid;
		return $returnvalue;
	}
	
	public function chatgroup($chatid){
		global $wpdb;
		
         $returnvalue = 0;
		 
		$table = WiseChatInstaller::getUsersTable();
		$sql = sprintf(
			'SELECT * FROM %s WHERE id = %d AND session_id = ""', $table, intval($chatid)
		);
		$results = $wpdb->get_results($sql);
		if (is_array($results) && count($results) > 0) {
			foreach($results as $result){
				$returnvalue = $result->name;
			}
			
			 
		}
       // $returnvalue = $chatid;
		return $returnvalue;
	}
	
	public function chatgroup_chat($chatid){
		global $wpdb;
		
         $returnvalue = 0;
		 
		$table = WiseChatInstaller::getUsersTable();
		$sql = sprintf(
			'SELECT * FROM %s WHERE id = %d AND created = 0', $table, intval($chatid)
		);
		$results = $wpdb->get_results($sql);
		if (is_array($results) && count($results) > 0) {
			
			
			$returnvalue = 1; 
		}
       // $returnvalue = $chatid;
		return $returnvalue;
	}

	/**
	 * Returns user's public ID. It is an encrypted combination of user's ID and channel's ID.
	 *
	 * @param WiseChatUser $user
	 * @param WiseChatChannel $channel
	 * @return string
	 */
	public function getUserPublicIdForChannel($user, $channel) {
		$publicIdData = array($user->getId(), $channel->getId());

		return base64_encode(WiseChatCrypt::encrypt(serialize($publicIdData)));
	}

	/**
	 * Returns rendered user name for given message.
	 *
	 * @param WiseChatMessage $message
	 *
	 * @return string HTML source
	 */
	public function getRenderedUserName($message) {
		return $this->getRenderedUserNameInternal($message->getUserName(), $message->getWordPressUserId(), $message->getUser());
	}
	
	/**
	* Returns rendered user name.
	*
	* @param string $userName
	* @param integer $wordPressUserId
	* @param WiseChatUser $user
	* @param string $className
	* @param string $customUserName
	* @param boolean $makeAlwaysLink
	*
	* @return string HTML source
	*/
	public function getRenderedUserNameInternal($userName, $wordPressUserId, $user, $className = '', $customUserName = null, $makeAlwaysLink = false) {
		$formattedUserName = $userName;
		$displayMode = $this->options->getIntegerOption('link_wp_user_name', 0);
		$styles = '';
		if ($displayMode > 0) {
			if (
				$this->options->isOptionEnabled('allow_change_text_color') &&
				$user !== null &&
				strlen($user->getDataProperty('textColor')) > 0
			) {
				$styles = sprintf('style="color: %s"', $user->getDataProperty('textColor'));
			}
		}


		if ($displayMode === 1) {
			$linkUserNameTemplate = $this->options->getOption('link_user_name_template', null);
			$wpUser = $wordPressUserId != null ? $this->usersDAO->getWpUserByID($wordPressUserId) : null;

			$variableId = '';
			$variableUserName = $variableDisplayName = $userName;
			if ($user !== null && strlen($user->getExternalType()) > 0) {
				$variableId = $user->getExternalId();
			} else if ($wpUser !== null) {
				$variableId = $wpUser->ID;
				$variableUserName = $wpUser->user_login;
				$variableDisplayName = $wpUser->display_name;
			}

			$userNameLink = null;
			if ($linkUserNameTemplate != null) {
				$variables = array(
					'id' => $variableId,
					'username' => $variableUserName,
					'displayname' => $variableDisplayName
				);
				
				$userNameLink = $this->getTemplatedString($variables, $linkUserNameTemplate);
			} else if ($user !== null && strlen($user->getExternalType()) > 0) {
				$userNameLink = $user->getProfileUrl();
			} else if ($wpUser !== null) {
				$userNameLink = get_author_posts_url($wpUser->ID);
			}

			if ($customUserName != null) {
				$formattedUserName = $customUserName;
			}
			
			if ($userNameLink != null) {
				$formattedUserName = sprintf(
					"<a href='%s' target='_blank' class='%s' rel='nofollow' %s>%s</a>", $userNameLink, $className, $styles, $formattedUserName
				);
			} else if ($makeAlwaysLink) {
				$formattedUserName = sprintf(
					"<a href='javascript://' class='%s' %s>%s</a>", $className, $styles, $formattedUserName
				);
			}
		} else if ($displayMode === 2) {
            $replyTag = '@'.$formattedUserName.':';
            $title = htmlspecialchars($this->options->getOption('message_insert_into_message', 'Insert into message').': '.$replyTag, ENT_COMPAT);

			if ($customUserName != null) {
				$formattedUserName = $customUserName;
			}

            $formattedUserName = sprintf(
                "<a href='javascript://' class='wcMessageUserReplyTo %s' data-name='%s' %s title='%s'>%s</a>", $className, $userName, $styles, $title, $formattedUserName
            );
        } else if ($makeAlwaysLink) {
			if ($customUserName != null) {
				$formattedUserName = $customUserName;
			}

			$formattedUserName = sprintf(
				"<a href='javascript://' class='%s' %s>%s</a>", $className, $styles, $formattedUserName
			);
		}
		
		return $formattedUserName;
	}
	
	/**
	* Returns rendered channel statistics.
	*
	* @param WiseChatChannel $channel
	*
	* @return string HTML source
	*/
	public function getRenderedChannelStats($channel) {
		if ($channel === null) {
			return 'ERROR: channel does not exist';
		}

		$variables = array(
			'channel' => $channel->getName(),
			'messages' => $this->messagesService->getNumberByChannelName($channel->getName()),
			'users' => $this->channelUsersDAO->getAmountOfUsersInChannel($channel->getId())
		);
	
		return $this->getTemplatedString($variables, $this->options->getOption('template', 'ERROR: TEMPLATE NOT SPECIFIED'));
	}
	
	/**
	* Returns rendered message content.
	*
	* @param WiseChatMessage $message
	*
	* @return string HTML source
	*/
	private function getRenderedMessageContent($message) {
		$formattedMessage = htmlspecialchars($message->getText(), ENT_QUOTES, 'UTF-8');

        /** @var WiseChatLinksPostFilter $linksFilter */
        $linksFilter = WiseChatContainer::get('rendering/filters/post/WiseChatLinksPostFilter');
		$formattedMessage = $linksFilter->filter(
            $formattedMessage,
            $this->options->isOptionEnabled('allow_post_links')
        );

        /** @var WiseChatAttachmentsPostFilter $attachmentsFilter */
        $attachmentsFilter = WiseChatContainer::get('rendering/filters/post/WiseChatAttachmentsPostFilter');
		$formattedMessage = $attachmentsFilter->filter(
			$formattedMessage,
            $this->options->isOptionEnabled('enable_attachments_uploader'),
            $this->options->isOptionEnabled('allow_post_links')
		);

        /** @var WiseChatImagesPostFilter $imagesFilter */
        $imagesFilter = WiseChatContainer::get('rendering/filters/post/WiseChatImagesPostFilter');
        $formattedMessage = $imagesFilter->filter(
			$formattedMessage,
            $this->options->isOptionEnabled('allow_post_images'),
            $this->options->isOptionEnabled('allow_post_links')
		);

        /** @var WiseChatYouTubePostFilter $youTubeFilter */
        $youTubeFilter = WiseChatContainer::get('rendering/filters/post/WiseChatYouTubePostFilter');
		$formattedMessage = $youTubeFilter->filter(
			$formattedMessage,
            $this->options->isOptionEnabled('enable_youtube'),
            $this->options->isOptionEnabled('allow_post_links'),
			$this->options->getIntegerOption('youtube_width', 186),
            $this->options->getIntegerOption('youtube_height', 105)
		);
		
		if ($this->options->isOptionEnabled('enable_twitter_hashtags')) {
            /** @var WiseChatHashtagsPostFilter $hashTagsFilter */
            $hashTagsFilter = WiseChatContainer::get('rendering/filters/post/WiseChatHashtagsPostFilter');
			$formattedMessage = $hashTagsFilter->filter($formattedMessage);
		}

		$emoticonsSet = $this->options->getIntegerOption('emoticons_enabled', 1);
		if ($emoticonsSet > 0) {
            /** @var WiseChatEmoticonsFilter $emoticonsFilter */
            $emoticonsFilter = WiseChatContainer::get('rendering/filters/post/WiseChatEmoticonsFilter');
            $formattedMessage = $emoticonsFilter->filter($formattedMessage, $emoticonsSet);
		}
		
		$formattedMessage = str_replace("\n", '<br />', $formattedMessage);
		
		return $formattedMessage;
	}
	
	private function getTemplatedString($variables, $template) {
		foreach ($variables as $key => $value) {
			$template = str_replace("{".$key."}", urlencode($value), $template);
		}
		
		return $template;
	}
}