<?php 
/**
 * Admin Page Class
 *
 * The Admin Page Class is used by including it in your plugin files and using its methods to 
 * create custom Admin Pages. It is meant to be very simple and 
 * straightforward. 
 *
 * This class is derived from My-Meta-Box (https://github.com/bainternet/My-Meta-Box script) which is 
 * a class for creating custom meta boxes for WordPress. 
 * 
 *  
 * @version 1.3.0
 * @copyright 2012 - 2014
 * @author Ohad Raz (email: admin@bainternet.info)
 * @link http://en.bainternet.info
 * 
 * @license GNU General Public LIcense v3.0 - license.txt
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package Admin Page Class
 * 
 * @Last Revised 
 */

if ( ! class_exists( 'BF_Admin_Page_Class') ) :

/**
 * Admin Page Class
 *
 * @package Admin Page Class
 * @since 0.1
 *
 * @todo Nothing.
 */

  class BF_Admin_Page_Class {
  
 

} // End Class

endif; // End Check Class Exists