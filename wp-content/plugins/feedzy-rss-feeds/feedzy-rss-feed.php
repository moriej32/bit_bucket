<?php
/**
 * Plugin Name: FEEDZY RSS Feeds
 * Plugin URI: http://themeisle.com/plugins/feedzy-rss-feeds/
 * Description: FEEDZY RSS Feeds is a small and lightweight plugin. Fast and easy to use, it aggregates RSS feeds into your WordPress site through simple shortcodes.				
 * Author: Themeisle
 * Author URI: http://themeisle.com
 * Version: 2.8.1
 * Text Domain: feedzy_rss_translate
 * Text Domain Path: /langs
 */

/***************************************************************
 * SECURITY : Exit if accessed directly
***************************************************************/
if ( !defined( 'ABSPATH' ) ) {
	
	die( 'Direct access not allowed!' );
	
}


/***************************************************************
 * Define constant
 ***************************************************************/
if ( !defined( 'FEEDZY_VERSION' ) ) {
	
    define( 'FEEDZY_VERSION', '2.8.1' );
	
}


/***************************************************************
 * Load plugin textdomain
 ***************************************************************/
function feedzy_rss_load_textdomain() {
	
	$path = dirname(plugin_basename( __FILE__ )) . '/langs/';
	load_plugin_textdomain( 'feedzy_rss_translate', false, $path );
	
}
add_action( 'init', 'feedzy_rss_load_textdomain' );


/***************************************************************
 * Add custom meta link on plugin list page
 ***************************************************************/
function feedzy_meta_links( $links, $file ) {
	
	if ( $file === 'feedzy-rss-feeds/feedzy-rss-feed.php' ) {
		$links[] = '<a href="http://docs.themeisle.com/article/277-feedzy-rss-feeds-hooks" target="_blank" title="'. __( 'Documentation and examples', 'feedzy_rss_translate' ) .'">'. __( 'Documentation and examples', 'feedzy_rss_translate' ) .'</a>';
		$links[] = '<a href="http://themeisle.com/wordpress-plugins/" target="_blank" title="'. __( 'More Plugins', 'feedzy_rss_translate' ) .'">'. __( 'More Plugins', 'feedzy_rss_translate' ) .'</a>';
	}
	
	return $links;
	
}
add_filter( 'plugin_row_meta', 'feedzy_meta_links', 10, 2 );


/***************************************************************
 * Load plugin files
 ***************************************************************/
$feedzyFiles = array( 'functions', 'shortcode', 'widget','ui' );
foreach( $feedzyFiles as $feedzyFile ){
	
	require_once( plugin_dir_path( __FILE__ ) . 'feedzy-rss-feeds-' . $feedzyFile . '.php' );
	
}

add_filter('admin_init', 'feedzy_add_actions_to_feed_general_registration_settings');
 
function feedzy_add_actions_to_feed_general_registration_settings()
{
    register_setting('general', 'feedzy_add_actions', 'esc_attr');
    add_settings_field('feedzy_add_actions', '<label for="feedzy_add_actions">'.__('Add all actions to feed' , 'feedzy_add_actions' ).'</label>' , 'feedzy_add_actions_to_feed_html', 'general');
}
 
function feedzy_add_actions_to_feed_html()
{
    $value = get_option( 'feedzy_add_actions', false );
    $checked = $value == "on" ? 'checked' : "";
    echo '<input type="checkbox" id="feedzy_add_actions" name="feedzy_add_actions" ' . $checked. ' />';
}

add_filter('admin_init', 'feedzy_feedzy_notify_me_general_registration_settings');

function feedzy_feedzy_notify_me_general_registration_settings()
{
    register_setting('general', 'feedzy_notify_me', 'esc_attr');
    add_settings_field('feedzy_notify_me', '<label for="feedzy_notify_me">'.__('Notify me of each action before posting to feed' , 'feedzy_notify_me' ).'</label>' , 'feedzy_notify_me', 'general');
}
 
function feedzy_notify_me()
{
    $value = get_option( 'feedzy_notify_me', false );
    $checked = $value == "on" ? 'checked' : "";
    echo '<input type="checkbox" id="feedzy_notify_me" name="feedzy_notify_me" ' . $checked . ' />';
}

add_filter('admin_init', 'feedzy_notify_me_general_registration_settings');

function feedzy_notify_me_general_registration_settings()
{
    register_setting('general', 'feedzy_do_not_add_any_actions', 'esc_attr');
    add_settings_field('feedzy_do_not_add_any_actions', '<label for="feedzy_do_not_add_any_actions">'.__('Do not add any actions to feed' , 'feedzy_do_not_add_any_actions' ).'</label>' , 'feedzy_do_not_add_any_actions', 'general');
}
 
function feedzy_do_not_add_any_actions()
{
    $value = get_option( 'feedzy_do_not_add_any_actions', false );
    $checked = $value == "on" ? 'checked' : "";
    echo '<input type="checkbox" id="feedzy_do_not_add_any_actions" name="feedzy_do_not_add_any_actions" ' . $checked . ' />';
}