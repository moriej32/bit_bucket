<?php
/***************************************************************
 * SECURITY : Exit if accessed directly
***************************************************************/
if ( !defined( 'ABSPATH' ) ) {
	die( 'Direct access not allowed!' );
}

class PostWrapper {    
    private $post = null; 
    private $author = null; 
    public $type;
    public $medium_file;
    public $thumbnail_file;
    public function __construct( $post ) {
        $this->post = $post;
        $this->author = new AuthorWrapper($post);
        if($post->post_type == "dyn_file")
        {
            $this->type = "file";
        }
    }
    
    function get_title()
    {
        return $this->post->post_title;
    }
    
    function get_permalink()
    {
        return $this->post->guid;
    }
    
    function get_author()
    {
        return $this->author;
    }
    
    function get_description()
    {
        return $this->post->post_title;
    }
    
    function get_content()
    {
        return $this->post->post_content;
    }
    
    function get_date($format)
    {
        return (new DateTime($this->post->post_date_gmt))->format($format);
    }
    
    function get_attachment()
    {  
        return $this->post->guid2;
    }

} 

class AuthorWrapper {  
    private $author = null;
    
    public function __construct( $author ) {
        $this->author = $author;
    }
    
    function get_name()
    {
        return $this->author->display_name;
    }
    
    function get_nicename()
    {
        return $this->author->user_nicename;
    }
    
    function get_id()
    {
        return $this->author->ID;
    }
    
    function get_email()
    {
        return $this->author->user_email;
    }
    
    
}
	
/***************************************************************
 * Main shortcode function
 ***************************************************************/
function feedzy_rss( $atts, $content = '' ) {
	global $feedzyStyle;
    global $wpdb;
	$feedzyStyle = true;
	$count = 0;

	//Load SimplePie if not already
	if ( !class_exists( 'SimplePie' ) ){
		require_once( ABSPATH . WPINC . '/class-feed.php' );
	}

	//Retrieve & extract shorcode parameters
	extract( shortcode_atts( array(
		"feeds" => '', 			//comma separated feeds url
		"max" => '5', 			//number of feeds items (0 for unlimited)
		"feed_title" => 'yes', 	//display feed title yes/no
		"target" => '_blank', 	//_blank, _self
		"title" => '', 			//strip title after X char
		"meta" => 'yes', 		//yes, no
		"summary" => 'yes', 	//strip title
		"summarylength" => '', 	//strip summary after X char
		"thumb" => 'yes', 		//yes, no, auto
		"default" => '', 		//default thumb URL if no image found (only if thumb is set to yes or auto)
		"size" => '', 			//thumbs pixel size
		"keywords_title" => '' 	//only display item if title contains specific keywords (comma-separated list/case sensitive)
		), $atts, 'feedzy_default' ) );

	//Use "shortcode_atts_feedzy_default" filter to edit shortcode parameters default values or add your owns.

	if ( !empty( $feeds ) ) {
		$feeds = rtrim( $feeds, ',' );
		$feeds = explode( ',', $feeds );
		
		//Remove SSL from HTTP request to prevent fetching errors
		foreach( $feeds as $feed ){
			$feedURL[] = preg_replace("/^https:/i", "http:", $feed);
		}

		if ( count( $feedURL ) === 1 ) {
			$feedURL = $feedURL[0];
		}
		
	}
	
	if ( $max == '0' ) {
		$max = '999';
	} else if ( empty( $max ) || !ctype_digit( $max ) ) {
		$max = '5';
	}

	if ( empty( $size ) || !ctype_digit( $size ) ){
		$size = '150';
	}
	$sizes = array( 'width' => $size, 'height' => $size );
	$sizes = apply_filters( 'feedzy_thumb_sizes', $sizes, $feedURL );

	if ( !empty( $title ) && !ctype_digit( $title ) ){
		$title = '';
	}

	if ( !empty($keywords_title)){
		$keywords_title = rtrim( $keywords_title, ',' );
		$keywords_title = array_map( 'trim', explode( ',', $keywords_title ) );
	}

	if ( !empty( $summarylength ) && !ctype_digit( $summarylength ) ){
		$summarylength = '';
	}

	if ( !empty( $default ) ) {
		$default = $default;
	
	} else {
		$default = apply_filters( 'feedzy_default_image', $default, $feedURL );
	}
    $are_feeds = !empty( $feeds);
    if ($are_feeds)
    {
        //Load SimplePie Instance
        $feed = new SimplePie();
        $feed -> set_feed_url( $feedURL );
        $feed -> enable_cache( true );
        $feed -> enable_order_by_date( true );
        $feed -> set_cache_class( 'WP_Feed_Cache' );
        $feed -> set_file_class( 'WP_SimplePie_File' );
        $feed -> set_cache_duration( apply_filters( 'wp_feed_cache_transient_lifetime', 7200, $feedURL ) );
        do_action_ref_array( 'wp_feed_options', array( $feed, $feedURL ) );
        $feed -> strip_comments( true );
        $feed -> strip_htmltags( array( 'base', 'blink', 'body', 'doctype', 'embed', 'font', 'form', 'frame', 'frameset', 'html', 'iframe', 'input', 'marquee', 'meta', 'noscript', 'object', 'param', 'script', 'style' ) );
        $feed -> init();
        $feed -> handle_content_type();

        // Display the error message
        if ( $feed -> error() ) {
            $content .= apply_filters( 'feedzy_default_error', $feed -> error(), $feedURL );	
        }

        $content .= '<div class="feedzy-rss">';

        if ( $feed_title == 'yes' ) {

            $content .= '<div class="rss_header">';
            $content .= '<h2><a href="' . $feed->get_permalink() . '" class="rss_title">' . html_entity_decode( $feed->get_title() ) . '</a> <span class="rss_description"> ' . $feed->get_description() . '</span></h2>';
            $content .= '</div>';
            
        }

        $content .= '<ul>';

        //Loop through RSS feed
        $items = apply_filters( 'feedzy_feed_items', $feed->get_items(), $feedURL );
    }
    else
    {
        
        //$results = $wpdb->get_results( 'SELECT to_friend_id FROM wp_authors_friends_list WHERE from_friend_id = '.get_current_user_id()  );
        $results = $wpdb->get_results( 'SELECT author_id FROM wp_authors_suscribers WHERE suscriber_id='.get_current_user_id()  );
        $postslist = get_posts( $args );
        $users = [];
        //array_push($users, 5);
        foreach ($results as $result)
        {
            array_push($users, $result->author_id);
        }
        
        $items = [];
        $attachment_metadata = [];
        if(count($users) > 0)
        {
            $dyn_files =  'SELECT wp_users.*, wp_posts.*, wp_posts2.guid as guid2, wp_posts2.ID as pId2 FROM wp_posts 
                INNER JOIN wp_users 
                ON wp_users.ID = wp_posts.post_author 
                INNER JOIN wp_postmeta
                ON wp_postmeta.post_id = wp_posts.ID 
                INNER JOIN wp_posts as wp_posts2
                ON wp_postmeta.meta_value = wp_posts2.ID
                WHERE wp_posts.post_author IN ('. join(',', $users) .'  ) AND (wp_posts.post_type="dyn_file" OR wp_posts.post_type="dyn-file-upload") AND wp_postmeta.meta_key="dyn_upload_value"';
            
            
            //$results = $wpdb->get_results( 'SELECT wp_posts.*, wp_users.* FROM wp_posts INNER JOIN wp_users 
            //ON wp_users.ID= wp_posts.post_author WHERE wp_posts.post_author IN ('. join(',', $users) .'  ) ORDER BY post_modified_gmt DESC' );
           
            $results = $wpdb->get_results( $dyn_files . " ORDER BY wp_posts.post_modified_gmt DESC" );
            $posts = [];
            foreach($results as $result)
            {
                $wrapper = new PostWrapper($result);
                array_push($items, $wrapper);
                $posts[$result->pId2] = $wrapper;
            }
            
            $postmeta_results = $wpdb->get_results("SELECT post_id, meta_value FROM wp_postmeta WHERE meta_key='_wp_attachment_metadata' AND post_id IN (". join(',', array_keys($posts)) ."  )" );
            
            foreach($postmeta_results as $result )
            {
                $attachment_metadata[$result->post_id] = unserialize($result->meta_value);
            }
            
            foreach($attachment_metadata as $key=>$value)
            {
                $posts[$key]->medium_file =  $attachment_metadata[$key]['sizes']['medium']['file'];
                $posts[$key]->thumbnail_file =  $attachment_metadata[$key]['sizes']['thumbnail']['file'];
            }
        }
        
        $content .= '<div class="feedzy-rss">';
        $content .= '<ul>';
    }
    

    
    foreach ( (array) $items as $item ) {
		$continue = apply_filters( 'feedzy_item_keyword', true, $keywords_title, $item, $feedURL ); 

		if ( $continue == true ) {

			//Count items
			if ( $count >= $max ){
				break;
			}
			$count++;
            
            //Fetch image thumbnail
            $itemAttr = apply_filters( 'feedzy_item_attributes', $itemAttr = '', $sizes, $item, $feedURL );
            //Build element DOM
            $content .= '<li ' . $itemAttr . '>';
            $content .= '<div class="rss_item_header">';
            if($are_feeds)
            {
                if ( $thumb == 'yes' || $thumb == 'auto') {
                    $thethumbnail = feedzy_retrieve_image( $item );
                }
                
                if ( $thumb == 'yes' || $thumb == 'auto' ) {
                    
                    $contentThumb = '';
                    
                    if ( ( ! empty( $thethumbnail ) && $thumb == 'auto' ) || $thumb == 'yes' ){
                        
                        $contentThumb .= '<div class="rss_image" style="width:' . $sizes['width'] . 'px; height:' . $sizes['height'] . 'px;">';
                        $contentThumb .= '<a href="' . $item->get_permalink() . '" target="' . $target . '" title="' . $item->get_title() . '" >';
                    
                        if ( !empty( $thethumbnail )) {
                            
                            $thethumbnail = feedzy_image_encode( $thethumbnail );
                            $contentThumb .= '<span class="default" style="width:' . $sizes['width'] . 'px; height:' . $sizes['height'] . 'px; background-image:  url(' . $default . ');" alt="' . $item->get_title() . '"></span>';
                            $contentThumb .= '<span class="fetched" style="width:' . $sizes['width'] . 'px; height:' . $sizes['height'] . 'px; background-image:  url(' . $thethumbnail . ');" alt="' . $item->get_title() . '"></span>';
                        
                        } else if ( empty( $thethumbnail ) && $thumb == 'yes' ) {
                            $contentThumb .= '<span style="width:' . $sizes['width'] . 'px; height:' . $sizes['height'] . 'px; background-image:url(' . $default . ');" alt="' . $item->get_title() . '"></span>';
                        }

                        $contentThumb .= '</a>';
                        $contentThumb .= '</div>';
                        
                    }

                    //Filter: feedzy_thumb_output
                    $content .= apply_filters( 'feedzy_thumb_output', $contentThumb, $feedURL );
                    
                }
            }
            else
            {
                
                $profile_meta = 'profile_pic_meta';
                
                $profile_img = get_user_meta( $item->get_author()->get_id(), $profile_meta, true );
                if($profile_img){
                    $img2show = site_url() . '/profile_pic/' . $profile_img;
                    $profile_link = "<img src={$img2show} height='55' width='55' / >";
                }
                $avatar = get_avatar($profile_id);
                $avatar = str_replace('&', '%26', $avatar);
                $avatarObj = DOMDocument::loadHTML($avatar);

                $imgObj = $avatarObj->getElementsByTagName('img')->item(0);
                $imgObj->setAttribute('height', 55);
                $imgObj->setAttribute('width', 55);
                $avatar = $imgObj->ownerDocument->saveHTML($imgObj);
                $content .= '<div class="rss_avatar">';
                $content .= $profile_link ? $profile_link : $avatar;
                $content .= '<div><a href="'.home_url('user/'.$item->get_author()->get_nicename()).'">'.$item->get_author()->get_name().'</a></div>';
                $content .= '</div>';
            }
			
			
            
            
            $contentTitle = '';
            $contentTitle .= '<span class="title"><a href="' . $item->get_permalink() . '" target="' . $target . '">';
         
            if ( is_numeric( $title ) && strlen( $item->get_title() ) > $title ) {
            
                $contentTitle .= preg_replace( '/\s+?(\S+)?$/', '', substr( $item->get_title(), 0, $title ) ) . '...';
            } else {
                $contentTitle .= $item->get_title();
            }
            $contentTitle .= '</a></span>';
            
            //Filter: feedzy_title_output
            $content .= apply_filters( 'feedzy_title_output', $contentTitle, $feedURL );
            
            $content .= '</div>';
			$content .= '<div class="rss_content">';

            //Define Meta args
            $metaArgs = array(
                        'author' => true,
                        'date' => true,
                        'date_format' => get_option( 'date_format' ),
                        'time_format' => get_option( 'time_format' )
                    );
           
            //Filter: feedzy_meta_args
            $metaArgs = apply_filters( 'feedzy_meta_args', $metaArgs, $feedURL );

			if ( $meta == 'yes' && ( $metaArgs[ 'author' ] || $metaArgs[ 'date' ] ) ) {

				$contentMeta = '';
                if (isset($item->type) && $item->type=="file")
                {       
                    if(!empty($item->thumbnail_file))
                    {
                        $contentMeta .= '<a href="'.$item->get_attachment() .'"><img src="'.home_url('wp-content/uploads/'.$item->thumbnail_file) .'"/></a>';
                        $contentMeta .= '<small>'. __( 'Posted', 'feedzy_rss_translate' ) . ' ';
                    }
                    else
                    {
                        $contentMeta .= '<small>'.'<a href="'.$item->get_attachment() .'">File</a> ' . __( 'posted', 'feedzy_rss_translate' ) . ' ';
                    }
                }
                else
                {
                    $contentMeta .= '<small>'. __( 'Posted', 'feedzy_rss_translate' ) . ' ';
                }

				if ( $item->get_author() && $metaArgs[ 'author' ] ) {
					$author = $item->get_author();
					if ( !$authorName = $author->get_name() ){
						$authorName = $author->get_email();
					}
					
					if( $authorName ){
						$domain = parse_url( $item->get_permalink() );
						$contentMeta .= __( 'by', 'feedzy_rss_translate' ) . ' <a href="'.($are_feeds ? 'http://' . $domain[ 'host' ] :  home_url('user/'.$item->get_author()->get_nicename())) .'" 
                        target="' . $target . '" '.($are_feeds ? ' title="' . $domain[ 'host' ] . '"':"").' >' . $authorName . '</a> ';
					}

				}

				if ( $metaArgs[ 'date' ] ) {
               
					$contentMeta .= __( 'on', 'feedzy_rss_translate') . ' ' . date_i18n( $metaArgs[ 'date_format' ], $item->get_date( 'U' ) );
					$contentMeta .= ' ';
                    $contentMeta .= __( 'at', 'feedzy_rss_translate' ) . ' ' . date_i18n( $metaArgs[ 'time_format' ], $item->get_date( 'U' ) );
				}
				
				$contentMeta .= '</small>';
                //Filter: feedzy_meta_output
                $content .= apply_filters( 'feedzy_meta_output', $contentMeta, $feedURL );
			}
            
			if ( $summary == 'yes' ) {
                $contentSummary = '';
                
                //Filter: feedzy_summary_input
                                 
                                    
                $contentSummary .= '<p>';
                if($are_feeds)
                {
                    $description = $item->get_description();   
                    $description = apply_filters( 'feedzy_summary_input', $description, $item->get_content(), $feedURL );
                    
                    if ( is_numeric( $summarylength ) && strlen( $description ) > $summarylength ) {
                        $contentSummary .= preg_replace( '/\s+?(\S+)?$/', '', substr( $description, 0, $summarylength ) ) . ' […]';
                    
                    } else {
                        $contentSummary .= $description . ' […]';
                    }
                }
                else{
                    $description = $item->get_content();   
                    if ( is_numeric( $summarylength ) && strlen( $description ) > $summarylength ) {
                        $contentSummary .= preg_replace( '/\s+?(\S+)?$/', '', substr( $description, 0, $summarylength ) ) . ' […]';
                    
                    } else {
                        $contentSummary .= $description ;
                    }
                }
                
                
                $contentSummary .= '</p>';
                //Filter: feedzy_summary_output
                $content .= apply_filters( 'feedzy_summary_output', $contentSummary, $item->get_permalink(), $feedURL );
			}
			
			$content .= '</div>';
			$content .= '</li>';
			
		} //endContinue
		
	} //endforeach

	$content .= '</ul>';
	$content .= '</div>';
	return apply_filters( 'feedzy_global_output', $content, $feedURL );
	
}//end of feedzy_rss
add_shortcode( 'feedzy-rss', 'feedzy_rss' );