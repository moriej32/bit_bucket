var pluginUrl = window.location.origin+'/bit_bucket/wp-content/plugins';
console.log()
jQuery(document).ready(function($) {
	$('.rateyo').each(function()
	{
		var rating = $(this).parent().find('.rating-val').val();
		rating = rating/20;
		$(this).rateYo({
			starWidth: "20px",
	    	rating: rating,
	    	readOnly: true
		});
	});
	var t = $('#leaderboard-table').DataTable({
		"responsive": true,
    	"columnDefs": [ {
        	"searchable": false,
        	"orderable": false,
        	"targets": 0
    	},
    	{
        	"orderable": false,
        	"targets": 1
    	},
    	{
        	"orderable": false,
        	"targets": 2
    	},
    	{
        	"orderable": false,
        	"targets": 3
        	
    	},
    	{
        	"orderable": false,
        	"targets": 4
        	
    	},
    	{
        	"orderable": false,
        	"targets": 5
        	
    	},
    	{ 
    		"orderable": false, 
    		"targets": 6 
    	},
    	{ 
    		"orderable": false, 
    		"targets": 7
    	}],
    	"order": [[ 2, 'desc' ]]
	});

	t.on( 'order.dt search.dt', function () {
    	t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    		if(i == 0)
    		{
    			cell.innerHTML = '<img width="50" height="37" src="'+pluginUrl+'/custom-leaderboard/assets/images/gold-crown.gif"/>';
    		}
    		else if(i == 1)
    		{
    			cell.innerHTML = '<img width="50" height="37" src="'+pluginUrl+'/custom-leaderboard/assets/images/silver-crown.gif"/>';
    		}
    		else if(i == 2)
    		{
    			cell.innerHTML = '<img width="50" height="37" src="'+pluginUrl+'/custom-leaderboard/assets/images/bronze-crown.gif"/>';
    		}
    		else
    		{
    			cell.innerHTML = '<span style="font-size: 1.5em">'+(i+1)+'</span>';
    		}
    	});
	}).draw();
	$("#sortby").change(function() {
    	if($(this).val() == 'views')
    	{
    		t = t.column(2).order('desc').draw();
    	}
    	if($(this).val() == 'endorsements')
    	{
    		t = t.column(3).order('desc').draw();
    	}
    	if($(this).val() == 'favourites')
    	{
    		t = t.column(4).order('desc').draw();
    	}
    	if($(this).val() == 'reviews')
    	{
    		t = t.column(5).order('desc').draw();
    	}
    	if($(this).val() == 'subscribers')
    	{
    		t = t.column(6).order('desc').draw();
    	}
    	if($(this).val() == 'downloads')
    	{
    		t = t.column(7).order('desc').draw();
    	}
	});
});
