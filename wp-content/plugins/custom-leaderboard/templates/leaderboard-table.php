<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<label for="sortby">Sort By</label>
		</div>
		<div class="col-md-4">
			<select id="sortby" class="form-control">
				<option value="views">Views</option>
				<option value="endorsements">Endorsements</option>
				<option value="favourites">Favourites</option>
				<option value="reviews">Reviews</option>
				<option value="subscribers">Subscribers</option>
				<option value="downloads">Downloads</option>
			</select>
		</div>
	</div>
	<div class=row>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table id="leaderboard-table" class="table">
				<thead>
					<tr>
						<th>Rank</th>
						<th style="text-align: center">Profile</th>
						<th>Views</th>
						<th>Endorsements</th>
						<th>Favorites</th>
						<th>Reviews</th>
						<th>Subscribers</th>
						<th>Downloads</th>
						<th>Total Sold</th>
					</tr>
				</thead>
				<tbody>
				<?php
				foreach ($rows as $row)
				{?>
					<tr>
						<td style="text-align: center"></td>
						<td>
							<div class="res-child-center-div">
								<?php echo $row['profile_pic'] ? $row['profile_pic'] : $row['avatar']; ?><br/>
								<a href="<?php echo home_url('user/'.$row['nicename']); ?>"><?php echo $row['name']; ?></a>
                            </div>
						</td>
						<td style="text-align: center"><?php echo $row['views']; ?></td>
						<td style="text-align: center"><?php echo $row['endorsements']; ?></td>
						<td style="text-align: center"><?php echo $row['favorites']; ?></td>
						<td>
							<div class="res-child-center-div">
								<input type="hidden" class="rating-val" value="<?php echo $row['reviews']; ?>"/>
								<div class="rateyo" style="margin: auto"></div><br/>
								<?php echo round($row['reviews']/20, 1); ?> out of 5<br/>
								<?php echo $row['revCount']; ?>
							</div>
						</td>
						<td style="text-align: center"><?php echo $row['subscribers']; ?></td>
						<td style="text-align: center"><?php echo $row['downloads']; ?></td>
						<td style="text-align: center"><?php echo $row['totlasold']; ?></td>
					</tr>
				<?php
				}
				?>
				</tbody>
			</table>
		</div>
	</div>	
</div>