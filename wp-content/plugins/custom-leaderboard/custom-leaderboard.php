<?php

/*
Plugin Name: Custom Leaderboard
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: ashfaq
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function custom_leaderboard_enqueue_scripts()
{
	wp_register_style('datatables-css', plugins_url('assets/vendor/datatables/datatables.min.css', __FILE__));
	wp_register_script('datatables-js', plugins_url('assets/vendor/datatables/datatables.min.js', __FILE__), array('jquery'), null, false);
	wp_register_style('rateyo-css', plugins_url('assets/vendor/rateyo/rateyo.min.css', __FILE__));
	wp_register_style('leaderboard-plugin-css', plugins_url('assets/css/style.css', __FILE__));
	wp_register_script('rateyo-js', plugins_url('assets/vendor/rateyo/rateyo.min.js', __FILE__), array('jquery'), null, false);
	wp_enqueue_style("datatables-css");
	wp_enqueue_script("datatables-js");
	wp_enqueue_style("rateyo-css");
	wp_enqueue_style('leaderboard-plugin-css');
	wp_enqueue_script("rateyo-js");
}

add_action( 'wp_enqueue_scripts', 'custom_leaderboard_enqueue_scripts' );

include 'libs/custom-leaderboard.php';

function init_custom_leaderboard()
{
	$customLeaderboard = new CustomLeaderboard();
	print $customLeaderboard->getLeaderboardData();
}

add_shortcode( 'custom-leaderboard', 'init_custom_leaderboard' );

?>