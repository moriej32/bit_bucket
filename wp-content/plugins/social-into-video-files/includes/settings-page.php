<?php 
if( ! defined("SF_VERSION") ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}
?>
<div id="ss" class="wrap">
	<div class="ss-container">
		<div class="ss-column ss-primary">
			<h2>WP Social Sharing Videos/ Files</h2>
			<form id="ss_settings" method="post" action="options.php">
			<?php settings_fields( 'wp_social_into_video_files' ); ?>
			<table class="form-table">
				<tr valign="top">
					<th scope="row">
						<label><?php _e('Social share button','wp-social-into-video-files');?></label>
					</th>
					<td>
						<input type="checkbox" id="facebook_share" name="wp_social_into_video_files[social_options][]" value="facebook" <?php checked( in_array( 'facebook', $opts['social_options'] ), true ); ?> /><label for="facebook_share"><?php echo _e('Facebook','wp-social-into-video-files')?></label>

						<input type="checkbox" id="twitter_share" name="wp_social_into_video_files[social_options][]" value="twitter" <?php checked( in_array( 'twitter', $opts['social_options'] ), true ); ?> /><label for="twitter_share"><?php echo _e('Twitter','wp-social-into-video-files')?></label>

						<input type="checkbox" id="googleplus_share" name="wp_social_into_video_files[social_options][]" value="googleplus" <?php checked( in_array( 'googleplus', $opts['social_options'] ), true ); ?> /><label for="googleplus_share"><?php echo _e('Google Plus','wp-social-into-video-files')?></label>

						<input type="checkbox" id="linkedin_share" name="wp_social_into_video_files[social_options][]" value="linkedin" <?php checked( in_array( 'linkedin', $opts['social_options'] ), true ); ?> /><label for="linkedin_share"><?php echo _e('Linkedin','wp-social-into-video-files')?></label>

						<input type="checkbox" id="dig_share" name="wp_social_into_video_files[social_options][]" value="dig" <?php checked( in_array( 'dig', $opts['social_options'] ), true ); ?> /><label for="dig_share"><?php echo _e('Dig','wp-social-into-video-files')?></label>

                        <input type="checkbox" id="snaptcha_share" name="wp_social_into_video_files[social_options][]" value="snaptcha" <?php checked( in_array( 'snaptcha', $opts['social_options'] ), true ); ?> /><label for="snaptcha_share"><?php echo _e('Snaptcha','wp-social-into-video-files')?></label>

					</td>
				</tr>
				
				<tr valign="top">
					<th scope="row">
						<label><?php _e('Automatically add sharing links?', 'wp-social-into-video-files'); ?></label>
					</th>
					<td>
						<ul>
						<?php foreach( $post_types as $post_type_id => $post_type ) { ?>
							<li>
								<label>
									<input type="checkbox" name="wp_social_into_video_files[auto_add_post_types][]" value="<?php echo esc_attr( $post_type_id ); ?>" <?php checked( in_array( $post_type_id, $opts['auto_add_post_types'] ), true ); ?>> <?php printf( __(' Auto display to %s', 'wp-social-into-video-files' ), $post_type->labels->name ); ?>
								</label>
							</li>
						<?php } ?>
						</ul>
						<small><?php _e('Automatically adds the sharing links to the end of the selected post types.', 'wp-social-into-video-files'); ?></small>
					</td>
				</tr>
			</table>
			<?php
				submit_button('Activate');
			?>
		</form>
	</div>
</div>
</div>
