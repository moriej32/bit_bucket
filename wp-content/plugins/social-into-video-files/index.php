<?php
/*
Plugin Name: WP Social Into Videos and Files
Version: 1.0
Plugin URI: www.doityourselfnation.org
Description: Adds very attractive responsive social sharing buttons of Facebook, Twitter, Linkedin, Dig, and much more to share videos or files
Author: Nhi P.
Author URI: www.doityourselfnation.org
Text Domain: wp-social-into-videos-file
License: GPL v1
*/

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

define( "SF_VERSION", "1.0" ); 
define( "SF_PLUGIN_DIR", plugin_dir_path( __FILE__ ) ); 
define( "SF_PLUGIN_URL", plugins_url( '/' , __FILE__ ) );

require_once SF_PLUGIN_DIR . 'includes/plugin.php';

if( ! is_admin() ) {
	require_once SF_PLUGIN_DIR . 'includes/class-public.php';
	new SF_Public();
} elseif( ! defined("DOING_AJAX") || ! DOING_AJAX ) {
	require SF_PLUGIN_DIR . 'includes/class-admin.php';
	new SF_Admin();
}

register_activation_hook(__FILE__, array('SF_Admin','wss_plugin_activation_action'));


add_action( 'plugins_loaded', 'wss_update_db_check_while_plugin_upgrade' );

function wss_update_db_check_while_plugin_upgrade(){
	$db_version=get_option('wss_plugin_version');
	if($db_version === FALSE)
		$db_version='1.0';	
}
