// create new scope
   // var custom_media = wp.media;

( function () {
  //  $('#top-bar').hide();
  // $('#top-nav,#top').hide();
        //     setTimeout(function () {    
        // },3000);
       // $('.breadcrumbs').attr('style','display:none');
		/* Hide all of the Massive Panel content */
		//$('.group').hide();
		//$('.section-group').hide();
		//$('.tab-group').hide();

		//$( '#mp-option-books' ).show();
	
   jQuery(document).ready(function($) {
       
         // $('head script[src*="jquery-ui.min.js"]').remove(); // for removing dynamically


	/*-----------------------------------------------------------------------------------*/
	/*	jQuery
	/*-----------------------------------------------------------------------------------*/

	
		/*-----------------------------------------------------------------------------------*/
		/*	Responsive Flip Book scripts & Massive Panel logic
		/*-----------------------------------------------------------------------------------*/
          
		// Update Warning
		$('#confirm_update').on('click', function(e) {
			$('#update_warning').slideUp(function() {
				$('#update_warning').remove();
			});
			$.post(ajaxurl, {action: 'disable_warning'});
			e.preventDefault();
		});

       // Backup
		$('#request_backup').on('click', function(e) {
			var urlAjaxExport = ajaxurl + "?action=export_flipbooks";
			location.href = urlAjaxExport;

			e.preventDefault();
		});

		// edit page
		$('.wrap1').on('click', 'table.fpages-table a.edit-page', function(e) {
			var $this = $(this);
                       
			//var start = performance.now();

			$this.parents('.fpages-table').find('.add-page.cancel').each(function() {
				$( this ).trigger('click');
			});

                        //console.log( 'Before inits: ' + Math.round( performance.now() - start ) + ' ms' );
			if( !$this.parents('tr.display').next().is(':visible') ) {
				initCSSeditor( $this.parents('tr.display').next().find('#rfbwp_page_css') );
				//console.log( '*** CSS: ' + Math.round( performance.now() - start ) + ' ms' );
				initHTMLeditor( $this.parents('tr.display').next().find('#field-rfbwp_page_html, #field-rfbwp_page_html_second') );
				//console.log( '*** HTML: ' + Math.round( performance.now() - start ) + ' ms' );
				initTocPopup( $this.parents('tr.display').next().find('.rfbwp-page-toc-popup') );
				//console.log( '*** ToC: ' + Math.round( performance.now() - start ) + ' ms' );
				initSelect( $this.parents('tr.display').next() );
				//console.log( '*** Select: ' + Math.round( performance.now() - start ) + ' ms' );
			}
                                //console.log( 'After inits: ' + Math.round( performance.now() - start ) + ' ms' );
            $('div.field-upload-audio').fadeOut();  
			$this.parents('tr.display').next().slideToggle();

			$this.parents('tr.display').next().find('a.rfbwp-page-save span.desc').text('Save Changes');
			$this.parents('tr.display').next().find('a.rfbwp-page-save').attr('href', '#Save Changes');

			e.preventDefault();
                        
		});
		
		// edit page
		$('.wrap1').on('click', 'table.fpages-table a.add-audio-page', function(e) {
			var $this = $(this);
                       
			//var start = performance.now();

			$this.parents('.fpages-table').find('.add-page.cancel').each(function() {
				$( this ).trigger('click');
			});

                        //console.log( 'Before inits: ' + Math.round( performance.now() - start ) + ' ms' );
			if( !$this.parents('tr.display').next().is(':visible') ) {
				//initCSSeditor( $this.parents('tr.display').next().find('#rfbwp_page_css') );
				//console.log( '*** CSS: ' + Math.round( performance.now() - start ) + ' ms' );
			//	initHTMLeditor( $this.parents('tr.display').next().find('#field-rfbwp_page_html, #field-rfbwp_page_html_second') );
				//console.log( '*** HTML: ' + Math.round( performance.now() - start ) + ' ms' );
			//	initTocPopup( $this.parents('tr.display').next().find('.rfbwp-page-toc-popup') );
				//console.log( '*** ToC: ' + Math.round( performance.now() - start ) + ' ms' );
				initSelect( $this.parents('tr.display').next() );
				//console.log( '*** Select: ' + Math.round( performance.now() - start ) + ' ms' );
			}
                                //console.log( 'After inits: ' + Math.round( performance.now() - start ) + ' ms' );
            $('div.field-upload-audio').fadeIn();
			$this.parents('tr.display').next().slideToggle();
			
			 $('div.field-rfbwp_fb_page_title').fadeOut();
			 $('div.stacked-fields').fadeOut();
			  $('div#field-rfbwp_page_css').fadeOut();
			   $('div#field-rfbwp_fb_page_title').fadeOut();
			    $('div#field-rfbwp_fb_page_index').fadeOut();
				 $('div#field-rfbwp_fb_page_url').fadeOut();
				 $('div#field-rfbwp_page_html').fadeOut();
				 $('div#field-rfbwp_fb_page_custom_class').fadeOut();
				  $('div#field-rfbwp_fb_page_toc_popup').fadeOut();

			$this.parents('tr.display').next().find('a.rfbwp-page-save span.desc').text('Save Changes');
			$this.parents('tr.display').next().find('a.rfbwp-page-save').attr('href', '#Save Changes');

			e.preventDefault();
                        
		});
		

                // delete page 
		$('.wrap1').on('click', 'table.fpages-table a.delete-page', function(e) {
			 uid =$('#uid').val();
                        e.preventDefault();

			var $this = $(this),
				activeBook = $this.parents('div.pages').attr('id'),
				activePage = $this.parents('tr.display').attr('id'),
				parent = $this.parents('div.pages'),
				delay_interval;

			rfbwp_add_loader();

			$this.parents('tr.display').slideUp('slow', function(){
				var data = $this.parents('tr.display').next('tr.page-set').find('input, select, textarea').serializeArray();

				if($this.parents('tr.display').next().hasClass('page-set'))
					$this.parents('tr.display').next().remove();

				$this.parents('tr.display').remove();

				var page_id = get_index(activePage);
				var id_active = get_index(activeBook);

				$.post(ajaxurl, {
					action: 'save_settings',
					data: data,
					value: 'Delete Page',
					activeID: id_active,
					pageID: page_id,
					curPageID: page_id,
					updating: 'delete_page',
                                        uid:uid
				}, function(response) {
					if(response.indexOf('mpc_data_size_exceeded') != -1) {
						display_alert('red', mpcthLocalize.messages.dialogs.maxInputVars, 15000);
					}

					rfbwp_remove_loader();

					check_pages_index(parent);
					update_page_display(parent);

					if(parent.find('table.fpages-table tr.page-set').length == 0) {
						$('li.button-sidebar.selected a').trigger('click');
						$('.books').hide();
						delay_interval = setInterval(function() {
							var id = parent.attr('id');
							id = get_index(id);

							remove_active_breadcrumbs();
							$('.books tr:nth-child('+ (parseInt(id)+1) +')').find('a.view-pages').trigger('click');
							clearInterval(delay_interval);
						}, 150);
					}
				});
			});
		});

		// move page up and down
		$('.wrap1').on('click', 'table.fpages-table tr.display a.down-page, table.fpages-table tr.display a.up-page', function(e) {
			e.preventDefault();

			var $this = $(this),
				parent = $this.parents('tr.display'),
				next = parent.next(),
				index,
				data,
				type,
				change,
				target;

			if($this.hasClass('up-page'))
				type = 'up';
			else
				type = 'down';

			if(type == 'up') {
				if(next.hasClass('page-set')) {
					index = parseInt(next.find('input#rfbwp_fb_page_index').attr('value'));
					if(parent.prev().find('select.rfbwp-page-type option:selected').text() == 'Double Page')
						index -= 2;
					else
						index -= 1;
				}
			} else if(type == 'down') {
				if(next.hasClass('page-set')) {
					index = parseInt(next.find('input#rfbwp_fb_page_index').attr('value'));
					if(next.next().next().find('select.rfbwp-page-type option:selected').text() == 'Double Page')
						index += 2;
					else
						index += 1;
				}
			}

			if(type == 'down') {
				if(next.next().hasClass('display')) {
					target = next.next().next();
				} else {
					target = 'error';
					display_alert('orange', mpcthLocalize.messages.dialogs.bottomPage, 2000);
					// Hmmm
					// We're sorry…
					// BUMMER
				}

				if(target != 'error') {
					parent.find('span.page-index').text(index);
					next.find('input#rfbwp_fb_page_index').attr('value', index);

					var i = parseInt(next.next().next().find('input#rfbwp_fb_page_index').attr('value'), 10);
					if(next.find('select.rfbwp-page-type option:selected').text() == 'Double Page')
						i -= 2;
					else
						i -= 1;
					next.next().next().find('input#rfbwp_fb_page_index').attr('value', i);

					parent.slideUp('fast', function() {
						target.after(next);
						target.after(parent);
						parent.slideDown();

						update_pages_order($(this).parents('div.pages'));
						update_page_display($(this).parents('div.pages'));
						next.find('a.rfbwp-page-save').attr('href', '#Save Changes');
						next.find('a.rfbwp-page-save').trigger('click', ['move_down']);
					});
                                        
				}

			} else if (type == 'up') {
				if(parent.prev().prev().hasClass('display')) {
					target = parent.prev().prev();
				} else {
					target = 'error';
					display_alert('orange', mpcthLocalize.messages.dialogs.topPage, 2000);
				}

				if(target != 'error') {
					parent.find('span.page-index').text(index);
					next.find('input#rfbwp_fb_page_index').attr('value', index);

					var i = parseInt(parent.prev().find('input#rfbwp_fb_page_index').attr('value'), 10);
					if(next.find('select.rfbwp-page-type option:selected').text() == 'Double Page')
						i += 2;
					else
						i += 1;
					parent.prev().find('input#rfbwp_fb_page_index').attr('value', i);

					parent.slideUp('fast', function() {
						target.before(parent);
						target.before(next);
						parent.slideDown();

						update_pages_order($(this).parents('div.pages'));
						update_page_display($(this).parents('div.pages'));
						next.find('a.rfbwp-page-save').attr('href', '#Save Changes');
						next.find('a.rfbwp-page-save').trigger('click', ['move_up']);
					});
				}
			}

		});

		/*
		*	This function is run after the page index has changed
		*	inside it's settings, it is not responsible for the
		*	Up & Down buttons.
		*/
		function index_update(parent) {
			var next = parent.next(),
				direction,
				indexDisplay,
				indexSettings = parseInt(next.find('input#rfbwp_fb_page_index').attr('value'));

			indexDisplay = parent.find('span.page-index').text();
			indexDisplay = indexDisplay.split('-');
			indexDisplay = parseInt(indexDisplay[0]);

			if(indexDisplay > indexSettings)
				direction = 'up';
			else
				direction = 'down';

			if(indexDisplay != indexSettings) {
				var target;

				parent.slideUp();
				next.remove();
				parent.parents('table.fpages-table').find('tr.page-set').each(function() {
					var $this = $(this);

					if(direction == 'down') {
						if(indexSettings >= parseInt($this.find('input#rfbwp_fb_page_index').attr('value'))) {
							target = $this;
						}
					} else {
						if(indexSettings >= parseInt($this.find('input#rfbwp_fb_page_index').attr('value'))) {
							target = $this;
						}
					}
				});

				if(direction == 'down') {
					parent.find('span.page-index').text(indexSettings);
					parent.slideUp('fast', function(){
						target.after(next);
						target.after(parent);
						parent.slideDown();
						update_pages_order($(this).parents('div.pages'));
						update_page_display($(this).parents('div.pages'));
					});
				} else {
					parent.find('span.page-index').text(indexSettings);
					parent.slideUp('fast', function(){
						target.prev().before(parent);
						target.prev().before(next);
						parent.slideDown();
						update_pages_order($(this).parents('div.pages'));
						update_page_display($(this).parents('div.pages'));
					});
				}
			} else {
				update_pages_order($(this).parents('div.pages'));
				update_page_display($(this).parents('div.pages'));
			}
		}

		// add new book
		$('.wrap1').on('click', 'a.add-book', function( e ) {
			e.preventDefault();

			$.post(ajaxurl, {
				action: 'add_new_book'
			}, function(response) {
				var id = parseInt(response);
				$('a#mp-option-settings_' + id + '-tab').addClass('add');
				$('a#mp-option-settings_' + id + '-tab').click(); // call the settings tab
				remove_active_breadcrumbs();
				//$('div.breadcrumbs span.breadcrumb-1 span.active').fadeIn();edit-button-alt
				$('div.breadcrumbs span.breadcrumb-1').fadeOut();
				$('div.breadcrumbs span.breadcrumb-0').addClass('disnst');
				$('div.breadcrumbs span.breadcrumb-2').fadeOut();
				$('div.breadcrumbs a.edit-button-alt').fadeOut();
				$('div.breadcrumbs span.breadcrumb-5').fadeOut();
				$('div.bottom-nav a.edit-buttonsnow').fadeOut();
				$('div.mp-toggle-header').fadeOut();
				$('div.bottom-nav').find('a.edit-buttonsnow').attr('style', 'display:none');
                                //console.log('res'+id);
				initSelect( $( '#mp-option-settings_' + id ).find( '[data-toggle-section="field-rfbwp_fb_name"]') );
				setup_footer('book-settings', id);
				$('div.bottom-nav').find('a.edit-button').attr('value', 'Save Settings');
			});
		});
//                $('.entry').on('click', 'a.add-book', function( e ) {
//			e.preventDefault();
//                        
//                        console.log('in entry log front')
//			$.post(ajaxurl, {
//				action: 'add_new_front_book'
//			}, function(response) {
//				var id = parseInt(response);
//				$('a#mp-option-settings_' + id + '-tab').addClass('add');
//				$('a#mp-option-settings_' + id + '-tab').click(); // call the settings tab
//				remove_active_breadcrumbs();
//				$('div.breadcrumbs span.breadcrumb-1 span.active').fadeIn();
//				$('div.breadcrumbs span.breadcrumb-1').addClass('selected');
//				$('div.breadcrumbs span.breadcrumb-2').fadeOut();
//
//				initSelect( $( '#mp-option-settings_' + id ).find( '[data-toggle-section="field-rfbwp_fb_name"]') );
//				setup_footer('book-settings', id);
//				$('div.bottom-nav').find('a.edit-button').attr('value', 'Save Settings');
//			});
//		});

		$('.wrap1').on('click', 'div.pages a.add-page', function(e) {
			e.preventDefault();

			var $this = $(this),
				current_id = $this.parents('tr.display').attr('id');
			//var start = performance.now();

			$this.parents('.fpages-table').find('tr.display:not(#' + current_id + ')').each(function() { $(this).find('.add-page.cancel').trigger( 'click' ); });
			check_pages_index($this.parents('table.fpages-table'));

			var	clone =	$this.parents('tr.display').next().clone(true);

			if( $this.hasClass( 'cancel' ) )
				$this.removeClass( 'cancel' ).find( 'span' ).text( mpcthLocalize.addNewPage );
			else
				$this.addClass( 'cancel' ).find( 'span' ).text( mpcthLocalize.cancelNewPage );

			if($this.parents('tr.display').next().next().hasClass('page-set') && $this.parents('tr.display').next().next().css('display') != 'none') {
				$this.parents('tr.display').next().next().slideUp('slow', function(){
					$(this).remove();
					check_pages_index($this.parents('table.fpages-table'));
				});
				return;
			}

			//console.log( 'After slide up & check indexes: ' + Math.round( performance.now() - start ) + ' ms' );

			if($this.parents('tr.display').next().css('display') != 'none')
				$this.parents('tr.display').next().slideUp('slow');

			var i = 0;
			$this.parents('tr.display').next().after(clone);

			$this.parents('tr.display').next().next().find('select.rfbwp-page-type option').prop('selected');

			$this.parents('tr.display').find('.page-set').slideUp('slow');
			$this.parents('tr.display').next().next().slideDown('slow');

			$this.parents('tr.display').next().next().find('a.rfbwp-page-save span.desc').text('Save Page');
			$this.parents('tr.display').next().next().find('a.rfbwp-page-save').attr('href', '#Save Page');

			////console.log( 'Before check indexes: ' + Math.round( performance.now() - start ) + ' ms' );
			// change page index
			check_pages_index( $this.parents('table.fpages-table') );
			////console.log( 'After last check indexes: ' + Math.round( performance.now() - start ) + ' ms' );
			clear_page_form( $this.parents('tr.display').next().next().attr('id'), $this.parents('.group.pages').attr( 'id') );
			////console.log( 'After clear page: ' + Math.round( performance.now() - start ) + ' ms' );
		});

		$('.wrap1').on('click', 'img.rfbwp-first-book', function(e) {
			e.preventDefault();

			$(this).parents('.books').find('a.add-book').trigger('click');
		});

		// add page, display the page add form
		$('.wrap1').on('click', 'div.pages img.rfbwp-first-page', function(e) {
			e.preventDefault();

			var $this = $(this),
				page_count,
				page_form,
				book_id;

			rfbwp_remove_loader();

			$('.page-settings').css( {'display' : 'block' } );
			$.post(ajaxurl, {
				action: 'page_form'
			}, function(response) {
				book_id = get_index($this.parents('div.pages').attr('id'));
				page_form = '<div class="rfbwp-add-page-form">';
				page_form += response;

				$.post(ajaxurl, {
					action: 'get_books_page_count',
					book_id: book_id
				}, function(response) {
					page_count = parseInt(response);
					$this.prev().find('#pset_' + page_count).css( { 'display': 'block' } );
					page_form = page_form.replace(/\[books]\[0]/g, "[books]["+book_id+"]");
					page_form = page_form.replace(/\[pages\]\[0\]/g, "[pages]["+page_count+"]");
					$this.prev().find('div#ps_' + page_count).append(page_form);
					$this.fadeOut();
					$this.prev().find('div#ps_' + page_count).find('div.rfbwp-add-page-form').find('input#rfbwp_fb_page_index').attr('value', '0');

					//update each of the fields
					$this.prev().find('div#ps_' + page_count).find('div.controls').children().each(function() {
						var $this = $(this),
							name,
							pages;

						if($this.attr('name') != undefined && $this.attr('name') != '') {
							name = $this.attr('name');
							name = name.split('[books][');
							pages = name[1].split('[pages]');
							name = name[0] + '[books][' + book_id + '][pages]' + pages[1];

							$this.attr('name', name);

							if( $this.attr('id') == 'rfbwp_fb_page_index' )
								$this.val( 0 );
						}
					});

					initCSSeditor( $this.prev().find('div#ps_' + page_count).find('#rfbwp_page_css') );
					initHTMLeditor( $this.prev().find('div#ps_' + page_count).find('#field-rfbwp_page_html, #field-rfbwp_page_html_second') );
					initSelect( $this.prev().find('div#ps_' + page_count) );

					$this.prev().find('div#ps_' + page_count).find('div.rfbwp-add-page-form').slideDown();

					rfbwp_remove_loader();
				});
			});
		});

		// save settings
		$('form#options-form').submit(function(e) { e.preventDefault(); return false; });
		$('.wrap1').on('submit', 'form#options-form', function(e) { e.preventDefault(); return false; });

		$('.wrap1').on('click', 'a.edit-button-alt', function(e) {
			$('.bottom-nav .edit-button').trigger('click');
            
			e.preventDefault();
		});

		$('.wrap1').on('keydown', 'form#options-form input', function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				event.stopPropagation();
			}
		});
       $('.wrap1').on('click', 'a.rfbwp-book-publish', function(e) {
		   //$('#discriptionfied_publish input.publish-button').trigger('click');
		    e.preventDefault();			
            var uids = $('#uid').val();
			var $this = $(this),
				data,
				id,
				descrip,
				reference,
				tags,
				backgro,
				privacyss;
				
			rfbwp_add_loader();

			/*if($this.parents('div.page-settings').attr('id') != undefined) {
				id = $this.parents('div.page-settings').attr('id');
				id = get_index(id);
			}
            updating = 'book';
			href = $this.attr('href').toString();
			href = href.split('#');
			href = href[1];

			val = href;

			if($this.parents('div.pages').attr('id') != undefined) {
				activeBook = $this.parents('div.pages').attr('id');
			} else {
				$this.parents('form').find('div.settings').each(function(){
					if($(this).css('display') == 'block')
						activeBook = $(this).attr('id');
				});
			}

			if( activeBook == undefined )
				id_active = val;
			else
				id_active = get_index(activeBook);	
			data = $('#discriptionfied_publish').find('input, select, textarea').serializeArray();*/
			descrip = $('textarea[name=rfbwp_fb_descript]').val();
			reference = $('textarea[name=rfbwp_fb_refernce]').val();
			tags = $('input[name=rfbwp_fb_tag]').val();
			backgro = $('input[name=rfbwp_fb_background]:checked').val();
			privacyss = $('input[name=rfbwp_fb_privacy]:checked').val();
			bgimgurl = $('input[name=rfbwp_fb_background_img]').val();
			dynadrevenue = $('input[name=dyn_ad_revenue]').val();
			counterval = $('#counterval').val();
			//if(dynadrevenue == 'yes'){
				var revuesrs = new Array();
				var percvals = new Array();
				$('.revusers').each(function(){
					revuesrs.push($(this).val());
				});			
				$('.percvals').each(function(){
					percvals.push($(this).val());
				});
			//}
			//alert(descrip);
			console.log(revuesrs);
			//$('#bg-content').fadeOut();
			 var mes = "Waiting For Successfully Publish Book";
			// display_alert('red',mes, 5000);
			ajax_request();
			function ajax_request() {
				$.post(ajaxurl, {action: 'publish_book_finially', descrip: descrip, reference:reference, tags:tags, backgro:backgro, privacyss:privacyss, uid:uids, bgimgurl:bgimgurl, revuesrs: revuesrs, percvals:percvals}, function(response) {
					$( window ).trigger( 'rfbwp-page-updated' );
					if(response.indexOf('mpc_data_size_exceeded') != -1) {
						display_alert('red', mpcthLocalize.messages.dialogs.maxInputVars, 15000);
					}else{
							//resst = "Successfully Publish your book";
						//display_confirmation('green',resst, 5000);
						 
					}					 
                    resst = "Successfully Publish your book";
						display_alert('green',resst, 15000);
					rfbwp_remove_loader();
					 $('div.success h2').attr('style','text-align: center;font-size: 35px;margin-bottom: 20px;color: #3c763d;');
					  $('#bg-content').attr('style','border: none;');
                    $('#bg-content').html('<div class="success"><h2>Thank you Successfully Publish your book</h2></div>');   
					//location.reload(true);
					//location.reload(true);					
					// location.reload(true);
				});
			}
						
		});   
		$('.wrap1').on('click', 'form#options-form input.save-button, form#options-form a.edit-button, a.rfbwp-page-save', function(e, move_dir) {
                        //console.log('uid')

                    e.preventDefault();
                        uid =$('#uid').val();
                        //console.log('uid'+uid)
			var $this = $(this),
				data,
				id,
				delay_interval,
				href,
				val,
				activeBook,
				id_active,
				imagsdd,
				res,
				resst,
				nbci,
				nbco,
				nfci,
				res1,
				res2,
				res3,
				headlinebook,
				fbnamebook,
				fbwidthbook,
				saonav,
				vapadding,
				fbhcbook,
				res4,
				res5,
                res6,
				res7,
				res8,
				booknamess;
			rfbwp_add_loader();

			if($this.parents('div.page-settings').attr('id') != undefined) {
				id = $this.parents('div.page-settings').attr('id');
				id = get_index(id);
			}

			href = $this.attr('href').toString();
			href = href.split('#');
			href = href[1];

			val = href;

			if($this.parents('div.pages').attr('id') != undefined) {
				activeBook = $this.parents('div.pages').attr('id');
			} else {
				$this.parents('form').find('div.settings').each(function(){
					if($(this).css('display') == 'block')
						activeBook = $(this).attr('id');
				});
			}

			if( activeBook == undefined )
				id_active = val;
			else
				id_active = get_index(activeBook);

			var updating = '',
				have_display = $this.parents('.page-set').siblings('#page-display_' + id).length != 0;

			if(move_dir != undefined) {
				updating = 'move_page';
			} else if(id == undefined) {
				updating = 'book';
			} else if(id == 0) {
				updating = 'first_page';
			} else if(id != undefined && have_display) {
				updating = 'edit_page';
			} else {
				updating = 'new_page';
			}

			if($this.hasClass('rfbwp-page-save') && id != '0') { // save, edit and add page != first
				var current_display = '#page-display_' + ( get_index( $this.parents('.page-set').attr('id') ) - 1 );

				if( $this.parents( '.fpages-table').find(current_display + ' .add-page').hasClass( 'cancel' ) )
					$this.parents( '.fpages-table').find(current_display + ' .add-page').removeClass( 'cancel' ).find( 'span' ).text( mpcthLocalize.addNewPage );

				$this.parents('tr.page-set').slideUp('down', function() {
					sort_page_index($(this).attr('id'));

					if(href == "Save Changes") {
						index_update($this.parents('tr.page-set').prev());
						update_page_display($this.parents('div.pages'));
					} else {
						update_page_display($this.parents('div.pages'));
						update_pages_order($this.parents('div.pages'));
						update_page_display($this.parents('div.pages'));
					}

					$this.trigger('rfbwp.page-save');
					data = $this.parents('tr.page-set').find('input, select, textarea').serializeArray();

					ajax_request();
				});
				$( 'html, body' ).animate( { 'scrollTop': $this.parents( 'tr.page-set' ).prev( '.display' ).offset().top - 32 }, 250 );
			} else if(move_dir == "sortable") {
				updating = "edit_pages";
				data = $('#mp-option-pages_' + val).find('input, select, textarea').serializeArray();

				ajax_request();
			} else {
				if(id == '0') { // save, edit and add first page
					$this.parents('tr.page-set').slideUp('down', function() {
						if(href == "Save Changes") {
							index_update($this.parents('tr.page-set').prev());
							update_page_display($this.parents('div.pages'));
						} else {
							update_page_display($this.parents('div.pages'));
						}

						val = 'Edit Settings';

						$this.trigger('rfbwp.first-page-save');
						data = $this.parents('tr.page-set').find('input, select, textarea').serializeArray();

						ajax_request();
					});
					$( 'html, body' ).animate( { 'scrollTop': $this.parents( 'tr.page-set' ).prev( '.display' ).offset().top - 32 }, 250 );
				} else { // save book and add new book
					val = $this.val();

					if( $this.val() != "Save Settings" )
						$('#mp-option-settings_' + id_active).find('.breadcrumb-2').fadeIn();

					if(val == '' && $this.hasClass('edit-button') || val == undefined && $this.hasClass('edit-button')) {
						val = "Edit Settings";
					}

					data = $('#mp-option-settings_' + id_active).find('input, select, textarea').serializeArray();
                                        
					imagsdd = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_hc_fco\]"]').serialize();
					 nfci = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_hc_fci\]"]').serialize();
					 nbco = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_hc_bco\]"]').serialize();
					 nbci = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_hc_bci\]"]').serialize();
					 
					 fbhcbook = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_name\]"]').serialize();
					 
					 //fbhcbook = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_name\]"]').serialize();
					 
					// fbhcbook = 'rfbwp_options%5Bbooks%5D%5B'+ id_active +'%5D%5Brfbwp_fb_hc_fco%5D=';
					// vapadding = 'rfbwp_options%5Bbooks%5D%5B'+ id_active +'%5D%5Brfbwp_fb_hc_fci%5D=';
					// saonav = 'rfbwp_options%5Bbooks%5D%5B'+ id_active +'%5D%5Brfbwp_fb_hc_bco%5D=';
					 //fbwidthbook = 'rfbwp_options%5Bbooks%5D%5B'+ id_active +'%5D%5Brfbwp_fb_hc_bci%5D=';
					 //fbnamebook = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_name\]"]').serialize();
					// headlinebook = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_heading_line\]"]').serialize();
					 
					 //booknamess = $('[name="rfbwp_options\[books\]\['+ id_active +'\]\[rfbwp_fb_name\]"]').serialize();
					 //res7 = 'rfbwp_options%5Bbooks%5D%5B'+ id_active +'%5D%5Brfbwp_fb_name%5D=';
					 //res8 = booknamess.replace( res7, "");
					 
					    res = imagsdd.split('=')[3];
						res1 = nfci.split('=')[3];
						res2 = nbco.split('=')[3];
						res3 = nbci.split('=')[3];
						headlinebook = fbhcbook.split('=')[3];
						
						 //rfbwp_remove_loader();
						 
						//res1 = nfci.split('=')[3];
						//res2 = nbco.split('=')[3];
						//res3 = nbci.split('=')[3];
						//headlinebook = fbhcbook.split('=')[3];
						//alert(res);
						/*res4 = imagsdd.replace("rfbwp_options%5Bbooks%5D%5B'+ id_active +'%5D%5Brfbwp_fb_hc_fco%5", "");
						res5 = imagsdd.replace("rfbwp_options%5Bbooks%5D%5B27%5D%5Brfbwp_fb_nav_general_v_padding%5D=", "");
						res6 = imagsdd.replace("rfbwp_options%5Bbooks%5D%5B27%5D%5Brfbwp_fb_nav_sap%5D=", "");
						res7 = imagsdd.replace("rfbwp_options%5Bbooks%5D%5B27%5D%5Brfbwp_fb_width%5D=", "");
						res8 = imagsdd.replace("rfbwp_options%5Bbooks%5D%5B27%5D%5Brfbwp_fb_name%5D=", "");
						res9 = imagsdd.replace("rfbwp_options%5Bbooks%5D%5B75%5D%5Brfbwp_fb_name%5D=", "");*/
						//display_alert('red',res, 5000);
						//rfbwp_remove_loader();
						//alert(res8);
						//$('#booknametool').append(res8); 
						//$('#curtain').attr('style', 'visibility:hidden');
				       // $('#curtain').attr('style', 'display:none');
						
						if(res == '' || res1 == '' || res2 == '' || res3 == '' || headlinebook == '')
						{	
					        rfbwp_remove_loader();
					        resst = "All Field Requird";
							display_alert('red',resst, 1500); 
							
							$('#curtain').attr('style', 'visibility:hidden');
				            $('#curtain').attr('style', 'display:none');
							$('#curtain').fadeOut();
						}
						else{
							ajax_request();
						}
				}
			}

			function ajax_request() {
				 var curtain = $('<div id="curtain"><div class="fb-spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>');
		          $('#bg-content').append( curtain );
				$.post(ajaxurl, {
                                        action: 'save_settings',
					data: data,
					activeID: id_active,
					curPageID: id,
					updating: updating,
					value: val,
					moveDir: move_dir,
                                        uid:uid
                                        
                                        
				}, function(response) {
					$( window ).trigger( 'rfbwp-page-updated' );
					if(response.indexOf('mpc_data_size_exceeded') != -1) {
						display_alert('red', mpcthLocalize.messages.dialogs.maxInputVars, 15000);
					}

					rfbwp_remove_loader();

					if(updating == 'book') {
                                                //location.reload(true);
						 $('div.breadcrumbs span.breadcrumb-5').fadeIn();
			           $('div#rfbwp_tools_toggle_content').removeClass('disnst');
	                   $('div.mp-toggle-content').fadeOut();
	                    $('div#rfbwp_tools').fadeIn();
	                   $('div#rfbwp_tools_toggle_content').fadeIn();
	                  $('a.edit-button-back').fadeIn();
	                    $('div.settings').fadeOut();
	                  $('a.edit-buttonsnow').fadeOut();
	                   $('a.edit-button').fadeOut();
	                   $('a.convert-book').trigger('click');
	 	        // var book_id = $("#rfbwp_tools").data("book-id");
		               var desc = $('<input type="hidden" name="bookid" value"'+ id_active +'">');
		              $('#iputbook').append(desc);
	
	                    $('a.edit-buttonstwo').fadeOut();
						var tdss =	$('#mp-option-pages_'+ id_active +'>#field-rfbwp_pages>.option>.fcontrols>.fpages-table tr').length;
						if(tdss > 6)
						{
							$('a.edit-buttonstwo').fadeIn();
						}
					}
                                       // location.reload(true);
				});
			}

			return false;
		});
//                $('div.controls').on('click',function(e){
//                   console.log(e.target.id) ;
//                   	var file_frame; // variable for the wp.media file_frame
//                        
//                  // console.log('here contorls -1'); 
//                   if(e.target.id == 'upload_rfbwp_fb_page_bg_image'){
//                       event.preventDefault();
//                       var mimeType = [ 'image/jpeg', 'image/png', 'image/gif' ],
//                        $target	 = $( this ).siblings( '.upload' ),
//                        $preview = $( this ).siblings( '.screenshot' );
//
//
//        // if the file_frame has already been created, just reuse it
//                        if ( file_frame ) {
//                                file_frame.open();
//                                return;
//                        } 
//
//                        file_frame = wp.media.frames.file_frame = wp.media({
//                                title: $( this ).data( 'uploader_title' ),
//                                button: {
//                                        text: $( this ).data( 'uploader_button_text' ),
//                                },
//                                multiple: false // set this to true for multiple file selection
//                        });
//
//                        file_frame.on( 'select', function() {
//                                attachment = file_frame.state().get('selection').first().toJSON();
//
//                                // do something with the file here
////                                $( '#frontend-button' ).hide();
////                                $( '#frontend-image' ).attr('src', attachment.url);
//                            var image =attachment;
//
//				if( mimeType.indexOf( image.mime ) >= 0 ) {
//					$target.val( image.url );
//					$preview.find( 'img:not(.default)' ).remove();
//					$preview.prepend( '<img src="' + image.url + '" alt="" />' );
//				}
//                        });
//
//                        file_frame.open();
//                    }
//                });
		$('.wrap1').on('rfbwp.firstTabReady', function(e, id) {
			$('.books tr:nth-child('+ (parseInt(id) + 1) +')').find('a.view-pages').trigger('click');
			//rfbwp_remove_loader();
		});

		// delete book
		$('.wrap1').on('click', 'table.books a.delete-book', function(e) {
			e.preventDefault();

			var $this = $(this),
				parent = $this.parents('table.books'),
				id = $this.attr('href').split('#');

				id = id[1];

			if ( ! confirm( mpcthLocalize.messages.dialogs.deleteBook + ' "' + $this.parents( 'td' ).find( '.book-name .pretty-name' ).text() + '"' ) ) {
				return;
			}

			rfbwp_add_loader();

			$.post(ajaxurl, {
				action: 'delete_book',
				id: id
			}, function(response) {
				if(response.indexOf('mpc_data_size_exceeded') != -1) {
					display_alert('red', mpcthLocalize.messages.dialogs.maxInputVars, 15000);
				}

				$this.parent().parent().slideUp(300, function() {
					$this.parents('tr').remove();

					parent.parents('form').find('div#mp-option-pages_' + id).remove();
					parent.parents('form').find('div#mp-option-settings_' + id).remove();
				});

				$('li.button-sidebar.selected a').trigger('click', id);

				rfbwp_remove_loader();

				/*
				$.post(ajaxurl, {
					action: 'rfbwp_refresh_books'
				}, function(response) {
					$('div.field-books div.controls').children().remove();
					$('div.field-books div.controls').append(response);

					rfbwp_check_books();
				});

				$.post(ajaxurl, {
					action: 'rfbwp_refresh_tabs_content'
				}, function(response) {
					$('form#options-form div.group.books').after(response);

					$('form#options-form div.group').each(function(){
					var $this = $(this);
					if($this.hasClass('settings') || $this.hasClass('pages'))
						$this.hide();
					});
					$('#mp-option-books').trigger('rfbwp.ajaxReady');

				});*/
			});
		});

		// update books table
		$('.wrap1').on('click', 'div#bg-content div#sidebar ul > li:first-child', function(e, id) {
			e.preventDefault();

			var respond1 = false,
				respond2 = false,
				respond3 = false;

			//$('div.field-books').css('min-height', $('div.field-books').height());
			$('div.field-books span').remove();
			$('div.field-books table.books').remove();
			$('div.field-books a.add-book').remove();

			$('div#top-nav ul#mp-section-flipbooks-tab li').each(function(){
				var $this = $(this);
				if($this.find('a').hasClass('settings') || $this.find('a').hasClass('pages'))
					$this.remove();
			});

			$('form#options-form div.group').each(function(){
				var $this = $(this);
				if($this.hasClass('settings') || $this.hasClass('pages'))
					$this.remove();
			});

			$.post(ajaxurl, {
				action: 'rfbwp_refresh_front_books'
			}, function(response) {
				$('div.field-books div.controls').children().remove();
				$('div.field-books div.controls').append(response);
				respond1 = true;
				if(id != undefined && id > -1 && respond1 && respond2 && respond3)
					$('.wrap1').trigger('rfbwp.firstTabReady', id);
			});

			$.post(ajaxurl, {
				action: 'rfbwp_refresh_tabs'
			}, function(response) {
				$('div#top-nav ul#mp-section-flipbooks-tab').append(response);
				respond2 = true;
				if(id != undefined && id > -1 && respond1 && respond2 && respond3)
					$('.wrap1').trigger('rfbwp.firstTabReady', id);
			});

			$.post(ajaxurl, {
				action: 'rfbwp_refresh_tabs_content'
			}, function(response) {
				rfbwp_remove_loader();
				$('form#options-form div.group.books').after(response);

				$('form#options-form div.group').each(function(){
				var $this = $(this);
				if($this.hasClass('settings') || $this.hasClass('pages'))
					$this.hide();
				});
				$('#mp-option-books').trigger('rfbwp.ajaxReady');
				respond3 = true;
				if(id != undefined && id > -1 && respond1 && respond2 && respond3)
					$('.wrap1').trigger('rfbwp.firstTabReady', id);
			});

		});

		$('.hide-checkbox').each(function() {
			var id = $(this).attr('id');
			var idAr = id.split('_checkbox');

			if($(this).attr('checked') == 'checked'){
				$('.'+idAr[0]+'_wrap').show();
			} else {
				$('.'+idAr[0]+'_wrap').hide();
			}
		});

		$('.hide-checkbox').change(function () {
			var id = $(this).attr('id');
			var idAr = id.split('_checkbox');

			if($(this).attr('checked') == 'checked'){
				$('.'+idAr[0]+'_wrap').slideDown();
			} else {
				$('.'+idAr[0]+'_wrap').slideUp();
			}

			if($(this).parent().find('div').hasClass('mp-related-object')){
				var related = $(this).parent().find('div.mp-related-object').text();
				$('#' + related + '_checkbox').attr('checked', false);
				$('.' + related + '_wrap').slideUp();
			}
		});

		$('textarea.displayall').each(function () {
			$(this).val($(this).val());
		});

		$('textarea.displayall-upload').each(function () {
			var taText = $(this).val();
			var urlArray = taText.split('http');
			taText = '';
			$.each(urlArray, function (i, val) {
				if(i != 0)
					taText += "http" + val;
			});
			$(this).val(taText);
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Massive Panel logic
		/*-----------------------------------------------------------------------------------*/

		/* CLICK Handler for:
		*	# button tabs
		*	# book settings button displayed in the books panel
		*	# view pages button displayed inside the books panel
		*/
		function rfbwp_page_form( $item ) {
			var item_id = get_index( $item.attr( 'id' ) ),
				$parent = $item.parents('.fpages-table');

			$parent.find( '#' + $item.attr( 'id' ) ).after( $parent.find( '#pset_' + item_id ) );
		}

		function rfbwp_menu_order( $parent ) {
			var menu_index = 1;

			$parent.find('.stacked-fields:not(:first-child):not(:last-child)').each( function() {
				var $this = $( this ),
					enabled = $this.find('.checkbox').is(':checked');

				if( enabled ) {
					$this.find( '#' + $this.data('section-id') + '_order input' ).val( menu_index );
					menu_index++;
				}
			});
		}

		function rfbwp_sort_menu( $parent ) {
			var $menu_type = $parent.find('.stacked-fields:first-child'),
				$menu_arrows = $parent.find('.stacked-fields:last-child');

			var $sort_items = $parent.find('.stacked-fields:not(:first-child):not(:last-child)');

			var numericallyOrderedDivs = $sort_items.sort(function (a, b) {
				var a_index = $(a).find( '#' + $(a).data('section-id') + '_order input' ).val(),
					b_index = $(b).find( '#' + $(b).data('section-id') + '_order input' ).val();

				return a_index > b_index;
			});

			$parent.append( $menu_type );
			$parent.append( numericallyOrderedDivs );
			$parent.append( $menu_arrows );
		}

		function rfbwp_page_numeration( $parent ) {
			var index = 0;

			$parent.find( 'tr.display' ).each( function() {
				var $this = $( this );

				if( !$this.next().hasClass('page-set') )
						rfbwp_page_form( $this );

				var $page_set = $this.next(),
					$page_index = $this.find('.page-index'),
					$page_set_index = $page_set.find('input#rfbwp_fb_page_index');

				$page_set_index.find('input#rfbwp_fb_page_index').attr('value', index);

				if( $page_set.find('select.rfbwp-page-type option:selected').text() === 'Double Page' ) {
					$page_index.text( index + ' - ' + (index + 1) );
					index += 2;
				} else {
					$page_index.text( index );
					index++;
				}

			});

		check_pages_index( $parent.parents('div.pages') );

			var book_cssid = $parent.parents('.pages').attr('id'),
				book_id = get_index( book_cssid );


			$('.wrap1').find('form#options-form a.edit-button').attr('href', '#' + book_id).attr('value', "Edit Settings");
			$('.wrap1').find('form#options-form a.edit-button').trigger('click', ["sortable"]);
		}
                
               
//$('.entry').on('click', '.button-tab a, a.book-settings, a.view-pages,.add-book', function(e) {
//			var start = performance.now();
//
//			rfbwp_add_front_loader();
//
//			var $this = $(this);
//			$this.addClass('selected');
//
//			var clicked_group = $this.attr('href');
//                        console.log('click group'+clicked_group)
//			//console.log( 'Before Setup Footer :' + Math.round( performance.now() - start ) + ' ms' );
//			if($this.hasClass('book-settings')) {
//				initSelect( $( clicked_group ).find( '[data-toggle-section="field-rfbwp_fb_name"]') );
//				setup_footer('book-settings', get_index($this.attr('href')));
//			} else if($this.hasClass('books'))
//				setup_footer('books');
//			else if($this.hasClass('add'))
//				setup_footer('add');
//			else if($this.hasClass('view-pages'))
//				setup_footer('view-pages');
//
//
//			$('.group').hide();
//
//			//console.log( 'After Hide :' + Math.round( performance.now() - start ) + ' ms' );
//			var target;
//			$(clicked_group).find('div.page-settings').each(function() {
//				var $this = $(this),
//					id = $this.attr('id');
//
//				id = get_index(id);
//				target = clicked_group + ' tr#pset_' + id + ' td';
//				$(target).append($this);
//
//				$(clicked_group + ' tr#pset_' + id).css( { display : 'none' } );
//			});
//
//			//var id_active = get_index(clicked_group);
//                        var id_active =1
//			if(id_active == undefined) {
//			//	$('#rfbwp_tools').attr('data-book-id', '').stop(true, true);
//				$('#rfbwp_import_id').val('');
//			} else {
//			//	$('#rfbwp_tools').attr('data-book-id', id_active).stop(true, true);
//				$('#rfbwp_import_id').val(id_active);
//			}
//                        console.log('id_ac'+id_active);
//
//			//console.log( 'Before Set Active :' + Math.round( performance.now() - start ) + ' ms' );
//			if(id_active != undefined) {
//				$.post(ajaxurl, {
//					action: 'set_active_book',
//					activeID: id_active
//				}, function() {
//					$(clicked_group).trigger('rfbwp.ajaxReady');
//				});
//			}
//
//			//console.log( 'After Set Active :' + Math.round( performance.now() - start ) + ' ms' );
//
//			if($this.hasClass('view-pages')) {
//				initAddFirstPage( $( clicked_group ) );
//				load_images( $( clicked_group ) );
//
//				initPagesSortable( $( clicked_group ) );
//				//console.log( 'Page Load :' + Math.round( performance.now() - start ) + ' ms' );
//			}
//
//			if( $this.hasClass( 'book-settings') ) {
//				update_covers( $( clicked_group ) );
//				//console.log( '*** Update Covers :' + Math.round( performance.now() - start ) + ' ms' );
//				load_images( $( clicked_group ) );
//				//console.log( '*** Load Images :' + Math.round( performance.now() - start ) + ' ms' );
//			}
//
//			$(clicked_group).fadeIn();
//
//			rfbwp_remove_front_loader();
//
//			//console.log( 'After Fade In:' + Math.round( performance.now() - start ) + ' ms' );
//			e.preventDefault();
//		});
		$('.wrap1').on('click', '.button-tab a, a.book-settings, a.view-pages', function(e) {
			var start = performance.now();

			rfbwp_add_loader();

			var $this = $(this);
			$this.addClass('selected');

			var clicked_group = $this.attr('href');
                        console.log('click group'+clicked_group)

			//console.log( 'Before Setup Footer :' + Math.round( performance.now() - start ) + ' ms' );
			if($this.hasClass('book-settings')) {
                            console.log('footer+1')
				initSelect( $( clicked_group ).find( '[data-toggle-section="field-rfbwp_fb_name"]') );
				setup_footer('book-settings', get_index($this.attr('href')));
			} else if($this.hasClass('books')){
                            console.log('footer+b')
				setup_footer('books');
                        }
			else if($this.hasClass('add')){
                            console.log('footer+add')
                        
                            //setup_footer('add');
                        }
                        else if($this.hasClass('view-pages')){
			console.log('footer+view')
                            setup_footer('view-pages');
                        }

			$('.group').hide();

			console.log( 'After Hide :' + Math.round( performance.now() - start ) + ' ms' );
			var target;
			$(clicked_group).find('div.page-settings').each(function() {
				var $this = $(this),
					id = $this.attr('id');

				id = get_index(id);
				target = clicked_group + ' tr#pset_' + id + ' td';
				$(target).append($this);

				$(clicked_group + ' tr#pset_' + id).css( { display : 'none' } );
			});

			var id_active = get_index(clicked_group);
			if(id_active == undefined) {
				$('#rfbwp_tools').attr('data-book-id', '').stop(true, true);
				$('#rfbwp_import_id').val('');
			} else {
				$('#rfbwp_tools').attr('data-book-id', id_active).stop(true, true);
				$('#rfbwp_import_id').val(id_active);
			}
//                         console.log('click group'+clicked_group);   
//			//console.log( 'Before Set Active :' + Math.round( performance.now() - start ) + ' ms' );
			if(id_active != undefined) {
				$.post(ajaxurl, {
					action: 'set_active_book',
					activeID: id_active
				}, function() {
					$(clicked_group).trigger('rfbwp.ajaxReady');
				});
			}
//
//			//console.log( 'After Set Active :' + Math.round( performance.now() - start ) + ' ms' );
//
			if($this.hasClass('view-pages')) {
				initAddFirstPage( $( clicked_group ) );
				load_images( $( clicked_group ) );

				initPagesSortable( $( clicked_group ) );
				//console.log( 'Page Load :' + Math.round( performance.now() - start ) + ' ms' );
			}

			if( $this.hasClass( 'book-settings') ) {
				update_covers( $( clicked_group ) );
				//console.log( '*** Update Covers :' + Math.round( performance.now() - start ) + ' ms' );
				load_images( $( clicked_group ) );
				//console.log( '*** Load Images :' + Math.round( performance.now() - start ) + ' ms' );
			}
                        
			$(clicked_group).fadeIn();

			rfbwp_remove_loader();

			//console.log( 'After Fade In:' + Math.round( performance.now() - start ) + ' ms' );
			e.preventDefault();
		});

		$('.group').on('rfbwp.ajaxReady', rfbwp_ajax_ready);

		function rfbwp_ajax_ready(e) {
			$(this).fadeIn(300);
			curtain.stop(true).fadeOut(200, function() {
				rfbwp_check_books();
			});
		}

		/* Click Section handler for the main sidebar (on the right) */
		$('.wrap1').on('click', '.button-sidebar a', function(e, id) {
			var $this = $(this);
			if(!$this.parent().hasClass('selected')) {
				$('.button-sidebar').removeClass('selected');
				$this.parent().addClass('selected');
				$('div.breadcrumbs').fadeOut();
			}

			$('.tab-group').fadeOut('slow');

			var firstTab = $this.attr('href')  + '-tab' + ' .button-tab:first a';
			$(firstTab).trigger('click');

			e.preventDefault();
		});

		/* Breadcrumbs */
		$('.wrap1').on('click', 'span.breadcrumb', function(e) {
			//var start = performance.now();

			e.preventDefault();

			rfbwp_add_loader();

			var $this = $(this),
				parentGroup = $this.parents('div.group'),
				targetBC,
				groupID;

			if($this.hasClass('selected'))
				if(!$this.hasClass('image-batch'))
					return;
				else
					$this.removeClass('image-batch');

			$('span.breadcrumb').each(function(){
				$(this).removeClass('selected');
			});

			groupID = get_index(parentGroup.attr('id'));

			$('.group').hide();

			//console.log( 'After Hide:' + Math.round( performance.now() - start ) + ' ms' );

			if($this.hasClass('breadcrumb-1')) {
				targetBC = '#mp-option-settings_' + groupID;

				update_covers( $(targetBC) );
				//console.log( '*** Update Covers :' + Math.round( performance.now() - start ) + ' ms' );
				load_images( $(targetBC) );
				//console.log( '*** Load Images :' + Math.round( performance.now() - start ) + ' ms' );

				initSelect( $( targetBC ).find( '[data-toggle-section="field-rfbwp_fb_name"]') );

				$(targetBC).fadeIn();
				//console.log( 'After Fade In:' + Math.round( performance.now() - start ) + ' ms' );
				setup_footer('book-settings');
				//console.log( 'After Settings Load :' + Math.round( performance.now() - start ) + ' ms' );
			} else if($this.hasClass('breadcrumb-2')) {
				targetBC = '#mp-option-pages_' + groupID;

				initAddFirstPage( $( targetBC ) );

				$(targetBC).find('div.page-settings').each(function() {
					var $this = $(this),
						id = $this.attr('id'),
						target = '';

					id = get_index(id);
					target = targetBC + ' tr#pset_' + id + ' td';
					$(target).append($this);

					$(targetBC + ' tr#pset_' + id).css( { display : 'none' } );
				});

				initPagesSortable( $( targetBC ) );
				load_images( $(targetBC) );

				var id = $this.parents('.group').attr('id');
				id = get_index(id);
				$('li.button-sidebar.selected a').trigger('click', id);
				$('.books').hide();

				$(targetBC).fadeIn();
				setup_footer('view-pages');
			} else if($this.hasClass('breadcrumb-0')) {
				targetBC = '#mp-option-books';
				setup_footer('books-shelf');
				$(targetBC).find('div.breadcrumbs').remove();
				$(targetBC).fadeIn();
			}

			rfbwp_remove_loader();

			//console.log( 'Total :' + Math.round( performance.now() - start ) + ' ms' );
		});
		
		
		
		// naz esert
		/* Breadcrumbs */
		$('.wrap1').on('click', 'a.edit-button', function(e) {
			//var start = performance.now();

			e.preventDefault();

			rfbwp_add_loader();

			var $this = $(this),
				parentGroup = $this.parents('div.group'),
				targetBC,
				groupID;

			if($this.hasClass('selected'))
				if(!$this.hasClass('image-batch'))
					return;
				else
					$this.removeClass('image-batch');

			$('span.breadcrumb').each(function(){
				$(this).removeClass('selected');
			});

			groupID = get_index(parentGroup.attr('id'));

			$('.group').hide();

			//console.log( 'After Hide:' + Math.round( performance.now() - start ) + ' ms' );

			 if($this.hasClass('breadcrumb-2')) {
				targetBC = '#mp-option-pages_' + groupID;

				initAddFirstPage( $( targetBC ) );

				$(targetBC).find('div.page-settings').each(function() {
					var $this = $(this),
						id = $this.attr('id'),
						target = '';

					id = get_index(id);
					target = targetBC + ' tr#pset_' + id + ' td';
					$(target).append($this);

					$(targetBC + ' tr#pset_' + id).css( { display : 'none' } );
				});

				initPagesSortable( $( targetBC ) );
				load_images( $(targetBC) );

				var id = $this.parents('.group').attr('id');
				id = get_index(id);
				$('li.button-sidebar.selected a').trigger('click', id);
				$('.books').hide();

				$(targetBC).fadeIn();
				setup_footer('view-pages');
			}  

			rfbwp_remove_loader();
              $('a.convert-book').trigger('click');
			//console.log( 'Total :' + Math.round( performance.now() - start ) + ' ms' );
		});
		// naz end 
		
		

		var css_editors = [];
		function initCSSeditor( $this ) {
			if( !$this.parent().find('.rfbwp_page_css_handler').length ) {
				var unique_id = 'rfbwp_css_' + Math.floor(Math.random()*10000), index = css_editors.length;
				$this.before('<div class="rfbwp_page_css_handler" id="' + unique_id + '" data-index="' + index + '"></div>');

				css_editors[index] = ace.edit( unique_id );
				var $css_handler = $this.hide();

				css_editors[index].getSession().setValue( $css_handler.val() );
				css_editors[index].getSession().setMode( 'ace/mode/css' );
				css_editors[index].getSession().on( 'change', function(){
					$css_handler.val( css_editors[index].getSession().getValue() );
				});
			}
		}

		$('.wrap1').on('click', '#field-rfbwp_fb_page_columns_sc .rfbwp-page-columns-sc', function() {
			var $editor = $( this ).parents('tr.page-set').find('#field-rfbwp_page_html_second'),
				$toc	= $( this ).parents('tr.page-set').find('#field-rfbwp_fb_page_toc_popup_second');

			$editor.toggleClass('active');
			$toc.toggleClass('active');
		});

		$('.wrap1').on('change', '#field-rfbwp_fb_page_type .mp-dropdown', function() {
			var $this = $( this ),
				$parent = $this.parents('tr.page-set'),
				$editor = $this.parents('tr.page-set').find('#field-rfbwp_page_html_second'),
				pageType = $parent.find('#rfbwp_fb_page_type').val();

			if( pageType == "Single Page" && $editor.hasClass('active') ) {
				$parent.find('#field-rfbwp_fb_page_columns_sc a').trigger('click');
				$parent.find('#field-rfbwp_fb_page_columns_sc').removeClass('active');
			} else if( pageType == "Double Page" && !$editor.hasClass('active') ){
				$parent.find('#field-rfbwp_fb_page_columns_sc').addClass('active');
				$parent.find('#field-rfbwp_fb_page_columns_sc a').trigger('click');
			}
		});

		function initHTMLeditor( $editors ) {
			var $parent = $editors.parents('.page-settings'),
				pageType = $parent.find('#rfbwp_fb_page_type').val(),
				$editor = $editors.parents('tr.page-set').find('#field-rfbwp_page_html_second');

			$editors.find( '.html-editor' ).each( function() {
				var	$this = $( this ),
					id = $this.parents( '.field' ).attr( 'id' ).replace( 'field-', ''),
					unique_id = 'rfbwp_html_' + Math.floor( Math.random() * 10000 );

				if( tinymce.get( $this.attr( 'id' ) ) === null ) {
					$this.attr( 'id', unique_id );

					tinyMCE.init({
						mode: 'exact',
						relative_urls: false,
						elements: unique_id,
						skin: 'mpc-flipbook',
						height: '240px',
						force_br_newlines: true,
						force_p_newlines: false,
						entity_encoding: 'raw',
						verify_html: false,
						plugins: [
							"autolink lists link image hr anchor",
							"code media nonbreaking paste textcolor colorpicker textpattern"
						],
						image_advtab: true,
						menubar : false,
						toolbar1: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | code",
					});

					$this.attr( 'data-editor', id );
				}
			});

			if( pageType == "Single Page" && $editor.hasClass('active') ) {
				$parent.find('#field-rfbwp_fb_page_columns_sc a').trigger('click');
				$parent.find('#field-rfbwp_fb_page_columns_sc').removeClass('active');
			} else if( pageType == "Double Page" && !$editor.hasClass('active') ){
				$parent.find('#field-rfbwp_fb_page_columns_sc').addClass('active');
				$parent.find('#field-rfbwp_fb_page_columns_sc a').trigger('click');
			}
		}

		$('.wrap1').on('mousedown', 'a.rfbwp-page-save', function() {
			var $this = $( this ),
				$editors = $this.parents('.page-settings').find('.html-editor');

			$editors.each( function() {
				var $editor = $( this ),
					content = tinyMCE.get( $editor.attr('id') ).getContent().replace(/<p>&nbsp;<\/p>$/, "");

				tinyMCE.get( $editor.attr('id') ).selection.select( tinyMCE.activeEditor.getBody(), true );
				tinyMCE.get( $editor.attr('id') ).selection.collapse( false );

				$editor
					.val( content );
			});

		});

		/* Init AddFirstPage */
		function initAddFirstPage( $this_el ) {
			if( $this_el.find('tr.display').length == 0 ) {
				var button = $this_el.find('img.rfbwp-first-page').clone(true);
				$this_el.find('img.rfbwp-first-page').remove();
				$this_el.find('table.fpages-table').after(button);
				$this_el.find('img.rfbwp-first-page').css( { display: 'inline-block' });
			}
		}

		/*  Init Sortables */
		function initMenuSortable( $this_el ) {
			$this_el.find('.stacked-fields:not(:first-child):not(:last-child)').append('<div class="mpc-sortable-handle"><i class="fa fa-arrows-v"></i></div>');
			rfbwp_sort_menu( $this_el );
			$this_el.sortable({
				items: '> .stacked-fields:not(:first-child):not(:last-child)',
				placeholder: 'stacked-fields ui-state-highlight',
				handle: '.mpc-sortable-handle'
			});

			$this_el.on('sortupdate', function( event, ui ) {
				rfbwp_menu_order( $this_el );
			} );

			$this_el.find('.stacked-fields:not(:first-child):not(:last-child) .checkbox' ).on( 'change', function() {
				rfbwp_menu_order( $this_el );
			} );
		}
		function initPagesSortable( $this_el ) {
			$this_el.find('.pages-table tbody').sortable({
				items: '> tr:not(.page-set)',
				placeholder: 'page-set ui-state-highlight',
				handle: '.mpc-sortable-handle'
			});

			$this_el.find('.pages-table tbody').on('sortupdate', function( event, ui ) {
				var $this = $( this );

				rfbwp_page_numeration( $this.parents('.pages-table') );
			});
		}

		/*  Select2 */
		function initSelect( $this_el ) {
			$this_el.find('.field-select').each(function(){
				var th = $( this ).find('.mp-dropdown');

//				if( !th.data('select2') )
//					th.select2({ minimumResultsForSearch: 10 });
			});
		}

		/*  Color Picker */
		function initColorPicker( $this_el ) {
			$this_el.find('.field-color').each(function(){
				var $this = $( this );

				if( !$this.find('input').hasClass('wp-color-picker') )
					$this.find('input').wpColorPicker();
			});
		}

		/* Icon Picker */
		function initIconPicker( $this_el ) {
			var $icons_modal = $( '#mpc_icon_select_grid_modal' );

			$this_el.each( function() {
				var $elem = $( this );

				$elem.find( '.mpc-icon-select' ).each( function() {
					var $icon_wrap = $( this ),
						$icon_clear = $icon_wrap.siblings( '.mpc-icon-select-clear' ),
						$icon_val = $icon_wrap.siblings( '.mpc-icon-select-value' ),
						$icon = $icon_wrap.children( 'i' );

					$icon_wrap.on( 'click', function( event ) {
						if ( $icons_modal.length ) {
							$icons_modal.dialog( 'option', 'target', $icon_wrap );
							$icons_modal.dialog( 'open' );
						}

						event.preventDefault();
					} );

					$icon_wrap.on( 'mpc.update', function( event, icon_class ) {
						$icon_val.val( icon_class );

						$icon.attr( 'class', icon_class );
						$icon_wrap.removeClass( 'mpc-icon-select-empty' );
					} );

					$icon_clear.on( 'click', function( event ) {
						$icon_val.val( '' );
						$icon.attr( 'class', '' );
						$icon_wrap.addClass( 'mpc-icon-select-empty' );

						event.preventDefault();
					} );
				} );
			} );
		}

		/* ToC popup */
		function tocGetPages( $this ) {
			var $toc_modal = $( '#mpc_toc_generator_modal' ),
				$toc_item_markup = $toc_modal.find('#toc_item_markup').clone().removeAttr('id'),
				$toc_items = $toc_modal.find('#mpc_toc_items').html(''),
				$pages = $this.parents('#field-rfbwp_pages').find('tr.page-set'),
				pages_count = 0;

			$pages.each( function() {
				var $page = $( this ),
					$page_markup = $toc_item_markup.clone(),
					page_title = $page.find('#rfbwp_fb_page_title').val(),
					page_number = $page.find('#rfbwp_fb_page_index').val();

				pages_count++;

				$page_markup.find('.page-checkbox').attr( 'name', 'page[' + pages_count + ']' );
				$page_markup.find('.page-lp').html( pages_count );
				$page_markup.find('.page-title').html( page_title );
				$page_markup.find('.page-number').html( page_number );

				$toc_items.append( $page_markup );
			});
		}

		function initTocPopup( $this_el ) {
			var $toc_modal = $( '#mpc_toc_generator_modal' );

			$this_el.each( function() {
				var $elem = $( this );

				$elem.find( '.rfbwp-page-toc-popup' ).each( function() {
					var $this = $( this );

					$this.on( 'click', function( event ) {
						var editor_id = $this.parents('.field').next().find('.html-editor').attr('id');

						if ( $toc_modal.length ) {
							tocGetPages( $this );

							$toc_modal.dialog( 'option', 'target', editor_id );
							$toc_modal.dialog( 'open' );
						}

						event.preventDefault();
					} );
				} );
			} );
		}

		/* open tab */
		setup_footer('books');

		$('.wrap1').on('click', 'div.mp-toggle-header', function(e) {
			e.preventDefault();
			//var start = performance.now();
                      //  $('.select2-chosen').attr('style','font-weight:normal');
                       // $('.wp-picker-container').attr('style','width:75%');
                        console.log('font selection1')
			var $this = $(this),
                            $section = $this.next('div.mp-toggle-content');

			if($this.hasClass('open'))
				$this.removeClass('open');
			else
				$this.addClass('open');

			//console.log( 'Before inits :' + Math.round( performance.now() - start ) + ' ms' );
			initColorPicker( $section );
			//console.log( '*** Color Picker Init:' + Math.round( performance.now() - start ) + ' ms' );
			initIconPicker( $section );
			//console.log( '*** Icon Picker Init :' + Math.round( performance.now() - start ) + ' ms' );
			rfbwpFontSelectInit( $section );
			//console.log( '*** Font Select Init :' + Math.round( performance.now() - start ) + ' ms' );
			initSelect( $section );
			//console.log( '*** Select Init :' + Math.round( performance.now() - start ) + ' ms' );
			rfbwp_hide_options( $section );
			//console.log( '*** Hide Options :' + Math.round( performance.now() - start ) + ' ms' );

			update_covers( $section );
			//console.log( '*** Update Covers :' + Math.round( performance.now() - start ) + ' ms' );
			load_images( $section );
			//console.log( '*** Load Images :' + Math.round( performance.now() - start ) + ' ms' );

			var $menu_order_toggle = ( $section.attr('data-toggle-section') == "field-rfbwp_fb_nav_menu_type" );
			if( $menu_order_toggle )
				initMenuSortable( $section );
			//console.log( '*** Init Menu Sortable :' + Math.round( performance.now() - start ) + ' ms' );

			//console.log( 'After Inits :' + Math.round( performance.now() - start ) + ' ms' );
			$this.next('div.mp-toggle-content').slideToggle('slow');
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Tools
		/*-----------------------------------------------------------------------------------*/

		var $normal_ids = $('#rfbwp_flipbook_batch_ids'),
			$normal_wrap = $('#rfbwp_flipbook_batch_images_wrap'),
			$normal_select = $('#rfbwp_flipbook_batch_select'),
			$large_ids = $('#rfbwp_flipbook_batch_ids'),
			$large_wrap = $('#rfbwp_flipbook_batch_images_wrap_large'),
			$large_select = $('#rfbwp_flipbook_batch_select_large'),
			$import_btn = $('#rfbwp_flipbook_batch_import');

		// Batch Images Import
//                _.extend(wp.media.gallery.defaults, {
//        my_custom_attr: 'default_val'
//      });
//		var custom_media = wp.media;
//		custom_media.view.Settings.Gallery = custom_media.view.Settings.Gallery.extend({
//			render: function() {
//				return this;
//			}
//		} );

		$('#rfbwp_flipbook_batch_import').on('click', function(e) {
			var normal = $('#rfbwp_flipbook_batch_ids').val(),
				large = $('#rfbwp_flipbook_batch_ids_large').val(),
				normal_num = (normal.match(/,/g)||[]).length + 1,
				large_num = (large.match(/,/g)||[]).length + 1,
				can_upload = true;
				var can_uploads = false;
		    var selectimg = $("#rfbwp_flipbook_batch_images_wrap img").length;
             
			  var curtain = $('<div id="curtain"><div class="fb-spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>');
		      $('#bg-content').append( curtain );
			  
			if(normal != '' && large != '') {
				if(normal_num != large_num) {
					display_alert('red', mpcthLocalize.messages.dialogs.normalLarge, 3000);
				} else {
					
					can_upload = true;
				}
			} else if(normal == '' && large != '') {
				can_upload = true;
			} else if(normal != '' && large == '') {
				can_upload = true;
			} else {
				display_alert('red', mpcthLocalize.messages.dialogs.noImages, 3000);
			}
           if(selectimg < 4 || selectimg == 0){
				  var message = 'At least 4 image need';
				  display_alert('red', message, 3000);
				  can_uploads = false;
			  }
			else
			 {				
				if(selectimg !=0)
			    {
			      if(selectimg%2 == 0)  {
				  can_uploads = true;
			     }else{
				  var message = 'Select even number of image';
				  display_alert('red', message, 3000);
				  can_uploads = false;
			    }
		       }
			}
			if(can_uploads == true) {
				$.post(ajaxurl, {
					action: 'batch_import',
					book_id: $('#rfbwp_tools').attr('data-book-id'),
					images_ids: $('#rfbwp_flipbook_batch_ids').val(),
					images_ids_large: $('#rfbwp_flipbook_batch_ids_large').val(),
					double_page: $('#rfbwp_flipbook_batch_double').is(':checked')
				}, function(response) {

					$('#rfbwp_flipbook_batch_ids').val('');
					$('#rfbwp_flipbook_batch_images_wrap').html('');
					$('#rfbwp_flipbook_batch_ids_large').val('');
					$('#rfbwp_flipbook_batch_images_wrap_large').html('');
					if(response == 'error-book-id') {
						display_alert('red', 'Wrong book ID.', 3000);
					} else if(response == 'error-page-id') {
						display_alert('red', 'Wrong page ID.', 3000);
					}

					
                    rfbwp_add_loader();
					
					 
					
					$.post(ajaxurl, {
						action: 'rfbwp_refresh_tabs_content_new',
						id:$('#rfbwp_tools').attr('data-book-id'),
					}, function(response) {
						

						$('form#options-form .group.settings, form#options-form .group.pages').remove();

						$('form#options-form div.group.books').after(response);

						$('form#options-form div.group').each(function(){
							var $this = $(this);
							if($this.hasClass('settings') || $this.hasClass('pages'))
								$this.hide();
						});
						
						$('div#rfbwp_tools').each(function(){
							var $this = $(this);
							if($this.hasClass('tollsbook'))
								$this.hide();
						});
						 $('div#rfbwp_tools').attr('style','display:none');
                         display_alert('green', mpcthLocalize.messages.dialogs.importFinished, 3000);
						$('#mp-option-pages_' + $('#rfbwp_tools').attr('data-book-id')).find('.breadcrumbs .breadcrumb-2').trigger('click');
                          
						rfbwp_remove_loader();
					});
				   
				});
			}

			e.preventDefault();
		});

//            $('div.controls').on('click', function(e) {
//                e.preventDefault();
//                console.log("in  uplaod 0"+e.target.id)
//
//                    if(e.target.id == 'upload_rfbwp_fb_page_bg_image'){
//                        console.log("in  uplaod 0")
//			if ( uploader ) {
//                           // console.log("in not uplaod")
//				uploader.open();
//				return;
//			}  
//
//			var mimeType = [ 'image/jpeg', 'image/png', 'image/gif' ],
//				$target	 = $( this ).siblings( '.upload' ),
//				$preview = $( this ).siblings( '.screenshot' );
//
//			var uploader = wp.media({
//					title: mpcthLocalize.messages.dialogs.selectImage,
//					button: {
//						text: mpcthLocalize.messages.dialogs.insertImage
//					},
//					multiple: false
//				});
//
//
//			uploader.on( 'select', function() {
//				var image = uploader.state().get( 'selection' ).first().toJSON();
//
//				if( mimeType.indexOf( image.mime ) >= 0 ) {
//					$target.val( image.url );
//					$preview.find( 'img:not(.default)' ).remove();
//					$preview.prepend( '<img src="' + image.url + '" alt="" />' );
//				}
//			});
//
//			uploader.open();
//                    }
//		});
                $(document).on('click','#upload_rfbwp_fb_page_bg_image', function(e) {
                            // e.preventDefault();
                   //  console.log(e.target.id)
                    do_upload_image(e.target)
		});
		
		$(document).on('click','#upload_rfbwp_fb_page_bg_audio_file', function(e) {
                            // e.preventDefault();
                   //  console.log(e.target.id)
                    do_upload_image(e.target)
		});
                
                //$('#upload_rfbwp_fb_page_bg_image_zoom').on('click', function() {
                $(document).on('click','#upload_rfbwp_fb_page_bg_image_zoom', function(e) {
                       do_upload_image(e.target)
		});
                //$('#upload_rfbwp_fb_hc_fci').on('click', function() {
                $(document).on('click','#upload_rfbwp_fb_hc_fci', function(e) {
                
                    do_upload_image(e.target)
		});
                //$('#upload_rfbwp_fb_hc_fco').on('click', function() {
                $(document).on('click','#upload_rfbwp_fb_hc_fco', function(e) {
                
                    do_upload_image(e.target)
		});
                //$('#upload_rfbwp_fb_hc_bco').on('click', function() {
                $(document).on('click','#upload_rfbwp_fb_hc_bco', function(e) {
                
                       do_upload_image(e.target)
		});
                $(document).on('click','#upload_rfbwp_fb_hc_bci', function(e) {

//                $('#upload_rfbwp_fb_hc_bci').on('click', function() {
                       do_upload_image(e.target)
		});
		  $(document).on('click','#upload_rfbwp_fb_background_img', function(e) {

//                $('#upload_rfbwp_fb_hc_bci').on('click', function() {
                       do_upload_image(e.target)
		});
                function do_upload_image($this){
                     console.log("in  uplaod 0")

			if ( uploader ) {
                            console.log("in not uplaod")
				uploader.open();
				return;
			}
                        console.log("in  uplaod")
              var mimeType = [ 'image/jpeg', 'image/png', 'image/gif','audio/mpeg','mp3' ],
			//var mimeType = [ 'image/jpeg', 'image/png', 'image/gif','audio/mpeg','mp3','aac','aax','act','aiff','amr','awb','flac','m4a', 'm4b','m4p','mpc','msv', 'ogg','wav','wma','webm','au','mp4','dct','mmf','mpc','tta','vox','wv','ra', 'rm'],
				$target	 = $( $this ).siblings( '.upload' ),
				$preview = $( $this ).siblings( '.screenshot' );

			var uploader = wp.media({
					title: mpcthLocalize.messages.dialogs.selectImage,
					button: {
						text: mpcthLocalize.messages.dialogs.insertImage
					},
					multiple: false
				});


			uploader.on( 'select', function() {
				var image = uploader.state().get( 'selection' ).first().toJSON();

				//if( mimeType.indexOf( image.mime ) >= 0 ) {
					$target.val( image.url );
					$preview.find( 'img:not(.default)' ).remove();
					$preview.prepend( '<img src="' + image.url + '" alt="" />' );
				//}
			});

			uploader.open();
                }
		$('.wrap1').on('click', '.upload_file_button', function() {
          //      $('#upload_rfbwp_fb_page_bg_image_zoom').on('click', function() {
			if ( uploader ) {
				uploader.open();
				return;
			}
			var $file_name = $( this ).siblings( '.uploaded_file_name' ),
				$target	 = $( this ).siblings( '.upload' );

			var uploader = wp.media({
					title: mpcthLocalize.messages.dialogs.selectImage,
					button: {
						text: mpcthLocalize.messages.dialogs.insertImage
					},
					multiple: false
				});


			uploader.on( 'select', function() {
				var file = uploader.state().get( 'selection' ).first().toJSON();

				$target.val( file.id );
				$file_name.html( file.filename );
			});

			uploader.open();
                        
                        
                });

		function rfbwp_gallery_selection( images ) {
			if ( images ) {
				var shortcode = new wp.shortcode({
					tag:      'gallery',
					attrs:    { ids: images },
					type:     'single'
				});

				var attachments = wp.media.gallery.attachments( shortcode );

				var selection = new wp.media.model.Selection( attachments.models, {
					props:    attachments.props.toJSON(),
					multiple: true
				});

				selection.gallery = attachments.gallery;

				selection.more().done( function() {
					selection.props.set({ query: false });
					selection.unmirror();
					selection.props.unset( 'orderby' );
				});

				return selection;
			}
			return false;
		}

		$('#rfbwp_flipbook_batch_select, #rfbwp_flipbook_batch_select_large').on('click', function(e) {
			var large_mod = '';
			if($(this).is('#rfbwp_flipbook_batch_select_large'))
				large_mod = '_large';

			var ids = $('#rfbwp_flipbook_batch_ids' + large_mod).val(),
				selection = (ids.length > 0 ) ? rfbwp_gallery_selection( ids ) : false,
				$batch = wp.media( {
					title: mpcthLocalize.messages.dialogs.selectImages,
					button: {
						text: mpcthLocalize.messages.dialogs.insertImages
					},
					frame: 'post',
					state: 'gallery-edit',
					multiple: true,
					selection: selection
				});

			$batch.open();

			$batch.on('update', function(obj) {
				var images = obj.models,
					list = [],
					markup = '';

				for(var i = 0; i < images.length; i++) {
					list[i] = images[i].id;
						markup += '<img width="50px"  data-bk="'+ list[i] +'"  height="50px" id="img'+ i +'"  src="' + images[i].attributes.sizes.thumbnail.url + '" class="rfbwp-batch-image" alt="Batch image ' + i + '"><i class="fa fa-close sd" id="sda'+ i +'"  rel="'+ i +'"></i>';
				}

				$('#rfbwp_flipbook_batch_ids' + large_mod).val(list.join(','));
				$('#rfbwp_flipbook_batch_images_wrap' + large_mod).html(markup);
			});

			e.preventDefault();
		});
        
		/*$('.sd').on('click', function(e) {
		  alert($("i.sd").data("idss"));	
        e.preventDefault();
		});*/
		
		$('#rfbwp_flipbook_batch_clear, #rfbwp_flipbook_batch_clear_large').on('click', function(e) {
			var large_mod = '';
			if($(this).is('#rfbwp_flipbook_batch_clear_large'))
				large_mod = '_large';

			$('#rfbwp_flipbook_batch_ids' + large_mod).val('');
			$('#rfbwp_flipbook_batch_images_wrap' + large_mod).fadeOut(function() {
				$('#rfbwp_flipbook_batch_images_wrap' + large_mod).html('').show();
			});

			e.preventDefault();
		});

		// Toogle Header
		$('#rfbwp_tools_toggle_title').on('click', function(e) {
			var $this = $(this),
                        $content = $('#rfbwp_tools_toggle_content');

			//if($this.is('.option-open')) {
                        if($('.option-open')[0]) {
				$this.removeClass('option-open');
				$content.slideUp();
			} else {
				$this.addClass('option-open');
				$content.slideDown();
			}

			e.preventDefault();
		});

		// Preview
		var $preview = $( '#rfbwp_page_preview' ),
			$preview_wrap = $( '#rfbwp_page_preview_wrap' );

		$preview.on( 'click', function( e ) {
			$preview.fadeOut( 250, function() {
				$preview_wrap.find( '.flipbook-container' ).removeClass( 'rfbwp-inited' );
				$preview_wrap.html( '' );
				$preview.css( 'transform', '' );
			} );
			$preview._active = undefined;

			e.preventDefault();
		} );
		$( window ).on( 'rfbwp-page-updated', function() {
			if ( $preview._active != undefined )
				$preview._active.trigger( 'click' );
		} );

		$( '#options-form' ).on( 'click', '.pages .breadcrumbs .breadcrumb:not(.selected)', function() {
			if ( $preview._active != undefined )
				$preview.trigger( 'click' );
		} );
		$( '#options-form' ).on( 'click', '.pages .page-type-td .preview-page', function( e ) {
			var $this = $( this ),
				book_id = $this.parents( 'div.pages' ).attr( 'id' ),
				page_id = $this.parents( '.display' ).attr( 'id' );

			$preview.fadeOut( 250, function() {
				$.post( ajaxurl, {
					action:  'preview_page',
					book_id: get_index( book_id ),
					page_id: get_index( page_id )
				}, function( response ) {
					if ( response == 'error-book-id' ) {
						display_alert( 'red', mpcthLocalize.messages.dialogs.bookID, 3000 );
					} else if ( response == 'error-page-id' ) {
						display_alert( 'red', mpcthLocalize.messages.dialogs.bookID, 3000 );
					} else {
						$preview_wrap.html( response );
						$preview.fadeIn( 250 );
						$preview_wrap.find( '.flipbook-container' ).addClass( 'rfbwp-inited' );

						$preview_wrap.find( 'img.lazy-load' ).each( function() {
							var $single_image = $( this );

							$single_image.attr( 'src', $single_image.attr( 'data-src' ) );
						} );

						if ( $preview.width() > 800 )
							$preview.css( 'transform', 'scale(' + ( 800 / $preview.width() ).toFixed( 2 ) + ')' );
						else if ( $preview.height() > $( window ).height() - 100 ) {
							$preview.css( 'transform', 'scale(' + ( ( $( window ).height() - 100 ) / $preview.height() ).toFixed( 2 ) + ')' );
						}
					}
				} );
			} );

			$preview._active = $this;

			e.preventDefault();
		} );

		// Import
		$('#rfbwp_import_back_url').val(location.href);

		$('#rfbwp_flipbook_import').on('click', function(e) {
			$('#rfbwp_import').click();

			e.preventDefault();
		});

		// Export
		$('#rfbwp_flipbook_export').on('click', function(e) {
			var urlAjaxExport = ajaxurl + "?action=export_flipbooks&book_id=" + $('#rfbwp_tools').attr('data-book-id');
			location.href = urlAjaxExport;

			e.preventDefault();
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Helper Functions
		/*-----------------------------------------------------------------------------------*/

		/* Loads the images after user select book */
		function load_images($this) {
			$this.find('.fb-dyn-images').each(function() {
				var $image = $(this);

				$image.attr('src', $image.attr('data-src'));
			});
		}

		/* Checks if books are correctly prepared (min 4 pages, first and last page as single) */
		function rfbwp_check_books() {
			var $books = $('#mp-option-books .books td');

			$books.each(function(index, book) {
				var message = mpcthLocalize.messages.errors.error;
				var separator = false;
				var count = 0;
				var $pages = $('#mp-option-pages_' + index + ' .pages-table tr:not(.page-set)');

				$pages.each(function(index, page) {
					if($(page).find('.page-type').html() == 'Single Page')
						count++;
					else
						count += 2;
				});

				if($pages.first().find('.page-type').html() != 'Single Page') {
					message += mpcthLocalize.messages.errors.firstPage;
					separator = true;
				}
				if($pages.last().find('.page-type').html() != 'Single Page') {
					if(separator) { 
                                            message += ', '; 
                                            separator = false; 
                                        }
					message += mpcthLocalize.messages.errors.lastPage;
					separator = true;
				}
				if(count < 4) {
					if(separator) { 
                                            message += ', ';
                                         separator = false; 
                                    }
					message += mpcthLocalize.messages.errors.minPages;
					separator = true;
				}
				if(count % 2 != 0) {
					if(separator) { 
                                            message += ', '; 
                                            separator = false; 
                                    }
					message += mpcthLocalize.messages.errors.evenPages;
				}

				if(message != mpcthLocalize.messages.errors.error )
					$(book).find('.book-error .distinction').html( message+ '.' );
			});
		}

		/* Helps setup footer display for each section */
		function setup_footer( type, bookID ) {
			remove_active_breadcrumbs();
                        console.log('type'+type+'bid'+bookID)
			$('div.bottom-nav').hide();
			$('#rfbwp_tools').hide();

			if(type == 'book-settings') { // book settings
				/* open first toggles */
				$('div.bottom-nav').find('a.edit-button').attr('value', 'Edit Settings').attr('href', '#' + bookID);

				$('div.group.settings').find('div.mp-toggle-content:first').css('display', 'block');
				$('div.group.settings').find('div.mp-toggle-header:first').addClass('open');
                
				$('div.breadcrumbs').fadeIn();
				$('div.bottom-nav a.edit-buttonsnow').fadeOut();
				$('div.breadcrumbs span.breadcrumb-1').addClass('selected');
				$('div.breadcrumbs .edit-button-alt').css('display', 'none');
				$('div.bottom-nav').fadeIn();
			} else if (type == "books" || type == "help") { // book panel
				rfbwp_check_books();
				$('div.breadcrumbs').hide();
				$('div.breadcrumbs .edit-button-alt').hide();
			} else if (type == "add") {
				$('div.breadcrumbs').fadeIn();
				$('div.bottom-nav').fadeIn();
				$('div.breadcrumbs .edit-button-alt').hide();
			} else if(type == "view-pages") {
				$('div.breadcrumbs').fadeIn();
				$('div.breadcrumbs span.breadcrumb-2').addClass('selected');
				$('div.breadcrumbs span.breadcrumb-0').fadeOut();
				$('div.breadcrumbs span.breadcrumb-1').fadeOut();
				$('div.breadcrumbs span.breadcrumb-5').fadeIn();
				$('div.breadcrumbs .edit-button-alt').hide();
				$('div.bottom-nav').fadeIn(); 
				$('div.bottom-nav a.edit-button-back').fadeOut();
				$('div.bottom-nav a.edit-button').fadeOut();
				$('div.bottom-nav a.edit-buttonsnow').fadeIn();
				$('div.bottom-nav a.edit-button-page').fadeOut();
				$('#discriptionfied_publish').fadeOut();
				
				
				//edit-button-back edit-button-page
                
				$('#rfbwp_tools').fadeOut();
			} else {
				rfbwp_refresh_bookshelf();
			}
		}

		function rfbwp_refresh_bookshelf() {
			rfbwp_add_loader();

			$.post(ajaxurl, {
				action: 'rfbwp_refresh_front_books'
			}, function(response) {
				$('div.field-books div.controls').children().remove();
				$('div.field-books div.controls').append(response);
				rfbwp_check_books();
				rfbwp_remove_loader();
                                
			});
		}

		function sort_page_index(id) {
			var appendID = get_index(id);
			appendID = appendID - 1;

			$('table.page-table tr#pset_' + appendID).after($('table.page-table tr#' + id));
		}

		function check_pages_index(parent) {
			var index = -1;
			var index_double = -1;

			parent.find('tr.page-set').each(function(){
				var $this = $(this),
					localID;

				index++;
				index_double++;
				if(get_index($this.attr('id')) != index.toString()) {
					$this.attr('id', 'pset_'+index.toString());
				}

				if($this.prev().hasClass('display') && get_index($this.prev().attr('id')) != index.toString()) {
					$this.prev().attr('id', 'page-display_'+index.toString());
				}

				// update the div.page-settings
				if($this.find('div.page-settings').attr('id') != undefined && get_index($this.find('div.page-settings').attr('id')) != index.toString()) {
					$this.find('div.page-settings').attr('id', 'ps_' + index.toString());
				}

				//update each of the fields
				$this.find('div.controls').children().each(function() {
					var $this = $(this),
						name;

					if($this.attr('name') != undefined && $this.attr('name') != '') {
						name = $this.attr('name');
						name = name.split('[pages]');

						if( $this.hasClass('html-editor') && $this.attr( 'data-editor' ) )
							name = name[0] + '[pages][' + index + '][' + $this.data('editor') + ']';
						else
							name = name[0] + '[pages][' + index + '][' + $this.attr('id') + ']';

						$this.attr('name', name);
					}
				});

				$this.find('input#rfbwp_fb_page_index').attr('value', index_double);

				if($this.find('div#field-rfbwp_fb_page_type select').val() == 'Double Page')
					index_double ++;
			});

		}

		var baseURL = $('div.mpc-logo').css('background-image');

		var browser = $.browser;
            try{
		if(browser.mozilla || browser.msie)
			baseURL = baseURL.split('url("');
		else
			baseURL = baseURL.split('url(');

		baseURL = baseURL[1];
		baseURL = baseURL.split('massive-panel');
		baseURL = baseURL[0] + 'massive-panel/';
                }catch(err){}
		function update_page_display(parent){
			parent.find('tr.page-set').each(function() {
                            
                            
				var $this = $(this),
					pageType = $this.find('select.rfbwp-page-type option:selected').text(),
					pageIndex = $this.find('input#rfbwp_fb_page_index').attr('value'),
					pageImage = $this.find('input#rfbwp_fb_page_bg_image').attr('value');
                                        pageImage = $this.find('div#rfbwp_fb_page_bg_image_image img').attr('src');
                                        
                                        console.log('page image'+pageImage+ 'index'+pageIndex)
				if(pageType == undefined || pageIndex == undefined)
					return;

				if($this.prev().hasClass('display')) {
					// we havve to update the table row

					// update page type
					$this.prev().find('span.page-type').text(pageType);

					// update page index
					if(pageType == 'Single Page')
						$this.prev().find('span.page-index').text(pageIndex);
					else
						$this.prev().find('span.page-index').html(pageIndex + ' - ' + (parseInt(pageIndex) + 1));

					// update page image
					if(pageImage != ''){
						//$this.prev().find('td.fthumb-preview img').attr('src', pageImage);
                                                    $('#pimg'+pageIndex+' img').attr('src', pageImage);
                                                    console.log('pimage'+pageIndex+ 'src'+ pageImage)
                                        }
                                        else
                                            $('#pimg'+pageIndex+' img').attr('src', baseURL + 'images/default-thumb.png');

						//$this.prev().find('td.fthumb-preview img').attr('src', baseURL + 'images/default-thumb.png');


				} else {
					// we have to add the table row
					var book_id = get_index($this.parents('div.pages').attr('id'));
					var output = '';

					output += '<tr id="page-display_'+ $this.parent().find('tr.display').length + '" class="display fpage-row-bg" ><td id="pimg'+pageIndex+'" class="fthumb-preview page-img-td">';
					if(pageImage != '')
						output += '<img src="' + pageImage + '" alt=""/>';
					else
						output += '<div class="no-cover"></div>';

					output += '</td><td class="page-type-td"><div class="fpage-type">' + pageType +'</div>';

					output += '<div class="fbtn-shadow-off">';
					output += '<a class="add-page mpc-button fmpc-button"><i class="dashicons dashicons-plus"></i> <span class="tooltip">' + mpcthLocalize.addNewPage + '</span></a>';
					output += '<a class="edit-page mpc-button fmpc-button" href="#' + book_id + '"><i class="dashicons dashicons-edit"></i> <span class="tooltip">' + mpcthLocalize.editPage + '</span></a>';
					output += '<a class="preview-page mpc-button" href="#' + book_id + '"><i class="dashicons dashicons-visibility"></i> <span class="tooltip">' + mpcthLocalize.previewPage + '</span></a>';
					output += '<a class="delete-page mpc-button fmpc-button" href="#' + book_id + '"><i class="dashicons dashicons-trash"></i> <span class="tooltip">' + mpcthLocalize.deletePage + '</span></a>';
					
					output += '<a class="add-audio-page mpc-button fmpc-button" href="#' + book_id + '"><i class="fa fa-file-audio-o"></i> <span class="tooltip">' + mpcthLocalize.addaudio + '</span></a>';
					
					output += '</div></td><td class="navigation page-btn-td">';

					output += '<a class="up-page mpc-button"><i class="dashicons dashicons-arrow-up-alt2"></i></a>';
					output += '<input type="checkbox" class="page-checkbox"/>';

					output += '<span class="desc">page</span>';
					if(pageType != 'Double Page')
						output += '<span class="page-index"><span class="index">' + pageIndex + '</span></span>';
					else
						output += '<span class="page-index"><span class="index">' + pageIndex + ' - ' + (parseInt(pageIndex) + 1) + '</span></span>';
					output += '<a class="down-page mpc-button"><i class="dashicons dashicons-arrow-down-alt2"></i></a>';
					output += '</td><td class="mpc-sortable-handle page-arrow-td"><i class="fa fa-arrows-v"></i></td></tr>';

					$this.before(output);
					$this.prev().css({ display: 'none' });
					$this.prev().slideDown();
				}
			});
		}

		function clear_page_form( id, book_id ){
			id = get_index( id );
			//var start = performance.now();
			var parentID = $('table.fpages-table tr#pset_' + id).prev().find('#rfbwp_fb_page_index').attr('value'),
				parentType = $('table.fpages-table tr#pset_' + id).prev().find('select.rfbwp-page-type option:selected').attr('value'),
				page_index = (parentType == 'Double Page') ? parseInt(parentID) + 2 : parseInt(parentID) + 1;
				book_id = get_index( book_id );
				var i = 0;
			$('table.fpages-table tr#pset_' + id).find('.field').each(function(){ i++;
				var $this = $(this);

				// clean inputs
				if($this.find('input').attr('name') != undefined && $this.find('input').attr('name') != '')
					$this.find('input').attr('value', '');

				// Page Index
				if($this.attr('id') == 'field-rfbwp_fb_page_index')
					$this.find('#rfbwp_fb_page_index').attr('value', page_index);


				if( $this.attr( 'id' ) == 'field-rfbwp_fb_page_type' ) {
					var $page_type = '<select class="mp-dropdown rfbwp-page-type" name="rfbwp_options[books][' + book_id + '][pages][' + id + '][rfbwp_fb_page_type]" id="rfbwp_fb_page_type" title=""><option selected="" value="Single Page">Single Page</option><option  value="Double Page">Double Page</option></select>';
					$this.find('.mp-dropdown').remove();
					$this.find('.controls').prepend( $page_type );

					initSelect( $this.parents('tr#pset_' + id) );
				}

				// if image then set the image to default
				if($this.attr('id') == 'field-rfbwp_fb_page_bg_image' || $this.attr('id') == 'field-rfbwp_fb_page_bg_image_zoom') {
					$this.find( 'img:not(.default)' ).remove();

					$this.find('img').show();
				}

				if( $this.attr('id') == 'field-rfbwp_page_html' || $this.attr('id') == 'field-rfbwp_page_html_second' ) {
					//console.log( 'Before HTML: ' + Math.round( performance.now() - start ) + 'ms' );
					var $editor = $this.find('.html-editor');

					$this.find('.mce-tinymce').remove();
					$editor.val( '' ).show();
					$editor.attr('id', 'rfbwp_page_html');

					initHTMLeditor( $this );
					//console.log( 'After HTML: ' + Math.round( performance.now() - start ) + 'ms' );
					initTocPopup( $this.prev() );
					//console.log( 'After ToC: ' + Math.round( performance.now() - start ) + 'ms' );
				}

				if( $this.attr('id') == 'field-rfbwp_page_css' ) {
					if( $this.find('.rfbwp_page_css_handler').length > 0 ) {

						$this.find('.rfbwp_page_css_handler').attr('id', 'delete_css_editor');
						var editor = ace.edit('delete_css_editor');
						editor.destroy();
						editor.container.remove();
					}

					$this.find('#rfbwp_page_css').val('');
					initCSSeditor( $this.find('#rfbwp_page_css') );
				}
			});
			//console.log( 'Each - count: ' + i );
			//console.log( 'After each: ' + Math.round( performance.now() - start ) + 'ms' );
		}

		function update_pages_order(parent){
			parent.find('tr.display').each(function() {
				var $this = $(this),
					index = 0,
					appendAfter,
					temp = $this.next();

				index = parseInt(temp.find('input#rfbwp_fb_page_index').attr('value'));

				if(temp.hasClass('page-set')) {
					index = parseInt(temp.find('input#rfbwp_fb_page_index').attr('value'));

					appendAfter = check_order(index, $this);

					if(appendAfter != undefined) {
						appendAfter.next().after($this);

						if(temp.hasClass('page-set'))
							$this.after(temp);
					}
				}

				check_pages_index($this.parents('table.fpages-table'));
			});
		}

		function check_order(index, row) {
			var after;

			if(index == '' || isNaN(index))
				index = 0;

			$('table.fpages-table tr.display').each(function() {
				var $this = $(this),
					next = $this.next();

				if(index > parseInt(next.find('input#rfbwp_fb_page_index').attr('value'))) {
					after = $this;
				}

				if(index == next.find('input#rfbwp_fb_page_index').attr('value') && $this.attr('id') != row.attr('id')) {
					if(next.find('select.rfbwp-page-type option:selected').attr('value') == 'Single Page')
						index++;
					else if(next.find('select.rfbwp-page-type option:selected').attr('value') === 'Double Page')
						index += 2;

					row.next().find('input#rfbwp_fb_page_index').attr('value', index);

					after = $this;
				}

			});

			return after;
		}

		/* Add Alert */
		function display_alert(color, message, delay) {
			var $wrap = $('div#bg-content'),
				left = $('#wpcontent').offset().left + 20 + 215, // WP Menu + padding + center in the panel
				window_height = $( window ).height(),
				panel_height = $( '#bg-content' ).height(),
				delayInterval;

			// add alert
			$wrap.append('<span class="rfbwp-alert ' + color + '">' + message + '</span>');
			$('span.rfbwp-alert').css( 'display', 'none');

			if( panel_height <= window_height )
				$('span.rfbwp-alert').addClass( 'not-fixed' );
			else
				$('span.rfbwp-alert').removeClass( 'not-fixed' ).css( { left: left } );

			$('span.rfbwp-alert')
				.css( 'display', 'block')
				.animate( { opacity: 1 }, 300 );

			delayInterval = setInterval(function() {
				$('span.rfbwp-alert').animate( { opacity: 0 }, 300, function() {
					$('span.rfbwp-alert').css( 'display', 'none').remove();
					clearInterval(delayInterval);
				});
			}, delay);
		}

		/* Add Alert */
		function display_confirmation(color, message, delay) {
			var wrap = $('div#bg-content'),
				delayInterval;

			// add alert
			wrap.append('<span class="rfbwp-alert ' + color + '">'+ message +'</span>');
			$('span.rfbwp-alert').hide();
			$('span.rfbwp-alert').css( { 'top': $(this).scrollTop() + 10 });

			$('span.rfbwp-alert').fadeIn('slow');

			delayInterval = setInterval(function() {
				$('span.rfbwp-alert').fadeOut('slow', function(){
					$(this).remove();
					clearInterval(delayInterval);
				});
			}, delay);
		}

		// fix after refresh
		function getSection() {
			var vars = window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
			return vars;
		}

		function get_index(id) {
			if( id !== '' ) {
				id = id.toString();
				id = id.split('_');
				id = id[1];
			}

			return id;
		}

		function remove_active_breadcrumbs() {
			$('span.breadcrumb').removeClass('selected');
		}

		var $loader = $('#curtain');
		function rfbwp_add_loader() {
			var left = $('#wpcontent').offset().left + 20 + 405,
				window_height = $( window ).height(),
				panel_height = $( '#bg-content' ).height();
                        //console.log('loader1'+ $loader);
			if( panel_height <= window_height )
				$('#curtain').find('.fb-spinner').addClass( 'not-fixed' );
			else
				$('#curtain').find('.fb-spinner').removeClass( 'not-fixed' ).css( { left: left } );

			$('#curtain').css( { display: 'block'} );
			$('#curtain').css('visibility', 'visible');
			$('#curtain').stop( true ).animate({ 'opacity': 1 }, 300);
		}
		function rfbwp_remove_loader() {
			$('#curtain').stop( true ).animate({ 'opacity': 0 }, 300, function() {
				$('#curtain').css('visibility', 'hidden');
				$('#curtain').css('display', 'none');
			});
		}
//                function rfbwp_add_front_loader() {
//                    var $loader = $('#curtain');
//			var left = $('.main_wrapper').offset().left + 20 + 405,
//				window_height = $( window ).height(),
//				panel_height = $( '#bg-content' ).height();
//
//			if( panel_height <= window_height )
//				$loader.find('.fb-spinner').addClass( 'not-fixed' );
//			else
//				$loader.find('.fb-spinner').removeClass( 'not-fixed' ).css( { left: left } );
//
//			$loader.css( { display: 'block'} );
//			$loader.css('visibility', 'visible');
//			$loader.stop( true ).animate({ 'opacity': 1 }, 300);
//		}
//		function rfbwp_remove_front_loader() {
//                        var $loader = $('#curtain');
//
//			$loader.stop( true ).animate({ 'opacity': 0 }, 300, function() {
//				$loader.css('visibility', 'hidden');
//				$loader.css('display', 'none');
//			});
//		}
		
                $('.wrap1').slideDown(500); // SOME LOADER AND AWESOME ANIMATION

		/*--------------------------- END Helper Functions -------------------------------- */

		/* ---------------------------------------------------------------- */
		/* Google Webfonts
		/* ---------------------------------------------------------------- */
		var googleFonts = '',
		googleFontsList = [];
                console.log('-- gf var'+mpcthLocalize.googleFonts)
		if(mpcthLocalize.googleFonts == false) {
                        console.log('in gfont selection 2438')

			$.getJSON('https://www.googleapis.com/webfonts/v1/webfonts?callback=?&key=' + mpcthLocalize.googleAPIKey, function(data) {
				if(data.error != undefined) {
					$('#mpcth_menu_font').after('<div class="mpcth-of-error">' + mpcthLocalize.googleAPIErrorMsg + '</div>');
				} else {
					var googleFontsData = { items: [] };

					for(var i = 0; i < data.items.length; i++) {
						googleFontsData.items[i] = {};
						googleFontsData.items[i].family = data.items[i].family;
						googleFontsData.items[i].variants = data.items[i].variants;
					}

					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						action: 'mpcth_cache_google_webfonts',
						google_webfonts: JSON.stringify(googleFontsData),
						dataType: 'json'
					});

					rfbwpAddGoogleFonts(data);
				}
			});
		} else {
			rfbwpAddGoogleFonts(JSON.parse(mpcthLocalize.googleFonts));
		}
                function getgfonts(){
                   // console.log('in gfonts0' + mpcthLocalize.googleAPIKey)
                    $.getJSON('https://www.googleapis.com/webfonts/v1/webfonts?callback=?&key=' + mpcthLocalize.googleAPIKey, function(data) {
				if(data.error != undefined) {
					$('#mpcth_menu_font').after('<div class="mpcth-of-error">' + mpcthLocalize.googleAPIErrorMsg + '</div>');
                                console.log('in data error')
	
                            } else {
                                console.log('in not data error')

					var googleFontsData = { items: [] };

					for(var i = 0; i < data.items.length; i++) {
						googleFontsData.items[i] = {};
						googleFontsData.items[i].family = data.items[i].family;
						googleFontsData.items[i].variants = data.items[i].variants;
					}

					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						action: 'mpcth_cache_google_webfonts',
						google_webfonts: JSON.stringify(googleFontsData),
						dataType: 'json'
					});

					rfbwpAddGoogleFonts(data);
				}
                });
            }
		function rfbwpAddGoogleFonts(data) {
			if(data.items != undefined) {
				var fontsCount = data.items.length;
				googleFontsList = data.items;

				googleFonts = '';
				for(var i = 0; i < fontsCount; i++) {
					var family = googleFontsList[i].family;

					googleFonts += '<option class="mpcth-option-google" data-index="' + i + '" value="' + family + '">' + family + '</option>';
				}
			}
		}

		function rfbwpFontSelectChange( e ) {
			var $this = e.data.el;

			$this.siblings('.font-handler').val( $this.val() );
		}

		function rfbwpFontSelectInit( $this_el ) {
                    //select2-chosen
                //    console.log('in font selection 2437')
                  //      console.log(mpcthLocalize.googleFonts)
 			$this_el.each( function() {
				var $elem = $( this );

				$elem.find('.rfbwp-of-input-font').each(function() {
					var $this = $(this);

					if( !$this.data( 'select2') ){
                        //                    console.log('in append font selection 2437')

						$this
							.append(googleFonts)
							.select2()
							.select2('val', $this.siblings('.font-handler').val())
							.on( 'change', { el: $this}, rfbwpFontSelectChange );
                                        }
                                    });
			});
		}

		/*----------------------------------------------------------------------------*\
			Hard cover
		\*----------------------------------------------------------------------------*/
		function update_covers( $this_el ) {
			$this_el.find( '#rfbwp_fb_hc_fco, #rfbwp_fb_hc_fci, #rfbwp_fb_hc_bco, #rfbwp_fb_hc_bci' ).each( function() {
				var $this = $( this );
				$this[ 0 ].name = $this[ 0 ].name.replace( '[pages][-1]', '' );
			} );
		}

		/*----------------------------------------------------------------------------*\
			Toggle Options
		\*----------------------------------------------------------------------------*/
		function rfbwp_toggle_options( $this ) {
			var	$parent = $this.parents('.stacked-fields:not([data-section-id="field-rfbwp_fb_nav_menu_type"])');

			if( 'field-' + $this.attr( 'id' ) == $parent.data( 'section-id' ) ) {
				if( $this.is(':checked') ) {
					$parent.find( '.field:not(#' + $parent.data( 'section-id' ) + ')' ).css( 'display', 'inline-block' );
					$parent.find( '.field:not(#' + $parent.data( 'section-id' ) + ')' ).animate({ opacity: 1 }, 300);
				} else {
					$parent.find( '.field:not(#' + $parent.data( 'section-id' ) + ')' ).animate({ opacity: 0 }, 300, function() {
						$parent.find( '.field:not(#' + $parent.data( 'section-id' ) + ')' ).css( 'display', 'none' );
					});
				}
			}
		}
                
		function rfbwp_hide_options( $sction ) {
			$sction.find( '.stacked-fields .checkbox' ).each( function() {
				var $this = $( this );
				rfbwp_toggle_options( $this );
			});
		}

		$('.wrap1').on('change', '.stacked-fields .checkbox', function() {
			var $this = $( this );

			rfbwp_toggle_options( $this );
		});

		/*----------------------------------------------------------------------------*\
			Presets
		\*----------------------------------------------------------------------------*/
		$( '.wrap1' ).on( 'change', '#rfbwp_fb_pre_style', function() {
			var $this = $( this ),
				$section = $this.parents( '.group.settings' ),
				preset = $this.val();

			if( preset !== '' && preset !== null )
				rfbwp_import_preset2( $section, preset, '#rfbwp_fb_pre_style');
		});

		$( '.wrap1' ).on( 'click', '#rfbwp_fb_down_preset', function( e ) {
			var $this = $( this ),
				$section = $this.parents( '.group.settings' );

			rfbwp_export_preset( $section );

			e.preventDefault();
		});

		function rfbwp_import_preset2( $section, preset,id) {
			var i;

			rfbwp_add_loader();

			$.getJSON( mpcthLocalize.presetsURL + preset + '.json', function( data ) {
				data = data.preset;
//console.log(data)
				//console.log('val='+$(id).val())

				display_alert( 'green', mpcthLocalize.messages.dialogs.presetLoaded, 1500 );

				rfbwp_remove_loader();
			});
		}
                function rfbwp_import_preset( $section, preset ) {
			var i;

			rfbwp_add_loader();

			$.getJSON( mpcthLocalize.presetsURL + preset + '.json', function( data ) {
				data = data.preset;
//console.log(data)
				for( i = 0; i < data.length - 1; i++ ) {
                                    console.log('field='+data[ i ].field_id+'val='+data[ i ].value+'type='+data[ i ].type)
					rfbwp_set_value( $section.find( '#' + data[ i ].field_id ), data[ i ].value, data[ i ].type );
				}

				display_alert( 'green', mpcthLocalize.messages.dialogs.presetLoaded, 1500 );

				rfbwp_remove_loader();
			});
		}

		function rfbwp_export_preset( $section ) {
			var field_id, value, type,
				preset = { "preset" : [] };

			$section.find( '.field' ).each( function() {
				var $this = $( this );

				if( $this.hasClass( 'field-checkbox' ) ) {
					value = $this.find( 'input[type="checkbox"]' ).prop( 'checked' );
					field_id = $this.find( 'input[type="checkbox"]' ).attr( 'id' );
					type = 'checkbox';
				} else if( $this.hasClass( 'field-select' ) ) {
					value = $this.find( 'select' ).val();
					field_id = $this.find( 'select' ).attr( 'id' );
					type = 'select';
				} else if( $this.hasClass( 'mp-dropdown' ) ) {
                                    console.log('in select preset')
					value = $this.find( 'select' ).val();
					field_id = $this.find( 'select' ).attr( 'id' );
					type = 'select';
				} 
                                else if( $this.hasClass( 'field-font_select' ) ) {
					value = $this.find( 'input.font-handler' ).val();
					field_id = $this.find( 'select' ).attr( 'id' );
					type = 'font-select';
				} else if( $this.hasClass( 'field-text-small' ) ) {
					value = $this.find( 'input' ).val();
					field_id = $this.find( 'input' ).attr( 'id' );
					type = 'text';
				} else if( $this.hasClass( 'field-color' ) ) {
					value = $this.find( 'input' ).val();
					field_id = $this.find( 'input' ).attr( 'id' );
					type = 'color';
				} else if( $this.hasClass( 'field-icon' ) ) {
					value = $this.find( 'input.mpc-icon-select-value' ).val();
					field_id = $this.attr( 'id' );
					type = 'icon';
				}

				preset.preset.push( {
					"field_id": field_id,
					"value" : value,
					"type" : type
				});
			});

			preset = JSON.stringify( preset );
		}

		function rfbwp_set_value( $field, value, type ) {
			if( value == undefined )
				return;

			if( type == 'text' ) {
				$field.val( value );
			} else if( type == 'color') {
				$field.val( value ).trigger( 'change' );
			} else if( type == 'select' ) {
				$field.val( value ).trigger( 'change' );
			} else if( type == 'checkbox' ) {
				$field.prop( 'checked', value );
			} else if( type == 'icon' ) {
				$field.find( 'input' ).val( value );
				$field.find( '.mpc-icon-select i' ).attr( 'class', value );
				if(  value != '' )
					$field.find( '.mpc-icon-select' ).removeClass( 'mpc-icon-select-empty' );
			} else if( type == 'font-select' ) {
				$field.siblings( 'input' ).val( value ).trigger( 'change' );
			}
		}
		
		
			function display_progress_step() {
		$step_options.addClass( 'rfbwp-disable' );
		$step_pdf.addClass( 'rfbwp-disable' );
		$step_convert.addClass( 'rfbwp-disable' );

		$step_info.addClass( 'rfbwp-active' );

		_estimation_time = Date.now();
		$progress_uploading._time.show();
	}

	function restart_form() {
		$input_name.val( '' );
		$input_pdf_overlay.text( $input_pdf_overlay.attr( 'data-text' ) )
			.parent().removeClass( 'rfbwp-active' );

		set_progress_value( $progress_uploading, '0%' );
		set_progress_value( $progress_converting, '0%' );
		set_progress_value( $progress_importing, '0%' );

		set_error_value( '' );

		$step_options.removeClass( 'rfbwp-disable' );
		$step_pdf.removeClass( 'rfbwp-disable rfbwp-active' );
		$step_convert.removeClass( 'rfbwp-disable rfbwp-active' );
		$step_info.removeClass( 'rfbwp-active' );

		$step_options.addClass( 'rfbwp-active' );
	}

	function check_server_connection() {
		$.ajax( {
			type: 'POST',
			url: 'https://wizard.mpcreation.net/api/status.json',
			dataType: 'json',
			success: function( response ) {
				_server_url = 'https://wizard.mpcreation.net/wizard/';

				$window.trigger( 'rfbwp-pdf-init' );
				$step_options.addClass( 'rfbwp-active' );
			},
			error: function() {
				set_error_value( 'server' );

				$( '.rfbwp-error-server .rfbwp-restart' ).one( 'click', function( event ) {
					event.preventDefault();

					check_server_connection();
				} );
			}
		} );
	}

	function set_error_value( type ) {
		$error_box.attr( 'data-error', type );

		if ( type != '' ) {
			$error_box.slideDown();
			clear_upload_files();
		} else {
			$error_box.slideUp();
		}
	}

	function set_progress_value( $target, value ) {
		$target._value.text( value );
		$target._line.width( value );
	}

	function get_upload_info() {
		$.ajax( {
			type: 'POST',
			url: _server_url + 'info.php',
			data: {
				pdf_path: _uploader_result.name,
				new_ID:   'true'
			},
			dataType: 'json',
			success: function( response ) {
				_pdf_pages_count = parseInt( response.pages );
				_images_path = response.dir;

				if ( _pdf_pages_count < 3 )
					set_error_value( 'pages' );
				else
					begin_convertion_process();
			},
			error: function() {
				set_error_value( 'info' );
			}
		} );
	}

	function begin_convertion_process() {
		rfbwp_add_loader();
		_convertion_loop = true;
		_convertion_loop_timer = setTimeout( check_convertion_progress, 100 );
		_convertion_faux_progress = 0;
		_convertion_last_progress = 0;

		_estimation_time = Date.now();
		$progress_converting._time.show();

		$.ajax( {
			type: 'POST',
			url: _server_url + 'convert.php',
			data: {
				pdf_path: _uploader_result.name,
				images_path: _images_path,
				zoom: _enable_zoom,
				options: _options
			},
			complete: function( response ) {
				_convertion_loop = false;
				clearTimeout( _convertion_loop_timer );

				if ( response.responseText == 'done' ) {
					set_progress_value( $progress_converting, '100%' );

					$progress_converting._time.hide();
					$progress_converting._time.text( $progress_converting._time.attr( 'data-text' ) );

					get_converted_images();
				} else {
					set_error_value( 'convertion' );
				}
			}
		} );
	}

	function check_convertion_progress() {
		$.ajax( {
			type: 'POST',
			url: _server_url + 'progress.php',
			data: {
				images_path: _images_path
			},
			complete: function( response ) {
				if ( _convertion_loop ) {
					var _convertion_progress = parseInt( response.responseText );
					var _progress = parseInt( ( _convertion_progress / _pdf_pages_count * ( 100 - _convertion_faux_progress ) ) + _convertion_faux_progress, 10 );

					if ( _convertion_progress == 0 )
						_convertion_faux_progress += .25;

					if ( _progress > 100 )
						_progress = 100;

					set_progress_value( $progress_converting, _progress + '%' );

					if ( _convertion_last_progress != _progress ) {
						var _converted = _progress / 100,
							_elapsed_time = ( Date.now() - _estimation_time ) / 1000,
							_end_time = ( _elapsed_time / _converted ) * ( 1 - _converted );

						if ( _elapsed_time > 5 )
							$progress_converting._time.text( '( ' + ( _end_time >> 0 ) + 'sec )' );

						_convertion_last_progress = _progress;
					}

					_convertion_loop_timer = setTimeout( check_convertion_progress, 250 );
				}
			}
		} );
	}

	function get_converted_images() {
		$.ajax( {
			type: 'POST',
			url: _server_url + 'images.php',
			data: {
				images_path: _images_path,
				zoom: _enable_zoom
			},
			dataType: 'json',
			success: function( response ) {
				_converted_images = response;

				if ( _converted_images.length ) {
					_estimation_time = Date.now();
					$progress_importing._time.show();

					import_single_image();
				} else {
					set_error_value( 'images' );
				}
			}
		} );
	}

	function import_single_image() {
		if ( _converted_images_index < _converted_images.length ) {
			$.post( ajaxurl, {
				action: 'rfbwp_import_single_image',
				fb_name: _flipbook_name,
				image_path: _server_url + 'images/' + _images_path + '/' + _converted_images[ _converted_images_index ]
			}, function( response ) {
				_uploaded_images.push( response );

				if ( ! _enable_zoom || ( _enable_zoom && _uploaded_images.length == _uploaded_images_zoom.length ) ) {
					var _progress = parseInt( _converted_images_index / _converted_images.length * 100, 10 ) + '%';
					set_progress_value( $progress_importing, _progress );

					_converted_images_index++;
					import_single_image();
				}
			}, 'json' ).fail( function() {
				set_error_value( 'import' );
			} );

			if ( _enable_zoom ) {
				$.post( ajaxurl, {
					action: 'rfbwp_import_single_image',
					fb_name: _flipbook_name,
					zoom: 1,
					image_path: _server_url + 'images/' + _images_path + '_zoom/' + _converted_images[ _converted_images_index ]
				}, function( response ) {
					_uploaded_images_zoom.push( response );

					if ( _uploaded_images.length == _uploaded_images_zoom.length ) {
						var _progress = parseInt( _converted_images_index / _converted_images.length * 100, 10 ) + '%';
						set_progress_value( $progress_importing, _progress );

						_converted_images_index++;
						import_single_image();
					}
				}, 'json' ).fail( function() {
					set_error_value( 'import' );
				} );
			}

			var _imported = _converted_images_index / _converted_images.length,
				_elapsed_time = ( Date.now() - _estimation_time ) / 1000,
				_end_time = ( _elapsed_time / _imported ) * ( 1 - _imported );

			if ( _elapsed_time > 5 )
				$progress_importing._time.text( '( ' + ( _end_time >> 0 ) + 'sec )' );
		} else {
			$progress_importing._time.hide();
			$progress_importing._time.text( $progress_importing._time.attr( 'data-text' ) );

			if ( _uploaded_images.length >= 3 ) {
				set_progress_value( $progress_importing, '100%' );
				generate_flipbook();
			} else {
				set_error_value( 'generate' );
			}
		}
	}

	function generate_flipbook() {
		_flipbook.rfbwp_fb_name = _flipbook_name;
		_flipbook.rfbwp_fb_book_id = _flipbook_name_book_id;
		_flipbook.rfbwp_fb_width = _uploaded_images[ 0 ].width;
		_flipbook.rfbwp_fb_height = _uploaded_images[ 0 ].height;
        _flipbook.rfbwp_fb_hc_fco = _rfbwp_fb_hc_fco;
		_flipbook.rfbwp_fb_hc_fci = _rfbwp_fb_hc_fci;
		_flipbook.rfbwp_fb_hc_bci = _rfbwp_fb_hc_bci;
		_flipbook.rfbwp_fb_hc_bco = _rfbwp_fb_hc_bco;
		_flipbook.rfbwp_fb_is_rtl = _rfbwp_fb_is_rtl;
		var _single_width = _uploaded_images[ 0 ].width,
			_page_index = 1;

		_flipbook.pages.push( {
			'rfbwp_fb_page_type': 'Single Page',
			'rfbwp_fb_page_bg_image': _uploaded_images[ 0 ].url,
			'rfbwp_fb_page_bg_image_zoom': _enable_zoom ? _uploaded_images_zoom[ 0 ].url : '',
			'rfbwp_fb_page_index': 0,
			'rfbwp_fb_page_custom_class': '',
			'rfbwp_page_css': '',
			'rfbwp_page_html': '',
			'rfbwp_fb_page_url': ''
		} );

		for ( var index = 1; index < _uploaded_images.length - 1; index++ ) {
			_flipbook.pages.push( {
				'rfbwp_fb_page_type': _single_width >= _uploaded_images[ index ].width ? 'Single Page' : 'Double Page',
				'rfbwp_fb_page_bg_image': _uploaded_images[ index ].url,
				'rfbwp_fb_page_bg_image_zoom': _enable_zoom ? _uploaded_images_zoom[ index ].url : '',
				'rfbwp_fb_page_index': _page_index,
				'rfbwp_fb_page_custom_class': '',
				'rfbwp_page_css': '',
				'rfbwp_page_html': '',
				'rfbwp_fb_page_url': ''
			} );

			_page_index += _single_width >= _uploaded_images[ index ].width ? 1 : 2;
		}

		_flipbook.pages.push( {
			'rfbwp_fb_page_type': 'Single Page',
			'rfbwp_fb_page_bg_image': _uploaded_images[ _uploaded_images.length - 1 ].url,
			'rfbwp_fb_page_bg_image_zoom': _enable_zoom ? _uploaded_images_zoom[ _uploaded_images.length - 1 ].url : '',
			'rfbwp_fb_page_index': _page_index,
			'rfbwp_fb_page_custom_class': '',
			'rfbwp_page_css': '',
			'rfbwp_page_html': '',
			'rfbwp_fb_page_url': ''
		} );

		add_new_flipbook();
	}

	function add_new_flipbook() {
		var book_id = $("#rfbwp_tools").data("book-id");
		$.post( ajaxurl, {
			action: 'rfbwp_create_new_flipbook_book',
			flipbook: _flipbook,
			activeid:book_id
		}, function() {
			clear_upload_files();
			rfbwp_add_loader();
			//$window.unbind( 'beforeunload' );
			//alert(_rfbwp_fb_is_rtl);
		 
				//location.reload();
				
				 $.post(ajaxurl, {
						action: 'rfbwp_refresh_tabs_content_new',
						id:book_id,
					}, function(response) {
						

						$('form#options-form .group.settings, form#options-form .group.pages').remove();

						$('form#options-form div.group.books').after(response);

						$('form#options-form div.group').each(function(){
							var $this = $(this);
							if($this.hasClass('settings') || $this.hasClass('pages'))
								$this.hide();
						});
						
						$('div#rfbwp_tools').each(function(){
							var $this = $(this);
							if($this.hasClass('tollsbook'))
								$this.hide();
						});
						 $('div#rfbwp_tools').attr('style','display:none');
                        // display_alert('green', mpcthLocalize.messages.dialogs.importFinished, 3000);
						$('#mp-option-pages_' + $('#rfbwp_tools').attr('data-book-id')).find('.breadcrumbs .breadcrumb-2').trigger('click');
                          //$('#mp-option-pages_'+ book_id).find('.breadcrumbs .breadcrumb-2').trigger('click');
						//rfbwp_remove_loader();
					});
		          
	    });
	}

	function clear_upload_files() {
		if ( _server_url != '' && _images_path != '' ) {
			$.ajax( {
				type: 'DELETE',
				url: _server_url + 'clear.php?images_path=' + _images_path
			} );
			if ( _enable_zoom ) {
				$.ajax( {
					type: 'DELETE',
					url: _server_url + 'clear.php?images_path=' + _images_path + '_zoom'
				} );
			}
		}

		if ( typeof _uploader_result != 'undefined' && typeof _uploader_result.deleteUrl != 'undefined' ) {
			$.ajax( {
				type: 'DELETE',
				url: _uploader_result.deleteUrl
			} );
		}
	}

	/* Form elements */
	var $window              = $( window ),
		$step_options        = $( '#rfbwp_step_options' ),
		$step_pdf            = $( '#rfbwp_step_pdf' ),
		$step_convert        = $( '#rfbwp_step_convert' ),
		$step_info           = $( '#rfbwp_step_info' ),
		$input_name          = $( '#rfbwp_name' ),
		$input_pdf           = $( '#rfbwp_pdf' ),
		$input_pdf_overlay   = $( '#rfbwp_pdf_overlay' ),
		$input_convert       = $( '#rfbwp_convert' ),
		$progress_uploading  = $( '#rfbwp_uploading' ),
		$progress_converting = $( '#rfbwp_converting' ),
		$progress_importing  = $( '#rfbwp_importing' ),
		$error_box           = $( '#rfbwp_error_box' ),
		$zoom_wrap           = $( '#rfbwp_zoom_wrap' ),
		$zoom                = $( '#rfbwp_zoom' ),
		$options             = $( '.rfbwp-validate-option' ),
		$restart_form        = $( '.rfbwp-restart' );

	/* Global variables */
	var _server_url               = '',
		_flipbook_name            = '',
		
		_rfbwp_fb_hc_fco            = '',
		_flipbook_name_bg1          = '',
		_flipbook_name_bg2          = '',
		_flipbook_name_bg3          = '',
		
		_rfbwp_fb_hc_fci            = '',
		_flipbook_name_bg4          = '',
		_flipbook_name_bg5          = '',
		_flipbook_name_bg6          = '',
		
		_rfbwp_fb_hc_bci            = '',
		_flipbook_name_bg7          = '',
		_flipbook_name_bg8          = '',
		_flipbook_name_bg9          = '',
		
		_rfbwp_fb_hc_bco           = '',
		_flipbook_name_bg10          = '',
		_flipbook_name_bg11          = '',
		_flipbook_name_bg12          = '',
		
		_rfbwp_fb_is_rtl           = '',
		_flipbook_name_bg13          = '',
		_flipbook_name_bg14          = '',
		_flipbook_name_bg15          = '',
		
		_flipbook_name_book_id    = '',
		_enable_zoom              = 1,
		_options                  = {},
		_uploader_data,
		_uploader_result,
		_pdf_pages_count          = 0,
		_images_path              = '',
		_convertion_loop          = false,
		_convertion_loop_timer,
		_estimation_time,
		_convertion_faux_progress = 0,
		_convertion_last_progress = 0,
		_converted_images         = [],
		_converted_images_index   = 0,
		_uploaded_images          = [],
		_uploaded_images_zoom     = [];

	/* Flipbook base */
	var _flipbook = {
		rfbwp_fb_name:   'Flipbook',
		rfbwp_fb_book_id:   'Flipbook',
		rfbwp_fb_width:  '400',
		rfbwp_fb_hc_fco: 'Flipbook',
		rfbwp_fb_hc_fci: 'Flipbook',
		rfbwp_fb_hc_bci:  'Flipbook',
		rfbwp_fb_hc_bco:  'Flipbook',
		rfbwp_fb_is_rtl:  'Flipbook',
		rfbwp_fb_height: '500',
		pages:           []
	};

	/* Init PDF Wizard */
	$window.on( 'rfbwp-init-pdf-wizard', function() {
		$window              = $( window );
		$step_options        = $( '#rfbwp_step_options' );
		$step_pdf            = $( '#rfbwp_step_pdf' );
		$step_convert        = $( '#rfbwp_step_convert' );
		$step_info           = $( '#rfbwp_step_info' );
		$input_name          = $( '#rfbwp_name' );
		$input_pdf           = $( '#rfbwp_pdf' );
		$input_pdf_overlay   = $( '#rfbwp_pdf_overlay' );
		$input_convert       = $( '#rfbwp_convert' );
		$progress_uploading  = $( '#rfbwp_uploading' );
		$progress_converting = $( '#rfbwp_converting' );
		$progress_importing  = $( '#rfbwp_importing' );
		$error_box           = $( '#rfbwp_error_box' );
		$zoom_wrap           = $( '#rfbwp_zoom_wrap' );
		$zoom                = $( '#rfbwp_zoom' );
		$options             = $( '.rfbwp-validate-option' );
		$restart_form        = $( '.rfbwp-restart' );

		/* Preparing info step progress bars */
		$progress_uploading._value  = $progress_uploading.find( '.rfbwp-value' );
		$progress_uploading._line   = $progress_uploading.find( '.rfbwp-line' );
		$progress_uploading._time   = $progress_uploading.find( '.rfbwp-time' );
		$progress_converting._value = $progress_converting.find( '.rfbwp-value' );
		$progress_converting._line  = $progress_converting.find( '.rfbwp-line' );
		$progress_converting._time  = $progress_converting.find( '.rfbwp-time' );
		$progress_importing._value  = $progress_importing.find( '.rfbwp-value' );
		$progress_importing._line   = $progress_importing.find( '.rfbwp-line' );
		$progress_importing._time   = $progress_importing.find( '.rfbwp-time' );

		_convertion_faux_progress = 0;
		_convertion_last_progress = 0;
		_converted_images_index   = 0;
		_uploaded_images          = [];
		_uploaded_images_zoom     = [];
		_flipbook.pages           = [];

		restart_form();

		check_server_connection();
	} );

	/* Init after server response */
	$window.on( 'rfbwp-pdf-init', function() {
		/* Flipbook name */
		_flipbook_name_book_id = $("#rfbwp_tools").data("book-id");
		 var booknamess = $('[name="rfbwp_options\[books\]\['+ _flipbook_name_book_id +'\]\[rfbwp_fb_name\]"]').serialize();
		var res7 = 'rfbwp_options%5Bbooks%5D%5B'+ _flipbook_name_book_id +'%5D%5Brfbwp_fb_name%5D=';
		var res8 = booknamess.split('=')[3];
		
		 _flipbook_name_bg1 = $('[name="rfbwp_options\[books\]\['+ _flipbook_name_book_id +'\]\[rfbwp_fb_hc_fco\]"]').serialize();
		_flipbook_name_bg2 = 'rfbwp_options%5Bbooks%5D%5B'+ _flipbook_name_book_id +'%5D%5Brfbwp_fb_hc_fco%5D=';
		_flipbook_name_bg3 = _flipbook_name_bg1.split('=')[3];
		_rfbwp_fb_hc_fco  = _flipbook_name_bg3;
		
		_flipbook_name_bg4 = $('[name="rfbwp_options\[books\]\['+ _flipbook_name_book_id +'\]\[rfbwp_fb_hc_fci\]"]').serialize();
		_flipbook_name_bg5 = 'rfbwp_options%5Bbooks%5D%5B'+ _flipbook_name_book_id +'%5D%5Brfbwp_fb_hc_fci%5D=';
		_flipbook_name_bg6 = _flipbook_name_bg4.split('=')[3];
		_rfbwp_fb_hc_fci  = _flipbook_name_bg6;
		
		_flipbook_name_bg7 = $('[name="rfbwp_options\[books\]\['+ _flipbook_name_book_id +'\]\[rfbwp_fb_hc_bci\]"]').serialize();
		_flipbook_name_bg8 = 'rfbwp_options%5Bbooks%5D%5B'+ _flipbook_name_book_id +'%5D%5Brfbwp_fb_hc_bci%5D=';
		_flipbook_name_bg9 = _flipbook_name_bg7.split('=')[3];
		_rfbwp_fb_hc_bci  = _flipbook_name_bg9;
		
		_flipbook_name_bg10 = $('[name="rfbwp_options\[books\]\['+ _flipbook_name_book_id +'\]\[rfbwp_fb_hc_bco\]"]').serialize();
		_flipbook_name_bg11 = 'rfbwp_options%5Bbooks%5D%5B'+ _flipbook_name_book_id +'%5D%5Brfbwp_fb_hc_bco%5D=';
		_flipbook_name_bg12 = _flipbook_name_bg10.split('=')[3];
		_rfbwp_fb_hc_bco  = _flipbook_name_bg12;
		
		_flipbook_name_bg13 = $('[name="rfbwp_options\[books\]\['+ _flipbook_name_book_id +'\]\[rfbwp_fb_is_rtl\]"]').serialize();
		_flipbook_name_bg14 = 'rfbwp_options%5Bbooks%5D%5B'+ _flipbook_name_book_id +'%5D%5Brfbwp_fb_is_rtl%5D=';
		_flipbook_name_bg15 = _flipbook_name_bg13.split('=')[3];
		_rfbwp_fb_is_rtl  = _flipbook_name_bg15;
		 
		
		
		$input_name.focus();
		_flipbook_name = res8.trim();
		$step_pdf.addClass( 'rfbwp-active' );
		$input_name.on( 'keyup', function() {
			/*_flipbook_name = res8.trim();
            alert(_rfbwp_fb_is_rtl);
			if ( _flipbook_name.length >= 3 ) {
				$step_pdf.addClass( 'rfbwp-active' );
			} else {
				$step_pdf.removeClass( 'rfbwp-active' );
				$step_convert.removeClass( 'rfbwp-active' );
			}*/
		} );

		/* File upload */
		$input_pdf.fileupload( {
			url: _server_url + 'upload.php',
			dataType: 'json',
			autoUpload: false,
			add: function( event, data ) {
				var _pdf_size = data.files[ 0 ].size / 1024 / 1024;

				$input_pdf_overlay.text( data.files[ 0 ].name )
					.parent().addClass( 'rfbwp-active' );

				if ( _pdf_size <= 100 ) {
					$step_convert.addClass( 'rfbwp-active' );

					set_error_value( '' );

					_uploader_data = data;
				} else {
					$step_convert.removeClass( 'rfbwp-active' );

					set_error_value( 'size' );
				}
			},
			start: display_progress_step,
			progress: function( event, data ) {
				var _loaded = data.loaded / data.total,
					_progress = parseInt( _loaded * 100, 10 ) + '%';

				set_progress_value( $progress_uploading, _progress );

				var _elapsed_time = ( Date.now() - _estimation_time ) / 1000,
					_end_time = ( _elapsed_time / _loaded ) * ( 1 - _loaded );

				if ( _elapsed_time > 5 )
					$progress_uploading._time.text( '( ' + ( _end_time >> 0 ) + 'sec )' );
			},
			done: function( event, data ) {
				set_progress_value( $progress_uploading, '100%' );
				_uploader_result = data.result.files[ 0 ];

				$progress_uploading._time.hide();
				$progress_uploading._time.text( $progress_uploading._time.attr( 'data-text' ) );

				if ( typeof _uploader_result.error != 'undefined' )
					set_error_value( 'upload' );
				else
					get_upload_info();
			},
			fail: function() {
				set_error_value( 'upload' );
			}
		} );

		/* Zoom options */
		$zoom.on( 'change', function() {
			if ( $zoom.is( ':checked' ) ) {
				_enable_zoom = 1;
				$zoom_wrap.find( 'input[type=text]' ).prop( 'disabled', false );
			} else {
				_enable_zoom = 0;
				$zoom_wrap.find( 'input[type=text]' ).prop( 'disabled', true );
			}
		} );

		/* Validate options */
		$options.on( 'blur', function() {
			var $this = $( this ),
				_base = $this.val(),
				_default = parseInt( $this.attr( 'data-default' ) ),
				_min = parseInt( $this.attr( 'data-min' ) ),
				_max = parseInt( $this.attr( 'data-max' ) ),
				_value = isNaN( parseInt( _base ) ) ? _default : Math.max( Math.min( Math.abs( parseInt( _base ) ), _max ), _min );

			$this.val( _value );

			if ( _base != _value ) {
				$this.addClass( 'rfbwp-blink' );
				setTimeout( function() {
					$this.removeClass( 'rfbwp-blink' );
				}, 1000 );
			}

			$options.each( function() {
				var $this = $( this );

				_options[ $this[ 0 ].name.replace( 'rfbwp_', '' ) ] = $this.val();
			} );
		} );
		$options.each( function() {
			var $this = $( this );

			_options[ $this[ 0 ].name.replace( 'rfbwp_', '' ) ] = $this.val();
		} );

		/* Begin convertion */
		$input_convert.on( 'click', function( event ) {
			event.preventDefault();
			
			$window.bind( 'beforeunload', function(e) {				
				return mpcthPDFLocalize.refreshBlockMessage;
				e.preventDefault();					
			});
			
			_uploader_data.submit();
		} );

		/* Restart form */
		$restart_form.on( 'click', function( event ) {
			event.preventDefault();

			restart_form();
		} );
	} );

	/* PDF Wizard toggle */
	$( '.wrap1' ).on( 'click', 'a.convert-book', function( e ) {
		e.preventDefault();

		$( '#field-rfbwp_books' ).hide();

		$( '#pdfbooks' ).addClass( 'rfbwp-mode-pdf-wizard' );
		$( window ).trigger( 'rfbwp-init-pdf-wizard' );

		$( '#pdfbooks' ).fadeIn( '250' );
	});
	$( '.wrap' ).on( 'click', 'a.cancel-convert-book', function( e ) {
		e.preventDefault();

		$( '#field-rfbwp_books' ).hide();

		$( '.wrap' ).removeClass( 'rfbwp-mode-pdf-wizard' );

		$( '#field-rfbwp_books' ).fadeIn( '250' );
	});
	$( window ).on( 'rfbwp-pdf-convert-end', function() {
		$( '.wrap a.cancel-convert-book' ).click();

		setTimeout( function() {
			//rfbwp_refresh_bookshelf();
		}, 250 );
	} );
	/* END - PDF Wizard toggle */
	
         /* $( '.wrap1' ).on( 'click', 'a#rfbwp_flipbook_batch_import_doc', function( e ) {
			 rfbwp_add_loader();
			      var file_data = $('#rfbwp_doc').prop('files')[0];   
                  var form_data = new FormData();                  
                   form_data.append('file', file_data);
			   //e.preventDefault();
			 console.log("submit event");
				$.post(ajaxurl, {
					action: 'rfbwp_uploaddocfile',
					book_id: $('#rfbwp_tools').attr('data-book-id'),
					filena:$('#rfbwp_doc').val(),
					cache: false,
                       contentType: false,
                        processData: false,
                        data: form_data,
				}, function(response) {

					$('#rfbwp_flipbook_batch_ids').val('');
					$('#rfbwp_flipbook_batch_images_wrap').html('');
					$('#rfbwp_flipbook_batch_ids_large').val('');
					$('#rfbwp_flipbook_batch_images_wrap_large').html('');
					if(response == 'error-book-id') {
						display_alert('red', 'Wrong book ID.', 3000);
					} else if(response == 'error-page-id') {
						display_alert('red', 'Wrong page ID.', 3000);
					}

					
                    rfbwp_add_loader();
					
					$.post(ajaxurl, {
						action: 'rfbwp_refresh_tabs_content'
					}, function(response) {
						

						$('form#options-form .group.settings, form#options-form .group.pages').remove();

						$('form#options-form div.group.books').after(response);

						$('form#options-form div.group').each(function(){
							var $this = $(this);
							if($this.hasClass('settings') || $this.hasClass('pages'))
								$this.hide();
						});
						
						$('div#rfbwp_tools').each(function(){
							var $this = $(this);
							if($this.hasClass('tollsbook'))
								$this.hide();
						});
						 $('div#rfbwp_tools').attr('style','display:none');
                         display_alert('green', mpcthLocalize.messages.dialogs.importFinished, 3000);
						$('#mp-option-pages_' + $('#rfbwp_tools').attr('data-book-id')).find('.breadcrumbs .breadcrumb-2').trigger('click');
                          
						rfbwp_remove_loader();
					});
				});
			

			e.preventDefault();
		});*/
		/*$('#rfbwp_flipbook_batch_import_doc').on('click', function() {
             var file_data = $('#rfbwp_doc').prop('files')[0];   
          var form_data = new FormData();                  
            form_data.append('file', file_data);
         alert(form_data);                             
          $.ajax({
                //url: 'uploaddoc.php', // point to server-side PHP script 
               // dataType: 'text',  // what to expect back from the PHP script, if anything
			    action: 'rfbwp_uploaddocfile',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(php_script_response){
                    alert(php_script_response); // display response from the PHP script, if any
                }
                });
              });*/
	       $( '.wrap1' ).on( 'click', 'a#rfbwp_flipbook_batch_import_doc', function( e ) {
			   var bla = $('#bookdoc').find('input').serializeArray();
			 var book_id = $("#rfbwp_tools").data("book-id");
			 rfbwp_add_loader();
		$.post( ajaxurl, {
			action: 'rfbwp_uploaddocfile',
			data: bla,
			activeid:book_id
		}, function(response) {
			//clear_upload_files();
			
			//$window.unbind( 'beforeunload' );
			alert(response);
		 
				//location.reload();
				 
				 $.post(ajaxurl, {
						action: 'rfbwp_refresh_tabs_content'
					}, function(response) {
						

						$('form#options-form .group.settings, form#options-form .group.pages').remove();

						$('form#options-form div.group.books').after(response);

						$('form#options-form div.group').each(function(){
							var $this = $(this);
							if($this.hasClass('settings') || $this.hasClass('pages'))
								$this.hide();
						});
						
						$('div#rfbwp_tools').each(function(){
							var $this = $(this);
							if($this.hasClass('tollsbook'))
								$this.hide();
						});
						 $('div#rfbwp_tools').attr('style','display:none');
                        // display_alert('green', mpcthLocalize.messages.dialogs.importFinished, 3000);
						$('#mp-option-pages_' + $('#rfbwp_tools').attr('data-book-id')).find('.breadcrumbs .breadcrumb-2').trigger('click');
                          //$('#mp-option-pages_'+ book_id).find('.breadcrumbs .breadcrumb-2').trigger('click');
						//rfbwp_remove_loader();
					});
			 
		 
	    });    
		   });
		   
		 

    $('#rfbwp_flipbook_doc_import').on('click', function() {
				  $('#button').click();
});
                       
					   $("#formss").on('submit',(function(e) {
						    var bla = $("#rfbwp_tools").data("book-id");
						   rfbwp_add_loader();
  e.preventDefault();
  $.ajax({
      //   url: "http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/responsive-flipbook-pdf/ajaxupload.php?id=" + bla,
	  url: mpcthLocalize.docurlfull + "ajaxupload.php?id=" + bla,
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    //$("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    if(data=='invalid file')
    {
		rfbwp_remove_loader();
     // invalid file format.
	  $("#formss")[0].reset(); 
     $("#err").html("Invalid File !").fadeIn();
    }
    else
    {
     // view uploaded file.
     $("#preview").html(data).fadeIn();
    
	  
			 var book_id = $("#rfbwp_tools").data("book-id");
			// var filenamess = $('#uploadImage').val();
			 //alert(filenamess);
		$.post( ajaxurl, {
			action: 'rfbwp_uploaddocfile',
			id:book_id,
		}, function(response) {
			//clear_upload_files();
			 $("#formss")[0].reset(); 
			//$window.unbind( 'beforeunload' );
			//alert(mpcthLocalize.docurlfull + "ajaxupload.php?id=" + bla);
		 
				//location.reload();
				 
				 $.post(ajaxurl, {
						action: 'rfbwp_refresh_tabs_content_new',
						id:book_id,
					}, function(response) {
						

						$('form#options-form .group.settings, form#options-form .group.pages').remove();

						$('form#options-form div.group.books').after(response);

						$('form#options-form div.group').each(function(){
							var $this = $(this);
							if($this.hasClass('settings') || $this.hasClass('pages'))
								$this.hide();
						});
						
						$('div#rfbwp_tools').each(function(){
							var $this = $(this);
							if($this.hasClass('tollsbook'))
								$this.hide();
						});
						 $('div#rfbwp_tools').attr('style','display:none');
                        // display_alert('green', mpcthLocalize.messages.dialogs.importFinished, 3000);
						$('#mp-option-pages_' + $('#rfbwp_tools').attr('data-book-id')).find('.breadcrumbs .breadcrumb-2').trigger('click');
                          //$('#mp-option-pages_'+ book_id).find('.breadcrumbs .breadcrumb-2').trigger('click');
						//rfbwp_remove_loader();
					});
			 
		 
	    }); 
			
    }
      },
     error: function(e) 
      {
    $("#err").html(e).fadeIn();
      }          
    });
 }));
	 
		   
		 function do_upload_doc(){
            
                }   
		   

/*
 * jQuery File Upload Plugin 5.42.3
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
!function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery","jquery.ui.widget"],e):"object"==typeof exports?e(require("jquery"),require("./vendor/jquery.ui.widget")):e(window.jQuery)}(function(e){"use strict";function t(t){var i="dragover"===t;return function(r){r.dataTransfer=r.originalEvent&&r.originalEvent.dataTransfer;var n=r.dataTransfer;n&&-1!==e.inArray("Files",n.types)&&this._trigger(t,e.Event(t,{delegatedEvent:r}))!==!1&&(r.preventDefault(),i&&(n.dropEffect="copy"))}}e.support.fileInput=!(new RegExp("(Android (1\\.[0156]|2\\.[01]))|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)|(w(eb)?OSBrowser)|(webOS)|(Kindle/(1\\.0|2\\.[05]|3\\.0))").test(window.navigator.userAgent)||e('<input type="file">').prop("disabled")),e.support.xhrFileUpload=!(!window.ProgressEvent||!window.FileReader),e.support.xhrFormDataFileUpload=!!window.FormData,e.support.blobSlice=window.Blob&&(Blob.prototype.slice||Blob.prototype.webkitSlice||Blob.prototype.mozSlice),e.widget("blueimp.fileupload",{options:{dropZone:e(document),pasteZone:void 0,fileInput:void 0,replaceFileInput:!0,paramName:void 0,singleFileUploads:!0,limitMultiFileUploads:void 0,limitMultiFileUploadSize:void 0,limitMultiFileUploadSizeOverhead:512,sequentialUploads:!1,limitConcurrentUploads:void 0,forceIframeTransport:!1,redirect:void 0,redirectParamName:void 0,postMessage:void 0,multipart:!0,maxChunkSize:void 0,uploadedBytes:void 0,recalculateProgress:!0,progressInterval:100,bitrateInterval:500,autoUpload:!0,messages:{uploadedBytes:"Uploaded bytes exceed file size"},i18n:function(t,i){return t=this.messages[t]||t.toString(),i&&e.each(i,function(e,i){t=t.replace("{"+e+"}",i)}),t},formData:function(e){return e.serializeArray()},add:function(t,i){return t.isDefaultPrevented()?!1:void((i.autoUpload||i.autoUpload!==!1&&e(this).fileupload("option","autoUpload"))&&i.process().done(function(){i.submit()}))},processData:!1,contentType:!1,cache:!1},_specialOptions:["fileInput","dropZone","pasteZone","multipart","forceIframeTransport"],_blobSlice:e.support.blobSlice&&function(){var e=this.slice||this.webkitSlice||this.mozSlice;return e.apply(this,arguments)},_BitrateTimer:function(){this.timestamp=Date.now?Date.now():(new Date).getTime(),this.loaded=0,this.bitrate=0,this.getBitrate=function(e,t,i){var r=e-this.timestamp;return(!this.bitrate||!i||r>i)&&(this.bitrate=(t-this.loaded)*(1e3/r)*8,this.loaded=t,this.timestamp=e),this.bitrate}},_isXHRUpload:function(t){return!t.forceIframeTransport&&(!t.multipart&&e.support.xhrFileUpload||e.support.xhrFormDataFileUpload)},_getFormData:function(t){var i;return"function"===e.type(t.formData)?t.formData(t.form):e.isArray(t.formData)?t.formData:"object"===e.type(t.formData)?(i=[],e.each(t.formData,function(e,t){i.push({name:e,value:t})}),i):[]},_getTotal:function(t){var i=0;return e.each(t,function(e,t){i+=t.size||1}),i},_initProgressObject:function(t){var i={loaded:0,total:0,bitrate:0};t._progress?e.extend(t._progress,i):t._progress=i},_initResponseObject:function(e){var t;if(e._response)for(t in e._response)e._response.hasOwnProperty(t)&&delete e._response[t];else e._response={}},_onProgress:function(t,i){if(t.lengthComputable){var r,n=Date.now?Date.now():(new Date).getTime();if(i._time&&i.progressInterval&&n-i._time<i.progressInterval&&t.loaded!==t.total)return;i._time=n,r=Math.floor(t.loaded/t.total*(i.chunkSize||i._progress.total))+(i.uploadedBytes||0),this._progress.loaded+=r-i._progress.loaded,this._progress.bitrate=this._bitrateTimer.getBitrate(n,this._progress.loaded,i.bitrateInterval),i._progress.loaded=i.loaded=r,i._progress.bitrate=i.bitrate=i._bitrateTimer.getBitrate(n,r,i.bitrateInterval),this._trigger("progress",e.Event("progress",{delegatedEvent:t}),i),this._trigger("progressall",e.Event("progressall",{delegatedEvent:t}),this._progress)}},_initProgressListener:function(t){var i=this,r=t.xhr?t.xhr():e.ajaxSettings.xhr();r.upload&&(e(r.upload).bind("progress",function(e){var r=e.originalEvent;e.lengthComputable=r.lengthComputable,e.loaded=r.loaded,e.total=r.total,i._onProgress(e,t)}),t.xhr=function(){return r})},_isInstanceOf:function(e,t){return Object.prototype.toString.call(t)==="[object "+e+"]"},_initXHRData:function(t){var i,r=this,n=t.files[0],o=t.multipart||!e.support.xhrFileUpload,s="array"===e.type(t.paramName)?t.paramName[0]:t.paramName;t.headers=e.extend({},t.headers),t.contentRange&&(t.headers["Content-Range"]=t.contentRange),o&&!t.blob&&this._isInstanceOf("File",n)||(t.headers["Content-Disposition"]='attachment; filename="'+encodeURI(n.name)+'"'),o?e.support.xhrFormDataFileUpload&&(t.postMessage?(i=this._getFormData(t),t.blob?i.push({name:s,value:t.blob}):e.each(t.files,function(r,n){i.push({name:"array"===e.type(t.paramName)&&t.paramName[r]||s,value:n})})):(r._isInstanceOf("FormData",t.formData)?i=t.formData:(i=new FormData,e.each(this._getFormData(t),function(e,t){i.append(t.name,t.value)})),t.blob?i.append(s,t.blob,n.name):e.each(t.files,function(n,o){(r._isInstanceOf("File",o)||r._isInstanceOf("Blob",o))&&i.append("array"===e.type(t.paramName)&&t.paramName[n]||s,o,o.uploadName||o.name)})),t.data=i):(t.contentType=n.type||"application/octet-stream",t.data=t.blob||n),t.blob=null},_initIframeSettings:function(t){var i=e("<a></a>").prop("href",t.url).prop("host");t.dataType="iframe "+(t.dataType||""),t.formData=this._getFormData(t),t.redirect&&i&&i!==location.host&&t.formData.push({name:t.redirectParamName||"redirect",value:t.redirect})},_initDataSettings:function(e){this._isXHRUpload(e)?(this._chunkedUpload(e,!0)||(e.data||this._initXHRData(e),this._initProgressListener(e)),e.postMessage&&(e.dataType="postmessage "+(e.dataType||""))):this._initIframeSettings(e)},_getParamName:function(t){var i=e(t.fileInput),r=t.paramName;return r?e.isArray(r)||(r=[r]):(r=[],i.each(function(){for(var t=e(this),i=t.prop("name")||"files[]",n=(t.prop("files")||[1]).length;n;)r.push(i),n-=1}),r.length||(r=[i.prop("name")||"files[]"])),r},_initFormSettings:function(t){t.form&&t.form.length||(t.form=e(t.fileInput.prop("form")),t.form.length||(t.form=e(this.options.fileInput.prop("form")))),t.paramName=this._getParamName(t),t.url||(t.url=t.form.prop("action")||location.href),t.type=(t.type||"string"===e.type(t.form.prop("method"))&&t.form.prop("method")||"").toUpperCase(),"POST"!==t.type&&"PUT"!==t.type&&"PATCH"!==t.type&&(t.type="POST"),t.formAcceptCharset||(t.formAcceptCharset=t.form.attr("accept-charset"))},_getAJAXSettings:function(t){var i=e.extend({},this.options,t);return this._initFormSettings(i),this._initDataSettings(i),i},_getDeferredState:function(e){return e.state?e.state():e.isResolved()?"resolved":e.isRejected()?"rejected":"pending"},_enhancePromise:function(e){return e.success=e.done,e.error=e.fail,e.complete=e.always,e},_getXHRPromise:function(t,i,r){var n=e.Deferred(),o=n.promise();return i=i||this.options.context||o,t===!0?n.resolveWith(i,r):t===!1&&n.rejectWith(i,r),o.abort=n.promise,this._enhancePromise(o)},_addConvenienceMethods:function(t,i){var r=this,n=function(t){return e.Deferred().resolveWith(r,t).promise()};i.process=function(t,o){return(t||o)&&(i._processQueue=this._processQueue=(this._processQueue||n([this])).pipe(function(){return i.errorThrown?e.Deferred().rejectWith(r,[i]).promise():n(arguments)}).pipe(t,o)),this._processQueue||n([this])},i.submit=function(){return"pending"!==this.state()&&(i.jqXHR=this.jqXHR=r._trigger("submit",e.Event("submit",{delegatedEvent:t}),this)!==!1&&r._onSend(t,this)),this.jqXHR||r._getXHRPromise()},i.abort=function(){return this.jqXHR?this.jqXHR.abort():(this.errorThrown="abort",r._trigger("fail",null,this),r._getXHRPromise(!1))},i.state=function(){return this.jqXHR?r._getDeferredState(this.jqXHR):this._processQueue?r._getDeferredState(this._processQueue):void 0},i.processing=function(){return!this.jqXHR&&this._processQueue&&"pending"===r._getDeferredState(this._processQueue)},i.progress=function(){return this._progress},i.response=function(){return this._response}},_getUploadedBytes:function(e){var t=e.getResponseHeader("Range"),i=t&&t.split("-"),r=i&&i.length>1&&parseInt(i[1],10);return r&&r+1},_chunkedUpload:function(t,i){t.uploadedBytes=t.uploadedBytes||0;var r,n,o=this,s=t.files[0],a=s.size,l=t.uploadedBytes,p=t.maxChunkSize||a,u=this._blobSlice,d=e.Deferred(),h=d.promise();return this._isXHRUpload(t)&&u&&(l||a>p)&&!t.data?i?!0:l>=a?(s.error=t.i18n("uploadedBytes"),this._getXHRPromise(!1,t.context,[null,"error",s.error])):(n=function(){var i=e.extend({},t),h=i._progress.loaded;i.blob=u.call(s,l,l+p,s.type),i.chunkSize=i.blob.size,i.contentRange="bytes "+l+"-"+(l+i.chunkSize-1)+"/"+a,o._initXHRData(i),o._initProgressListener(i),r=(o._trigger("chunksend",null,i)!==!1&&e.ajax(i)||o._getXHRPromise(!1,i.context)).done(function(r,s,p){l=o._getUploadedBytes(p)||l+i.chunkSize,h+i.chunkSize-i._progress.loaded&&o._onProgress(e.Event("progress",{lengthComputable:!0,loaded:l-i.uploadedBytes,total:l-i.uploadedBytes}),i),t.uploadedBytes=i.uploadedBytes=l,i.result=r,i.textStatus=s,i.jqXHR=p,o._trigger("chunkdone",null,i),o._trigger("chunkalways",null,i),a>l?n():d.resolveWith(i.context,[r,s,p])}).fail(function(e,t,r){i.jqXHR=e,i.textStatus=t,i.errorThrown=r,o._trigger("chunkfail",null,i),o._trigger("chunkalways",null,i),d.rejectWith(i.context,[e,t,r])})},this._enhancePromise(h),h.abort=function(){return r.abort()},n(),h):!1},_beforeSend:function(e,t){0===this._active&&(this._trigger("start"),this._bitrateTimer=new this._BitrateTimer,this._progress.loaded=this._progress.total=0,this._progress.bitrate=0),this._initResponseObject(t),this._initProgressObject(t),t._progress.loaded=t.loaded=t.uploadedBytes||0,t._progress.total=t.total=this._getTotal(t.files)||1,t._progress.bitrate=t.bitrate=0,this._active+=1,this._progress.loaded+=t.loaded,this._progress.total+=t.total},_onDone:function(t,i,r,n){var o=n._progress.total,s=n._response;n._progress.loaded<o&&this._onProgress(e.Event("progress",{lengthComputable:!0,loaded:o,total:o}),n),s.result=n.result=t,s.textStatus=n.textStatus=i,s.jqXHR=n.jqXHR=r,this._trigger("done",null,n)},_onFail:function(e,t,i,r){var n=r._response;r.recalculateProgress&&(this._progress.loaded-=r._progress.loaded,this._progress.total-=r._progress.total),n.jqXHR=r.jqXHR=e,n.textStatus=r.textStatus=t,n.errorThrown=r.errorThrown=i,this._trigger("fail",null,r)},_onAlways:function(e,t,i,r){this._trigger("always",null,r)},_onSend:function(t,i){i.submit||this._addConvenienceMethods(t,i);var r,n,o,s,a=this,l=a._getAJAXSettings(i),p=function(){return a._sending+=1,l._bitrateTimer=new a._BitrateTimer,r=r||((n||a._trigger("send",e.Event("send",{delegatedEvent:t}),l)===!1)&&a._getXHRPromise(!1,l.context,n)||a._chunkedUpload(l)||e.ajax(l)).done(function(e,t,i){a._onDone(e,t,i,l)}).fail(function(e,t,i){a._onFail(e,t,i,l)}).always(function(e,t,i){if(a._onAlways(e,t,i,l),a._sending-=1,a._active-=1,l.limitConcurrentUploads&&l.limitConcurrentUploads>a._sending)for(var r=a._slots.shift();r;){if("pending"===a._getDeferredState(r)){r.resolve();break}r=a._slots.shift()}0===a._active&&a._trigger("stop")})};return this._beforeSend(t,l),this.options.sequentialUploads||this.options.limitConcurrentUploads&&this.options.limitConcurrentUploads<=this._sending?(this.options.limitConcurrentUploads>1?(o=e.Deferred(),this._slots.push(o),s=o.pipe(p)):(this._sequence=this._sequence.pipe(p,p),s=this._sequence),s.abort=function(){return n=[void 0,"abort","abort"],r?r.abort():(o&&o.rejectWith(l.context,n),p())},this._enhancePromise(s)):p()},_onAdd:function(t,i){var r,n,o,s,a=this,l=!0,p=e.extend({},this.options,i),u=i.files,d=u.length,h=p.limitMultiFileUploads,c=p.limitMultiFileUploadSize,f=p.limitMultiFileUploadSizeOverhead,g=0,_=this._getParamName(p),m=0;if(!c||d&&void 0!==u[0].size||(c=void 0),(p.singleFileUploads||h||c)&&this._isXHRUpload(p))if(p.singleFileUploads||c||!h)if(!p.singleFileUploads&&c)for(o=[],r=[],s=0;d>s;s+=1)g+=u[s].size+f,(s+1===d||g+u[s+1].size+f>c||h&&s+1-m>=h)&&(o.push(u.slice(m,s+1)),n=_.slice(m,s+1),n.length||(n=_),r.push(n),m=s+1,g=0);else r=_;else for(o=[],r=[],s=0;d>s;s+=h)o.push(u.slice(s,s+h)),n=_.slice(s,s+h),n.length||(n=_),r.push(n);else o=[u],r=[_];return i.originalFiles=u,e.each(o||u,function(n,s){var p=e.extend({},i);return p.files=o?s:[s],p.paramName=r[n],a._initResponseObject(p),a._initProgressObject(p),a._addConvenienceMethods(t,p),l=a._trigger("add",e.Event("add",{delegatedEvent:t}),p)}),l},_replaceFileInput:function(t){var i=t.fileInput,r=i.clone(!0);t.fileInputClone=r,e("<form></form>").append(r)[0].reset(),i.after(r).detach(),e.cleanData(i.unbind("remove")),this.options.fileInput=this.options.fileInput.map(function(e,t){return t===i[0]?r[0]:t}),i[0]===this.element[0]&&(this.element=r)},_handleFileTreeEntry:function(t,i){var r,n=this,o=e.Deferred(),s=function(e){e&&!e.entry&&(e.entry=t),o.resolve([e])},a=function(e){n._handleFileTreeEntries(e,i+t.name+"/").done(function(e){o.resolve(e)}).fail(s)},l=function(){r.readEntries(function(e){e.length?(p=p.concat(e),l()):a(p)},s)},p=[];return i=i||"",t.isFile?t._file?(t._file.relativePath=i,o.resolve(t._file)):t.file(function(e){e.relativePath=i,o.resolve(e)},s):t.isDirectory?(r=t.createReader(),l()):o.resolve([]),o.promise()},_handleFileTreeEntries:function(t,i){var r=this;return e.when.apply(e,e.map(t,function(e){return r._handleFileTreeEntry(e,i)})).pipe(function(){return Array.prototype.concat.apply([],arguments)})},_getDroppedFiles:function(t){t=t||{};var i=t.items;return i&&i.length&&(i[0].webkitGetAsEntry||i[0].getAsEntry)?this._handleFileTreeEntries(e.map(i,function(e){var t;return e.webkitGetAsEntry?(t=e.webkitGetAsEntry(),t&&(t._file=e.getAsFile()),t):e.getAsEntry()})):e.Deferred().resolve(e.makeArray(t.files)).promise()},_getSingleFileInputFiles:function(t){t=e(t);var i,r,n=t.prop("webkitEntries")||t.prop("entries");if(n&&n.length)return this._handleFileTreeEntries(n);if(i=e.makeArray(t.prop("files")),i.length)void 0===i[0].name&&i[0].fileName&&e.each(i,function(e,t){t.name=t.fileName,t.size=t.fileSize});else{if(r=t.prop("value"),!r)return e.Deferred().resolve([]).promise();i=[{name:r.replace(/^.*\\/,"")}]}return e.Deferred().resolve(i).promise()},_getFileInputFiles:function(t){return t instanceof e&&1!==t.length?e.when.apply(e,e.map(t,this._getSingleFileInputFiles)).pipe(function(){return Array.prototype.concat.apply([],arguments)}):this._getSingleFileInputFiles(t)},_onChange:function(t){var i=this,r={fileInput:e(t.target),form:e(t.target.form)};this._getFileInputFiles(r.fileInput).always(function(n){r.files=n,i.options.replaceFileInput&&i._replaceFileInput(r),i._trigger("change",e.Event("change",{delegatedEvent:t}),r)!==!1&&i._onAdd(t,r)})},_onPaste:function(t){var i=t.originalEvent&&t.originalEvent.clipboardData&&t.originalEvent.clipboardData.items,r={files:[]};i&&i.length&&(e.each(i,function(e,t){var i=t.getAsFile&&t.getAsFile();i&&r.files.push(i)}),this._trigger("paste",e.Event("paste",{delegatedEvent:t}),r)!==!1&&this._onAdd(t,r))},_onDrop:function(t){t.dataTransfer=t.originalEvent&&t.originalEvent.dataTransfer;var i=this,r=t.dataTransfer,n={};r&&r.files&&r.files.length&&(t.preventDefault(),this._getDroppedFiles(r).always(function(r){n.files=r,i._trigger("drop",e.Event("drop",{delegatedEvent:t}),n)!==!1&&i._onAdd(t,n)}))},_onDragOver:t("dragover"),_onDragEnter:t("dragenter"),_onDragLeave:t("dragleave"),_initEventHandlers:function(){this._isXHRUpload(this.options)&&(this._on(this.options.dropZone,{dragover:this._onDragOver,drop:this._onDrop,dragenter:this._onDragEnter,dragleave:this._onDragLeave}),this._on(this.options.pasteZone,{paste:this._onPaste})),e.support.fileInput&&this._on(this.options.fileInput,{change:this._onChange})},_destroyEventHandlers:function(){this._off(this.options.dropZone,"dragenter dragleave dragover drop"),this._off(this.options.pasteZone,"paste"),this._off(this.options.fileInput,"change")},_setOption:function(t,i){var r=-1!==e.inArray(t,this._specialOptions);r&&this._destroyEventHandlers(),this._super(t,i),r&&(this._initSpecialOptions(),this._initEventHandlers())},_initSpecialOptions:function(){var t=this.options;void 0===t.fileInput?t.fileInput=this.element.is('input[type="file"]')?this.element:this.element.find('input[type="file"]'):t.fileInput instanceof e||(t.fileInput=e(t.fileInput)),t.dropZone instanceof e||(t.dropZone=e(t.dropZone)),t.pasteZone instanceof e||(t.pasteZone=e(t.pasteZone))},_getRegExp:function(e){var t=e.split("/"),i=t.pop();return t.shift(),new RegExp(t.join("/"),i)},_isRegExpOption:function(t,i){return"url"!==t&&"string"===e.type(i)&&/^\/.*\/[igm]{0,3}$/.test(i)},_initDataAttributes:function(){var t=this,i=this.options,r=this.element.data();e.each(this.element[0].attributes,function(e,n){var o,s=n.name.toLowerCase();/^data-/.test(s)&&(s=s.slice(5).replace(/-[a-z]/g,function(e){return e.charAt(1).toUpperCase()}),o=r[s],t._isRegExpOption(s,o)&&(o=t._getRegExp(o)),i[s]=o)})},_create:function(){this._initDataAttributes(),this._initSpecialOptions(),this._slots=[],this._sequence=this._getXHRPromise(!0),this._sending=this._active=0,this._initProgressObject(this),this._initEventHandlers()},active:function(){return this._active},progress:function(){return this._progress},add:function(t){var i=this;t&&!this.options.disabled&&(t.fileInput&&!t.files?this._getFileInputFiles(t.fileInput).always(function(e){t.files=e,i._onAdd(null,t)}):(t.files=e.makeArray(t.files),this._onAdd(null,t)))},send:function(t){if(t&&!this.options.disabled){if(t.fileInput&&!t.files){var i,r,n=this,o=e.Deferred(),s=o.promise();return s.abort=function(){return r=!0,i?i.abort():(o.reject(null,"abort","abort"),s)},this._getFileInputFiles(t.fileInput).always(function(e){if(!r){if(!e.length)return void o.reject();t.files=e,i=n._onSend(null,t),i.then(function(e,t,i){o.resolve(e,t,i)},function(e,t,i){o.reject(e,t,i)})}}),this._enhancePromise(s)}if(t.files=e.makeArray(t.files),t.files.length)return this._onSend(null,t)}return this._getXHRPromise(!1,t&&t.context)}})});	
	 	
	});
})();
