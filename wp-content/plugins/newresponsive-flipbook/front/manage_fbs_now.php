<?php

 function show_fb() {
  $current_user = wp_get_current_user();
  if(!is_user_logged_in())
	wp_redirect(home_url());
        error_reporting(E_ALL & ~ E_NOTICE);
        require_once(plugin_dir_path( __FILE__ ).'../../../../wp-admin/includes/plugin.php');
        require_once(plugin_dir_path( __FILE__ ).'../massive-panel/theme-settings.php');
		require_once(plugin_dir_path( __FILE__ ).'../massive-panel/theme-options.php');
		require_once(plugin_dir_path( __FILE__ ).'../../responsive-flipbook-pdf/pdf_wizard.php');
		//require_once(plugin_dir_path( __FILE__ ).'../front/ajaxupload.php');
		//theme-options-font
        mp_register_settings();
     	$settings_output = mp_get_settings();
	//$content = mp_display_front_content();
        $plugin_data = get_plugin_data( MPC_PLUGIN_FILE );
 	wp_enqueue_style('mp_theme_et_icons', MPC_PLUGIN_ROOT.'/assets/fonts/et-icons.css');
	wp_enqueue_style('mp_theme_et_line', MPC_PLUGIN_ROOT.'/assets/fonts/et-line.css');
	wp_enqueue_style('mp_theme_settings_css', MPC_PLUGIN_ROOT.'/massive-panel/css/mp-styles.css');
	wp_enqueue_style('mp_theme_select2_css', MPC_PLUGIN_ROOT.'/massive-panel/select2/select2.css');
	wp_enqueue_style('flipbook_styles', MPC_PLUGIN_ROOT.'/assets/css/style.min.css');
	wp_enqueue_style('wp-color-picker1', MPC_PLUGIN_ROOT.'/front/css/color-picker.css');
	
	wp_enqueue_style( 'rfbwp-pdf-css', MPC_PLUGIN_ROOT . '/assets/css/pdf_wizard.css' );
	wp_enqueue_script( 'rfbwp-upload-js', MPC_PLUGIN_ROOT . '/js/fileupload.min.js', array('jquery'), false, true);  
	//wp_enqueue_script( 'rfbwp-uploads-js', MPC_PLUGIN_ROOT . '/js/fileuploadv.js', array('jquery'), false, true);
	//wp_enqueue_script( 'rfbwp-pdf-js', MPC_PLUGIN_ROOT . '/js/pdf_wizard.min.js', array( 'jquery', 'rfbwp-upload-js' ) );
	//wp_enqueue_script( 'rfbwp-pdf-js', MPC_PLUGIN_ROOT . '/js/pdf_wizard_fornt.js', array( 'jquery', 'rfbwp-upload-js' ) );
	
        wp_enqueue_style('wp-jquery-ui-dialog');
        wp_enqueue_script('mp_theme_select2_js', MPC_PLUGIN_ROOT.'/massive-panel/select2/select2.min.js', array('jquery'), false, true);
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script(
			'iris',
			admin_url( 'js/iris.min.js' ),
			array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
			false,
			1
		);
        wp_enqueue_script(
			'wp-color-picker',
			admin_url( 'js/color-picker.min.js' ),
			array( 'iris' ),
			false,
			1
		);
        $colorpicker_l10n = array(
			'clear' => __( 'Clear' ),
			'defaultString' => __( 'Default' ),
			'pick' => __( 'Select Color' ),
			'current' => __( 'Current Color' ),
		);
        wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n ); 
		
        /* UPLOAD IMAGE */
        wp_enqueue_script( 'plupload-handlers' );
        wp_enqueue_script('mp_theme_icon_select_js', MPC_PLUGIN_ROOT.'/massive-panel/mpc_icon/icon_select/field_icon_select.js', array('jquery', 'jquery-ui-dialog'), false, true );
	wp_enqueue_script('mp_theme_toc_generator_js', MPC_PLUGIN_ROOT.'/massive-panel/mpc_toc/field_toc.js', array('jquery', 'jquery-ui-dialog'), false, true );
                        
	wp_enqueue_script('webfonts', '//ajax.googleapis.com/ajax/libs/webfont/1.1.2/webfont.js');
        wp_enqueue_style('rfbwp-styles2', MPC_PLUGIN_ROOT.'/massive-panel/css/mp-styles.css');
       wp_enqueue_script('mp_theme_settings_js', MPC_PLUGIN_ROOT.'/front/js/fbsnew.js', array('jquery', 'jquery-ui-core', 'jquery-ui-dialog'), false, true);
        wp_enqueue_media();
                        
        wp_enqueue_style('rfbwp-fontawesome', MPC_PLUGIN_ROOT.'/assets/fonts/font-awesome.css');
         // JS
	wp_enqueue_script('ion-sound', MPC_PLUGIN_ROOT.'/assets/js/ion.sound.min.js', array('jquery'));
	wp_enqueue_script('jquery-doubletab', MPC_PLUGIN_ROOT.'/assets/js/jquery.doubletap.js', array('jquery'));
wp_enqueue_script('jquery-migrate', "/wp-includes/js/jquery/jquery-migrate.js", array(), '1.2.1' );
	wp_localize_script( 'ion-sound', 'mpcthLocalize', array(
		'soundsPath' => MPC_PLUGIN_ROOT . '/assets/sounds/',
		'downloadPath' => MPC_PLUGIN_ROOT . '/includes/download.php?file='
	) );
	wp_enqueue_script('mp_theme_tinymce_js', MPC_PLUGIN_ROOT.'/massive-panel/tinymce/tinymce.min.js', array('jquery'), false, true);
	wp_enqueue_script('mp_theme_ace_js', MPC_PLUGIN_ROOT.'/massive-panel/ace/ace.js', array('jquery'), false, true);
	wp_enqueue_script('mp_theme_ace_css_mode', MPC_PLUGIN_ROOT.'/massive-panel/ace/mode-css.js', array('jquery', 'mp_theme_ace_js'), false, true);

	wp_localize_script('mp_theme_settings_js', 'mpcthLocalize', array(
		'optionsName'		=> $rfbwp_shortname,
		'googleAPIErrorMsg' => __('There is problem with access to Google Webfonts. Please try again later. If this message keeps appearing please contact our support at <a href="http://mpc.ticksy.com/">mpc.ticksy.com</a>.', 'mpcth'),
		'googleAPIKey'		=> MPC_RFBWP_GOOGLE_FONTS_API_ID,
		'googleFonts'		=> stripslashes( $google_webfonts ),
		'addNewPage'		=> __('Add New Page', 'rfbwp' ),
		'editPage'			=> __('Edit Page', 'rfbwp' ),
		'previewPage'		=> __('Preview Page', 'rfbwp' ),
		'deletePage'		=> __('Delete Page', 'rfbwp' ),
		'addaudio'		=> __('Add Audio', 'rfbwp' ),
		'cancelNewPage'		=> __('Cancel', 'rfbwp'),
		'presetsURL'		=> plugin_dir_url( __FILE__ ) . 'presets/',
		'docurlfull'        => home_url() . '/wp-content/plugins/responsive-flipbook-pdf/',
		'messages'			=> array(
			'errors' => array(
				'lastPage'		=> __( 'last page must be single', 'rfbwp' ),
				'firstPage'		=> __( 'first page must be single', 'rfbwp' ),
				'minPages'		=> __( 'book needs at least 4 pages', 'rfbwp' ),
				'evenPages'		=> __( 'number of pages must be even', 'rfbwp' ),
				'error'			=> __( '<span>ERROR: </span>', 'rfbwp' ),
			),
			'dialogs' => array(
				'maxInputVars'	=> __( 'We are sorry but your changes weren\'t saved. Please increase "max_input_vars" value in your "php.ini" file.', 'rfbwp' ),
				'bottomPage'	=> __( 'Oops! It looks like this page is already at the bottom.', 'rfbwp' ),
				'topPage'		=> __( 'Oops! It looks like this page is already at the top.', 'rfbwp' ),
				'bookSaved'		=> __( 'Book settings saved successfully.', 'rfbwp' ),
				'importFinished'=> __( 'Import has been successfully finished.', 'rfbwp' ),
				'normalLarge'	=> __( "Number of normal and large images don't match.", 'rfbwp' ),
				'noImages'		=> __( 'No images selected to upload.', 'rfbwp' ),
				'bookID'		=> __( 'Wrong book ID.', 'rfbwp' ),
				'selectImage'	=> __( 'Select Image', 'rfbwp' ),
				'insertImage'	=> __( 'Insert Image', 'rfbwp' ),
				'selectImages'	=> __( 'Select Images', 'rfbwp' ),
				'insertImages'	=> __( 'Insert Images', 'rfbwp' ),
				'presetLoaded'	=> __( 'Preset has been loaded.', 'rfbwp' ),
				'deleteBook'	=> __( 'Are you sure you want to delete: ', 'rfbwp' ),
				'selectAudio'	=> __( 'Select Audio', 'rfbwp' ),
				'insertAudio'	=> __( 'Insert Audio', 'rfbwp' ),
			),
		),
	) );
                    
        

        ?> 
<style>
    .wp-color-result:hover,
                .wp-color-result{
                    background-color:#fff;
                }
    .entry{
        border: none !important;
        box-shadow: none !important;
    }
    div#field-rfbwp_pages {
    border: none;
     padding: 0 !important;
}
.entry table {
margin: 0 !important;
}
.entry table td{
    
}
.media-modal{
    z-index: 22222222222 !important;
}
.fw-fsel{
    font-weight: normal;
}  
.fc-fcol{
    width: 74.35%;
}
.breadcrumbs,.tab-group,.section-group,.group,#top-bar,#top-nav,#top{
    display: none;
} 
#mp-option-books{
    display:block;
}
.select2-chosen{
    font-weight:normal
}
.wp-picker-container{
   
    font-weight: normal;
}
#field-rfbwp_fb_hc_fcc  .wp-picker-container,#field-rfbwp_fb_hc_bcc .wp-picker-container {
     
    font-weight: normal;
}
#field-rfbwp_fb_border_color .wp-picker-container{
 
    font-weight: normal;
}
.wrap1{
    display: block;
}

#upload_rfbwp_fb_page_bg_image,#upload_rfbwp_fb_page_bg_image_zoom,.rfbwp-page-save{
    text-shadow: none;
}
#rfbwp_tools{
/*    display: none;*/
}
.entry table{
    text-shadow: none !important;
}
.dsg{dispaly:none;}

#bg-content {
    position: relative;
    width: 848px;
    overflow: hidden;
    border: 1px solid #dddddd;
    background: #fff;
    border-top: 0;
    margin: 0 auto;
}
.entry h1{display:none;}

#rfbwp_fb_descript{padding-top:20px;}
#rfbwp_fb_descript .description-top {width: 100%;
    float: left;}
#rfbwp_fb_descript .controls  input[type=radio]{width: 5%;float: left;}
.controls  .texts{float: left;}
.discriptionfied .description-top {width: 100%;
    float: left;}
.discriptionfied input[type=radio]{float: left;height: 18px;margin-right: 20px;margin-right: 5px;    width: 4%;}
a.add-audio-page {
    display: inline-block;
    margin: 0;
    margin-right: 10px;
    background: #00b16a;
    color: #fff;
}


.edit-button-page{
	background: #52b3d9;  
    padding: 0 32px 0 15px;
    float: left;
    height: 35px;
    line-height: 35px;
    font-weight: bold;
    text-transform: uppercase;
    color: #fff;
   }
.edit-button-page:after {
    position: absolute;
    left: 51px;
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0px 35px 35px 0;
    border-color: transparent #505050 transparent transparent;
    -webkit-transition: all .25s;
    -moz-transition: all .25s;
    -o-transition: all .25s;
    transition: all .25s;
} 
.entry table tr:hover td {
    
    background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
    background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
}
input#rfbwp_fb_tag{width:100%;}
 
#rfbwp_docsubmit{display:none;}
.mp-toggle-header{display:none;}
.rfbwp_tools_toggle_header{display:none;}
input#rfbwp_fb_page_bg_audio_file{
	float: left;
width: 100%;}
a#upload_rfbwp_fb_page_bg_audio_file{float:right}
.success{min-height: 310px;}
.success h2{text-align: center;font-size: 35px;margin-bottom: 20px;color: #3c763d;}
a.edit-buttonstwo {
    text-decoration: none;
    padding: 0 15px 0 15px;
    float: right;
    height: 35px;
    line-height: 35px;
    font-weight: bold;
    text-transform: uppercase;
}
#bg-content {
    position: relative;
    width: 100% !important;
    overflow: hidden;
    border: 1px solid #dddddd;
    background: #fff;
    border-top: 0;
}
.screenshot img {
    width: 60%;
    
}
.sd{color: #d24d57;
    position: absolute;
    margin-left: -14px;
    font-size: 16px;
	cursor: pointer;
}
.entry table td {
padding: 10px !important;
}
#audiouploadforpage{display:none;}

#progress-wrp {
	
	height: 20px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #eee;
    box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
}
#progress-wrp .progress-bar{
	height: 20px;
    border-radius: 3px;
    background-color: #0099CC;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
	margin-bottom: 10px;
}
#progress-wrp .status{
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #000000;
}
#rfbwp_tools_toggle_content{ padding: 30px 20px 30px !important; }
.desc{    
    position: relative;
    display: inline-block;
    width: 68%;
	}
	
.p_90 {
    width: 100%;
    max-width: 1100px !important;
}	
.grid-3-4 {
width: 100% !important;
}
</style>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->

 
<script type="text/javascript">

var jquery = jQuery.noConflict();
$(document).ready(function (e) {
 $('select').on('change', function() {
  if(this.value == 'img')
  {
	 $('div#img').attr('style','display:block'); 
	 $('div#doc').attr('style','display:none'); 
	 $('div#pdf').attr('style','display:none'); 
  }
  if(this.value == 'pdf')
  {
	  $('div#img').attr('style','display:none'); 
	 $('div#doc').attr('style','display:none'); 
	 $('div#pdf').attr('style','display:block');   
  }
   if(this.value == 'doc')
  {
	 $('div#img').attr('style','display:none'); 
	 $('div#doc').attr('style','display:block'); 
	 $('div#pdf').attr('style','display:none');
  }
  
})

$('.wrap1').on('click', 'div#rfbwp_fb_background', function( e ) {
      var book_id = $("#rfbwp_tools").data("book-id");		
      var dfs = $('input[name=rfbwp_fb_background]:checked').val();
	  if(dfs == 'newbackground')
	  { 
       $('div#rfbwp_fb_background_img').attr('style','display:block');
      }
	  else{
		$('div#rfbwp_fb_background_img').attr('style','display:none');  
	  }
    });	
	$('.wrap1').css("min-height", $(window).height()-230 + "px" );
	
	var counter = 1;
	jQuery('.dyn_ad_revenue_checkbox').click(function(){
	var revenueval = $(this).val();
	if(revenueval == 'yes'){
		jQuery('.revnue').css('display','block');
		jQuery('.revenue-paypal').click(function(){
			email = $('.revenue-paypal-email').val();
			var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			var result = pattern .test(email);
			//if(result){
			jQuery('.revenue-paypal-email').val('');
			$("#loading").css('display','block');
			$("#author-lists").css('display','none');
			setTimeout(function(){											
			jQuery.post("<?php echo get_template_directory_uri();?>/get-author.php",{email:email,counter:counter},function(data){	
				if(data['author']){
					if(data['marchant_paypal'] && data['marchant_revenue']){	
					var newRow = jQuery('<tr id="row'+data['counter']+'"><td><a class="remove'+data['counter']+'">Remove</a></td><td><p id="authorbtn'+data['counter']+'">Author Name</p><input type="hidden" class="revusers" name="revusers'+data['counter']+'" class="authorid" value="'+data['authorID']+'"/></td><td><input type="number" class="num" id="numbersOnly'+data['counter']+ '" name="percentage-revenue'+data['counter']+ '" max="" min=""/>%<input type="hidden" class="percval" id="numval'+data['counter']+'" name="perceval'+data['counter']+'"/></td></tr>');
					jQuery('table.author-lists').append(newRow);
					jQuery("#error-msg").html('');
					jQuery("#counterval").val(data['counter']);
					}else{
						jQuery("#error-msg").html("User is not allow for revenue share.");
					}
				}else{
					jQuery("#error-msg").html("No user Found. Please try again.");
				}
				jQuery("#authorbtn"+data['counter']).html(data['author']);
				jQuery("#numbersOnly"+data['counter']).attr('min',1);
				jQuery("#numbersOnly"+data['counter']).attr('max',80);
				jQuery(".remove"+data['counter']).click(function(){
					$("#row"+data['counter']).remove();
					checksum();
				});
				jQuery("#author-lists").on('change', '#numbersOnly'+data['counter'], function(){
					jQuery('#numval'+data['counter']).val(this.value);
					checksum();
				});
				function checksum(){
					var sum = 0;
					jQuery('.num').each(function(){
						sum += Number(jQuery(this).val());	
					});										
					jQuery('#chnl-perc-val').val(100 - sum);
				}
			},'json');
			$("#loading").css('display','none');
			$("#author-lists").css('display','block');
			},5000);								
			counter++;									
			//}else{
				//jQuery("#error-msg").html("Please enter valid Email address.");
			//}
		});
	}else{
		jQuery('.revnue').css('display','none');
		jQuery("#error-msg").html('');
	}
	});
 
});
var $ = jQuery.noConflict();


</script>
<?php  $currentproductid = rfbwp_add_new_book_fornt(); ?>	
<script>
(function( $ ) {
	// Add Color Picker to all inputs that have 'color-field' class
	$(function() {
	$('.field-color input.wp-color-picker').wpColorPicker();
	});
	
	 
	 
})( jQuery );
</script>
<script>
$(document).on('click','i.sd',function(e){
    e.preventDefault();
    var rel = this.rel;
     //or using attr()
    var rel=$(this).attr('rel'); 
    // alert(rel);  
	var removeid = $('img#img' + rel).data("bk");
	 // alert(removeid);
	  var oladid = $('#rfbwp_flipbook_batch_ids').val();
	 if(rel == 0)
	 {
		 var res = oladid.replace(removeid + ',' ,'');
	  $('#rfbwp_flipbook_batch_ids').val(res);
	 }
	 else{
		 var res = oladid.replace(',' + removeid  ,'');
	  $('#rfbwp_flipbook_batch_ids').val(res); 
	 }
	 $('img#img' + rel).remove();
	 $('i#sda' + rel).remove();
	
	  
  });
 
</script>
<div class="wrap1 col-md-12 col-sm-12" id="wpcontent">
		
				 
<div id="bg-content">				 
				 
<style>
 
#rfbwp_tools{display:none;}	
.activesection{display:block;}
</style>	
<form action="/" id="options-form" name="options-form" method="post">

                                                    <input type="hidden" name="action" value="rfbwp_save_settings" />
                                                    <input type="hidden" name="security" value="<?php echo wp_create_nonce('rfbwp-theme-data'); ?>" />
                                                    <input type="hidden" name="uid" id="uid"  value="<?php echo $current_user->ID; ?>" />
	 
 <div class="group settings " id="mp-option-settings_<?php echo $currentproductid;  ?>" style="display:block">
 <div class="navigatonbackforth">
	<input id="next_btn1" type="button" class="next_btn" value="Next">
	<input id="currentbookid" type="hidden" value="<?php echo $currentproductid;  ?>">
 </div>
 
 
	 <div class="mp-toggle-header open">
		 <span class="toggle-name">Main</span>
		 <span class="toggle-arrow"></span>
	 </div>
	 <div class="mp-toggle-content" data-toggle-section="field-rfbwp_fb_name" style="display: block;">
		 <div id="field-rfbwp_fb_name" class="field  field-text-medium">
             <div class="option">
			     <div class="controls fcontrols">
                     <div class="description-top ">Book Name: </div>
                     <input style="width: 76%;" id="rfbwp_fb_name" class="mp-input-medium mp-input-border " name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_name]" type="text" value=""> 
				     <div class="help-icon">
						 <span class="mp-tooltip bottom">Flip book name is used to generate a unique shortcode for the book (NOTE: Please use only laters, numbers and spaces), width and height are used to specify books dimantions.</span> 
					 </div> 
				 </div>
				 <div class="clear"> </div>
			 </div>
		 </div>
			 
		     <div id="field-rfbwp_fb_is_rtl" class="field  field-checkbox">
                 <div class="option">
                     <div class="controls fcontrols">
                         <div class="description-top ">Right To Left:</div>
                         <input id="rfbwp_fb_is_rtl" class="checkbox of-input" type="checkbox" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_is_rtl]"> 
				         <div class="help-icon">
				             <span class="mp-tooltip top">Select this option to enable RTL display.</span> 
				         </div>
				     </div>
				         <div class="clear"></div>
				 </div>
			 </div>
		     <div class="stacked-fields" data-section-id="field-rfbwp_fb_hc_fco">
			     <div class="help-icon">
				     <span class="mp-tooltip left">Specify images for front cover.</span>
			     </div>
		         <div id="field-rfbwp_fb_hc_fco" class="field  field-upload rfbwp-page-bg-image col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                     <div class="option">
                         <div class="controls fcontrols">
                             <span class="of_metabox_desc">Choose front cover outside image:</span>
                             <div class="screenshot" id="rfbwp_fb_hc_fco_image"> 
			                     <img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-image.png" class="default">
			                 </div> 
			                 <input id="rfbwp_fb_hc_fco" class="upload mp-input mp-input-border rfbwp-page-bg-image" type="text" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_hc_fco]" value="">
                             <a id="upload_rfbwp_fb_hc_fco" class="upload_button mpc-button rfbwp-page-bg-image" rel="1546"> 
			                     <i class="dashicons dashicons-format-image"> </i> Upload Image 
			                 </a>
					     </div>
				     </div> 
			     </div>
			     <div id="field-rfbwp_fb_hc_fci" class="field  field-upload rfbwp-page-bg-image col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                     <div class="option">
                         <div class="controls fcontrols">
                             <span class="of_metabox_desc">Choose front cover inside image:</span>
                             <div class="screenshot" id="rfbwp_fb_hc_fci_image">
						         <img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-image.png" class="default">
						     </div>
						     <input id="rfbwp_fb_hc_fci" class="upload mp-input mp-input-border rfbwp-page-bg-image" type="text" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_hc_fci]" value="">
                             <a id="upload_rfbwp_fb_hc_fci" class="upload_button mpc-button rfbwp-page-bg-image" rel="1546">
						         <i class="dashicons dashicons-format-image"></i> Upload Image
						     </a> 
					     </div> 
					     <div class="clear"></div>
				     </div>
			     </div>
		     </div>
		     <div id="field-rfbwp_fb_hc_fcc" class="field  field-color">
                 <div class="option">
                     <div class="controls fcontrols">
                         <div class="description-top ">Front cover side:</div>
                         <input class="mp-color mp-input-border wp-color-picker" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_hc_fcc]" id="rfbwp_fb_hc_fcc" type="text" value="#dddddd" style="display: none;">
							 
				     </div>
				     <div class="clear"></div>
			     </div>
		     </div>
		     <div class="stacked-fields" data-section-id="field-rfbwp_fb_hc_bco">
		         <div class="help-icon">
				     <span class="mp-tooltip left">Specify images for back cover.</span>
			     </div>
			     <div id="field-rfbwp_fb_hc_bco" class="field  field-upload rfbwp-page-bg-image col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                     <div class="option">
                         <div class="controls fcontrols">
                             <span class="of_metabox_desc">Choose back cover outside image:</span>
                             <div class="screenshot" id="rfbwp_fb_hc_bco_image">
						         <img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-image.png" class="default">
						     </div> 
						     <input id="rfbwp_fb_hc_bco" class="upload mp-input mp-input-border rfbwp-page-bg-image" type="text" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_hc_bco]" value="">
                             <a id="upload_rfbwp_fb_hc_bco" class="upload_button mpc-button rfbwp-page-bg-image" rel="1546">
						         <i class="dashicons dashicons-format-image"></i> Upload Image
						     </a>
					     </div>
				     </div>
			     </div>
			     <div id="field-rfbwp_fb_hc_bci" class="field  field-upload rfbwp-page-bg-image col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                     <div class="option">
                         <div class="controls fcontrols">
                             <span class="of_metabox_desc">Choose back cover inside image:</span>
                             <div class="screenshot" id="rfbwp_fb_hc_bci_image">
						         <img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-image.png" class="default">
						     </div>
						     <input id="rfbwp_fb_hc_bci" class="upload mp-input mp-input-border rfbwp-page-bg-image" type="text" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_hc_bci]" value="">
                             <a id="upload_rfbwp_fb_hc_bci" class="upload_button mpc-button rfbwp-page-bg-image" rel="1546">
						         <i class="dashicons dashicons-format-image"></i> Upload Image
						     </a>
					     </div>
					     <div class="clear"></div>
				     </div>
			     </div>
		     </div> 
		     <div id="field-rfbwp_fb_hc_bcc" class="field  field-color">
                 <div class="option">
                     <div class="controls fcontrols">
                         <div class="description-top ">Back cover side:</div>
                        
						         <input class="mp-color mp-input-border wp-color-picker" name="rfbwp_options[books][<?php echo $currentproductid;  ?>][rfbwp_fb_hc_bcc]" id="rfbwp_fb_hc_bcc" type="text" value="#dddddd" style="display: none;">
							   
				     </div>
				     <div class="clear"></div>
			     </div>
		     </div>
			 
			  <div id="field-rfbwp_fb_page_bg_audio_file" class="field  field-upload rfbwp-page-bg-audio-file" style="display:none">
                                 <div class="option">
                                     <div class="controls fcontrols">
                                         <span class="of_metabox_desc">Upload Audio:</span>
                                         <input id="rfbwp_fb_page_bg_audio_file" class="upload mp-input mp-input-border rfbwp-page-bg-audio-file pageaudio'. $i .'" type="text" name="'.$settings['books'][$bookID]['pages'][$i]['rfbwp_fb_page_bg_audio_file'].'" value="">
                                         
										
									 </div>
									  
									 <div class="clear"></div>
								 </div>
							 </div>
	 </div>
	  
	  <div class="navigatonbackforth">
	     <input id="next_btn1" class="next_btn" type="button" value="Next">
	  </div>
 </div>	

  <div id="rfbwp_tools" data-book-id="<?php echo $currentproductid;  ?>">
                    <div class="navigatonbackforth">
                       <input id="pre_btn1" type="button" class="pre_btn" value="Previous">
                        <input id="next_btn2" name="next"  class="next_btn" type="button" value="Next">
                    </div>
					
					         
                         

                        <div id="rfbwp_tools_toggle_content" style="display:block;">
						               <header>
                                   
								         <select style="float: right;">
									          <option value="img"> Image</option>
									            <option value="pdf"> PDF </option>
									            <option value="doc"> Document File/Office File</option>
									     </select>	
                                        </header>
						              <div id="img">
                                        <form action="<?php echo admin_url('admin-ajax.php'); ?>" enctype="multipart/form-data" method="post">
                                               <!-- <div class="description-top"><?php _e('Import Flipbook settings:', 'rfbwp'); ?> </div>-->
                                                <input type="hidden" name="action" value="import_flipbooks">
                                                <input type="hidden" name="back_url" value="" id="rfbwp_import_back_url">
                                                <input type="hidden" name="mp-settings" value="Save Page">
                                                <input type="hidden" name="book_id" id="rfbwp_import_id">
                                              <input type="file" class="dshidd" name="import_flipbooks_file" id="rfbwp_import_file" style="display: none;">
                                                <input type="submit" id="rfbwp_import">
                                             <a id="rfbwp_flipbook_import" class="dshidd" class="mpc-button revert" href="#">
                                                        <i class="dashicons dashicons-upload"></i>
                                                        <?php _e('Import', 'rfbwp'); ?>
                                                </a> 
                                              <!--<div class="help-icon">
                                                        <span class="mp-tooltip top">
                                                                <?php _e('Use this field to import all Flipbooks and pages settings from previously created backup. <br /><br /> (NOTE: File must have .rfb extension).', 'rfbwp'); ?>
                                                        </span>
                                                </div>-->
                                        </form>
 
                                        <div class="description-top"><?php _e('Batch Images upload:', 'rfbwp'); ?> </div>
                                        <div class="help-icon">
                                                <span class="mp-tooltip top">
                                                        <?php _e('Use this feature to import multiple images at once. Please notice that the order and amount of Normal and Large images must be exactly the same. <br /><br />If you do not wish to use zoomed (large) images you can left this field empty.', 'rfbwp'); ?>
                                                </span>
                                        </div>
                                        <div class="select-section">
                                                <input type="hidden" id="rfbwp_flipbook_batch_ids" name="rfbwp_flipbook_batch_ids" value="">
                                                <a id="rfbwp_flipbook_batch_select" class="mpc-button revert" href="#">
                                                        <i class="dashicons dashicons-format-gallery"></i>
                                                        <?php _e('Select Batch Image', 'rfbwp'); ?>
                                                </a>
                                                <a id="rfbwp_flipbook_batch_clear" class="mpc-button delete-page" href="#0">
                                                        <i class="dashicons dashicons-trash"></i>
                                                        <?php _e('Delete', 'rfbwp'); ?>
                                                </a>
                                                <div id="rfbwp_flipbook_batch_images_wrap"></div>
                                        </div>
                                     <div class="select-section" style="display:none">
                                                <input type="hidden" id="rfbwp_flipbook_batch_ids_large" name="rfbwp_flipbook_batch_ids_large" value="">
                                                <a id="rfbwp_flipbook_batch_select_large" class="mpc-button revert" href="#">
                                                        <i class="dashicons dashicons-format-gallery"></i>
                                                        <?php _e('Select Large', 'rfbwp'); ?>
                                                </a>
                                                <a id="rfbwp_flipbook_batch_clear_large" class="mpc-button delete-page" href="#0">
                                                        <i class="dashicons dashicons-trash"></i>
                                                        <?php _e('Delete', 'rfbwp'); ?>
                                                </a>
                                                <div id="rfbwp_flipbook_batch_images_wrap_large"></div>
                                        </div>
										</div>
										
										 <div id="pdf" style="display:none">
							      <div class="field">
                                        <div class="description-top"><?php _e('Batch PDF upload:', 'rfbwp'); ?> </div>
                                        <div class="help-icon">
                                                <span class="mp-tooltip top">
                                                        <?php _e('Use this feature to import multiple images at once. Please notice that the order and amount of Normal and Large images must be exactly the same. <br /><br />If you do not wish to use zoomed (large) images you can left this field empty.', 'rfbwp'); ?>
                                                </span>
                                        </div>
									 <a class="convert-book mpc-button revert" style="display:none" href="#"><i class="dashicons dashicons-book"></i> Convert PDF</a>
									 <div id="booknametool"></div>
									</div> 
                               
								
								 <?php echo rfbwp_display_pdf_wizard();
								 ?>
							  </div>
							  
							  <div id="doc" style="display:none">
							   <div class="field">
							   </div>
							   <div class="field">
                                    <div class="description-top"><?php _e('Batch Document upload:', 'rfbwp'); ?> </div>
									<div class="help-icon">
										<span class="mp-tooltip top">
											<?php _e('Only Docx File Alowed', 'rfbwp'); ?>
										</span>
									</div>
									 <form id="formss" action="ajaxupload.php" method="post" enctype="multipart/form-data">
                                        <input id="uploadImage" type="file" class="mpc-button revert" accept="doc/*" name="image" />
										<span id="iputbook"></span>
                                        <input id="button" type="submit" value="Upload" style="display:none">
                                      </form>
									   <a id="rfbwp_flipbook_doc_import" class="mpc-button revert" href="#" >
                                                <i class="dashicons dashicons-upload"></i>
                                                <?php _e('Upload', 'rfbwp'); ?>
                                        </a>
									    <div id="err"></div>
							   </div></div>	
									  </div>
						<div class="navigatonbackforth" style="display:none">
                            <input id="pre_btn1" type="button" class="pre_btn" value="Previous">
                            <input id="next_btn2" name="next"  class="next_btn" type="button" value="Next">
                       </div>
        </div>
		
		
<div class="group pages" id="mp-option-pages_<?php echo $currentproductid;  ?>">
<div class="navigatonbackforth">
   <input id="pre_btn2"  class="pre_btn"  type="button" value="Previous">
   <input id="next_btn3" class="next_btn" name="next" type="button" value="Next">
 </div>
  
	<div class="breadcrumbs" style="display: none;">
		
		<span class="breadcrumb-2 breadcrumb dsg" style="display: none;">All Page</span>
		
	</div>
	<div id="field-rfbwp_pages" class="field  field-pages">
	    <div class="option">
			<div class="controls fcontrols">
				<table class="fpages-table"><tbody>
				  <tr id="pset_0" class="page-set"><td collspan="3"></td></tr>
				 </tbody>
			</table>
		</div><div class="description"></div>
     <div class="clear"></div></div>
	 <div id="pagasssss">
	 </div>
	 </div>
	
		<div class="navigatonbackforth">
		<input id="pre_btn2" class="pre_btn" type="button" value="Previous">
		<input class="next_btn" id="next_btn3" name="next"  type="button" value="Next">
		</div>
</div>

              
	</form>	

  <div id="discriptionfied_publish" class="discriptionfied" style="display:none">
	  <div class="navigatonbackforth">
		<input id="pre_btn3" class="pre_btn" type="button" value="Previous">
		
		</div>
					 <form action="/" id="options-forms" name="options-forms" method="post">
					       <input type="hidden" name="action" value="rfbwp_publish_book_finially" />
						     <input name="mp-publish" style="display:none" class="publish-button" type="submit" value="<?php esc_attr_e('Publish', 'rfbwp'); ?>" />
					 
					<div id="rfbwp_fb_descript" class="field  field-text-medium">
					

                                                
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top "><label>Description: </label></div>
                                 <div id="desc"></div>
								<textarea  rows="3" id="rfbwp_fb_descript" class="mp-input-medium mp-input-border form-control" name="rfbwp_fb_descript"  value=""></textarea> 
       
								  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book Description
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
					  </div>
					  <div id="rfbwp_fb_refernce" class="field  field-text-medium">
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top "><label>Referance: </label></div>
                                  <textarea  rows="3" id="rfbwp_fb_refernce" class="mp-input-medium mp-input-border form-control" name="rfbwp_fb_refernce"  value=""></textarea> 
		
								  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book Referance
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
					  </div> 
					 <div id="rfbwp_fb_tag" class="field  field-text-medium">
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top "><label>Add Tags: </label></div>
                                <input id="rfbwp_fb_tag" class="mp-input-medium mp-input-border " name="rfbwp_fb_tag" type="text" value=""> 
		
								  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book name tag
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>      
					  </div>
					  <div id="rfbwp_fb_background" class="field  field-text-medium" >
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top "><label>Add Background:</label> </div>

                                  <div class="addbgclas"><span class="texts">White Background</span><input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio"  checked="checked"value="whitebacground"></div>
								  <div class="addbgclas"><span class="texts">Profile Background</span><input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="profilebackground"></div>
								  <div class="addbgclas"><span class="texts">New Background</span> <input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="newbackground"></div>

						  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book Background
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
						
						 
					  </div>
					  <div id="rfbwp_fb_background_img" class="field  field-upload rfbwp-page-bg-image" style="display:none">
                                <div class="option">
									<div class="controls fcontrols">
										<span class="of_metabox_desc">Choose background image:</span>
                                            <div class="screenshot" id="rfbwp_fb_background_img">
											       
												<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-image.png" class="default">
											</div>
										<input id="rfbwp_fb_background_img" class="upload mp-input mp-input-border rfbwp-page-bg-image" type="text" name="rfbwp_fb_background_img" value="">
                                      <a id="upload_rfbwp_fb_background_img" class="upload_button mpc-button rfbwp-page-bg-image" ><i class="dashicons dashicons-format-image"></i> Upload Image</a>
									</div>
							   </div>
							   <div class="clear"></div>
						</div> 
						<div id="rfbwp_fb_ad_revenue_section" class="field  field-text-medium">
							<div class="option">
								<div class="controls fcontrols">
									<div class="description-top"><label>Ad Revenue Share:</label></div>									
									<span class="texts">Yes</span><input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox mp-input-medium mp-input-border" value="yes">
									<span class="texts">No</span><input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox mp-input-medium mp-input-border" value="no" checked="checked">
									<div class="revnue" style="display:none">
										<label for="revenue-paypal-email">Add Paypal Email</label>
										<input type="email" required name="revenue-paypal-email" class="revenue-paypal-email" id="revenue-paypal-email">
										<input type="button" value="Add" class="revenue-paypal">
										<div id="error-msg"></div>
										<div id="loading" style="display:none"><img id="loadingImage" src="<?php echo get_template_directory_uri();?>/images/ajax-loader.gif"></div>
										<table id="author-lists" class="author-lists"><tr><th></th><th><b>Users</b></th><th><b> % of Revenue</b></th></tr><tr><td></td><td><p><?php echo $current_user->display_name;?></p></td><td><input id="chnl-perc-val" type="text" value="100" readonly />%</td></tr></table>
									</div>
								</div>
								<div class="clear"></div>
							</div>
					   </div>
					  <div id="rfbwp_fb_privacy" class="field  field-text-medium">
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top "><label>Privacy Options: </label></div>
                                 <span class="texts">Private</span><input id="rfbwp_fb_privacy" class="mp-input-medium mp-input-border " name="rfbwp_fb_privacy" type="radio"  value="private">
								 <span class="texts">Public</span><input id="rfbwp_fb_privacy" class="mp-input-medium mp-input-border " name="rfbwp_fb_privacy" type="radio"  checked="checked" value="public">

				
                                   <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book privacy
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
					  </div>
					  
					  <div id="field-rfbwp_fb_book_publish" class="field  field-button rfbwp-book-publish revert">
                             <div class="option">
                                  <div class="controls fcontrols">
                                        <a class="rfbwp-book-publish revert mpc-button" href="#Publish">
                                       <i class="dashicons dashicons-edit"></i> Publish</a></div><div class="clear">
									   </div>
									   </div>
						</div>
					  </form>
					  <div class="navigatonbackforth">
		                 <input id="pre_btn3" class="pre_btn" type="button" value="Previous">
		
		               </div>
                 </div> 	
		</div> <!-- end bg-content -->

        <div id="rfbwp_page_preview">
			<div id="rfbwp_page_preview_wrap"></div>
			<div id="rfbwp_page_preview_close"><?php _e('Click anywhere to close.', 'rfbwp') ?></div>
		</div>
</div>
<style>

.navigatonbackforth{
	height: 35px;
    line-height: 35px;
    background: #333333;
    font-weight: bold;
    text-transform: capitalize;
	}
.next_btn{	
	float: right;
    background: #2997c3;
    border: none;
    height: 35px;
    color: #fff;
    padding-left: 20px;
  padding-right: 20px;
}

.pre_btn{	
	float: left;
    background: #2997c3;
    border: none;
    height: 35px;
    color: #fff;
    padding-left: 20px;
  padding-right: 20px;
}
#rfbwp_tools_toggle_content{ min-height: 330px; }
</style>
        <!-- end wrap -->
  <?php      //error_reporting(E_ALL && ~ E_NOTICE);
	
  
  
}
/*
function get_books_front_pages_table($bookID) {
	global $settings;
	global $rfbwp_shortname;
	$output = '';
	$fb_table = 'rfbwp_options';
	$fb_options = get_option( $fb_table );
	//$settings = rfbwp_get_settings();
	
	$settings = $fb_options;

	$output .= '<table class="fpages-table"><tbody>';

	$page_count = isset($settings['books'][$bookID]['pages']) ? count($settings['books'][$bookID]['pages']) : 0;
    
	$j = -1;
	
      $output .='<input id="totalpagecurrentbook" type="hidden" value="'. $page_count .'">';
	  
	for($i = 0; $i < $page_count; $i++) {
		$j++;

		$page_type  = isset($settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_type']) ? $settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_type'] : '';
		$page_index = isset($settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_index']) ? $settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_index'] : '';

		$output .= '<tr id="page-display_'.$j.'" class="display fpage-row-bg" >'
                        . '<td id="pimg'.$i.'" class="fthumb-preview page-img-td">';

		if(isset($settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image']) && $settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'] != '')
			$output .= '<img class="fb-dyn-images" data-src="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'" src="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'" alt="Image not found"/>';
		else
			$output .= '<div class="no-cover"></div>';

		$output .= '</td><td class="page-type-td">'
                        . '<div class="fpage-type">'.$page_type.'</div>';
               

		$output .= '<div class="fbtn-shadow-off">';
		//$output .= '<a class="add-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-plus"></i> <span class="tooltip">'. __('Add New Page', 'rfbwp' ) . '</span></a>';
		//$output .= '<a class="edit-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-edit"></i> <span class="tooltip">'. __('Edit Page', 'rfbwp' ) . '</span></a>';
		$output .= '<a class="preview-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-visibility"></i> <span class="tooltip">'. __('Page Preview', 'rfbwp' ) . '</span></a>';
		$output .= '<a class="delete-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-trash"></i> <span class="tooltip">'. __('Delete Page', 'rfbwp' ) . '</span></a>';
		
		$output .= '<a class="add-audio-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="fa fa-file-audio-o"></i> <span class="tooltip">'. __('Add Audio', 'rfbwp' ) . '</span></a>';
		
		$output .= '</div>';
                

		$output .= '</td>'
                        . '<td class="navigation page-btn-td">';

		$output .= '<a class="up-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-arrow-up-alt2"></i></a>';
		$output .= '<input type="checkbox" class="page-checkbox" />';
		$output .= '<span class="desc">page</span>';
		if($page_type != 'Double Page')
			$output .= '<span class="page-index">'.$page_index.'</span>';
		else
			$output .= '<span class="page-index">'.$page_index.' - '.(int)($page_index+1).'</span>';
		$output .= '<a class="down-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-arrow-down-alt2"></i></a>';

		$output .= '</td>'
                        . '<td class="mpc-sortable-handle page-arrow-td"><i class="fa fa-arrows-v"></i></td></tr>'
                        . '<tr id="pset_'.$j.'" class="page-set" style="display: none;">
				     <td collspan="3">
					     <div id="ps_'. $j.'" class="page-settings">
						     <div id="field-rfbwp_page_separator" class="field  field-separator">
                                 <div class="option">
                                     <div class="controls fcontrols"></div>
                                     <div class="description"></div>
                                     <div class="clear"></div>
								 </div>
							 </div>
						     <div class="stacked-fields" id="exceptaudio" data-section-id="field-rfbwp_fb_page_type"> 
							     <div id="field-rfbwp_fb_page_type" class="field  field-select rfbwp-page-type">
                                     <div class="option">
                                         <div class="controls fcontrols">
                                             <select class="mp-dropdown rfbwp-page-type" name="'.$settings['books'][$bookID]['pages'][$i]['rfbwp_fb_page_type'].'" id="rfbwp_fb_page_type">
											     <option selected="" value="Single Page">Single Page</option>
											 </select>
											 <div class="help-icon">
											     <span class="mp-tooltip right">Select page type:<br> - single - normal single page (left or right), <br> - double - one image displayed across both pages (left and right). </span>
											 </div>
										 </div>
									 </div>
								 </div>
								 <div id="field-rfbwp_fb_page_preview" class="field right  field-button rfbwp-page-preview">
                                     <div class="option">
                                         <div class="controls fcontrols">
                                             <a class="rfbwp-page-preview mpc-button" href="#0">
											     <i class="dashicons preview"></i> Page Preview
												 <span class="tooltip">Page Preview</span>
											 </a>
										 </div>
										 <div class="clear"></div>
									 </div>
								 </div>
							 </div>
							 <div id="exceptaudio" class="stacked-fields no-border">
							     <div class="help-icon">
								     <span class="mp-tooltip right">Specify pages background image.</span>
								 </div>
								 <div id="field-rfbwp_fb_page_bg_image" class="field left  field-upload rfbwp-page-bg-image col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                                     <div class="option">
                                         <div class="controls fcontrols">
                                             <span class="of_metabox_desc">Choose page background:</span>
                                             <div class="help-icon">
											     <span class="mp-tooltip right">Specify pages background image.</span>
											 </div>
											 <div class="screenshot" id="rfbwp_fb_page_bg_image_image">
											     <img src="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'" data-src="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'" alt="" class="mp-image-border fb-dyn-images">
												 <img src="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'" style="display:none;" class="default">
											 </div>
											 <input id="rfbwp_fb_page_bg_image" class="upload has-file mp-input mp-input-border rfbwp-page-bg-image" type="text" name="'.$settings['books'][$bookID]['pages'][$i]['rfbwp_fb_page_bg_image'].'" value="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'">
                                             <a id="upload_rfbwp_fb_page_bg_image" class="upload_button mpc-button rfbwp-page-bg-image" rel="1546">
											     <i class="dashicons dashicons-format-image"></i> Upload Image
											 </a>
										 </div>
									 </div>
								 </div>
								 <div id="field-rfbwp_fb_page_bg_image_zoom" class="field right  field-upload rfbwp-page-bg-image-zoom col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                                     <div class="option">
                                         <div class="controls fcontrols">
                                             <span class="of_metabox_desc">Choose page hi-res background:</span>
                                             <div class="help-icon">
											     <span class="mp-tooltip top">Specify hi-res page background images, it is used by the zoom feature (optional).</span>
											 </div>
										     <div class="screenshot" id="rfbwp_fb_page_bg_image_zoom_image">
											     <img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-zoom-image.png" class="default">
											 </div>
											 <input id="rfbwp_fb_page_bg_image_zoom" class="upload mp-input mp-input-border rfbwp-page-bg-image-zoom" type="text" name="'.$settings['books'][$bookID]['pages'][$i]['rfbwp_fb_page_bg_image_zoom'].'" value="">
                                             <a id="upload_rfbwp_fb_page_bg_image_zoom" class="upload_button mpc-button rfbwp-page-bg-image-zoom" rel="1546">
											     <i class="dashicons dashicons-format-image"></i> Upload Image
											 </a>
										 </div>
										 <div class="clear"></div>
									 </div>
							     </div>
							 </div>
							 <div id="field-rfbwp_fb_page_bg_audio_file" class="field  field-upload rfbwp-page-bg-audio-file">
                                 <div class="option">
                                     <div class="controls fcontrols">
                                         <span class="of_metabox_desc">Upload Audio:</span>
                                         <input style="width:100%" id="rfbwp_fb_page_bg_audio_file1'. $i .'" class="upload mp-input mp-input-border rfbwp-page-bg-audio-file pageaudio'. $i .'" type="text" name="'.$settings['books'][$bookID]['pages'][$i]['rfbwp_fb_page_bg_audio_file'].'" value="">
                                         <a id="upload_rfbwp_fb_page_bg_audio_file" class="upload_button mpc-button rfbwp-page-bg-audio-file" data-audio-id="'. $i .'" rel="1546">
										     <i class="dashicons dashicons-upload"></i>Add Audio
										 </a>
										 
									 </div>
									 <!--<div id="progress-wrp"><div class="progress-bar"></div ><div class="status"></div></div>-->
									 <div class="clear"></div>
								 </div>
							 </div>
							 
							 <div id="field-rfbwp_fb_page_save" class="field  field-button rfbwp-page-save revert">
                                 <div class="option">
                                     <div class="controls fcontrols">
                                         <a style="color:#fff" class="rfbwp-audio-save revert mpc-button" href="#'. $i.'">
										     <i class="dashicons dashicons-edit"></i> Save Audio
										 </a>
									 </div>
								     <div class="clear"></div>
								 </div>
							 </div>
						 </div>
					 </td>
				 </tr>';
	}

	if($page_count == 0) {
		$output .= '<tr id="pset_0" class="page-set"><td collspan="3"></td></tr>';
	}

	$output .= '</tbody></table>';

	return $output;
}*/

function get_books_front_pages_table($bookID) {
	global $settings, $wpdb;
	 
	$output = '';
	 
	 $sql = '
			SELECT *
			FROM wp_book_page
			WHERE   book ='. $bookID; 
		
		
		$totatpage_books = $wpdb->get_results( $sql ); 

	$output .= '<table class="fpages-table"><tbody>';

	 
     $page_count = $wpdb->query($sql);
	$j = 0;
	$i = 0;
	$page_index = 1;
      $output .='<input id="totalpagecurrentbook" type="hidden" value="'. $page_count .'">';
	 foreach($totatpage_books as $totatpage_book ) {
	 
		$j++;
             
		$page_type  = 'Single Page';

		$output .= '<tr id="page-display_'.$j.'" class="display fpage-row-bg" >'
                        . '<td id="pimg'.$i.'" class="fthumb-preview page-img-td">';

		 
			$output .= '<img class="fb-dyn-images" data-src="'. $totatpage_book->fbpagebgimage .'" src="'. $totatpage_book->fbpagebgimage .'" alt="Image not found"/>';
	

		$output .= '</td><td class="page-type-td">'
                        . '<div class="fpage-type">'.$page_type.'</div>';
               

		$output .= '<div class="fbtn-shadow-off">';
		$output .= '<a class="preview-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-visibility"></i> <span class="tooltip">'. __('Page Preview', 'rfbwp' ) . '</span></a>';
		$output .= '<a class="delete-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-trash"></i> <span class="tooltip">'. __('Delete Page', 'rfbwp' ) . '</span></a>';
		
		$output .= '<a class="add-audio-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="fa fa-file-audio-o"></i> <span class="tooltip">'. __('Add Audio', 'rfbwp' ) . '</span></a>';
		
		$output .= '</div>';
                

		$output .= '</td>'
                        . '<td class="navigation page-btn-td">';

		$output .= '<a class="up-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-arrow-up-alt2"></i></a>';
		$output .= '<input type="checkbox" class="page-checkbox" />';
		$output .= '<span class="desc">page</span>';
		if($page_type != 'Double Page')
			$output .= '<span class="page-index">'.$page_index.'</span>';
		else
			$output .= '<span class="page-index">'.$page_index.' - '.(int)($page_index+1).'</span>';
		$output .= '<a class="down-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-arrow-down-alt2"></i></a>';

		$output .= '</td>'
                        . '<td class="mpc-sortable-handle page-arrow-td"><i class="fa fa-arrows-v"></i></td></tr>'
                        . '<tr id="pset_'.$j.'" class="page-set" style="display: none;">
				     <td collspan="3">
					     <div id="ps_'. $j.'" class="page-settings">
						     <div id="field-rfbwp_page_separator" class="field  field-separator">
                                 <div class="option">
                                     <div class="controls fcontrols"></div>
                                     <div class="description"></div>
                                     <div class="clear"></div>
								 </div>
							 </div>
						      
							 <div id="field-rfbwp_fb_page_bg_audio_file" class="field  field-upload rfbwp-page-bg-audio-file">
                                 <div class="option">
                                     <div class="controls fcontrols">
                                         <span class="of_metabox_desc">Upload Audio:</span>
                                         <input style="width:100%" id="rfbwp_fb_page_bg_audio_file'. $j .'" class="upload mp-input mp-input-border rfbwp-page-bg-audio-file pageaudio'. $j .'" type="text" name="rfbwp_fb_page_bg_audio_file" value="'. $totatpage_book->audiofile .'">
                                         <a id="upload_rfbwp_fb_page_bg_audio_file" class="upload_button mpc-button rfbwp-page-bg-audio-file" data-audio-id="'. $j .'" rel="1546">
										     <i class="dashicons dashicons-upload"></i>Add Audio
										 </a>
										 
									 </div>
									 <!--<div id="progress-wrp"><div class="progress-bar"></div ><div class="status"></div></div>-->
									 <div class="clear"></div>
								 </div>
							 </div>
							 
							 <div id="field-rfbwp_fb_page_save" class="field  field-button rfbwp-page-save revert">
                                 <div class="option">
                                     <div class="controls fcontrols">
                                         <a style="color:#fff" class="rfbwp-audio-save revert mpc-button" href="#'. $j.'">
										     <i class="dashicons dashicons-edit"></i> Save Audio
										 </a>
									 </div>
								     <div class="clear"></div>
								 </div>
							 </div>
						 </div>
					 </td>
				 </tr>';
				 $i++;
				 $page_index++;
	}

	if($page_count == 0) {
		$output .= '<tr id="pset_0" class="page-set"><td collspan="3"></td></tr>';
	}

	$output .= '</tbody></table>';

	return $output;
}
function rfbwp_get_books(){
global $wpdb, $paged;	
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
echo get_query_var( 'author_name' );
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
echo $profile_id = $author->ID;
	          $args = array(
			        'post_type' => 'dyn_book',
					'paged' => $paged,
					'post_status' => 'publish',
					'post_author' => $author_id_nowsss,
					'posts_per_page' => 10
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
				
				echo $postID = the_title() ."<br>";
				
				endwhile;
				echo paginate_links(array('add_args' => array('display_page' => 'newbook')));
				endif;
				wp_reset_postdata();
}

function rfbwp_get_books_OLD(){
	//error_reporting(-1);
$cat=isset($_GET['cat_type']) ? $_GET['cat_type'] : '';
global $wpdb, $paged;
$authorss = get_user_by( 'slug', get_query_var( 'author_name' ) );
$post_author_name = get_query_var( 'author_name' );
echo $author_id_nowsss = $authorss->ID;
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
if($cat=='new-books'){
    $bkstart = ($paged == 1) ? 0 : intval($paged-1) * 12;    
}
else $bkstart=0;
//create custom loop for books
if(!is_user_logged_in()){   
   /* if(isset($_GET['sortbook']) && $_GET['sortbook'] != ""){
        if($_GET['sortbook'] == 'endorsements'){
            $endorsements = true;
            $bktemp = "SELECT p.*,endocount FROM wp_posts p left join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'dyn_book' AND p.post_status = 'publish'  order by endors.endocount desc ";   
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12'; 
        }elseif ($_GET['sortbook'] == 'views') {
            $views = true;
            $bktemp= 'SELECT *  FROM wp_posts AS p JOIN wp_postmeta AS meta WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID = meta.post_id AND meta.meta_key="popularity_count" ORDER BY meta.meta_value' ;
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
        }elseif ($_GET['sortbook'] == 'favourites') {
            $favourites = true;
            $bktemp = "SELECT p.*,favcount FROM wp_posts p left join (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id where p.post_type = 'dyn_book' AND p.post_status = 'publish'  order by favorite.favcount desc";
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
        }elseif ($_GET['sortbook'] == 'comments') {
            $comments = true;
            $bktemp = "SELECT * FROM wp_posts where post_type = 'dyn_book' AND post_status = 'publish' order by comment_count ";  
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12'; 
        }    
    }else{
        $bktemp= 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC' ;
        $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
    }
    $bkresult = $wpdb->get_results( $bkquery ); 
    $bktotal=$wpdb->query($bktemp);*/
}else if(is_user_logged_in()){
    if(isset($_GET['sortbook']) && $_GET['sortbook'] != ""){
        if($_GET['sortbook'] == 'endorsements'){
            $endorsements = true;        
            $bktemp = 'SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='. $author_id_now .'
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id WHERE p.post_type="dyn_book"  AND p.post_status="publish" AND p.ID IN (SELECT post_id
            FROM wp_postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'. $author_id_now .'[[:>:]]") ORDER BY CAST(endocount AS SIGNED INTEGER ) DESC';
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
        }elseif ($_GET['sortbook'] == 'views') {
            $views = true;
            $bktemp = 'SELECT * FROM wp_posts AS p INNER JOIN wp_postmeta AS meta ON p.ID=meta.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN wp_postmeta AS meta ON p.ID=meta.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='. $author_id_now .'
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN wp_postmeta AS meta ON p.ID=meta.post_id WHERE p.post_type="dyn_book"  AND p.post_status="publish" AND p.ID IN (SELECT post_id
            FROM wp_postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'. $author_id_now .'[[:>:]]") AND meta.meta_key="popularity_count" ORDER BY CAST(meta_value AS SIGNED INTEGER ) DESC ';
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';

        }elseif ($_GET['sortbook'] == 'favourites') {
            $favourites = true;
            $bktemp = 'SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.$author_id_now.'
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id WHERE p.post_type="dyn_book"  AND p.post_status="publish" AND p.ID IN (SELECT post_id
            FROM wp_postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'. $author_id_now .'[[:>:]]") ORDER BY CAST(favcount AS SIGNED INTEGER ) DESC';
            
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';

        }elseif ($_GET['sortbook'] == 'comments') {
            $comments = true;
            $bktemp = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION
            SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.$author_id_now.'
            UNION
            SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book"  AND post_status="publish" AND ID IN (SELECT post_id
            FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'. $author_id_now .'[[:>:]]") ORDER BY  comment_count  DESC ';
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12'; 
        }    
    }else{
       // $bktempss = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND post_author='.$author_id_nowsss.'DESC ';
        
       // $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 50';
   // 
   $data_result = 'SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_author ='.$author_id_nowsss;
   
   
   }
   
    $bkresult = $wpdb->get_results( $data_result ); 
   $bktotal=$wpdb->query( $data_result );
 
}

?>

<div class="list-group" id="new-book-tab">
<?php echo '<a href="#new-books" id="new-books"><h5><b>NEW BOOKS</b></h5></a>'; ?>
<!-- Sort block -->	
<?php echo get_query_var( 'author_name' ); ?>
<div class="alert alert-success v-d" style="display:none;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> book is Deleted.
	</div>
<div class="cover-twoblocks">
    <div class="sortvideo-div">
        <form method="get" id="sortbook-form">
            <p><label>Currently Sorted By : </label>
                <select class="sortbook" name="sortbook">
                    <option value="recent" <?= ($selected)?'selected':'';?>>Recent</option>
                    <option value="views" <?= ($views)?'selected':'';?>>Views</option>  
                    <option value="endorsements" <?= ($endorsements)?'selected':'';?>>Endorsements</option>
                    <option value="favourites" <?= ($favourites)?'selected':'';?>>Favorites</option>
                    <option value="comments" <?= ($comments)?'selected':'';?>>comments</option>
                </select>
            </p>
        </form>
    </div>
    
    <div class="well well-sm">
        <strong>Views</strong>
        <div class="btn-group">
            <a href="" id="list-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List
            </a>
            <a href="" id="grid-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
</div>

	<div class="row">				 
<?php 
if($bkresult)
{
$bkri=0;
$dyn_book = array();
foreach ($bkresult as $bkdata){
	$dyn_book[] = $row->ID;
}
$args = array(
					'post__in' => $dyn_book,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
				
    $postID = get_the_ID();
    $bkdata = get_post($postID);	
    $post = get_post($postID);
    $privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
    $selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
    $post_author     = $bkdata->post_author;
    $user_ID         = get_current_user_id(); 
    $selectUsersList = explode( ",", $selectUsersList[0] ); 
	$refr = get_post_meta($postID,'video_options_refr',true);
	if($refr){}else{$refr= 'No reference given'; }
    $thumbnail_id = get_post_thumbnail_id($postID);
    $thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
    $imgsrc= $thumbnail_url [0];
    $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");
    $endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
    $endorsnum=count($endors);
    $favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
    $favsnum=count($favs);
    $book_author=get_the_author_meta('display_name', $bkdata->post_author);
    //echo "privacy".$privacyOption[0].is_user_logged_in();             
        if(!is_user_logged_in()) {  // case where user is not logged in 
           if(isset($privacyOption[0]) and $privacyOption[0] == "public") {            
			  //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
			    ?>
					<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">
		                 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">
					   	   <div class="image-holder">
						   <a href="<?php echo get_permalink($postID); ?>">
						   <div class="hover-item"></div>
						   </a>    	
								<?php							
							    if( has_post_thumbnail($postID) ){
                                    echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
                                } else {
                                    echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                } ?>											
						   </div>
								<div class="layout-title-box text-center" style="display:block;">
								<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
								<?php
								preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);
								if ( count($matches[0]) && count($matches[1]) >4){
								?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
								<?php 
									}
								else if(strlen($bkdata->post_title)>30){ 
							   	?>
							    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

							   <?php
							    }
								else {
										?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

								<?php } ?>
								</h6>
								<!-- <ul class="stats">
									<li ><?php videopress_displayviews( $postID ); ?></li>
									<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
									<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
									
								</ul>
								-->
                                <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                    <div class="video_excerpt_text one">
                                        <h4 class="tittle-h4">Description</h4>
                                        <div class="inline-blockright"><?php 
                                            if($bkdata->post_content ==""){echo "No description available.";}else{
                                                echo $bkdata->post_content;
                                            } 
                                        ?></div>
                                    </div>                  
                                    <ul class="stats">
                                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                        <li><?php videopress_countviews($postID); ?></li>
                                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                        <li><?php comments_number(); ?></li>
                                    </ul>                  
                                </div>

								<ul class="bottom-detailsul" style="vertical-align:baseline;">
								 <li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
								<i class="fa fa-info-circle" aria-hidden="true"></i> Details
								</a>
								</li>
							    </ul>
							   </div>
							   
						 </div>
					   </div>		
			   <?php
			    $bkri++;
		  }
		  else
		  {
			    if(!isset($privacyOption[0]))
				{
					 //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
			    ?>
					<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">
		                <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">
					   	   <div class="image-holder">
						   <a href="<?php echo get_permalink($postID); ?>">
						   <div class="hover-item"></div>
						   </a>            
								<?php							
							   if( has_post_thumbnail($postID) ){
									echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
								}else{
										echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
								}?>											
						   </div>
								<div class="layout-title-box text-center" style="display:block;">
								<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
								<?php
								preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

								if ( count($matches[0]) && count($matches[1]) >4){;
								?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
								<?php 
									}
								else if(strlen($bkdata->post_title)>30){ 
							   	?>
							    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

							   <?php
							    }
								else {
										?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

								<?php } ?>
								</h6>
								<!-- <ul class="stats">
									<li ><?php videopress_displayviews( $postID ); ?></li>
									<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
									<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
									
								</ul>
								-->
                                <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                    <div class="video_excerpt_text one">
                                        <h4 class="tittle-h4">Description</h4>
                                        <div class="inline-blockright"><?php 
                                            if($bkdata->post_content ==""){echo "No description available.";}else{
                                                echo $bkdata->post_content;
                                            } 
                                        ?></div>
                                    </div>                     
                                    <ul class="stats">
                                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                        <li><?php videopress_countviews($postID); ?></li>
                                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                        <li><?php comments_number(); ?></li>
                                    </ul>                  
                                </div>
								<ul class="bottom-detailsul" style="vertical-align:baseline;">
								
								<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
								<i class="fa fa-info-circle" aria-hidden="true"></i> Details
								</a>
								</li>
							    
							   </ul>
							   </div>
							   
						 </div>
					   </div>		
			   <?php
			    $bkri++;
				}
		  }
		}

		
		if( $author_id_nowsss == $user_IDsss ) {  // case where user is logged in
			 if($post_author == $user_ID)	
			 {    // Case where logged in User is same as Video Author User
		         // if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center <?php echo $postID; ?>" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                    
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a href="#" class="del-booksid" data-id="<?php echo $postID; ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php echo $postID; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
			 }	
			 else
			 {    // Case where logged in User is not same as Video Author User                
				  if(isset($privacyOption[0]) and $privacyOption[0] == "public")
				  {

					  //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>   		
										<?php							
									    if( has_post_thumbnail($postID) ){
											echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
										}else{
												echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
										}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                    
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									  
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
				  }
				  else
				  {
					  if(!isset($privacyOption[0])) {
						  // if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                   
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										 
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
					  } else  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										 //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">
    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">	
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										  if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
										}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                     
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										 
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
									 }
									 else
									 {  // case where user is not in access list
									 	 
									 }
							   }
							   else
							   {  }
					  }	 
				  }
			 }	
		} //end else user not login  
		
		if( $author_id_nowsss != $$user_ID  )
		{
			?>
			<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>   		
										<?php							
									    if( has_post_thumbnail($postID) ){
											echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
										}else{
												echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
										}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                    
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									  
									   </ul>
									   </div>
									   
								 </div>
							   </div>
			<?php
		}
        ?>  
        
        <!-- Modal box detail for books --> 
        <div class="modal-box" id="myModal-<?php echo $postID; ?>">                  
            <div class="modal-body">
                <a class="js-modal-close close">×</a>
                <div class="layout-2-details layout-2-details-<?php echo $postID; ?>">                        
                    <img class="center-block bimg-<?php echo $postID; ?>" width="150"/>    
                    <h6 class="layout-title">  
                    <?php
                        preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                        if ( count($matches[0]) && count($matches[1]) >4){;
                        ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                        <?php 
                        }
                        else if(strlen($bkdata->post_title)>30){ 
                        ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                       <?php
                        }
                        else {
                                ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                        <?php } ?>              
                    </h6>
                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                    <div class="video_excerpt_text one"><h4 class="tittle-h4 ">Description</h4>
                        <div class="inline-blockright">
                            <?php if($bkdata->post_content ==""){
                                echo "No description available.";}else{
                                echo $bkdata->post_content;
                            }?>
                        </div>
                    </div>                                          
                    <ul class="stats">
                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                        <li><?php videopress_countviews($postID); ?></li>
                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                        <li><?php comments_number(); ?></li>
                    </ul> 
                    <div class="clear"></div>
                </div>
            </div>
        </div> 
<div class="modal-box" id="myModalPrivacy-<?php echo $postID; ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php echo $postID; ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php echo $postID; ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) and $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php echo $postID; ?>" name="privacy-option-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php echo $postID; ?>" name="privacy-option-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php echo $postID; ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php echo $postID; ?>" name="privacy-option-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php echo $postID; ?>" name="privacy-option2-<?php echo $postID; ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php echo $postID; ?>">
													<?php
													  echo '<select id="tokenize-'.$postID.'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo $postID; ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php echo $postID; ?>">
													<?php
													  echo '<select id="tokenize-'.$postID.'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo $postID; ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php echo $postID; ?>">
												<?php
												  echo '<select id="tokenize-'.$postID.'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo $postID; ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php echo $postID; ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php echo $postID; ?>" name="save-privacy-option-<?php echo $postID; ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo $postID; ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php echo $postID; ?>").click(function(){
							 $("#privacy-radio1-<?php echo $postID; ?>").prop( "checked", false );
						     $(".select-box-<?php echo $postID; ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php echo $postID; ?>").click(function(){
							 $("#privacy-radio-<?php echo $postID; ?>").prop( "checked", false );
						     $(".select-box-<?php echo $postID; ?>").slideUp();
							 $('select#select_users_list-<?php echo $postID; ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php echo $postID; ?>").click(function(){
							 $('#msgLoader-<?php echo $postID; ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php echo $postID; ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php echo $postID; ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php echo $postID; ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php echo $postID; ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php echo $postID; ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php echo $postID; ?>, #privacy-radio-<?php echo $postID; ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php echo $postID; ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>		
        <?php
       // }//end for
	   
	   endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_video')))."</div>";
				endif;
				wp_reset_postdata();

      }else{
          ?>
          <p class="empty-book">No book available</p>
          <?php  
      }
      //end if      
      ?>
      </div>
      <div class="row" data-anchor="new-books">
        <div class="col-sm-12">
            <nav class="pull-left" id="book-pagination">        
            <?php
            if($cat=='new-books'){
        $pcurrent=max( 1, get_query_var('page') );
        }
        else{
         $pcurrent=1;   
        }
        
        $total = 0;
        $total_round = round($bktotal/12);
        $non_total = $bktotal/12;
        if($total_round < $non_total){
            $total = $non_total + 1;
        }


 $paginate=paginate_links( array(   
    'format' => '?page=%#%',
    'current' => $pcurrent,
    'type' => 'array',
    'end_size' =>1,
    'mid_size'=>4,
    'total' => $total
           ) );

$pcount=count($paginate);


 $maxp=ceil(($bktotal/12)/2);
 
if( $pcurrent<= ceil($bktotal / 12)  )
{ 
 if( ceil($bktotal / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($bktotal / 12) >= 5 ){


 if ($pcurrent<=6)
 {
    if ($pcurrent>2)
    {echo $paginate[0].'&nbsp;';}
      if($paginate[$pcurrent-2]){
    echo $paginate[$pcurrent-2].'&nbsp;';
       }
    echo $paginate[$pcurrent-1].'&nbsp;';
    echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
    echo $paginate[5].'&nbsp;';
    echo $paginate[6].'&nbsp;';
    echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($bktotal/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}
                  
            ?>
            </nav>
        </div>
    </div>    
    <script type="text/javascript">
        $("#new_books .page-numbers").each(function(){
            /*FA: replaced this

            var href=$(this).attr("href");
            if(typeof(href) !== "undefined"){
                parts = href.split('/');                
                lastPart = parts.pop() == '' ? parts[parts.length - 1] : parts.pop();
                if(lastPart.length >= 4){                    
                    var pos=href.indexOf("?");       
                    var anchor=$(this).parents(".row").eq(0).data("anchor");                     
                    var url=window.location.href;
                    if(url.indexOf("cat_type") == -1){
                        if(url.indexOf("?") != -1){
                            href += "&cat_type=" + anchor;   
                            var sorturl = url.split("?");
                            if(url.indexOf("sortbook") == -1){
                               href += "&sortbook=" + sorturl[1];
                            }                                                  
                            $(this).attr("href", href);
                        }else{
                            href += "&cat_type=" + anchor;                         
                            $(this).attr("href", href);     
                        }
                    }                    
                }else{
                    href2 = href.substring(0,href.lastIndexOf("/"));
                    $(this).attr("href", href2); 
                }
            }
            */   

         var href=$(this).attr("href");
     if(href){
       var vsorturl='sortbook=';
       var patt = /sortbook/g;
       
       var pos=href.indexOf("?");
       if(pos>0){       
        var vparts=href.split("?")[1].split("&");
          vparts.forEach(function(val,indx){
            if(patt.test(val)) {
                vsorturl=val;               
             }          
          });
        }
       href=href.substring(0,pos);
       //var pgn = "<?php echo get_query_var('page'); ?>";
       var pgn=$("#new_books span.page-numbers.current").text();
      //var anchor=$(this).parents(".row").eq(0).data("anchor"); 
      if($(this).text()=="Next »")        
      $(this).attr("href", href + "?cat_type=new-books&"+vsorturl+"&page=" + eval( Number(pgn) + 1) );
      else if ($(this).text()=="« Previous")
      $(this).attr("href", href + "?cat_type=new-books&"+vsorturl+"&page=" + eval(pgn-1));
       else
      $(this).attr("href", href + "?cat_type=new-books&"+vsorturl+"&page=" + $(this).text());
    }               
 });
       
        
        // show detail popup
        $("a.detailsblock-btn").on("click", function(){
            var id = $(this).attr("data-modal-id");
            if(id){                
                var image = $(this).attr("data-image");                
                $(".bimg-"+id).attr("src",image);
                $("#myModal-"+id).fadeIn(); 
            }
                                
        });
        // list and grid view
        $('#new-book-tab .bookitem').addClass('grid-group-item');   
        $("#grid-book").click(function(event){
            event.preventDefault();
            $('#new-book-tab .bookitem').removeClass('list-group-item changeStyleList').addClass('grid-group-item changeStyleGrid');
            $('.layout-title-box .layout-2-details').fadeOut();
            $('.detailsblock-btn').show();
        });
        $("#list-book").click(function(event){
            event.preventDefault();
            $('#new-book-tab .bookitem').removeClass('grid-group-item changeStyleGrid').addClass('list-group-item changeStyleList');
            $('.layout-title-box .layout-2-details').fadeIn();
            $('.detailsblock-btn').show();
			$(".bookitem").attr("style",'padding: 5px 5px 5px 5px !important; margin:0px !important; height:330px;');
        });
        //end list/grid view
        // sort book
        $(".sortbook").on("change", function(){
            this.form.submit();
        });
        
    </script>
	<script>
		jQuery(document).ready(function(){
			var $ = jQuery.noConflict();

			$('.del-booksid').click(function(e){
				e.preventDefault();

				console.log($(this).attr('data-id'));
				var confrm = confirm("Are you sure you want to continue!");
				var book_id = $(this).attr('data-id');
				var user_id = '<?= get_current_user_id;?>';

				if(confrm){
					$.ajax({
						type: "POST",
						url: ajaxurl,
						data: { action:'user_book_delete_book','book_id': book_id},
						success : function(data){
							$('.'+book_id).remove();
							$('.alert-success').attr('style','display:block');
						}
					});
				}
			})

		})

	</script>
					<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>



	 
    <?php
    }//end function
    
  
?>