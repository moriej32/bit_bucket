<?php
function search_file_display( $postID, $viewstyle  ){
	 global $wpdb;
		 ob_start();
		 //error_reporting(-1);
		 $output = '';
		 $postdata = get_post( $postID  );
		 $uploaded_imgssss = get_post_meta( $postID, 'dyn_thumbaniimg_src', true );
		  $show = '';
		 if($viewstyle == "list"){
			$viewstyle = "search-content"; 
			$show = "hidden";
		 }
		 elseif($viewstyle == "grid"){
			 $viewstyle = "item2  col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center  group-search-grid";
             $show = "show";			 
		 }
		 ?>
         
		<!-- Start Layout Wrapper -->
		<div id="post-<?php echo $postID; ?>" class="searchallcontent <?php echo $viewstyle; ?>">
   		 <div class="layout-2-wrapper solid-bg">

       		 <div class="grid-6 first">
			     <?php if(!empty($uploaded_imgssss) & $uploaded_imgssss != 'default'){ ?>
				 <a href="<?php echo get_permalink($postID); ?>">
				  <div  class="image-holder" style="background: url('http://www.doityourselfnation.org/bit_bucket/<?php echo $uploaded_imgssss;?>');height: 128px; background-repeat: no-repeat;background-position: center;">
				   
               		
				  </div> </a>
				 <?php } else{ ?>
				  <a href="<?php echo get_permalink($postID); ?>">
         		   <div class="image-holder dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', $postID); ?>">
              		 
           		 </div></a>
				  <?php } ?>
        	</div>

       		 <div class="layout-2-details">
           	 <h3 class="layout-title"><a href="<?php echo get_permalink( $postID ); ?>" title="<?php echo $postdata->post_title; ?>"><?php echo $postdata->post_title; ?></a></h3>
            	<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', $postID ); ?></div>
            	<?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
            	<?php
					 
				              $descriptions = strip_tags($postdata->post_content);
					
					        if($descriptions != ''){
				?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo $descriptions; ?></div></div>
				<?php }else{ ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright">No Description available</div></div>
				<?php } ?>
           		 <ul class="stats">
               		 <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                		<li><?php videopress_displayviews( get_the_ID() ); ?></li>
                		<li><i class="fa fa-wrench"></i>
                	<?php
                    $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID );
                    echo count($result); 
                	?> Endorsments</li>
               		 <li><i class="fa  fa-heart"></i> 
               		 <?php 
                	  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID;
                	  $result1 = $wpdb->get_results($sql_aux);
                	  echo count($result1); 
                	?> Favorites</li>
               	 <!--end favorite-->
                	<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', $postID , "post"); ?></li>
                	<li><?php comments_number() ?></li>
                	<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', $postID ); ?></li>
            	</ul>
           	  <div class="clear"></div>
        	</div>

          		<?php
              	  $refr = get_post_meta($postID,'video_options_refr',true);
              	  if($refr){}
               	 else{$refr= 'No reference given'; }

                $ftype=apply_filters( 'dyn_file_type_by_mime', $postID );

                $imgClass = apply_filters('dyn_class_by_mime', $postID);

                $download_count=get_post_meta($postID, 'dyn_download_count', true);

                $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post"); 
      		  ?>

       		 <div class="clear"></div>

       		 <ul class="bottom-detailsul <?php echo $show; ?>">
             <li><h3 class="layout-title"><a href="<?php echo get_permalink( $postID ); ?>" title="<?php echo $postdata->post_title; ?>"><?php echo $postdata->post_title; ?></a></h3>
     		</li>			 
       		  <li>
           		 <a class="detailsblock-btn"  data-postID="<?php echo $postID; ?>" data-modal-id="docs" data-author="<?php echo get_the_author_meta( 'display_name', $postdata->post_author ); ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $postdata->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($postdata->post_content); ?>" data-image="" data-comments="<?php echo $postdata->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo count($result); ?>" data-favr="<?php echo count($result1); ?> " data-revs="<?php echo $revnum ?>">
          	 	 <i class="fa fa-info-circle" aria-hidden="true"></i> Details
          		 </a>
        	 </li>
        	</ul>
   		 </div>
	</div>

		 <?php
		  $output = ob_get_contents();
	 
		ob_end_clean();
		return $output;
	} //end of product video   
?>	
