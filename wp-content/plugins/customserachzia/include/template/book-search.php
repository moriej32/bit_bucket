<?php
function search_book_display( $postID, $viewstyle  ){
	 global $wpdb;
		 ob_start();
		// error_reporting(-1);
		 $output = '';
		 $postdata = get_post( $postID  );
		 $uploaded_imgssss = get_post_meta( $postID, 'dyn_thumbaniimg_src', true );
		  $show = '';
		 if($viewstyle == "list"){
			$viewstyle = "search-content"; 
			$show = "hidden";
		 }
		 elseif($viewstyle == "grid"){
			 $viewstyle = "item2  col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center  group-search-grid";
             $show = "show";			 
		 }
		 ?>
         
		<!-- Start Layout Wrapper -->
		<div id="post-<?php echo $postID; ?>" class="searchallcontent <?php echo $viewstyle; ?>">
  		  <div class="layout-2-wrapper solid-bg">

       		 <div class="grid-6 first">
			          <div class="galleryOverlay" style="display:none;">
								   <?php 
								   
								       $bookshortecode = get_post_meta( $postID, 'bookshortcode', true );
										 
										echo $valuetest =  do_shortcode( '[responsive-flipbooksss id="'. $bookshortecode .'"]' );
										
                                         
                                            ?>
											
				 </div>
            		<div class="image-holder galleryImgCont">
						<a href="<?php echo get_permalink( $postID ); ?>">
						 
						</a>
						               <?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:100%;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:100%;">';
											}?>
            
            </div>
       		 </div>

        	<div class="layout-2-details">
            <h3 class="layout-title"><a href="<?php echo get_permalink( $postID ); ?>" title="<?php echo $postdata->post_title; ?>"><?php echo $postdata->post_title; ?></a></h3>
            
            <?php echo apply_filters( 'dyn_display_review', $output, $postID , "post" ); ?>
			<?php
					 
				              $descriptions = strip_tags($postdata->post_content);
					
					        if($descriptions != ''){ ?>
            <div class="video_excerpt_text"><?php echo $descriptions; ?></div>
							<?php } else { ?>
			<div class="video_excerpt_text"><h4 class="tittle-h4">Description</h4><div class="inline-blockright">No Description available</div></div>
							
							<?php } ?>
            	<ul class="stats">
              	    <li><?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?></li>
               	 	<li><?php videopress_displayviews( $postID ); ?></li>
               	 	<li><i class="fa fa-wrench"></i>
              		<?php
                    $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postID );
                    echo count($result); 
                	?> Endorsments</li>
                	<li><i class="fa  fa-heart"></i> 
               		<?php 
                 	 $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postID;
                 	 $result1 = $wpdb->get_results($sql_aux);
                  	echo count($result1); 
               		 ?> Favorites</li>
               		 <!--end favorite-->
               		 <li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', $postID, "post"); ?></li>
                	<li><?php comments_number() ?></li>
            	</ul>
            <div class="clear"></div>
          	</div>
        	 <?php
			 
                $book_author = get_the_author_meta( 'display_name', $postdata->post_author );

                $refr = get_post_meta($postID,'video_options_refr',true);
                 if($refr){}
                 else{$refr= 'No reference given'; }

                $thumbnail_id=get_post_thumbnail_id($postID);
                $thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
                $imgsrc= $thumbnail_url [0];

                $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");
         ?>


       		 	<div class="clear"></div>

        		<ul class="bottom-detailsul <?php echo $show; ?>">
				<li><h3 class="layout-title"><a href="<?php echo get_permalink( $postID ); ?>" title="<?php echo $postdata->post_title; ?>"><?php echo $postdata->post_title; ?></a></h3>
     		</li>
        			 <li>
          			<a class="detailsblock-btn" data-postID="<?php echo $postID; ?>" data-modal-id="videosModal" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $postdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($postdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $postdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo count($result); ?>" data-favr="<?php echo count($result1); ?> " data-revs="<?php echo $revnum ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a>
         			</li>
       		 	</ul>
    		</div>
		</div>

		 <?php
		  $output = ob_get_contents();
	 
		ob_end_clean();
		return $output;
	} //end of product video   
?>	
