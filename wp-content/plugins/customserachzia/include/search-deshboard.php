<?php
require_once( plugin_dir_path( __FILE__ ) . 'template/product-search.php' );
require_once( plugin_dir_path( __FILE__ ) . 'template/video-search.php' );
require_once( plugin_dir_path( __FILE__ ) . 'template/book-search.php' );
require_once( plugin_dir_path( __FILE__ ) . 'template/file-search.php' );

	 function search_all_post($searchvalue, $offset=5,$type)
      {
		$display_count = 12;
		  if($type == 'all')
		  {
			  $type = array('post', 'product', 'dyn_book', 'dyn_file');
		  }
		 $args = array(
			'posts_per_page'   => $display_count,
			'offset'           => $offset,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => $type,
			'post_status'      => 'publish',
			's'                => $searchvalue,
			'numberposts'      => -1,
			'suppress_filters' => true 
			);
		$posts_array = get_posts( $args ); 
		return $posts_array;
      }
	  
    function search_total_post_count($searchvalue, $type)
      {
	    
		  if($type == 'all')
		  {
			  $type = array('post', 'product', 'dyn_book', 'dyn_file');
		  }
		 $args = array(
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => $type,
			'numberposts'      => -1,
			'post_status'      => 'publish',
			's'                => $searchvalue,
			'suppress_filters' => true 
			);
			
		$posts_array = get_posts( $args ); 
		$count = 0;
		 foreach($posts_array as $signa){
			$count++;
		}
        $ftotal = $count;
		return $ftotal;	  
 
      }
	 function get_total_post_search_z(){
		// error_reporting(-1);
		 header("Content-type: application/json");
		 ob_start();
		   $responce = array();
		   $searchvalue = $_POST['searchvalue'];
		   $limit = 0;
		   $newquery = 0;
		   $total_content_search  = '';
		   if($_POST['limitserach'] )
		   {
			$limit = $_POST['limitserach'];
			$newquery = 1;
		   }
		   $type = $_POST['searchtype'];
		   $viewstyle = $_POST['viewstyle'];
		   $charctershort = $_POST['charactersort'];
		   
		   $totalpos = search_total_post_count($searchvalue, $type);
		   
		   if($_POST['charactersort'] )
		   {
			  $views =  $_POST['viewshort'];
			  $total_content_search =  sortpost_character($searchvalue, $type, $views, $limit); 
			 // sortpost_character
               	   
		   }else{
			    $total_content_search =  search_all_post($searchvalue, $limit, $type); 
		   }
		  
		   $differ = 12;
		   if($newquery == 1)
		   {
			   $nextpage = $limit + $differ;
		   }else{
			    $nextpage = 13;
		   }
		  
		   
		   if($totalpos > $nextpage)
		   {
				$responce['nextsearch']	= 1;   
		   }else{
			  $responce['nextsearch']	= 0;  
		   }
		  $html_content_search = html_search_all_post($total_content_search, $viewstyle);
		  $responce['searchcontentsss'] = $html_content_search ;
		  //$responce['searchcontentsss'] =  $totalpos; ;
		   $responce['nextlimit'] = $nextpage ;
		    ob_end_clean();
			//echo $html_content_search;
		  echo json_encode($responce);
		 
		wp_die();
	  }
	 function html_search_all_post($toatl_posts, $viewstyle){
		ob_start();
		 $output = '';
		   
		    foreach($toatl_posts as $toatl_post)
            {
	           $postID = $toatl_post->ID;
			  //$output .= $postID;
	          $postType =get_post_type($postID); 
		  
		     if($postType=="product")
		     {
			   
			  //echo $postID;
			 echo  search_product_display( $postID, $viewstyle);
	
			
		      } // end of product loop
			  
			 if($postType=="post")
		     {
			   
			  //echo $postID;
			 echo search_video_display( $postID, $viewstyle);
			
		      } // end of video loop
			  
			 if($postType=="dyn_book")
		       {
			   
			     //echo $postID;
				echo search_book_display( $postID, $viewstyle);
			
		      } // end of video loop
			  
			 if($postType=="dyn_file")
		       {
			   
			   // echo $postID;
			   echo search_file_display( $postID, $viewstyle );
		      } // end of video loop
		  
		 
	    }
		//get_template_part('includes/hover_video'); 
		//get_template_part('includes/hover_search_page');
		$output = ob_get_contents();
	 
		ob_end_clean();
		return $output;
	 }
	function sortpost_character($searchvalue, $type, $views, $rstart){
		global $wpdb;
		//error_reporting(-1);
		$tempquery = '';
		
		$typess = "'" . implode( "','", $type ) . "'";
		
		if($views == 1)
		{
			$tempquery = "SELECT *, (SELECT COUNT(post_id) FROM wp_user_views where  post_id = ID) as countview FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY countview DESC";
		}
		elseif($views == 2)
		{
			$tempquery = "SELECT * FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY ID DESC";
		}
		elseif($views == 3)
		{
			$tempquery = "SELECT *, (SELECT Count(post_id) FROM wp_endorsements WHERE post_id = ID) as countendorsements FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY countendorsements DESC";
		}
		elseif($views == 4)
		{
			$tempquery = "SELECT *, (SELECT Count(post_id) FROM wp_favorite WHERE post_id = ID) as countfavorite FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY countfavorite DESC";
		}
		elseif($views == 5)
		{
			$tempquery = "SELECT *, (SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=ID) as countcoment FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY countcoment DESC";
		}
		elseif($views == 6)
		{
			$tempquery = "SELECT *, (SELECT Count(post_id) FROM wp_dyn_review  where  post_id=ID) as countreview FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY countreview DESC";
		}
		
		elseif($views == 7)
		{
			$tempquery = "SELECT * FROM wp_posts WHERE post_type IN($typess) AND post_status ='publish'AND post_title LIKE '%$searchvalue%' ORDER BY RAND() DESC";
		}
		
		$rtquery = $tempquery . ' LIMIT ' . $rstart . ', 12';
        $rresult = $wpdb->get_results($rtquery);
		return $rresult;
	}
	
function register_newaccount_function(){
	ob_start();
	 header("Content-type: application/json");
	 global $wpdb;
	 $responce = array();
	 
	 $user_name = $_POST['username_reg'];
	 $user_email = $_POST['email_reg'];
	 $fname = $_POST['fname'];
	 $lname = $_POST['lname'];
	 $password_reg = $_POST['password_reg'];
	 $address1 = $_POST['address1'];
	 $address2 = $_POST['address2'];
	 $country12 = $_POST['country12'];
	 $paypalemail_reg = $_POST['paypalemail_reg'];
	 $citytown_reg = $_POST['citytown_reg'];
	 $zip_reg = $_POST['zip_reg'];
	 $phone_reg = $_POST['phone_reg'];
	 $storename_reg = $_POST['storename_reg'];
	 $revnueshare_reg = $_POST['revnueshare_reg'];
	 $donations_reg = $_POST['donations_reg'];
	 $responce['newaccount_success'] = 0;
	 if( username_exists( $user_name ) != '' ){
		 
		$responce['username_error'] = 'Username Already Exists, Choose another one';
	}
	if( email_exists($user_email) ){
		 
		$responce['email_error'] = 'Username Already Exists, Choose another one';
	}
	
	if ( !username_exists( $user_name ) and email_exists($user_email) == false ) {
  	
	
		// Check if valid email
		if( is_email( $user_email ) ){
			// Register into database
			$userid = wp_create_user( $user_name, $password_reg, $user_email );
			
			// Notify Success
			 $responce['notify_success'] = 'A verification has been sent to <strong>'. $user_email.'</strong> . Please click the link given in that email to confirm your registration.';
			 $responce['newaccount_success'] = 1;
			 
			 update_user_meta($userid, 'billing_first_name', $fname);
			 update_user_meta($userid, 'billing_last_name', $lname);
			 update_user_meta($userid, 'billing_address_1', $address1);
			 update_user_meta($userid, 'billing_address_2', $address2);
			 update_user_meta($userid, 'billing_city', $citytown_reg);
			 update_user_meta($userid, 'billing_postcode', $zip_reg);
			 update_user_meta($userid, 'billing_phone', $phone_reg);
			 update_user_meta($userid, 'billing_country', $country12);
			 update_user_meta($userid, 'pv_shop_name', $storename_reg);
			 update_user_meta($userid, 'pv_paypal', $paypalemail_reg);
			 
			  $scret_code = md5($userid . time());
			  $scret_code = $scret_code . "3f3b3d" . base64_encode($userid);
              update_user_meta($userid, "wcemailverifiedcode", $scret_code);
			  
			  update_user_meta($userid, "newuserregtime", time());
			 //pv_shop_name
			 
			 $usernewnowz = new WP_User($userid);

             $usernewnowz->set_role('noverifyuser');
			 
			 $insert = $wpdb->insert($wpdb->prefix."users_merchant_accounts",array(
					'user_id'	    => $userid,
					'paypal_email'	=> $paypalemail_reg,
					'status'	=> $donations_reg,
					));
					
			 $wpdb->insert($wpdb->prefix."users_revenue_status",array(
					'user_id'	    => $userid,
					'revenue_status'=> $revnueshare_reg,
				));	

            $vrifylink = 'www.doityourselfnation.org/bit_bucket/verification-account/?confirmation='. $scret_code;
			$subject = get_bloginfo('name')." Registration";
			$message = sprintf( __( 'Congratulations %s!', 'testmessage' ),  $user_name ) . "\n\n";
			$message .= sprintf( __( 'Username %s', 'testmessage' ),  $user_name ) . "\n\n";
			$message .= wpautop( wp_kses_post( wptexturize( '<a href="' . esc_url( home_url() ) . '/verification-account/?confirmation=' . $scret_code . '">Verify Now</a>'  ) ) );
			
			
			
			$header = 'from: Registration '.get_option('admin_email');
			//$send_contact=mail($user_email,$subject,$message,$header);
			$newaccountcreate  = new Affiliate_WP_Emails;
			$newaccountcreate->send( $user_email, $subject, $message );				
			
		}else{ // If Email not valid
			$responce['email_error'] = 'Invalid Email';
		} // End Email Validation
		
	 
	
     // Else username and email exist
    }
	ob_end_clean();
	  echo json_encode($responce);
		 
		wp_die(); 
}

//end register_new_account_wp	

?>