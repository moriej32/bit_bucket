<?php
/*
  Plugin Name: CheckoutAddress WooCommerce
  Description: Allows your customers to autocomplete billing and shipping addresses on the checkout page using the Google Maps API.
  Author: ziauddin
  Author URI: 
  Plugin URI: 
  Text Domain: checkout-address-autocomplete-for-woocommerce
  Version: 1.3
  Requires at least: 3.0.0
  Tested up to: 4.6.1 
 */


// Load Frontend Javascripts
 add_action('wp_footer', 'ecr_addrac_scripts');
 
function ecr_addrac_scripts() {
    if(is_checkout()){
        google_maps_script_loader();
    }
	if(is_product()){
        google_maps_script_loader();
    }
	 
  }


function google_maps_script_loader() {
    global $wp_scripts; $gmapsenqueued = false;
    foreach ($wp_scripts->registered as $key => $script) {
        if (preg_match('#maps\.google(?:\w+)?\.com/maps/api/js#', $script->src)) {
            $gmapsenqueued = true;
        }
    }

    if (!$gmapsenqueued) {
        wp_enqueue_script('google-autocomplete', 'https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyB2tAgomwfUyn3Kai-GtryVY63wmG3QB5o');
    }
    wp_enqueue_script('rp-autocomplete', plugin_dir_url( __FILE__ ) . 'autocomplete.js');
}