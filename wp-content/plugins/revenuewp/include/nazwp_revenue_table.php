<?php

class NZWP_Admin_Setup
{
	/**
	 * WC > Referrals menu
	 */

	public function __construct()
	{
		add_filter( 'set-screen-option', array( 'NZWP_Admin_Setup', 'set_table_option' ), 10, 3 );
		add_action( 'admin_menu', array( 'NZWP_Admin_Setup', 'menu' ) );

	}


	/**
	 * Add the commissions sub menu 
	 *
	 * @since 1.0.0 
	 * @access public 
	 *
	 */
	public static function menu()
	{
		/*$hook = add_submenu_page(
			'woocommerce',
			__( 'Revenue', 'revenuewp' ), __( 'Revenue', 'revenuewp' ),
			'manage_woocommerce',
			array( 'NZWP_Admin_Setup', 'revenue_sale_page' )
		);*/
		
		$hook = add_submenu_page(
			'clicks_count',
			__( 'Revenue', 'revenuewp' ), __( 'Revenue', 'revenuewp' ),
			'manage_woocommerce',
			'pv_admin_revenue',
			array( 'NZWP_Admin_Setup', 'revenue_sale_page' )
		);
		
		//$hook = add_menu_page(__( 'Revenue', 'revenuewp' ), __( 'Revenue', 'revenuewp' ), 'administrator', 'admin-sale-revenue', array( 'NZWP_Admin_Setup', 'revenue_sale_page' ));

		add_action( "load-$hook", array( 'NZWP_Admin_Setup', 'add_options' ) );

		//add_action( "admin_print_styles-$hook", 	array( 'WCV_Admin_Setup', 'commission_enqueue_style' ) );
		//add_action( "admin_print_scripts-$hook", 	array( 'WCV_Admin_Setup', 'commission_my_enqueue_script' ) );



	} // menu() 



	/**
	 *
	 *
	 * @param unknown $status
	 * @param unknown $option
	 * @param unknown $value
	 *
	 * @return unknown
	 */
	public static function set_table_option( $status, $option, $value )
	{
		if ( $option == 'revenue_sale_per_page' ) {
			return $value;
		}
	}


	/**
	 *
	 */
	public static function add_options()
	{
		global $NZWP_Admin_Page;

		$args = array(
			'label'   => 'Rows',
			'default' => 10,
			'option'  => 'revenue_sale_per_page'
		);
		add_screen_option( 'per_page', $args );

		$NZWP_Admin_Page = new NZWP_Admin_Page();

	}


	/**
	 * HTML setup for the WC > Commission page
	 */
	public static function revenue_sale_page()
	{
		global $NZWP_Admin_Page;
      // $NZWP_Admin_Page = new NZWP_Admin_Page();
	  global $wpdb;
		
        $get = $_GET['done'];
		$userid_after_pay = $_GET['userid'];
		if($get == 1)
		{
			 $query  = "UPDATE wp_users_revenue_status SET `lastpaid` = NOW() WHERE user_id =". $userid_after_pay;
		   $result = $wpdb->query( $query );	
		}
	   add_thickbox();
	   
	     $sqls = "SELECT user_id FROM wp_users_revenue_status";
		 $userids = $wpdb->get_results( $sqls );
		 
		
		 
		 foreach($userids as $userid)
		 {
			  $return_url = current_page_url()."&done=1&userid=". $userid->user_id;
		      $cancel_url = current_page_url()."&done=0";
			// echo $userid->user_id; 
		?>
		
		<div id="moreshippedss<?php echo $userid->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-bodys">
					<?php 
					$paypal_email = '';
					 $datapaypals = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =". $userid->user_id ." and status=1");
		             foreach ($datapaypals as $datapaypal)
					 {
						$paypal_email = $datapaypal->paypal_email; 
						
					 }
					 $amount_pay = NZWP_Admin_Setup::get_total_due_post_by_userid( $userid->user_id );
					 //echo $userid->user_id;
					 //https://www.sandbox.paypal.com/webscr&cmd=
					 //https://www.paypal.com/cgi-bin/webscr
					 
					?>
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypal_email ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel_return" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo $userid->user_id; ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<input type="hidden" name="amount" value="<?php echo $amount_pay; ?>">
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="button button-primary donate-button">Contenue-></button>
													</div>
												</div>

											</form>
			       </div>		
                </div>
			</div>	
        </div>	
		 <?php } ?>	
		 
		 <div id="moreshippedss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-bodys">
					<?php 
					 
					?>
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
												<input type="hidden" name="cmd" value="_xclick">
												<input type="hidden" name="business" value="<?php echo $paypal_email ?>">
												<input type="hidden" name="return" value="<?php echo $return_url; ?>">
												<input type="hidden" name="cancel_return" value="<?php echo $cancel_url; ?>">
												<input type="hidden" name="item_name" value="Do It Yourself Nation">
												<input type="hidden" name="item_number" value="<?php echo $userid->user_id; ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="button_subtype" value="services">
												<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
												<input type="hidden" name="amount" value="<?php echo $amount_pay; ?>">
												<input type="hidden" name="on0" value="Email" required >
												<div class="form-group">
													<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-4 col-sm-8">
														<button type="submit" class="button button-primary donate-button">Contenue-></button>
													</div>
												</div>

											</form>
			       </div>		
                </div>
			</div>	
        </div>
           <style>	
		   .form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	    margin-bottom: 10px;
	     margin-top: 10px;
}
.form-group{ margin-top: 20px; }
#TB_window {border-radius: 5px;}
#TB_title {
    border-radius: 5px;
}
#TB_ajaxWindowTitle {
    color: #3498db;
    font-size: 15px;
   
}
          </style>		   
		<div class="wrap">

			<div id="icon-woocommerce" class="icon32 icon32-woocommerce-reports"><br/></div>
			<h2><?php _e( 'Revenue', 'revenuewp' ); ?></h2>

			<form id="posts-filter" method="get">

			<?php 
				$page  = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_STRIPPED );
				$paged = filter_input( INPUT_GET, 'paged', FILTER_SANITIZE_NUMBER_INT );

				printf( '<input type="hidden" name="page" value="%s" />', $page );
				printf( '<input type="hidden" name="paged" value="%d" />', $paged );
			?>

			<input type="hidden" name="page" value="nzwp_admin_revenue_sle"/>
				
			<?php 
			
			 $NZWP_Admin_Page->prepare_items(); 
			
			?>
			<?php $NZWP_Admin_Page->display() ?>

			</form>
			<div id="ajax-response"></div>
			<br class="clear"/>
		</div>
	<?php
	}


	
	
    public static function get_total_post_by_userid( $user_id ){
		
		global $wpdb;
		
		$post_table = $wpdb->prefix."posts";
		
	    $post_urlqs = $wpdb->get_results("SELECT guid FROM $post_table WHERE post_author=".$user_id );
		
		$monthlyrevenu = 0;
		$monthlytotalrevenue = 0;
		
		foreach( $post_urlqs as $post_urlq ){
			
			$post_url = $post_urlq->guid;
			$monthlyrevenu = NZWP_Admin_Setup::get_total_revenue_post_id( $post_url );
			$monthlytotalrevenue = $monthlyrevenu + $monthlytotalrevenue;
		}
		
		return $monthlytotalrevenue;
	}
	public static function get_total_revenue_post_id( $post_url ){
		global $wpdb;
		
		$table_name = $wpdb->prefix."oio_custom_clicks_count";
	    $table_name2 = $wpdb->prefix."oio_custom_views_count";
		$currentmonth = date('n');
		$previousmonth = $currentmonth - 1;
		
		$getadfreeuserm = $wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."'");
		
		$banner_clicks_month = $getadfreeuserm->clickM;
		
		$getadfreeuserm2 = $wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=MONTH(NOW()) and contentUrl='".$post_url."'");
		
		$video_views_month = $getadfreeuserm2->clickM;
		
		$month_total = $banner_clicks_month + $video_views_month;
		
		return $month_total;
		//return $previousmonth;
		
	}
	
	
	 public static function get_total_due_post_by_userid( $user_id ){
		
		global $wpdb;
		
		$post_table = $wpdb->prefix."posts";
		
	    $post_urlqs = $wpdb->get_results("SELECT guid FROM $post_table WHERE post_author=".$user_id );
		
		$monthlyrevenu = 0;
		$monthlytotalrevenue = 0;
		
		foreach( $post_urlqs as $post_urlq ){
			
			$post_url = $post_urlq->guid;
			$monthlyrevenu = NZWP_Admin_Setup::get_total_due_revenue_post_id( $post_url );
			$monthlytotalrevenue = $monthlyrevenu + $monthlytotalrevenue;
		}
		
		return $monthlytotalrevenue;
	}
	public static function get_total_due_revenue_post_id( $post_url ){
		global $wpdb;
		
		$table_name = $wpdb->prefix."oio_custom_clicks_count";
	    $table_name2 = $wpdb->prefix."oio_custom_views_count";
		$currentmonth = date('n');
		$previousmonth = $currentmonth - 1;
		
		$getadfreeuserm = $wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=$previousmonth and contentUrl='".$post_url."'");
		
		$banner_clicks_month = $getadfreeuserm->clickM;
		
		$getadfreeuserm2 = $wpdb->get_row("SELECT sum(cost) as clickM FROM $table_name2 WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date)=$previousmonth and contentUrl='".$post_url."'");
		
		$video_views_month = $getadfreeuserm2->clickM;
		
		$month_total = $banner_clicks_month + $video_views_month;
		
		return $month_total;
		//return $previousmonth;
		
	}
}


if ( !class_exists( 'WP_List_Table' ) ) require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';

/**
 * WC_Simple_Referral_Admin class.
 *
 * @extends WP_List_Table
 */
class NZWP_Admin_Page extends WP_List_Table
{

	public $index;


	/**
	 * __construct function.
	 *
	 * @access public
	 */
	function __construct()
	{
		global $status, $page;

		$this->index = 0;

		//Set parent defaults
		parent::__construct( array(
								  'singular' => 'revenuesale',
								  'plural'   => 'revenuesales',
								  'ajax'     => false
							 ) );
	}


	/**
	 * column_default function.
	 *
	 * @access public
	 *
	 * @param unknown $item
	 * @param mixed   $column_name
	 *
	 * @return unknown
	 */
	function column_default( $item, $column_name )
	{
		global $wpdb;

		switch ( $column_name ) {
			case 'id' :
				return $item->id;
			case 'user_id' :
				$user = get_userdata( $item->user_id );
				$dsplayna = $user->user_nicename;
				return '<a href="http://www.doityourselfnation.org/bit_bucket/analytics/?user=' . $item->user_id  . '">' . $dsplayna . '</a>';
			case 'total_pendding' :
			     $total_pendding = NZWP_Admin_Setup::get_total_post_by_userid( $item->user_id );
				 return '$'. $total_pendding;
			case 'total_due' :
				 $total_due = NZWP_Admin_Setup::get_total_due_post_by_userid( $item->user_id );
				 return '$'. $total_due;
				 
			case 'status' :
			      $url = add_query_arg( array(
    'action'    => 'foo_modal_box',
    'TB_iframe' => 'true',
    'width'     => '600',
    'height'    => '400',
	'inlineId'  => 'moreshippedss'
), admin_url( 'admin.php' ) );
				//return '<a href="' . $url . '" class="button button-primary thickbox">Pay Now</a>';
				$pay_enable = '';
				 $total_due_on = NZWP_Admin_Setup::get_total_due_post_by_userid( $item->user_id );
				 
				 if($total_due_on == 0 ){
					$pay_enable = ''; 
				 } else{
					 $pay_enable = ' button-primary thickbox'; 
				 }
				 
				return '<input alt="#TB_inline?height=200&width=300&inlineId=moreshippedss'. $item->user_id .'" title="Revenue Pay This User" class=" button '. $pay_enable .'" type="button" value="Pay now" />';
		     
			case 'receive_total':
			
			    $total_receive = $item->receivetotal;
				
			    return '$'. $total_receive;
			
			case 'lastpaid' :
				return $item->lastpaid ;  
		}
	}


	/**
	 * column_cb function.
	 *
	 * @access public
	 *
	 * @param mixed $item
	 *
	 * @return unknown
	 */
	function column_cb( $item )
	{
		return sprintf(
			'<input type="checkbox" name="%1$s[]" value="%2$s" />',
			/*$1%s*/
			'id',
			/*$2%s*/
			$item->id
		);
	}


	/**
	 * get_columns function.
	 *
	 * @access public
	 * @return unknown
	 */
	function get_columns()
	{
		$columns = array(
			'cb'               => '<input type="checkbox" />',
			'user_id'          => __( 'Username', 'revenuewp' ),
			'total_pendding'   => __( 'Total Pending', 'revenuewp' ),
			'total_due'        => __( 'Total Due', 'revenuewp' ),
			'receive_total'    => __( 'Receive Total', 'revenuewp' ),
			'lastpaid'         => __( 'Last Paid Date', 'revenuewp' ),
			'status'           => __( 'Status', 'revenuewp' ),
			
		);

		return $columns;
	}


	/**
	 * get_sortable_columns function.
	 *
	 * @access public
	 * @return unknown
	 */
	function get_sortable_columns()
	{
		$sortable_columns = array(
			'lastpaid'       => array( 'lastpaid', true ),
			'total_pendding' => array( 'total_pendding', false ),
			'user_id'   => array( 'user_id', false ),
			'total_due'  => array( 'total_due', false ),
			'status'     => array( 'status', false ),
		);

		return $sortable_columns;
	}


	/**
	 * Get bulk actions
	 *
	 * @return unknown
	 */
	function get_bulk_actions()
	{
		$actions = array(
			//'pay_now'     => __( 'Pay All Now', 'revenuewp' ),
			//'mark_due'      => __( 'Mark due', 'wcvendors' ),
			//'mark_reversed' => __( 'Mark reversed', 'wcvendors' ),
			 'delete' => __('Delete', 'revenuewp'),
		);

		return $actions;
	}


	/**
	 *
	 */
	function extra_tablenav( $which )
	{
		if ( $which == 'top' ) {
			?><div class="alignleft actions" style="width: 70%;">
			<?php
			// Months drop down 
			//$this->months_dropdown( 'commission' );
			
			// commission status drop down 
			//$this->status_dropdown( 'commission' );
			
			// Vendor drop down 
			//$this->vendor_dropdown( 'commission' );
			$url = add_query_arg( array(
    'action'    => 'foo_modal_box',
    'TB_iframe' => 'true',
    'width'     => '600',
    'height'    => '400',
	'inlineId'  => 'moreshippedss'
), admin_url( 'admin.php' ) );
				echo '<a href="' . $url . '" class="button button-primary thickbox">Pay Now Everyone</a>';
			//submit_button( __( 'Pay All Now' ), false, false, false, array( 'id' => "post-query-submit", 'name' => 'payall-at-time' ) );
			?></div>
			<?php
		}
	}


	/**
	 * Display a monthly dropdown for filtering items
	 *
	 * @since  3.1.0
	 * @access protected
	 *
	 * @param unknown $post_type
	 */
	function months_dropdown( $post_type )
	{
		global $wpdb, $wp_locale;

		$table_name = $wpdb->prefix . "pv_commission";

		$months = $wpdb->get_results( "
			SELECT DISTINCT YEAR( time ) AS year, MONTH( time ) AS month
			FROM $table_name
			ORDER BY time DESC
		" );

		$month_count = count( $months );

		if ( !$month_count || ( 1 == $month_count && 0 == $months[ 0 ]->month ) )
			return;

		$m = isset( $_GET[ 'm' ] ) ? (int) $_GET[ 'm' ] : 0;
		?>
		<select name="m" id="filter-by-date">
			<option<?php selected( $m, 0 ); ?> value='0'><?php _e( 'Show all dates', 'wcvendors' ); ?></option>
			<?php
			foreach ( $months as $arc_row ) {
				if ( 0 == $arc_row->year )
					continue;

				$month = zeroise( $arc_row->month, 2 );
				$year  = $arc_row->year;

				printf( "<option %s value='%s'>%s</option>\n",
					selected( $m, $year . $month, false ),
					esc_attr( $arc_row->year . $month ),
					/* translators: 1: month name, 2: 4-digit year */
					sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $month ), $year )
				);
			}
			?>
		</select>
		
	<?php
	}

	/**
	 * Display a status dropdown for filtering items
	 *
	 * @since  3.1.0
	 * @access protected
	 *
	 * @param unknown $post_type
	 */
	function status_dropdown( $post_type )
	{
		$com_status = isset( $_GET[ 'com_status' ] ) ? $_GET[ 'com_status' ] : '';
		?>
		<select name="com_status">
			<option<?php selected( $com_status, '' ); ?> value=''><?php _e( 'Show all Statuses', 'wcvendors' ); ?></option>
			<option<?php selected( $com_status, 'due' ); ?> value="due"><?php _e( 'Due', 'wcvendors' ); ?></option>
			<option<?php selected( $com_status, 'paid' ); ?> value="paid"><?php _e( 'Paid', 'wcvendors' ); ?></option>
			<option<?php selected( $com_status, 'reversed' ); ?> value="reversed"><?php _e( 'Reversed', 'wcvendors' ); ?></option>
		</select>
	<?php
	}

	/**
	 * Display a vendor dropdown for filtering commissions
	 *
	 * @since  1.9.2
	 * @access public
	 *
	 * @param unknown $post_type
	 */
	public function vendor_dropdown( $post_type ){ 

		$user_args 			= array( 'fields' => array( 'ID', 'display_name' ) );
		$vendor_id 			= isset( $_GET[ 'vendor_id' ] ) ? $_GET[ 'vendor_id' ] : '';
		$new_args           = $user_args;
		$new_args[ 'role' ] = 'vendor';
		$users              = get_users( $new_args );

		// Generate the drop down 
		$output = '<select style="width: 30%;" name="vendor_id" id="vendor_id" class="select2">'; 
		$output .= "<option></option>";
		foreach ( (array) $users as $user ) {
			$select = selected( $user->ID, $vendor_id, false );
			$output .= "<option value='$user->ID' $select>$user->display_name</option>";
		}
		$output .= '</select>';

		echo $output; 

	} // vendor_dropdown() 
   

	/**
	 * Process bulk actions
	 *
	 * @return unknown
	 */
	function process_bulk_action()
	{
		if ( !isset( $_GET[ 'id' ] ) ) return;

		$items = array_map( 'intval', $_GET[ 'id' ] );
		$ids   = implode( ',', $items );

		switch ( $this->current_action() ) {
			case 'pay_now':
				$result = $this->pay_now( $ids );

				if ( $result )
					echo '<div class="updated"><p>' . __( 'Commission marked paid.', 'wcvendors' ) . '</p></div>';
				break;

			case 'mark_due':
				$result = $this->mark_due( $ids );

				if ( $result )
					echo '<div class="updated"><p>' . __( 'Commission marked due.', 'wcvendors' ) . '</p></div>';
				break;

			case 'mark_reversed':
				$result = $this->mark_reversed( $ids );

				if ( $result )
					echo '<div class="updated"><p>' . __( 'Commission marked reversed.', 'wcvendors' ) . '</p></div>';
				break;

			default:
				// code...
				break;
		}

	}


	/**
	 *
	 *
	 * @param unknown $ids (optional)
	 *
	 * @return unknown
	 */
	public function pay_now( $ids = array() )
	{
		global $wpdb;

		$table_name = $wpdb->prefix . "users";
        foreach($ids as $id)
		{
		 $datapaypal = $wpdb->get_results("SELECT paypal_email FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =". $id ." and status=1");
		 $paypal_email = $datapaypal[0];
		}
		//$query  = "UPDATE `{$table_name}` SET `status` = 'paid' WHERE id IN ($ids) AND `status` = 'due'";
		//$result = $wpdb->query( $query );

		return $result;
	}
	
	


	/**
	 *
	 *
	 * @param unknown $ids (optional)
	 *
	 * @return unknown
	 */
	public function mark_reversed( $ids = array() )
	{
		global $wpdb;

		$table_name = $wpdb->prefix . "pv_commission";
		$query  = "UPDATE `{$table_name}` SET `status` = 'reversed' WHERE id IN ($ids)";
		$result = $wpdb->query( $query );

		return $result; 

	}


	/**
	 *
	 *
	 * @param unknown $ids (optional)
	 *
	 * @return unknown
	 */
	public function mark_due( $ids = array() )
	{
		global $wpdb;

		$table_name = $wpdb->prefix . "pv_commission";

		$query  = "UPDATE `{$table_name}` SET `status` = 'due' WHERE id IN ($ids)";
		$result = $wpdb->query( $query );

		return $result;
	}


	/**
	 * prepare_items function.
	 *
	 * @access public
	 */
	function prepare_items()
	{
		global $wpdb;

		$_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );

		$per_page     = $this->get_items_per_page( 'revenue_sale_per_page', 10 );
		$current_page = $this->get_pagenum();

		$orderby = !empty( $_REQUEST[ 'orderby' ] ) ? esc_attr( $_REQUEST[ 'orderby' ] ) : 'date';
		$order   = ( !empty( $_REQUEST[ 'order' ] ) && $_REQUEST[ 'order' ] == 'asc' ) ? 'ASC' : 'DESC';
		$com_status = !empty( $_REQUEST[ 'com_status' ] ) ? esc_attr( $_REQUEST[ 'com_status' ] ) : '';
		$vendor_id = !empty( $_REQUEST[ 'vendor_id' ] ) ? esc_attr( $_REQUEST[ 'vendor_id' ] ) : '';
		$status_sql = '';
		$time_sql = ''; 

		/**
		 * Init column headers
		 */
		$this->_column_headers = $this->get_column_info();

		/**
		 * Process bulk actions
		 */
		$this->process_bulk_action();

		/**
		 * Get items
		 */
		$sql = "SELECT COUNT(ID) FROM {$wpdb->prefix}users_revenue_status";

		/*if ( !empty( $_GET[ 'm' ] ) ) {

			$year  = substr( $_GET[ 'm' ], 0, 4 );
			$month = substr( $_GET[ 'm' ], 4, 2 );

			$time_sql = "
				WHERE MONTH(`time`) = '$month'
				AND YEAR(`time`) = '$year'
			";

			$sql .= $time_sql;
		}

		if ( !empty( $_GET[ 'com_status' ] ) ) { 

			if ( $time_sql == '' ) { 
				$status_sql = " 
				WHERE status = '$com_status'
				"; 
			} else { 
				$status_sql = " 
				AND status = '$com_status'
				";
			}
			
			$sql .= $status_sql; 
		}


		if ( !empty( $_GET[ 'vendor_id' ] ) ) { 

			if ( $time_sql == '' || $status_sql == '' ) { 
				$vendor_sql = " 
				WHERE vendor_id = '$vendor_id'
				"; 
			} else { 
				$vendor_sql = " 
				AND vendor_id = '$vendor_id'
				";
			}
			
			$sql .= $vendor_sql; 
		}*/

		$max = $wpdb->get_var( $sql );

		$sql = "
			SELECT * FROM {$wpdb->prefix}users_revenue_status
		";

		/*if ( !empty( $_GET[ 'm' ] ) ) {
			$sql .= $time_sql;
		}

		if ( !empty( $_GET['com_status'] ) ) { 
			$sql .= $status_sql;
		}

		if ( !empty( $_GET['vendor_id'] ) ) { 
			$sql .= $vendor_sql;
		}*/

		$offset = ( $current_page - 1 ) * $per_page; 

		$sql .= "
			ORDER BY `{$orderby}` {$order}
			LIMIT {$offset}, {$per_page}
		";

		// $this->items = $wpdb->get_results( $wpdb->prepare( $sql, ( $current_page - 1 ) * $per_page, $per_page ) );
		$this->items = $wpdb->get_results( $sql );

		/**
		 * Pagination
		 */
		$this->set_pagination_args( array(
										 'total_items' => $max,
										 'per_page'    => $per_page,
										 'total_pages' => ceil( $max / $per_page )
									) );
	}


}
