<?php

// Click Studio License class V1.1.1

class AffiliateWP_Multi_Level_Affiliates_Licenses {

# URL to check for updates, this is where the index.php script goes
public $api_url;

# Type of package to be updated
//public $settings;

protected $license_key;

function __construct($plugin_config, $plugin_settings) {
		
	$this->plugin_config = $plugin_config;
	$this->plugin_settings = $plugin_settings;
	//$this->license_key = $this->plugin_settings[$this->plugin_config['plugin_prefix'].'_'.'license_key'];
	$this->license_key = ( !empty($this->plugin_settings[$this->plugin_config['plugin_prefix'].'_'.'license_key']) ) ? $this->plugin_settings[$this->plugin_config['plugin_prefix'].'_'.'license_key'] : '';
		
	$this->capture_license_key();

}

// Capture the license key
public function capture_license_key() {
	
	// Process if no other actions are active
	if ( !isset($_GET[$this->plugin_config['plugin_prefix'].'_license_change']) ) {
	
		$current_key = $this->get_license_option('license_key');
		
		if( empty($current_key) && !empty($this->license_key) ) {
			
			// Set new key
			$this->set_license_option('license_key', $this->license_key);
			
		}
	
	}
}

// Remove all license data
public function remove_license_data() {
	$license_data = $this->get_license_options();
	foreach($license_data as $key => $value) {
		delete_site_option($this->plugin_config['plugin_prefix'].'_'.$key);
	}
}

// Get a license key or status
public function get_license_option($option_key) {
	// Accepts 'license_key' or 'license_status' as the option key
	return get_site_option(  $this->plugin_config['plugin_prefix'].'_'.$option_key, '' );
}

// Set a license key or status
private function set_license_option($option_key, $value) {
	return update_site_option( $this->plugin_config['plugin_prefix'].'_'.$option_key, trim($value) );
}

// Get both license key and status
private function get_license_options() {
	$license_data = array(
		'license_key' => $this->get_license_option('license_key'),
		'license_status' => $this->get_license_option('license_status'),
	);
	
	return $license_data;
}

// $license_data->license will be either 'valid' or 'invalid' or 'deactivated'
// Activate license
public function activate_license() {
	
	$license_key = $this->get_license_option('license_key');
	
	if( 
	(!empty($license_key)) && 
	(($this->get_license_option('license_status') != 'valid'))
	){

		$license_data = $this->edd_api_request('activate_license');
		$license_status = $license_data->license;
		if( (isset($license_status)) && (!empty($license_status)) ) {
			$this->set_license_option('license_status', $license_status);
		}
	
	}

	
}

// Deactivate license
public function deactivate_license() {

	if( 
	(isset($_GET[$this->plugin_config['plugin_prefix'].'_license_change'])) &&
	($_GET[$this->plugin_config['plugin_prefix'].'_license_change'] == 'deactivate') 
	){
		$license_key = $this->get_license_option('license_key');
		if( !empty($license_key) ) {
	
			$license_data = $this->edd_api_request('deactivate_license');
			$license_status = $license_data->license;
			if( (isset($license_status)) && ($license_status == 'deactivated') ) {
				$this->remove_license_data();
				return TRUE;
			}
	
		}
		
	}
	
	return FALSE;
	
}

// Check license status
public function check_license() {
	
	$license_key = $this->get_license_option('license_key');
	if( !empty($license_key) ) {
		$license_data = $this->edd_api_request('check_license');
		return $license_data->license;
	}
}


// EDD API request
private function edd_api_request($action) {
	// Accepts 'activate_license' or 'deactivate_license' or 'check_license'
	$api_params = array(
		'edd_action'=> $action,
		'license' 	=> $this->get_license_option('license_key'),
		'item_name' => urlencode( $this->plugin_config['plugin_item_name'] ),
		'url'       => home_url()
	);

	$response = wp_remote_post( $this->plugin_config['plugin_updater_url'], array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )	{
		return false;
	} else {
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		return $license_data;
	}
	
}

// Get the license message and actions
public function license_status_msg() {
	
	$status_msg = '';

	$license_key = $this->get_license_option('license_key');
	$license_status =$this->get_license_option('license_status');
		
	if( 'valid' === $license_status && ! empty( $license_key ) ) {
		
		$href = add_query_arg( $this->plugin_config['plugin_prefix'].'_license_change', 'deactivate', $_SERVER['REQUEST_URI'] );
		$href_text = 'Deactivate License';
		$status_msg .= '<a target="_self" class="button-primary" href="'.$href.'">'.$href_text.'</a>';
		$status_msg .= '<p style="color:green;">&nbsp;' . __( 'Your license is valid.', 'wc-autoship' ) . '</p>';	
					
	} elseif( 'invalid' === $license_status && ! empty( $license_key ) ) {	
		
		$status_msg .= '<p style="color:red;">&nbsp;' . __( 'Your license is invalid.', 'wc-autoship' ) . '</p>';	
				
	} else{
		$status_msg .= '<p style="">&nbsp;' . __( 'Enter your license key and save the settings to activate.', 'wc-autoship' ) . '</p>';
		
	}
		
	return $status_msg;
}

 
} // End of class
?>