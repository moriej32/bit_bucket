<?php

class AffiliateWP_Multi_Level_Affiliates_Base {

	protected $plugin_settings;
	protected $plugin_config;

	public function __construct() {
		
		$this->plugin_config = unserialize(AFFWP_MLA_PLUGIN_CONFIG);
		$this->plugin_settings = unserialize(AFFWP_MLA_PLUGIN_SETTINGS);
		
		// If plugin features enabled
		if( $this->plugin_settings[$this->plugin_config['plugin_prefix'].'_enable_mla'] == '1' ) :
		
			$this->includes();
			$this->setup_objects();	
		
		endif;
		
	}
	
	private function includes() {
		
		// Includes
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/includes/gamajo-template-loader.php';
		
		// Core
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-template-loader.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-common.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-hooks.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-shortcodes.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-affiliate.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-referral.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-matrix.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-statistics.php';
		
		// Integrations
		// AffiliateWP BuddyPress
		if ( class_exists( 'AffiliateWP_BuddyPress' ) ) :
			if( $this->plugin_settings[$this->plugin_config['plugin_prefix'].'_dashboard_tab_enable'] == '1' 
				&& $this->plugin_settings[$this->plugin_config['plugin_prefix'].'_buddypress_enable'] == '1' ) :
				require_once $this->plugin_config['plugin_dir'] . 'plugin_core/integrations/affiliatewp-buddypress.php';
			endif;
		endif;
		
	}
	
	private function setup_objects() {
		$this->hooks = new AffiliateWP_MLA_Hooks();
		$this->shortcodes = new AffiliateWP_MLA_Shortcodes();
	}
	
} // End of class

?>