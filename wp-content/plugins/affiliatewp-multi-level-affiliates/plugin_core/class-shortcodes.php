<?php
class AffiliateWP_MLA_Shortcodes extends AffiliateWP_MLA_Common {

	public function __construct() {
		
		parent::__construct();
		
		// Affiliate Area Tab Content
		add_shortcode( 'mla_dashboard', array( $this, 'mla_dashboard' ) );
		
		// Affiliate Rates
		add_shortcode( 'mla_dashboard_rates', array( $this, 'mla_dashboard_rates' ) );
		
		// Affiliate Earnings
		add_shortcode( 'mla_dashboard_earnings', array( $this, 'mla_dashboard_earnings' ) );
		
		// Network Statistics
		add_shortcode( 'mla_dashboard_statistics', array( $this, 'mla_dashboard_statistics' ) );
		
		// Network Chart
		add_shortcode( 'mla_dashboard_chart', array( $this, 'mla_dashboard_chart' ) );
		
		// Debug output
		add_shortcode( 'mla_output_debug_data', array( 'AffiliateWP_MLA_Common', 'mla_output_debug_data' ) );
		
	}
	
	// The affiliate area tab content
	public function mla_dashboard( $atts = '' ) {
		
		if(empty($atts)) $atts = array();
		
		$dashboard_features = $this->plugin_setting('dashboard_features');
		
		$default_atts = array( 
			'affiliate_id' => affwp_get_affiliate_id( get_current_user_id() ),
			'display_rates' => ( array_key_exists( 'rates', $dashboard_features ) ) ? (bool) TRUE : (bool) FALSE, /* TRUE, FALSE */
			'display_earnings' => ( array_key_exists( 'earnings', $dashboard_features ) ) ? (bool) TRUE : (bool) FALSE, /* TRUE, FALSE */
			'display_network_statistics' => ( array_key_exists( 'network_statistics', $dashboard_features ) ) ? (bool) TRUE : (bool) FALSE, /* TRUE, FALSE */
			'display_network_chart' => ( array_key_exists( 'network_chart', $dashboard_features ) ) ? (bool) TRUE : (bool) FALSE, /* TRUE, FALSE */
		 	);
			
    	$data = array_merge( $default_atts, $atts );
		
		return $this->get_shortcode_template( 'dashboard-tab', $data );
		
	}
	
	// The front end affiliates area
	public function mla_dashboard_rates( $atts = '' ) {
		
		if(empty($atts)) $atts = array();

		$default_atts = array( 'affiliate_id' => affwp_get_affiliate_id( get_current_user_id() ) );
		
    	$data = array_merge( $atts, $default_atts );
		
		return $this->get_shortcode_template( 'dashboard-affiliate-rates', $data );
		
	}
	
	// The front end affiliates area
	public function mla_dashboard_earnings( $atts = '' ) {
		
		if(empty($atts)) $atts = array();

		$default_atts = array( 'affiliate_id' => affwp_get_affiliate_id( get_current_user_id() ) );
		
    	$data = array_merge( $default_atts, $atts );
		
		return $this->get_shortcode_template( 'dashboard-affiliate-earnings', $data );
		
	}
	
	// The front end affiliates area
	public function mla_dashboard_statistics( $atts = '' ) {
		
		if(empty($atts)) $atts = array();

		$default_atts = array( 'affiliate_id' => affwp_get_affiliate_id( get_current_user_id() ) );
		
    	$data = array_merge( $default_atts, $atts );
		
		return $this->get_shortcode_template( 'dashboard-network-statistics', $data );
		
	}
	
	// The front end affiliates area
	public function mla_dashboard_chart( $atts = '' ) {
		
		if(empty($atts)) $atts = array();
		
		$default_atts = array( 
			'affiliate_id' => affwp_get_affiliate_id( get_current_user_id() ), 
			'chart_size' => 'medium', /* small, medium, large */
			'show_preloader' => (bool) TRUE, /* TRUE, FALSE */
			);
			
    	$data = array_merge( $default_atts, $atts );
		
		return $this->get_shortcode_template( 'dashboard-network-chart', $data );
		
	}
	
	// Return a template as an object
	public function get_shortcode_template( $template_part, $data ) {
		
		$template_loader = new AffiliateWP_MLA_Template_Loader();
		$template_loader->set_template_data( $data );
		
		return $template_loader->get_template_object( $template_part );
		
	}
	
}

?>