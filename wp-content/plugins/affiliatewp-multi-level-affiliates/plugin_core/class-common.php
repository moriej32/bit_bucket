<?php

class AffiliateWP_MLA_Common {
	
	protected $plugin_settings;
	protected $plugin_config;

	public function __construct() {
		
		$this->plugin_config = unserialize(AFFWP_MLA_PLUGIN_CONFIG);
		$this->plugin_settings = unserialize(AFFWP_MLA_PLUGIN_SETTINGS);
		
		add_shortcode( 'mla_output_debug_data', array( $this, 'mla_output_debug_data' ) );	
	}
	
	// Get a plugin setting
	public function plugin_setting( $key ) {

		return $this->plugin_settings[$this->plugin_config['plugin_prefix'].'_'.$key ];
		
	}
	
	// Get matrix setting
	public function matrix_setting($setting_key, $group_id) {
		
		$value = AffiliateWP_Multi_Level_Affiliates_Settings::get_matrix_setting($setting_key, $group_id);
		
		return $value;	
	}
	
	// Get order total - new method
	public function get_order_total() {
		
		$default_total = 0.00;
		
		$saved_total = get_transient( 'mla_referral_order_total' );
		$order_total = ( $saved_total > 0.00 ) ? $saved_total : $default_total;
		
		return $order_total;
		
	}
	
	// Check if Affiliate Groups enabled
	public function groups_enabled() {
		if ( class_exists('AffiliateWP_Affiliate_Groups') ) return (bool) TRUE;
	}
	
	// Get WordPress timezone
	public function wp_get_timezone_string() {
 
		// if site timezone string exists, return it
		if ( $timezone = get_option( 'timezone_string' ) )
			return $timezone;
	 
		// get UTC offset, if it isn't set then return UTC
		if ( 0 === ( $utc_offset = get_option( 'gmt_offset', 0 ) ) )
			return 'UTC';
	 
		// adjust UTC offset from hours to seconds
		$utc_offset *= 3600;
	 
		// attempt to guess the timezone string from the UTC offset
		if ( $timezone = timezone_name_from_abbr( '', $utc_offset, 0 ) ) {
			return $timezone;
		}
	 
		// last try, guess timezone string manually
		$is_dst = date( 'I' );
	 
		foreach ( timezone_abbreviations_list() as $abbr ) {
			foreach ( $abbr as $city ) {
				if ( $city['dst'] == $is_dst && $city['offset'] == $utc_offset )
					return $city['timezone_id'];
			}
		}
		 
		// fallback to UTC
		return 'UTC';
	}
	
	
	// Store debug data
	public function store_debug_data($data) {
		$data = maybe_serialize( $data );
		set_transient( 'mla_data', $data, 12 * HOUR_IN_SECONDS );
	}
	
	// Output debug data
	public function mla_output_debug_data() {
		
		echo '<br><br>'.'---------------------- MLA Debug Data ----------------------'.'<br><br><br><br>';
		print_r( get_transient( 'mla_data') );
		//print_r( maybe_unserialize( get_transient( 'mla_data') ) );
		//echo $mla_parent = affwp_get_affiliate_meta( '56', 'mla_level_1', TRUE );
		
		echo '<br><br><br><br>'.'---------------------- END MLA Debug Data ----------------------'.'<br><br>';
		
	}
	
	
	
}

?>