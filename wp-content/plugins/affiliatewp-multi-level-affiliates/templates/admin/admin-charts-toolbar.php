<style type="text/css">
#affwp-mla-reports-toolbar {
	padding-top: 10px;
	padding-bottom: 10px;
}
#affwp-mla-reports-toolbar table.affwp-tab-table td {
	padding-left: 10px!important;
}
#affwp-mla-reports-toolbar input, #affwp-mla-reports-toolbar select {
	height: 36px!important;
	margin-top: 0px!important;
	margin-bottom: 0px!important;
}
.affwp-mla-reports .select2-container {
	width: 100%!important;
}
.affwp-mla-reports .select2-container, .affwp-mla-reports .back_to_list {
    display: block!important;
	float:left!important;
}
.affwp-mla-reports .select2-container .select2-choice {
    padding: 5px!important;
}
</style>

<?php 
$date_range = ( !empty($_POST['date_range']) ) ? $_POST['date_range'] :  '';
$per_page = ( !empty($_POST['per_page']) ) ? $_POST['per_page'] :  '';
$start_date = ( !empty($_POST['start_date']) ) ? $_POST['start_date'] :  '';
$end_date = ( !empty($_POST['end_date']) ) ? $_POST['end_date'] :  '';

$report_affiliate_id = ( empty( $_POST['affiliate_id'] ) ) ? '' : $_POST['affiliate_id'] ;
?>

<div id="affwp-mla-reports-toolbar" class="affwp-mla-reports"> 

	<form method="post">
    
    <input name="aff_mla_toolbar" type="hidden" id="mla_binary_report" value="1">
    
    <div>
    
    <table class="affwp-tab-table" width="100%" cellpadding="5" cellspacing="5">
      <tbody>
        <tr>
          <td width="20%">
          <select name="chart" id="chart">
            <option value="network">Affiliate Network</option>
          </select>
          </td>
          <td width="35%">
            <select name="affiliate_id" id="parent_affiliate_id">
            <option value="">Select Affiliate</option>
            <?php 
			$ps = new AffiliateWP_Multi_Level_Affiliates_Settings();
			$all_affiliates = $ps->get_affiliates_dropdown();
			foreach( $all_affiliates as $affiliate_id => $name ) : 
			?>
            <option value="<?php echo $affiliate_id ;?>" <?php if( $report_affiliate_id == $affiliate_id ) { ?>selected="selected"<?php }; ?>><?php echo affwp_get_affiliate_name( $affiliate_id ) ;?></option>
            <?php endforeach ;?>
            </select>
          </td>
          <td>
          <input type="submit" value="View Chart">
          </td>
        </tr>
      </tbody>
	</table>
    
	</div>
    
    </form>

</div>