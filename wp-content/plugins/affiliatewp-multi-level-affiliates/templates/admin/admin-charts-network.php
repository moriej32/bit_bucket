<style type="text/css">
.affwp-tab-content_mla_chart { overflow: auto;max-width: auto;max-height: auto;}
.affwp-tab-content_mla_chart table tr { border-bottom: none!important;}

/* all nodes */
.affwp-tab-content_mla_chart .mla_network_node, .affwp-tab-content_mla_chart .mla_network_node .child_node {
	/*height: 68px;*/
	height: 50px;
	padding-top: 10px;
	background-color: #0E97C4;
	-moz-border-radius: 5px;
    -webkit-border-radius: 5px;
	text-align: center;
    vertical-align: middle;
    border: none;;
	padding: 0px;
	line-height: 20px;
	color: #ffffff;
}

/* the top affiliate node */
#chart_div > table > tbody > tr:nth-child(2) > td.mla_network_node.google-visualization-orgchart-node-medium {
	padding-top: 5px;
	padding-bottom: 5px;
	height: 40px;
	-webkit-box-shadow: rgba(0, 0, 0, 0.4) 3px 3px 3px;
    -moz-box-shadow: rgba(0, 0, 0, 0.4) 3px 3px 3px;	
}

/* all child nodes */
.affwp-tab-content_mla_chart .mla_network_node .child_node {
	padding-top: 5px;
	-webkit-box-shadow: rgba(0, 0, 0, 0.4) 3px 3px 3px;
    -moz-box-shadow: rgba(0, 0, 0, 0.4) 3px 3px 3px;
}

/* active affiliates */
.affwp-tab-content_mla_chart .mla_network_node .child_node.active {
	background-color: #0E97C4;	
}

/* inactive and pending affiliates */
.affwp-tab-content_mla_chart .mla_network_node .child_node.inactive, .affwp-tab-content_mla_chart .mla_network_node .child_node.pending {
	background-color: #B4B4B4;	
}

#loading { position: absolute; width: 100%; }

 /* Tooltips */
.persons_info_tooltip {
	position: absolute;
	left: 0;
	top: 0;
}
.affiliate_tooltips_ {
	position: absolute;
	padding: 10px;
	background: #fff;
	margin-top: 10px;
	width: 250px;
	max-width: 250px;
	z-index: 99999;
	border-radius: 3px;
	border: 1px solid #eee;
	box-shadow: 0 0 10px 0 #ddd;
	display: none;
}

.affiliate_tooltips_:before {
	position: absolute;
	left: 50%;
	color: #fff;
	margin-left: -8px;
	font-size: 22px;
	z-index: -1;
}

.affiliate_tooltips_.arrow_top:before {
	content: '\25b2';
	top: -12px;
	text-shadow: 0 -9px 5px rgba(0, 0, 0, 0.05);
}

.affiliate_tooltips_.arrow_bottom:before {
	content: '\25bc';
	bottom: -12px;
	text-shadow: 0 9px 5px rgba(0, 0, 0, 0.05);
}
.affiliate_tooltips_ .aff_info_row {
	padding-top: 5px;
}

.affiliate_tooltips_ .chart_affiliate_actions {
	padding-top: 10px;
}
</style>

<?php
$charts_affiliate_id = ( empty( $_POST['affiliate_id'] ) ) ? '' : $_POST['affiliate_id'] ;

if( empty($charts_affiliate_id) ) :
else:

$affiliate = new AffiliateWP_MLA_Affiliate( $charts_affiliate_id );
$affiliates = $affiliate->get_entire_network();
?>

<div id="affwp-tab-content_mla_chart" class="affwp-mla-reports affwp-tab-content_mla_chart">

    <?php if(!empty($affiliates) ) { ?>
    <div id="chart_div"></div>
    <?php } else {?>
    <div>Your network is empty</div>
    <?php } ?>

</div>

<!-- Generate the Chart -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', { packages: ["orgchart"] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Affiliate Name');
        data.addColumn('string', 'Parent Affiliate');
        data.addColumn('string', 'ToolTip');

        // For each orgchart box, provide the name, manager, and tooltip to show.
        data.addRows([

		<?php
		// Generate each affiliate
		foreach($affiliates as $affiliate_id => $args) :
            
			// Affiliate Objects
			$affiliate_obj = $args['affiliate_object'];
			$p_affiliate_obj = $args['parent_affiliate_object'];
            
            /************* Editable Area *************/
            
            // Both display names must be identical for chart to render properly
            $display_name = affwp_get_affiliate_name( $affiliate_obj->affiliate_id );
            $parent_display_name = affwp_get_affiliate_name( $p_affiliate_obj->affiliate_id );
            $affiliate_status = affwp_get_affiliate_status( $affiliate_id );
			$tooltip = '';
            
            $child_node = '<div class="child_node '. $affiliate_status.' affiliate_id_'.$affiliate_id.' ">'.$display_name.'</div>';
            
            /*********** END Editable Area ***********/
        ?>

		[{ v: '<?php echo $display_name ;?>', f: '<?php echo $child_node ?>' }, '<?php echo $parent_display_name ;?>', '<?php echo $tooltip ;?>'],
		
		<?php endforeach; ?>

        ]);

      var options = {
      	allowHtml: true,
      	allowColapse: true,
      	size: 'medium',
      	nodeClass: 'mla_network_node',
      	selectedNodeClass: 'mla_network_node_selected'
	   };

      var chart_div = document.getElementById('chart_div');
      // Create the chart.
      var chart = new google.visualization.OrgChart(chart_div);
      google.visualization.events.addListener(chart, 'ready', chartReadyHandler);
      // Draw the chart, setting the allowHtml option to true for the tooltips.
      chart.draw(data, options);

  }

  function chartReadyHandler() {

      $("#loading").remove();

      var elem = $(".persons_info_tooltip").wrap('<p/>').parent();
      var html = elem.html();
      elem.remove();
      $("body").append(html);

      var persons = $("div[class*=affiliate_id_]");
      persons.hover(showTooltip, hideTooltip);

      var tooltips = $("div.affiliate_tooltips_");
      tooltips.hover(function () {
          $(this).stop().fadeIn();
      }, function () {
          $(this).stop().fadeOut();
      })

      function showTooltip() {
          var t = this;
          var id = $(t).attr("class").match(/\d+/)[0];
          var tooltip = $("div.affiliate_tooltip_id_" + id);
          var top = $(t).offset().top + $(t).outerHeight();
          var left = $(t).offset().left + ($(t).outerWidth() - tooltip.outerWidth()) * 0.5;

          if (!isScrolledIntoView(tooltip, top + 10)) {
              top = $(t).offset().top - tooltip.outerHeight() - 20;
              tooltip.addClass("arrow_bottom").removeClass("arrow_top");
          } else {
              tooltip.addClass("arrow_top").removeClass("arrow_bottom");
          }

          tooltip.css("top", top);
          tooltip.css("left", left);
          tooltip.stop().fadeIn();
      }

      function hideTooltip() {
          var t = this;
          var id = $(t).attr("class").match(/\d+/)[0];
          var tooltip = $("div.affiliate_tooltip_id_" + id);
          tooltip.stop().fadeOut();
      }

      function isScrolledIntoView(elem, top) {
          var docViewTop = $(window).scrollTop();
          var docViewBottom = docViewTop + $(window).height();

          var elemBottom = top + $(elem).outerHeight();

          return ((elemBottom <= docViewBottom) && (top >= docViewTop));
      }

  }

</script>

<!-- Show Preloader -->
<?php if( !empty($affiliates ) ) : ?>
<div id="loading" style="background">Loading Network Chart.......</div>
<?php endif; ?>

<!-- The HTML for Tooltips starts here -->
<div class="persons_info_tooltip">
    <?php 
	$common = new AffiliateWP_MLA_Common();
	
	foreach($affiliates as $affiliate_id => $args) :
	
	$modal_content = array(
			'Affiliate ID' 	=> $affiliate_id,
			'Status' 		=> affwp_get_affiliate_status( $affiliate_id ),
			'Earnings'		=> affwp_get_affiliate_earnings( $affiliate_id, TRUE ),
		);	
	
	// Info for binary mode only
	if ( $common->mla_mode() == 'binary' ) {
	
		$binary = new AffiliateWP_MLA_Binary( $affiliate_id );
		
		$previous_month_date_vars = array( 'timeframe' => 'other_month', 'offset' => -1 );
		
		$modal_content['Current Month TV'] = affwp_currency_filter( $binary->monthly_sales_volume_factory( array( 'mode' => 'network' ) ) );
		$modal_content['Current Month PV'] = affwp_currency_filter( $binary->monthly_sales_volume_factory( array( 'mode' => 'single' ) ) );
		$modal_content['Previous Month TV'] = affwp_currency_filter( $binary->monthly_sales_volume_factory( array( 'mode' => 'network' ) + $previous_month_date_vars ) );
		$modal_content['Previous Month PV'] = affwp_currency_filter( $binary->monthly_sales_volume_factory( array( 'mode' => 'single' ) + $previous_month_date_vars ) );
	
	}//elseif( $common->mla_mode() == 'matrix' ) {}
	
	$modal_content = apply_filters( 'mla_admin_chart_modal_data', $modal_content, $common->mla_mode(), $affiliate_id );
	
	?>
    <div class="affiliate_tooltips_ affiliate_tooltip_id_<?php echo $affiliate_id ;?>">
    	<h4><?php echo affwp_get_affiliate_name( $affiliate_id ); ?></h4>
    	<?php foreach( $modal_content as $title => $info ) : ?>
        <div class="aff_info_row"><?php echo $title.': '.$info; ?></div>
        <?php endforeach ;?>
        <div class="chart_affiliate_actions">
        <!--<span>Actions:<span>-->
        <!--<span><a target="_blank" href="<?php //echo get_site_url( '', '/wp-admin/admin.php?page=affiliate-wp-affiliates&action=edit_affiliate&affiliate_id='.$affiliate_id ); ?>">Edit Affiliate</a><span>-->
    	</div>
    </div>
    <?php endforeach; ?>
</div>

<?php endif; ?>