<?php
	$affiliate = new AffiliateWP_MLA_Affiliate( $data->affiliate_id );
	$affiliates = $affiliate->get_entire_network();
	if(!empty($affiliates) ) {
?>

<style type="text/css">

.affwp-tab-content_mla_chart { overflow: auto;max-width: auto;max-height: auto;}
.affwp-tab-content_mla_chart table tr { border-bottom: none!important;}

/*all nodes*/
.affwp-tab-content_mla_chart .mla_network_node {
    text-align: center;
    vertical-align: middle;
    border: none;;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -webkit-box-shadow: rgba(0, 0, 0, 0.5) 3px 3px 3px;
    -moz-box-shadow: rgba(0, 0, 0, 0.5) 3px 3px 3px;
	padding: 0px;
	color: #ffffff;
	height: 68px;
}

/*the top affiliate node*/
#chart_div > table > tbody > tr:nth-child(2) > td.mla_network_node.google-visualization-orgchart-node-medium {
	padding-top: 5px;
	padding-bottom: 5px;
	height: 40px;
	background-color: #0593CF;
}

/*all child nodes*/
.affwp-tab-content_mla_chart .mla_network_node .child_node {
	-moz-border-radius: 5px;
    -webkit-border-radius: 5px;
	padding-top: 10px;
	height: 68px;
}

/*active affiliates*/
.affwp-tab-content_mla_chart .mla_network_node .child_node.active {
	background-color: #0593CF;	
}

/*inactive affiliates*/
.affwp-tab-content_mla_chart .mla_network_node .child_node.inactive {
	background-color: #B4B4B4;	
}


#loading { position: absolute; width: 100%; background: url('spinner.gif') no-repeat center center;}

</style>

<div id="affwp-tab-content_mla_chart" class="affwp-tab-content_mla_chart">

 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
      google.charts.load('current', {packages:["orgchart"]});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Affiliate Name');
        data.addColumn('string', 'Parent Affiliate');
        data.addColumn('string', 'ToolTip');

        // For each orgchart box, provide the name, manager, and tooltip to show.
        data.addRows([
		
		<?php
		// Generate each affiliate
		foreach($affiliates as $affiliate_id => $args) :
		
		// 'WP_User' Objects below
		$user_info = $args['user_info'];
		$parent_user_info = $args['parent_user_info'];
		
		/************* Editable Area *************/
		
		// Both display names must be identical for chart to render properly
		$user_display_name = $user_info->display_name;
		$parent_display_name = $parent_user_info->display_name;
		$affiliate_status = affwp_get_affiliate_status( $affiliate_id );
		
		$earnings = affwp_get_affiliate_earnings( $affiliate_id, true );
		$tooltip = 'Affiliate ID: '.$affiliate_id.' - Status: '.$affiliate_status;
		
		$child_node = '<div class="child_node '. $affiliate_status.'">'.$user_info->display_name.'</br></br> '.$earnings.'</div>';
		
		/*********** END Editable Area ***********/
		?>
		
		[{v:'<?php echo $user_display_name ;?>', f:'<?php echo $child_node ?>'}, '<?php echo $parent_display_name ;?>', '<?php echo $tooltip ;?>'],
		
		<?php endforeach; ?>
		
        ]);
		
		var options = {
          allowHtml: true,
          allowColapse: true,
		  size: '<?php $data->chart_size ; ?>',
		  nodeClass: 'mla_network_node',
    	  selectedNodeClass: 'mla_network_node_selected'
        };

        // Create the chart.
        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        // Draw the chart, setting the allowHtml option to true for the tooltips.
        chart.draw(data, options);
      }
   </script>

   
<div id="chart_div"></div>

<?php do_action( 'mla_dashboard_after_network_chart', $data->affiliate_id ); ?>
   
</div>

<?php } else {?>
<div>Your network is empty</div>
<?php } ?>


<?php if( $data->show_preloader && !empty($affiliates) ) : ?>
<div id="loading" style="background">Loading Network Chart.......</div>
<script type = "text/javascript">
	window.onload = function() {document.getElementById('loading').style.visibility='hidden'}
</script>
<?php endif; ?>