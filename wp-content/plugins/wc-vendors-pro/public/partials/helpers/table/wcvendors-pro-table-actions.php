<?php

/**
 * Dashboard action URL 
 *
 * This file is used to display a dashboard action url 
 *
 * @link       http://www.wcvendors.com
 * @since      1.0.0
 *
 * @package    WCVendors_Pro
 * @subpackage WCVendors_Pro/public/partials/helpers/table
 */

?>

<div class="row-actionss row-actions-<?php echo $this->id?>"> 
<style> 
.wcv-grid table .row-actions-order a {
    font-size: .875em;
    display: block;
    text-decoration: none;
    line-height: 1.5em;
    margin-bottom: 2px;
    margin-top: 3px;
}
.entry table td {
    padding: 9px;
    border-top: 1px solid #ffffff;
    border-bottom: 1px solid #e0e0e0;
    border-left: 1px solid #e0e0e0;
    background: #fafafa;
    background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
    background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
    max-width: 100px;
    overflow: hidden;
}
</style>
<?php 
	
	foreach ( $this->actions as $action => $details ) { 

		if ( !empty( $details ) ) { 

			if ( empty( $details['url'] ) ) { 
				if ( $action == 'view' ) { 
					$action_url = get_permalink( $object_id ); 
				} else { 
					$action_url = WCVendors_Pro_Dashboard::get_dashboard_page_url( $this->post_type . '/' . $action . '/' . $object_id );
				} 
			} else { 
				$action_url = $details[ 'url' ];
			} 
		} 

		( ! empty( $details[ 'class' ] ) ) ? $class='class="' .$details[ 'class' ]. '"' : $class = '';
		( ! empty( $details[ 'id' ] ) ) ? $id='id="' .$details[ 'id' ]. '"' : $id = ''; 
		( ! empty( $details[ 'target' ] ) ) ? $target='target="' .$details[ 'target' ]. '"' : $target = ''; 
		$custom = ''; 
		if ( ! empty( $details[ 'custom' ] ) ) { 
			foreach ($details['custom'] as $attr => $value ) {
				$custom .= $attr. '="'. $value .'" '; 
			}

		}

		echo '<a href="'. $action_url.'" '.$id.' '.$class.' '.$target.' '.$custom.' >'. $details[ 'label' ].'</a>'; 
	} 
?>
</div>

