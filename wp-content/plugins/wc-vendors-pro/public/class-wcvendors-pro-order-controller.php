<?php
/**
 * The WCVendors Pro order Controller class
 *
 * This is the order controller class for all front end order management 
 *
 * @package    WCVendors_Pro
 * @subpackage WCVendors_Pro/public
 * @author     Jamie Madden <support@wcvendors.com>
 */
class WCVendors_Pro_Order_Controller {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wcvendors_pro    The ID of this plugin.
	 */
	private $wcvendors_pro;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Is the plugin in debug mode 
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      bool    $debug    plugin is in debug mode 
	 */
	private $debug;

	/**
	 * Is the plugin base directory 
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $base_dir  string path for the plugin directory 
	 */
	private $base_dir;

	/**
	 * The tables header rows 
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $columns    The table columns
	 */
	private $columns;

	/**
	 * The table rows 
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $rows    The table rows
	 */
	private $rows;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wcvendors_pro    The ID of this plugin.
	 */
	private $controller_type;

	private static $billing_fields; 
	private static $shipping_fields; 

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wcvendors_pro       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wcvendors_pro, $version, $debug ) {

		$this->wcvendors_pro 	= $wcvendors_pro;
		$this->version 			= $version;
		$this->debug 			= $debug; 
		$this->base_dir			= plugin_dir_path( dirname(__FILE__) ); 
		$this->controller_type 	= 'order'; 

		$pv_options = get_option( 'wc_prd_vendor_options' ); 
		$orders_sales_range = ( isset( $pv_options[ 'orders_sales_range' ] ) ) ? $pv_options[ 'orders_sales_range' ] : 'monthly'; 
		$default_start = ''; 

		switch ( $orders_sales_range ) {
			case 'annually':
				$default_start = '-1 year'; 
				break;
			case 'quarterly':
				$default_start = '-3 month'; 
				break;
			case 'monthly':
				$default_start = '-1 month'; 
				break;
			case 'weekly':
				$default_start = '-1 week'; 
				break;
			case 'custom':
				$default_start = '-1 year'; 
				break;
			default:
				break;
		}

		$this->start_date 	= ( !empty( $_SESSION[ 'PV_Session' ][ '_wcv_order_start_date_input' ] ) ) 	? $_SESSION[ 'PV_Session' ][ '_wcv_order_start_date_input' ] : strtotime( apply_filters( 'wcv_order_start_date', $default_start ) ); 
		$this->end_date 	= ( !empty( $_SESSION[ 'PV_Session' ][ '_wcv_order_end_date_input' ] ) ) 	? $_SESSION[ 'PV_Session' ][ '_wcv_order_end_date_input' ] : strtotime( apply_filters( 'wcv_order_end_date', 'now' ) ); 		

		$this->columns 			= $this->table_columns(); 
		$this->rows 			= $this->table_rows(); 


		self::$billing_fields = apply_filters( 'wcv_order_billing_fields', array(
			'first_name' => array(
				'label' => __( 'First Name', 'woocommerce' ),
				'show'  => false
			),
			'last_name' => array(
				'label' => __( 'Last Name', 'woocommerce' ),
				'show'  => false
			),
			'company' => array(
				'label' => __( 'Company', 'woocommerce' ),
				'show'  => false
			),
			'address_1' => array(
				'label' => __( 'Address 1', 'woocommerce' ),
				'show'  => false
			),
			'address_2' => array(
				'label' => __( 'Address 2', 'woocommerce' ),
				'show'  => false
			),
			'city' => array(
				'label' => __( 'City', 'woocommerce' ),
				'show'  => false
			),
			'postcode' => array(
				'label' => __( 'Postcode', 'woocommerce' ),
				'show'  => false
			),
			'country' => array(
				'label'   => __( 'Country', 'woocommerce' ),
				'show'    => false,
				'class'   => 'js_field-country select short',
				'type'    => 'select',
				'options' => array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WCVendors_Pro_Form_Helper::countries()
			),
			'state' => array(
				'label' => __( 'State/County', 'woocommerce' ),
				'class'   => 'js_field-state select short',
				'show'  => false
			),
			'email' => array(
				'label' => __( 'Email', 'woocommerce' ),
			),
			'phone' => array(
				'label' => __( 'Phone', 'woocommerce' ),
			),
		) );

		self::$shipping_fields = apply_filters( 'wcv_order_shipping_fields', array(
			'first_name' => array(
				'label' => __( 'First Name', 'woocommerce' ),
				'show'  => false
			),
			'last_name' => array(
				'label' => __( 'Last Name', 'woocommerce' ),
				'show'  => false
			),
			'company' => array(
				'label' => __( 'Company', 'woocommerce' ),
				'show'  => false
			),
			'address_1' => array(
				'label' => __( 'Address 1', 'woocommerce' ),
				'show'  => false
			),
			'address_2' => array(
				'label' => __( 'Address 2', 'woocommerce' ),
				'show'  => false
			),
			'city' => array(
				'label' => __( 'City', 'woocommerce' ),
				'show'  => false
			),
			'postcode' => array(
				'label' => __( 'Postcode', 'woocommerce' ),
				'show'  => false
			),
			'country' => array(
				'label'   => __( 'Country', 'woocommerce' ),
				'show'    => false,
				'type'    => 'select',
				'class'   => 'js_field-country select short',
				'options' => array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WCVendors_Pro_Form_Helper::countries()
			),
			'state' => array(
				'label' => __( 'State/County', 'woocommerce' ),
				'class'   => 'js_field-state select short',
				'show'  => false
			),
		) );


	}

	/**
	 * Display the custom order table 
	 *
	 * @since    1.0.0
	 */
	public function display() { 

		// Use the internal table generator to create object list 
		$order_table = new WCVendors_Pro_Table_Helper( $this->wcvendors_pro, $this->version, $this->controller_type, null, get_current_user_id() ); 

		$order_table->set_columns( $this->columns ); 
		$order_table->set_rows( $this->rows ); 

		// display the table 
		$order_table->display(); 

	}

	/**
	 *  Process the form submission from the front end. 
	 *
	 * @since    1.0.0
	 */
	public function process_submit() {


		if ( isset( $_GET[ 'wcv_mark_shipped' ] ) ) { 

			$vendor_id = get_current_user_id(); 
			$order_id =  $_GET[ 'wcv_mark_shipped' ];
             
			self::mark_shipped( $vendor_id, $order_id ); 
		}
		
		if ( isset( $_GET[ 'wcv_mark_accept' ] ) ) { 

			$vendor_id = get_current_user_id(); 
			$order_id =  $_GET[ 'wcv_mark_accept' ];

			self::mark_accept_refund( $vendor_id, $order_id ); 
		}
		
		if ( isset( $_GET[ 'wcv_mark_notaccept' ] ) ) { 

			$vendor_id = get_current_user_id(); 
			$order_id =  $_GET[ 'wcv_mark_notaccept' ];

			self::mark_notaccept_refund( $vendor_id, $order_id ); 
		}
		

		if ( isset( $_GET['wcv_shipping_label'] ) ) { 

			$vendor_id = get_current_user_id(); 
			$order_id =  $_GET[ 'wcv_shipping_label' ];

			self::shipping_label( $vendor_id, $order_id ); 
		}

		if ( isset( $_GET['wcv_export_orders'] ) ) { 

			$vendor_id = get_current_user_id(); 
			$this->export_csv(); 
		}

		if ( isset( $_POST['wcv_order_id'] ) && isset( $_POST[ 'wcv_add_note'] ) ) { 
		
			if ( !wp_verify_nonce( $_POST[ 'wcv_add_note' ], 'wcv-add-note' ) ) return false;

			$order_id 	= (int) $_POST[ 'wcv_order_id' ];
			$comment 	= $_POST[ 'wcv_comment_text' ];  

			if ( empty( $comment ) ) {
				wc_add_notice( __( 'You need type something in the note field', 'wcvendors-pro' ), 'error' );
				return false;
			}
             $comment = $comment . "http://www.doityourselfnation.org/bit_bucket/contact-respond/?orderid=" . $order_id ;
			self::add_order_note( $order_id, $comment ); 
		}

		if ( isset( $_POST[ 'wcv_add_tracking_number' ] ) ) { 
		
			if ( !wp_verify_nonce( $_POST[ 'wcv_add_tracking_number' ], 'wcv-add-tracking-number' ) ) return false;

			self::update_shipment_tracking(); 
		}


		//  Process the date updates for the form
		if ( isset( $_POST[ 'wcv_order_date_update' ] ) ) { 

			if ( !wp_verify_nonce( $_POST[ 'wcv_order_date_update' ], 'wcv-order-date-update' ) ) return; 

			// Start Date 
			if ( isset( $_POST[ '_wcv_order_start_date_input' ] ) || '' === $_POST[ '_wcv_order_start_date_input' ] ) { 
				$this->start_date = strtotime( $_POST[ '_wcv_order_start_date_input' ] ); 
				$_SESSION[ 'PV_Session' ][ '_wcv_order_start_date_input' ] = strtotime( $_POST[ '_wcv_order_start_date_input' ] );
			} 

			// End Date 
			if ( isset( $_POST[ '_wcv_order_end_date_input' ] ) || '' === $_POST[ '_wcv_order_end_date_input' ] ) { 
				$this->end_date = strtotime( $_POST[ '_wcv_order_end_date_input' ] ); 
				$_SESSION[ 'PV_Session' ][ '_wcv_order_end_date_input' ] = strtotime( $_POST[ '_wcv_order_end_date_input' ] );
			} 

		}

	} // process_submit() 

	/**
	 *  Process the delete action 
	 *
	 * @since    1.0.0
	 */
	public function process_delete( ) { 

	} // process_delete() 

	
	/**
	 *  Update Table Headers for display
	 * 
	 * @since    1.0.0
	 * @param 	 array 	$headers  array passed via filter 
	 */
	public function table_columns( ) {

		$columns = apply_filters( 'wcv_order_table_columns', array( 
					'ID' 			=> __( 'ID', 			'wcvendors-pro' ), 
					'order_number'	=> __( 'Order', 		'wcvendors-pro' ),
					'customer'  	=> __( 'Customer', 		'wcvendors-pro' ),
					'products'  	=> __( 'Products', 		'wcvendors-pro' ), 
					'total'  		=> __( 'Total', 		'wcvendors-pro' ), 
					'status'    	=> __( 'Ship ped', 		'wcvendors-pro' ), 
					'order_date'  	=> __( 'Order-Date', 	'wcvendors-pro' ), 
		) ); 

		return $columns;

	} // table_columns() 

	/**
	 *  create the table data 
	 * 
	 * @since    1.0.0
	 * @return   array  $new_rows   array of stdClass objects passed back to the filter 
	 */
	public function table_rows( ) {
		
		$date_range = array( 
			'before' => date( 'Y-m-d', $this->end_date ), 
			'after'  => date( 'Y-m-d', $this->start_date ), 
		); 

		$all_orders = WCVendors_Pro_Vendor_Controller::get_orders2( get_current_user_id(), $date_range ); 

		$rows = array(); 

		if ( !empty( $all_orders ) ) { 

			foreach ( $all_orders as $_order ) { 

				$order 			= $_order->order; 
				$products_html 	= ''; 
				$needs_shipping = false; 
				$needs_to_ship 	= false; 
				$downloadable 	= false; 		

				if ( !empty( $_order->order_items ) ){ 

					foreach ( $_order->order_items as $item ) { 
					
						$product_id = !empty( $item['variation_id'] ) ? $item['variation_id'] : $item['product_id'];
						$_product = new WC_Product( $product_id );

						$needs_shipping 	= $_product->is_virtual(); 
						if ( !$needs_shipping ) $needs_shipping = 0; 

						$downloadable 		= ( $_product->is_downloadable('yes') ) ?  true: false; 
						if ( $downloadable == null )  $downloadable = 0; 
						$products_html .= '<strong>'. $item['qty'] . ' x ' . $item['name'] . '</strong><br />'; 

						if ( ! empty( $item[ 'item_meta_array' ] ) ) { 

							foreach ( $item[ 'item_meta_array' ] as $meta ) {

								// Skip hidden core fields
								if ( in_array( $meta->key, apply_filters( 'woocommerce_hidden_order_itemmeta', array(
									'_qty',
									'_tax_class',
									'_product_id',
									'_variation_id',
									'_line_subtotal',
									'_line_subtotal_tax',
									'_line_total',
									'_line_tax',
									'method_id', 
									'cost', 
									WC_Vendors::$pv_options->get_option( 'sold_by_label' ), 
								) ) ) ) {
									continue;
								}

								// Skip serialised meta
								if ( is_serialized( $meta->value ) ) {
									continue;
								}

								// Get attribute data
								if ( taxonomy_exists( wc_sanitize_taxonomy_name( $meta->key ) ) ) {
									$term           = get_term_by( 'slug', $meta->value, wc_sanitize_taxonomy_name( $meta->key ) );
									$meta->key  	= wc_attribute_label( wc_sanitize_taxonomy_name( $meta->key ) );
									$meta->value 	= isset( $term->name ) ? $term->name : $meta->value;
								} else {
									$meta->key   	= apply_filters( 'woocommerce_attribute_label', wc_attribute_label( $meta->key, $_product ), $meta->key );
								}

								$products_html .= '<strong>' . wp_kses_post( rawurldecode( $meta->key ) ) . '</strong> : ' . wp_kses_post( rawurldecode( $meta->value ) ) . '<br />';
							}
						}


						$needs_to_ship = ( $needs_shipping || !$downloadable ) ? true : false; 
					}

				}

				$shippers = (array) get_post_meta( $order->id, 'wc_pv_shipped', true ); 
				$has_shipped = in_array( get_current_user_id() , $shippers ) ? __( 'Yes', 'wcvendors-pro' )  : __( 'No', 'wcvendors-pro' ); 
				$shipped = ( $needs_to_ship ) ? $has_shipped : __( 'NA', 'wcvendors-pro' ) ; 
                
				$statsus = get_post_meta($order->id, '_wcv_refund_cus', true );
				$refund_stats = get_post_meta($order->id, 'wc_pv_refund_accept', true );
				
				$row_actions = apply_filters( 'wcv_orders_row_actions_' . $order->get_order_number(), array( 
					'view_details'  	=> 
							array(  
								'label' 	=> __( 'View Order Details', 		'wcvendors-pro' ), 
								'url' 		=> '#', 
								'custom'	=> array( 
										'id'			=> 'open-order-details-modal-' . $order->get_order_number(), 
									),
							), 
					'print_label'  	=> 
							array(  
								'label' 	=> __( 'Shipping Label', 	'wcvendors-pro' ), 
								'url' 		=> '?wcv_shipping_label='. $order->get_order_number(), 
								'target' 	=> '_blank' 
							), 
					'add_note'  	=> 
							array(  
								'label' 	=> __( 'Order Note', 		'wcvendors-pro' ), 
								'url' 		=> '#', 
								'custom'	=> array( 
										'id'			=> 'open-order-note-modal-' . $order->get_order_number(), 
									),
							), 
					'add_tracking'  => 
							array(  
								'label' 	=> __( 'Tracking Number', 	'wcvendors-pro' ), 
								'url' 		=> '#', 
								'custom'	=> array( 
										'id'			=> 'open-tracking-modal-' . $order->get_order_number(), 
									),
							),		

				), $order->get_order_number() ); 

				if ( !$needs_to_ship ) { 
					unset( $row_actions['print_label'] ); 
					unset( $row_actions['add_tracking'] ); 
				} 
				
				//  If it hasn't been shipped then provide a link to mark as shipped. 
				if ( __( 'No', 'wcvendors-pro' ) == $shipped ) { 
					$row_actions['mark_shipped'] = array( 
						'label' 	=> __( 'Mark Shipped', 'wcvendors-pro' ), 
						'url' 		=> '?wcv_mark_shipped='.$order->get_order_number() 
					); 
				} 

				$hide_view_details 		= WC_Vendors::$pv_options->get_option( 'hide_order_view_details' );
				$hide_shipping_label 	= WC_Vendors::$pv_options->get_option( 'hide_order_shipping_label' );
				$hide_order_note 		= WC_Vendors::$pv_options->get_option( 'hide_order_order_note' );
				$hide_tracking_number	= WC_Vendors::$pv_options->get_option( 'hide_order_tracking_number' );
				$hide_mark_shipped		= WC_Vendors::$pv_options->get_option( 'hide_order_mark_shipped' );
				
				 if(!empty($statsus))
				{
					
				  $row_actions['refund_us'] = array( 
						'label' 	=> __( 'Accept Refund', 'wcvendors-pro' ), 
						'url' 		=> '?wcv_mark_accept='.$order->get_order_number() 
					);
					
				  $row_actions['decline_us'] = array( 
						'label' 	=> __( 'Decline Refund', 'wcvendors-pro' ), 
						'url' 		=> '?wcv_mark_notaccept='.$order->get_order_number() 
					);
					
                   $hide_view_details = 'yes';
                   $hide_shipping_label = 'yes';	
                   $hide_order_note = 'yes';
                   $hide_tracking_number = 'yes';
                   $hide_mark_shipped = 'yes';				   
				}
				if($refund_stats == 1){
					
					 $hide_refund_us = 'yes';
                     $hide_decline_us = 'yes';
					 
					 $row_actions['refund_accept'] = array( 
						'label' 	=> __( 'Refunded', 'wcvendors-pro' ), 
						'url' 		=> '#' 
					);
				   
					}
					
               /* if($refund_stats == 0){
					
					 $hide_refund_us = 'yes';
                     $hide_decline_us = 'yes';
					 $hide_refund_accept = 'yes';
					 
				   $hide_view_details = '';
                   $hide_shipping_label = '';	
                   $hide_order_note = '';
                   $hide_tracking_number = '';
                   $hide_mark_shipped = '';
				   
					}*/
				if ( $hide_view_details && array_key_exists( 'view_details', $row_actions ) ){  unset( $row_actions[ 'view_details' ] );  }
				if ( $hide_shipping_label && array_key_exists( 'print_label', $row_actions ) ){  unset( $row_actions[ 'print_label' ] ); }
				if ( $hide_order_note && array_key_exists( 'add_note', $row_actions ) ){  unset( $row_actions[ 'add_note' ] );  }
				if ( $hide_tracking_number && array_key_exists( 'add_tracking', $row_actions ) ){  unset( $row_actions[ 'add_tracking' ] );  }
				if ( $hide_mark_shipped && array_key_exists( 'mark_shipped', $row_actions ) ){  unset( $row_actions[ 'mark_shipped' ] );  }
				
				if ( $hide_refund_us && array_key_exists( 'decline_us', $row_actions ) ){  unset( $row_actions[ 'decline_us' ] );  }
				if ( $hide_decline_us && array_key_exists( 'refund_us', $row_actions ) ){  unset( $row_actions[ 'refund_us' ] );  }
				if ( $hide_refund_accept && array_key_exists( 'refund_accept', $row_actions ) ){  unset( $row_actions[ 'refund_accept' ] );  }

				$commission_due 	= sprintf( get_woocommerce_price_format(), get_woocommerce_currency_symbol( $order->get_order_currency() ), $_order->total_due );
				$shipping_due 		= sprintf( get_woocommerce_price_format(), get_woocommerce_currency_symbol( $order->get_order_currency() ), $_order->total_shipping );
				$tax_due 			= sprintf( get_woocommerce_price_format(), get_woocommerce_currency_symbol( $order->get_order_currency() ), $_order->total_tax );
				$total_text 		= '<span class="wcv-tooltip" data-tip-text="'. sprintf( '%s %s %s %s %s %s', __( 'Product: ' , 'wcvendors-pro'), $commission_due, __( 'Shipping: ' , 'wcvendors-pro'), $shipping_due, __( 'Tax: ' , 'wcvendors-pro'), $tax_due ).'">'.wc_price( $_order->commission_total ).'</span>'; 

				$new_row = new stdClass(); 

				$can_view_emails 	= WC_Vendors::$pv_options->get_option( 'can_view_order_emails' );
				$hide_phone 		= WC_Vendors::$pv_options->get_option( 'hide_order_customer_phone' );

				$customer_details = $order->get_formatted_shipping_address().'<br />'; 

				if ( $can_view_emails ) { 
					$customer_details .= $order->billing_email . '<br />'; 
				}

				if ( ! $hide_phone ){ 
					$customer_details .= $order->billing_phone; 
				}

				$new_row->ID			= $order->get_order_number(); 
				$new_row->order_number	= $order->get_order_number(); 
				$new_row->customer		= $customer_details; 
				$new_row->products 		= $products_html;
				$new_row->total 		= $total_text;
				$new_row->status 		= $shipped;
				$new_row->order_date	= date_i18n( wc_date_format(), strtotime( $order->order_date ) ); 
				$new_row->row_actions 	= $row_actions; 
				$new_row->action_after 	= $this->order_details_template( $_order ) . $this->order_note_template( $order->get_order_number() ) . $this->tracking_number_template( $order->get_order_number(), get_current_user_id() );

				do_action( 'wcv_orders_add_new_row', $new_row ); 

				$rows[] = $new_row; 

			} 
		} // check for orders 

		return apply_filters( 'wcv_orders_table_rows', $rows ); 

	} // table_rows() 


	/**
	 *  Change the column that actions are displayed in 
	 * 
	 * @since    1.0.0
	 * @param 	 string $column  		column passed from filter 
	 * @return   string $new_column   	new column passed back to filter 
	 */
	public function table_action_column( $column ) {

		$new_column = 'order_date'; 
		return $new_column; 

	}

	/**
	 *  Add actions before and after the table 
	 * 
	 * @since    1.0.0
	 */
	public function table_actions() {

		$add_url = '?wcv_export_orders'; 
		include('partials/order/wcvendors-pro-order-table-actions.php');
	}

	/**
	 *  Change the column that actions are displayed in 
	 * 
	 * @since    1.0.0
	 * @param 	 string $column  		column passed from filter 
	 * @return   string $new_column   	new column passed back to filter 
	 */
	public function table_no_data_notice( $notice ) {

		$notice = apply_filters( 'wcv_orders_table_no_data_notice', __( 'No orders found.', 'wcvendors-pro' ) ); 
		return $notice; 
	}

	/**
	 *  Get the store id of the vendor
	 * 
	 * @since    1.0.0
	 * @param 	 array 	$vendor_id  which vendor is being mark shipped
	 * @param 	 array 	$order_id  which order is being marked shipped 
	 * @todo 	clean up the code to bring into newer code standards
	 */
	public static function mark_shipped( $vendor_id, $order_id ) {

		global $woocommerce; 

		$store_name 	= WCV_Vendors::get_vendor_shop_name( $vendor_id );
		$shippers 		= (array) get_post_meta( $order_id, 'wc_pv_shipped', true );
		
		$order_tracking_details = get_post_meta( $order_id, '_wcv_tracking_details', true ); 
		//$vendor_id = get_current_user_id(); 
		$_wcv_tracking_number = $order_tracking_details['_wcv_tracking_number'];
		/*$vendor_tracking_details = array( 
			 '_wcv_shipping_provider' 	=> $_POST['_wcv_shipping_provider_'. $order_id ], 
			 '_wcv_tracking_number' 	=> $_POST['_wcv_tracking_number_' .$order_id ], 
			 '_wcv_date_shipped' 		=> $_POST['_wcv_date_shipped_' . $order_id ], 
		); */
		
		
		$order 			= new WC_Order( $order_id ); 
        if(!empty( $_wcv_tracking_number ) ){
		if( !in_array( $vendor_id, $shippers ) ) {
			
			$shippers[] = $vendor_id;
			$mails = $woocommerce->mailer()->get_emails();
			
			if ( !empty( $mails ) ) {
				$mails[ 'WC_Email_Notify_Shipped' ]->trigger( $order_id, $vendor_id );
			}
			
			do_action( 'wcvendors_vendor_ship', $order_id, $vendor_id );

			wc_add_notice( __( 'Order marked shipped.', 'wcvendors' ), 'success' );

		}

		update_post_meta( $order_id, 'wc_pv_shipped', $shippers );
		} else{
			update_post_meta( $order_id, 'wc_pv_shipped_notship', '0' );
			//echo "Your Need add tracking number by clcik tracking url";
		}
	} 

	public static function mark_accept_refund( $vendor_id, $order_id ) {

		global $woocommerce; 

		$store_name 	= WCV_Vendors::get_vendor_shop_name( $vendor_id );
		$accept 		= 1;
		//$order 			= new WC_Order( $order_id ); 
		$refund_stats = get_post_meta($order_id, '_wcv_refund_amount', true );
		$mails = $woocommerce->mailer()->get_emails();
			
			if ( !empty( $mails ) ) {
				$mails[ 'WC_Email_Notify_Refund_Accept' ]->trigger( $order_id, $vendor_id );
			}
			
			do_action( 'wcvendors_vendor_ship', $order_id, $vendor_id );

			wc_add_notice( __( 'Refund Accept from Author.', 'wcvendors' ), 'success' );
			
         // self::mysite_refunded($order_id);
		   // $order = new WC_Order_Refund( $order_id );
          // $order->update_status( 'refunded' );
		  $default_args = array(
		  'amount'         => $refund_stats,
		  'order_id'       => $order_id,
		  
		  );
		   wc_create_refund($default_args);
		//add_action( 'woocommerce_order_status_refunded', 'mysite_refunded', 10, 1);

		update_post_meta( $order_id, 'wc_pv_refund_accept', $accept );
	} 
	
  public static function refund_system_ajax_call()
   {
	   global $woocommerce; 
	   
	   $full_amunt = $_POST['typeamounts'];
	   $typepayment = $_POST['typeamount'];
	   $parsitial = $_POST['parsitial'];
	   $order_number = $_POST['order_number'];
	   
	   $result = '';
	   $refund_amound = 0;
	   $type_refund = '';
	    $refund  = 0;
		if($_POST['typeamount']){
	   switch($typepayment)
	   {
		   case 'partial_refund':
		       if(  $parsitial > $full_amunt)
			   {
				  // $result =" Refund Amount should be less then order Amount";
				  $result = 0;
			   }else{
				   $refund = 1;
				  $type_refund = 'Partial Order' . ' ' . '$'. $parsitial ;   
				  $refund_amound = $parsitial;		
			   }
			  
                			   
			    break;
		   
		   case 'full_refund':
		   
		            $type_refund = 'Full Order';  
					 $refund = 1;
					$refund_amound = $full_amunt;
		         break;
		        
	   }
	   if( $refund == 1){
		   
		   // $result = 'Successfully Submit Your Refund Request';
		    $result = 1;
	        $order 		= new WC_Order( $order_number );
	         $comment = 'Customer Order refund' . ' ' . $type_refund;
	          // add_filter( 'woocommerce_new_order_note_data', array( __CLASS__, 'filter_order_note' ), 10, 2 );
	        $order->add_order_note( $comment, 1 );
	         //  remove_filter( 'woocommerce_new_order_note_data', array( __CLASS__, 'filter_order_note' ), 10, 2 );
			wc_add_notice( __( 'The customer has been notified.', 'wcvendors-pro' ), 'success' ); 	
			
		   // $mails = $woocommerce->mailer()->get_emails();
			
			if ( !empty( $mails ) ) {
				
				$mails[ 'WC_Email_Notify_Vendor_Refund' ]->trigger( $order_number , $type_refund);
				
			}
             update_post_meta( $order_number, '_wcv_refund_amount', $refund_amound ); 	
             update_post_meta( $order_number, '_wcv_refund_cus', 'refunded'); 
			 
           }			
	     echo $result; 
		}
	  die();
   }
	
	public static function mark_notaccept_refund( $vendor_id, $order_id ) {

		global $woocommerce; 

		$store_name 	= WCV_Vendors::get_vendor_shop_name( $vendor_id );
		$accept 		= 0;
		//$order 			= new WC_Order( $order_id ); 
		
		$mails = $woocommerce->mailer()->get_emails();
			
			if ( !empty( $mails ) ) {
				$mails[ 'WC_Email_Notify_Refund_Notaccept' ]->trigger( $order_id, $vendor_id );
			}
			
			do_action( 'wcvendors_vendor_ship', $order_id, $vendor_id );

			wc_add_notice( __( 'Refund Not Accept from Author.', 'wcvendors' ), 'success' );

		
		update_post_meta( $order_id, 'wc_pv_refund_accept', $accept );
	} 
	/**
	 *  Get the store id of the vendor
	 * 
	 * @since    1.0.0
	 * @param 	 array 	$vendor_id  which vendor is being mark shipped
	 * @param 	 array 	$order_id  which order is being marked shipped 
	 * @todo 	 check the vendor is in the order otherwise kick out
	 */
	public static function shipping_label( $vendor_id, $order_id ) {

		$store_name 		= WCV_Vendors::get_vendor_shop_name( $vendor_id );
		$order 				= new WC_Order( $order_id ); 
		$base_dir			= plugin_dir_path( dirname(__FILE__) ); 
		$countries			= WCVendors_Pro_Form_Helper::countries(); 

		$store_address1 	= get_user_meta( $vendor_id, '_wcv_store_address1', 	true ); 
		$store_address2 	= get_user_meta( $vendor_id, '_wcv_store_address2', 	true ); 
		$store_city	 		= get_user_meta( $vendor_id, '_wcv_store_city', 		true ); 
		$store_state	 	= get_user_meta( $vendor_id, '_wcv_store_state',		true ); 
		$store_country		= $countries[ get_user_meta( $vendor_id, '_wcv_store_country', 	true ) ]; 
		$store_postcode		= get_user_meta( $vendor_id, '_wcv_store_postcode', 	true ); 

		$vendor_items 		= WCV_Queries::get_products_for_order( $order->id );
		$vendor_products 	= array();
		$order_items 		= $order->get_items();

		foreach ( $order_items as $key => $value ) {
			if ( in_array( $value[ 'variation_id' ], $vendor_items) || in_array( $value[ 'product_id' ], $vendor_items ) ) {
				$vendor_products[] = $value;
			}
		}

		// Prevent user editing the $_GET variable
		if ( empty( $vendor_products ) ) return; 

		$products_html = ''; 

		foreach ( $vendor_products as $key => $item ) { 
			// May need to fix for variations 
			$_product = new WC_Product( $item['product_id'] ); 
			$products_html .= '<strong>'. $item['qty'] . ' x ' . $item['name'] . '</strong><br />'; 
		}

		wc_get_template( 'shipping-label.php', apply_filters( 'wcvendors_pro_order_shipping_label', array(
			'order' 			=> $order, 
			'store_name'		=> $store_name, 
			'store_address1'	=> $store_address1, 	
			'store_address2'	=> $store_address2, 	
			'store_city' 		=> $store_city,		
			'store_state'		=> $store_state, 	
			'store_country'		=> $store_country,		
			'store_postcode' 	=> $store_postcode,	
			'picking_list'		=> $products_html, 
			) ), 'wc-vendors/dashboard/order/', $base_dir . 'templates/dashboard/order/' );

		die(); 

	}  // shipping_label()

	/**
	 *  Add an order note 
	 * 
	 * @since    1.0.0
	 * @param 	 array 	$note  order note array 
	 */
	public static function add_order_note( $order_id, $comment ) {
		global $woocommerce; 

		$order 		= new WC_Order( $order_id ); 

		if ( is_object( $order ) ) {
			add_filter( 'woocommerce_new_order_note_data', array( __CLASS__, 'filter_order_note' ), 10, 2 );
			$order->add_order_note( $comment, 1 );
			remove_filter( 'woocommerce_new_order_note_data', array( __CLASS__, 'filter_order_note' ), 10, 2 );
			wc_add_notice( __( 'The customer has been notified.', 'wcvendors-pro' ), 'success' );
			
			$mails = $woocommerce->mailer()->get_emails();
			
			$vendor_id = get_current_user_id(); 
			
			if ( !empty( $mails ) ) {
				$mails[ 'WC_Email_Notify_Order_Note_Vendor' ]->trigger( $order_id, $vendor_id );
			}
			
		}

	}

	/**
	 *  Filter the order note
	 * 
	 * @since    1.0.0
	 * @param 	 array 	$commentdata  comment data
	 * @param 	 array  $order 		  order this is relevant to
 	 * @todo 	 clean up the code to bring into newer code standards
	 */
	public static function filter_order_note( $commentdata, $order )
	{
		$user_id = get_current_user_id();

		$commentdata[ 'user_id' ]              = $user_id;
		$commentdata[ 'comment_author' ]       = WCV_Vendors::get_vendor_shop_name( $user_id );
		$commentdata[ 'post_author' ]    	   = $user_id;
		$commentdata[ 'comment_author_url' ]   = WCV_Vendors::get_vendor_shop_page( $user_id );
		$commentdata[ 'comment_author_email' ] = wp_get_current_user()->user_email;

		return $commentdata;
	}

	/**
	 *  Order Note Template
	 * 	
	 * @since    1.0.0
	 * @param 	 int 	$order_id 	order id for notes. 
	 */
	public function order_note_template( $order_id ) { 

		$can_add_comments = WC_Vendors::$pv_options->get_option( 'can_submit_order_comments' );

		$form = ''; 

		if ( $can_add_comments ) {
			ob_start();
			$notes = $this->existing_order_notes( $order_id ); 
			wc_get_template( 'order_note_form.php', array('order_id' => $order_id, 'notes' => $notes ), 'wc-vendors/dashboard/order/', $this->base_dir . 'templates/dashboard/order/' );	
			$form = ob_get_contents();
			ob_end_clean();
		} 

		return $form;
	}


	/**
	 *  Order Details Template
	 * 	
	 * @since    1.0.0
	 * @param 	 int 	$order_id 	order id for notes. 
	 */
	public function order_details_template( $_order ) { 

		$form = ''; 


		// Get line items
		$order 					= $_order->order; 
		$line_items          	= $_order->order_items;
		$billing_fields 		= self::$billing_fields; 
		$shipping_fields 		= self::$shipping_fields; 

		$order_taxes 		 = array(); 

		if ( wc_tax_enabled() ) {
			$order_taxes         = $order->get_taxes();
			$tax_classes         = WC_Tax::get_tax_classes();
			$classes_options     = array();
			$classes_options[''] = __( 'Standard', 'woocommerce' );

			if ( ! empty( $tax_classes ) ) {
				foreach ( $tax_classes as $class ) {
					$classes_options[ sanitize_title( $class ) ] = $class;
				}
			}

			$show_tax_columns = sizeof( $order_taxes ) === 1;
		}

		ob_start();
		
		wc_get_template( 'order_details.php', array( 
			'order' 				=> $order, 
			'_order'				=> $_order, 
			'order_id' 				=> $order->get_order_number(), 
			'line_items' 			=> $line_items, 
			'order_taxes'			=> $order_taxes, 
			'billing_fields'		=> $billing_fields, 
			'shipping_fields'		=> $shipping_fields, 
			), 
		'wc-vendors/dashboard/order/', $this->base_dir . 'templates/dashboard/order/' );	
		
		$form = ob_get_contents();
		ob_end_clean();

		return $form;
	}

	/**
	 *  Existing Order Notes 
	 * 
	 * @since    1.0.0
	 * @param 	 int 	$order_id 	order id for notes. 
	 */
	public function existing_order_notes( $order_id ) { 

		$can_view_comments = WC_Vendors::$pv_options->get_option( 'can_view_order_comments' );
	
		$notes = '';

		if ( ! $can_view_comments ) return ; 
				
		$order_notes = $this->get_vendor_order_notes( $order_id ); 
		
		if ( !empty( $order_notes ) ) { 
			ob_start();
			foreach ( $order_notes as $order_note ) { 
				$time_posted 	= human_time_diff( strtotime( $order_note->comment_date_gmt ), current_time( 'timestamp', 1 ) );
				$note_text 		= $order_note->comment_content; 
				wc_get_template( 'order_note.php', array( 'time_posted' => $time_posted, 'note_text' => $note_text ), 'wc-vendors/dashboard/order/', $this->base_dir . 'templates/dashboard/order/' );	
			}
			$notes = ob_get_contents();
			ob_end_clean();
		}

		return $notes; 
	}

	/**
	 *  Get the vendor notes for an order 
	 * 
	 * @since    1.0.0
	 * @param 	 int 	$order_id 	order id for notes. 
	 */
	 public function get_vendor_order_notes( $order_id ) {

        $notes = array();

        $args = array(
        	'user_id' => get_current_user_id(), 
            'post_id' => $order_id,
            'approve' => 'approve',
            'type' => ''
        );

        remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

        $comments = get_comments( $args );

        foreach ( $comments as $comment ) {

            $is_customer_note = get_comment_meta( $comment->comment_ID, 'is_customer_note', true );

            if ( $is_customer_note ) {
                $notes[] = $comment;
            }
        }

        add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

        return (array) $notes;

    } // get_vendor_order_notes()


   /**
	 *  Trigger the csv export 
	 * 
	 * @param 	 int 	$vendor_id  which vendor is being mark shipped
	 * @since    1.0.0
	 */
	public function export_csv() {

		include_once('class-wcvendors-pro-export-helper.php'); 

		$date_range = array( 
			'before' => date( 'Y-m-d', $this->end_date ), 
			'after'  => date( 'Y-m-d', $this->start_date ), 
		); 

		$csv_output 	= new WCVendors_Pro_Export_Helper( $this->wcvendors_pro, $this->version, $this->debug ); 
		$csv_headers 	= $this->columns; 
		//  remove the ID column as its not required 
		unset( $csv_headers['ID'] ); 
		$csv_headers	= apply_filters( 'wcv_order_export_csv_headers', $csv_headers ); 
		$csv_rows 		= apply_filters( 'wcv_order_export_csv_rows', $csv_output->format_orders_export( WCVendors_Pro_Vendor_Controller::get_orders2( get_current_user_id(), $date_range ) ) );  
		$csv_filename 	= apply_filters( 'wcv_order_export_csv_filename', 'orders' ); 
		
		$csv_output->download_csv( $csv_headers, $csv_rows, $csv_filename );
		
	} // download_csv() 


	/**
	 *  Tracking Number Template
	 * 
	 * @since    1.0.0
	 * @param 	 int 	$order_id 	order id for notes. 
	 */
	public function tracking_number_template( $order_id, $vendor_id ) { 

		$form = ''; 

		ob_start();

		$tracking_details = $this->get_vendor_tracking_details( $order_id, $vendor_id );

		//  Clean up any empty indexes 
		if ( ! isset( $tracking_details['_wcv_shipping_provider'] ) ) 	$tracking_details['_wcv_shipping_provider'] = ''; 
		if ( ! isset( $tracking_details['_wcv_tracking_number'] ) ) 	$tracking_details['_wcv_tracking_number'] 	= ''; 
		if ( ! isset( $tracking_details['_wcv_date_shipped'] ) ) 		$tracking_details['_wcv_date_shipped'] 		= ''; 

		wc_get_template( 'tracking_number.php', array( 
			'order_id' 			=> $order_id, 
			'tracking_details' 	=> $tracking_details 
		), 'wc-vendors/dashboard/order/', $this->base_dir . 'templates/dashboard/order/' );	
		
		$form = ob_get_contents();
		ob_end_clean();
	
		return $form;
	}

	/**
	 *  Tracking Number Template
	 * 
	 * @since    1.0.0
	 * @param 	 int 	$order_id 	order id for notes. 
	 */
	public function get_vendor_tracking_details( $order_id, $vendor_id ) { 

		$order_tracking_details = get_post_meta( $order_id, '_wcv_tracking_details', true ); 	

		if ( $order_tracking_details == '' ) return array(); 

		if ( array_key_exists( $vendor_id, $order_tracking_details ) ) { 
			return $order_tracking_details[ $vendor_id ]; 
		} else { 
			return array(); 
		}	

	} 

	
	
	/**
	 *  Update the order shipment tracking ajax call
	 * 
	 * @since    1.0.0
	 */
	/*public function update_shipment_tracking_alax( ) {
		
        $order_iddta = $_POST['data'];
		$tok = preg_split('/&/',  $order_iddta);
		
		$wcv_shipping_provider = preg_split('/=/',  $tok[0]);
		$_wcv_shipping_provider = $wcv_shipping_provider[1];
		
		$wcv_tracking_number = preg_split('/=/',  $tok[1]);
		$_wcv_tracking_number = $wcv_tracking_number[1];
		
		$wcv_date_shipped = preg_split('/=/',  $tok[2]);
		$_wcv_date_shipped = $wcv_date_shipped[1];
		
		$_wcv_order_id = preg_split('/=/',  $tok[3]);
		$order_id = $_wcv_order_id[1];
		 $fd = false;
		 if($_wcv_shipping_provider == 'FedEx')
		 {
			 if(preg_match("/^[0-9]{12,24}+$/",$_wcv_tracking_number ))
			 {
				  $fd = true;
				
			 } else{
				 $fd = false;
			 }
		 }
		elseif($_wcv_shipping_provider == 'Australia+Post')
		 {
			// echo $_wcv_shipping_provider;
			 if(preg_match("/^[0-9]{10}|[0-9]{6}|[0-9]{4}$/",$_wcv_tracking_number ))
			 {
				  $fd = true;
				
			 } else{
				 $fd = false;
			 }
		 } 
		 elseif($_wcv_shipping_provider == 'Canada+Post')
		 {
			 $url = 'https://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber='.$_wcv_tracking_number;
			 $fileconent =  file_get_contents( $url );
			 if(preg_match("/Unfortunately/",$fileconent ))
			 {
				  $fd = false;
				
			 } else{
				 $fd = true;
			 }
		 }
		 elseif($_wcv_shipping_provider == 'DHL+Intraship+(DE)')
		 {
			 $url = 'https://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc='. $_wcv_tracking_number . '&rfn=&extendedSearch=true';
			 $fileconent =  file_get_contents( $url );
			 if(preg_match("/vor/",$fileconent ))
			 {
				  $fd = false;
				
			 } else{
				 $fd = true;
			 }
		 }
		 elseif($_wcv_shipping_provider == 'FedEx+Sameday')
		 {
			 $url = 'https://www.fedexsameday.com/fdx_dotracking_ua.aspx?tracknum='. $_wcv_tracking_number;
			 $fileconent =  file_get_contents( $url );
			 if(preg_match("/Invalid|No Record Found/",$fileconent ))
			 {
				  $fd = false;
				
			 } else{
				 $fd = true;
			 }
		 }
		  elseif($_wcv_shipping_provider == 'OnTrac')
		 {
			 $url = 'http://www.ontrac.com/trackingdetail.asp?tracking='. $_wcv_tracking_number;
			 $fileconent =  file_get_contents( $url );
			 if(preg_match("/could not be located/",$fileconent ))
			 {
				  $fd = false;
				
			 } else{
				 $fd = true;
			 }
		 }
		  elseif($_wcv_shipping_provider == 'UPS')
		 {
			 $url = 'https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums='. $_wcv_tracking_number;
			 $fileconent =  file_get_contents( $url );
			 if(preg_match("/not a valid/",$fileconent ))
			 {
				  $fd = false;
				
			 } else{
				 $fd = true;
			 }
		 }
		 
		  elseif($_wcv_shipping_provider == 'USPS')
		 {
			 $url = 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1='. $_wcv_tracking_number;
			 $fileconent =  file_get_contents( $url );
			 if(preg_match("/Not Trackable/",$fileconent ))
			 {
				  $fd = false;
				
			 } else{
				 $fd = true;
			 }
		 }
		//$order_id = $_POST['_wcv_order_id' ];  PHP file_get_contents()   
		//https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=45612
					
        if($fd == true)
		{			
		$order_tracking_details = get_post_meta( $order_id, '_wcv_tracking_details', true ); 
		$vendor_id = get_current_user_id(); 
		
		$vendor_tracking_details = array( 
			 '_wcv_shipping_provider' 	=> $_wcv_shipping_provider, 
			 '_wcv_tracking_number' 	=> $_wcv_tracking_number, 
			 '_wcv_date_shipped' 		=> $_wcv_date_shipped, 
		); 

		$order_tracking_details[ $vendor_id ] = $vendor_tracking_details; 

		$tracking_base_url 	= '';
		$tracking_provider 	= ''; 

		// Loop through providers and get the URL to input 
		foreach ( $this->shipping_providersnew() as $provider_countries ) {

			foreach ( $provider_countries as $provider => $url ) {

				if ( sanitize_title( $provider ) == sanitize_title( $vendor_tracking_details[ '_wcv_shipping_provider' ] ) ) {
					$tracking_base_url = $url;
					$tracking_provider = $provider;
					break;
				}

			}

			if ( $tracking_base_url ) { 
				break;
			}

		}
		
		$order_note 	= __('A vendor has added a tracking number to your order. You can track this at the following url. ', 'wcvendors-pro' ); 
		$full_link 		= sprintf( $tracking_base_url, $vendor_tracking_details[ '_wcv_tracking_number'] ); 
		$order_note 	.= sprintf( '<a href="%s" target="_blank">%s</a>', $full_link, $full_link ) ; 

		$this->add_order_note( $order_id, $order_note ); 

		update_post_meta( $order_id, '_wcv_tracking_details', $order_tracking_details ); 

		// Mark as shipped as tracking information has been added
		self::mark_shipped( $vendor_id, $order_id ); 
		echo "saved";
	   }else{
		   echo "No Saved, Your Tracking ID May Be Wrong";
	   }
      die();
	} // update_shipment_tracking()*/
	
	public function update_shipment_tracking_alax( ) {
		
		header("Content-type: application/json");
		libxml_use_internal_errors( true);
		//error_reporting(-1);
        $order_iddta = $_POST['data'];
		$tok = preg_split('/&/',  $order_iddta);
		
		$wcv_shipping_provider = preg_split('/=/',  $tok[0]);
		$_wcv_shipping_provider = $wcv_shipping_provider[1];
		
		$wcv_tracking_number = preg_split('/=/',  $tok[1]);
		$_wcv_tracking_number = $wcv_tracking_number[1];
		
		$wcv_date_shipped = preg_split('/=/',  $tok[2]);
		$_wcv_date_shipped = $wcv_date_shipped[1];
		
		$_wcv_order_id = preg_split('/=/',  $tok[3]);
		$order_id = $_wcv_order_id[1];
		
		 //$order_id = $_POST['orderid'];
		 //$_wcv_shipping_provider = $_POST['carrier_code'];
		 
		 //$_wcv_tracking_number = $_POST['trackingnumber'];
		 
		 $fd = false;
		 $messageofdelevery = '';
		 $responce = array();
	     $updatedb = false;
		switch($_wcv_shipping_provider){
		
		case 'fedex':
				                if( is_numeric( $_wcv_tracking_number )  )
								{
								  // $messageofdelevery = 'match';
                                   $url = 'http://track.snapcx.io/?carrier_code=fedex&track_id='.$_wcv_tracking_number;
			                       $fileconent =  file_get_contents( $url );
								   if(!preg_match("/cannot be found/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('/html/body/div/div/div/div[2]/div/table/tbody/tr[1]/td');
										$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'DELIVERED')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
								    }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									}
								   
									 
								}else{
									  $messageofdelevery = 'Tracking Number Error';
									  $updatedb = false;
								}
		       break;
			   
		case 'dhl':
		                      // $length = strlen($_wcv_tracking_number);
				               if( is_numeric( $_wcv_tracking_number )  )
								{
								   //$messageofdelevery = 'match';	
								   
								   $url = 'http://track.snapcx.io/?carrier_code=dhl&track_id='.$_wcv_tracking_number;
			                       $fileconent =  file_get_contents( $url );
								   if(!preg_match("/cannot be found/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('/html/body/div/div/div/div[2]/div/table/tbody/tr[1]/td');
										//$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'SHIPPED')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
								    }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									}
								   
								}else{
									
									  $messageofdelevery = 'Tracking Number Error';
									  $updatedb = false;
								}
		       break;
			   
		/*case 'dhl-pieceid':
		
		                      if(preg_match( "/\JD[0-9]/", $_wcv_tracking_number )  )
								{
								   $messageofdelevery = 'match';	
								}else{
									 $messageofdelevery = 'Not match';
								}
		       break;	*/   
			   
		case 'usps':
		
		          if( is_numeric( $_wcv_tracking_number )  )
								{
								  // $messageofdelevery = 'match';
                                   $url = 'http://track.snapcx.io/?carrier_code=usps&track_id='.$_wcv_tracking_number;
			                       $fileconent =  file_get_contents( $url );
								   if(!preg_match("/cannot be found/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('/html/body/div/div/div/div[2]/div/table/tbody/tr[1]/td');
										$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'DELIVERED')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
								    }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									}
								   
									 
								}else{
									  $messageofdelevery = 'Tracking Number Error';
									  $updatedb = false;
								}
		       break;	   
			   
		/*case 'tnt':
		          if( is_numeric( $_wcv_tracking_number )  )
								{
								   $messageofdelevery = 'match';	
								}else{
									 $messageofdelevery = 'Not match';
								}
					//$messageofdelevery = $_wcv_shipping_provider;			
		       break;	   
			   
		case 'tnt-reference':
		          if( is_numeric( $_wcv_tracking_number )  )
								{
								   $messageofdelevery = 'match';	
								}else{
									 $messageofdelevery = 'Not match';
								}
					//$messageofdelevery = $_wcv_shipping_provider;		
		       break;	   
			   
		case 'dpd':
		         if(preg_match( "/^1[0-9]/", $_wcv_tracking_number )  )
								{
								   $messageofdelevery = 'match';	
								}else{
									 $messageofdelevery = 'Not match';
								}
		       break;	*/   
			   
		/*case 'aramex':
		       break;	   
			   
		case 'dhl-global-mail':
		       break;	   
			   
		case 'dhl-global-forwarding':
		       break;	   
			   
		case 'echo':
		       break;	   
			   
		case 'fedex-freight':
		       break;	   
			   
		case 'canada-post':
		       break;
			   */
		case 'canpar':
		              if(preg_match( "/D[0-9]/", $_wcv_tracking_number )  )
					    {
		                       $temurl = 'https://www.canpar.com/en/track/TrackingAction.do?locale=en&type=0&reference=';
						  	   $fullurl = $temurl . $track_id; 
			                   $fileconent =  file_get_contents( $fullurl );
							   if(preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                     {
				                   $messageofdelevery = 'Tracking Number Error';
								   $updatedb = false;
			                    }else{
									 $doc = new DOMDocument;
									 $doc->loadHTML( $fileconent); 
									 $xpath = new DOMXpath( $doc);
									 $staus = $xpath->query('//*[@class="centreContent"]//table[@id="table0"]/tr[3]/td[1]');
									 $messageofdelevery = $staus->item(0)->nodeValue; 
                                     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
									 $updatedb = true;									 
								 }
						} else {
							
							 $messageofdelevery = 'Tracking Number Error';
							 $updatedb = false;
						}
		       break;	   
			   
			   
		/*case 'purolator':
		       break;	   
			   
			   
		case 'apc':
		       break;	   
			   
		case 'ontrac':
		       break;	   
			   
		case 'yrc':
		       break;	   
			   
		case 'asendia-usa':
		       break;	   
			   
		case 'ensenda':
		       break;	   
			   
		case 'newgistics':
		       break;	   
			   
		case 'lasership':
		       break;	   
			   
			   
		case 'expeditors':
		       break;	   
			   
			   
		case 'i-parcel':
		       break;	   
			   
		case 'abf':
		       break;	   
			   
		case 'old-dominion':
		       break;	   
			   
		case 'con-way':
		       break;	   
			   
		case 'estes':
		       break;	   
			   
		case 'rl-carriers':
		       break;	   
			   
			   
		case 'directlink':
		       break;	   
			   
		case 'international-seur':
		       break;	   
			   
			   
		case 'gls':
		       break;	   
			   
			   
		case 'dhl-benelux':
		       break;	   
			   
		case 'postnord':
		       break;	*/   
			   
		case 'royal-mail':
		             if(preg_match( "/.*GB/", $_wcv_tracking_number )  )
								{
								   $fullurl = 'http://track.aftership.com/hong-kong-post/'.$_wcv_tracking_number;
			                       $request = wp_remote_get( $fullurl);
							        $fileconent = wp_remote_retrieve_body( $request );
								  if(!preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('//*[@id="page"]/div/div/div[2]/div/div[3]/div/p');
										//$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'Delivered')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
											
								     }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									 }
									 
		                        }else{
									
									$messageofdelevery = 'Tracking Number Error';
								}
		       break;	   
			   
		/*case 'parcel-force':
		       break;	   
			   
			   
		case 'fedex-uk':
		       break;	   
			   
			   
		case 'dpd-uk':
		       break;	   
			   
		case 'collectplus':
		       break;	   
			   
		case 'skynetworldwide':
		       break;	   
			   
		case 'skynetworldwide-uk':
		       break;	   
			   
			   
		case 'tnt-uk':
		       break;	   
			   
		case 'tnt-uk-reference':
		       break;	   
			   
		case 'interlink-express':
		       break;	   
			   
			   
		case 'interlink-express-reference':
		       break;	   
			   
		case 'uk-mail':
		       break;	   
			   
		case 'yodel':
		       break;	   
			   
		case 'yodel-international':
		       break;	   
			   
		case 'hermes':
		       break;	   
			   
		case 'myhermes-uk':
		       break;	   
			   
		case 'norsk-global':
		       break;	   
			   
		case 'panther':
		       break;	   
			   
		case 'deltec-courier':
		       break;	   
			   
			   
		case 'xdp-uk':
		       break;	   
			   
		case 'trakpak':
		       break;	   
			   
		case 'xdp-uk-reference':
		       break;	   
			   
		case 'an-post':
		       break;	   
			   
		case 'dpd-ireland':
		       break;	   
			   
		case 'fastway-ireland':
		       break;	   
			   
		case 'nightline':
		       break;	   
			   
		case 'tuffnells':
		       break;	   
			   
		case 'arrowxl':
		       break;	   
			   
		case 'deutsch-post':
		       break;	   
			   
		case 'dhl-germany':
		       break;	   
			   
		case 'hermes-de':
		       break;	   
			   
		case 'dpd-de':
		       break;	   
			   
		case 'asendia-de':
		       break;	   
			   
		case 'dhl-deliverit':
		       break;	   
			   
		case 'austrian-post-registered':
		       break;	   
			   
		case 'austrian-post':
		       break;	   
			   
		case 'swiss-post':
		       break;	   
			   
		case 'spain-correos-es':
		       break;	   
			   
		case 'correosexpress':
		       break;	   
			   
		case 'nacex-spain':
		       break;	   
			   
		case 'spanish-seur':
		       break;	   
		case 'asm':
		       break;	   
			   
		case 'mrw-spain':
		       break;	   
			   
		case 'envialia':
		       break;

        case 'redur-es':
		       break;

        case 'packlink':
		       break;

        case 'dhl-es':
		       break;

        case 'magyar-posta':
		       break;   

		case 'exapaq':
		       break;	

		case 'portugal-ctt':
		       break;

		case 'chronopost-portugal':
		       break;	

		case 'portugal-seur':
		       break;

		case 'adicional':
		       break;

		case 'la-poste-colissimo':
		       break;

		case 'colissimo':
		       break;

		case 'chronopost-france':
		       break;
			   
        case 'tnt-fr':
		       break;
			   
		case 'colis-prive':
		       break;

		case 'geodis-calberson-fr':
		       break;
			   
		case 'bert-fr':
		       break;
			   
		case 'sic-teliway':
		       break;

		case 'geodis-espace':
		       break;

		case 'mondialrelay':
		       break;	   
			   
		case 'postnl':
		       break;

        case 'postnl-international':
		       break;

        case 'postnl-3s':
		       break;
		
		case 'dhl-nl':
		       break;

		case 'gls-netherlands':
		       break;
			 
		case 'dhlparcel-nl':
		       break;


		case 'transmission-nl':
		       break;

        case 'bpost':
		       break;

		case 'bpost-international':
		       break;

		case 'landmark-global':
		       break;

		case 'posti':
		       break;

		case 'posten-norge':
		       break;

		case 'sweden-posten':
		       break;

		case 'dbschenker-se':
		       break;

		case 'matkahuolto':
		       break;

		case 'danmark-post':
		       break;

		case 'inpost-paczkomaty':
		       break;

		case 'raben-group':
		       break;

		case 'italy-sda':
		       break;

		case 'dmm-network':
		       break;

		case 'poste-italiane-paccocelere':
		       break;

		case 'poste-italiane':
		       break;

		case 'brt-it':
		       break;

		case 'fercam':
		       break;

        case 'gls-italy':
		       break;

        case 'sgt-it':
		       break;
			   
		case 'tnt-it':
		       break;

		case 'tntpost-it':
		       break;

		case 'tnt-click':
		       break;

     	case 'russian-post':
		       break;   

        case 'postur-is':
		       break;
			   
		case 'nova-poshta':
		       break;

		case 'dhl-poland':
		       break;

		case 'poczta-polska':
		       break;	   

        case 'dpd-poland':
		       break;
			   
		case 'siodemka':
		       break;

		case 'opek':
		       break;	

		case 'lietuvos-pastas':
		       break;

		case 'ceska-posta':
		       break;

		case 'elta-courier':
		       break;

		case 'acscourier':
		       break;

		case 'taxydromiki':
		       break;

		case 'speedcouriers-gr':
		       break;

		case 'ptt-posta':
		       break;

		case 'belpost':
		       break;

		case 'bgpost':
		       break;

		case 'hrvatska-posta':
		       break;

		case 'kn':
		       break;

	    case 'posta-romana':
		       break;   
			   
		case 'cyprus-post':
		       break;

		case 'ukrposhta':
		       break;   
         
        case 'sf-express':
		       break;
			   
		case 'sfb2c':
		       break;

		case 'dhl-global-mail-asia':
		       break;

		case 'kerry-logistics':
		       break;

		case 'quantium':
		       break;

		case 'dpex':
		       break;

		case 'omniparcel':
		       break;

		case 'china-post':
		       break;*/

		case 'china-ems':
		       // $length = strlen($_wcv_tracking_number);
				               if(preg_match( "/LX.*CN/", $_wcv_tracking_number )  )
								{
								   //$messageofdelevery = 'match';	
								   
								   $url = 'http://track.snapcx.io/?carrier_code=china-ems&track_id='.$_wcv_tracking_number;
			                       $fileconent =  file_get_contents( $url );
								   if(!preg_match("/cannot be found/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('/html/body/div/div/div/div[2]/div/table/tbody/tr[1]/td');
										//$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'SHIPPED')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
								    }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									}
								   
								}else{
									
									  $messageofdelevery = 'Tracking Number Error';
									  $updatedb = false;
								}
		       break;

		/*case '4px':
		       break;

		case 'yanwen':
		       break;

		case 'sfcservice':
		       break;

		case 'ec-firstclass':
		       break;

		case 'aupost-china':
		       break;

		case 'wedo':
		       break;	   

        case 'ppbyb':
		       break;
			   
		case 'jcex':
		       break;

		case 'wishpost':
		       break;

		case 'sto':
		       break;

		case 'greyhound':
		       break; 

		case 'globegistics':
		       break;

		case 'post56':
		       break;

		case 'flytexpress':
		       break;

		case 'szdpex':
		       break;

		case '800bestex':
		       break;

		case 'cnexps':
		       break;

		case 'equick-cn':
		       break;

		case 'boxc':
		       break;

		case 'buylogic':
		       break;*/

		case 'hong-kong-post':
		         if(preg_match( "/RB.*HK/", $_wcv_tracking_number )  )
								{
								   $fullurl = 'http://track.aftership.com/hong-kong-post/'.$_wcv_tracking_number;
			                       $request = wp_remote_get( $fullurl);
							        $fileconent = wp_remote_retrieve_body( $request );
								  if(!preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('//*[@id="page"]/div/div/div[2]/div/div[3]/div/p');
										//$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'Delivered')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
											
								     }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									 }
									 
		                        }else{
									
									$messageofdelevery = 'Tracking Number Error';
								}
		       break;

		/*case 'empsexpress':
		       break;

		case 'idexpress':
		       break;

		case 'sekologistics':
		       break;

		case 'pfcexpress':
		       break;

		case 'cpacket':
		       break;

		case 'yto':
		       break;

		case 'taqbin-hk':
		       break;

		case 'detrack':
		       break;

		case 'tgx':
		       break;

		case 'ramgroup-za':
		       break;

		case 'zjs-express':
		       break;

		case 'asendia-uk':
		       break;

		case 'lwe-hk':
		       break;

		case 'yunexpress':
		       break;

		case 'nationwide-my':
		       break;

		case 'cuckooexpress':
		       break;

		case 'rrdonnelley':
		       break;

		case 'maxcellents':
		       break;

        case 'oneworldexpress':
		       break;

		case 'hh-exp':
		       break;

		case 'ajexpress':
		       break;

		case 'hunter-express':
		       break;

		case 'yakit':
		       break;

		case 'dhl-active-tracing':
		       break;

		case 'eparcel-kr':
		       break;

		case 'dex-i':
		       break;

		case 'febrt-it-parceliddex':
		       break;

		case 'sendit':
		       break;

		case 'mailamericas':
		       break;

		case 'correos-de-costa-rica':
		       break;

		case 'copa-courier':
		       break;

		case 'mara-xpress':
		       break;

		case 'mikropakket':
		       break;

		case 'wndirect':
		       break;

		case 'kiala':
		       break;

		case 'ruston':
		       break;

		case 'wanbexpress':
		       break;

		case 'cj-korea-thai':
		       break;

		case 'wise-express':
		       break;

		case 'rpd2man':
		       break;

		case 'acommerce':
		       break;

		case 'panther-reference':
		       break;

		case 'logwin-logistics':
		       break;

		case 'wiseloads':
		       break;

		case 'rincos':
		       break;

		case 'alliedexpress':
		       break;

		case 'hermes-it':
		       break;

		case 'asendia-hk':
		       break;


		case 'ninjavan-thai':
		       break;

		case 'eurodis':
		       break;

		case 'whistl':
		       break;

		case 'abcustom':
		       break;

		case 'tipsa':
		       break;

		case 'ninjavan-philippines':
		       break;

		case 'zto-express':
		       break;

		case 'nim-express':
		       break;

		case 'alljoy':
		       break;

		case 'doora':
		       break;

		case 'dynalogic':
		       break;

		case 'collivery':
		       break;

		case 'gsi-express':
		       break;	

		case 'rcl':
		       break;

		case 'mxe':
		       break;

		case 'birdsystem':
		       break;

		case 'paquetexpress':
		       break;

		case 'line':
		       break;

		case 'efs':
		       break;

		case 'instant':
		       break;

		case 'kpost':
		       break;

		case 'collectco':
		       break;

		case 'ddexpress':
		       break;

		case 'smooth':
		       break;

		case 'panther-order-number':
		       break;

		case 'sailpost':
		       break;

		case 'tcs':
		       break;

		case 'ezship':
		       break;

		case 'nova-poshtaint':
		       break;

		case 'skynetworldwide-uae':
		       break;

		case 'demandship':
		       break;

		case 'pickup':
		       break;

		case 'mudita':
		       break;

		case 'mainfreight':
		       break;

		case 'aprisaexpress':
		       break;

		case 'homedirect-logistics':
		       break;

        case 'imxmail':
		       break;


		case 'cbl-logistica':
		       break;

		case 'easy-mail':
		       break;

		case 'dhl-hk':
		       break;

		case 'raiderex':
		       break;

		case 'itis':
		       break;

		case 'imexglobalsolutions':
		       break;

		case 'singapore-post':
		       break;

		case 'rpxonline':
		       break;

		case '17postservice':
		       break;
		
		case 'nanjingwoyuan':
		       break;

		case 'roadbull':
		       break;

		case 'jersey-post':
		       break;


		case 'directlog':
		       break;

		case 'ekart':
		       break;

        case 'zyllem':
		       break;
			   
		case 'singapore-speedpost':
		       break;	   
			   
		case 'yundaex':
		       break;	 

		case '360lion':
		       break;	

		case 'gofly':
		       break;	

		case 'dpe-express':
		       break;	

		case 'simplypost':
		       break;	


		case 'taqbin-sg':
		       break;	


		case 'xq-express':
		       break;	


		case 'xl-express':
		       break;	

		case 'ninjavan':
		       break;	

		case 'vtfe':
		       break;	

		case 'zalora-7-eleven':
		       break;	

		case 'jet-ship':
		       break;	

		case 'nhans-solutions':
		       break;	

		case 'kgmhub':
		       break;	

		case 'qxpress':
		       break;	

		case 'rzyexpress':
		       break;	

		case 'parcelpost-sg':
		       break;	

		case 'parcel-express':
		       break;	

		case 'korea-post':
		       break;	


		case 'cj-gls':
		       break;	

		case 'ecargo-asia':
		       break;	

		case 'srekorea':
		       break;	

		case 'rocketparcel':
		       break;	

		case 'india-post':
		       break;	

		case 'india-post-int':
		       break;	

		case 'delhivery':
		       break;	

		case 'bluedart':
		       break;	

		case 'nuvoex':
		       break;	

		case 'dtdc':
		       break;	

		case 'delcart-in':
		       break;	


		case 'professional-couriers':
		       break;	

		case 'parcelled-in':
		       break;	

		case 'safexpress':
		       break;	

		case 'red-express':
		       break;	

		case 'red-express-wb':
		       break;	

		case 'first-flight':
		       break;	

		case 'gati-kwe':
		       break;	

		case 'gojavas':
		       break;	

		case 'ecom-express':
		       break;	

		case 'xpressbees':
		       break;
			   
		case 'japan-post':
		       break;	

		case 'taqbin-jp':
		       break;	

		case 'sagawa':
		       break;	

		case 'taiwan-post':
		       break;	

		case 'bh-posta':
		       break;	


		case 'spreadel':
		       break;	

		case 'malaysia-post':
		       break;	

		case 'malaysia-post-posdaftar':
		       break;	

		case 'taqbin-my':
		       break;	

		case 'abxexpress-my':
		       break;	

		case 'gdex':
		       break;	


		case 'skynet':
		       break;	

		case 'citylinkexpress':
		       break;	


		case 'mypostonline':
		       break;	

		case 'airpak-express':
		       break;	

		case 'ninjavan-my':
		       break;	

		case 'matdespatch':
		       break;	

		case 'thailand-post':
		       break;	

		case 'dynamic-logistics':
		       break;	


		case '2go':
		       break;
			   
		case 'courex':
		       break;	

		case 'xend':
		       break;
			   
		case 'lbcexpress':
		       break;	

		case 'air21':
		       break;	

		case 'jam-express':
		       break;	

		case 'airspeed':
		       break;	

		case 'raf':
		       break;	

		case 'pos-indonesia':
		       break;	


		case 'pos-indonesia-int':
		       break;	


		case 'jayonexpress':
		       break;	


		case 'jne':
		       break;	

		case 'pandulogistics':
		       break;	

		case 'rpx':
		       break;	


		case 'tiki':
		       break;	

		case 'lion-parcel':
		       break;
			   
		case 'ninjavan-id':
		       break;
			   
		case 'wahana':
		       break;
			   
		case 'first-logistics':
		       break;	

		case 'vnpost':
		       break;	

		case 'ghn':
		       break;	

		case 'vnpost-ems':
		       break;
			   
		case 'lao-post':
		       break;	

		case 'viettelpost':
		       break;	

		case 'kerryttc-vn':
		       break;	


		case 'cambodia-post':
		       break;	*/


		case 'australia-post':
		                 //$messageofdelevery = 'Not match';
		                 if(preg_match( "/\MKD[0-9]/", $_wcv_tracking_number )  )
								{
								     $url = 'http://track.snapcx.io/?carrier_code=australia-post&track_id='.$_wcv_tracking_number;
			                       $fileconent =  file_get_contents( $url );
								   if(!preg_match("/cannot be found/",$fileconent ))
			                       {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('/html/body/div/div/div/div[2]/div/table/tbody/tr[1]/td');
										//$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'DELIVERED')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
								    }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									}
								   
								   
								}else{
									 $messageofdelevery = 'Tracking Number Error';
								}
		       break;	

		/*case 'dotzot':
		       break;	

		case 'scudex-express':
		       break;	

		case 'toll-ipec':
		       break;	


		case 'toll-priority':
		       break;	

		case 'tnt-au':
		       break;	

		case 'adsone':
		       break;	


		case 'fastway-au':
		       break;	


		case 'star-track':
		       break;	


		case 'dtdc-au':
		       break;	

		case 'sendle':
		       break;	

		case 'star-track-express':
		       break;	

		case 'couriers-please':
		       break;	

		case 'new-zealand-post':
		       break;	

		case 'directfreight-au':
		       break;	


		case 'courierpost':
		       break;	

		case 'fastway-nz':
		       break;	

		case 'ubi-logistics':
		       break;
			   
		case 'bondscouriers':
		       break;	

		case 'correos-de-mexico':
		       break;	

		case 'estafeta':
		       break;	

		case 'mexico-senda-express':
		       break;	

		case 'mexico-redpack':
		       break;	

		case 'aeroflash':
		       break;	


		case 'brazil-correios':
		       break;	

		case 'correos-chile':
		       break;	

		case 'correo-argentino':
		       break;	

		case 'oca-ar':
		       break;	

		case 'emirates-post':
		       break;	

		case '4-72':
		       break;	

		case 'thecourierguy':
		       break;
			   
		case 'israel-post':
		       break;	

		case 'israel-post-domestic':
		       break;	

		case 'sapo':
		       break;	

		case 'alphafast':
		       break;	


		case 'dawnwing':
		       break;	

		case 'fastway-za':
		       break;	


		case 'specialisedfreight-za':
		       break;
			   
		case 'dpe-za':
		       break;	

		case 'smsa-express':
		       break;	


		case 'courierit':
		       break;	

		case 'saudi-post':
		       break;	

		case 'axl':
		       break;	

		case 'nipost':
		       break;
			   
		case 'courier-plus':
		       break;	


		case 'speedexcourier':
		       break;	


		case 'post-serbia':
		       break;	

		case 'skypostal':
		       break;	

		case 'kangaroo-my':
		       break;	

		case 'fastrak-th':
		       break;	

		case 'jocom':
		       break;	

		case 'b2ceurope':
		       break;	

		case 'costmeticsnow':
		       break;	*/

	    default:
		                           $length = strlen($_wcv_tracking_number);
								   if( $length > 9 )
								   {
									   
								   $fullurl = 'http://track.aftership.com/'. $_wcv_shipping_provider .'/'.$_wcv_tracking_number;
			                       $request = wp_remote_get( $fullurl);
							       $fileconent = wp_remote_retrieve_body( $request );
								   if(!preg_match("/No tracking results available for this Barcode/",$fileconent ))
			                        {
								   
										$doc = new DOMDocument; $doc->loadHTML( $fileconent);
										$xpath = new DOMXpath( $doc);
										$staus = $xpath->query('//*[@id="page"]/div/div/div[2]/div/div[3]/div/p');
										//$delevarydate = $xpath->query('/html/body/div/div/div/div[3]/table/tbody/tr[2]/td[1]');
										$messageofdelevery = $staus->item(0)->nodeValue;
										if( $messageofdelevery == 'Delivered')
										   {
												$messageofdelevery = 'Something error! Please check your Tracking Number or Date Shipped';
											  $updatedb = false;
											}else{
										     $messageofdelevery = 'Tracking Number added success.Customer receive a message with Tracking Number and Date Shipped'; 
											 $updatedb = true;
											}
											
								     }else{
										$messageofdelevery = 'Tracking Number Error';
										$updatedb = false;
									 } 
		                          } else{
									  
									  $messageofdelevery = 'Tracking Number Error';
									  $updatedb = false;  
								  }
		}							 
		$responce['successdb'] = 0;
		if($updatedb == true){
		update_post_meta( $order_id, '_aftership_tracking_provider', $_wcv_shipping_provider ); 
		update_post_meta( $order_id, '_aftership_tracking_number', $_wcv_tracking_number ); 
		update_post_meta( $order_id, '_aftership_tracking_shipdate', $_wcv_date_shipped ); 
		update_post_meta( $order_id, '_aftership_tracking_shipdate', $_wcv_date_shipped );
          $responce['successdb'] = 1;	
		}
		//wc_pv_shipped

		
		
	   $responce['resultone'] = $messageofdelevery;
	  
	   echo json_encode($responce);
						  
      die();
	} // update_shipment_tracking()
	
	public function tracking_update_none()
	{
		header("Content-type: application/json");
		//error_reporting(-1);
        // $order_id = $_POST['orderid'];
		 //$carrier_code = $_POST['carrier_code'];
		 //$trackingnumber = $_POST['trackingnumber'];
		 
		 $order_iddta = $_POST['data'];
		 $tok = preg_split('/&/',  $order_iddta);
		
		 $wcv_shipping_provider = preg_split('/=/',  $tok[0]);
		 $carrier_code = $wcv_shipping_provider[1];
		
		 $wcv_tracking_number = preg_split('/=/',  $tok[1]);
		 $trackingnumber = $wcv_tracking_number[1];
		
		 $wcv_date_shipped = preg_split('/=/',  $tok[2]);
		 $_wcv_date_shipped = $wcv_date_shipped[1];
		
		 $_wcv_order_id = preg_split('/=/',  $tok[3]);
		$order_id = $_wcv_order_id[1];
		 $order_note 	= __('A vendor has added a tracking number to your order. You can track this at the following url. ', 'wcvendors-pro' );
		 
		 $full_link = home_url() . '/tracking-information/?carrier_code='. $carrier_code . '&track_id='. $trackingnumber; 
		 $order_note 	.= sprintf( '<a href="%s" target="_blank">%s</a>', $full_link, $full_link ) ;
		
		 $orderid = new WC_Order($order_id);
		 $orderid->add_order_note( $order_note ); 
		
		//$this->add_order_note($order_id, $order_note );
		
		$vendor_id = get_current_user_id(); 
		// Mark as shipped as tracking information has been added
		self::mark_shipped( $vendor_id, $order_id );
		
		$responce['success'] = 'Success';
		
		echo json_encode($responce);
						  
      die();
	}

	
	/**
	 *  Shipment tracking providers
	 * 
	 * @since    1.0.0
	 * @return 	 array 	shipping providers 
	 */
	public static function shipping_providers_ajax( ) { 

		return $shipping_providers = apply_filters( 'wcv_shipping_providers_list', array(
					'Australia' => array(
						'Australia Post' => 'http://auspost.com.au/track/track.html?id=%s',
						'FedEx' => 'https://www.fedex.com/apps/fedextrack/?tracknumbers=%s&cntry_code=au', 
					),
					'Canada' => array(
						'Canada Post' => 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=%s',
					),
					'Germany' => array(
						'DHL Intraship (DE)' => 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=%s&rfn=&extendedSearch=true',
						'Hermes' => 'https://tracking.hermesworld.com/?TrackID=%s',
						'Deutsche Post DHL' => 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=%s',
						'UPS Germany' => 'http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=de_DE&InquiryNumber1=%1s',
						'DPD' => 'https://tracking.dpd.de/parcelstatus?query=%s&locale=en_DE',
					),
					'Ireland' => array(
						'DPD' => 'http://www2.dpd.ie/Services/QuickTrack/tabid/222/ConsignmentID/%s/Default.aspx',
					),
					'Italy' => array(
						'BRT (Bartolini)' => 'http://as777.brt.it/vas/sped_det_show.hsm?referer=sped_numspe_par.htm&Nspediz=%s',
						'DHL Express' => 'http://www.dhl.it/it/express/ricerca.html?AWB=%s&brand=DHL'
					),
					'India' => array(
						'DTDC' => 'http://www.dtdc.in/dtdcTrack/Tracking/consignInfo.asp?strCnno=%s',
					),
					'Netherlands' => array(
						'PostNL' => 'https://mijnpakket.postnl.nl/Claim?Barcode=%s&Postalcode=%s&Foreign=False&ShowAnonymousLayover=False&CustomerServiceClaim=False',
						'DPD.NL' => 'http://track.dpdnl.nl/?parcelnumber=%1s',
					),
					'New Zealand' => array(
						'Courier Post' => 'http://trackandtrace.courierpost.co.nz/Search/%s',
						'NZ Post' => 'http://www.nzpost.co.nz/tools/tracking?trackid=%1$s',
						'Fastways' => 'http://www.fastway.co.nz/courier-services/track-your-parcel?l=%s',
						'PBT Couriers' => 'http://www.pbt.com/nick/results.cfm?ticketNo=%s',
					),
					'South African' => array(
						'SAPO' => 'http://sms.postoffice.co.za/TrackingParcels/Parcel.aspx?id=%s',
					),
					'United Kingdom' => array(
						'DHL' => 'http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=%1$s',
						'DPD' => 'http://www.dpd.co.uk/tracking/trackingSearch.do?search.searchType=0&search.parcelNumber=%1$s',
						'InterLink' => 'http://www.interlinkexpress.com/apps/tracking/?reference=%1$s&postcode=%2$s#results',
						'ParcelForce' => 'http://www.parcelforce.com/portal/pw/track?trackNumber=%1$s',
						'Royal Mail' => 'https://www.royalmail.com/track-your-item/?trackNumber=%1$s',
						'TNT Express (consignment)' => 'http://www.tnt.com/webtracker/tracking.do?requestType=GEN&searchType=CON&respLang=en&respCountry=GENERIC&sourceID=1&sourceCountry=ww&cons=%1$s&navigation=1&genericSiteIdent=',
						'TNT Express (reference)' => 'http://www.tnt.com/webtracker/tracking.do?requestType=GEN&searchType=REF&respLang=en&respCountry=GENERIC&sourceID=1&sourceCountry=ww&cons=%1$s&navigation=1&genericSiteIdent=',
						'UK Mail' => 'https://old.ukmail.com/ConsignmentStatus/ConsignmentSearchResults.aspx?SearchType=Reference&SearchString=%1$s',
					),
					'United States' => array(
						'Fedex' => 'http://www.fedex.com/Tracking?action=track&tracknumbers=%s',
						'FedEx Sameday' => 'https://www.fedexsameday.com/fdx_dotracking_ua.aspx?tracknum=%s',
						'OnTrac' => 'http://www.ontrac.com/trackingdetail.asp?tracking=%s',
						'UPS' => 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=%s',
						'USPS' => 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=%s',
					),
				) );

	}
	
	
	
	
	/**
	 *  Update the order shipment tracking
	 * 
	 * @since    1.0.0
	 */
	public function update_shipment_tracking( ) { 

		$order_id = $_POST['_wcv_order_id' ]; 
		$order_tracking_details = get_post_meta( $order_id, '_wcv_tracking_details', true ); 
		$vendor_id = get_current_user_id(); 
		
		$vendor_tracking_details = array( 
			 '_wcv_shipping_provider' 	=> $_POST['_wcv_shipping_provider_'. $order_id ], 
			 '_wcv_tracking_number' 	=> $_POST['_wcv_tracking_number_' .$order_id ], 
			 '_wcv_date_shipped' 		=> $_POST['_wcv_date_shipped_' . $order_id ], 
		); 

		$order_tracking_details[ $vendor_id ] = $vendor_tracking_details; 

		$tracking_base_url 	= '';
		$tracking_provider 	= ''; 

		// Loop through providers and get the URL to input 
		foreach ( $this->shipping_providersnew() as $provider_countries ) {

			foreach ( $provider_countries as $provider => $url ) {

				if ( sanitize_title( $provider ) == sanitize_title( $vendor_tracking_details[ '_wcv_shipping_provider' ] ) ) {
					$tracking_base_url = $url;
					$tracking_provider = $provider;
					break;
				}

			}

			if ( $tracking_base_url ) { 
				break;
			}

		}
		
		$order_note 	= __('A vendor has added a tracking number to your order. You can track this at the following url. ', 'wcvendors-pro' ); 
		$full_link 		= sprintf( $tracking_base_url, $vendor_tracking_details[ '_wcv_tracking_number'] ); 
		$order_note 	.= sprintf( '<a href="%s" target="_blank">%s</a>', $full_link, $full_link ) ; 

		$this->add_order_note( $order_id, $order_note ); 

		update_post_meta( $order_id, '_wcv_tracking_details', $order_tracking_details ); 

		// Mark as shipped as tracking information has been added
		self::mark_shipped( $vendor_id, $order_id ); 

	} // update_shipment_tracking()


	/**
	 *  Shipment tracking providers
	 * 
	 * @since    1.0.0
	 * @return 	 array 	shipping providers 
	 */
	public static function shipping_providers( ) { 

		return $shipping_providers = apply_filters( 'wcv_shipping_providers_list', array(
					'Australia' => array(
						'Australia Post' => 'http://auspost.com.au/track/track.html?id=%1$s',
						'FedEx' => 'https://www.fedex.com/apps/fedextrack/?tracknumbers=%1&cntry_code=au', 
					),
					'Canada' => array(
						'Canada Post' => 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=%1$s',
					),
					'Germany' => array(
						'DHL Intraship (DE)' => 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=%1$s&rfn=&extendedSearch=true',
						'Hermes' => 'https://tracking.hermesworld.com/?TrackID=%1$s',
						'Deutsche Post DHL' => 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=%1$s',
						'UPS Germany' => 'http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=de_DE&InquiryNumber1=%1$s',
						'DPD' => 'https://tracking.dpd.de/parcelstatus?query=%1$s&locale=en_DE',
					),
					'Ireland' => array(
						'DPD' => 'http://www2.dpd.ie/Services/QuickTrack/tabid/222/ConsignmentID/%1$s/Default.aspx',
					),
					'Italy' => array(
						'BRT (Bartolini)' => 'http://as777.brt.it/vas/sped_det_show.hsm?referer=sped_numspe_par.htm&Nspediz=%1$s',
						'DHL Express' => 'http://www.dhl.it/it/express/ricerca.html?AWB=%1$s&brand=DHL'
					),
					'India' => array(
						'DTDC' => 'http://www.dtdc.in/dtdcTrack/Tracking/consignInfo.asp?strCnno=%1$s',
					),
					'Netherlands' => array(
						'PostNL' => 'https://mijnpakket.postnl.nl/Claim?Barcode=%1$s&Postalcode=%2$s&Foreign=False&ShowAnonymousLayover=False&CustomerServiceClaim=False',
						'DPD.NL' => 'http://track.dpdnl.nl/?parcelnumber=%1$s',
					),
					'New Zealand' => array(
						'Courier Post' => 'http://trackandtrace.courierpost.co.nz/Search/%1$s',
						'NZ Post' => 'http://www.nzpost.co.nz/tools/tracking?trackid=%1$s',
						'Fastways' => 'http://www.fastway.co.nz/courier-services/track-your-parcel?l=%1$s',
						'PBT Couriers' => 'http://www.pbt.com/nick/results.cfm?ticketNo=%1$s',
					),
					'South African' => array(
						'SAPO' => 'http://sms.postoffice.co.za/TrackingParcels/Parcel.aspx?id=%1$s',
					),
					'United Kingdom' => array(
						'DHL' => 'http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=%1$s',
						'DPD' => 'http://www.dpd.co.uk/tracking/trackingSearch.do?search.searchType=0&search.parcelNumber=%1$s',
						'InterLink' => 'http://www.interlinkexpress.com/apps/tracking/?reference=%1$s&postcode=%2$s#results',
						'ParcelForce' => 'http://www.parcelforce.com/portal/pw/track?trackNumber=%1$s',
						'Royal Mail' => 'https://www.royalmail.com/track-your-item/?trackNumber=%1$s',
						'TNT Express (consignment)' => 'http://www.tnt.com/webtracker/tracking.do?requestType=GEN&searchType=CON&respLang=en&respCountry=GENERIC&sourceID=1&sourceCountry=ww&cons=%1$s&navigation=1&genericSiteIdent=',
						'TNT Express (reference)' => 'http://www.tnt.com/webtracker/tracking.do?requestType=GEN&searchType=REF&respLang=en&respCountry=GENERIC&sourceID=1&sourceCountry=ww&cons=%1$s&navigation=1&genericSiteIdent=',
						'UK Mail' => 'https://old.ukmail.com/ConsignmentStatus/ConsignmentSearchResults.aspx?SearchType=Reference&SearchString=%1$s',
					),
					'United States' => array(
						'Fedex' => 'http://www.fedex.com/Tracking?action=track&tracknumbers=%1$s',
						'FedEx Sameday' => 'https://www.fedexsameday.com/fdx_dotracking_ua.aspx?tracknum=%1$s',
						'OnTrac' => 'http://www.ontrac.com/trackingdetail.asp?tracking=%1$s',
						'UPS' => 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=%1$s',
						'USPS' => 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=%1$s',
					),
				) );

	}
	public static function shipping_providersnew( ) { 

		return $shipping_providers = apply_filters( 'wcv_shipping_providers_list', array(
					'fedex'                 => __( 'FedEx', 'wcvendors-pro' ),
					'dhl'                   => __( 'DHL Express', 'wcvendors-pro' ),
					'dhl-pieceid'           => __( 'DHL Express (Piece ID)', 'wcvendors-pro' ),
					'usps'                  => __( 'USPS', 'wcvendors-pro' ),
					'tnt'                   => __( 'TNT', 'wcvendors-pro' ),
					'tnt-reference'         => __( 'TNT Reference', 'wcvendors-pro' ),
					'dpd'                   => __( 'DPD', 'wcvendors-pro' ),
					'aramex'                => __( 'Aramex', 'wcvendors-pro' ),
					'dhl-global-mail'       => __( 'DHL eCommerce', 'wcvendors-pro' ),
					'dhl-global-forwarding' => __( 'DHL Global Forwarding', 'wcvendors-pro' ),
					'echo'                  => __( 'Echo', 'wcvendors-pro' ),
					'fedex-freight'         => __( 'FedEx Freight', 'wcvendors-pro' ),
					'canada-post'           => __( 'Canada Post', 'wcvendors-pro' ),
					'canpar'                => __( 'Canpar Courier', 'wcvendors-pro' ),
					'purolator'             => __( 'Purolator', 'wcvendors-pro' ),
					'apc'                   => __( 'APC Postal Logistics', 'wcvendors-pro' ),
					'ontrac'                => __( 'OnTrac', 'wcvendors-pro' ),
					'yrc'                   => __( 'YRC', 'wcvendors-pro' ),
					'asendia-usa'           => __( 'Asendia USA', 'wcvendors-pro' ),
					'ensenda'               => __( 'Ensenda', 'wcvendors-pro' ),
					'newgistics'            => __( 'Newgistics', 'wcvendors-pro' ),
					'lasership'             => __( 'LaserShip', 'wcvendors-pro' ),
					'expeditors'            => __( 'Expeditors', 'wcvendors-pro' ),
					'i-parcel'              => __( 'i-parcel', 'wcvendors-pro' ),
					'abf'                   => __( 'ABF Freight', 'wcvendors-pro' ),
					'old-dominion'          => __( 'Old Dominion Freight Line', 'wcvendors-pro' ),
					'con-way'               => __( 'Con-way Freight', 'wcvendors-pro' ),
					'estes'                 => __( 'Estes', 'wcvendors-pro' ),
					'rl-carriers'           => __( 'RL Carriers', 'wcvendors-pro' ),
					'directlink'            => __( 'Direct Link', 'wcvendors-pro' ),
					'international-seur'    => __( 'International Seur', 'wcvendors-pro' ),
					'gls'                   => __( 'GLS', 'wcvendors-pro' ),
					'dhl-benelux'           => __( 'DHL Benelux', 'wcvendors-pro' ),
					'postnord'              => __( 'PostNord Logistics', 'wcvendors-pro' ),
					'royal-mail'            => __( 'Royal Mail', 'wcvendors-pro' ),
					'parcel-force'          => __( 'Parcel Force', 'wcvendors-pro' ),
					'fedex-uk'              => __( 'FedEx UK', 'wcvendors-pro' ),
					'dpd-uk'                => __( 'DPD UK', 'wcvendors-pro' ),
					'collectplus'           => __( 'Collect+', 'wcvendors-pro' ),
					'skynetworldwide'       => __( 'SkyNet Worldwide Express', 'wcvendors-pro' ),
					'skynetworldwide-uk'    => __( 'Skynet Worldwide Express UK', 'wcvendors-pro' ),
					'tnt-uk'                => __( 'TNT UK', 'wcvendors-pro' ),
					'tnt-uk-reference'      => __( 'TNT UK Reference', 'wcvendors-pro' ),
					'interlink-express'     => __( 'DPD Local', 'wcvendors-pro' ),
					'interlink-express-reference' => __( 'DPD Local reference', 'wcvendors-pro' ),
					'uk-mail'               => __( 'UK Mail', 'wcvendors-pro' ),
					'yodel'                 => __( 'Yodel Domestic', 'wcvendors-pro' ),
					'yodel-international'   => __( 'Yodel International', 'wcvendors-pro' ),
					'hermes'                => __( 'Hermesworld', 'wcvendors-pro' ),
					'myhermes-uk'           => __( 'myHermes UK', 'wcvendors-pro' ),
					'norsk-global'          => __( 'Norsk Global', 'wcvendors-pro' ),
					'panther'               => __( 'Panther', 'wcvendors-pro' ),
					'deltec-courier'        => __( 'Deltec Courier', 'wcvendors-pro' ),
					'xdp-uk'                => __( 'XDP Express', 'wcvendors-pro' ),
					'trakpak'               => __( 'TrakPak', 'wcvendors-pro' ),
					'xdp-uk-reference'      => __( 'XDP Express Reference', 'wcvendors-pro' ),
					'an-post'               => __( 'An Post', 'wcvendors-pro' ),
					'dpd-ireland'           => __( 'DPD Ireland', 'wcvendors-pro' ),
					'fastway-ireland'       => __( 'Fastway Ireland', 'wcvendors-pro' ),
					'nightline'             => __( 'Nightline', 'wcvendors-pro' ),
					'tuffnells'             => __( 'Tuffnells Parcels Express', 'wcvendors-pro' ),
					'arrowxl'               => __( 'Arrow XL', 'wcvendors-pro' ),
					'deutsch-post'          => __( 'Deutsche Post Mail', 'wcvendors-pro' ),
					'dhl-germany'           => __( 'Deutsche Post DHL', 'wcvendors-pro' ),
					'hermes-de'             => __( 'Hermes Germany', 'wcvendors-pro' ),
					'dpd-de'                => __( 'DPD Germany', 'wcvendors-pro' ),
					'asendia-de'            => __( 'Asendia Germany', 'wcvendors-pro' ),
					'dhl-deliverit'         => __( 'DHL 2-Mann-Handling', 'wcvendors-pro' ),
					'austrian-post-registered' => __( 'Austrian Post (Registered)', 'wcvendors-pro' ),
					'austrian-post'         => __( 'Austrian Post (Express)', 'wcvendors-pro' ),
					'swiss-post'            => __( 'Swiss Post', 'wcvendors-pro' ),
					'spain-correos-es'      => __( 'Correos de España', 'wcvendors-pro' ),
					'correosexpress'        => __( 'Correos Express', 'wcvendors-pro' ),
					'nacex-spain'           => __( 'NACEX Spain', 'wcvendors-pro' ),
					'spanish-seur'          => __( 'Spanish Seur', 'wcvendors-pro' ),
					'asm'                   => __( 'ASM', 'wcvendors-pro' ),
					'mrw-spain'             => __( 'MRW', 'wcvendors-pro' ),
					'envialia'              => __( 'Envialia', 'wcvendors-pro' ),
					'redur-es'              => __( 'Redur Spain', 'wcvendors-pro' ),
					'packlink'              => __( 'Packlink', 'wcvendors-pro' ),
					'dhl-es'                => __( 'DHL Spain Domestic', 'wcvendors-pro' ),
					'magyar-posta'          => __( 'Magyar Posta', 'wcvendors-pro' ),
					'exapaq'                => __( 'DPD France', 'wcvendors-pro' ),
					'portugal-ctt'          => __( 'Portugal CTT', 'wcvendors-pro' ),
					'chronopost-portugal'   => __( 'Chronopost Portugal', 'wcvendors-pro' ),
					'portugal-seur'         => __( 'Portugal Seur', 'wcvendors-pro' ),
					'adicional'             => __( 'Adicional Logistics', 'wcvendors-pro' ),
					'la-poste-colissimo'    => __( 'La Poste', 'wcvendors-pro' ),
					'colissimo'             => __( 'Colissimo', 'wcvendors-pro' ),
					'chronopost-france'     => __( 'Chronopost France', 'wcvendors-pro' ),
					'tnt-fr'                => __( 'TNT France', 'wcvendors-pro' ),
					'colis-prive'           => __( 'Colis Privé', 'wcvendors-pro' ),
					'geodis-calberson-fr'   => __( 'GEODIS - Distribution & Express', 'wcvendors-pro' ),
					'bert-fr'               => __( 'Bert Transport', 'wcvendors-pro' ),
					'sic-teliway'           => __( 'Teliway SIC Express', 'wcvendors-pro' ),
					'geodis-espace'         => __( 'Geodis E-space', 'wcvendors-pro' ),
					'mondialrelay'          => __( 'Mondial Relay', 'wcvendors-pro' ),
					'postnl'                => __( 'PostNL', 'wcvendors-pro' ),
					'postnl-international'  => __( 'PostNL International', 'wcvendors-pro' ),
					'postnl-3s'             => __( 'PostNL International 3S', 'wcvendors-pro' ),
					'dhl-nl'                => __( 'DHL Netherlands', 'wcvendors-pro' ),
					'gls-netherlands'       => __( 'GLS Netherlands', 'wcvendors-pro' ),
					'dhlparcel-nl'          => __( 'DHL Parcel NL', 'wcvendors-pro' ),
					'transmission-nl'       => __( 'TransMission', 'wcvendors-pro' ),
					'bpost'                 => __( 'Bpost', 'wcvendors-pro' ),
					'bpost-international'   => __( 'Bpost international', 'wcvendors-pro' ),
					'landmark-global'       => __( 'Landmark Global', 'wcvendors-pro' ),
					'posti'                 => __( 'Posti', 'wcvendors-pro' ),
					'posten-norge'          => __( 'Posten Norge / Bring', 'wcvendors-pro' ),
					'sweden-posten'         => __( 'PostNord Sweden', 'wcvendors-pro' ),
					'dbschenker-se'         => __( 'DB Schenker', 'wcvendors-pro' ),
					'matkahuolto'           => __( 'Matkahuolto', 'wcvendors-pro' ),
					'danmark-post'          => __( 'PostNord Denmark', 'wcvendors-pro' ),
					'inpost-paczkomaty'     => __( 'InPost Paczkomaty', 'wcvendors-pro' ),
					'raben-group'           => __( 'Raben Group', 'wcvendors-pro' ),
					'italy-sda'             => __( 'Italy SDA', 'wcvendors-pro' ),
					'dmm-network'           => __( 'DMM Network', 'wcvendors-pro' ),
					'poste-italiane-paccocelere' => __( 'Poste Italiane Paccocelere', 'wcvendors-pro' ),
					'poste-italiane'        => __( 'Poste Italiane', 'wcvendors-pro' ),
					'brt-it'                => __( 'BRT Bartolini', 'wcvendors-pro' ),
					'fercam'                => __( 'FERCAM Logistics & Transport', 'wcvendors-pro' ),
					'gls-italy'             => __( 'GLS Italy', 'wcvendors-pro' ),
					'sgt-it'                => __( 'SGT Corriere Espresso', 'wcvendors-pro' ),
					'tnt-it'                => __( 'TNT Italy', 'wcvendors-pro' ),
					'tntpost-it'            => __( 'Nexive (TNT Post Italy)', 'wcvendors-pro' ),
					'tnt-click'             => __( 'TNT-Click Italy', 'wcvendors-pro' ),
					'russian-post'          => __( 'Russian Post', 'wcvendors-pro' ),
					'postur-is'             => __( 'Iceland Post', 'wcvendors-pro' ),
					'nova-poshta'           => __( 'Nova Poshta', 'wcvendors-pro' ),
					'dhl-poland'            => __( 'DHL Poland Domestic', 'wcvendors-pro' ),
					'poczta-polska'         => __( 'Poczta Polska', 'wcvendors-pro' ),
					'dpd-poland'            => __( 'DPD Poland', 'wcvendors-pro' ),
					'siodemka'              => __( 'Siodemka', 'wcvendors-pro' ),
					'opek'                  => __( 'FedEx Poland Domestic', 'wcvendors-pro' ),
					'lietuvos-pastas'       => __( 'Lietuvos Paštas', 'wcvendors-pro' ),
					'ceska-posta'           => __( 'Česká Pošta', 'wcvendors-pro' ),
					'elta-courier'          => __( 'ELTA Hellenic Post', 'wcvendors-pro' ),
					'acscourier'            => __( 'ACS Courier', 'wcvendors-pro' ),
					'taxydromiki'           => __( 'Geniki Taxydromiki', 'wcvendors-pro' ),
					'speedcouriers-gr'      => __( 'Speed Couriers', 'wcvendors-pro' ),
					'ptt-posta'             => __( 'PTT Posta', 'wcvendors-pro' ),
					'belpost'               => __( 'Belpost', 'wcvendors-pro' ),
					'bgpost'                => __( 'Bulgarian Posts', 'wcvendors-pro' ),
					'hrvatska-posta'        => __( 'Hrvatska Pošta', 'wcvendors-pro' ),
					'kn'                    => __( 'Kuehne + Nagel', 'wcvendors-pro' ),
					'posta-romana'          => __( 'Poșta Română', 'wcvendors-pro' ),
					'cyprus-post'           => __( 'Cyprus Post', 'wcvendors-pro' ),
					'ukrposhta'             => __( 'UkrPoshta', 'wcvendors-pro' ),
					'sf-express'            => __( 'S.F. Express', 'wcvendors-pro' ),
					'sfb2c'                 => __( 'S.F International', 'wcvendors-pro' ),
					'dhl-global-mail-asia'  => __( 'DHL Global Mail Asia', 'wcvendors-pro' ),
					'kerry-logistics'       => __( 'Kerry Express Thailand', 'wcvendors-pro' ),
					'quantium'              => __( 'Quantium', 'wcvendors-pro' ),
					'dpex'                  => __( 'DPEX', 'wcvendors-pro' ),
					'omniparcel'            => __( 'Omni Parcel', 'wcvendors-pro' ),
					'china-post'            => __( 'China Post', 'wcvendors-pro' ),
					'china-ems'             => __( 'China EMS (ePacket)', 'wcvendors-pro' ),
					'4px'                   => __( '4PX', 'wcvendors-pro' ),
					'yanwen'                => __( 'Yanwen', 'wcvendors-pro' ),
					'sfcservice'            => __( 'SFC Service', 'wcvendors-pro' ),
					'ec-firstclass'         => __( 'EC-Firstclass', 'wcvendors-pro' ),
					'aupost-china'          => __( 'AuPost China', 'wcvendors-pro' ),
					'wedo'                  => __( 'WeDo Logistics', 'wcvendors-pro' ),
					'ppbyb'                 => __( 'PayPal Package', 'wcvendors-pro' ),
					'jcex'                  => __( 'JCEX', 'wcvendors-pro' ),
					'wishpost'              => __( 'WishPost', 'wcvendors-pro' ),
					'sto'                   => __( 'STO Express', 'wcvendors-pro' ),
					'greyhound'             => __( 'Greyhound', 'wcvendors-pro' ),
					'globegistics'          => __( 'Globegistics Inc.', 'wcvendors-pro' ),
					'post56'                => __( 'Post56', 'wcvendors-pro' ),
					'flytexpress'           => __( 'Flyt Express', 'wcvendors-pro' ),
					'szdpex'                => __( 'DPEX China', 'wcvendors-pro' ),
					'800bestex'             => __( 'Best Express', 'wcvendors-pro' ),
					'cnexps'                => __( 'CNE Express', 'wcvendors-pro' ),
					'equick-cn'             => __( 'Equick China', 'wcvendors-pro' ),
					'boxc'                  => __( 'BOXC', 'wcvendors-pro' ),
					'buylogic'              => __( 'Buylogic', 'wcvendors-pro' ),
					'hong-kong-post'        => __( 'Hong Kong Post', 'wcvendors-pro' ),
					'empsexpress'           => __( 'EMPS Express', 'wcvendors-pro' ),
					'idexpress'             => __( 'IDEX', 'wcvendors-pro' ),
					'sekologistics'         => __( 'SEKO Logistics', 'wcvendors-pro' ),
					'pfcexpress'            => __( 'PFC Express', 'wcvendors-pro' ),
					'cpacket'               => __( 'cPacket', 'wcvendors-pro' ),
					'yto'                   => __( 'YTO Express', 'wcvendors-pro' ),
					'taqbin-hk'             => __( 'TAQBIN Hong Kong', 'wcvendors-pro' ),
					'detrack'               => __( 'Detrack', 'wcvendors-pro' ),
					'tgx'                   => __( 'Kerry Express Hong Kong', 'wcvendors-pro' ),
					'ramgroup-za'           => __( 'RAM', 'wcvendors-pro' ),
					'zjs-express'           => __( 'ZJS International', 'wcvendors-pro' ),
					'asendia-uk'            => __( 'Asendia UK', 'wcvendors-pro' ),
					'lwe-hk'                => __( 'Logistic Worldwide Express', 'wcvendors-pro' ),
					'yunexpress'            => __( 'Yun Express', 'wcvendors-pro' ),
					'nationwide-my'         => __( 'Nationwide Express', 'wcvendors-pro' ),
					'cuckooexpress'         => __( 'Cuckoo Express', 'wcvendors-pro' ),
					'rrdonnelley'           => __( 'RRD International Logistics U.S.A', 'wcvendors-pro' ),
					'maxcellents'           => __( 'Maxcellents Pte Ltd', 'wcvendors-pro' ),
					'oneworldexpress'       => __( 'One World Express', 'wcvendors-pro' ),
					'hh-exp'                => __( 'Hua Han Logistics', 'wcvendors-pro' ),
					'ajexpress'             => __( '>a j express', 'wcvendors-pro' ),
					'hunter-express'        => __( 'Hunter Express', 'wcvendors-pro' ),
					'yakit'                 => __( 'Yakit', 'wcvendors-pro' ),
					'dhl-active-tracing'    => __( 'DHL Active Tracing', 'wcvendors-pro' ),
					'eparcel-kr'            => __( 'eParcel Korea', 'wcvendors-pro' ),
					'dex-i'                 => __( 'DEX-I', 'wcvendors-pro' ),
					'febrt-it-parceliddex'  => __( 'BRT Bartolini(Parcel ID)', 'wcvendors-pro' ),
					'sendit'                => __( 'Sendit', 'wcvendors-pro' ),
					'mailamericas'          => __( 'MailAmericas', 'wcvendors-pro' ),
					'correos-de-costa-rica' => __( 'Correos de Costa Rica', 'wcvendors-pro' ),
					'copa-courier'          => __( 'Copa Airlines Courier', 'wcvendors-pro' ),
					'mara-xpress'           => __( 'Mara Xpress', 'wcvendors-pro' ),
					'mikropakket'           => __( 'Mikropakket', 'wcvendors-pro' ),
					'wndirect'              => __( 'wnDirect', 'wcvendors-pro' ),
					'kiala'                 => __( 'Kiala', 'wcvendors-pro' ),
					'ruston'                => __( 'Ruston', 'wcvendors-pro' ),
					'wanbexpress'           => __( 'WanbExpress', 'wcvendors-pro' ),
					'cj-korea-thai'         => __( 'CJ Korea Express (Thailand)', 'wcvendors-pro' ),
					'wise-express'          => __( 'Wise Express', 'wcvendors-pro' ),
					'rpd2man'               => __( 'RPD2man Deliveries', 'wcvendors-pro' ),
					'acommerce'             => __( 'aCommerce', 'wcvendors-pro' ),
					'panther-reference'     => __( 'Panther Reference', 'wcvendors-pro' ),
					'logwin-logistics'      => __( 'Logwin Logistics', 'wcvendors-pro' ),
					'wiseloads'             => __( 'Wiseloads', 'wcvendors-pro' ),
					'rincos'                => __( 'Rincos', 'wcvendors-pro' ),
					'alliedexpress'         => __( 'Allied Express', 'wcvendors-pro' ),
					'hermes-it'             => __( 'Hermes Italy', 'wcvendors-pro' ),
					'asendia-hk'            => __( 'Asendia HK', 'wcvendors-pro' ),
					'ninjavan-thai'         => __( 'Ninja Van Thailand', 'wcvendors-pro' ),
					'eurodis'               => __( 'Eurodis', 'wcvendors-pro' ),
					'whistl'                => __( 'Whistl', 'wcvendors-pro' ),
					'abcustom'              => __( 'AB Custom Group', 'wcvendors-pro' ),
					'tipsa'                 => __( 'TIPSA', 'wcvendors-pro' ),
					'ninjavan-philippines'  => __( 'Ninja Van Philippines', 'wcvendors-pro' ),
					'zto-express'           => __( 'ZTO Express', 'wcvendors-pro' ),
					'nim-express'           => __( 'Nim Express', 'wcvendors-pro' ),
					'alljoy'                => __( 'ALLJOY SUPPLY CHAIN CO., LTD', 'wcvendors-pro' ),
					'doora'                 => __( 'Doora Logistics', 'wcvendors-pro' ),
					'dynalogic'             => __( 'Dynalogic Benelux BV', 'wcvendors-pro' ),
					'collivery'             => __( 'MDS Collivery Pty (Ltd)', 'wcvendors-pro' ),
					'gsi-express'           => __( 'GSI EXPRESS', 'wcvendors-pro' ),
					'rcl'                   => __( 'Red Carpet Logistics', 'wcvendors-pro' ),
					'mxe'                   => __( 'MXE Express', 'wcvendors-pro' ),
					'birdsystem'            => __( 'BirdSystem', 'wcvendors-pro' ),
					'paquetexpress'         => __( 'Paquetexpress', 'wcvendors-pro' ),
					'line'                  => __( 'Line Clear Express & Logistics Sdn Bhd', 'wcvendors-pro' ),
					'efs'                   => __( 'EFS (E-commerce Fulfillment Service)', 'wcvendors-pro' ),
					'instant'               => __( 'INSTANT (Tiong Nam Ebiz Express Sdn Bhd)', 'wcvendors-pro' ),
					'kpost'                 => __( 'Korea Post', 'wcvendors-pro' ),
					'collectco'             => __( 'CollectCo', 'wcvendors-pro' ),
					'ddexpress'             => __( 'DD Express Courier', 'wcvendors-pro' ),
					'smooth'                => __( 'Smooth Couriers', 'wcvendors-pro' ),
					'panther-order-number'  => __( 'Panther Order Number', 'wcvendors-pro' ),
					'sailpost'              => __( 'SAILPOST', 'wcvendors-pro' ),
					'tcs'                   => __( 'TCS', 'wcvendors-pro' ),
					'ezship'                => __( 'EZship', 'wcvendors-pro' ),
					'nova-poshtaint'        => __( 'Nova Poshta (International)', 'wcvendors-pro' ),
					'skynetworldwide-uae'   => __( 'SkyNet Worldwide Express UAE', 'wcvendors-pro' ),
					'demandship'            => __( 'DemandShip', 'wcvendors-pro' ),
					'pickup'                => __( 'PICK-UP', 'wcvendors-pro' ),
					'mudita'                => __( 'MUDITA', 'wcvendors-pro' ),
					'mainfreight'           => __( 'Mainfreight', 'wcvendors-pro' ),
					'aprisaexpress'         => __( 'Aprisa Express', 'wcvendors-pro' ),
					'homedirect-logistics'  => __( 'Homedirect Logistics', 'wcvendors-pro' ),
					'imxmail'               => __( 'IMX Mail', 'wcvendors-pro' ),
					'cbl-logistica'         => __( 'CBL Logistics', 'wcvendors-pro' ),
					'easy-mail'             => __( 'Easy Mail', 'wcvendors-pro' ),
					'dhl-hk'                => __( 'DHL Hong Kong', 'wcvendors-pro' ),
					'raiderex'              => __( 'RaidereX', 'wcvendors-pro' ),
					'itis'                  => __( 'ITIS International', 'wcvendors-pro' ),
					'imexglobalsolutions'   => __( 'IMEX Global Solutions', 'wcvendors-pro' ),
					'singapore-post'        => __( 'Singapore Post', 'wcvendors-pro' ),
					'rpxonline'             => __( 'RPX Online', 'wcvendors-pro' ),
					'17postservice'         => __( '17 Post Service', 'wcvendors-pro' ),
					'nanjingwoyuan'         => __( 'Nanjing Woyuan', 'wcvendors-pro' ),
					'roadbull'              => __( 'Roadbull Logistics', 'wcvendors-pro' ),
					'jersey-post'           => __( 'Jersey Post', 'wcvendors-pro' ),
					'directlog'             => __( 'Directlog', 'wcvendors-pro' ),
					'ekart '                => __( 'Ekart', 'wcvendors-pro' ),
					'zyllem'                => __( 'Zyllem', 'wcvendors-pro' ),
					'singapore-speedpost'   => __( 'Singapore Speedpost', 'wcvendors-pro' ),
					'yundaex'               => __( 'Yunda Express', 'wcvendors-pro' ),
					'360lion'               => __( '360 Lion Express', 'wcvendors-pro' ),
					'gofly'                 => __( 'GoFly', 'wcvendors-pro' ),
					'dpe-express'           => __( 'DPE Express', 'wcvendors-pro' ),
					'simplypost'            => __( 'SimplyPost', 'wcvendors-pro' ),
					'taqbin-sg'             => __( 'TAQBIN Singapore', 'wcvendors-pro' ),
					'xq-express'            => __( 'XQ Express', 'wcvendors-pro' ),
					'xl-express'            => __( 'XL Express', 'wcvendors-pro' ),
					'ninjavan'              => __( 'Ninja Van', 'wcvendors-pro' ),
					'vtfe'                  => __( 'VicTas Freight Express', 'wcvendors-pro' ),
					'zalora-7-eleven'       => __( 'Zalora 7-Eleven', 'wcvendors-pro' ),
					'jet-ship'              => __( 'Jet-Ship Worldwide', 'wcvendors-pro' ),
					'nhans-solutions'       => __( 'Nhans Solutions', 'wcvendors-pro' ),
					'kgmhub'                => __( 'KGM Hub', 'wcvendors-pro' ),
					'qxpress'               => __( 'Qxpress', 'wcvendors-pro' ),
					'rzyexpress'            => __( 'RZY Express', 'wcvendors-pro' ),
					'parcelpost-sg'         => __( 'Parcel Post Singapore', 'wcvendors-pro' ),
					'parcel-express'        => __( 'Parcel Express', 'wcvendors-pro' ),
					'korea-post'            => __( 'Korea Post EMS', 'wcvendors-pro' ),
					'cj-gls'                => __( 'CJ GLS', 'wcvendors-pro' ),
					'ecargo-asia'           => __( 'Ecargo', 'wcvendors-pro' ),
					'srekorea'              => __( 'SRE Korea', 'wcvendors-pro' ),
					'rocketparcel'          => __( 'Rocket Parcel International', 'wcvendors-pro' ),
					'india-post'            => __( 'India Post Domestic', 'wcvendors-pro' ),
					'india-post-int'        => __( 'India Post International', 'wcvendors-pro' ),
					'delhivery'             => __( 'Delhivery', 'wcvendors-pro' ),
					'bluedart'              => __( 'Bluedart', 'wcvendors-pro' ),
					'nuvoex'                => __( 'NuvoEx', 'wcvendors-pro' ),
					'dtdc'                  => __( 'DTDC India', 'wcvendors-pro' ),
					'delcart-in'            => __( 'Delcart', 'wcvendors-pro' ),
					'professional-couriers' => __( 'Professional Couriers', 'wcvendors-pro' ),
					'parcelled-in'          => __( 'Parcelled.in', 'wcvendors-pro' ),
					'safexpress'            => __( 'Safexpress', 'wcvendors-pro' ),
					'red-express'           => __( 'Red Express', 'wcvendors-pro' ),
					'red-express-wb'        => __( 'Red Express Waybill', 'wcvendors-pro' ),
					'first-flight'          => __( 'First Flight Couriers', 'wcvendors-pro' ),
					'gati-kwe'              => __( 'Gati-KWE', 'wcvendors-pro' ),
					'gojavas'               => __( 'GoJavas', 'wcvendors-pro' ),
					'ecom-express'          => __( 'Ecom Express', 'wcvendors-pro' ),
					'xpressbees'            => __( 'XpressBees', 'wcvendors-pro' ),
					'japan-post'            => __( 'Japan Post', 'wcvendors-pro' ),
					'taqbin-jp'             => __( 'Yamato Japan', 'wcvendors-pro' ),
					'sagawa'                => __( 'Sagawa', 'wcvendors-pro' ),
					'taiwan-post'           => __( 'Taiwan Post', 'wcvendors-pro' ),
					'bh-posta'              => __( 'JP BH Pošta', 'wcvendors-pro' ),
					'spreadel'              => __( 'Spreadel', 'wcvendors-pro' ),
					'malaysia-post'         => __( 'Malaysia Post EMS / Pos Laju', 'wcvendors-pro' ),
					'malaysia-post-posdaftar'=> __( 'Malaysia Post - Registered', 'wcvendors-pro' ),
					'taqbin-my'             => __( 'TAQBIN Malaysia', 'wcvendors-pro' ),
					'abxexpress-my'         => __( 'ABX Express', 'wcvendors-pro' ),
					'gdex'                  => __( 'GDEX', 'wcvendors-pro' ),
					'skynet'                => __( 'SkyNet Malaysia', 'wcvendors-pro' ),
					'citylinkexpress'       => __( 'City-Link Express', 'wcvendors-pro' ),
					'mypostonline'          => __( 'Mypostonline', 'wcvendors-pro' ),
					'airpak-express'        => __( 'Airpak Express', 'wcvendors-pro' ),
					'ninjavan-my'           => __( 'Ninja Van Malaysia', 'wcvendors-pro' ),
					'matdespatch'           => __( 'Matdespatch', 'wcvendors-pro' ),
					'thailand-post'         => __( 'Thailand Thai Post', 'wcvendors-pro' ),
					'dynamic-logistics'     => __( 'Dynamic Logistics', 'wcvendors-pro' ),
					'2go'                   => __( '2GO', 'wcvendors-pro' ),
					'courex'                => __( 'Courex', 'wcvendors-pro' ),
					'xend'                  => __( 'Xend Express', 'wcvendors-pro' ),
					'lbcexpress'            => __( 'LBC Express', 'wcvendors-pro' ),
					'air21'                 => __( 'AIR21', 'wcvendors-pro' ),
					'jam-express'           => __( 'Jam Express', 'wcvendors-pro' ),
					'airspeed'              => __( 'Airspeed International Corporation', 'wcvendors-pro' ),
					'raf'                   => __( 'RAF Philippines', 'wcvendors-pro' ),
					'pos-indonesia'         => __( 'Pos Indonesia Domestic', 'wcvendors-pro' ),
					'pos-indonesia-int'     => __( 'Pos Indonesia Intl', 'wcvendors-pro' ),
					'jayonexpress'          => __( 'Jayon Express (JEX)', 'wcvendors-pro' ),
					'jne'                   => __( 'JNE', 'wcvendors-pro' ),
					'pandulogistics'        => __( 'Pandu Logistics', 'wcvendors-pro' ),
					'rpx'                   => __( 'RPX Indonesia', 'wcvendors-pro' ),
					'tiki'                  => __( 'Tiki', 'wcvendors-pro' ),
					'lion-parcel'           => __( 'Lion Parcel', 'wcvendors-pro' ),
					'ninjavan-id'           => __( 'Ninja Van Indonesia', 'wcvendors-pro' ),
					'wahana'                => __( 'Wahana', 'wcvendors-pro' ),
					'first-logistics'       => __( 'First Logistics', 'wcvendors-pro' ),
					'vnpost'                => __( 'Vietnam Post', 'wcvendors-pro' ),
					'ghn'                   => __( 'Giao hàng nhanh', 'wcvendors-pro' ),
					'vnpost-ems'            => __( 'Vietnam Post EMS', 'wcvendors-pro' ),
					'lao-post'              => __( 'Lao Post', 'wcvendors-pro' ),
					'viettelpost'           => __( 'ViettelPost', 'wcvendors-pro' ),
					'kerryttc-vn'           => __( 'Kerry TTC Express', 'wcvendors-pro' ),
					'cambodia-post'         => __( 'Cambodia Post', 'wcvendors-pro' ),
					'australia-post'        => __( 'Australia Post', 'wcvendors-pro' ),
					'dotzot'                => __( 'Dotzot', 'wcvendors-pro' ),
					'scudex-express'        => __( 'Scudex Express', 'wcvendors-pro' ),
					'toll-ipec'             => __( 'Toll IPEC', 'wcvendors-pro' ),
					'toll-priority'         => __( 'Toll Priority', 'wcvendors-pro' ),
					'tnt-au'                => __( 'TNT Australia', 'wcvendors-pro' ),
					'adsone'                => __( 'ADSOne', 'wcvendors-pro' ),
					'fastway-au'            => __( 'Fastway Australia', 'wcvendors-pro' ),
					'star-track'            => __( 'StarTrack', 'wcvendors-pro' ),
					'dtdc-au'               => __( 'DTDC Australia', 'wcvendors-pro' ),
					'sendle'                => __( 'Sendle', 'wcvendors-pro' ),
					'star-track-express'    => __( 'Star Track Express', 'wcvendors-pro' ),
					'couriers-please'       => __( 'Couriers Please', 'wcvendors-pro' ),
					'new-zealand-post'      => __( 'New Zealand Post', 'wcvendors-pro' ),
					'directfreight-au'      => __( 'Direct Freight Express', 'wcvendors-pro' ),
					'courierpost'           => __( 'CourierPost', 'wcvendors-pro' ),
					'fastway-nz'            => __( 'Fastway New Zealand', 'wcvendors-pro' ),
					'ubi-logistics'         => __( 'UBI Smart Parcel', 'wcvendors-pro' ),
					'bondscouriers'         => __( 'Bonds Couriers', 'wcvendors-pro' ),
					'correos-de-mexico'     => __( 'Correos de Mexico', 'wcvendors-pro' ),
					'estafeta'              => __( 'Estafeta', 'wcvendors-pro' ),
					'mexico-senda-express'  => __( 'Mexico Senda Express', 'wcvendors-pro' ),
					'mexico-redpack'        => __( 'Mexico Redpack', 'wcvendors-pro' ),
					'aeroflash'             => __( 'Mexico AeroFlash', 'wcvendors-pro' ),
					'brazil-correios'       => __( 'Brazil Correios', 'wcvendors-pro' ),
					'correos-chile'         => __( 'Correos Chile', 'wcvendors-pro' ),
					'correo-argentino'      => __( 'Correo Argentino', 'wcvendors-pro' ),
					'oca-ar'                => __( 'OCA Argentina', 'wcvendors-pro' ),
					'emirates-post'         => __( 'emirates-post', 'wcvendors-pro' ),
					'4-72'                  => __( '4-72 Entregando', 'wcvendors-pro' ),
					'thecourierguy'         => __( 'The Courier Guy', 'wcvendors-pro' ),
					'israel-post'           => __( 'Israel Post', 'wcvendors-pro' ),
					'israel-post-domestic'  => __( 'Israel Post Domestic', 'wcvendors-pro' ),
					'sapo'                  => __( 'South African Post Office', 'wcvendors-pro' ),
					'alphafast'             => __( 'alphaFAST', 'wcvendors-pro' ),
					'dawnwing'              => __( 'Dawn Wing', 'wcvendors-pro' ),
					'fastway-za'            => __( 'Fastway South Africa', 'wcvendors-pro' ),
					'specialisedfreight-za' => __( 'Specialised Freight', 'wcvendors-pro' ),
					'dpe-za'                => __( 'DPE South Africa', 'wcvendors-pro' ),
					'smsa-express'          => __( 'SMSA Express', 'wcvendors-pro' ),
					'courierit'             => __( 'Courier IT', 'wcvendors-pro' ),
					'saudi-post'            => __( 'Saudi Post', 'wcvendors-pro' ),
					'axl'                   => __( 'AXL Express & Logistics', 'wcvendors-pro' ),
					'nipost'                => __( 'NiPost', 'wcvendors-pro' ),
					'courier-plus'          => __( 'Courier Plus', 'wcvendors-pro' ),
					'speedexcourier'        => __( 'Speedex Courier', 'wcvendors-pro' ),
					'post-serbia'           => __( 'Post Serbia', 'wcvendors-pro' ),
					'skypostal'             => __( 'Asendia HK – Premium Service (LATAM)', 'wcvendors-pro' ),
					'kangaroo-my'           => __( 'Kangaroo Worldwide Express', 'wcvendors-pro' ),
					'fastrak-th'            => __( 'Fastrak Services', 'wcvendors-pro' ),
					'jocom'                 => __( 'Jocom', 'wcvendors-pro' ),
					'b2ceurope'             => __( 'B2C Europe', 'wcvendors-pro' ),
					'costmeticsnow'         => __( 'Cosmetics Now', 'wcvendors-pro' ),
					
				) );

	}


	/**
	 * Filter the customers shipping address for the order table and view order details 
	 * 
	 * @since 1.3.6 
	 * @access public 
	 */
	public function filter_formatted_shipping_address( $address ){ 

		$hide_name 				= WC_Vendors::$pv_options->get_option( 'hide_order_customer_name' );
		$hide_shipping_address 	= WC_Vendors::$pv_options->get_option( 'hide_order_customer_shipping_address' );

		if ( $hide_name ) { 
			unset( $address['first_name'] ); 
			unset( $address['last_name'] ); 
		}

		if ( $hide_shipping_address ) { 
			unset( $address[ 'company' ] ); 
			unset( $address[ 'address_1' ] ); 
			unset( $address[ 'address_2' ] ); 
			unset( $address[ 'city' ] ); 
			unset( $address[ 'state' ] ); 
			unset( $address[ 'postcode' ] ); 
			unset( $address[ 'country' ] ); 
		}

		return $address; 

	} // filter_formatted_shipping_address


	/**
	 * Filter the customers billing address for view order details
	 * 
	 * @since 1.3.6 
	 * @access public 
	 */
	public function filter_formatted_billing_address( $address ){ 

		$hide_name 				= WC_Vendors::$pv_options->get_option( 'hide_order_customer_name' );
		$hide_billing_address 	= WC_Vendors::$pv_options->get_option( 'hide_order_customer_billing_address' );

		if ( $hide_name ) { 
			unset( $address['first_name'] ); 
			unset( $address['last_name'] ); 
		}

		if ( $hide_billing_address ) { 
			unset( $address[ 'company' ] ); 
			unset( $address[ 'address_1' ] ); 
			unset( $address[ 'address_2' ] ); 
			unset( $address[ 'city' ] ); 
			unset( $address[ 'state' ] ); 
			unset( $address[ 'postcode' ] ); 
			unset( $address[ 'country' ] ); 
		}

		return $address; 

	} // filter_formatted_billing_address

}