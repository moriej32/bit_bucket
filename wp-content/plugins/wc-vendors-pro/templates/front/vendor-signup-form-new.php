<?php
/**
 * The template for displaying the vendor application form 
 *
 * Override this template by copying it to yourtheme/wc-vendors/front
 *
 * @package    WCVendors_Pro
 * @version    1.3.2
 */
?>
<form method="post" action="" class="wcv-form"> 

	<?php WCVendors_Pro_Store_Form::sign_up_form_data(); ?>

	<h3><?php _e( 'Vendor Application', 'wcvendors-pro'); ?></h3>

	<div class="wcv-signupnotice"> 
		<?php echo $vendor_signup_notice; ?>
	</div>

	        <br />
          <?php WCVendors_Pro_Store_Form::store_name( '' ); ?>
	       <div class="control-group"><label for="_wcv_paypal_address" class="">PayPal Address</label><div class="control">
			<input type="email" class="" style="" name="_wcv_paypal_address" id="_wcv_paypal_address" value="" placeholder="your paypal address" required> 
			</div><p class="tip">Your PayPal address is used to send you your commission.</p></div>
	
	       <p class="form-row">
				<input type="submit" class="button" name="apply_for_vendor_submit" value="Submit">
			</p>

	</form>