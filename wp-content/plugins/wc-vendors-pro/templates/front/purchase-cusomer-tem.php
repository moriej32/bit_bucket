<?php
/**
 * The template for displaying the vendor application form 
 *
 * Override this template by copying it to yourtheme/wc-vendors/front
 *
 * @package    WCVendors_Pro
 * @version    1.3.2
 */
?>
<style>
 th,td{text-align: center;}
 table{width:100%}
 .button.view, .button.leave_feedback {
    background-color: #96588a !important;
    border-color: #96588a !important;
    color: #ffffff !important;
    text-decoration: none !important;
    padding: .618em .857em;
    line-height: 17px !important;
    font-size: .857em;
    margin-right: .236em;
}
.button.refund{
    background-color: #96588a !important;
    border-color: #96588a !important;
    color: #ffffff !important;
    text-decoration: none !important;
    padding: .618em .857em;
    line-height: 17px !important;
   font-size: 1em;
    margin-right: .236em;
}
.button {
    font-size: 100%;
    margin: 0;
    line-height: 1;
    cursor: pointer;
    position: relative;
    font-family: inherit;
    text-decoration: none;
    overflow: visible;
    padding: .618em 1em;
    font-weight: 700;
    border-radius: 3px;
    left: auto;
    color: #515151;
    background-color: #ebe9eb;
    border: 0;
        border-top-color: currentcolor;
        border-right-color: currentcolor;
        border-bottom-color: currentcolor;
        border-left-color: currentcolor;
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
}
#refunded{
	pointer-events: none;
	background-color: gray !important;
}
span{white-space: nowrap;
display: inline-block;
background-image: none;
box-shadow: none;
-webkit-box-shadow: none;
text-shadow: none;
font-weight: 700;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
    font-weight: 700;
    margin-right: 13px;
}
.btn-danger {
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
    font-weight: 700;
    margin-right: 13px;
}

</style>

	<?php WCVendors_Pro_Store_Form::sign_up_form_data(); ?>

	<h3><?php _e( 'My Purchases', 'wcvendors-pro'); ?></h3>

	<div class="wcv-signupnotice"> 
		<?php echo $vendor_signup_notice; ?>
	</div>

	<br />

	<div class="wcv-tabs top" data-prevent-url-change="true">

		<?php WCVendors_Pro_Store_Form::purchess_form_tabs( ); ?>

		<!-- Store Settings Form -->
		<div class="tabs-content" id="myorder">
           <?php
$my_orders_columns = apply_filters( 'woocommerce_my_account_my_orders_columns', array(
	'order-number'  => __( 'Order', 'woocommerce' ),
	'order-date'    => __( 'Date', 'woocommerce' ),
	'order-status'  => __( 'Status', 'woocommerce' ),
	'order-total'   => __( 'Total', 'woocommerce' ),
	'order-actions' => '&nbsp;',
) );

$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
	'numberposts' => $order_count,
	'meta_key'    => '_customer_user',
	'meta_value'  => get_current_user_id(),
	'post_type'   => wc_get_order_types( 'view-orders' ),
	'post_status' => array_keys( wc_get_order_statuses() )
) ) );
remove_filter( 'woocommerce_my_account_my_orders_actions', 'feedback_link_action' );
if ( $customer_orders ) : ?>

	<h2><?php echo apply_filters( 'woocommerce_my_account_my_orders_title', __( '', 'woocommerce' ) ); ?></h2>

	<table class="shop_table shop_table_responsive my_account_orders">

		<thead>
			<tr>
				<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
					<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php foreach ( $customer_orders as $customer_order ) :
				$order      = wc_get_order( $customer_order );
				$item_count = $order->get_item_count();
				
				 $date1 = date('Y-m-d');
				
			    $date2 = date( 'Y-m-d', strtotime( $order->order_date ) );
				
				 
                $namestatus = 'Refund';
                 $diff = abs(strtotime($date2) - strtotime($date1));
				 $years = floor($diff / (365*60*60*24));
                 $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
				//get_post_meta( $order_number, '_wcv_refund_cus', 'refund'); 
				$statsus = get_post_meta($order->get_order_number(), '_wcv_refund_cus', true );
				if(!empty($statsus)){
					
				$namestatus = 'Refunded';
				}
				if($days > 29)
				{
				 $statsus = 'refunded';	
				}
				$orderall = new WC_Order( $customer_order);
                $itemorders = $orderall->get_items();
				foreach ( $itemorders as $itemorder ) {
                     // $product_name = $item['name'];
                        $product_id_fromorder = $itemorder['product_id'];
                   //  $product_variation_id = $item['variation_id'];
                 }
				?>
				<tr class="order">
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
									<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo date( 'Y-m-d', strtotime( $order->order_date ) ); ?>" title="<?php echo esc_attr( strtotime( $order->order_date ) ); ?>"><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo wc_get_order_status_name( $order->get_status() ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php echo sprintf( _n( '%s for %s item', '%s for %s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ); ?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
									$actions = array(
										'pay'    => array(
											'url'  => $order->get_checkout_payment_url(),
											'name' => __( 'Pay', 'woocommerce' )
										),
										'refund'   => array(
											'url'  => '',
											'name' => $namestatus
										),
										'cancel' => array(
											'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
											'name' => __( 'Cancel', 'woocommerce' )
										)
									);

									if ( ! $order->needs_payment() ) {
										unset( $actions['pay'] );
									}

									if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
										unset( $actions['cancel'] );
									}
									unset( $actions['leave_feedback'] );

									if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
										foreach ( $actions as $key => $action ) {
											echo '<a href="' . esc_url( $action['url'] ) . '" class="button ' . sanitize_html_class( $key ).$order->get_order_number() . ' refund" id="'. $statsus .'">' . esc_html( $action['name'] ) . '</a>';
										   
										}
										// echo apply_filters( 'dyn_review_button', $output,  $order->get_order_number(), "productreview" );
										echo apply_filters( 'dyn_review_button', $output,  $product_id_fromorder, "productreview" );
									}
								?>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
				
<script>
$('.refund<?php echo $order->get_order_number() ?>').click(function(e){
	   e.preventDefault();
		var $ = jQuery.noConflict();
		
		$('#refund-form<?php echo $order->get_order_number() ?>').modal();
			
	});

	    $(document).ready(function(){
			var $ = jQuery.noConflict();
 $(".dyn_checkbox1").click(function(e){
	 		
             $(".input-form-pas").hide();
					  
  });
 $(".dyn_checkbox2").click(function(e){
	 
             $(".input-form-pas").show();
			
                      
   });
   
   
   
   $('#refund-submit<?php echo $order->get_order_number() ?>').click(function(e){
						e.preventDefault();
                          
						  var sdd = '<?php echo $statsus; ?>';
                       // alert(sdd);
						var typeamount = $('input[name=dyn_refund<?php echo $order->get_order_number() ?>]:checked').val();
						var typeamounts = $('input[name=full_amount<?php echo $order->get_order_number() ?>]').val();
                      
						var parsitial = jQuery('input[name="par_amount<?php echo $order->get_order_number() ?>"]').val();
						var order_number =  $('input[name=order_numer<?php echo $order->get_order_number() ?>]').val();
						
						var data = { 
					       action: 'refund_system_ajax_call_call',
						   typeamount : typeamount,
						   typeamounts : typeamounts,
						   parsitial : parsitial,
						   order_number : order_number
				        }; 

			        	$.post(wcv_frontend_product_variation.ajax_url, data, function( response ) {
						// alert(response);
						 
						  // $(".refund<?php echo $order->get_order_number() ?>").css('pointer-events: none');
						  // $(".modal").hide();
						   // pointer-events: none  Successfully Submit Your Refund Request
						   if(response == 1)
						   {
							   location.reload();
						   }else{
							   
							   alert('Refund Amount should be less then order Amount');
						   }
						   
				          });
				
						
						
					});
					
      });
             
</script>
<!-- Modal -->
<div class="modal fade" id="refund-form<?php echo $order->get_order_number() ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Refund System</h4>
			</div>
			<div class="modal-body">
			    <div id="alert"></div>
				<form method="post" id="refund-form">
					<input type="hidden" name="action" value="purchse_refund">
					  <div class="form-group">
                        <label for="dyn-tags"><br/>How Much:</label>
                            <div class="checkbox">
                               <label>Full
                               <input type="radio" name="dyn_refund<?php echo $order->get_order_number() ?>" class="dyn_checkbox1" value="full_refund" checked="checked" >
							   </label>
                               <label>Partial
                              <input type="radio" name="dyn_refund<?php echo $order->get_order_number() ?>" class="dyn_checkbox2 " value="partial_refund" >
							  </label>
                           </div>
						   <div class="input-form-pas form-group" style="display:none">
						    
						     <input type="text" name="par_amount<?php echo $order->get_order_number() ?>" class="form-control" placeholder="Partial Amount">
							 <input type="hidden" name="full_amount<?php echo $order->get_order_number() ?>" class="form-control" value="<?php echo $order->get_total(); ?>">
						     <input type="hidden" name="order_numer<?php echo $order->get_order_number() ?>" class="form-control" value="<?php echo $order->get_order_number() ?>">
					
						   </div>
						   
               </div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="refund-submit<?php echo $order->get_order_number() ?>" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div>	 



			<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>

		</div>

		<div class="tabs-content" id="mydownload">
			<!-- Paypal address -->
			 <table class="shop_table shop_table_responsive my_account_orders" style=" background: #fff;">

		<thead>
			<tr class="headerdownload">
				 
					<th class=""><span >Date</span></th>
					<th class=""><span >Downloads remaining</span></th>
					<th class=""><span >Product Name</span></th>
			 
			</tr>
		</thead>

		<tbody>
	<?php 
	    global $woocommerce;
	if ( $downloadsalls = $woocommerce->customer->get_downloadable_products() ) : ?>


	
			<?php foreach ( $downloadsalls as $downloadsall ) : ?>
			  	<tr class="wc-my-downloads-digital-downloads">
				<?php
				// Get order date
				$orderdownload              = new WC_Order( $downloadsall['order_id'] );
				$itemstotal              = $orderdownload->get_items();
				$downloadLimit      = get_post_meta( $downloadsall['product_id'], '_download_limit', true );
				$downloadsRemaining = ( $downloadLimit - $downloadsall['downloads_remaining'] );
				$downloadOrder      = (array) $orderdownload;
				if(empty($downloadsRemaining))
				{
					$downloadsRemaining = "No Expiry";
				}
				?>
				<td><?php echo date( 'm/d/Y', strtotime( $downloadOrder['order_date'] ) ); ?></td>
				<td><?php echo apply_filters( 'woocommerce_available_available_download_count', '<span class="wc-my-downloads-available-count">' . $downloadsRemaining . '</span>', $downloadsall ); ?></td>
				<td><?php echo apply_filters( 'woocommerce_available_download_link', '<a href="' . esc_url( $downloadsall['download_url'] ) . '" class="wc-my-downloads-download-link">' . $downloadsall['download_name'] . '</a>', $downloadsall ); ?></td>
				
					<?php
					

					do_action( 'woocommerce_available_download_start', $downloadsall );

					
					do_action( 'woocommerce_available_download_end', $downloadsall );
					?>
				</tr>
			<?php endforeach; ?>
		 

	      <?php else: ?>
		     <tr class="wc-my-downloads-digital-downloads" > <td style="background: #fff;"><?php echo "No Found"; ?></td></tr>
	       <?php endif; ?>
          </tbody>
		  
		  </table>
		</div>

		
	<div class="tabs-content" id="branding">
		<?php WCVendors_Pro_Store_Form::store_banner( ); ?>	

		<!-- Store Icon -->
		<?php WCVendors_Pro_Store_Form::store_icon( ); ?>	
	</div>

	 

	 

	</div>