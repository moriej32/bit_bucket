<?php
/**
 * The template for displaying the tracking number form this is displayed in the modal pop up.
 *
 * Override this template by copying it to yourtheme/wc-vendors/dashboard/order
 *
 * @package    WCVendors_Pro
 * @version    1.0.3
 */
?>

<?php 
	//  Change text to make UI a little cleaner 
	$button_text = ''; 

	if ( isset( $tracking_details['_wcv_tracking_number' ] ) && '' != $tracking_details['_wcv_tracking_number' ] )  { 
		$button_text =  __( 'Update Tracking Details', 'wcvendors-pro' ); 
	} else { 
		$button_text =  __( 'Add Tracking Details', 'wcvendors-pro' ); 
	}

?>
<style>
.wcv-modal{ max-width: 600px !important; margin: 0 auto; border-radius: 5px; height: auto !important; }
.wcv-grid input{ color: #666;width: 100%; border-radius: 5px !important; border: 1px solid !important; padding: 7px !important; }
.wcv-modal input[type=submit] { -webkit-appearance: button;cursor: pointer; color: #fff;background-color: #3498db;border-color: #4cae4c; } 
.wcv-modal .control-group{ padding: 7px; }
.wcv-modal h3{ color: #3498db; }
.modal-body{ padding: 30px; }

.overlaysearch {
    height: 100%;
   
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.2);
    overflow-x: hidden;
    transition: 0.5s;
}

.overlaysearch-content {
    position: relative;
    top: 40%;
   
    text-align: center;
    margin-top: 30px;
    border-radius: 5px;
    padding: 30px;
    background-color: #fff;
    margin: 0 auto;
    width: 50%;
	color: #337ab7;
    opacity: 1;

}
.overlaysearch-content h2{color: #337ab7;}	
</style>
<div class="wcv-shade wcv-fade">
	<div id="tracking-modal-<?php echo $order_id; ?>" class="wcv-modal wcv-fade" data-trigger="#open-tracking-modal-<?php echo $order_id; ?>" data-width="80%" data-height="80%" aria-labelledby="modalTitle-<?php echo $order_id; ?>" aria-hidden="true" role="dialog">

		<div class="modal-header">
	            <button class="modal-close wcv-dismiss"></button>
	            <h3 id="modal-title"><?php _e( 'Shipment Tracking', 'wcvendors-pro'); ?></h3>
	    </div>

	   <div class="modal-body" id="tracking-modal-<?php echo $order_id; ?>-content">
	              <div id="overlayssss<?php echo $order_id; ?>" style="display:none">
		              <div class="overlaysearch-content">
                       <h2><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></h2>
                      </div>
					</div>
			<form method="post" action="" id="theForm<?php echo $order_id?>">

				<?php WCVendors_Pro_Tracking_Number_Form::shipping_provider( $tracking_details['_wcv_shipping_provider' ], $order_id ); ?>

				<?php WCVendors_Pro_Tracking_Number_Form::tracking_number( $tracking_details['_wcv_tracking_number' ], $order_id ); ?>

				<?php WCVendors_Pro_Tracking_Number_Form::date_shipped( $tracking_details['_wcv_date_shipped' ], $order_id ); ?>

				  <?php WCVendors_Pro_Tracking_Number_Form::form_data_ajax( $order_id, $button_text ); ?>

			</form>

		<div id="messagessss"></div>
		  
    <!-- <script>

	var $ = jQuery.noConflict();
	$('#save_button<?php echo $order_id?>').click(function(e){
		e.preventDefault();
				
			 var Provider = $('#s2id__wcv_shipping_provider_<?php echo $order_id?> span.select2-chosen').text();
			 var order_id = '<?php echo $order_id?>';
			 var trackid = $('_wcv_tracking_number_<?php echo $order_id?>').val();
			// var form_data = new FormData(this); 
			//alert( $('#theForm<?php echo $order_id?>').serialize());
             //form_data.append('action', 'process_submit_order');			 
				var datas = { 
					action: 'process_submit_order',
					data: $('#theForm<?php echo $order_id?>').serialize(),
				}; 

				$.post(wcv_frontend_product_variation.ajax_url, datas, function( response ) {
						// $('#messagessss').append(response);
						 //$('#messagessss').attr('style','display:block');
						alert(response);
						
				});
				
			});
	</script>	-->
	
	<script>

	var $ = jQuery.noConflict();
	$('#save_button<?php echo $order_id?>').click(function(e){
		e.preventDefault();
			$("#overlayssss<?php echo $order_id; ?>").fadeIn();	
			 var provider = $('#s2id__wcv_shipping_provider_<?php echo $order_id?> span.select2-chosen').text();
			 var order_id = '<?php echo $order_id?>';
			 var trackid = $('_wcv_tracking_number_<?php echo $order_id?>').val();
			// var form_data = new FormData(this); 
			//alert( $('#theForm<?php echo $order_id?>').serialize());
             //form_data.append('action', 'process_submit_order');			 
				var datas = { 
					action: 'process_submit_order',
					data: $('#theForm<?php echo $order_id?>').serialize(),
					 //orderid: order_id,
					// carrier_code: provider,
					 //trackingnumber: trackid,
					dataType: "json",
				}; 

				$.post(wcv_frontend_product_variation.ajax_url, datas, function( response ) {
						// $('#messagessss').append(response);
						 //$('#messagessss').attr('style','display:block');
						alert(response.resultone);
						$('#messagessss').html(response.resultone);
						var successdb = response.successdb;
						if(successdb == 1)
						{
						  var succdata = { 
					          action: 'process_marked_shipped',
					          //orderid: order_id,
							  //carrier_code: provider,
							  //trackingnumber: trackid,
							  data: $('#theForm<?php echo $order_id?>').serialize(),
							  dataType: "json",
				           };
						   $.post(wcv_frontend_product_variation.ajax_url, succdata, function( result ) {
							   //alert(result.success);
							   $('.modal-close').click();
						   });	
						}
						
					$("#overlayssss<?php echo $order_id; ?>").fadeOut();	
				});
				
			});
	</script>
			
		</div>
	</div>


<style>
#messagessss{
color: #3c763d;
background-color: #dff0d8;
border-color: #d6e9c6;
padding: 9px;
margin-bottom: 20px;
border: 1px solid transparent;
border-radius: 4px;
margin-top: 11px;
width: 30%;
display:none;
}
</style>	
	
</div>
