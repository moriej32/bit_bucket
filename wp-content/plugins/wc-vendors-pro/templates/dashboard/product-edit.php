

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>


<?php
/**
 * The template for displaying the Product edit form
 *
 * Override this template by copying it to yourtheme/wc-vendors/dashboard/
 *
 * @package    WCVendors_Pro
 * @version    1.3.2
 */
/**
 *   DO NOT EDIT ANY OF THE LINES BELOW UNLESS YOU KNOW WHAT YOU'RE DOING
 *
*/

$title = 	( is_numeric( $object_id ) ) ? __('Save Changes', 'wcvendors-pro') : __('Add Product', 'wcvendors-pro');
$product = 	( is_numeric( $object_id ) ) ? wc_get_product( $object_id ) : null;

// Get basic information for the product
$product_title     			= ( isset($product) && null !== $product ) ? $product->post->post_title    : '';
$product_description        = ( isset($product) && null !== $product ) ? $product->post->post_content  : '';
$product_short_description  = ( isset($product) && null !== $product ) ? $product->post->post_excerpt  : '';
$post_status				= ( isset($product) && null !== $product ) ? $product->post->post_status   : '';

/**
 *  Ok, You can edit the template below but be careful!
*/
?>



<h2><?php echo $title; ?></h2>
<?php 
	$current_user = wp_get_current_user();
	global $wpdb;	
	$check_marchant_paypal = $wpdb->get_row("SELECT paypal_email FROM ".$wpdb->prefix . "users_merchant_accounts WHERE user_id=".$current_user->ID);
	$check_marchant_revenue = $wpdb->get_row("SELECT revenue_status FROM ".$wpdb->prefix . "users_revenue_status WHERE user_id=".$current_user->ID);
?>
<!-- Product Edit Form -->
<form method="post" action="" id="wcv-product-edit" enctype="multipart/form-data" class="wcv-form wcv-formvalidator">

	<!-- Basic Product Details -->
	<div class="wcv-product-basic wcv-product">
		<!-- Product Title -->
		<?php WCVendors_Pro_Product_Form::title( $object_id, $product_title ); ?>
		<!-- Product Description -->
		<?php WCVendors_Pro_Product_Form::description( $object_id, $product_description );  ?>
		<!-- Product Short Description -->
		<?php WCVendors_Pro_Product_Form::short_description( $object_id, $product_short_description );  ?>
		<!-- Product Categories -->
	    <?php //WCVendors_Pro_Product_Form::categories( $object_id, true ); ?>
	    <!-- Product Tags -->
	    <?php WCVendors_Pro_Product_Form::tags( $object_id, true ); ?>
	</div>

	<div class="all-100">
    	<!-- Media uploader -->
		<div class="wcv-product-media">
			<?php WCVendors_Pro_Form_helper::product_media_uploader( $object_id ); ?>
			<p class="tip">Max Video size: 35 MB</p>
		</div>
		 <input type="hidden" name="productvideofile" class="" id="productvideofile">
		 <input type="hidden" name="productvideoimage" class="" id="productvideoimage">
		<div id="progress-wrp" style="display:none"><div class="progress-bar"></div ></div>
		 
	</div>

	<br />

	<div class="all-100">
		<!-- Product Type -->
		
		 <div class="control-group">
			<select id="product-type" name="product-type" class="control select2" style="width: 100%;" tabindex="-1" title="">
		  
		    <option value="simple">Physical product</option>
			<option value="downloadable">Downloadable Product</option>
			 
		 
		</select>
		<div class="show_if_simple show_if_external">
		 <p class="tip">Example: Toys, Appliances, etc.</p>
		</div>
		<div class="show_if_downloadable" id="">
		   <p class="tip">Example: Digital music, Digital pdf,Digital software, etc.</p>
		</div>
		</div>
		
		
	</div>
	<br />

	<div class="all-100">
		<div class="wcv-tabs top" data-prevent-url-change="true">

			<?php WCVendors_Pro_Product_Form::product_meta_tabs( ); ?>

			<?php do_action( 'wcv_before_general_tab', $object_id ); ?>

			<!-- General Product Options -->
			<div class="wcv-product-general tabs-content" id="general">

				<div class="hide_if_grouped">
					<!-- SKU  -->
					<?php //WCVendors_Pro_Product_Form::sku( $object_id ); ?>
					 <div class="control-group">
					
					<label for="_stock" class="">Stock Qty</label><div class="control">
					<input type="number" class="" style="" name="stockss" id="stockss" value="" placeholder="" step="any"> </div>
					<p class="tip">Stock quantity.</p>
					
					</div>

					<!-- Private listing  -->
					<?php //WCVendors_Pro_Product_Form::private_listing( $object_id ); ?>
				</div>


				<div class="options_group show_if_external">
					<?php WCVendors_Pro_Product_Form::external_url( $object_id ); ?>
					<?php WCVendors_Pro_Product_Form::button_text( $object_id ); ?>
				</div>

				<div class="">
					<!-- Price and Sale Price -->
					<?php WCVendors_Pro_Product_Form::prices( $object_id ); ?>
				</div>


				<div class=" background-home">
					<div class="">
						<div class="add-background"><label>Add Background:</label></div>
						<?php WCVendors_Pro_Product_Form::background( $object_id ); ?>
					</div>
				</div>
				<div id="upload_container">
					<div class="form-group">
						<?php WCVendors_Pro_Product_Form::image_upload( $object_id ); ?>
					</div>
				</div>


				<script type="text/javascript">
					jQuery(document).ready(function() {
						jQuery("#upload_container").css({'display':'none'});
						jQuery(".dyn_checkbox").click( function() {
							var test = $(this).val();
							if( test == 'background_new' ){
								jQuery("#upload_container").css({'display':'block'});
							}else{
								jQuery("#upload_container").css({'display':'none'});
							}
						});
						
							jQuery( '.shipping_product_data' ).on( 'change', '.wc-enhanced-select', function() {

			                var testss = $(this).val();
			                if( testss == 'specific')
							{
							 jQuery("#specific-countries").css({'display':'block'});
							 
							}else{
								
								jQuery("#specific-countries").css({'display':'none'});
							 
							}
							
		                     }); 
							 
							 
							  $('form#wcv-product-edit').submit(function(e) {
								 var typeofproductsd = $( 'select#product-type' ).val();
								 
								 var rateshipp = $("input[name='_wcv_shipping_fees[]']").map(function(){return $(this).val();}).get();
								 
								  //alert(rateshipp);
								  
                                if (typeofproductsd == 'simple') {
									if(rateshipp == '')
									{
										alert('Shipping rate missing');
										return false; // or e.preventDefault();
									} else{
										return true; // or e.preventDefault();
									}
                                }
                              });
					});
				</script>
				<?php if($check_marchant_paypal && $check_marchant_revenue->revenue_status==1){?>
				<div class="revenue-home">
					<div class="">
						<div class="add-background"><label>Add Revenue Share:</label></div>
						<div class="checkbox">
							<label>Yes
							<input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox" value="yes"></label>
							<label>No
							<input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox" value="no" checked="checked"></label>
						</div>
						<div class="revnue" style="display:none">
						  <label for="revenue-paypal-email">Add Paypal Email</label>
						  <input type="email" name="revenue-paypal-email" class="revenue-paypal-email" id="revenue-paypal-email">
						  <input type="button" value="Add" class="revenue-paypal">
						  <div id="error-msg"></div>
						  <div id="loading" style="display:none"><img id="loadingImage" src="<?php echo get_template_directory_uri();?>/images/ajax-loader.gif"></div>
						  <table id="author-lists" class="author-lists"><tr><th></th><th><b>Users</b></th><th><b> % of Revenue</b></th></tr><tr><td></td><td><p><?php echo $current_user->display_name;?></p></td><td><input id="chnl-perc-val" type="text" value="100" readonly />%</td></tr></table>
						</div>
						<input type="hidden" name="counterval" id="counterval">
					</div>
				</div>
				<script type="text/javascript">
                jQuery(document).ready(function() {
					jQuery("#error-msg").html('');
                  	jQuery("#revenue-table").html('');                 	
					var counter = 1;
					jQuery('.dyn_ad_revenue_checkbox').click(function(){
						var revenueval = $(this).val();
						if(revenueval == 'yes'){
							jQuery('.revnue').css('display','block');
							jQuery('.revenue-paypal').click(function(){
								email = $('.revenue-paypal-email').val();
								var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
								var result = pattern .test(email);
								if(result){
								jQuery('.revenue-paypal-email').val('');
								$("#loading").css('display','block');
								$("#author-lists").css('display','none');
								setTimeout(function(){											
								jQuery.post("<?php echo get_template_directory_uri();?>/get-author.php",{email:email,counter:counter},function(data){	
									if(data['author']){
									var newRow = jQuery('<tr id="row'+data['counter']+'"><td><a class="remove'+data['counter']+'">Remove</a></td><td><p id="authorbtn'+data['counter']+'">Author Name</p><input type="hidden" name="revusers'+data['counter']+'" class="authorid" value="'+data['authorID']+'"/></td><td><input type="number" class="num" id="numbersOnly'+data['counter']+ '" name="percentage-revenue'+data['counter']+ '" max="" min=""/>%<input type="hidden" id="numval'+data['counter']+'" name="perceval'+data['counter']+'"/></td></tr>');
									jQuery('table.author-lists').append(newRow);
									jQuery("#error-msg").html('');
									jQuery("#counterval").val(data['counter']);
									}else{
										jQuery("#error-msg").html("No user Found. Please try again.");
									}
									jQuery("#authorbtn"+data['counter']).html(data['author']);
									jQuery("#numbersOnly"+data['counter']).attr('min',1);
									jQuery("#numbersOnly"+data['counter']).attr('max',80);
									jQuery(".remove"+data['counter']).click(function(){
										$("#row"+data['counter']).remove();
										checksum();
									});
									jQuery("#author-lists").on('change', '.num', function(){
										jQuery('#numval'+data['counter']).val(this.value);
										checksum();
									});
									function checksum(){
										var sum = 0;
										jQuery('.num').each(function(){
											sum += Number(jQuery(this).val());	
										});										
										jQuery('#chnl-perc-val').val(100 - sum);
									}
								},'json');
								$("#loading").css('display','none');
								$("#author-lists").css('display','block');
								},5000);								
								counter++;									
								}else{
									jQuery("#error-msg").html("Please enter valid Email address.");
								}
							});
						}else{
							jQuery('.revnue').css('display','none');
							jQuery("#error-msg").html('');
						}
					});					
                });
               </script>
			   <?php } ?>
			   <!--<div class=" sales-revenue-home">
					<div class="">
						<div class="add-background"><label>Sales Revenue Share:</label></div>
						<div class="checkbox">
							<label>Yes
							<input type="radio" name="dyn_sales_revenue" class="dyn_sales_revenue_checkbox" value="yes"></label>
							<label>No
							<input type="radio" name="dyn_sales_revenue" class="dyn_sales_revenue_checkbox" value="no" checked="checked"></label>
						</div>
						<p id="user-found-err-msg" style="display:none">No user Found. Please try again.</p>
						<p id="user-allow-err-msg" style="display:none">User is not allow for revenue share.</p>
						<p id="email-err-msg" style="display:none">Please enter valid Email address.</p>
						<div class="sales-revnue" style="display:none">
						  <label for="sales-revenue-paypal-email">Add Paypal Email</label>
						  <input type="email" name="sales-revenue-paypal-email" class="sales-revenue-paypal-email" id="sales-revenue-paypal-email">
						  <input type="button" value="Add" class="sales-revenue-paypal">	 
						  <div id="sales-loading" style="display:none"><img id="sales-loadingImage" src="<?php echo get_template_directory_uri();?>/images/ajax-loader.gif"></div>
						  <table id="sales-author-lists" class="sales-author-lists"><tr><th></th><th><b>Users</b></th><th><b> % of Revenue</b></th></tr><tr><td></td><td><p><?php echo $current_user->display_name;?></p></td><td><input id="sales-chnl-perc-val" type="text" value="100" readonly />%</td></tr></table>
						</div>						
					</div>
				</div>-->
				<script type="text/javascript">
                jQuery(document).ready(function() {
					//jQuery("#sales-error-msg").html('');              	
					var salesCounter = 1;
					jQuery('.dyn_sales_revenue_checkbox').click(function(){
						var salesrevenueval = $(this).val();
						if(salesrevenueval == 'yes'){
							jQuery('.sales-revnue').css('display','block');
							jQuery('.sales-revenue-paypal').click(function(){					
								jQuery("#user-found-err-msg").css('display','none');
								jQuery("#user-allow-err-msg").css('display','none');
								jQuery("#email-err-msg").css('display','none');
								salesEmail = $('.sales-revenue-paypal-email').val();
								var salesPattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
								var Sresult = salesPattern .test(salesEmail);
								if(Sresult){
								jQuery('.sales-revenue-paypal-email').val('');
								$("#sales-loading").css('display','block');
								$("#sales-author-lists").css('display','none');
								setTimeout(function(){											
								jQuery.post("<?php echo get_template_directory_uri();?>/get-author-sales.php",{salesEmail:salesEmail,salesCounter:salesCounter},function(data){	
									if(data['salesAuthor']!='none'){
										if(data['salesMarchant_paypal']!='none' && data['salesMarchant_revenue']!='none'){
											var salesNewRow = jQuery('<tr id="sales-row'+data['salesCounter']+'"><td><a class="sales-remove'+data['salesCounter']+'">Remove</a></td><td><p id="sales-authorbtn'+data['salesCounter']+'">Author Name</p></td><td><input type="number" class="sales-num" id="sales-numbersOnly'+data['salesCounter']+ '" name="sales-percentage-revenue'+data['salesCounter']+ '" max="" min=""/>%</td></tr>');
											jQuery('table.sales-author-lists').append(salesNewRow);
										}else{
											jQuery("#user-found-err-msg").css('display','none');
											jQuery("#user-allow-err-msg").css('display','block');
											jQuery("#email-err-msg").css('display','none');
										}
									}else{
										jQuery("#user-found-err-msg").css('display','block');
										jQuery("#user-allow-err-msg").css('display','none');
										jQuery("#email-err-msg").css('display','none');
									}
									jQuery("#sales-authorbtn"+data['salesCounter']).html(data['salesAuthor']);
									jQuery("#sales-numbersOnly"+data['salesCounter']).attr('min',1);
									jQuery("#sales-numbersOnly"+data['salesCounter']).attr('max',80);
									jQuery(".sales-remove"+data['salesCounter']).click(function(){
										$("#sales-row"+data['salesCounter']).remove();
										saleschecksum();
									});
									jQuery("#sales-author-lists").on('change', '.sales-num', function(){
										saleschecksum();
									});
									function saleschecksum(){
										var sum = 0;
										jQuery('.sales-num').each(function(){
											sum += Number(jQuery(this).val());	
										});										
										jQuery('#sales-chnl-perc-val').val(100 - sum);
									}
								},'json');
								$("#sales-loading").css('display','none');
								$("#sales-author-lists").css('display','block');
								},5000);								
								salesCounter++;									
								}else{
									jQuery("#user-found-err-msg").css('display','none');
									jQuery("#user-allow-err-msg").css('display','none');
									jQuery("#email-err-msg").css('display','block');
								}
							});
						}else{
							jQuery('.sales-revnue').css('display','none');
							jQuery("#user-found-err-msg").css('display','none');
							jQuery("#user-allow-err-msg").css('display','none');
							jQuery("#email-err-msg").css('display','none');
						}
					});					
                });
               </script>
				<div class=" show_if_external status-home">
					<div class="add-background"><label>Privacy Options:</label></div>
					<?php WCVendors_Pro_Product_Form::status( $object_id ); ?>
				</div>

				<div style="display:none;" class="select-box">
					<?php
					echo '<label for="dyn-tags">Please select the User you want to give access</label><br><select id="tokenize" class="tokenize-sample product-tokenize" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
					$args1 = array(
							'role' => 'free_user',
							'orderby' => 'id',
							'order' => 'desc'
					);
					$subscribers = get_users($args1);
					foreach ($subscribers as $user) {
						if(get_current_user_id() == $user->id)
						{ }
						else
						{
							echo '<option value="'.$user->id.'">' . $user->user_email.'</option>';
						}
					}
					echo '</select>';
					?>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$("#privacy-radio").click(function(){
							$(".select-box").slideDown();
						});
						$("#privacy-radio1").click(function(){
							$(".select-box").slideUp();
							//$('select#select_users_list option').removeAttr("selected");
						});
					});
					$('#tokenize').tokenize();
					datas: "bower.json.php"
				</script>




				<div class="show_if_simple show_if_external show_if_variable">
					<!-- Tax -->
					<?php  //WCVendors_Pro_Product_Form::tax( $object_id ); ?>
				</div>

				<div class="show_if_downloadable" id="files_download">
					<!-- Downloadable files -->
					<?php WCVendors_Pro_Product_Form::download_files( $object_id ); ?>
					<!-- Download Limit -->
					<?php WCVendors_Pro_Product_Form::download_limit( $object_id ); ?>
					<!-- Download Expiry -->
					<?php WCVendors_Pro_Product_Form::download_expiry( $object_id ); ?>
					<!-- Download Type -->
					<?php WCVendors_Pro_Product_Form::download_type( $object_id ); ?>
					
				</div>
				
			</div>

			<?php do_action( 'wcv_after_general_tab', $object_id ); ?>

			<?php do_action( 'wcv_before_inventory_tab', $object_id ); ?>

			<!-- Inventory -->
			<div class="wcv-product-inventory inventory_product_data tabs-content" id="inventory">

				<?php WCVendors_Pro_Product_Form::manage_stock( $object_id ); ?>

				<?php do_action( 'wcv_product_options_stock' ); ?>

				<div class="stock_fields show_if_simple show_if_variable">
					<?php WCVendors_Pro_Product_Form::stock_qty( $object_id ); ?>
					<?php WCVendors_Pro_Product_Form::backorders( $object_id ); ?>
				</div>

				<?php WCVendors_Pro_Product_Form::stock_status( $object_id ); ?>
				<div class="options_group show_if_simple show_if_variable">
					<?php WCVendors_Pro_Product_Form::sold_individually( $object_id ); ?>
				</div>

				<?php do_action( 'wcv_product_options_sold_individually' ); ?>

				<?php do_action( 'wcv_product_options_inventory_product_data' ); ?>

			</div>

			<?php do_action( 'wcv_after_inventory_tab', $object_id ); ?>

			<?php do_action( 'wcv_before_shipping_tab', $object_id ); ?>

			<!-- Shipping  -->
			<div class="wcv-product-shipping shipping_product_data tabs-content" id="shipping">

				<div class="hide_if_grouped hide_if_external">

					<!-- Shipping rates  -->
					<?php WCVendors_Pro_Product_Form::shipping_rates( $object_id ); ?>
					<!-- weight  -->
					<div class="countrylist" style="display:none">
					  <style>
					       .header td{font-weight: bolder;
                           font-size: 15px;
						   }
						 #showallcountry, #hidecountry{
                          color: #007ED5;
                          font-weight: bold;
                          cursor: pointer;
						 }
						   
					  </style>
					  <div class="wcv-cols-groups">
					  <table>
					  <tr class="header">
					  <td>ISO Code</td> <td>Country Name</td>
					  <td>ISO Code</td> <td>Country Name</td>
					  <td>ISO Code</td> <td>Country Name</td>
					  <td>ISO Code</td> <td>Country Name</td>
					  </tr>
					<?php 
					
					$countryall_lists =  WCVendors_Pro_Form_Helper::countriesnew();  
					
					$counter = 1;
					
					foreach ($countryall_lists  as $isocodesss => $countryall_list ) {
						 if ($counter % 4 == 1)
							{  
								//echo '<div class="all-25"><ul>';
								echo '<tr>';
							}
							
							
						?>	
							
						<td>  <?php 	echo $isocodesss; ?></td><td>  <?php 	echo $countryall_list; ?> </td>
						<?php	
							if ($counter % 4 == 0)
							{
								echo '</tr>';
						//	echo "</ul></div>";
						
								}
								
							$counter++;
							
						
					}
					?>
					</table>
					</div>
					</div>
					<?php WCVendors_Pro_Product_Form::weight( $object_id ); ?>
					<!-- Dimensions -->
					<?php WCVendors_Pro_Product_Form::dimensions( $object_id ); ?>
					<?php do_action( 'wcv_product_options_dimensions' ); ?>
					<!-- shipping class -->
					<?php //WCVendors_Pro_Product_Form::shipping_class( $object_id ); ?>
					<?php do_action( 'wcv_product_options_shipping' ); ?>
					 <?php WCVendors_Pro_Product_Form::countries_shippinf_op( $object_id ); ?>
					 
				  <div id="specific-countries" style="display:none">
				    <div class="select-box">
					<?php
					echo '<label for="dyn-tags">Specific Countries</label><br><select id="spacicecountry" class="tokenize-sample product-tokenize" name="spacicecountry[]" style=" width: 100%;"  multiple="multiple">';
					$countries 				= WCVendors_Pro_Form_Helper::countries();  
					 
					foreach ($countries as $valuess => $countrie) {
						 
							echo '<option value="'. $valuess .'">' . $countrie .'</option>';
						
					}
					echo '</select>';
					?>
					
				</div>
				<script type="text/javascript">
					 
					$('#spacicecountry').tokenize();
					//datas: "bower.json.php"
				</script>
				<style>
				.tokenize-sample {width:100% !important;}
				 ul.TokensContainer{height: auto !important;}
				 .btn-success {color: #ffffff !important; background-color: #337ab7 !important;}
				 label {
                   display: inline-block;
                   max-width: 100%;
                   margin-bottom: 5px;
                   font-weight: 600;
                 }
				 .wcv-media-uploader-gallery a{font-size: 12px !important;}
				 .wcv-featuredimg img{
					 max-height: 100% !important;
					 border: 1px solid aliceblue;
					 height: 100%;
				 }
                .entry h2, .comment-text h2 {
                   font-size: 17px;
                  margin-bottom: 10px;
                  }
				 table{width: 100%;}
	             .labelclass{
                            color: #337ab7;
							font-weight: bold;
							font-size: 12px;
							cursor: pointer;  
					}

					/* progress bar */
				 #progress-wrp {
	
					height: 20px;
					position: relative;
					border-radius: 3px;
					margin: 10px;
					text-align: left;
					background: #eee;
					box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
					}
				#progress-wrp .progress-bar{
						height: 20px;
						border-radius: 3px;
						background-color: #0099CC;
						width: 0;
						box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
						margin-bottom: 10px;
				}
				#progress-wrp .status{
					top:3px;
					left:50%;
					position:absolute;
					display:inline-block;
					color: #000000;
				}
				
				.wcv-form p.tip {
				color: inherit !important;
				position: relative;
				margin: 0 0 10px 0px !important;
     
				font-weight: bold;
				font-size: 12px !important;
				}
				.entry{background-color: rgb(248, 248, 248) !important;
                      padding: 15px 25px !important;}
					  
			    .select2-container-multi .select2-choices {
                    height: auto!important;
					height: 1%;
					margin: 0;
					padding: 4px 6px !important;
					position: relative;
					border: 1px solid #aaa;
					cursor: text;
					overflow: hidden;
					background-color: #fff;
					border-radius: 5px;
				}
				 .status-100 {
					width: auto !important;
					float: left;
					margin-right: 20px;
				}
                .file_url{ display:none !important; }
				.wcv-featuredimg{margin-bottom: 5px;}
				a.wcv-media-uploader-featured-add, a.wcv-media-uploader-featured-delete{font-size: 12px;}
				.ormessage{    padding: 0px 0 13px 0;
							text-align: center;
							width: 28%;
							font-weight: bold;
							}

							/* curtain */
.overlay {
    height: 100%;
   
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.2);
    overflow-x: hidden;
    transition: 0.5s;
}

.overlay-content {
    position: relative;
    top: 25%;
   
    text-align: center;
    margin-top: 30px;
    border-radius: 5px;
    padding: 30px;
    background-color: #fff;
    margin: 0 auto;
    width: 50%;
	color: #337ab7;
    opacity: 1;

}
.overlay-content h2{color: #337ab7;}	
				</style>
				  </div>
				  
				<div class="shippingpolicy">
				   <?php WCVendors_Pro_Store_Form::shipping_form_paddpage( ); ?>
				</div>
				</div>

			</div>

			<?php do_action( 'wcv_after_shipping_tab', $object_id ); ?>

			<?php do_action( 'wcv_before_linked_tab', $object_id ); ?>

			<!-- Upsells and grouping -->
	<!--		<div class="wcv-product-upsells tabs-content" id="linked_product">

				<?php /*WCVendors_Pro_Product_Form::up_sells( $object_id ); */?>

				<?php /*WCVendors_Pro_Product_Form::crosssells( $object_id ); */?>

				<div class="hide_if_grouped hide_if_external">

					<?php /*WCVendors_Pro_Product_Form::grouped_products( $object_id, $product ); */?>

				</div>
			</div>-->

			<?php do_action( 'wcv_after_linked_tab', $object_id ); ?>

			<!-- Attributes -->

			<?php do_action( 'wcv_before_attributes_tab', $object_id ); ?>

			<!--<div class="wcv_product_attributes tabs-content" id="attributes">

				<?php /*//WCVendors_Pro_Product_Form::product_attributes( $object_id ); */?>

			</div>-->

			<?php //do_action( 'wcv_after_linked_product_tab', $object_id ); ?>

			<!-- Variations -->

			<?php do_action( 'wcv_before_variations_tab', $object_id ); ?>

			<div class="wcv_product_variations tabs-content" id="variations">

				<?php WCVendors_Pro_Product_Form::product_variations( $object_id ); ?>

			</div>

			<?php do_action( 'wcv_after_variations_tab', $object_id ); ?>

			<?php WCVendors_Pro_Product_Form::form_data( $object_id, $post_status ); ?>
			<?php WCVendors_Pro_Product_Form::save_button(  ); ?>
			<?php //WCVendors_Pro_Product_Form::draft_button( __('Save Draft','wcvendors-pro') ); ?>

			</div>
		</div>
</form>
<div id="overlayssss" class="overlay" style="display:none;">
 
  <div class="overlay-content">
    <h2>Waiting........</h2>
  </div>
</div>	
<div id="videouploadone" style="display:none">

                   <form id="uploadforms" enctype="multipart/form-data">
					      <input id="files" type="file" required name="file" class="file_multi_video" accept="video/*">
					      <div id="featuredimage"> </div>
						  <!--<video width="400" controls>
                             <source src="mov_bbb.mp4" id="video_here">
                                Your browser does not support HTML5 video.
                          </video>-->
						  <div id="mytestsss"></div>
					       <br/>
						    <button type="submit" id="featuredimagesumbit" style="display:none;" class="btn btn-success"> Upload </button>
						</form>
						
				       
				</div>
	<!--<div class="modal fade" id="videoaddmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Added Video</h4>
					</div>
					<div class="modal-body">
					
					    <form id="uploadforms" enctype="multipart/form-data">
					      <input id="" type="file" required name="file" accept="video/*">
					      <div id="featuredimage"> </div>
					       <br/>
						    <div id="progress-wrp" style="display:none"><div class="progress-bar"></div ></div>
				         <button type="submit" id="featuredimagesumbit" style="display:none;"  class="btn btn-success"> Upload </button>
						</form>
					</div>
				</div>
			</div>
		</div>-->
		<script type="text/javascript">
		   $("body").prepend('<div id="overlay" class="ui-widget-overlay" style="z-index: 1001; display: none;"></div>');
           $("body").prepend("<div id='PleaseWait' style='display: none;'><img src='/images/spinner.gif'/></div>");
                 
				 // function selectstate(countryval)
		      // {
				  $(document).on("change", "#countryselcetedrate", function(evt) {
			    
				 var countryval = $(this).val();
				  console.log(countryval);
				 //var currentli = $('.nav').find('a.current').parent().prop('className');
				 var rootclass = $(this).closest("tr").attr("class");
						 $.ajax({
									url: '<?php echo admin_url('admin-ajax.php'); ?>',
									type: "POST",
                                    dataType: "json",
									data: {
										action:'state_by_coutry_callback',
									    'countryname':countryval,
									},
									success: function(response){
										
									 $('.' + rootclass  +' #stateofcountry' ).html(response.resoption); 
									 
									 }
								});
		       //  }
			  });
				 $('#shipping').on('click','.wcv_shipping_rates a.insert',function(){
					 setTimeout(function() { 
					 var count = $('.wcv_shipping_rates').find('tbody').children('tr').length;
		             // alert(count);
					 $('.wcv_shipping_rates tbody tr:last').addClass('shipping_rates'+count);
					  }, 1000);
				 });
		    
		   	     $(document).on("change", ".file_multi_video", function(evt) {
					 
  var max_file_size 			= 36700160;
  var oFile = document.getElementById('files').files[0];
   var filesize = oFile.size;
   
  if(filesize < max_file_size){ 
  // $('#featuredimagesumbit').attr('style','display:block;');
   $('#featuredimagesumbit').trigger('click');
 
  }else{
	   $('#featuredimagesumbit').attr('style','display:none;');
	alert('Size is not Accept, Max size: 35 MB'); 
  }
});
		    $('#videoaddmodelads').click(function(e){
				e.preventDefault();
                 //#morecountry  moreshipped
				var $ = jQuery.noConflict();
				$('.file_multi_video').trigger('click');
				//$('#videoaddmodel').modal();
			});
			
			 $(document).on("change", "select#product-type", function(evt) {
				 var product_type    = $('#product-type').val();
				 //alert(product_type);
				 if(product_type == 'downloadable')
				 {
					$('.shipping').hide(); 
				 }
			 });
			
			$('#showallcountry').click(function(e){
				 $('.countrylist').attr('style', 'display:block'); 
				 $('#showallcountry').attr('style', 'display:none'); 
				 $('#hidecountry').attr('style', 'display:inline-block'); 
				
			});
			
			 $('#hidecountry').click(function(e){
				 $('.countrylist').attr('style', 'display:none'); 
				 $('#showallcountry').attr('style', 'display:inline-block'); 
				 $('#hidecountry').attr('style', 'display:none'); 
			});
			
			$('#uploadforms').on( "submit", function(e) { 
				e.preventDefault();
                 //#morecountry  moreshipped
				var $ = jQuery.noConflict();
				var progress_bar_id 		= '#progress-wrp'; //ID of an element for response output
				//progress_bar_id.fadIn();
				
				$("#progress-wrp").attr('style','display:block');
				$("#overlayssss").attr('style','display:block;');
				
				var form_data = new FormData(this);
				
				form_data.append('action', 'product_video_method'); 
				
				$.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						//action: 'videofil_upload',
						type: "POST",
						data : form_data,
						contentType: false,
						dataType: 'json',
						cache: false,
						processData:false,
						xhr: function(){
								//upload Progress
								var xhr = $.ajaxSettings.xhr();
								if (xhr.upload) {
									xhr.upload.addEventListener('progress', function(event) {
									var percent = 0;
									var position = event.loaded || event.position;
									var total = event.total;
									if (event.lengthComputable) {
										percent = Math.ceil(position / total * 100);
									}
									//update progressbar
									$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
									$(progress_bar_id + " .status").text(percent +"%");
									}, true);
								 }
								return xhr;
						},
						success: function(data){
							$('#_featured_image_id').val(data.is_inserid);
							//productvideofile
							//$('#curtain').remove();
							//alert(video completed upload);
							$('#productvideofile').val(data.productvideofile);
							$('#productvideoimage').val(data.is_insertss);
							 //$( '.wcv-featuredimg' ).append( '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/uploads/r55KjX_Jellyfish.jpg">'); 
							  $( '.wcv-featuredimg' ).append( '<img src="'+ data.is_insertss +'">'); 
	
		   // $('#_featured_image_id').val(json.id); 
            $('#overlayssss').attr('style','display:none;');
		    $('.wcv-media-uploader-featured-add').addClass('hidden'); 
		    $('.wcv-media-uploader-featured-delete').removeClass('hidden');
			//$("#videoaddmodel").fadeOut();
			//$('#videoaddmodel').attr('style', 'display:none');
			$('button.close').click();
						},
						mimeType:"multipart/form-data"
					});
			});
			 
         </script>