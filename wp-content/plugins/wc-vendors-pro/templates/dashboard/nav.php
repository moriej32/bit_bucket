<?php
/**
 * The template for displaying the Pro Dashboard navigation 
 *
 * Override this template by copying it to yourtheme/wc-vendors/dashboard/
 *
 * @var 	   $page_url  		The permalink to the page 
 * @var 	   $page_label 		The page label for the menu item 
 * @package    WCVendors_Pro
 * @version    1.0.3
 */
?>
<li id="dashboard-menu-item-<?php echo $page['slug']; ?>" class="<?php echo $class; ?>">
    <a href="<?php if($page['slug']=='vendor_aff_list'){
       echo str_replace("/dashboard", "", $page_url);
    }
	//elseif($page['slug']=='downloads'){
       // echo str_replace("/dashboard", "", $page_url);
    //}
	elseif($page['slug']=='addresses'){
        echo str_replace("/dashboard", "", $page_url);
    }elseif($page['slug']=='affiliate-area'){
        echo str_replace("/dashboard", "", $page_url);
    }elseif($page_label=='View Store'){
        global $current_user;
        get_currentuserinfo();
        $user= $current_user->user_login . "\n";
        $page_url=site_url().'/user/'.$user.'?store=recent'; echo $page_url;
    } else{echo $page_url;} ?>"><?php echo $page_label; ?></a>
</li>