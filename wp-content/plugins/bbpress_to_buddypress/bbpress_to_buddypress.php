<?php
/**
 * @package bbpress_to_buddypress
 * @version 1.0.1
 */
/*
Plugin Name: BBPress To BuddyPress
Plugin URI: http://mavensoftwares.com/
Description: Its a custom plugin for buddypress
Author: MavenSoftwares
Version: 1.0.1
Author URI: http://mavensoftwares.com/
*/

function bbp_current_user_id_custom() {
	return bbp_get_current_user_id();
}

function bbp_user_profile_url_custom( $user_id = 0, $user_nicename = '' ) {
	return esc_url( bbp_get_user_profile_url( $user_id, $user_nicename ) );
}

function bbp_user_profile_edit_url_custom($user_id = 0){
	return esc_url(bbp_user_profile_edit_url($user_id));
}

$UserForumSetup = new UserForumSetup(); /// call class object here
class UserForumSetup{
    public function __construct(){
        add_shortcode('userForum', array($this, 'userForum'));
    }
	public function pre( $array = 'No Array' ){
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
	public function userForum( $atts ){
		global $wpdb;
	
		$user_id 		= $atts['user_id'];			
		$author_obj 	= get_user_by('id', $user_id);
		$login 			= $author_obj->user_login;
		$display_name 	= $author_obj->display_name;
		$url			= get_bloginfo('url');
	if(isset($_POST['additionalinfog']) && !empty($_POST['additionalinfog'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['additionalinfog'];

					update_user_meta(get_current_user_id(),'additionalinfog',$metaval);
				}
	if(isset($_POST['cdescriptiongs']) && !empty($_POST['cdescriptiongs'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['cdescriptiongs'];

					update_user_meta(get_current_user_id(),'cdescriptiongs',$metaval);
				}
    if(isset($_POST['phone']) && !empty($_POST['phone'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['phone'];

					update_user_meta(get_current_user_id(),'phone_contact',$metaval);
				}
    if(isset($_POST['email']) && !empty($_POST['email'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['email'];

					update_user_meta(get_current_user_id(),'email_contact',$metaval);
				}
				
    if(isset($_POST['fax']) && !empty($_POST['fax'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['fax'];

					update_user_meta(get_current_user_id(),'fax_contact',$metaval);
				}	
				
				
    /*if(isset($_POST['ne_contact_title']) && !empty($_POST['ne_contact_title'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['ne_contact_title'];

					update_user_meta(get_current_user_id(),'ne_contact_title',$metaval);
				}	
    if(isset($_POST['ne_contact_name']) && !empty($_POST['ne_contact_name'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['ne_contact_name'];

					update_user_meta(get_current_user_id(),'ne_contact_name',$metaval);
				}
    if(isset($_POST['ne_contact_link']) && !empty($_POST['ne_contact_link'])){
					//$metaname = $_POST['metaname'];
					$metaval = $_POST['ne_contact_link'];

					update_user_meta(get_current_user_id(),'ne_contact_link',$metaval);
				}*/
	   if(isset($_POST['notbable'])){			
    if(($_FILES['ne_contact_image']['type'] == 'image/jpg') || ($_FILES['ne_contact_image']['type'] == 'image/jpeg') || ($_FILES['ne_contact_image']['type'] == 'image/png') || ($_FILES['ne_contact_image']['type'] == 'image/gif')){
		$user_id = get_current_user_id();
		//$filenamefeed = preg_replace('/\s+/', '', $_FILES['bgfile_feed']['name']);
		$file = 'wp-content/uploads/'.rand($user_id, 99999999).$_FILES['ne_contact_image']['name'];
		move_uploaded_file($_FILES['ne_contact_image']['tmp_name'], $file);
         
		
		//update_user_meta($user_id, 'ne_contact_image', $file);
		//echo '<script>window.location.assign(window.location.href);</script>';
	    }

        $ne_contact_link = $_POST['ne_contact_link'];
	    $ne_contact_name = $_POST['ne_contact_name'];
	    $ne_contact_title = $_POST['ne_contact_title'];
		 
       $wpdb->insert( 'wp_notablemember', array(
	                            'member_id'=>$user_id,
								'member_name'=>$ne_contact_name,
								'member_title' =>$ne_contact_title,
								'member_link' =>$ne_contact_link,
								'member_img' =>$file
								), array( '%d', '%s', '%s', '%s', '%s' ));
								
	   }	
					
			
echo '<div id="bbpress-forums">
	<div id="bbp-user-wrapper">
		<div id="bbp-single-user-details">
			<!--<div id="bbp-user-avatar">
				<span class="vcard">	
					<a rel="me" title="'.$display_name.'" href="'.$url.'/user/'.$login.'" class="avtarimg url fn n">
						'.get_avatar( $user_id, 150 ).'
					</a>
				</span>
			</div>--><!-- #author-avatar -->
			<div id="bbp-user-navigation">
				<ul class="nav nav-tabs">
					<li class="active">
						<span class="vcard bbp-user-profile-link">
							<a data-toggle="tab" rel="me" title="'.$display_name.'\'s Profile" href="#menu1" class="url fn n">Profile</a>
						</span>
					</li>
					<li class="current">
						<span class="bbp-user-topics-created-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Topics Started" href="#menu2">Topics Started</a>
						</span>
					</li>
					<li class="">
						<span class="bbp-user-replies-created-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Replies Created" href="#menu3">Replies Created</a>
						</span>
					</li>
					<li class="">
						<span class="bbp-user-favorites-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Favorites" href="#menu4">Favorites</a>
						</span>
					</li>
					<li class="">
						<span class="bbp-user-subscriptions-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Subscriptions" href="#menu5">Subscriptions</a>
						</span>
					</li>
                     <li class="">
						<span class="bbp-user-subscriptions-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Subscriptions" href="#menu7">Add notable members</a>
						</span>
					</li>	
                   <li class="">
						<span class="bbp-user-subscriptions-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Subscriptions" href="#menu8">Add additional info</a>
						</span>
					</li>
                    <li class="">
						<span class="bbp-user-subscriptions-link">
							<a data-toggle="tab" title="'.$display_name.'\'s Subscriptions" href="#menu9">Add contact us</a>
						</span>
					</li>					
					<li class="">
						<span class="bbp-user-edit-link">
							<a title="Edit '.$display_name.'\'s Profile" href="';
							bbp_user_profile_edit_url_custom($user_id);
							echo'">Edit</a>
						</span>
					</li>				
				</ul>
			</div><!-- #bbp-user-navigation -->
		</div><!-- #bbp-single-user-details -->	
		
		<div id="bbp-user-body">
		<div class="tab-content">';		
	
	//forum		topic		replie
	$forumArray = get_posts( 
		array(
			'posts_per_page' 	=> -1,	
			'post_type' 		=> 'forum',
			'post_status' 		=> 'publish'
	)	);		
	$totalTopicArray = get_posts( 
		array(
			'posts_per_page' 	=> -1,	
			'post_type' 		=> 'topic',
			'post_status' 		=> 'publish'
	)	);	  	//user_id 
	//$result = $wpdb->get_results("SELECT * FROM ".$wpdb->base_prefix."user_views WHERE ip='ip'");
	$totalViewedTopicArray = get_posts( 
		array(
			'posts_per_page' 	=> -1,
			'post_type' 		=> 'topic',
			'post_status' 		=> 'publish',
			'meta_query' 		=> array(
				array(
					'key' 		=> 'user_id_save',
					'value' 	=> $user_id,
					'compare' 	=> '='
	)	)	)	);	
	$topicArray = get_posts( 
		array(
			'posts_per_page' 	=> -1,	
			'post_type' 		=> 'topic',
			'author' 			=> $user_id,
			'post_status' 		=> 'publish'
	)	);	
	//$totalViewedTopic = count($totalViewedTopicArray);
	$totalTopic = count($totalTopicArray);
	$replieArray = get_posts( 
		array(
			'posts_per_page' 	=> -1,	
			'post_type' 		=> 'replie',
			'post_status' 		=> 'publish'
	)	);
	
	
	
	echo '
	<div id="menu1" class="tab-pane fade in active">
		<div class="bbp-user-profile" id="bbp-user-profile">
			<h2 class="entry-title">Profile</h2>
			<div class="bbp-user-section">
				<p class="bbp-user-forum-role">Forum Role: Keymaster</p>
				<p class="bbp-user-topic-count">Topics Started: 1</p>
				<p class="bbp-user-reply-count">Replies Created: 0</p>
			</div>
		</div><!-- #bbp-author-topics-started -->	
	</div>
	<div id="menu2" class="tab-pane fade">';
	
	if(isset($forumArray) and is_array($forumArray) and count($forumArray)>0){
		foreach($forumArray as $key=>$val){
			global $post; $post = $val; setup_postdata( $post );
			$meta = get_post_meta( get_the_ID() );
			//$this->pre($meta);
	}	}
	//$this->pre($forumArray);
	
	if(isset($topicArray) and is_array($topicArray) and count($topicArray)>0){
	echo '<div class="bbp-user-topics-started" id="bbp-user-topics-started">
		<h2 class="entry-title">Forum Topics Started</h2>
		<div class="bbp-user-section">
			<div class="bbp-pagination">
				<div class="bbp-pagination-count">
					Viewing topic 1 (of 1 total)
				</div>
				<div class="bbp-pagination-links">		
				</div>
			</div>
			<ul class="bbp-topics" id="bbp-forum-0">
				<li class="bbp-header">
					<ul class="forum-titles">
						<li class="bbp-topic-title">Topic</li>
						<li class="bbp-topic-voice-count">Voices</li>
						<li class="bbp-topic-reply-count">Posts</li>
						<li class="bbp-topic-freshness">Freshness</li>
					</ul>
				</li>
				<li class="bbp-body">';
		foreach($topicArray as $key=>$val){
			global $post; $post = $val; setup_postdata( $post );
			$meta = get_post_meta( get_the_ID() );

			
			$parent_id = wp_get_post_parent_id(get_the_ID());			
			$voice = get_post_meta( get_the_ID() , '_bbp_voice_count' , true );
			$reply = get_post_meta( get_the_ID() , '_bbp_reply_count' , true );
			/**
			 * Get current user's info to find profile_pic
			 * If there is no profile pic then show Gravatar
			 */
			$current_user = wp_get_current_user();
			$user_id = get_current_user_id();
			$profile_meta = 'profile_pic_meta';
			$profile_img = get_user_meta( $user_id, $profile_meta, true );
			if($profile_img){
				$img2show = site_url() . '/profile_pic/' . $profile_img;
				$profile_link = "<img src={$img2show} height='35' width='35' / >";
			}else{
                $profile_link = get_avatar( $current_user->ID, '35' );
			}
			
			echo '<ul class="odd bbp-parent-forum-'.$parent_id.' user-id-'.get_the_author_ID().' post-'.get_the_ID().' topic type-topic status-publish hentry" id="bbp-topic-'.get_the_ID().'">
				<li class="bbp-topic-title">
					<a href="'.get_the_permalink().'" class="bbp-topic-permalink">'.get_the_title().'</a>
					<p class="bbp-topic-meta">
						<span class="bbp-topic-started-by">Started by: <a rel="nofollow" class="bbp-author-avatar" title="View '.$display_name.'\'s profile" href="'.$url.'/user/'.$login.'">'.$profile_link.'</a>&nbsp;<a rel="nofollow" class="bbp-author-name" title="View '.$display_name.'\'s profile" href="'.$url.'/user/'.$login.'">'.$display_name.'</a></span>	
						<span class="bbp-topic-started-in">in: <a href="'.get_the_permalink($parent_id).'">'.get_the_title($parent_id).'</a></span>
					</p>
				</li>
				
				<li class="bbp-topic-voice-count">'.$voice.'</li>
				<li class="bbp-topic-reply-count">'.$reply.'</li>
				<li class="bbp-topic-freshness">		
					<a title="'.get_the_title().'" href="'.get_the_permalink().'">'.human_time_diff(get_the_time('U'),current_time('timestamp')).' ago'.'</a>		
					<p class="bbp-topic-meta">			
						<span class="bbp-topic-freshness-author"><a rel="nofollow" class="bbp-author-avatar" title="View '.$display_name.'\'s profile" href="'.bbp_user_profile_url_custom(get_the_author_ID()).'">'.$profile_link.'</a>&nbsp;<a rel="nofollow" class="bbp-author-name" title="View '.$display_name.'\'s profile" href="'.$url.'/user/'.$login.'">'.$display_name.'</a></span>
					</p>
				</li>
			</ul>';
			//$this->pre($meta);
		}	
		echo '</li>
				<li class="bbp-footer">
					<div class="tr">
						<p><span class="td colspan4">&nbsp;</span></p>
					</div>
				</li>
			</ul>
			<div class="bbp-pagination">
						<div class="bbp-pagination-count">
							Viewing topic 1 (of '.$totalTopic.' total)
						</div>
						<div class="bbp-pagination-links">		
						</div>
					</div>			
				</div>
			</div><!-- #bbp-user-topics-started -->';
	}	
	//$this->pre($topicArray);	
	
	if(isset($replieArray) and is_array($replieArray) and count($replieArray)>0){
		foreach($replieArray as $key=>$val){
			global $post; $post = $val; setup_postdata( $post );
			$meta = get_post_meta( get_the_ID() );
			//$this->pre($meta);
	}	}
	//$this->pre($replieArray);
		echo '
	</div>
	<div id="menu3" class="tab-pane fade">
		<div class="bbp-user-replies-created" id="bbp-user-replies-created">
			<h2 class="entry-title">Forum Replies Created</h2>
			<div class="bbp-user-section">
				<p>You have not replied to any topics.</p>	
			</div>
		</div><!-- #bbp-user-replies-created -->	
	</div>
	<div id="menu4" class="tab-pane fade">
		<div class="bbp-user-favorites" id="bbp-user-favorites">
			<h2 class="entry-title">Favorite Forum Topics</h2>
			<div class="bbp-user-section">
				<p>You currently have no favorite topics.</p>
			</div>
		</div><!-- #bbp-user-favorites -->	
	</div>
	<div id="menu5" class="tab-pane fade">
		<div class="bbp-user-subscriptions" id="bbp-user-subscriptions">
			<h2 class="entry-title">Subscribed Forums</h2>
			<div class="bbp-user-section">
				<p>You are not currently subscribed to any forums.</p>
			</div>
		</div><!-- #bbp-user-subscriptions -->	
	</div>
	<div id="menu6" class="tab-pane fade">
		<fieldset class="bbp-form">';
	
		
		if ( !function_exists( 'bbp_displayed_user_field' ) ) { 
			require_once $_SERVER['DOCUMENT_ROOT'].'wp-content/plugins/bbpress/includes/users/template.php';
		} 
		$filelocation='wp-content/plugins/bbpress/templates/default/bbpress/form-user-edit.php';
		include($_SERVER['DOCUMENT_ROOT'].$filelocation);
	
		echo '	
		</fieldset>
	</div>';
	 echo '
	  <div id="menu8" class="tab-pane fade">
		<div class="bbp-user-subscriptions" id="bbp-user-subscriptions">
			<h2 class="entry-title">Add additional info</h2>
			<div class="bbp-user-section">
				<div class="form-group">
				  <form method="post" action="">
				  <textarea  name="additionalinfog" class="form-control" >
				   '. get_the_author_meta('additionalinfog', $current_user->ID) .'
				  </textarea>
				  <br>
				  <input type="submit" class="btn btn-success custom_btn_style" value="Update">
				</div>
				</form>
			</div>
		</div><!-- #bbp-user-subscriptions -->	
	</div>';
	echo '
	  <div id="menu9" class="tab-pane fade">
		<div class="bbp-user-subscriptions" id="bbp-user-subscriptions">
			<h2 class="entry-title">Add contact us</h2>
			<div class="bbp-user-section">
				<div class="form-group">
				  <form method="post" action="">
				  
				  <span>Address</span>
				  <input type="text" class="form-control" name="cdescriptiongs" placeholder="Address" value="'. get_the_author_meta('cdescriptiongs', $current_user->ID) .'">
				  <br>
				   <span>Pnone</span>
				  <input type="text" class="form-control" name="phone" placeholder="Phone" value="'. get_the_author_meta('phone_contact', $current_user->ID) .'">
				  <br>
				   <span>Email</span>
				  <input type="text" class="form-control" name="email" placeholder="Email" value="'. get_the_author_meta('email_contact', $current_user->ID) .'">
				  <br>
				   <span>Fax</span>
				   <input type="text" class="form-control" name="fax" placeholder="Fax" value="'. get_the_author_meta('fax_contact', $current_user->ID) .'">
				  <br>
				  <span>Skype</span>
				   <input type="text" class="form-control" name="skype" placeholder="Skype" value="'. get_the_author_meta('skype', $current_user->ID) .'">
				    <br>
				  <span>WhatsApp</span>
				   <input type="text" class="form-control" name="whatsApp" placeholder="WhatsApp" value="'. get_the_author_meta('whatsApp', $current_user->ID) .'">
				    <br>
				  <input type="submit" class="btn btn-success custom_btn_style" value="Update">
				</div>
				</form>
			</div>
		</div><!-- #bbp-user-subscriptions -->	 
	</div>';
	echo '
	  <div id="menu7" class="tab-pane fade">
		<div class="bbp-user-subscriptions" id="bbp-user-subscriptions">
			<h2 class="entry-title">Add notable members</h2>
			<div class="bbp-user-section">
				<div class="form-group">
				  <form method="post" action="" enctype="multipart/form-data">
				  <span>Profile Image</span>
				  <input type="file" id="ne_contact_image" name="ne_contact_image" required value="'. get_the_author_meta('ne_contact_image', $current_user->ID) .'">
				   <br>
				  <span>Name</span>
				  <input type="text" class="form-control" name="ne_contact_name" placeholder="Name" value="'. get_the_author_meta('ne_contact_name', $current_user->ID) .'">
				  <br>
				  <span>Title</span>
				  <input type="text" class="form-control" name="ne_contact_title" placeholder="Title" value="'. get_the_author_meta('ne_contact_title', $current_user->ID) .'">
				  <br>
				  <span>Contact URL</span>
				  <input type="text" class="form-control" name="ne_contact_link" placeholder="Contact URL" value="'. get_the_author_meta('ne_contact_link', $current_user->ID) .'">
				  <br>
				  <input type="submit" class="btn btn-success custom_btn_style" name="notbable" value="Update">
				</div>
				</form>
			</div>
		</div><!-- #bbp-user-subscriptions -->	
	</div>
	</div>
	</div>	
	</div>
</div>';

		$a = 0;
		if($a == 10){
		/*************************************************** forum code *******************************************/  
		do_action( 'bbp_template_before_user_details' ); ?>

		<div id="bbp-single-user-details">
			<div id="bbp-user-avatar">

				<span class='vcard'>
					<a class="avtarimg url fn n" href="<?php bbp_user_profile_url_custom($user_id); ?>" title="<?php bbp_displayed_user_field( 'display_name' ); ?>" rel="me">
						<?php //echo get_avatar( bbp_get_displayed_user_field( 'user_email', 'raw' ), apply_filters( 'bbp_single_user_details_avatar_size', 150 ) ); ?>
						<?php echo $profile_link; ?>
					</a>
				</span>

			</div><!-- #author-avatar -->

			<div id="bbp-user-navigation">
				<ul>
					<li class="<?php if ( bbp_is_single_user_profile() ) :?>current<?php endif; ?>">
						<span class="vcard bbp-user-profile-link">
							<a class="url fn n" href="<?php bbp_user_profile_url_custom($user_id); ?>" title="<?php printf( esc_attr__( "%s's Profile", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>" rel="me"><?php _e( 'Profile', 'bbpress' ); ?></a>
						</span>
					</li>

					<li class="<?php if ( bbp_is_single_user_topics() ) :?>current<?php endif; ?>">
						<span class='bbp-user-topics-created-link'>
							<a href="<?php bbp_user_topics_created_url($user_id); ?>" title="<?php printf( esc_attr__( "%s's Topics Started", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Topics Started', 'bbpress' ); ?></a>
						</span>
					</li>

					<li class="<?php if ( bbp_is_single_user_replies() ) :?>current<?php endif; ?>">
						<span class='bbp-user-replies-created-link'>
							<a href="<?php bbp_user_replies_created_url($user_id); ?>" title="<?php printf( esc_attr__( "%s's Replies Created", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Replies Created', 'bbpress' ); ?></a>
						</span>
					</li>

					<?php if ( bbp_is_favorites_active() ) : ?>
						<li class="<?php if ( bbp_is_favorites() ) :?>current<?php endif; ?>">
							<span class="bbp-user-favorites-link">
								<a href="<?php bbp_favorites_permalink($user_id); ?>" title="<?php printf( esc_attr__( "%s's Favorites", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Favorites', 'bbpress' ); ?></a>
							</span>
						</li>
					<?php endif; ?>

					<?php if ( bbp_is_user_home() || current_user_can( 'edit_users' ) ) : ?>

						<?php if ( bbp_is_subscriptions_active() ) : ?>
							<li class="<?php if ( bbp_is_subscriptions() ) :?>current<?php endif; ?>">
								<span class="bbp-user-subscriptions-link">
									<a href="<?php bbp_subscriptions_permalink($user_id); ?>" title="<?php printf( esc_attr__( "%s's Subscriptions", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Subscriptions', 'bbpress' ); ?></a>
								</span>
							</li>
						<?php endif; ?>

						<li class="<?php if ( bbp_is_single_user_edit() ) :?>current<?php endif; ?>">
							<span class="bbp-user-edit-link">
								<a href="<?php bbp_user_profile_edit_url($user_id); ?>" title="<?php printf( esc_attr__( "Edit %s's Profile", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Edit', 'bbpress' ); ?></a>
							</span>
						</li>

					<?php endif; ?>

				</ul>
			</div><!-- #bbp-user-navigation -->
		</div><!-- #bbp-single-user-details -->

		<?php 
		do_action( 'bbp_template_after_user_details' );
		/*************************************************** forum code end *************************************/
		}	
	}
}

?>
