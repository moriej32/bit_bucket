<?php
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Views_List extends WP_List_Table {
	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => 'id', //singular name of the listed records
			'plural'   => 'ids', //plural name of the listed records
			'ajax'     => false //should this table support ajax?

		] );

	}
	
	function column_default($item, $column_name){
        switch($column_name){
            case 'id':
            case 'ipAddress':
            case 'contentUrl': 
            case 'views': 
			case 'duration': 
			case 'cost': 			
            case 'date':    
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }
	
	function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="id[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['id']                //The value of the checkbox should be the record's id
        );
    }
	
	function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text            
            'ipAddress'     => 'IP Address',
            'contentUrl'    => 'Content URL',
			'duration' => 'Duration',
			'cost' => 'Cost',
            'date'  => 'Date'
        );
        return $columns;
    }
	
	function get_sortable_columns() {
        $sortable_columns = array(
            'id'     => array('id',false), 
            'ipAddress'     => array('ipAddress',false),     //true means it's already sorted
            'contentUrl'     => array('contentUrl',false),
			'duration' => array('duration',false),
			'cost' => array('cost',false),
            'date'  => array('date',false)
        );
        return $sortable_columns;
    }
	
	function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }
	
	function process_bulk_action() {
        global $wpdb;       
        if(isset($_POST['id'])){ 
              if( 'delete' === $this->current_action() ) { 
                 foreach($_POST['id'] as $id) {
                     $id = absint($id);                                        
                     $wpdb->query("DELETE FROM ".$wpdb->prefix."oio_custom_views_count WHERE ID=$id");
                 }
              }
        }  
    }
	
	
	function extra_tablenav( $which ) {
		global $wpdb, $wp_locale;
		//$move_on_url = '&cat-filter=';
		if ( $which == "top" ){
			?>
			<div class="alignleft actions bulkactions">
			<?php			
			$months = $wpdb->get_results( $wpdb->prepare("SELECT DISTINCT YEAR(date) AS year, MONTH(date) AS month FROM ".$wpdb->prefix."oio_custom_views_count ORDER BY date DESC "));
			
			//$contents = $wpdb->get_results($wpdb->prepare("SELECT contentUrl FROM ".$wpdb->prefix."oio_custom_views_count ORDER BY date DESC "));
			
			if( $months ){
				?>
				<!--form method="post"-->
				<select name="date-filter" class="ewc-filter-cat">
					<option value="">All Dates</option>
					<?php
					foreach ( $months as $arc_row ) {			  
						
					  if ( 0 == $arc_row->year )
						continue;

					  $month = zeroise( $arc_row->month, 2 );
					  $year = $arc_row->year;
					  
					  $selected = '';
					  if( $_POST['date-filter'] == $arc_row->year . $month ){
						$selected = ' selected = "selected"';   
					  }

					  printf( "<option %s value='%s' ".$selected.">%s</option>\n",
						selected( $year . $month, true ),
						esc_attr( $arc_row->year . $month ),
						/* translators: 1: month name, 2: 4-digit year */
						sprintf( __('%1$s %2$d'), $wp_locale->get_month( $month ), $year )
					  );
					}
					?>
				</select>
				<!--select name="content-filter">
					<option value="">All Contents</option>
					<?php /* foreach($contents as $content){
						$contenturl = $content->contentUrl;
						$end = end((explode('/', rtrim($contenturl, '/'))));
						
						$selected = '';
						if( $_POST['content-filter'] == $contenturl ){
							$selected = ' selected = "selected"';   
						}
						
						echo '<option '.$selected.' value="'.$contenturl.'">'.$end.'</option>';
					} */?>
				</select-->
				<input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter">
				<!--/form-->
				<?php   
			}
			?>  
			</div>
			<?php
		}
		if ( $which == "bottom" ){
			//The code that goes after the table is there

		}
	} 
	
	
	public function table_data(){
	   global $wpdb;
	   $data = array();
	   $string_query = "SELECT * FROM ".$wpdb->prefix ."oio_custom_views_count";
	   if(isset($_POST['filter_action'])){
			$getmonth = substr($_POST['date-filter'], -2);
			$getyear = substr($_POST['date-filter'], 0, 4);
			//$contentUrl = $_POST['content-filter'];
			/* if($getmonth!=NULL && $getyear!=NULL && $_POST['content-filter']!=NULL){
				$string_query = $string_query . " where MONTH(date)=".$getmonth ." and YEAR(date)=".$getyear ." and contentUrl= '".$contentUrl."'";
			} */
			//else{
				if($getmonth!=NULL && $getyear!=NULL){
					$string_query = $string_query . ' where MONTH(date)='.$getmonth .' and YEAR(date)='.$getyear. ' ORDER BY DAY(date) DESC';
				}
				/* else {
					if($_POST['content-filter']!=NULL)
					$string_query = $string_query . " where contentUrl= '".$contentUrl."'";
				}
			} */
				
	   }else{
		   $string_query = $string_query . ' ORDER BY date DESC';
		   }
	   $query = $wpdb->get_results($string_query);		
	   foreach($query as $myrow){
			$data[] = array(
				'id'        	=> $myrow->id,
				'ipAddress'     	=> $myrow->ipAddress,
				'contentUrl' 	=> $myrow->contentUrl,
				'duration' => $myrow->duration,
				'cost' => $myrow->cost.'$',
				'date'   => $myrow->date
			);
		}		
        return $data;
    }
	
	function prepare_items() {
        global $wpdb; 
        $per_page = 10;
        
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        $this->process_bulk_action();
		           
        $data = $this->table_data();
                      
        /* function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'ID'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder'); */
        
        $current_page = $this->get_pagenum();
                
        $total_items = count($data);
        
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        $this->items = $data;
        
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
	
}

function views_screen_columns( $columns ) {
		$columns["date"] = "date";
		return $columns;
}
add_filter('manage_clicks-count_page_views_count_columns', 'views_screen_columns');


function view_render_list_page(){
	global $wpdb; 
	//Create an instance of our package class...
    $viewListTable = new Views_List();
    //Fetch, prepare, sort, and filter our data...
    $viewListTable->prepare_items();
	$screen = get_current_screen();
	$itemrow = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."oiopub_purchases WHERE item_id=35");
	$value = $itemrow->item_duration_left;
	
	?>
	<div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Count Views Records</h2>		
		<p><?php echo $value;?></p>
        <p><?php //echo $screen->id;?></p>		
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->  
		<form id="posts-filter" method="post">            
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>">				
			<?php $viewListTable->display() ?>	
		</form>	
        
    </div>
<?php }