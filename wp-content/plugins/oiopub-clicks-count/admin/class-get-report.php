<?php
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Record_List extends WP_List_Table{
	
	public function __construct(){
		
		parent::__construct([
			'singular' => 'id',
			'plural'   => 'ids',
			'ajax'     => false
		]);
		
	}
	
	function column_default($item, $column_name){
        switch($column_name){
            case 'id':
            case 'contentUrl': 
			case 'clicks':
            case 'views': 
			case 'cost': 			
            case 'date':    
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }
	
	function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="id[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }
	
	function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'contentUrl'    => 'Content URL',
			'clicks' => ' Total Clicks',
			'views' => 'Total Views',
			'cost' => 'Total Cost',
            'date'  => 'Current Date'
        );
        return $columns;
    }
	
	function get_sortable_columns() {
        $sortable_columns = array(
            'id'     => array('id',false), //true means it's already sorted
            'contentUrl'     => array('contentUrl',false),
			'clicks'     => array('clicks',false),
			'views'     => array('views',false),
			'cost' => array('cost',false),
            'date'  => array('date',false)
        );
        return $sortable_columns;
    }
	
	function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }
	
	function process_bulk_action() {
        global $wpdb;       
        if(isset($_POST['id'])){ 
              if( 'delete' === $this->current_action() ) { 
                 foreach($_POST['id'] as $id) {
                     $id = absint($id);                                        
                     $wpdb->query("DELETE FROM ".$wpdb->prefix."oio_custom_views_count WHERE ID=$id");
                 }
              }
        }  
    }
	
	public function extra_tablenav( $which ) {
		global $wpdb, $wp_locale;
		if ( $which == "top" ){
			?>
			<div class="alignleft actions bulkactions">
			<?php			
			$months = $wpdb->get_results( $wpdb->prepare("SELECT DISTINCT YEAR(date) AS year, MONTH(date) AS month FROM ".$wpdb->prefix."oio_custom_clicks_count UNION SELECT DISTINCT YEAR(date) AS year, MONTH(date) AS month FROM ".$wpdb->prefix."oio_custom_views_count"));
			
			$contents = $wpdb->get_results($wpdb->prepare("SELECT contentUrl FROM ".$wpdb->prefix."oio_custom_clicks_count UNION DISTINCT SELECT contentUrl FROM ".$wpdb->prefix."oio_custom_views_count"));
			
			if( $months ){
				?>
				<form method="post">
				<select name="date-filter" class="ewc-filter-cat">
					<option value="">All Dates</option>
					<?php
					foreach ( $months as $arc_row ) {			  
						
					  if ( 0 == $arc_row->year )
						continue;

					  $month = zeroise( $arc_row->month, 2 );
					  $year = $arc_row->year;
					  
					  $selected = '';
					  if( $_POST['date-filter'] == $arc_row->year . $month ){
						$selected = ' selected = "selected"';   
					  }
					  
					  if($arc_row->year . $month == date("Ym")){
						  $value = "This Month";
					  }else{
						  $value = sprintf( __('%1$s %2$d'), $wp_locale->get_month( $month ), $year);
					  }

					  printf( "<option %s value='%s' ".$selected.">%s</option>\n",
						selected( $year . $month, true ),
						esc_attr( $arc_row->year . $month ),
						/* translators: 1: month name, 2: 4-digit year */
						$value
					  );
					}
					?>
				</select>
				<select name="content-filter">
					<option value="">All Contents</option>
					<?php foreach($contents as $content){
						$contenturl = $content->contentUrl;
						$end = end((explode('/', rtrim($contenturl, '/'))));
						
						$selected = '';
						if( $_POST['content-filter'] == $contenturl ){
							$selected = ' selected = "selected"';   
						}
						
						echo '<option '.$selected.' value="'.$contenturl.'">'.$end.'</option>';
					}?>
				</select>
				<input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter">
				</form>
				<?php   
			}
			?>  
			</div>
			<?php
		}
		if ( $which == "bottom" ){
			//The code that goes after the table is there

		}
	} 
	
	public function table_data(){
	   global $wpdb, $wp_locale;
	   $data = array();
	   $clicktable = $wpdb->prefix."oio_custom_clicks_count";
	   $viewtable = $wpdb->prefix."oio_custom_views_count";
	   $getQuery = "1";
	   if(isset($_POST['filter_action'])){
		   $getmonth = substr($_POST['date-filter'], -2);
		   $getyear = substr($_POST['date-filter'], 0, 4);
		   if($getmonth!=NULL && $getyear!=NULL && $_POST['content-filter']!=NULL){
				$getQuery = "MONTH(date)=$getmonth and YEAR(date)=$getyear and contentUrl= '".$_POST['content-filter']."'";
		   }
		   else{
				if($getmonth!=NULL && $getyear!=NULL)
					$getQuery = "MONTH(date)=$getmonth and YEAR(date)=$getyear";
				else {
					if($_POST['content-filter']!=NULL)
					$getQuery = "contentUrl= '".$_POST['content-filter']."'";
				}
		   }
	   }
	   
	   $getcontenturls = $wpdb->get_results("SELECT contentUrl FROM $clicktable WHERE $getQuery UNION DISTINCT SELECT contentUrl FROM $viewtable WHERE $getQuery");
	   
	   foreach($getcontenturls as $getcurl){
		   $contenturl=	$getcurl->contentUrl;	
		   
		   $clickquery = $wpdb->get_row("SELECT SUM(clicks) as totalclicks, SUM(cost) as clickscost, MONTH(date) as month, YEAR(date) as year FROM $clicktable where contentUrl='$contenturl'");
		   
		   $viewquery = $wpdb->get_row("SELECT SUM(views) as totalviews, SUM(cost) as viewcost FROM $viewtable where contentUrl='$contenturl'");
		   
		   if($clickquery->month && $clickquery->year){
			   $month = $clickquery->month;
			   $year = $clickquery->year;
		   }else{
			   if($viewquery->month && $viewquery->year){
				   $month = $viewquery->month;
				   $year = $viewquery->year;
			   }
		   }
		   
		   $cost = $clickquery->clickscost + $viewquery->viewcost;		   
		   
		   if(!$viewquery->totalviews)$viewquery->totalviews=0;
		   if(!$clickquery->totalclicks)$clickquery->totalclicks=0;
		   
		   $data[] = array(
				'id'   => $contenturl,
				'contentUrl'=> $contenturl,
				'clicks' => $clickquery->totalclicks,
				'views' => $viewquery->totalviews,
				'cost' => $cost.'$',
				'date' => sprintf( __('%1$s %2$d'), $wp_locale->get_month( $month ), $year)
		   );
		   
		   
	   }	   
       return $data;
    }
	
	function prepare_items() {
        global $wpdb; 
        $per_page = 10;
        
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        $this->process_bulk_action();
		           
        $data = $this->table_data();
                      
        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'ID'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
        
        $current_page = $this->get_pagenum();
                
        $total_items = count($data);
        
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        $this->items = $data;
        
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
}

function get_report(){
	
	//Create an instance of our package class...
    $recordTable = new Record_List();
    //Fetch, prepare, sort, and filter our data...
    $recordTable->prepare_items();
	
	?>
	<div class="wrap">        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Report</h2>	  
		
		<?php $recordTable->display() ?>	
		
    </div>
<?php }