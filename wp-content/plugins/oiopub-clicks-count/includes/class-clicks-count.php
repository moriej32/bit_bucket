<?php
class Clicks_Count {
	
	public function __construct() {

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}
	
	private function load_dependencies() {
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-clicks-record-lists.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-views-record-lists.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-get-report.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-pay-paypal.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-temporarydaya-paypal.php';
		
	}
	
	private function define_admin_hooks(){
		
		function clicks_add_menu_items(){
			//add_menu_page('Clicks Count', 'OIO clicks count', 'activate_plugins', 'clicks_count', 'click_render_list_page');
			add_menu_page('Ads Revenue', 'Ads Revenue', 'manage_options', 'clicks_count', 'click_render_list_page');
			add_submenu_page('clicks_count', 'Views Count', 'Views Count', 'manage_options','views_count', 'view_render_list_page');
			add_submenu_page('clicks_count', 'Report', 'Report', 'manage_options','report', 'get_report');
			add_submenu_page('clicks_count', 'paypal', 'paypal', 'manage_options','paypal', 'pay_paypal');
			//add_submenu_page('clicks_count', 'tempdata', 'tempdata', 'manage_options','tempdata', 'tempdata');

		} add_action('admin_menu', 'clicks_add_menu_items');
		
	}
	
	
	private function define_public_hooks(){
		
		function oio_clicks_count_script() { ?>
		<script type="text/javascript">
		var jquery = jQuery.noConflict();
			//var media = document.getElementById('myVideo');
			var post_id = '<?php global $post; echo $post->ID; ?>';
			var post_url = '<?php echo get_permalink($post->ID)?>';
			var author_id = '<?php global $post; echo $post->post_author; ?>';
			var posttype = '<?php echo get_post_type(get_the_ID()) ; ?>'			
			var count = 0;					
			
			jQuery(document).ready(function(){	
				var bla = $('#blaval').val();
				jQuery.getJSON("http://jsonip.com/?callback=?", function(data){
					ip_address = data.ip;
					//alert(ip_address);
				});
				
				jQuery('#rev-share').click(function() {
					if(jQuery(this).is(':checked')){
						//alert('checked');
						jQuery(this).val(1);
					}
					else{
						//alert('unchecked');
						jQuery(this).val(0);
					}
				});
				
				jQuery('.oio-click').click(function(){
					//e.preventDefault();
					//alert(ip_address+' '+post_id+' '+post_url+' '+author_id);	
					var type = 'clicks';
					if(post_id==2017){
						post_id = jQuery('.oio-click').attr("id");
						post_url = jQuery('#titleafter-edrit'+post_id+' a').attr("href");
						//alert(post_url);
					}else{
						post_id = '<?php global $post; echo $post->ID; ?>';
						post_url = '<?php echo get_permalink($post->ID)?>';
					}						
					jQuery.ajax({
						url: "<?php echo plugin_dir_url( __FILE__ );?>ajax/OIO-clicks-count-new.php",
						data:{ip:ip_address, post_id:post_id, post_url:post_url, type:type, author_id:author_id},
						type: "post",
					}).done(function(data){
						//alert(data);
						console.log(data);				
					});
				});	
				
				if(posttype=='dyn_file' || posttype=='dyn_book'){
					var media = document.getElementsByTagName('video')[0]; 
				}else{
					var media = document.getElementById('custom_elite_vp_videoPlayerAD');
				}
				//old line
				jQuery(window).load(function(){
				
					//var popupitemid = "<?php echo  $videoid; ?>";						
					getDuration = media.duration;
					if(getDuration>0){
						dtype = 'views';					
						jQuery.ajax({
							url: "<?php echo plugin_dir_url( __FILE__ );?>ajax/OIO-views-count-new.php",
							data:{itemid: bla, ip: ip_address, post_url: post_url, dtype: dtype, getDuration: getDuration, author_id: author_id},
							type: "post",
						}).done(function(data){
							//alert(data);
							console.log(data);				
						});
					}
					console.log(bla);		
				});
			});
			var $ = jQuery.noConflict();
		</script>	
		<?php }
		add_action( 'wp_head', 'oio_clicks_count_script' );		


		//require plugin_dir_path( __FILE__ ) . 'ajax/OIO-clicks-count.php';
		//require plugin_dir_path( __FILE__ ) . 'ajax/OIO-views-count.php';
		
	}
	
}