<?php
class Clicks_Count_Activator{
	
	public static function activate(){
		global $wpdb;
					
		if (function_exists('is_multisite') && is_multisite()) {
			// check if it is a network activation - if so, run the activation function for each blog id
			if ($networkwide) {
				$old_blog = $wpdb->blogid;
				// Get all blog ids
				$blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);
					self::activate_per_site();
				}
				switch_to_blog($old_blog);
				return;
			}	
		}
		self::activate_per_site();
	}
	
	public static function activate_per_site(){
		self::create_oio_custom_clicks_count_table();
		self::create_oio_custom_views_count_table();
		self::create_users_revenue_status_table();
		self::create_paypal_payment_record_table();
		self::create_ad_revenue_share();
	}
	
	public static function create_oio_custom_clicks_count_table(){
		
		global $wpdb;
		$table_name = $wpdb->prefix . "oio_custom_clicks_count"; 	
		$charset_collate = $wpdb->get_charset_collate();
		
		//add clicks_views table
		$sql = "CREATE TABLE IF NOT EXISTS $table_name(
			id int(10) NOT NULL auto_increment,
			ipAddress varchar(20) NOT NULL,
			contentUrl varchar(255) NOT NULL,
			author_id int(10) NOT NULL default '0',
			clicks int(10) NOT NULL default '0',
			time time NOT NULL,
			date date NOT NULL,
			cost float(10,3) NOT NULL default '0', 
			PRIMARY KEY (id)
		)ENGINE=MyISAM $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);
		
	}
	
	
	public static function create_oio_custom_views_count_table(){
		
		global $wpdb;
		$table_name = $wpdb->prefix . "oio_custom_views_count"; 	
		$charset_collate = $wpdb->get_charset_collate();
		
		//add clicks_views table
		$sql = "CREATE TABLE IF NOT EXISTS $table_name(
			id int(10) NOT NULL auto_increment,
			ipAddress varchar(20) NOT NULL,
			contentUrl varchar(255) NOT NULL,
			author_id int(10) NOT NULL default '0',
			views int(10) NOT NULL default '0',
			time time NOT NULL,
			date date NOT NULL,
			duration int(10) NOT NULL default '0',
			cost float(10,3) NOT NULL default '0', 
			PRIMARY KEY (id)
		)ENGINE=MyISAM $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);
		
	}
	
		
	public static function create_users_revenue_status_table(){
		
		global $wpdb;
		$table_name = $wpdb->prefix . "users_revenue_status"; 	
		$charset_collate = $wpdb->get_charset_collate();
		
		//add users_revenue_status table
		$sql = "CREATE TABLE IF NOT EXISTS $table_name(
			id int(10) NOT NULL auto_increment,
			user_id int(10) NOT NULL default '0',
			revenue_status int(10) NOT NULL default '0',
			date date NOT NULL,
			PRIMARY KEY (id)
		)ENGINE=MyISAM $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);		
	}
	
	
	public static function create_paypal_payment_record_table(){
		
		global $wpdb;
		$table_name = $wpdb->prefix . "paypal_payment_record"; 	
		$charset_collate = $wpdb->get_charset_collate();	
		
		//add paypal_payment_record table
		$sql = "CREATE TABLE IF NOT EXISTS $table_name(
			id int(10) NOT NULL auto_increment,
			author_id int(10) NOT NULL default '0',
			amount float(10,2) NOT NULL default '0',
			payment_status varchar(255) NOT NULL,
			paid_date date NOT NULL,
			date date NOT NULL,
			PRIMARY KEY (id)
		)ENGINE=MyISAM $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);	
		
	}
	
	public static function create_ad_revenue_share(){
		
		global $wpdb;
		$table_name = $wpdb->prefix . "ad_revenue_share"; 	
		$charset_collate = $wpdb->get_charset_collate();	
		
		//add paypal_payment_record table
		$sql = "CREATE TABLE IF NOT EXISTS $table_name(
			id int(10) NOT NULL auto_increment,
			postType varchar(255) NOT NULL,
			authorID int(10) NOT NULL default '0',
			getPercentageUserID int(10) NOT NULL default '0',
			getPercentageValue int(10) NOT NULL default '0',
			postID int(10) NOT NULL default '0',
			postUrl varchar(255) NOT NULL,
			date date NOT NULL,
			PRIMARY KEY (id)
		)ENGINE=MyISAM $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);
		
	}
		
}