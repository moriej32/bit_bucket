<?php
/*
Plugin Name: OIO clicks count
Plugin URI: http://www.doityourselfnation.org/
Author: nazmin
Author URI: http://www.doityourselfnation.org/
Version: 0.1
Description: OIO Publisher custom count system
*/

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-oio-clicks-count-activator.php
 */
function activate_OIO_clicks_count(){
	
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-oio-clicks-count-activator.php';
	Clicks_Count_Activator::activate();
	
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-oio-clicks-count-deactivator.php
 */
function deactivate_OIO_clicks_count(){
	
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-oio-clicks-count-deactivator.php';
	Clicks_Count_Deactivator::deactivate();
	
}

register_activation_hook( __FILE__, 'activate_OIO_clicks_count' );
register_deactivation_hook( __FILE__, 'deactivate_OIO_clicks_count' );

/**
 * The core plugin class that is used to define internationalization
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-clicks-count.php';


/**
 * Begins execution of the plugin.
 *
 *
 * @since    1.0.0
 */
function run_clicks_count() {

	new Clicks_Count();

}
run_clicks_count();
