<?php
/*
  Plugin Name: MSH - Redirect After Post
  Plugin URI: http://www.mainstreehost.com
  Author: Matthew Backlas for Mainstreethost
  Description: Redirects to post after publish.
  Version: 1.0
 */
 
 
add_filter('redirect_post_location', 'redirect_to_post_on_publish_or_save');
function redirect_to_post_on_publish_or_save($location)
{
    if (isset($_POST['save']) || isset($_POST['publish'])) {
		// wirte some redirect here based on the thing below
        /* if (preg_match("/post=([0-9]*)/", $location, $match)) {
            $pl = get_permalink($match[1]);
            if ($pl) {
                wp_redirect($pl);
            }
        }
		*/
    }
}
?>