<?php
/**
 * Vendor new order email
 *
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.0.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<?php do_action( 'woocommerce_email_before_order_table', $order, true ); ?>

<h2><?php printf( __( 'Order: %s', 'wcvendors'), $order->get_order_number() ); ?> (<?php printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order->order_date ) ), date_i18n( woocommerce_date_format(), strtotime( $order->order_date ) ) ); ?>)</h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'wcvendors' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Quantity', 'wcvendors' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Price', 'wcvendors' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( array(
			'show_sku'      => true,
			'show_image'    => false,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => false,
			'sent_to_admin' => false
		) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action('woocommerce_email_after_order_table', $order, true); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, true ); ?>

<h2><?php _e( 'Shipping details', 'woocommerce' ); ?></h2>

<p>1) Ship the item using one of the shipping providers below</p>
<ul>
  <li>Australia
       <ul> <li>Australia Post</li>
		 <li>FedEx</li></ul>
  </li>
  <li>Canada
       <ul><li>Canada Post</li></ul> 
  </li>
  <li>Germany
     <ul> <li>DHL Intraship (DE)</li> 
	  <li>Hermes</li> 
	  <li>Deutsche Post DHL</li> 
	  <li>UPS Germany</li> 
	  <li>DPD</li> </ul>
  </li>
  <li>Ireland
      <ul><li>DPD</li> </ul>
  </li> 
  <li>Italy
      <ul><li>BRT (Bartolini)</li> 
	  <li>DHL Express</li> </ul>
  </li>
  <li>India
       <ul><li>DTDC</li> </ul>
  </li> 
   <li>Netherlands
       <ul> <li>PostNL</li> 
		 <li>DPD.NL</li> </ul>
   </li>
   <li>New Zealand
       <ul> <li>Courier Post</li>
		<li>NZ Post</li>
		<li>Fastways</li>
		<li>PBT Couriers</li></ul>
   </li>
   <li>South African
       <ul><li>SAPO</li></ul>
	   
   </li> 
   <li>United Kingdom
       <ul> <li>DHL</li>
		<li>DPD</li>
		<li>InterLink</li>
		<li>ParcelForce</li>
		<li>Royal Mail</li>
		<li>TNT Express (consignment)</li>
		<li>TNT Express (reference)</li>
		<li>UK Mail</li></ul>
		
   </li> 
   <li>United States
        <ul>
       <li>Fedex</li>
	   <li>FedEx Sameday</li>
	   <li>OnTrac</li>
	   <li>UPS</li>
	   <li>USPS</li></ul>
   </li>   
</ul>
<p>2) Click ship on the product located on your store dashboard under the order tab and place the correct tracking number and shipping provider in it</p>
<a href="http://www.doityourselfnation.org/bit_bucket/dashboard/order/">Click Here</a>
<p>3) To communicate with purchaser use order note for the product on your orders tab </p>
<p>4) You have 6 days to ship item and provide tracking number otherwise order will be canceled</p>


<h2><?php _e( 'Customer details', 'wcvendors' ); ?></h2>

<?php if ( $order->billing_email ) : ?>
	<p><strong><?php _e( 'Email:', 'wcvendors' ); ?></strong> <?php echo $order->billing_email; ?></p>
<?php endif; ?>
<?php if ( $order->billing_phone ) : ?>
	<p><strong><?php _e( 'Tel:', 'wcvendors' ); ?></strong> <?php echo $order->billing_phone; ?></p>
<?php endif; ?>

<?php wc_get_template( 'emails/email-addresses.php', array( 'order' => $order ) ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>