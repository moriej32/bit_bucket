 <?php 
 /**
 * Plugin Name: WC Vendor Shipping Pro
 * Description:       The WC Vendors Pro plugin 
 * Version:           1.3.6
 * Author:            Nazmin
 */
 $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
if ( in_array( 'woocommerce/woocommerce.php', $active_plugins) ){
class WC_NEW_SHIPPING_METHOD
{
     
    function __construct() {
        add_filter( 'woocommerce_shipping_methods',array($this,'add_wc_new_shipping_method'));
         add_action( 'woocommerce_shipping_init',array($this,'wc_new_shipping_method_init') ); 
          
          
    }
          
    function add_wc_new_shipping_method( $methods ) {
       $methods[] = 'WC_NEW_SHIPPING_METHODS';
       return $methods;
     }
      
     function wc_new_shipping_method_init(){
       require_once 'includes/class-new-shipping.php';
     }
  
}
 
new WC_NEW_SHIPPING_METHOD();
 }
 
add_filter( 'contact_us_by_order_id','respond_contact_us' );
function respond_contact_us($order){
	 $message = 0;
	 $orderid = $order;
     $realorder = wc_get_order($orderid);
		if ( is_object( $realorder ) )
			{
				$order_id = $realorder->get_item_count();
				if ($realorder->has_status( 'processing' ) ) {
					$message = 1;	
				}
				else{
					$message = 0; 
				}
			}else{
				
			$message = 0;
			
			}
   return $message;
}
/* register_activation_hook(__FILE__, 'my_activation_send_email');

function my_activation_send_email() {
    if (! wp_next_scheduled ( 'my_hourly_email' )) {
	wp_schedule_event(time(), 'hourly', 'my_hourly_email');
    }
}

add_action('my_hourly_email', 'do_this_hourly_email');

function do_this_hourly_email()
{
	global $wpdb, $woocommerce;
		$sql = '
			SELECT DISTINCT( order_id )
			FROM wp_pv_commission
			WHERE   status = "due" '; 
			
	   $date1 = date('Y-m-d');
				
		$days = $diff = $years	= $months = '';   
		
		$unique_orders = $wpdb->get_results( $sql ); 
		 if( $unique_orders ){
			foreach ( $unique_orders as $order ) { 

				$order_id 			= $order->order_id; 
				$vendor_id 						= $order->vendor_id; 
				$ordersasd = new WC_Order($order_id);
                $order_date = $ordersasd->order_date;
				
				$diff = abs(strtotime($order_date) - strtotime($date1));
				$years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
				if( $days < 6 )
				{
				  $mails = $woocommerce->mailer()->get_emails();
			
			       if ( !empty( $mails ) ) {
					
				     $mails[ 'WC_Email_Notify_For_Shipped' ]->trigger( $order_id, $vendor_id, $days );
					   wc_add_notice( __( '$days days remaining for shipping your Products.', 'wcvendors' ), 'success' );
			       }	
				}
				
			}	
		 }			
}

register_deactivation_hook(__FILE__, 'my_deactivation_email');

function my_deactivation_email() {
	wp_clear_scheduled_hook('my_hourly_email');
}
*/

function wc_new_shipping_method_by_country(){
	ob_start(); 
    require_once 'includes/class-more-country-shipping.php';	
     header("Content-Type: application/json");
	 
	  $chat = array();
	  
	  $ansa = &$chat;
	  
	  $fdsabds = WC_MORE_SHIPPING_COUNTRY::shipping_rate_by_country_post($_POST['postidsss']);
	  
	   $ansa['is_insert'] =  $fdsabds;
   ob_end_clean();
		echo json_encode($chat);
		
		exit;
     }
	 
 add_action('wp_ajax_adtocart_call', 'addtocart_ajax');
add_action('wp_ajax_nopriv_adtocart_call', 'addtocart_ajax');
function addtocart_ajax()
  {
	$product_id = $_POST['productid'];
    WC()->cart->add_to_cart($product_id);
	echo "success";
    die();
}

add_action('wp_ajax_checkout_call', 'checkout_function');
add_action('wp_ajax_nopriv_checkout_call', 'checkout_function');
function checkout_function()
  {
	
	 
	define( 'WOOCOMMERCE_CHECKOUT', true );
	WC()->cart->calculate_totals();
	$checkout = WC()->checkout();
	echo wc_get_template( 'checkout/form-checkout.php', array( 'checkout' => $checkout ) ); 
	die();
}	

add_action('wp_ajax_checkout_action_finial', 'checkout_action_finial_function');
add_action('wp_ajax_nopriv_checkout_action_finial', 'checkout_action_finial_function');
function checkout_action_finial_function()
  {
	  global $woocommerce;
	   $cart = $woocommerce->cart;

        foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item){
                 if($cart_item['product_id'] == $_POST['product_id'] ){
        // Remove product in the cart using  cart_item_key.
              $cart->remove_cart_item($cart_item_key);
			  //echo "Remove success";
             }
           }
		    echo '<i class="fa fa-shopping-cart" aria-hidden="true"><sup>'.
			sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ).
			'</sup></i> <bdi> - </bdi>'. 
			WC()->cart->get_cart_total();
			
    die();
}
 
add_action('wp_ajax_checkout_menu_popup', 'checkout_popup_menu_function');
add_action('wp_ajax_nopriv_checkout_menu_popup', 'checkout_popup_menu_function');
function checkout_popup_menu_function()
  {
	  global $woocommerce;
		    echo '<i class="fa fa-shopping-cart" aria-hidden="true"><sup>'.
			sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ).
			'</sup></i> <bdi> - </bdi>'. 
			WC()->cart->get_cart_total();
			
    die();
} 
 
add_filter('woocommerce_order_button_text','replace_order_button_text',1);
function replace_order_button_text($order_button_text) {
	
	$order_button_text = 'Buy Now';
	
	return $order_button_text;
}

add_action('woocommerce_checkout_process', 'billing_address_rule_custom');
function billing_address_rule_custom() {
	
	$country = WC()->customer->get_shipping_country();
	
	 if ( $_POST['billing_address_1'] )
	 {
	   	$address = urlencode($_POST['billing_address_1']);
		$region = $country;
		$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response);	
        if ($response_a->status != 'OK')
		{
			 wc_add_notice( __( 'Please check Your Address' ), 'error' );
		}			
	 }


   // return $address_fields;
}
	 
 add_action('wp_ajax_shipping_rate_country_by_postid', 'wc_new_shipping_method_by_country');
 add_action("wp_ajax_nopriv_shipping_rate_country_by_postid", 'wc_new_shipping_method_by_country'); 

function wc_new_product_video_method(){
	ob_start(); 
    require_once 'includes/class-more-country-shipping.php';	
     header("Content-Type: application/json");
	 
	  
	  $fdsabdsss = WC_MORE_SHIPPING_COUNTRY::product_upload_video();
	  
   ob_end_clean();
		echo json_encode($fdsabdsss);
		
		
	   	exit;
     }
 add_action('wp_ajax_product_video_method', 'wc_new_product_video_method');
 add_action("wp_ajax_nopriv_product_video_method", 'wc_new_product_video_method'); 