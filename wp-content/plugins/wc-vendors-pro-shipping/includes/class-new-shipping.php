<?php class WC_NEW_SHIPPING_METHODS extends WC_Shipping_Method{
 
    public function __construct(){
        $this->id = 'wc_new_shipping_method';
        $this->method_title = __( 'Shipping Pro', 'woocommerce' );
 
        // Load the settings.
        $this->init_form_fields();    // For creating necessary fields for shipping setting page
        $this->init_settings();
 
 
        // Define user set variables
        $this->enabled   = $this->get_option( 'enabled' );
        $this->title         = $this->get_option( 'title' );
       
       
        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) ); // Updating Shipping setting fields values
 
        add_filter( 'woocommerce_package_rates', array($this,'wc_hide_shipping_when_is_available'), 100 );  // this will hide other shipping methods if our shipping method is available
    }
 
 
    /**
     * Hide shipping rates when free shipping is available.
     * Updated to support WooCommerce 2.6 Shipping Zones.
     *
     * @param array $rates Array of rates found for the package.
     * @return array
     */
    function wc_hide_shipping_when_is_available( $rates ) {
        $new_product = array();
        foreach ( $rates as $rate_id => $rate ) {
            if ( 'wc_new_shipping_method' === $rate->method_id ) {
                $new_product[ $rate_id ] = $rate;
                break;
            }
 
        } 
        return ! empty( $new_product ) ? $new_product : $rates;
    }
 
    public function init_form_fields(){
        $this->form_fields = array(
            'enabled' => array(
              'title'       => __( 'Enable/Disable', 'woocommerce' ),
              'type'            => 'checkbox',
              'label'       => __( 'Enable New Shipping for WooCommerce products', 'woocommerce' ),
              'default'         => 'yes'
            ),
            'title' => array(
              'title'       => __( 'WooCommerce New Shipping', 'woocommerce' ),
              'type'            => 'text',
              'description'     => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
              'default'     => __( 'Shipping Cost', 'woocommerce' ),
               
            )
        );
    }
 
 
    public function calculate_shipping($package=array()){ 
           
          // This is where you'll add your rates
            $country = WC()->customer->get_shipping_country();
		   $city = WC()->customer->get_shipping_city();
		   $postcode = WC()->customer->get_shipping_postcode();
		   $shi_address = WC()->customer->get_shipping_address();
		   $postcode = WC()->customer->get_shipping_postcode();
		   $state = WC()->customer->get_shipping_state();
		   
		   foreach( WC()->cart->get_cart() as $cart_item ){
                  $product_id = $cart_item['product_id'];
            }
		 $shipping_details = get_post_meta( $product_id, '_wcv_shipping_rates',  true );	
		// $shipping_rate = ( is_array( $shipping_details ) && array_key_exists( 'fee', $shipping_details ) ) ? $shipping_details[ 'fee' ] : ''; 
		$rates = 0;
		if( !empty( $shipping_details ))
		{
			foreach ( $shipping_details as $shipping_rate ) {
			 if($country == $shipping_rate['country'] & $state == $shipping_rate['state'] )
			  {
				$rates = $shipping_rate['fee'];
			  }else
			  {
				 if($country == $shipping_rate['country'])
			     {
				  $rates = $shipping_rate['fee'];
			     } 
			  }
			
			}
		}
          if( $country !='' & $city !='' &  $postcode !='' &  $shi_address !='' &  $state !='' )
			{ 
		
             $this->add_rate( array(
              'id'  => $this->id,
              'label' => $this->title,
			  //'label' => $product_id,
              'cost'    => $rates
             ));
             // This will add custom cost to shipping method 
	      }
    }
 
} ?>