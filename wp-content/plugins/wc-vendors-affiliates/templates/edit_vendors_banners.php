<?php if(is_user_logged_in()){ ?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">
<style>.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }.empty-attr{    background: #eaebec !important; font-weight: bold !important;}  tr.one-color-change td {  background: rgb(255, 248, 248);  }.wcvendors-table.wcvendors-table-shop_coupon.wcv-table.position{    margin-top: 10px !important;} .group-total{ padding-left: 8px !important; margin-left: 29px; border: 1px solid rgba(220, 217, 217, 0.52); padding: 8px;    background: white; border-radius: 4px;}  .all-100.group-total.latest{margin-top: 10px !important;}  .left-content,.right-content{  float: left;  width: 47%;  }.right-content{    margin-left: 42px;}  .right-content .all-100.group-total.latest:first-child {  margin-top: 0px !important;  } .banner-add{    padding-bottom: 11px;}   .affwp-creative{         padding-right: 12px; }  .affwp-creative p a img{     max-width: 50%; }

</style>
<style>
    .form-row td{ text-align: left;  }
    .form-row .description{padding-top: 7px; font-size: 13px;  }
    .form-table{width:97%}  .form-control{width: 40%;    height: 29px;    padding: 3px 12px;}
    .form-table{width:100%;margin: 0px !important;   margin-bottom: 10px !important;}
    @media screen and (max-width: 769px) {  .form-control{width: 100%;} #description{width:100%}  }
    .img-res{padding-top: 5px;}
</style>


<div class="wrap">

    <h2><?php _e( 'Edit  Banners', 'affiliate-wp' ); ?></h2>

    <form method="post"  enctype="multipart/form-data" class="bs-docs-example">

     <?php foreach($results as $result){ ?>
        <table class="form-table">

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="name"><?php _e( 'Name', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="text" name="pv_name" class="form-control" value="<?php if($result->name){echo $result->name;} ?>">
                    <p class="description"><?php _e( 'The name of this banner. For your identification only.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="name"><?php _e( 'Description', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <textarea name="pv_description" id="description" class="form-control" rows="8" ><?php if($result->description){ echo $result->description;} ?></textarea>
                    <p class="description"><?php _e( 'An optional description for this banner. This is displayed below the banner for affiliates.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="url"><?php _e( 'URL', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="text" name="pv_url" class="form-control" value="<?php if($result->url){ echo $result->url;} ?>">
                    <p class="description"><?php _e( 'The banner link should take the person directly to the product you wish to sell. The affiliate&#8217;s referral ID will be automatically appended.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="text"><?php _e( 'Text', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="text" name="pv_text"  id="text" class="form-control"  maxlength="255"  value="<?php if($result->text){echo $result->text;} ?>" >
                    <p class="description"><?php _e( 'Text for this banner.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="image"><?php _e( 'Image', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="file" name="fileToUpload" placeholder="File" class="form-control" value="<?php if($result->file){ echo $result->image; } ?>" />
                    <?php if($result->image){ ?> <img class="img-res" src="<?php echo $result->image; ?>"> <?php } ?>
                    <p class="description"><?php _e( 'Select an image if you would like an image banner. You can also enter an image URL if your image is hosted elsewhere.', 'affiliate-wp' ); ?></p>

                    <div id="preview_image"></div>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="image"><?php _e( 'Rate', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="number" min="0" max="100" name="banner_rate" id="pv_shop_affiliates_rate" class="form-control" placeholder="Rate%" value="<?php echo $result->rate; ?>">
                    <p class="description"><?php _e( 'Rate for this banner.', 'affiliate-wp' ); ?></p>
                    <div id="rate"></div>
                </td>

            </tr>



            <tr class="form-row form-required">

                <th scope="row">
                    <label for="status"><?php _e( 'Status', 'affiliate-wp' ); ?></label>
                </th>

                <td>

                    <select name="pv_status"  id="status" class="form-control" >
                        <option value="Active" <?php if($result->status && $result->status=='Active' ){echo 'selected';} ?> >Active</option>
                        <option value="Inactive" <?php if($result->status && $result->status=='Inactive' ){echo 'selected';} ?>> Inactive</option>
                    </select>

                    <p class="description"><?php _e( 'Select the status of the banner.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>


        </table>
       <?php } ?>
        <input type="hidden" name="creative_id" value="<?php if($result->creative_id){ echo $result->creative_id; } ?>">
        <input type="submit" name="edit_submit_banner" id="pv_store_affiliates_submit" class="btn btn-success" value="<?php _e( 'Edit Banners', 'wcvendors_affiliates' ); ?>">

    </form>

</div>
<script>
    $('.wcv-navigation ul li:first-child').removeClass('active');
    $('#dashboard-menu-item-vendor_aff_list').addClass('active');
</script>

<?php } ?>
