<style>
.form-row td{ text-align: left;  }
.form-row .description{padding-top: 7px; font-size: 13px;  }
.form-table{width:97%}  .form-control{width: 40%;    height: 29px;    padding: 3px 12px;}
.form-table{width:100%;margin: 0px !important;   margin-bottom: 10px !important;}
@media screen and (max-width: 769px) {  .form-control{width: 100%;} #description{width:100%}  }

</style>

<div class="wrap">

    <h2><?php _e( 'New Banners', 'affiliate-wp' ); ?></h2>

    <form method="post"  enctype="multipart/form-data" class="bs-docs-example">

        <p><?php printf( __( 'Use this screen to add a new banner, such as a text link or image banner.', 'affiliate-wp' ), admin_url( 'user-new.php' ) ); ?></p>

        <table class="form-table">

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="name"><?php _e( 'Name', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="text" name="pv_name" class="form-control" >
                    <p class="description"><?php _e( 'The name of this banner. For your identification only.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="name"><?php _e( 'Description', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <textarea name="pv_description" id="description" class="form-control" rows="8" ></textarea>
                    <p class="description"><?php _e( 'An optional description for this banner. This is displayed below the banner for affiliates.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="url"><?php _e( 'URL', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="text" name="pv_url" class="form-control" >
                    <p class="description"><?php _e( 'The banner link should take the person directly to the product you wish to sell. The affiliate&#8217;s referral ID will be automatically appended.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="text"><?php _e( 'Text', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="text" name="pv_text"  id="text" class="form-control"  maxlength="255" />
                    <p class="description"><?php _e( 'Text for this banner.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="image"><?php _e( 'Image', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input type="file" name="fileToUpload" placeholder="File" class="form-control" />
                    <p class="description"><?php _e( 'Select an image if you would like an image banner. You can also enter an image URL if your image is hosted elsewhere.', 'affiliate-wp' ); ?></p>

                    <div id="preview_image"></div>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="status"><?php _e( 'Status', 'affiliate-wp' ); ?></label>
                </th>

                <td>

                    <select name="pv_status"  id="status" class="form-control">
                        <option value="Active">Active</option>
                        <option value="Inactive"> Inactive</option>
                    </select>

                    <p class="description"><?php _e( 'Select the status of the banner.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>


        </table>


        <input type="submit" name="submit_banner" id="pv_store_affiliates_submit" class="btn btn-success" value="<?php _e( 'Create Banners', 'wcvendors_affiliates' ); ?>">

    </form>

</div>
