<?php if(is_user_logged_in()){ ?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">
<style>.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }.empty-attr{    background: #eaebec !important; font-weight: bold !important;}  tr.one-color-change td {  background: rgb(255, 248, 248);  }.wcvendors-table.wcvendors-table-shop_coupon.wcv-table.position{    margin-top: 10px !important;} .group-total{ padding-left: 8px !important; margin-left: 29px; border: 1px solid rgba(220, 217, 217, 0.52); padding: 8px;    background: white; border-radius: 4px;}  .all-100.group-total.latest{margin-top: 10px !important;}  .left-content,.right-content{  float: left;  width: 47%;  }.right-content{    margin-left: 42px;}  .right-content .all-100.group-total.latest:first-child {  margin-top: 0px !important;  } .banner-add{    padding-bottom: 11px;}   .affwp-creative{         padding-right: 12px; }  .affwp-creative p a img{     max-width: 50%; }

</style>
<style>
    .form-row td{ text-align: left;  }
    .form-row .description{padding-top: 7px; font-size: 13px;  }
    .form-table{width:97%}  .form-control{width: 40%;    height: 29px;    padding: 3px 12px;}
    .form-table{width:100%;margin: 0px !important;   margin-bottom: 10px !important;}
    @media screen and (max-width: 769px) {  .form-control{width: 100%;} #description{width:100%}  }
</style>

<div class="wrap">

    <h2><?php _e( 'New Banners', 'affiliate-wp' ); ?></h2>

    <form method="post"  enctype="multipart/form-data" class="bs-docs-example">

        <p><?php printf( __( 'Use this screen to add a new banner, such as a text link or image banner.', 'affiliate-wp' ), admin_url( 'user-new.php' ) ); ?></p>

        <table class="form-table">

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="name"><?php _e( 'Name', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input required type="text" name="pv_name" class="form-control" >
                    <p class="description"><?php _e( 'The name of this banner. For your identification only.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="name"><?php _e( 'Description', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <textarea required name="pv_description" id="description" class="form-control" rows="8" ></textarea>
                    <p class="description"><?php _e( 'An optional description for this banner. This is displayed below the banner for affiliates.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>
            <tr>
			   <th scope="row">
                    <label for="image"><?php _e( 'Affiliate Type', 'affiliate-wp' ); ?></label>
                </th>
				<td><select onchange="affitype(this.value)" class="form-control" required  name="typevalue">
				<option value="product">Products</option>
				<option value="ads" >Ads banner or videos</option>
				<option value="zeroads">Sign up and see zero Ads</option>
				</select></td>
			</tr>
            <tr class="form-row form-required" id="urlfind">

                <th scope="row">
                    <label for="url"><?php _e( 'URL', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input  type="text" name="pv_url" class="form-control" >
                    <p class="description"><?php _e( 'The banner link should take the person directly to the product you wish to sell. The affiliate&#8217;s referral ID will be automatically appended.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="text"><?php _e( 'Text', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input required type="text" name="pv_text"  id="text" class="form-control"  maxlength="255" />
                    <p class="description"><?php _e( 'Text for this banner.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="image"><?php _e( 'Image', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input required type="file" name="fileToUpload"  id="fileToUpload" placeholder="File" class="form-control" />
                    <p class="description"><?php _e( 'Select an image if you would like an image banner. You can also enter an image URL if your image is hosted elsewhere.', 'affiliate-wp' ); ?></p>

                    <div id="preview_image"></div>
					<img src="" id="preview_imgnew" style="width: 300px; height: 250px;" />
                </td>

            </tr>
			
			<tr>
			   <th scope="row">
                    <label for="image"><?php _e( 'Banners size', 'affiliate-wp' ); ?></label>
                </th>
				<td><select onchange="bannersizechose(this.value)" class="form-control" required name="bannersize">
				<option value="0">300x250 px</option>
				<option value="1" >728x90</option>
				<option value="2" >300x600</option>
				<option value="3" >468x60</option>
				<option value="4" >160x600</option>
				<option value="5">Custom</option>
				</select></td>
			</tr>
			<tr id="showcustomerbanner" style="display:none">
			   <th scope="row">
                    <label for="image"><?php _e( 'Custom Banners size', 'affiliate-wp' ); ?></label>
                </th>
				<td> <input required type="number" min="10" name="cbannersizew" id="cbannersizeww" placeholder="width" id="text" class="form-control" style="float: left; margin-right: 10px;" /><input type="number" required min="10" id="cbannersizewh" name="cbannersizeh" placeholder="height" id="text" class="form-control"  /></td>
			</tr>

            <tr class="form-row form-required">

                <th scope="row">
                    <label for="image"><?php _e( 'Rate', 'affiliate-wp' ); ?></label>
                </th>

                <td>
                    <input required type="number" min="0" max="100" name="banner_rate" id="pv_shop_affiliates_rate" class="form-control" placeholder="Rate%" >
                    <p class="description"><?php _e( 'Rate for this banner.', 'affiliate-wp' ); ?></p>
                    <div id="rate"></div>
                </td>

            </tr>



            <tr class="form-row form-required">

                <th scope="row">
                    <label for="status"><?php _e( 'Status', 'affiliate-wp' ); ?></label>
                </th>

                <td>

                    <select name="pv_status"  id="status" class="form-control">
                        <option value="Active">Active</option>
                        <option value="Inactive"> Inactive</option>
                    </select>

                    <p class="description"><?php _e( 'Select the status of the banner.', 'affiliate-wp' ); ?></p>
                </td>

            </tr>


        </table>


        <input type="submit" name="submit_banner" id="pv_store_affiliates_submit" class="btn btn-success" value="<?php _e( 'Create Banners', 'wcvendors_affiliates' ); ?>">

    </form>

</div>


<script>
    $('.wcv-navigation ul li:first-child').removeClass('active');
    $('#dashboard-menu-item-vendor_aff_list').addClass('active');
	
	 function bannersizechose(sizevalue)
		  {
			  if(sizevalue == 5)
			  {
				  var height = $('#cbannersizewh').val();
				  var width = $('#cbannersizewh').val();
				  $('#showcustomerbanner').fadeIn();
				  
				  $("#preview_imgnew").css("width", width);
                  $("#preview_imgnew").css("height", height);
				  
			  }else{
				  $('#showcustomerbanner').fadeOut(); 
			  }
			  if(sizevalue == 0)
			  {
				 // $('#preview_imgnew').attr();
				  $("#preview_imgnew").css("width", 300);
                  $("#preview_imgnew").css("height", 250);
			  }
			  if(sizevalue == 1)
			  {
				 // $('#preview_imgnew').attr();
				  $("#preview_imgnew").css("width", 728);
                  $("#preview_imgnew").css("height", 90);
			  }
			  if(sizevalue == 2)
			  {
				 // $('#preview_imgnew').attr();
				  $("#preview_imgnew").css("width", 300);
                  $("#preview_imgnew").css("height", 600);
			  }
			  if(sizevalue == 3)
			  {
				 // $('#preview_imgnew').attr();
				  $("#preview_imgnew").css("width", 468);
                  $("#preview_imgnew").css("height", 60);
			  }
			  if(sizevalue == 4)
			  {
				 // $('#preview_imgnew').attr();
				  $("#preview_imgnew").css("width", 160);
                  $("#preview_imgnew").css("height", 160);
			  }
		  }
		  
		  function affitype(typeaff)
		  {
			  var ratet = '';
			  if(typeaff == 'product')
			  {
				  $('#urlfind').fadeIn();
				   $('#pv_shop_affiliates_rate').val('');
				   $('#pv_shop_affiliates_rate').prop("readonly",false);
			  }
			  else{
				  $('#urlfind').fadeOut(); 
				  if(typeaff == 'ads')
				  {
					  ratet = 20;
				  }else {
					ratet = 50;  
				  }
				  
				  $('#pv_shop_affiliates_rate').val( ratet );
				  $('#pv_shop_affiliates_rate').attr('readonly','readonly');
			  }
		  }
		  
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#preview_imgnew').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#fileToUpload").change(function(){
        readURL(this);
    });
</script>


<?php } ?>

