
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">
<style>.page-template-default .entry table {  margin: 0px !important; }.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }.empty-attr{    background: #eaebec !important; font-weight: bold !important;}
    tr.one-color-change td {  background: rgb(255, 248, 248);  }.wcvendors-table.wcvendors-table-shop_coupon.wcv-table.position{    margin-top: 10px !important;} .group-total{ padding-left: 8px !important; margin-left: 29px; border: 1px solid rgba(220, 217, 217, 0.52); padding: 8px;    background: white; border-radius: 4px;}  .all-100.group-total.latest{margin-top: 10px !important;}  .left-content,.right-content{  float: left;  width: 47%;  }.right-content{    margin-left: 42px;}  .right-content .all-100.group-total.latest:first-child {  margin-top: 0px !important;  }
.banner-add{    padding-bottom: 11px;}
</style>

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active overview"><a href="#overview" aria-controls="creatives" role="tab" class="reviewtab " data-toggle="tab">Overview</a> </li>
    <li role="presentation" class=""><a href="#affiliates" class="affiliates" aria-controls="home" role="tab" data-toggle="tab">Affiliates</a></li>
    <li role="presentation" class=""><a href="#referrals" class="referrals" aria-controls="forum" role="tab" data-toggle="tab">Referrals</a> </li>
    <li role="presentation" class=""><a href="#visits" aria-controls="reviews" role="tab" class="reviewtab" data-toggle="tab">Visits</a> </li>
    <li role="presentation" class=""><a href="#payouts" aria-controls="payouts" role="tab" class="reviewtab" data-toggle="tab">Payouts</a> </li>
    <li role="presentation" class=" creatives"><a href="#creatives" aria-controls="creatives" role="tab" class="reviewtab" data-toggle="tab">Banner Upload</a> </li>
</ul>


<div class="tab-content">
    <div class="tab-pane" role="tabpanel" id="affiliates">
        <div class="wcv-cols-group wcv-horizontal-gutters">
            <div class="all-100">

                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($affilates_row as $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($affilates){
                        foreach($affilates as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo  $key->affiliate_id ;?></td>
                                <td><?php echo  $key->user_nicename ;?></td>
                                <td><?php echo  $key->earnings ;?></td>
                                <td><?php echo  $key->rate ;?></td>
                                <td><?php echo  $key->referrals ;?></td>
                                <td><?php echo  $key->visits ;?></td>
                                <td><?php echo  $key->aff_status ;?></td>
                                <td><?php echo $key->user_email ;?></td>
                                <!--  <td><?php /*echo $key->added_date ;*/?></td>-->
                            </tr>
                        <?php }
                    }else{
                        echo ' <tr><td class="empty-attr">You have not affilates yet.</td> </tr> ';
                    }  ?>

                    </tbody>
                </table>


            </div>
        </div>
    </div>

    <div class="tab-pane" role="tabpanel" id="referrals">
        <div class="wcv-cols-group wcv-horizontal-gutters">
            <div class="all-100">

                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($referrals_row as $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($referrals){
                        foreach($referrals as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->amount ;?></td>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->reference ;?></td>
                                <td><?php echo $key->description ;?></td>
                                <td><?php echo $key->date ;?></td>
                                <td><?php echo $key->status ;?></td>
                            </tr>
                        <?php }
                    }else{
                        echo '<tr><td class="empty-attr">You have not referrals yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <div class="tab-pane" role="tabpanel" id="visits">
        <div class="wcv-cols-group wcv-horizontal-gutters">
            <div class="all-100">

                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($visits_row as $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($visits){
                        foreach($visits as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->url ;?></td>
                                <td><?php echo $key->referring_url ;?>  </td>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->referral_id ;?></td>
                                <td><?php if( $key->converted){echo '<span class="visit-converted yes"><i></i></span>' ;}else{echo '<span class="visit-converted no"><i></i></span>';}?>  </td>
                                <td><?php echo $key->date ;?></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not visits yet.</td> </tr>';
                    }  ?>

                    </tbody>
                </table>


            </div>
        </div>
    </div>
    <div class="tab-pane" role="tabpanel" id="payouts">
        <div class="wcv-cols-group wcv-horizontal-gutters">
            <div class="all-100">

                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($payouts_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($payouts){
                        foreach($payouts as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->payout_id ;?></td>
                                <td><?php echo $key->amount ;?></td>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->referrals ;?></td>
                                <td><?php echo $key->payout_method ;?></td>
                                <td><?php echo $key->status ;?></td>
                                <td><?php echo $key->date;?></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not visits yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>


            </div>
        </div>
    </div>

    <div class="tab-pane" role="tabpanel" id="creatives">
        <div class="wcv-cols-group wcv-horizontal-gutters">


            <div class="all-50 banner-add">
                <a  class="wcv-button button  " href="<?php echo site_url().'/vendors_banners_add/';?>">Add Banner</a>
            </div>

            <div class="all-100">

                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($creatives_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($creatives){
                        foreach($creatives as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->name ;?></td>
                                <td><a target="_blank" href="<?php echo $key->url ;?>">View Url</a></td>
                                <td><?php echo '[affiliate_creative id="'.$key->creative_id.'"]' ;?></td>
                                <td><?php echo $key->status ;?></td>
                                <td><a target="_blank" href="<?php echo $key->image ;?>">View Banner</a></td>
                                <td><?php if($key->rate){echo $key->rate.'%' ;}else{echo '0%';}?></td>
                                <td><?php echo $key->date;?></td>
                                <td><a href="<?php echo site_url();?>/vendors_banners_edit/?id=<?php echo $key->creative_id;?>">Edit</a></td>
                                <td><a style="color:red" href="<?php echo site_url();?>/vendor_aff_list/?del=<?php echo $key->creative_id;?>">Delete</a></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not banners yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <div class="tab-pane active" role="tabpanel" id="overview">
        <div class="wcv-cols-group wcv-horizontal-gutters">
        <div class="left-content">
            <div class="all-100 group-total">
                <h1>Totals</h1>
                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($overview_peid_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($overview_total){
                        foreach($overview_total as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->paid_earnings ;?></td>
                                <td><?php echo $key->paid_earnings_this_month;?></td>
                                <td><?php echo $key->paid_earnings_today ;?></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not total paid earnings yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>
            </div>

            <div class="all-100 group-total latest">
                <h1>Latest Affiliate Registrations</h1>
                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table position">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($latest_affilate_registration_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($latest_affilate_registration){
                        foreach($latest_affilate_registration as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->status ;?></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not latest affiliate registrations yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>
            </div>
            </div>


            <div class="right-content">
            <div class="all-100 group-total latest">
                <h1>Most Valuable Affilates</h1>
                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table position">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($most_valiuable_affilates_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($most_valiuable_affilates){
                        foreach($most_valiuable_affilates as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->paid_earnings ;?></td>
                                <td><?php echo $key->payout_referrals ;?></td>
                                <td><?php echo $key->visit ;?></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not most valuable affilates yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>
            </div>


            <div class="all-100 group-total latest">
                <h1>Recent Referrals</h1>
                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table position">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($recent_referrals_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($recent_referrals){
                        foreach($recent_referrals as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->amount ;?></td>
                                <td><?php echo $key->description ;?></td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not recent referrals yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>
            </div>


            <div class="all-100 group-total latest">
                <h1>Recent Referrals Visits</h1>
                <table role="grid" class="wcvendors-table wcvendors-table-shop_coupon wcv-table position">

                    <!-- Output the table header -->
                    <thead>
                    <tr>
                        <!-- TEST -->
                        <?php foreach($recent_referrals_visits_row as  $key){ ?>
                            <th><?php echo $key; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($recent_referrals_visits){
                        foreach($recent_referrals_visits as $val => $key){
                            ?>
                            <tr <?php if( $val % 2 == 0){echo 'class="two-color-change"';}else{echo 'class="one-color-change"'; } ?>>
                                <td><?php echo $key->user_login ;?></td>
                                <td><?php echo $key->url ;?></td>
                                <td><?php if( $key->converted){echo '<span class="visit-converted yes"><i></i></span>' ;}else{echo '<span class="visit-converted no"><i></i></span>';}?>  </td>
                            </tr>
                        <?php }
                    } else{
                        echo '<tr><td class="empty-attr">You have not referrals Visits yet.</td> </tr>';
                    } ?>

                    </tbody>
                </table>
            </div>
            </div>

        </div>
    </div>
</div>
<?php  if(isset($_GET['active']) && $_GET['active'] && $_GET['active']=='banners'){?>
<script>
    $('.nav.nav-tabs .overview').removeClass('active');
    $('div #overview').removeClass('active');
    $('.nav.nav-tabs .creatives').addClass('active');
    $('div #creatives').addClass('active');

</script>
<?php } ?>

<script>
    $('.wcv-navigation ul li:first-child').removeClass('active');
    $('#dashboard-menu-item-vendor_aff_list').addClass('active');
</script>

