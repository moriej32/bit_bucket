<div class="pv_shop_name_container">
	<p><b><?php _e( 'Affiliates rate', 'wcvendors_affiliates' ); ?>%</b><br/>
		<?php _e( 'Your rate % for your affiliates', 'wcvendors_affiliates' ); ?><br/>

		<input type="number" min="0" max="100" name="pv_shop_affiliates_rate" id="pv_shop_affiliates_rate" placeholder="<?php _e( 'Affiliates rate', 'wcvendors_affiliates' ); ?>%"
			   value="<?php echo abs(doubleval(get_user_meta( $user_id, 'pv_shop_affiliates_rate', true ))); ?>"/>
	</p>
</div>
