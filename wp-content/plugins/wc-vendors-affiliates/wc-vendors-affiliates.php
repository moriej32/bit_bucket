<?php
/*
 Plugin Name: WC Vendors Affiliates
 Description: WC Vendors Affiliates
 Version: 1.0.0
 Requires at least: 4.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( !defined( 'wcv_a_plugin_dir' ) ) 		define( 'wcv_a_plugin_dir', trailingslashit( dirname( __FILE__ ) ) . '/' );
register_activation_hook( __FILE__, array( 'WC_Vendors_Affiliates', 'install' ) );
register_uninstall_hook( __FILE__, array( 'WC_Vendors_Affiliates', 'uninstall' ) );

class WC_Vendors_Affiliates
{

    public static function getTableName()
    {
        global $wpdb;
        return $wpdb->prefix . 'vendor_affiliates';
    }

    public function __construct()
    {
        add_action( 'wcvendors_settings_after_shop_name', array( $this, 'add_affiliates_rate_input' ) );
        add_action( 'wcvendors_shop_settings_saved', array( $this, 'save_affiliates_rate_settings' ) );
        add_action( 'wcv_pro_store_settings_saved', array( $this, 'save_affiliates_rate_settings' ) );
        add_action( 'affwp_register_user', array($this, 'add_registered_affiliate_to_vendor'), 10, 3);
        add_filter( 'affwp_calc_referral_amount', array($this, 'get_affiliate_rate'), 90, 5);
        add_filter( 'wcv_commission_rate', array($this, 'pull_affiliate_commission_from_vendor'), 90, 6);
        add_filter( 'wcv_order_table_columns', array($this, 'wcv_order_table_columns'), 90);
        add_filter( 'wcv_orders_table_rows', array($this, 'wcv_orders_table_rows'), 90);
        add_filter( 'manage_shop_order_posts_columns', array( $this, 'shop_order_columns' ), 90);
        add_action( 'manage_shop_order_posts_custom_column', array( $this, 'render_shop_order_columns' ), 2 );
        add_action( 'get_template_vendor_affilate_list', array( $this, 'get_template_vendor_affilate_list' ));
        add_action( 'affwp_affiliate_dashboard_tabs', array( $this, 'add_vendors_menu_to_affiliates' ));
        add_filter( 'wcv_pro_dashboard_urls',array( $this, 'wcv_pro_dashboard_urls_affilate' ),1);
        add_filter( 'affwp_affiliate_area_tabs', array( $this, 'affwp_affiliate_area_tabs' ) );
        add_filter( 'affwp_affiliate_area_tabs', array( $this, 'affwp_affiliate_area_tabs_creative' ) );
        add_action( 'vendors_list_as_affilates',array( $this, 'vendors_list_as_affilates') );
        add_action( 'add_creative_affilate_area',array( $this, 'add_creative_affilate_area') );
        add_filter( 'affwp_affiliate_area_page_url',array( $this, 'affwp_affiliate_area_page_url' ),10,3);
        add_action( 'add_uplode_backgrounds',array( $this, 'add_uplode_backgrounds') );
        add_action( 'get_affiliate_ref_add_banners',array( $this, 'get_affiliate_ref_add_banners') );
        add_action( 'become_affiliate', array( $this, 'generate_become_affiliate_button' ));
        add_action( 'init', array( $this, 'submit_become_affiliate_button' ));
        add_action( 'init', array( $this, 'submit_creative_affiliate_button' ));
        add_action( 'init', array( $this, 'become_affiliate_unsubmit' ));
        add_action( 'init', array( $this, 'delete_vendor_banners' ));
        add_action( 'init', array( $this, 'submit_creative_edit_affiliate_button' ));


        add_shortcode( 'vendor_aff_list',  array($this , 'get_template_vendor_affilate_list'));
        add_shortcode( 'downloads',  array($this , 'get_download'));
        add_shortcode( 'addresses',  array($this , 'get_addresses'));
        add_shortcode( 'edit_address_billing',  array($this , 'get_edit_address_billing'));
        add_shortcode( 'edit_address_shipping',  array($this , 'get_edit_address_shipping'));
        add_shortcode( 'vendors_banners',  array($this , 'add_banners_as_vendor'));
        add_shortcode( 'edit_vendors_banners',  array($this , 'edit_vendors_banners'));
    }





    public function submit_creative_affiliate_button() {
        global $wpdb;
        $user_id = get_current_user_id();
		$bannersizewidth = '';
		$bannersizeheight = '';
		 
        if($user_id){
            if (isset($_POST['submit_banner']) && $_POST['submit_banner']) {
				
				 if(isset($_POST['bannersize']) && !empty($_POST['bannersize'])) {
					 $bannersizepost = $_POST['bannersize'];
					 if( $bannersizepost == 0)
					 {
						 $bannersizewidth = 300;
						 $bannersizeheight = 250;
					 }
					 elseif( $bannersizepost == 1)
					 {
						 $bannersizewidth = 728;
						 $bannersizeheight = 90;
					 }
					  elseif( $bannersizepost == 2)
					 {
						 $bannersizewidth = 300;
						 $bannersizeheight = 600;
					 }
					  elseif( $bannersizepost == 3)
					 {
						 $bannersizewidth = 468;
						 $bannersizeheight = 60;
					 }
					  elseif( $bannersizepost == 4)
					 {
						 $bannersizewidth = 160;
						 $bannersizeheight = 160;
					 }
					 elseif( $bannersizepost == 5)
					 {
						 $bannersizewidth = $_POST['cbannersizew'];
						 $bannersizeheight = $_POST['cbannersizeh'];
					 }
				 }

                if ($_FILES['fileToUpload'] && isset($_FILES['fileToUpload'])) {
                    if (($_FILES['fileToUpload']['type'] == 'image/jpg')
                        || ($_FILES['fileToUpload']['type'] == 'image/jpeg')
                        || ($_FILES['fileToUpload']['type'] == 'image/png')
                        || ($_FILES['fileToUpload']['type'] == 'image/gif')
                    ) {
                        $file = 'wp-content/uploads/' . rand($user_id, 99999999) . $_FILES['fileToUpload']['name'];
                        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $file);
                       $file = site_url() .'/'. $file;
					  
                       $resizeObj = new resizenewimage( $file );
                       $resizeObj -> resizeImage($bannersizewidth, $bannersizeheight, 0);
                       $file = site_url() .'/'. $resizeObj -> saveImage('wp-content/uploads/' . rand($user_id, 99999999) . $_FILES['fileToUpload']['name'], 100);
					   
                    }
                }

                if(isset($_POST['pv_name']) && !empty($_POST['pv_name'])) {
                    $banner_tit = $_POST['pv_name'];
                } else {
                    $banner_tit = 'Banner';
                }

                if(isset($_POST['pv_description']) && !empty($_POST['pv_description'])) {
                    $banner_descr = $_POST['pv_description'];
                } else {
                    $banner_descr = '';
                }

                if(isset($_POST['pv_url']) && !empty($_POST['pv_url'])) {
                    if (preg_match("/(http|https):\/\/(.*?)$/i", $_POST['pv_url'], $matches) > 0) {
                        $banner_url= $matches[0];
                    }else{
                        $banner_url=site_url();
                    }

                } else {
                    $banner_url = site_url();
                }
				
                if(isset($_POST['typevalue']) && !empty($_POST['typevalue'])) {
                    if($_POST['typevalue'] == 'ads')
					{
					 $banner_url = 'http://www.doityourselfnation.org/bit_bucket/banner-ads/';	
					}
					
					if($_POST['typevalue'] == 'zeroads')
					{
					 $banner_url = 'http://www.doityourselfnation.org/bit_bucket/advertisement-free';	
					}
                }
				
                if(isset($_POST['pv_text']) && !empty($_POST['pv_text'])) {
                    $banner_text = $_POST['pv_text'];
                } else {
                    $banner_text = 'Do It Yourself Nation';
                }


                if(isset($_POST['pv_status']) && !empty($_POST['pv_status']) ) {
                    if($_POST['pv_status'] =='Active') {
                        $banner_status = 'Active';
                    } elseif($_POST['pv_status'] =='Inactive')  {
                        $banner_status = 'Inactive';
                    }else{
                        $banner_status = 'Inactive';
                    }
                } else {
                    $banner_status = 'Active';
                }
                $date=date('Y-m-d h:i:s');
                if(isset($_POST['banner_rate']) && !empty($_POST['banner_rate']) ) {
                $rate = abs(doubleval($_POST[ 'banner_rate' ]));
                if($rate && $rate > 100) {
                    $rate = 0;
                }
                }

                $insert=$wpdb->insert(
                    'wp_affiliate_wp_creatives',
                     array(
                        'name' => $banner_tit,
                        'description' => $banner_descr,
                        'url' => $banner_url ,
                        'text' => $banner_text,
                        'image' => $file,
                        'status' => $banner_status,
                        'date' => $date,
                        'rate' => $rate,
                        'store_id' => $user_id
                    )
                );
                if($insert){
                    echo "<script>alert('Your banner was uploaded');</script>";
                    wp_redirect(site_url().'/vendor_aff_list/?active=banners');
                }
            }
        }
    }



    public function affwp_affiliate_area_page_url($affiliate_area_page_url, $affiliate_area_page_id, $tab){
        if($tab=='vendors'){
            $affiliate_area_page_url = add_query_arg( array( 'tab' => $tab ), $affiliate_area_page_url );
        }
        return $affiliate_area_page_url;
    }

    public function affwp_affiliate_area_tabs( $tabs ) {

        return array_merge( $tabs, array( 'vendors' ) );

    }

    public function affwp_affiliate_area_tabs_creative( $tabs ) {

        return array_merge( $tabs, array( 'add_creative' ) );

    }

    public function vendors_list_as_affilates(){

        global $wpdb;
        $affiliate_id = affwp_get_affiliate_id();

        if($affiliate_id) {
            $results = $wpdb->get_results( "SELECT *FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = s.vendor_id
            WHERE s.affiliate_id = ".$affiliate_id." " );
            $row=array(1=>'Vendor ID',2=>'Vendor Name',3=>"Vendor Email",4=>'Date');
            if($results) {
                wc_get_template( 'dashboard-tab-vendors.php', array(
                    'results'=>$results,
                    'row'=>$row,
                ), 'wc-vendors/dashboard/', wcv_a_plugin_dir . 'templates/' );

            }else{
                echo 'You don\'t have Vendors';
            }
        }
    }

    public function add_creative_affilate_area(){
        $user_id=get_current_user();
        if($user_id) {
            wc_get_template('dashboard-tab-add_creative.php', array(
                'user_id' => $user_id,
            ), 'wc-vendors/dashboard/', wcv_a_plugin_dir . 'templates/');
        }
    }




    public function add_vendors_menu_to_affiliates() {

        echo '<li class="affwp-affiliate-dashboard-tab ';
        if($this->affwp_affiliate_area_page_url('vendors')){echo 'active';}
        echo '">
				<a href="'.site_url().'/affiliate-area/?tab=vendors">Your Vendors</a>
			</li>';
    }



    public function wcv_pro_dashboard_urls_affilate($dashboard_pages){

        $dashboard_pages['vendor_affilate_list'] = array(
            'slug'			=> 'vendor_aff_list',
            'label'			=> __('Your Affilates', 'wcvendors-pro' ),
            'actions'		=> array(),
        );

        $dashboard_pages['affiliate-area'] = array(
            'slug'			=> 'affiliate-area',
            'label'			=> __('Other Affilates', 'wcvendors-pro' ),
            'actions'		=> array(),
        );

        return $dashboard_pages;
    }

    protected function get_views() {
        return array();
    }





    public function shop_order_columns($columns){
        $columns['total_without']      = __( 'Total without commissions', 'woocommerce' );
        $columns['order_total']      = __( 'Total', 'woocommerce' );
        $columns['affiliate_commission']    = __( 'Affiliate Commission' );
        $columns['system_commission']    = __( 'System Commission', 'woocommerce' );
        return $columns;
    }

    public function render_shop_order_columns($column){
        global $post, $woocommerce, $the_order,$wpdb;
        switch ( $column ) {
            case 'total_without' :
                echo $the_order->get_formatted_order_total();

                if ( $the_order->payment_method_title ) {
                    echo '<small class="meta">' . __( 'Via', 'woocommerce' ) . ' ' . esc_html( $the_order->payment_method_title ) . '</small>';
                }
                break;

            case 'affiliate_commission' :
              /* $order = wc_get_order( $post->ID );
                $items = $order->get_items();
                foreach ( $items as $item ) {
                    $order = new WC_Order($the_order->id);
                    $product_price = $order->get_total();
                    $item_id = $item['product_id'];
                    $rate = $this->get_affilate_rate($item_id);
                    $affiliate_commission = $product_price * ($rate / 100);
                    echo wc_price($affiliate_commission);
                }*/
				$affiliate_commission = $this->get_affilate_rate($the_order->id);
				echo $affiliate_commission;
				
				
                break;

            case 'order_total' :
                $commission = $wpdb->get_results( "
								SELECT * FROM {$wpdb->prefix}pv_commission
								WHERE     order_id = '" . $post->ID . "'
							" );
				$affiliate_commission = $this->get_affilate_rate($the_order->id);			
                foreach($commission as $row){
                    $total=woocommerce_price( $row->total_due + $row->total_shipping + $row->tax - $affiliate_commission );
                    echo $total;
                }
                break;

            case 'system_commission' :
                $order = wc_get_order( $post->ID );
                $product_price = $order->get_total();
                $commission = $wpdb->get_results( "
								SELECT * FROM {$wpdb->prefix}pv_commission
								WHERE     order_id = '" . $post->ID . "'
							" );
				 $total_due = 0;
                 $total_shipping = 0;	
                 $taxs = 0;				 
                foreach($commission as $row){
					//old code
                  //  $total=woocommerce_price( $row->total_due + $row->total_shipping + $row->tax );
                   // echo  wc_price($product_price - doubleval(strip_tags($total)) );
				   // new code
				    $total_due = $row->total_due;
					$total_shipping = $row->total_shipping;
					$taxs = $row->tax;
					
                }
				 $total = $total_due + $total_shipping + $taxs;
				
				echo  wc_price($product_price - doubleval(strip_tags($total)) );
				
                break;

        }
    }





    public function wcv_order_table_columns($columns)
    {
        $columns['total_without_commissions'] = 'Total without commissions';
        $columns['affiliate_commission'] = 'Affiliate Commission';
        $columns['system_commission'] = 'System Commission';
        return $columns;
    }

    public function wcv_orders_table_rows($rows)
    {
        global $woocommerce, $post;
        global $wpdb;
        foreach($rows as &$row) {
            $order = new WC_Order($row->ID);
            $total = $order->get_total();
            $row->total_without_commissions = wc_price($total);
			//old
           // $row->system_commission = wc_price($total - doubleval(strip_tags($row->total)) );
		   // new
		   $row->system_commission = wc_price($total*10/100);
			 
            $user_id = get_current_user_id();
			// new
			//$affiliate_commission = get_affilate_rate($row->ID);
			
          //if($wpdb->get_var("SELECT affiliate_id FROM {$wpdb->prefix}affiliate_wp_referrals WHERE reference={$row->ID}") ){
              // $rate = abs(doubleval(get_user_meta( $user_id, 'pv_shop_affiliates_rate', true )));
				// old
               $row->affiliate_commission = wc_price($total * ($rate / 100));
				//new
				//$row->affiliate_commission = $affiliate_commission;
				$affiliate_commission = $this->get_affilate_rate($row->ID);
				 $row->affiliate_commission = wc_price( $affiliate_commission );
				 $totalnow = $this->totla_com_nzm($row->ID);
				 $row->total =  $totalnow ;
           // }
        }
        return $rows;
    }
	
	
   //new by nazmin
   
   public function totla_com_nzm($orderid){
		  global $wpdb;
		  $commission = $wpdb->get_results( "
								SELECT * FROM {$wpdb->prefix}pv_commission
								WHERE     order_id = '" . $orderid . "'
							" );
				$affiliate_commission = $this->get_affilate_rate($orderid);			
                foreach($commission as $row){
                    $total= woocommerce_price( $row->total_due + $row->total_shipping + $row->tax - $affiliate_commission );
                
                }
			return 	 $total;
	 }
   
   
     /*public function system_commission_nz($orderid)
	 {
		 global $wpdb;
		 
	             $order = new WC_Order($orderid);
                $product_price = $order->get_total();
                $commission = $wpdb->get_results( "
								SELECT * FROM {$wpdb->prefix}pv_commission
								WHERE     order_id = '" . $orderid . "'
							" );
				 $total_due = 0;
                 $total_shipping = 0;	
                 $taxs = 0;				 
                foreach($commission as $row){
					 
				    $total_due = $row->total_due;
					$total_shipping = $row->total_shipping;
					$taxs = $row->tax;
					
                }
				 $total = $total_due + $total_shipping + $taxs;
				
				$finaltotal =  wc_price($product_price - doubleval(strip_tags($total)) );
				
	     return $finaltotal;			
	 }*/

    public function get_affilate_rate($order_id){
		
		//old code by arshak
		
       /* $_pf = new WC_Product_Factory();
        $product = $_pf->get_product($product_id);
        $user_id = $product->post->post_author;
        $rate = abs(doubleval(get_user_meta( $user_id, 'pv_shop_affiliates_rate', true )));
        if($rate > 100) {
            $rate = 100;
        }
        return $rate;*/
		
		//new code by nazmin
		
		global $wpdb;
		//$affilate_url = get_permalink($product_id);
		$amount_ref = $wpdb->get_var("SELECT amount FROM {$wpdb->prefix}affiliate_wp_referrals WHERE reference = $order_id ");
		
        return $amount_ref;
		
		
    }

    public function pull_affiliate_commission_from_vendor($commission, $product_id, $product_price, $order, $qty)
    {
        global $wpdb;
        if($wpdb->get_var("SELECT affiliate_id FROM {$wpdb->prefix}affiliate_wp_referrals WHERE reference={$order->id}") ) {

            $rate = $this->get_affilate_rate($product_id);
            $commission -= $product_price * ($rate / 100);
            $commission      = round( $commission, 2 );
        }
        return $commission;
    }

    public function get_affiliate_rate($referral_amount, $affiliate_id, $amount, $reference, $product_id)
    {
        $rate = $this->get_affilate_rate($product_id);
        //$type = affwp_get_affiliate_rate_type( $affiliate_id );
        $decimals = affwp_get_decimal_count();

        $referral_amount = round( $amount * $rate / 100, $decimals );
        if ( $referral_amount < 0 ){
            $referral_amount = 0;
        }
        return $referral_amount;
    }


    public static function install()
    {
        global $wpdb;
        $table_name = self::getTableName();
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id bigint NOT NULL AUTO_INCREMENT,
            vendor_id bigint NOT NULL,
            affiliate_id bigint(20) NOT NULL,
            added_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
	    )";
        $wpdb->query($sql);
    }

    public static function uninstall()
    {
        global $wpdb;
        $sql = "DROP TABLE IF EXISTS ".self::getTableName();
        $wpdb->query($sql);
    }


    public function add_affiliates_rate_input()
    {
        if ( !WCV_Vendor_Dashboard::can_view_vendor_page() ) {
            return false;
        }

        $user_id = get_current_user_id();

        wc_get_template( 'settings.php', array(
            'user_id'          => $user_id,
        ), 'wc-vendors/dashboard/settings/', wcv_a_plugin_dir . 'templates/settings/' );

    }

    public function save_affiliates_rate_settings($user_id) {
        //var_dump($user_id); die;
        if ( isset( $_POST[ 'pv_shop_affiliates_rate' ] ) ) {
            $rate = abs(doubleval($_POST[ 'pv_shop_affiliates_rate' ]));
            if($rate && $rate > 100) {
                $rate = 100;
            }
            update_user_meta( $user_id, 'pv_shop_affiliates_rate', $rate );
        }
    }

    public function is_vendor_affiliate($vendor_id){
        global $wpdb;
        $affiliate_id = affwp_get_affiliate_id();
        if($affiliate_id) {
            $user_count = $wpdb->get_var( "SELECT COUNT(*) FROM ".self::getTableName()." WHERE vendor_id=".$vendor_id." and affiliate_id=".$affiliate_id." " );
            //$sql = "SELECT *FROM ".self::getTableName()." WHERE vendor_id=".$vendor_id." and affiliate_id=".$affiliate_id." ";
            // $result=$wpdb->get_results($sql);
            if($user_count) {
                return true;
            }
        }
        return false;
    }







    public function generate_become_affiliate_button($vendor_id)
    {
        if($this->is_vendor_affiliate($vendor_id)) {
            global $wp;
            $current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            echo '<div class="container affiliate-link">
                  <a href="#" id="affiliate-referral-link" data-toggle="tooltip" title="You will receive '.abs(doubleval(get_user_meta( $vendor_id, 'pv_shop_affiliates_rate', true ))).'% from your referral payments">
                  Your referral link '.affwp_get_affiliate_referral_url( array( 'base_url' => $current_url )).'
                  </a>
                  </div>';
        } /*else {
            wc_get_template( 'become-affiliate.php', array(
                'vendor_id'          => $vendor_id,
            ), 'wc-vendors/dashboard/settings/', wcv_a_plugin_dir . 'templates/settings/' );
        }*/
    }

    public function add_user_as_affiliate_to_vendor($user_id, $vendor_id) {
        global $wpdb;
        $affiliate_id = affwp_get_affiliate_id();
        if(!$affiliate_id) {
            $affiliate_id = affwp_add_affiliate( array( 'user_id' => $user_id ) );
        }
        $wpdb->insert(
            self::getTableName(),
            array(
                'vendor_id' => $vendor_id,
                'affiliate_id' => $affiliate_id,
            )
        );
    }

    public function submit_become_affiliate_button() {
        if ( isset( $_POST[ 'submit' ] ) ) {
           /* $vendor_id = intval($_POST[ 'pv_become_vendor_id' ]);
            $user_id = get_current_user_id();*/
            $vendor_id = intval($_POST[ 'author_id' ]);
            $user_id = get_current_user_id();
            /* if(!WCV_Vendors::is_vendor( $vendor_id ) || $vendor_id == $user_id){
                die('Wrong Vendor');
            }*/

            if($this->is_vendor_affiliate($vendor_id)) {
                return wp_redirect($_SERVER['HTTP_REFERER'].'?store=recent');
            }

            if($user_id) {
                $this->add_user_as_affiliate_to_vendor($user_id, $vendor_id);
            } else {
                global $wp;
                return wp_redirect(site_url('affiliate-area?vendor='.$vendor_id.'&return=http://'."$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".''));
            }
            wp_redirect($_SERVER['HTTP_REFERER'].'?store=recent');
        }
    }



    public function become_affiliate_unsubmit() {
        if ( isset( $_POST[ 'unsubmit' ] )) {
            global $wpdb;
            $vendor_id = intval($_POST[ 'author_id' ]);
            $affiliate_id = affwp_get_affiliate_id();
            if(!empty($vendor_id) && !empty($affiliate_id) ) {
                if($this->is_vendor_affiliate($vendor_id)) {
                    $update = $wpdb->delete('wp_vendor_affiliates', array('affiliate_id' => $affiliate_id, 'vendor_id' => $vendor_id), array('%d'));
                }
            }
        }
    }


    public function add_registered_affiliate_to_vendor($affiliate_id, $status, $args){

        if(isset($_GET['vendor']) && $vendor_id = abs(intval($_GET['vendor']))) {
            if(!$this->is_vendor_affiliate($vendor_id)){
                $this->add_user_as_affiliate_to_vendor(get_current_user_id(), $vendor_id);
                if(isset($_GET['return'])) {
                    return wp_redirect($_GET['return']);
                }
            }
        }
    }


    public function get_download(){
        $user_id=get_current_user_id();
        if($user_id) {
            echo do_shortcode("[wcv_pro_dashboard_nav]");
            wc_get_template('myaccount/my-downloads.php');
        }
    }

    public function get_addresses(){
        $user_id=get_current_user_id();
        if($user_id) {
            echo do_shortcode("[wcv_pro_dashboard_nav]");
            wc_get_template( 'myaccount/my-address.php' );
        }
    }

    public function get_template_vendor_affilate_list(){
        global $wpdb;
        $user_id=get_current_user_id();


        if($user_id) {
            $affilates = $wpdb->get_results( "SELECT *,c.status as aff_status
            FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id." " );

            $referrals = $wpdb->get_results( "SELECT *,c.status as aff_status
            FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_referrals m ON m.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id." " );

            $visits = $wpdb->get_results( "SELECT *,c.status as aff_status,	'Direct traffic' AS referring_url,
            (SELECT referral_id FROM wp_affiliate_wp_referrals WHERE affiliate_id=c.affiliate_id and referral_id=m.referral_id and referral_id > 0 ) as converted
            FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_visits m ON m.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id." " );

            $payouts = $wpdb->get_results( "SELECT *,c.status as aff_status
            FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_payouts m ON m.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id." " );

            $creatives = $wpdb->get_results( "SELECT *
            FROM wp_affiliate_wp_creatives where store_id=".get_current_user_id()." order by creative_id desc
            " );



            //overview
            $date=date('Y-m-d');
            $time = strtotime($date);
            $final = date("Y-m-d", strtotime("-1 month", $time));

            $overview_total = $wpdb->get_results( "SELECT sum(m.amount) as paid_earnings,
            (SELECT sum(k.amount) FROM wp_affiliate_wp_payouts k inner join wp_vendor_affiliates s on s.affiliate_id=k.affiliate_id and (k.date BETWEEN  '".$final."' AND  '".$date."'  ) and s.vendor_id = ".$user_id." ) as paid_earnings_this_month,
            (SELECT sum(k.amount) FROM wp_affiliate_wp_payouts k inner join wp_vendor_affiliates s on s.affiliate_id=k.affiliate_id and DATE_FORMAT(k.date, '%Y-%m-%d') = '".$date."'   and s.vendor_id = ".$user_id." ) as paid_earnings_today
            FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_payouts m ON m.affiliate_id = s.affiliate_id
            WHERE s.vendor_id = ".$user_id."   " );



            $latest_affilate_registration = $wpdb->get_results( "SELECT *FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id."  " );

            $recent_referrals = $wpdb->get_results( "SELECT *FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_referrals m ON m.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id." " );

            $recent_referrals_visits = $wpdb->get_results( "SELECT *,
            (SELECT referral_id FROM wp_affiliate_wp_referrals WHERE affiliate_id=c.affiliate_id and referral_id=m.referral_id and referral_id > 0 ) as converted
            FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_visits m ON m.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id." " );

            $most_valiuable_affilates = $wpdb->get_results( "SELECT
            *,count(n.affiliate_id) as visit,sum(m.referrals) as payout_referrals,sum(m.amount) as paid_earnings FROM wp_vendor_affiliates s
            INNER JOIN wp_affiliate_wp_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_payouts m ON m.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_visits n ON n.affiliate_id = s.affiliate_id
            INNER JOIN wp_users d ON d.ID = c.user_id
            WHERE s.vendor_id = ".$user_id."     group by m.affiliate_id  order by paid_earnings desc,payout_referrals desc,visit desc" );




            //end overview


            $affilates_row=array('Name','Affiliate ID','Username','Paid Earnings','Rate','Paid Referrals','visits','status',"Email");
            $referrals_row=array('Amount','Affiliate','Reference','Description','Date','Status');
            $visits_row=array('Landing Page','Referring URL','Affiliate','Referral ID','Converted','Date');
            $payouts_row=array('Payout ID','Amount','Affiliate','Referrals','Payout Method','Status','Date');
            $creatives_row=array('Name','URL','Shortcode','Status','Image','Rate','Date');
            $overview_peid_row=array('Paid Earnings','Paid Earnings This Month','Paid Earnings Today');
            $latest_affilate_registration_row=array('Affiliate','Status');
            $most_valiuable_affilates_row=array('Affiliate','Earnings','Referrals','Visits');
            $recent_referrals_row=array('Affiliate','Amount','Description');
            $recent_referrals_visits_row=array('Affiliate','Url','Converted');

            echo do_shortcode("[wcv_pro_dashboard_nav]");
            wc_get_template( 'vendor_affilate_list.php', array(
                'affilates'=>$affilates,
                'affilates_row'=>$affilates_row,
                'referrals_row'=>$referrals_row,
                'payouts_row'=>$payouts_row,
                'referrals'=>$referrals,
                'visits_row'=>$visits_row,
                'visits'=>$visits,
                'payouts'=>$payouts,
                'creatives'=>$creatives,
                'creatives_row'=>$creatives_row,
                'overview_peid_row'=>$overview_peid_row,
                'latest_affilate_registration_row'=>$latest_affilate_registration_row,
                'recent_referrals_visits_row'=>$recent_referrals_visits_row,
                'recent_referrals_visits'=>$recent_referrals_visits,
                'recent_referrals_row'=>$recent_referrals_row,
                'most_valiuable_affilates'=>$most_valiuable_affilates,
                'most_valiuable_affilates_row'=>$most_valiuable_affilates_row,
                'recent_referrals'=>$recent_referrals,
                'latest_affilate_registration'=>$latest_affilate_registration,
                'overview_total'=>$overview_total,
            ), 'wc-vendors/dashboard/', wcv_a_plugin_dir . 'templates/' );

        }

    }



    public function get_edit_address_billing(){
        $user_id=get_current_user_id();

        if($user_id) {
            echo do_shortcode("[wcv_pro_dashboard_nav]");
            $load_address = 'billing';
            $current_user = wp_get_current_user();
            $load_address = sanitize_key( $load_address );

            $address = WC()->countries->get_address_fields( get_user_meta( get_current_user_id(), $load_address . '_country', true ), $load_address . '_' );

            // Enqueue scripts
            wp_enqueue_script( 'wc-country-select' );
            wp_enqueue_script( 'wc-address-i18n' );

            // Prepare values
            foreach ( $address as $key => $field ) {

                $value = get_user_meta( get_current_user_id(), $key, true );

                if ( ! $value ) {
                    switch( $key ) {
                        case 'billing_email' :
                        case 'shipping_email' :
                            $value = $current_user->user_email;
                            break;
                        case 'billing_country' :
                        case 'shipping_country' :
                            $value = WC()->countries->get_base_country();
                            break;
                        case 'billing_state' :
                        case 'shipping_state' :
                            $value = WC()->countries->get_base_state();
                            break;
                    }
                }

                $address[ $key ]['value'] = apply_filters( 'woocommerce_my_account_edit_address_field_value', $value, $key, $load_address );
            }


            wc_get_template( 'myaccount/form-edit-address.php', array(
                'load_address' 	=> $load_address,
                'address'		=> apply_filters( 'woocommerce_address_to_edit', $address )
            ) );

        }
    }

    public function edit_vendors_banners(){
        if(isset($_GET['id']) && $_GET['id'] ) {
            global $wpdb;
            echo do_shortcode("[wcv_pro_dashboard_nav]");
            $user_id = get_current_user_id();
            $results = $wpdb->get_results( "SELECT *FROM wp_affiliate_wp_creatives WHERE store_id=".$user_id." and creative_id=".$_GET['id']."" );

            if($results) {
                wc_get_template('edit_vendors_banners.php', array(
                    'results' => $results,
                ), 'wc-vendors/dashboard/', wcv_a_plugin_dir . 'templates/');
            }
        }else{
              exit;
        }
    }


    public function delete_vendor_banners(){
        if(isset($_GET['del']) && $_GET['del'] ) {
            global $wpdb;
            $user_id = get_current_user_id();
            $results = $wpdb->get_results( "SELECT *FROM wp_affiliate_wp_creatives WHERE store_id=".$user_id." and creative_id=".$_GET['del']."" );
            if(!empty($results)) {
                $update=$wpdb->delete('wp_affiliate_wp_creatives',array('creative_id'=>$_GET['del']), array( '%d' ));
                wp_redirect(site_url() . '/vendor_aff_list/?active=banners');
            }
        }
    }


    public function submit_creative_edit_affiliate_button() {

            global $wpdb;
            $user_id = get_current_user_id();
                if ($user_id) {
                    if (isset($_POST['edit_submit_banner']) && $_POST['edit_submit_banner']) {
                        if(isset($_POST['creative_id']) && $_POST['creative_id'] ) {
                            $results = $wpdb->get_results( "SELECT *FROM wp_affiliate_wp_creatives WHERE store_id=".$user_id." and creative_id = ".$_POST['creative_id']."" );
                            if(!empty($results)) {
                        if (!empty($_FILES['fileToUpload']) && isset($_FILES['fileToUpload'])) {

                            if (($_FILES['fileToUpload']['type'] == 'image/jpg')
                                || ($_FILES['fileToUpload']['type'] == 'image/jpeg')
                                || ($_FILES['fileToUpload']['type'] == 'image/png')
                                || ($_FILES['fileToUpload']['type'] == 'image/gif')
                            ) {

                                if(!empty($_FILES['fileToUpload']['name'])) {
                                    $file = 'wp-content/uploads/' . rand($user_id, 99999999) . $_FILES['fileToUpload']['name'];
                                    if ($file) {
                                        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $file);
                                        $file = site_url() . '/' . $file;
                                    }
                                }
                            }else{
                                $file= $results[0]->image;
                            }
                        }

                        if (isset($_POST['pv_name']) && !empty($_POST['pv_name'])) {
                            $banner_tit = $_POST['pv_name'];
                        }

                        if (isset($_POST['pv_description']) && !empty($_POST['pv_description'])) {
                            $banner_descr = $_POST['pv_description'];
                        }


                        if(isset($_POST['pv_url']) && !empty($_POST['pv_url'])) {
                             if (preg_match("/(http|https):\/\/(.*?)$/i", $_POST['pv_url'], $matches) > 0) {
                                 $banner_url= $matches[0];
                             }else{
                                 $banner_url=site_url();
                             }

                        }

                        if (isset($_POST['pv_text']) && !empty($_POST['pv_text'])) {
                            $banner_text = $_POST['pv_text'];
                        }


                        if (isset($_POST['pv_status']) && !empty($_POST['pv_status'])) {
                            if ($_POST['pv_status'] == 'Active') {
                                $banner_status = 'Active';
                            } elseif ($_POST['pv_status'] == 'Inactive') {
                                $banner_status = 'Inactive';
                            } else {
                                $banner_status = 'Inactive';
                            }
                        }

                        $date = date('Y-m-d h:i:s');

                                if(isset($_POST['banner_rate']) && !empty($_POST['banner_rate']) ) {
                                    $rate = abs(doubleval($_POST[ 'banner_rate' ]));
                                    if($rate && $rate > 100) {
                                        $rate = 0;
                                    }
                                }

                        $update=$wpdb->update('wp_affiliate_wp_creatives', array(
                            'creative_id' => $_POST['creative_id'],
                            'name' => $banner_tit,
                            'description' => $banner_descr,
                            'url' => $banner_url,
                            'text' => $banner_text,
                            'image' => $file,
                            'status' => $banner_status,
                            'date' => $date,
                            'rate' => $rate,
                            'store_id' => $user_id),
                             array('creative_id'=>$_POST['creative_id']));


                        if ($update) {
                            echo "<script>alert('Your banner was Edit');</script>";
                            wp_redirect(site_url() . '/vendor_aff_list/?active=banners');
                        }
                    }
                }
            }
        }
    }

    public function add_banners_as_vendor(){
        echo do_shortcode("[wcv_pro_dashboard_nav]");
        wc_get_template( 'add_banners.php', array(
        ), 'wc-vendors/dashboard/', wcv_a_plugin_dir . 'templates/' );
    }


    public function get_edit_address_shipping(){
        $user_id=get_current_user_id();

        if($user_id) {
            echo do_shortcode("[wcv_pro_dashboard_nav]");
            $load_address = 'shipping';
            $current_user = wp_get_current_user();
            $load_address = sanitize_key( $load_address );

            $address = WC()->countries->get_address_fields( get_user_meta( get_current_user_id(), $load_address . '_country', true ), $load_address . '_' );

            // Enqueue scripts
            wp_enqueue_script( 'wc-country-select' );
            wp_enqueue_script( 'wc-address-i18n' );

            // Prepare values
            foreach ( $address as $key => $field ) {

                $value = get_user_meta( get_current_user_id(), $key, true );

                if ( ! $value ) {
                    switch( $key ) {
                        case 'shipping_email' :
                        case 'billing_email' :
                            $value = $current_user->user_email;
                            break;
                        case 'shipping_country' :
                        case 'billing_country' :
                            $value = WC()->countries->get_base_country();
                            break;
                        case 'shipping_state' :
                        case 'billing_state' :
                            $value = WC()->countries->get_base_state();
                            break;
                    }
                }

                $address[ $key ]['value'] = apply_filters( 'woocommerce_my_account_edit_address_field_value', $value, $key, $load_address );
            }


            wc_get_template( 'myaccount/form-edit-address.php', array(
                'load_address' 	=> $load_address,
                'address'		=> apply_filters( 'woocommerce_address_to_edit', $address )
            ) );

        }
    }


}

new WC_Vendors_Affiliates();

# ========================================================================#
   #  Requires : Requires PHP5, GD library.
   #  Usage Example:
   #                     include("resize_class.php");
   #                     $resizeObj = new resize('images/cars/large/input.jpg');
   #                     $resizeObj -> resizeImage(150, 100, 0);
   #                     $resizeObj -> saveImage('images/cars/large/output.jpg', 100);
   # ========================================================================#


class resizenewimage
        {
            // *** Class variables
            private $image;
            private $width;
            private $height;
            private $imageResized;

            function __construct($fileName)
            {
                // *** Open up the file
                $this->image = $this->openImage($fileName);

                // *** Get width and height
                $this->width  = imagesx($this->image);
                $this->height = imagesy($this->image);
            }

            ## --------------------------------------------------------

            private function openImage($file)
            {
                // *** Get extension
                $extension = strtolower(strrchr($file, '.'));

                switch($extension)
                {
                    case '.jpg':
                    case '.jpeg':
                        $img = @imagecreatefromjpeg($file);
                        break;
                    case '.gif':
                        $img = @imagecreatefromgif($file);
                        break;
                    case '.png':
                        $img = @imagecreatefrompng($file);
                        break;
                    default:
                        $img = false;
                        break;
                }
                return $img;
            }

            ## --------------------------------------------------------

            public function resizeImage($newWidth, $newHeight, $option="auto")
            {
                // *** Get optimal width and height - based on $option
                $optionArray = $this->getDimensions($newWidth, $newHeight, $option);

                $optimalWidth  = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];


                // *** Resample - create image canvas of x, y size
                $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
                imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);


                // *** if option is 'crop', then crop too
                if ($option == 'crop') {
                    $this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
                }
            }

            ## --------------------------------------------------------

            private function getDimensions($newWidth, $newHeight, $option)
            {

               switch ($option)
                {
                    case 'exact':
                        $optimalWidth = $newWidth;
                        $optimalHeight= $newHeight;
                        break;
                    case 'portrait':
                        $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                        $optimalHeight= $newHeight;
                        break;
                    case 'landscape':
                        $optimalWidth = $newWidth;
                        $optimalHeight= $this->getSizeByFixedWidth($newWidth);
                        break;
                    case 'auto':
                        $optionArray = $this->getSizeByAuto($newWidth, $newHeight);
                        $optimalWidth = $optionArray['optimalWidth'];
                        $optimalHeight = $optionArray['optimalHeight'];
                        break;
                    case 'crop':
                        $optionArray = $this->getOptimalCrop($newWidth, $newHeight);
                        $optimalWidth = $optionArray['optimalWidth'];
                        $optimalHeight = $optionArray['optimalHeight'];
                        break;
                }
                return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
            }

            ## --------------------------------------------------------

            private function getSizeByFixedHeight($newHeight)
            {
                $ratio = $this->width / $this->height;
                $newWidth = $newHeight * $ratio;
                return $newWidth;
            }

            private function getSizeByFixedWidth($newWidth)
            {
                $ratio = $this->height / $this->width;
                $newHeight = $newWidth * $ratio;
                return $newHeight;
            }

            private function getSizeByAuto($newWidth, $newHeight)
            {
                if ($this->height < $this->width)
                // *** Image to be resized is wider (landscape)
                {
                    $optimalWidth = $newWidth;
                    $optimalHeight= $this->getSizeByFixedWidth($newWidth);
                }
                elseif ($this->height > $this->width)
                // *** Image to be resized is taller (portrait)
                {
                    $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                    $optimalHeight= $newHeight;
                }
                else
                // *** Image to be resizerd is a square
                {
                    if ($newHeight < $newWidth) {
                        $optimalWidth = $newWidth;
                        $optimalHeight= $this->getSizeByFixedWidth($newWidth);
                    } else if ($newHeight > $newWidth) {
                        $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                        $optimalHeight= $newHeight;
                    } else {
                        // *** Sqaure being resized to a square
                        $optimalWidth = $newWidth;
                        $optimalHeight= $newHeight;
                    }
                }

                return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
            }

            ## --------------------------------------------------------

            private function getOptimalCrop($newWidth, $newHeight)
            {

                $heightRatio = $this->height / $newHeight;
                $widthRatio  = $this->width /  $newWidth;

                if ($heightRatio < $widthRatio) {
                    $optimalRatio = $heightRatio;
                } else {
                    $optimalRatio = $widthRatio;
                }

                $optimalHeight = $this->height / $optimalRatio;
                $optimalWidth  = $this->width  / $optimalRatio;

                return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
            }

            ## --------------------------------------------------------

            private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
            {
                // *** Find center - this will be used for the crop
                $cropStartX = ( $optimalWidth / 2) - ( $newWidth /2 );
                $cropStartY = ( $optimalHeight/ 2) - ( $newHeight/2 );

                $crop = $this->imageResized;
                //imagedestroy($this->imageResized);

                // *** Now crop from center to exact requested size
                $this->imageResized = imagecreatetruecolor($newWidth , $newHeight);
                imagecopyresampled($this->imageResized, $crop , 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight , $newWidth, $newHeight);
            }

            ## --------------------------------------------------------

            public function saveImage($savePath, $imageQuality="100")
            {
                // *** Get extension
                $extension = strrchr($savePath, '.');
                   $extension = strtolower($extension);

                switch($extension)
                {
                    case '.jpg':
                    case '.jpeg':
                        if (imagetypes() & IMG_JPG) {
                            imagejpeg($this->imageResized, $savePath, $imageQuality);
                        }
                        break;

                    case '.gif':
                        if (imagetypes() & IMG_GIF) {
                            imagegif($this->imageResized, $savePath);
                        }
                        break;

                    case '.png':
                        // *** Scale quality from 0-100 to 0-9
                        $scaleQuality = round(($imageQuality/100) * 9);

                        // *** Invert quality setting as 0 is best, not 9
                        $invertScaleQuality = 9 - $scaleQuality;

                        if (imagetypes() & IMG_PNG) {
                             imagepng($this->imageResized, $savePath, $invertScaleQuality);
                        }
                        break;

                    // ... etc

                    default:
                        // *** No extension - No save.
                        break;
                }

                imagedestroy($this->imageResized);
				return $savePath;
            }


            ## --------------------------------------------------------

        }