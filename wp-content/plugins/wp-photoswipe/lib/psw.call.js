//jQuery version

if( window.device != 'desktop'){
(function ($) {
    'use strict';
    $(function () {
		var grab_all; 
		grab_all = document.getElementsByTagName("body");
		var grab_all_html;
		grab_all_html = grab_all[0].innerHTML;

		if(grab_all_html.indexOf('ngg-gallery') >= 0){
			var place_holder = ".ngg-gallery-thumbnail a";
		}
		else if(grab_all_html.indexOf('gallery-icon') >= 0){
			var place_holder = ".gallery-icon a";
		}
		else{
			var place_holder_div = document.createElement("div");
			place_holder_div.setAttribute("class","psw_target");
			grab_all[0].appendChild(place_holder_div);
			var place_holder = ".psw_target";
		}
		
        var myPhotoSwipe = {};


            myPhotoSwipe = $(place_holder).photoSwipe({
                imageScaleMethod: 'fit',
                autoStartSlideshow: false,
                captionAndToolbarAutoHideDelay: 5000,
                loop: true,
                doubleTapZoomLevel: 1.5,
                enableDrag: true,
                enableMouseWheel: false,
                enableKeyboard: false,
                zIndex: 1000
            });
    });
}(jQuery));
}
