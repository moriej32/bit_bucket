<?php

/*
  Plugin Name: MSH - Custom CSS and JS
  Plugin URI: http://www.mainstreehost.com
  Author: Matthew Backlas for Mainstreethost
  Description: Extends theme to include css and js files not associated with the theme. Just put the files in the respective folder inside the plugin folder and it will load them last, automatically.
  Version: 1.0
 */

class MSH_custom_css_and_js {

    const CSS_PATH = 'css/';
    const JS_PATH = 'js/';

// constructor
    public function __construct() {
        $this->load();
    }

//desctructor
    public function __destruct() {
        
    }

// enqueue files - general abstraction to retrive files and enqueue them
    private function enqueue_files($path) {
        $i = 0;

        $dir = plugin_dir_path(__FILE__);
        $file_array = scandir($dir . $path);

        // shift off the '.' and '..'
        array_shift($file_array);
        array_shift($file_array);

        foreach ($file_array as $file) {
            $name = 'ccaj-' . $i;
            if ($path == 'css/') {
                wp_enqueue_style($name, plugins_url() . '/msh_custom_css_and_js/' . $path . $file);
            } else {
                wp_enqueue_script($name, plugins_url() . '/msh_custom_css_and_js/' . $path . $file);
            }
            $i++;
        }
    }

// enqueue css
    public function ccaj_css() {
        $this->enqueue_files(self::CSS_PATH);
    }

// enqueue scripts
    public function ccaj_scripts() {
        $this->enqueue_files(self::JS_PATH);
    }

// call functions
    public function load() {
        add_action('wp_enqueue_scripts', array($this, 'ccaj_css'), 9999);
        add_action('wp_enqueue_scripts', array($this, 'ccaj_scripts'), 9999);
    }

}

$custom_css_and_js = new MSH_custom_css_and_js();
?>