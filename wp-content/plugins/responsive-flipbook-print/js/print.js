/* Print Extension */

jQuery( document ).ready( function( $ ) {
	function _scale_content( $container, ratio ) {
		$container.find( '.page-html *:not(.no-scale), .mpc-numeration-wrap span' ).each( function() {
			var $this = $( this );

			if ( typeof $this.data( '_scale' ) != 'undefined' ) {
				var _data = $this.data( '_scale' );

				if ( $this.is( 'img' ) )
					$this.css( {
						'width': _data[ 'width' ] * ratio * .8 + "px",
						//'height': _data[ 'height' ] * ratio + "px",
						'margin-top': _data[ 'margin-top' ] * ratio + "px",
						'margin-right': _data[ 'margin-right' ] * ratio + "px",
						'margin-bottom': _data[ 'margin-bottom' ] * ratio + "px",
						'margin-left': _data[ 'margin-left' ] * ratio + "px",
						'padding-top': _data[ 'padding-top' ] * ratio + "px",
						'padding-right': _data[ 'padding-right' ] * ratio + "px",
						'padding-bottom': _data[ 'padding-bottom' ] * ratio + "px",
						'padding-left': _data[ 'padding-left' ] * ratio + "px"
					} );
				else if( $this.parent().hasClass( 'mpc-numeration-wrap' ))
					$this.css( {
						'font-size': _data[ 'font-size' ] * ratio + "px",
						'line-height': _data[ 'line-height' ] * ratio + "px",
						'margin-top': _data[ 'margin-top' ] * ratio + "px",
						'margin-right': _data[ 'margin-right' ] * ratio + "px",
						'margin-bottom': _data[ 'margin-bottom' ] * ratio + "px",
						'margin-left': _data[ 'margin-left' ] * ratio + "px",
						'padding-top': _data[ 'padding-top' ] * ratio + "px",
						'padding-right': _data[ 'padding-right' ] * ratio + "px",
						'padding-bottom': _data[ 'padding-bottom' ] * ratio + "px",
						'padding-left': _data[ 'padding-left' ] * ratio + "px",
						'border-width': _data[ 'border-width' ] * ratio + "px",
						'border-radius': _data[ 'border-radius' ] * ratio + "px"
					} );
				else
					$this.css( {
						'font-size': _data[ 'font-size' ] * ratio + "px",
						'line-height': _data[ 'line-height' ] * ratio + "px"
					} );
			}
		} );
	}
	
	function rfbwp_print_refresh() {
		$flipbook.siblings( '.rfbwp-cover-wrap' ).each( function() {
			$( this ).css( 'opacity', 1 );
		} );
	}
	
	// Init browser print 
	function rfbwp_print_browser() {				
		window.print();
		rfbwp_print_refresh();
		
		setTimeout( function() {
			rfbwp_print_destroy();
			$( window ).trigger( 'resize' );
		}, 100 );		
	}	
	
	// Destroy Print
	function rfbwp_print_destroy() {
		$print_container.css( 'display', 'none' ).remove();
		$body.removeClass( 'flipbook-print' );
	}
	
	// Delete not needed markup & CSS
	function rfbwp_print_clear() {
		/* Remove trash CSS and assign ID to data attr */
		$print_flipbook
			.css( {
				position: '',
				left: '',
				'margin-top': '',
				transform: '',
				'-webkit-transform': ''
			} )
			.attr( 'data-fb-id', $print_flipbook.attr( 'id' ) )
			.removeAttr( 'id' );

		/* Remove shadows */	
		$print_flipbook
			.find( '.fb-shadow-bottom-left, .fb-shadow-top-left, .fb-shadow-bottom-right, .fb-shadow-top-right' )
			.remove();
		
		/* Remove Trasitions wrap */
		$print_flipbook
			.find( '> div:not(.turn-page-wrapper)' )
			.remove();
		
		$print_flipbook.find( '.turn-page-wrapper' ).each( function() {
			var $page_wrap = $( this );

			/* Remove trash CSS */
			$page_wrap.css( {
				position: '',
				width: '',
				top: '',
				right: '',
				left: '',
				'z-index': '',
				display: ''
			} );

			/* Remove style attr */
			$page_wrap.find( '.fb-page' ).removeAttr( 'style' );	
			$page_wrap.find( '> div' ).removeAttr( 'style' );

			/* Remove unneeded stuff */
			$page_wrap.find( '> div:nth-child(2)' ).remove();
			$page_wrap.find( '.fb-inside-shadow-right, .fb-inside-shadow-left, .fb-page-edge-shadow-left, .fb-page-edge-shadow-right' ).remove();

			if( $page_wrap.find( '.fb-page').is( '.double' ) )
				$page_wrap.addClass( 'double' );				

			if( $page_wrap.is( '.first' ) || $page_wrap.is( '.last') ) 
				$page_wrap.addClass( 'print' );

			if( $page_wrap.is( '.double' ) && $page_wrap.is( '.even' ) )
				$page_wrap.remove();

			if( $page_wrap.is( '.double' ) ) 
				$page_wrap.find( '.mpc-numeration-wrap' ).each( function() {
					var $this = $( this ),							
						$clone = $this.clone(),
						_page_number = parseInt( $clone.attr( 'data-page-number' ) ) + 1;

					$clone
						.attr( 'data-page-number', _page_number )
						.addClass( 'odd' );

					$clone.find( 'span' ).text( _page_number );

					$this.after( $clone );
				});
		} );
	}
	
	// Handle a extra options like Hard Covers print
	function rfbwp_print_extras() {
		var $print_data		= $flipbook.siblings( '.fb-nav' ).find( '.print' ),
			_print_orient	= $print_data.data( 'print-orient' ), // Print Orientation
			_hard_covers	= $print_data.data( 'print-hc' ), // Print Hard Covers
			_combine_fl		= $print_data.data( 'combine-fl' ); // Combine First & Last page together
			
		if( _print_orient )				
			$print_flipbook.addClass( 'print-orient-' + _print_orient );
		
		if( _hard_covers ) {
			$print_flipbook.addClass( 'print-hc' );
			rfbwp_print_add_hard_covers();
		}
		
		if( _combine_fl )
			$print_flipbook.addClass( 'print-combine-fl' );
	}
	
	// Add Hard Covers to Print 
	function rfbwp_print_add_hard_covers() {
		var $front_out_cover = $flipbook.parents( '.flipbook-container' ).find( '.rfbwp-cover-wrap.rfbwp-front img:nth-child(1)' ).clone(),
			$front_ins_cover = $flipbook.parents( '.flipbook-container' ).find( '.rfbwp-cover-wrap.rfbwp-front img:nth-child(2)' ).clone(),
			$back_out_cover = $flipbook.parents( '.flipbook-container' ).find( '.rfbwp-cover-wrap.rfbwp-back img:nth-child(1)' ).clone(),	
			$back_ins_cover = $flipbook.parents( '.flipbook-container' ).find( '.rfbwp-cover-wrap.rfbwp-back img:nth-child(2)' ).clone(),
			_cover_wrapper = '<div class="turn-page-wrapper hard-cover"></div>',
			_page_height = $flipbook.parents( '.flipbook-container' ).find( '.rfbwp-cover-wrap.rfbwp-front' ).height();
				
		_page_height = _page_height > 520 ? 520 : _page_height;		
				
		$front_ins_cover
				.prependTo( $print_flipbook )
				.wrap( _cover_wrapper )
				.parent().addClass( 'front-ins' )
				.css( {
					height: _page_height - 20,
					marginBottom: -_page_height + 20
				});
		
		$front_out_cover
				.prependTo( $print_flipbook )
				.wrap( _cover_wrapper )
				.parent().addClass( 'front-out' )
				.css( {
					height: _page_height - 20
				});
				
		$back_ins_cover
				.appendTo( $print_flipbook )
				.wrap( _cover_wrapper )
				.parent().addClass( 'back-ins' )
				.css( {
					height: _page_height - 20,
					marginTop: -_page_height + 20 
				});
		
		$back_out_cover
				.appendTo( $print_flipbook )
				.wrap( _cover_wrapper )
				.parent().addClass( 'back-out' )
				.css( {
					height: _page_height - 20
				});	
	}
	
	// Resize
	function rfbwp_print_resize() {
		if( $print_container.find( '.turn-page-wrapper:not(.hard-cover)' ).height() <= 500 )
			return;
		
		var _first_fb_w = $print_flipbook.width() * 0.5,
			_first_fb_h = $print_flipbook.height(),
			_ratio = _first_fb_w / _first_fb_h,
			_scale_ratio = 500 / _first_fb_h * 1.0;
		
		$print_container.find( '.flipbook' ).height( 500 ).width( 500 * _ratio * 2 );
		$print_container.find( '.flipbook' ).find( '.turn-page-wrapper' ).height( 500 );
		
		_scale_content( $print_container, _scale_ratio );
	}
		
	// Print
	function rfbwp_print() {			
		var _toc_class = $flipbook.parents( '.flipbook-container' ).is( '.toc-new' ) ? 'toc-new' : 'toc-old';	
		
		/* Create Print Container */
		$body
			.prepend( _print_container )
			.addClass( 'flipbook-print' );
	
		$print_container = $body.find( '.flipbook-printable' );
			
		$print_flipbook.find('div.fb-page-content .page-html *:not(.no-scale), div.fb-page-content .mpc-numeration-wrap span').each(function() {
			var $this = $( this );

			if ( $this.is( 'img' ) )
				$this.data( '_scale', {
					'width': $this.attr( 'width' ) ? parseInt( $this.attr( 'width' ) ) : parseInt( $this.width() ),
					'margin-top': parseInt( $this.css( 'margin-top' ) ),
					'margin-right': parseInt( $this.css( 'margin-right' ) ),
					'margin-bottom': parseInt( $this.css( 'margin-bottom' ) ),
					'margin-left': parseInt( $this.css( 'margin-left' ) ),
					'padding-top': parseInt( $this.css( 'padding-top' ) ),
					'padding-right': parseInt( $this.css( 'padding-right' ) ),
					'padding-bottom': parseInt( $this.css( 'padding-bottom' ) ),
					'padding-left': parseInt( $this.css( 'padding-left' ) )
				} );
			else if( $this.parent().hasClass( 'mpc-numeration-wrap' ) )
				$this.data( '_scale', {
					'font-size': parseInt( $this.css( 'font-size' ) ),
					'line-height': parseInt( $this.css( 'line-height' ) ),
					'margin-top': parseInt( $this.css( 'margin-top' ) ),
					'margin-right': parseInt( $this.css( 'margin-right' ) ),
					'margin-bottom': parseInt( $this.css( 'margin-bottom' ) ),
					'margin-left': parseInt( $this.css( 'margin-left' ) ),
					'padding-top': parseInt( $this.css( 'padding-top' ) ),
					'padding-right': parseInt( $this.css( 'padding-right' ) ),
					'padding-bottom': parseInt( $this.css( 'padding-bottom' ) ),
					'padding-left': parseInt( $this.css( 'padding-left' ) ),
					'border-width': parseInt( $this.css( 'border-width' ) ),
					'border-radius': parseInt( $this.css( 'border-radius' ) )
				} );
			else {
				var _font_size = parseInt( $this.css( 'font-size' ) ),
					_line_height = $this.css( 'line-height' );

				if ( _line_height.indexOf( 'px' ) == -1 )
					_line_height *= _font_size;

				$this.data( '_scale', {
					'font-size': _font_size,
					'line-height': parseInt( _line_height )
				} );
			}
		});	
			
		/* Copy ToC style */
		$print_flipbook.addClass( _toc_class );
						
		/* Insert Print View */
		$print_container.append( $print_flipbook );		
				
		/* Add Extras */
		rfbwp_print_extras();
		
		/* Resize */
		rfbwp_print_resize();
		
		/* Clear Markup */
		rfbwp_print_clear();
				
		/* Init Browser Print Dialog */
		rfbwp_print_browser();
	}

	/* Select pages to print */
	function rfbwp_print_prepare() {
		var $this = $( this ),
			$flipbook_container = $flipbook.parents( '.flipbook-container' ),
			$print_choice = $flipbook.parents( '.flipbook-container' ).find( 'div.show-all' );
	
		$print_choice.find( '.show-all-thumb' ).each( function() {
			var $item = $( this );
			
			if( !$item.is( '.selected' ) ) 
				$print_flipbook.find( '[page=' + $item.attr( 'data-page' ) + ']' ).remove();
		} );
						
		$flipbook_container.find( '.show-all-close' ).trigger( 'click' );		
				
		rfbwp_print();
	}
	
	function rfbwp_print_close() {
		var $flipbook_container = $flipbook.parents( '.flipbook-container' );
		
		$flipbook_container.find( '.print-accept' ).remove();		
	}
	
	function rfbwp_print_selectable() {
		$flipbook.parents( '.flipbook-container' ).find( '.fb-nav .show-all' ).trigger( 'click' );
		var $flipbook_container = $flipbook.parents( '.flipbook-container' ),
			$print_choice = $flipbook.parents( '.flipbook-container' ).find( 'div.show-all' ).addClass( 'print-select' ),
			$accept_button = $flipbook_container.find( '.print' ).clone().addClass( 'active print-accept' ).removeClass( 'print' ),
			_page_index = 1;
			
		$flipbook_container.find( '.alternative-nav .show-all-close' ).before( $accept_button );
		
		$print_choice.find( '.show-all-thumb' ).addClass( 'selected' );
		$print_choice.find( '.show-all-thumb' ).off( 'click' );
		$print_choice.find( '.show-all-thumb' ).on( 'click', function() {
			var $this = $( this );
			
			if( $this.is( '.even') ) 
				$this.next( '.odd' ).toggleClass( 'selected' );
			else 
				$this.prev( '.even' ).toggleClass( 'selected' );
			
			$this.toggleClass( 'selected' );
		} );
		
		$print_choice.find( '.show-all-thumb.first' ).attr( 'data-page', _page_index );
		_page_index++;
		
		$print_choice.find( '.show-all-thumb' ).each( function() {
			var $item = $( this );
			
			if( !$item.is( '.even' ) )
				$item.append( '<span class="mpc-checkbox">Print</span>' );
			
			if( !$item.is( '.first' ) && !$item.is( '.last') ) {
				$item.attr( 'data-page', _page_index );
				_page_index++;
			}					
		} );
		
		$print_choice.find( '.show-all-thumb.last' ).attr( 'data-page', _page_index );
				
		$accept_button.on( 'click', rfbwp_print_prepare );
		
		$flipbook_container.find( '.show-all-close' ).on( 'click', rfbwp_print_close );
	}
	
	/* Global variables */
	var $body				= $( 'body' ),
		$flipbook			= '',
		$print_container	= '',
		$print_flipbook		= '',
		_print_container	= '<div class="flipbook-printable"></div>';
	
	/* Easy Print toggle */
	$( document ).on( 'click', '.fb-nav .print', function( e ) {
		e.preventDefault();
		
		var $this = $( this );
		
		$flipbook = $this.parents( '.fb-nav' ).siblings( '.flipbook' );
		$print_flipbook = $flipbook.clone();
		
		if( $this.attr( 'data-print-choice' ) )
			rfbwp_print_selectable();
		else
			rfbwp_print();
	});
} );