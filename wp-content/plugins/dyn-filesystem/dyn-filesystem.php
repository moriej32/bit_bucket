<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.doityourselfnation.org
 * @since             1.0.0
 * @package           Dyn_Filesystem
 *
 * @wordpress-plugin
 * Plugin Name:       DYN Filesystem
 * Plugin URI:        http://www.doityourselfnation.org
 * Description:       Filesystem plugin for doityourselfnation.org
 * Version:           1.0.0
 * Author:            papul
 * Author URI:        http://www.doityourselfnation.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dyn-filesystem
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dyn-filesystem-activator.php
 */
function activate_dyn_filesystem() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dyn-filesystem-activator.php';
	Dyn_Filesystem_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dyn-filesystem-deactivator.php
 */
function deactivate_dyn_filesystem() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dyn-filesystem-deactivator.php';
	Dyn_Filesystem_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dyn_filesystem' );
register_deactivation_hook( __FILE__, 'deactivate_dyn_filesystem' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dyn-filesystem.php';

// feed channel page function
function feedssubsfrindscontrol(){
	
	  $endorse_feed = $_POST['endorse_feed'];
	  $favorite_feed = $_POST['favorite_feed'];
	  $donate_feed = $_POST['donate_feed'];
	  $edited_feed = $_POST['edited_feed'];
	  $add_play_list_feed = $_POST['add_play_list_feed'];
	  //$file_feed = $_POST['file_feed'];
	 // $product_feed = $_POST['product_feed'];
	  $review_feed = $_POST['review_feed'];
	  $falg_feed = $_POST['falg_feed'];
	  $upload_feed = $_POST['upload_feed'];
	  $comment_feed = $_POST['comment_feed'];
	  //$viewnumber_feed = $_POST['viewnumber_feed'];
	  $downloadcount_feed = $_POST['downloadcount_feed'];
	 // $posttime_feed = $_POST['posttime_feed'];
	  //$taglist_feed = $_POST['taglist_feed'];
	  $numberofsale_feed = $_POST['numberofsale_feed'];
	  
	  // friend
	  
	   $endorsef_feed = $_POST['endorsef_feed'];
	  $favoritef_feed = $_POST['favoritef_feed'];
	  $donatef_feed = $_POST['donatef_feed'];
	  $editedf_feed = $_POST['editedf_feed'];
	  $add_play_listf_feed = $_POST['add_play_listf_feed'];
	 // $filef_feed = $_POST['filef_feed'];
	 // $productf_feed = $_POST['productf_feed'];
	  $reviewf_feed = $_POST['reviewf_feed'];
	  $falgf_feed = $_POST['falgf_feed'];
	  $uploadf_feed = $_POST['uploadf_feed'];
	  $commentf_feed = $_POST['commentf_feed'];
	  //$viewnumberf_feed = $_POST['viewnumberf_feed'];
	  $downloadcountf_feed = $_POST['downloadcountf_feed'];
	  //$posttimef_feed = $_POST['posttimef_feed'];
	  //$taglistf_feed = $_POST['taglistf_feed'];
	  $numberofsalef_feed = $_POST['numberofsalef_feed'];
 
	  
	  $user_id = get_current_user_id();
	  
	    $get_endorse_feed = get_user_meta( $user_id, '_endorse_feed', true); 
		if($get_endorse_feed = 0 || $get_endorse_feed = 1 ){
			update_user_meta($user_id, '_endorse_feed', $endorse_feed);
		}
		else{
			add_user_meta( $user_id, '_endorse_feed', $endorse_feed);
		}
	 
	  $get_favorite_feed = get_user_meta( $user_id, '_favorite_feed', true); 
		if($get_favorite_feed = 0 || $get_favorite_feed = 1 ){
			update_user_meta($user_id, '_favorite_feed', $favorite_feed);
		}
		else{
			add_user_meta( $user_id, '_favorite_feed', $favorite_feed);
		}
	   
	    $add_play_list_feed = get_user_meta( $user_id, '_add_play_list_feed', true); 
		if($add_play_list_feed = 0 || $add_play_list_feed = 1 ){
			update_user_meta($user_id, '_add_play_list_feed', $add_play_list_feed);
		}
		else{
			add_user_meta( $user_id, '_add_play_list_feed', $add_play_list_feed);
		}
		$edited_feed = get_user_meta( $user_id, '_edited_feed', true); 
		if($edited_feed = 0 || $edited_feed = 1 ){
			update_user_meta($user_id, '_book_feed', $edited_feed);
		}
		else{
			add_user_meta( $user_id, '_edited_feed', $edited_feed);
		}
		
		/*$get_file_feed = get_user_meta( $user_id, '_file_feed', true); 
		if($get_file_feed = 0 || $get_file_feed = 1 ){
			update_user_meta($user_id, '_file_feed', $file_feed);
		}
		else{
			add_user_meta( $user_id, '_file_feed', $file_feed);
		}
		
		$get_product_feed = get_user_meta( $user_id, '_product_feed', true); 
		if($get_product_feed = 0 || $get_product_feed = 1 ){
			update_user_meta($user_id, '_product_feed', $file_feed);
		}
		else{
			add_user_meta( $user_id, '_product_feed', $file_feed);
		}*/
		
		$get_donate_feed = get_user_meta( $user_id, '_donate_feed', true); 
		if($get_donate_feed = 0 || $get_donate_feed = 1 ){
			update_user_meta($user_id, '_donate_feed', $donate_feed);
		}
		else{
			add_user_meta( $user_id, '_donate_feed', $donate_feed);
		}
		
		$get_review_feed = get_user_meta( $user_id, '_review_feed', true); 
		if($get_review_feed = 0 || $get_review_feed = 1 ){
			update_user_meta($user_id, '_review_feed', $review_feed);
		}
		else{
			add_user_meta( $user_id, '_review_feed', $review_feed);
		}
		
		$get_flag_feed = get_user_meta( $user_id, '_flag_feed', true); 
		if($get_flag_feed = 0 || $get_flag_feed = 1 ){
			update_user_meta($user_id, '_flag_feed', $falg_feed);
		}
		else{
			add_user_meta( $user_id, '_flag_feed', $falg_feed);
		}
		
		$get_upload_feed = get_user_meta( $user_id, '_upload_feed', true); 
		if($get_upload_feed = 0 || $get_upload_feed = 1 ){
			update_user_meta($user_id, '_upload_feed', $upload_feed);
		}
		else{
			add_user_meta( $user_id, '_upload_feed', $upload_feed);
		}
		
		$get_comment_feed = get_user_meta( $user_id, '_comment_feed', true); 
		if($get_comment_feed = 0 || $get_comment_feed = 1 ){
			update_user_meta($user_id, '_comment_feed', $comment_feed);
		}
		else{
			add_user_meta( $user_id, '_comment_feed', $comment_feed);
		}
		
		/*$get_viewnumber_feed = get_user_meta( $user_id, '_viewnumber_feed', true); 
		if($get_viewnumber_feed = 0 || $get_viewnumber_feed = 1 ){
			update_user_meta($user_id, '_viewnumber_feed', $viewnumber_feed);
		}
		else{
			add_user_meta( $user_id, '_viewnumber_feed', $viewnumber_feed);
		}*/
		
		$get_downloadcount_feed = get_user_meta( $user_id, '_downloadcount_feed', true); 
		if($get_downloadcount_feed = 0 || $get_downloadcount_feed = 1 ){
			update_user_meta($user_id, '_downloadcount_feed', $downloadcount_feed);
		}
		else{
			add_user_meta( $user_id, '_downloadcount_feed', $downloadcount_feed);
		}
		
		/*$get_posttime_feed = get_user_meta( $user_id, '_posttime_feed', true); 
		if($get_posttime_feed = 0 || $get_posttime_feed = 1 ){
			update_user_meta($user_id, '_posttime_feed', $posttime_feed);
		}
		else{
			add_user_meta( $user_id, '_posttime_feed', $posttime_feed);
		}
		
		$get_taglist_feed = get_user_meta( $user_id, '_taglist_feed', true); 
		if($get_taglist_feed = 0 || $get_taglist_feed = 1 ){
			update_user_meta($user_id, '_taglist_feed', $taglist_feed);
		}
		else{
			add_user_meta( $user_id, '_taglist_feed', $taglist_feed);
		}*/
		
		$get_numberofsale_feed = get_user_meta( $user_id, '_numberofsale_feed', true); 
		if($get_numberofsale_feed = 0 || $get_numberofsale_feed = 1 ){
			update_user_meta($user_id, '_numberofsale_feed', $numberofsale_feed);
		}
		else{
			add_user_meta( $user_id, '_numberofsale_feed', $numberofsale_feed);
		}
		
		
		//friends 
		
		 $get_endorsef_feed = get_user_meta( $user_id, '_endorsef_feed', true); 
		if($get_endorsef_feed = 0 || $get_endorsef_feed = 1 ){
			update_user_meta($user_id, '_endorsef_feed', $endorsef_feed);
		}
		else{
			add_user_meta( $user_id, '_endorsef_feed', $endorsef_feed);
		}
	 
	  $get_favoritef_feed = get_user_meta( $user_id, '_favoritef_feed', true); 
		if($get_favoritef_feed = 0 || $get_favoritef_feed = 1 ){
			update_user_meta($user_id, '_favoritef_feed', $favoritef_feed);
		}
		else{
			add_user_meta( $user_id, '_favoritef_feed', $favoritef_feed);
		}
	   
	    $add_play_listf_feed = get_user_meta( $user_id, '_add_play_listf_feed', true); 
		if($add_play_listf_feed = 0 || $add_play_listf_feed = 1 ){
			update_user_meta($user_id, '_add_play_listf_feed', $add_play_listf_feed);
		}
		else{
			add_user_meta( $user_id, '_add_play_listf_feed', $add_play_listf_feed);
		}
		$editedf_feed = get_user_meta( $user_id, '_editedf_feed', true); 
		if($editedf_feed = 0 || $editedf_feed = 1 ){
			update_user_meta($user_id, '_editedf_feed', $editedf_feed);
		}
		else{
			add_user_meta( $user_id, '_editedf_feed', $editedf_feed);
		}
		
		/*$get_filef_feed = get_user_meta( $user_id, '_filef_feed', true); 
		if($get_filef_feed = 0 || $get_filef_feed = 1 ){
			update_user_meta($user_id, '_filef_feed', $filef_feed);
		}
		else{
			add_user_meta( $user_id, '_filef_feed', $filef_feed);
		}
		
		$get_productf_feed = get_user_meta( $user_id, '_productf_feed', true); 
		if($get_productf_feed = 0 || $get_productf_feed = 1 ){
			update_user_meta($user_id, '_productf_feed', $filef_feed);
		}
		else{
			add_user_meta( $user_id, '_productf_feed', $filef_feed);
		}*/
		
		$get_donatef_feed = get_user_meta( $user_id, '_donatef_feed', true); 
		if($get_donatef_feed = 0 || $get_donatef_feed = 1 ){
			update_user_meta($user_id, '_donatef_feed', $donatef_feed);
		}
		else{
			add_user_meta( $user_id, '_donatef_feed', $donatef_feed);
		}
		
		$get_reviewf_feed = get_user_meta( $user_id, '_reviewf_feed', true); 
		if($get_reviewf_feed = 0 || $get_reviewf_feed = 1 ){
			update_user_meta($user_id, '_reviewf_feed', $reviewf_feed);
		}
		else{
			add_user_meta( $user_id, '_reviewf_feed', $reviewf_feed);
		}
		
		$get_flagf_feed = get_user_meta( $user_id, '_flagf_feed', true); 
		if($get_flagf_feed = 0 || $get_flagf_feed = 1 ){
			update_user_meta($user_id, '_flagf_feed', $falgf_feed);
		}
		else{
			add_user_meta( $user_id, '_flagf_feed', $falgf_feed);
		}
		
		$get_uploadf_feed = get_user_meta( $user_id, '_uploadf_feed', true); 
		if($get_uploadf_feed = 0 || $get_uploadf_feed = 1 ){
			update_user_meta($user_id, '_uploadf_feed', $uploadf_feed);
		}
		else{
			add_user_meta( $user_id, '_uploadf_feed', $uploadf_feed);
		}
		
		$get_commentf_feed = get_user_meta( $user_id, '_commentf_feed', true); 
		if($get_commentf_feed = 0 || $get_commentf_feed = 1 ){
			update_user_meta($user_id, '_commentf_feed', $commentf_feed);
		}
		else{
			add_user_meta( $user_id, '_commentf_feed', $commentf_feed);
		}
		
		/*$get_viewnumberf_feed = get_user_meta( $user_id, '_viewnumberf_feed', true); 
		if($get_viewnumberf_feed = 0 || $get_viewnumberf_feed = 1 ){
			update_user_meta($user_id, '_viewnumberf_feed', $viewnumberf_feed);
		}
		else{
			add_user_meta( $user_id, '_viewnumberf_feed', $viewnumberf_feed);
		}*/
		
		$get_downloadcountf_feed = get_user_meta( $user_id, '_downloadcountf_feed', true); 
		if($get_downloadcountf_feed = 0 || $get_downloadcountf_feed = 1 ){
			update_user_meta($user_id, '_downloadcountf_feed', $downloadcountf_feed);
		}
		else{
			add_user_meta( $user_id, '_downloadcountf_feed', $downloadcountf_feed);
		}
		
		/*$get_posttimef_feed = get_user_meta( $user_id, '_posttimef_feed', true); 
		if($get_posttimef_feed = 0 || $get_posttimef_feed = 1 ){
			update_user_meta($user_id, '_posttimef_feed', $posttimef_feed);
		}
		else{
			add_user_meta( $user_id, '_posttimef_feed', $posttimef_feed);
		}
		
		$get_taglistf_feed = get_user_meta( $user_id, '_taglistf_feed', true); 
		if($get_taglistf_feed = 0 || $get_taglistf_feed = 1 ){
			update_user_meta($user_id, '_taglistf_feed', $taglistf_feed);
		}
		else{
			add_user_meta( $user_id, '_taglistf_feed', $taglistf_feed);
		}*/
		
		$get_numberofsalef_feed = get_user_meta( $user_id, '_numberofsalef_feed', true); 
		if($get_numberofsalef_feed = 0 || $get_numberofsalef_feed = 1 ){
			update_user_meta($user_id, '_numberofsalef_feed', $numberofsalef_feed);
		}
		else{
			add_user_meta( $user_id, '_numberofsalef_feed', $numberofsalef_feed);
		}
	  /*
	  	 product_feed: product_feed,
							
							donate_feed: donate_feed,review_feed: review_feed,falg_feed: falg_feed,
							upload_feed: upload_feed,comment_feed: comment_feed,
							
							viewnumber_feed: viewnumber_feed,downloadcount_feed: downloadcount_feed,posttime_feed: posttime_feed,
							
							taglist_feed: taglist_feed,numberofsale_feed: numberofsale_feed,endorsef_feed: endorsef_feed,
							
							favoritef_feed: favoritef_feed,videof_feed: videof_feed,filef_feed: filef_feed,bookf_feed: bookf_feed,
							
							productf_feed: productf_feed,donatef_feed: donatef_feed,reviewf_feed: reviewf_feed,
							
							falgf_feed: falgf_feed,uploadf_feed: uploadf_feed,commentf_feed: commentf_feed,
							
							viewnumberf_feed: viewnumberf_feed,downloadcountf_feed: downloadcountf_feed,posttimef_feed: posttimef_feed,
							
							taglistf_feed: taglistf_feed,numberofsalef_feed: numberofsalef_feed,
	  */
	  //echo $favorite_feed; video_feed
	  echo '<strong>'. "Successfully Update Feed For Subscriptions and Friends" .'</strong>';
	  die();
  }
add_action('wp_ajax_feedssubs_feed', 'feedssubsfrindscontrol');

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dyn_filesystem() {

	$plugin = new Dyn_Filesystem();
	$plugin->run();

}
run_dyn_filesystem();
