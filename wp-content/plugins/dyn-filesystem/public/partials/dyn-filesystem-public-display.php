<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
