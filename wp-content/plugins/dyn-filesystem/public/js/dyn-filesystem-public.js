(function( $ ) {
	'use strict';

$(function(){
	$(".file-upload-button").click(function(){
		$(".dyn-upload-details").toggle();
	});

	$(".dyn-file-sort-select").change(function(){
		//window.location.href = $(this).find(':selected').data("url");
		var loc=$(this).find(':selected').data("url");
		var loc_parts=loc.split("&");
		var chckt=/page=\d/;
		var chckt2=/sortvideo=\w+/;
		var chckt3=/sortbook=\w+/;
		loc_parts.forEach(function(vl,nd){
			if( chckt.test(vl) ){
              loc=loc.replace(vl, "page=1");             
			}
			else if( chckt2.test(vl) ){
			  loc=loc.replace(/sortvideo=\w+&/, "changedtofile=1");             
			}
			else if( chckt3.test(vl) ){
              loc=loc.replace(/sortbook=\w+&/, "changedtofile=1");             
			}
		});

		window.location.href = loc;
	});
 });

})( jQuery );
