(function( $ ) {
	'use strict';

$(function() {
	var _custom_media = true,
	_orig_send_attachment = wp.media.editor.send.attachment;

	function class_by_mime(mime){
		switch(mime){
			case "application/vnd.ms-powerpoint":
			case "application/vnd.ms-powerpoint.presentation.macroenabled.12":
			case "application/vnd.ms-powerpoint.slideshow.macroenabled.12":
			case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
			case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
				return "dyn-ppt";
				break;
			case "application/zip":
			case "application/x-compressed":
			case "application/x-zip-compressed":
			case "multipart/x-zip":
				return "dyn-zip";
				break;
			case "image/jpeg":
			case "image/gif":
			case "image/x-icon":
				return "dyn-jpg";
				break;
			case "image/png":
				return "dyn-png";
				break;
			case "audio/mpeg3":
			case "audio/mpeg":
			case "audio/x-mpeg-3":
			case "video/mpeg":
			case "video/x-mpeg":
				return "dyn-mp3";
				break;
			case "video/mp4":
				return "dyn-mp4";
				break;
			case "application/excel":
			case "application/vnd.ms-excel":
			case "application/x-excel":
			case "application/x-msexcel":
				return "dyn-xls";
				break;
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
						     return "dyn-xls";
				             break;	
			case "application/msword":
			case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
			case "application/rtf":
			case "application/x-rtf":
			case "text/richtext":
				return "dyn-doc";
				break;
			case "application/pdf":
				return "dyn-pdf";
				break;
			case "application/octet-stream":
				return "dyn-iso";
				break;
			default:
				return "dyn-unknown";
				break;
		}
	}

	$(".upload-dyn-file").click(function(){
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $(this);
		//var id = button.attr('id').replace('_button', '');
		var elem = $(this).closest(".display-dyn-file-upload");
		_custom_media = true;
		wp.media.editor.send.attachment = function(props, attachment){
			if ( _custom_media ) {
				var cssClass = "dyn-file-sprite";
				cssClass += " " + class_by_mime(attachment.mime);
				elem.find(".dyn-uploaded-file").removeClass().addClass("dyn-uploaded-file");
				elem.find(".dyn-uploaded-file").addClass(cssClass);
				elem.find(".dyn-file-upload-url").val(attachment.url);
				elem.find(".dyn-file-upload-value").val(attachment.id);
				button.next(".remove-dyn-file").show();
			} else {
				return _orig_send_attachment.apply( this, [props, attachment] );
			};
		}

		wp.media.editor.open(button);
		return false;
	});

	$(".remove-dyn-file").click(function(){
		var elem = $(this).closest(".display-dyn-file-upload");
		elem.find(".dyn-file-upload-url").val('');
		elem.find(".dyn-file-upload-value").val('');
		elem.find(".dyn-uploaded-file").removeClass().addClass("dyn-uploaded-file");
		$(this).hide();
		return false;
	});

	$("input[name='dyn-file-upload[type]']").on('change', function(){
		$(".display-dyn-value").hide();
		$(".display-dyn-value.display-" + $(this).val()).show();
		console.log($(this).val());
	});
});

})( jQuery );
