<?php
/* PAGE MARKUP */
function rfbwp_display_pdf_wizard() {
	ob_start();
	?>
	<div id="rfbwp_pdf_wizard" class="rfbwp-pdf-wizard-wrap" style="display: block;">
		<div id="rfbwp_error_box" class="rfbwp-error-box" data-error="">
			<div class="rfbwp-error-server rfbwp-error">
				<h3><?php _e( 'Server connection issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server didn\'t respond to request.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-size rfbwp-error">
				<h3><?php _e( 'PDF size issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'PDF size is larger then 50MB.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-upload rfbwp-error">
				<h3><?php _e( 'PDF upload issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'PDF couldn\'t be uploaded to the server.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-pages rfbwp-error">
				<h3><?php _e( 'PDF pages issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'PDF have too few pages. It should have at least 3 pages.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-info rfbwp-error">
				<h3><?php _e( 'PDF data issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t extract PDF data for convertion.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-convertion rfbwp-error">
				<h3><?php _e( 'PDF convertion issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t convert PDF to images.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-images rfbwp-error">
				<h3><?php _e( 'Images list issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t list converted images.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-import rfbwp-error">
				<h3><?php _e( 'Images import issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Server couldn\'t import converted images.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
			<div class="rfbwp-error-generate rfbwp-error">
				<h3><?php _e( 'Imported images issue.', 'rfbwp-pdf' ); ?></h3>
				<p><?php _e( 'Too few images imported. Flipbook should have at least 3 pages.', 'rfbwp-pdf' ); ?></p>
				<p><?php _e( 'Please <a href="#" class="rfbwp-restart">try again</a>. If the issue would repeat contact our support team :)', 'rfbwp-pdf' ); ?></p>
			</div>
		</div>
		<!--<div id="rfbwp_step_options" class="rfbwp-step">
			<h3><?php _e( 'Step 1: <em>Set Flipbook options</em>', 'rfbwp-pdf' ); ?></h3>
			<label for="rfbwp_name"><?php _e( 'Flipbook name:', 'rfbwp-pdf' ); ?></label>
			<input id="rfbwp_name" type="text" name="rfbwp_name" placeholder="<?php _e( 'Flipbook name', 'rfbwp-pdf' ); ?>">

			<div class="rfbwp-col-left">
				<h5><?php _e( 'Normal page', 'rfbwp-pdf' ); ?></h5>
				<label for="rfbwp_width">
					<?php _e( 'Max width:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_width" class="rfbwp-validate-option" type="text" name="rfbwp_width" placeholder="<?php _e( 'Width', 'rfbwp-pdf' ); ?>" value="600"
						data-default="600"
						data-min="100"
						data-max="1000">
					<span>px</span>
				</label>
				<label for="rfbwp_height">
					<?php _e( 'Max height:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_height" class="rfbwp-validate-option" type="text" name="rfbwp_height" placeholder="<?php _e( 'Height', 'rfbwp-pdf' ); ?>" value="600"
						data-default="600"
						data-min="100"
						data-max="1000">
					<span>px</span>
				</label>
				<label for="rfbwp_quality">
					<?php _e( 'Quality:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_quality" class="rfbwp-validate-option" type="text" name="rfbwp_quality" placeholder="<?php _e( 'Quality', 'rfbwp-pdf' ); ?>" value="75"
						data-default="75"
						data-min="1"
						data-max="100">
					<span>%</span>
				</label>
			</div>

			<div id="rfbwp_zoom_wrap" class="rfbwp-col-right">
				<h5><?php _e( 'Zoom page', 'rfbwp-pdf' ); ?></h5>
				<label for="rfbwp_zoom" class="rfbwp-zoom">
					<input id="rfbwp_zoom" type="checkbox" name="rfbwp_zoom" checked="checked">
					<?php _e( 'Enabled', 'rfbwp-pdf' ); ?>
				</label>
				<label for="rfbwp_zoom_width">
					<?php _e( 'Max width:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_zoom_width" class="rfbwp-validate-option" type="text" name="rfbwp_zoom_width" placeholder="<?php _e( 'Width', 'rfbwp-pdf' ); ?>" value="1200"
						data-default="1200"
						data-min="100"
						data-max="2000">
					<span>px</span>
				</label>
				<label for="rfbwp_zoom_height">
					<?php _e( 'Max height:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_zoom_height" class="rfbwp-validate-option" type="text" name="rfbwp_zoom_height" placeholder="<?php _e( 'Height', 'rfbwp-pdf' ); ?>" value="1200"
						data-default="1200"
						data-min="100"
						data-max="2000">
					<span>px</span>
				</label>
				<label for="rfbwp_zoom_quality">
					<?php _e( 'Quality:', 'rfbwp-pdf' ); ?>
					<input id="rfbwp_zoom_quality" class="rfbwp-validate-option" type="text" name="rfbwp_zoom_quality" placeholder="<?php _e( 'Quality', 'rfbwp-pdf' ); ?>" value="90"
						data-default="90"
						data-min="1"
						data-max="100">
					<span>%</span>
				</label>
			</div>
		</div>-->
		<div class="description-top"><?php _e( 'Select PDF', 'rfbwp-pdf' ); ?></div>
		
		<div id="rfbwp_step_pdf" class="rfbwp-step">
		 
			<div class="rfbwp-link">
				<input id="rfbwp_pdf" type="file" name="files[]">
				<span id="rfbwp_pdf_overlay" class="rfbwp-file-name" data-text="<?php _e( 'Click to select file', 'rfbwp-pdf' ); ?>"><?php _e( 'Click to select file', 'rfbwp-pdf' ); ?></span>
			</div>
		</div>
		<div id="rfbwp_step_convert" class="rfbwp-step">
			<h3><?php _e( ' <em>Start convertion</em>', 'rfbwp-pdf' ); ?></h3>
			<a href="#" id="rfbwp_convert" class="rfbwp-link"><?php _e( 'Convert PDF', 'rfbwp-pdf' ); ?></a>
		</div>
		<div id="rfbwp_step_info" class="rfbwp-step">
			<h3><?php _e( 'Convertion in progress', 'rfbwp-pdf' ); ?></h3>
			<div id="rfbwp_uploading" class="rfbwp-progress">
				<span><?php _e( 'Uploading: ' ); ?></span>
				<span class="rfbwp-value">0%</span>
				<span class="rfbwp-time" data-text="<?php _e( '( calculating... )', 'rfbwp-pdf' ); ?>"><?php _e( '( calculating... )', 'rfbwp-pdf' ); ?></span>
				<span class="rfbwp-line"></span>
			</div>
			<div id="rfbwp_converting" class="rfbwp-progress">
				<span><?php _e( 'Converting: ' ); ?></span>
				<span class="rfbwp-value">0%</span>
				<span class="rfbwp-time" data-text="<?php _e( '( calculating... )', 'rfbwp-pdf' ); ?>"><?php _e( '( calculating... )', 'rfbwp-pdf' ); ?></span>
				<span class="rfbwp-line"></span>
			</div>
			<div id="rfbwp_importing" class="rfbwp-progress">
				<span><?php _e( 'Importing: ' ); ?></span>
				<span class="rfbwp-value">0%</span>
				<span class="rfbwp-time" data-text="<?php _e( '( calculating... )', 'rfbwp-pdf' ); ?>"><?php _e( '( calculating... )', 'rfbwp-pdf' ); ?></span>
				<span class="rfbwp-line"></span>
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
}

/*----------------------------------------------------------------------------*\
	AJAX REQUESTS
\*----------------------------------------------------------------------------*/

/* Importing single image */
add_action( 'wp_ajax_rfbwp_import_single_image', 'rfbwp_import_single_image' );
function rfbwp_import_single_image() {
	if ( ! isset( $_REQUEST[ 'image_path' ] ) && ! isset( $_REQUEST[ 'fb_name' ] ) )
		return;

	$zoom = '';
	if ( isset( $_REQUEST[ 'zoom' ] ) )
		$zoom = '(zoom) ';

	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	$uploaded_file  = wp_upload_bits( $_REQUEST[ 'fb_name' ] . ' - ' . $zoom . basename( $_REQUEST[ 'image_path' ] ), null, file_get_contents( $_REQUEST[ 'image_path' ] ) );
	$wp_upload_dir  = wp_upload_dir();
	$file_path      = $wp_upload_dir[ 'basedir' ] . str_replace( $wp_upload_dir[ 'baseurl' ], '', $uploaded_file[ 'url' ] );
	$parent_post_id = 0;
	$filetype       = wp_check_filetype( basename( $file_path ), null );
	$file_data      = array(
		'guid'           => $wp_upload_dir[ 'url' ] . '/' . basename( $file_path ),
		'post_mime_type' => $filetype[ 'type' ],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	$file_id       = wp_insert_attachment( $file_data, $file_path, $parent_post_id );
	$file_metadata = wp_generate_attachment_metadata( $file_id, $file_path );
	wp_update_attachment_metadata( $file_id, $file_metadata );

	$file_info = array(
		'url'    => $uploaded_file[ 'url' ],
		'width'  => $file_metadata[ 'width' ],
		'height' => $file_metadata[ 'height' ]
	);

	echo json_encode( $file_info );

	die(0);
}

/* Creating new flipbook */
add_action( 'wp_ajax_rfbwp_create_new_flipbook_book', 'rfbwp_create_new_flipbook' );
function rfbwp_create_new_flipbook() {
	if ( ! isset( $_REQUEST[ 'flipbook' ] ) )
		return;

	$base_flipbook = array(
		'rfbwp_fb_force_open' => '0',
		'rfbwp_fb_border_size' => '0',
		'rfbwp_fb_border_color' => '',
		'rfbwp_fb_border_radius' => '0',
		'rfbwp_fb_outline' => '0',
		'rfbwp_fb_outline_color' => '',
		'rfbwp_fb_inner_shadows' => '1',
		'rfbwp_fb_edge_outline' => '0',
		'rfbwp_fb_edge_outline_color' => '',
		'rfbwp_fb_fs_color' => '#ededed',
		'rfbwp_fb_fs_opacity' => '95',
		'rfbwp_fb_fs_icon_color' => '1',
		'rfbwp_fb_toc_display_style' => '0',
		'rfbwp_fb_heading_font' => '0',
		'rfbwp_fb_heading_family' => 'default',
		'rfbwp_fb_heading_fontstyle' => 'regular',
		'rfbwp_fb_heading_size' => '24',
		'rfbwp_fb_heading_line' => '',
		'rfbwp_fb_heading_color' => '#2b2b2b',
		'rfbwp_fb_content_font' => '0',
		'rfbwp_fb_content_family' => 'default',
		'rfbwp_fb_content_fontstyle' => 'regular',
		'rfbwp_fb_content_size' => '24',
		'rfbwp_fb_content_line' => '',
		'rfbwp_fb_content_color' => '#2b2b2b',
		'rfbwp_fb_num_font' => '0',
		'rfbwp_fb_num_family' => 'default',
		'rfbwp_fb_num_fontstyle' => 'regular',
		'rfbwp_fb_num_size' => '24',
		'rfbwp_fb_num_line' => '',
		'rfbwp_fb_num_color' => '#2b2b2b',
		'rfbwp_fb_toc_font' => '0',
		'rfbwp_fb_toc_family' => 'default',
		'rfbwp_fb_toc_fontstyle' => 'regular',
		'rfbwp_fb_toc_size' => '24',
		'rfbwp_fb_toc_line' => '',
		'rfbwp_fb_toc_color' => '#2b2b2b',
		'rfbwp_fb_zoom_border_size' => '10',
		'rfbwp_fb_zoom_border_color' => '#ECECEC',
		'rfbwp_fb_zoom_border_radius' => '10',
		'rfbwp_fb_zoom_outline' => '1',
		'rfbwp_fb_zoom_outline_color' => '#D0D0D0',
		'rfbwp_fb_sa_thumb_cols' => '3',
		'rfbwp_fb_sa_thumb_border_size' => '1',
		'rfbwp_fb_sa_thumb_border_color' => '#878787',
		'rfbwp_fb_sa_vertical_padding' => '10',
		'rfbwp_fb_sa_horizontal_padding' => '10',
		'rfbwp_fb_sa_border_size' => '10',
		'rfbwp_fb_sa_border_color' => '#F6F6F6',
		'rfbwp_fb_sa_border_radius' => '10',
		'rfbwp_fb_sa_outline' => '1',
		'rfbwp_fb_sa_outline_color' => '#D6D6D6',
		'rfbwp_fb_nav_menu_type' => '0',
		'rfbwp_fb_nav_menu_position' => 'bottom',
		'rfbwp_fb_nav_stack' => '0',
		'rfbwp_fb_nav_text' => '0',
		'rfbwp_fb_nav_toc' => '1',
		'rfbwp_fb_nav_toc_order' => '1',
		'rfbwp_fb_nav_toc_index' => '2',
		'rfbwp_fb_nav_toc_icon' => 'fa fa-th-list',
		'rfbwp_fb_nav_zoom' => '1',
		'rfbwp_fb_nav_zoom_order' => '2',
		'rfbwp_fb_nav_zoom_icon' => 'fa fa-search-plus',
		'rfbwp_fb_nav_zoom_out_icon' => 'fa fa-search-minus',
		'rfbwp_fb_nav_ss' => '1',
		'rfbwp_fb_nav_ss_order' => '3',
		'rfbwp_fb_nav_ss_delay' => '2000',
		'rfbwp_fb_nav_sap_icon_next' => 'fa fa-chevron-down',
		'rfbwp_fb_nav_sap_icon_prev' => 'fa fa-chevron-up',
		'rfbwp_fb_nav_ss_icon' => 'fa fa-play',
		'rfbwp_fb_nav_ss_stop_icon' => 'fa fa-pause',
		'rfbwp_fb_nav_sap' => '1',
		'rfbwp_fb_nav_sap_order' => '4',
		'rfbwp_fb_nav_sap_icon' => 'fa fa-th',
		'rfbwp_fb_nav_sap_icon_close' => 'fa fa-times',
		'rfbwp_fb_nav_fs' => '1',
		'rfbwp_fb_nav_fs_order' => '5',
		'rfbwp_fb_nav_fs_icon' => 'fa fa-expand',
		'rfbwp_fb_nav_fs_close_icon' => 'fa fa-compress',
		'rfbwp_fb_nav_arrows' => '1',
		'rfbwp_fb_nav_arrows_toolbar' => '1',
		'rfbwp_fb_nav_prev_icon' => 'fa fa-chevron-left',
		'rfbwp_fb_nav_next_icon' => 'fa fa-chevron-right',
		'rfbwp_fb_nav_general' => '1',
		'rfbwp_fb_nav_general_v_padding' => '15',
		'rfbwp_fb_nav_general_h_padding' => '15',
		'rfbwp_fb_nav_general_margin' => '20',
		'rfbwp_fb_nav_general_fontsize' => '22',
		'rfbwp_fb_nav_general_bordersize' => '0',
		'rfbwp_fb_nav_general_shadow' => '0',
		'rfbwp_fb_nav_default' => '1',
		'rfbwp_fb_nav_default_color' => '#2b2b2b',
		'rfbwp_fb_nav_default_background' => '',
		'rfbwp_fb_nav_hover' => '1',
		'rfbwp_fb_nav_hover_color' => '#22b4d8',
		'rfbwp_fb_nav_hover_background' => '',
		'rfbwp_fb_nav_border_default' => '1',
		'rfbwp_fb_nav_border_color' => '',
		'rfbwp_fb_nav_border_radius' => '2',
		'rfbwp_fb_nav_border_hover' => '1',
		'rfbwp_fb_nav_border_hover_color' => '',
		'rfbwp_fb_nav_border_hover_radius' => '2',
		'rfbwp_fb_num' => '0',
		'rfbwp_fb_num_hide' => '1',
		'rfbwp_fb_num_style' => '0',
		'rfbwp_fb_num_background' => '',
		'rfbwp_fb_num_border' => '0',
		'rfbwp_fb_num_border_color' => '',
		'rfbwp_fb_num_border_size' => '2',
		'rfbwp_fb_num_border_radius' => '2',
		'rfbwp_fb_num_v_position' => 'top',
		'rfbwp_fb_num_h_position' => 'center',
		'rfbwp_fb_num_v_padding' => '12',
		'rfbwp_fb_num_h_padding' => '10',
		'rfbwp_fb_num_v_margin' => '12',
		'rfbwp_fb_num_h_margin' => '10',
		'rfbwp_fb_hc' => '',
		'rfbwp_fb_hc_fco' => '',
		'rfbwp_fb_hc_fci' => '',
		'rfbwp_fb_hc_fcc' => '',
		'rfbwp_fb_hc_bco' => '',
		'rfbwp_fb_hc_bci' => '',
		'rfbwp_fb_hc_bcc' => ''
	);

	$fb_table = 'rfbwp_options';
	$fb_options = get_option( $fb_table );
	//$fb_book_id = count( $fb_options['books'] );
   // $fb_book_id = $_REQUEST['activeid'];
	$new_flipbooks = $_REQUEST[ 'flipbook' ];
	$new_flipbookss = str_replace( '%3A', ':', $new_flipbooks);
	$new_flipbooksss = str_replace( '%2F', '/', $new_flipbookss);
	$new_flipbook = array_merge( $base_flipbook, $new_flipbooksss );
    $fb_book_id = $new_flipbook['rfbwp_fb_book_id'];
	$new_sh_id = strtolower( str_replace( '+', '_', $new_flipbook[ 'rfbwp_fb_name' ] ) );
	foreach ( $fb_options[ 'books' ] as $book ) {
		if ( $book[ 'rfbwp_fb_name' ] != '' ) {
			$sh_id = strtolower( str_replace( ' ', '_', $book[ 'rfbwp_fb_name' ] ) );

			//if ( $new_sh_id == $sh_id )
				//$new_flipbook[ 'rfbwp_fb_name' ] = $new_flipbook[ 'rfbwp_fb_name' ] . ' (' . $fb_book_id . ')';
		}
	}

	unregister_setting( $fb_table, $fb_table, 'mp_validate_options' );

	$fb_options[ 'books' ][ $fb_book_id ] = $new_flipbook;

	update_option( $fb_table, $fb_options );

	register_setting( $fb_table, $fb_table, 'mp_validate_options' );

	die(0);
	if(!empty($fb_book_id)){
		return $fb_book_id;
	}
	
}



// doc file to page

  
add_action( 'wp_ajax_rfbwp_uploaddocfile', 'filetopage' );
function filetopage(){
	$bookid = $_POST['id'];
	//$path = 'uploads/'; // upload directory
	//$plugin_dir_path = dirname(__FILE__);    
//	$final_imagess = $bookid.'.docx';
	//$file_name = $path.$final_imagess;
	$document = plugin_dir_path( __FILE__ ). '/uploads/'. $bookid .'.docx';
	 //$path = $path.strtolower($final_imagess); 
	 //$path = $path.strtolower($final_image); 
	$docObj = new DocxConversion($document);
	 
 /*$striped_content = '';
        $content = '';

       echo $zip = zip_open('164ziadfartw.docx');

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);
*/
	 
//$docObj = new DocxConversion("test.docx");
//$docObj = new DocxConversion("test.xlsx");
//$docObj = new DocxConversion("test.pptx");
 $docText= $docObj->convertToText();
 //$length = strlen($docText);
 //echo $length;
   //$document = '164ziadfartw.docx';
  //$ST =  extracttext($document);
 $string = wordwrap($docText, 3000, ";;", true);
 $strings =count(explode(";;", $string)) .'\\n';
// $s = array();
  $dd = '';
 $s = explode(";;", $string);
//echo $document;
 
 global $rfbwp_shortname;
	$option_name = 'rfbwp_options';
	$options = get_option($option_name);
   // $bookid = $_POST['data'];
	$book_id = $bookid;
	$page_type =  'Single Page';

	if(isset($options['books'][$book_id])) {
		if(!isset($options['books'][$book_id]['pages']))
			$options['books'][$book_id]['pages'] = array();

		$pages = $options['books'][$book_id]['pages'];

		$index = 0;
		if(!empty($pages)) {
			$end = end($pages);
			$index = $end['rfbwp_fb_page_type'] == 'Double Page' ? (int)($end['rfbwp_fb_page_index']) + 2 : (int)($end['rfbwp_fb_page_index']) + 1;
		}

		for ($i = 0; $i < $strings; $i++) {
		    $dd =  $s[$i];
 
			$page = array();
			$page['rfbwp_fb_page_type'] = $page_type;
			$page['rfbwp_fb_page_index'] = $index;
			$page['rfbwp_fb_page_custom_class'] = '';
			$page['rfbwp_page_css'] = '';
			$page['rfbwp_page_html'] =  $dd;

			$pages[] = $page;

			if($page_type == 'Double Page')
				$index += 2;
			else
				$index++;
		}

		$options['books'][$book_id]['pages'] = $pages;

		unregister_setting($option_name, $option_name, 'mp_validate_options');

		update_option($option_name, $options);

		register_setting($option_name, $option_name, 'mp_validate_options');
	}
	die();
 
 
 

}


// doc file to page

  class DocxConversion{
    private $filename;

    public function __construct($filePath) {
        $this->filename = $filePath;
    }

    private function read_doc() {
        $fileHandle = fopen($this->filename, "r");
        $line = @fread($fileHandle, filesize($this->filename));   
        $lines = explode(chr(0x0D),$line);
        $outtext = "";
        foreach($lines as $thisline)
          {
            $pos = strpos($thisline, chr(0x00));
            if (($pos !== FALSE)||(strlen($thisline)==0))
              {
              } else {
                $outtext .= $thisline." ";
              }
          }
         $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/","",$outtext);
        return $outtext;
    }

    private function read_docx(){

        $striped_content = '';
        $content = '';

        $zip = zip_open($this->filename);

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);

        return $striped_content;
    }

 /************************excel sheet************************************/

function xlsx_to_text($input_file){
    $xml_filename = "xl/sharedStrings.xml"; //content file name
    $zip_handle = new ZipArchive;
    $output_text = "";
    if(true === $zip_handle->open($input_file)){
        if(($xml_index = $zip_handle->locateName($xml_filename)) !== false){
            $xml_datas = $zip_handle->getFromIndex($xml_index);
            $xml_handle = DOMDocument::loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
            $output_text = strip_tags($xml_handle->saveXML());
        }else{
            $output_text .="";
        }
        $zip_handle->close();
    }else{
    $output_text .="";
    }
    return $output_text;
}

/*************************power point files*****************************/
function pptx_to_text($input_file){
    $zip_handle = new ZipArchive;
    $output_text = "";
    if(true === $zip_handle->open($input_file)){
        $slide_number = 1; //loop through slide files
        while(($xml_index = $zip_handle->locateName("ppt/slides/slide".$slide_number.".xml")) !== false){
            $xml_datas = $zip_handle->getFromIndex($xml_index);
            $xml_handle = DOMDocument::loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
            $output_text .= strip_tags($xml_handle->saveXML());
            $slide_number++;
        }
        if($slide_number == 1){
            $output_text .="";
        }
        $zip_handle->close();
    }else{
    $output_text .="";
    }
    return $output_text;
}


    public function convertToText() {

        if(isset($this->filename) && !file_exists($this->filename)) {
            return "File Not exists";
        }

        $fileArray = pathinfo($this->filename);
        $file_ext  = $fileArray['extension'];
        if($file_ext == "doc" || $file_ext == "docx" || $file_ext == "xlsx" || $file_ext == "pptx")
        {
            if($file_ext == "doc") {
                return $this->read_doc();
            } elseif($file_ext == "docx") {
                return $this->read_docx();
            } elseif($file_ext == "xlsx") {
                return $this->xlsx_to_text();
            }elseif($file_ext == "pptx") {
                return $this->pptx_to_text();
            }
        } else {
            return "Invalid File Type";
        }
    }

}

 function videofil_upload_video(){
	  
	// ini_set('display_errors',0);
try{
	 
	 $upload_dir = wp_upload_dir();
      global $current_user;  get_currentuserinfo();
      if($_FILES['file']){
      /* echo "<pre>";
         print_r($_REQUEST);
      die; */
      $path = $upload_dir['path'];
      $privacyOption = $_REQUEST['privacy-option'];
	  
      
      $xmlFile = pathinfo($_FILES['file']['name']);
      $randValue = random_string();
      $extension = $xmlFile['extension'];

      $fileNameVal = $randValue.'.'.$extension;
      $imageName = $randValue;

      $path=$path.'/'.$fileNameVal;
	   $video = $path;
	  //$video = 'http://localhost/now/dotin/wp-content/uploads/introvideos/5656428078fc7.mp4'; //path to video wp-includes/functions.php
$frame_count = 10; //Amount of frames to render from video


$video_path = pathinfo($video);

      if(move_uploaded_file($_FILES['file']['tmp_name'],$path)){
      	$title     = $_POST['post_title'];
      	$content   = $_POST['post_content'];
      	$video_options = $upload_dir['url'].'/'.$fileNameVal;
      	//exec("ffmpeg -i ".$path." -vf fps=1 ".$upload_dir['path']."/".$imageName.".png");
      	//exec("ffmpeg -y -i ".$path." -ss 00:00:14 -vframes 1 -an -vcodec png -f rawvideo -s 500x270 ".$upload_dir['path']."/".$imageName.".png", $output);
      	$cmd = "/usr/bin/ffmpeg -i    ".$path." -ss   00:00:01.435 -f image2 -vframes 1       /home/app/doityourselfnation.org/bit_bucket/ffmpeg/thumbs/".$imageName."thumb.jpg";
        exec($cmd);
		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                require_once(ABSPATH . "wp-admin" . '/includes/file.php');
                require_once(ABSPATH . "wp-admin" . '/includes/media.php');

      	$title     = $_POST['post_title'];
      	$content   = $_POST['post_content'];


      	$data= 	 array('video_type'      => 'upload',
      	               'video_upload' => $video_options,
      				   'youtube_url' => '',
      				   'embed_code'=>'');
      		// print_r($data);
      		//$post_category = $_POST['post_category'] ;
      	$post = array(
      		'post_title' => $title,
      		'post_content' => $content,
      		//'post_category'=>$post_category,
      		'post_status' => 'publish'
      	);

      	$pid = wp_insert_post($post,true);
      	$video_options_refr = $_POST['ref'];
      	add_post_meta($pid, 'video_options', $data);
      	add_post_meta($pid, 'video_options_refr', $video_options_refr);

      	$post_tmp = get_post( $pid );
      	$author_id = $post_tmp->post_author;

      	if(isset($_POST['dyn_thumbnail'])){
      		if($_POST['dyn_thumbnail'] == 'thumbnail_custom'){
      			if(($_FILES['dyn_thumb_file']['type'] == 'image/jpg')
      				|| ($_FILES['dyn_thumb_file']['type'] == 'image/jpeg')
      				|| ($_FILES['dyn_thumb_file']['type'] == 'image/png')
      				|| ($_FILES['dyn_thumb_file']['type'] == 'image/gif')){
      					$attach_id = media_handle_upload('dyn_thumb_file', 0);
      					set_post_thumbnail( $pid, $attach_id );
      			}
      		}
      		else{
      			//$imagetouse = $upload_dir['path']."/defaultimage.jpeg";
				// $imagetouse = 'http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/defaultimage.jpeg';
				/*$imagetouse = $upload_dir['path']."/".$imageName.".png";

      			// Check image file type
      			$wp_filetype = wp_check_filetype( $imagetouse, null );
      			// Set attachment data
      			$attachment = array(
      				'post_mime_type' => $wp_filetype['type'],
      				'post_title'     => sanitize_file_name( $imagetouse ),
      				'post_content'   => '',
      				'post_status'    => 'inherit'
      			);
      			// Create the attachment
      			$attach_id = wp_insert_attachment( $attachment, $imagetouse, $pid );
      			// Define attachment metadata
      			$attach_data = wp_generate_attachment_metadata( $attach_id, $imagetouse );

      			// Assign metadata to attachment
      			wp_update_attachment_metadata( $attach_id, $attach_data );

      			// And finally assign featured image to post
      			set_post_thumbnail( $pid, $attach_id );*/
			$img_url = "http://www.doityourselfnation.org/bit_bucket/ffmpeg/thumbs/" . $imageName . "thumb.jpg";
				$upload_dir = wp_upload_dir(); // Set upload folder
            $image_data = file_get_contents($img_url); // Get image data
            $filename   = basename($img_url); // Create image file name

            // Check folder permission and define file location
            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }

            // Create the image  file on the server
            file_put_contents( $file, $image_data );
      			// Check image file type
      			$wp_filetype = wp_check_filetype( $filename, null );
      			// Set attachment data
      			$attachment = array(
      				'post_mime_type' => $wp_filetype['type'],
      				'post_title'     => sanitize_file_name( $filename ),
      				'post_content'   => '',
      				'post_status'    => 'inherit'
      			);
      			// Create the attachment
      			$attach_id = wp_insert_attachment( $attachment, $file, $pid );
				
				 // Include image.php
            require_once(ABSPATH . 'wp-admin/includes/image.php');
      			// Define attachment metadata
      			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

      			// Assign metadata to attachment
      			wp_update_attachment_metadata( $attach_id, $attach_data );

      			// And finally assign featured image to post
      			set_post_thumbnail( $pid, $attach_id );
      		}
      	}

      	if(isset($_POST['dyn_background'])){
      		if( $_POST['dyn_background'] == 'background_no' ){
      			if ( ! delete_post_meta( $pid, 'dyn_background_src' ) ) {}
      			//echo '<script>alert("background_no");</script>';
      		}
      		if( $_POST['dyn_background'] == 'background_user' ){
      			if( get_user_meta( $author_id, 'background_image', true ) ){
      				$img = get_user_meta( $author_id, 'background_image' );
      				update_post_meta( $pid, 'dyn_background_src', $img[0] );
      				//echo '<script>alert("background_user - '.$img[0].'");</script>';
      			}
      		}
      		if( $_POST['dyn_background'] == 'background_new' ){
      			if(($_FILES['dyn_bg']['type'] == 'image/jpg')
      				|| ($_FILES['dyn_bg']['type'] == 'image/jpeg')
      				|| ($_FILES['dyn_bg']['type'] == 'image/png')
      				|| ($_FILES['dyn_bg']['type'] == 'image/gif')){
      				$file = 'wp-content/themes/videopress/images/'.rand($author_id, 99999999).$_FILES['dyn_bg']['name'];
      				move_uploaded_file($_FILES['dyn_bg']['tmp_name'], $file);
      				update_post_meta( $pid, 'dyn_background_src', $file);
      				//echo '<script>alert("background_new- '.$file.'");</script>';
      			}else{
      				echo '<div class="alert alert-danger">
      						<span>Background image failed its type is different then jpeg, jpg, gif or png</span>
      					</div>';
      			}
      		}
      	}
		
		
		/*if($_POST['dyn_ad_revenue']){
			if($_POST['dyn_ad_revenue'] == 'yes'){
				$counterval = $_POST['counterval'];
				$date = date('Y-m-d');
				$author = wp_get_current_user();				
				global $wpdb;
				if($counterval){
					for($i=2; $i<=$counterval; $i++){
						$percentageUserID = 'revusers'.$i;
						$percentageUserVal = 'perceval'.$i;
						$wpdb->insert($wpdb->prefix."ad_revenue_share",array( 
							'postType' => 'video',
							'authorID' => $author->ID,
							'getPercentageUserID' => $_POST[$percentageUserID],
							'getPercentageValue' => $_POST[$percentageUserVal],
							'postID' => $pid,
							'date' => $date
						));	
					}
				} 
			}
		}*/

		if(!empty($_REQUEST['select_users_list'])){
		  $select_users_list = implode(",", $_REQUEST['select_users_list']);
		  update_post_meta( $pid, 'select_users_list', $select_users_list);
	    }

      	if(isset($_POST['tagsinput']) && !empty($_POST['tagsinput'])){

      		$tagsinput = $_POST['tagsinput'];
      		wp_set_post_tags( $pid, $tagsinput, true );

      	}
      	the_alert(get_site_url()."/".slug_title($title));

                $user_ID = $current_user->ID;
                $body["message"] = "New video has been uploaded by ".$current_user->display_name.".";
                $body["message"] .= "<h3><a href='".get_site_url()."/".slug_title($title)."' target='_blank' > ".$title."</a></h3>";
                $body["message"] .= "<br />".$content;
                $body["message"] .= "<hr />";
                $body["subject"] = "New video has been uploaded ";
                notification_subscribers($user_ID,$body);
      }
      update_post_meta( $pid, 'privacy-option', $privacyOption);
      
      }
      else if($_POST['youtube_url']){

      	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                require_once(ABSPATH . "wp-admin" . '/includes/file.php');
                require_once(ABSPATH . "wp-admin" . '/includes/media.php');
      	$title     = $_POST['post_title'];
      	$content   = $_POST['post_content'];

      	$data= 	 array('video_type'      => 'youtube_url',
      	               'video_upload' => '',
      				   'youtube_url' => $_POST['youtube_url'],
      				   'embed_code'=>'');

      	//$post_category = $_POST['post_category'] ;
      	$post = array(
      		'post_title' => $title,
      		'post_content' => $post_content,
      		//'post_category'=>$post_category,
      		'post_status' => 'publish'
      	);

      	$pid = wp_insert_post($post,true);
      	$video_options_refr = $_POST['ref'];

      	add_post_meta($pid, 'video_options', $data);
      	add_post_meta($pid, 'video_options_refr', $video_options_refr);
      	the_alert(get_site_url()."/".slug_title($title));
      	$user_ID = $current_user->ID;
                $body["message"] = "New video has been uploaded by ".$current_user->display_name." <a href=".$_POST['youtube_url']." > ".$title."</a>";
                $body["message"] .= "<br />".$content;
                $body["subject"] = "New video has been uploaded ";
                notification_subscribers($user_ID,$body);
      }
      else if($_POST['embed_code']){


      	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                require_once(ABSPATH . "wp-admin" . '/includes/file.php');
                require_once(ABSPATH . "wp-admin" . '/includes/media.php');
      	$title     = $_POST['post_title'];
      	$content   = $_POST['post_content'];

      	$data= 	 array('video_type'      => 'embed_code',
      	               'video_upload' => '',
      				   'youtube_url' => '',
      				   'embed_code'=>$_POST['embed_code']);

      	//$post_category = $_POST['post_category'] ;
      	$post = array(
      		'post_title' => $title,
      		'post_content' => $post_content,
      		//'post_category'=>$post_category,
      		'post_status' => 'publish'
      	);

      	$pid = wp_insert_post($post,true);
      	$video_options_refr = $_POST['ref'];

      	add_post_meta($pid, 'video_options', $data);
      	add_post_meta($pid, 'video_options_refr', $video_options_refr);
      	the_alert(get_site_url()."/".slug_title($title));
      }	  
	

}catch(Exception $e){
	echo '<div class="error">';
	echo $e->getMessage();
	echo '</div>';
}

die();
}

