<?php
include '../../../wp-load.php';
if(is_user_logged_in()){
	global $wpdb;
	
	/// Collaboration delete code
	if(isset($_POST['remove_collaboration']) && $_POST['remove_collaboration'] == 'remove_collaboration'){
		if(isset($_POST['collaboration_id'])){
			wp_delete_post( $_POST['collaboration_id'] );
			$wpdb->get_results("DELETE FROM ".$wpdb->prefix."authors_collaboration_list WHERE collaboration_post_id='".$_POST['collaboration_id']."' ");
		}
	}
	
	/// Collaboration shared code
	if(isset($_POST['collaboration_post_id']) && $_POST['collaboration_post_id'] > 0){
		if(isset($_POST['from_collaboration_id'])){
			if(count($_POST['to_collaboration_id']) > 0){
				foreach($_POST['to_collaboration_id'] as $to_collaboration_id){
					
					$result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."authors_collaboration_list WHERE collaboration_post_id='".$_POST['collaboration_post_id']."' AND to_collaboration_id='".$to_collaboration_id."' ");					
					if(count($result) == 0){
					$wpdb->insert(
						$wpdb->prefix."authors_collaboration_list",
							array(
								'collaboration_post_id' 	=> $_POST['collaboration_post_id'],
								'from_collaboration_id' 	=> $_POST['from_collaboration_id'],
								'to_collaboration_id'		=> $to_collaboration_id
							)
						);
					}
				}
			}
		}
	}
	
	if(!empty($_POST['collaboration_type'])){
		
		//print_r($_POST);
		$from_collaboration_id		= $_POST['from_collaboration_id'];
		$to_collaboration_id		= $_POST['to_collaboration_id'];
		$collaboration_type			= $_POST['collaboration_type'];
		$collaboration_msg			= $_POST['collaboration_msg'];

		$wpdb->insert(
			$wpdb->prefix."authors_collaboration_list",
			array(
				'from_collaboration_id' 	=> $from_collaboration_id,
				'to_collaboration_id'		=> $to_collaboration_id,
				'collaboration_type'		=> $collaboration_type,
				'collaboration_msg'			=> $collaboration_msg
			)
		);
		//$_POST['add_collaboration_submit'] = "";
	}
	
}