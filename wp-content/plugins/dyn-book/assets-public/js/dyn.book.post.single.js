// /includes/file-book.php
(function($){
	
	  //click file then make it full screen
	 $('.dyn-book-single-click').on("click",function(event) {
		 event.preventDefault();
		 //alert($('#rfbwp_fullscreen .flipbook-container').length);
		 $('.dyn-book-single-fullscreen').toggle();
		 if ($('#rfbwp_fullscreen .flipbook-container').length==0) {
			$('.dyn-book-single-fullscreen ul li.fullscreen').trigger('click');
		 }
	 });
	 
	 
	 // exit when fullsceen
	 $('.dyn-file-exit').on("click",function(event) {
		event.preventDefault();
		//alert($('#rfbwp_fullscreen .flipbook-container').length);
		$('.dyn-book-single-fullscreen').toggle();
		if ($('#rfbwp_fullscreen .flipbook-container').length>0) {
			
			$('.dyn-book-single-fullscreen ul li.fullscreen').trigger('click');
		}
		 
	 });
})(jQuery);

