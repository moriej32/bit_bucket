<?php
/****
Because this bottom function make too heavy sql query  and conflict with other plugins  i have disabled it, 
so the trick for searchable is saving dyn-book-mb-post to post->content
using add_action( 'save_post', and remove_action( 'save_post', 
and not showing editor when edit dyn_book. This bottom functions is only for emergency record if the content is made empty again.
***/

function dyn_book_post_search_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
	  
	  $query->set( 'meta_query', array(
			'relation' => 'OR',
			array(
				'key' => 'dyn-book-mb-post',
				'value' => $query->query_vars['s'],
				'compare' => 'like',
			)
		));
	  
    }
  }
}
add_action('pre_get_posts','dyn_book_post_search_filter');

function dyn_book_postchange_meta_key_where_from_and_to_or($sql) {
  if(is_search()) {
    $sql['where'] = substr($sql['where'], 1, 3) == 'AND' ? substr_replace($sql['where'], 'OR', 1, 3) : $sql['where'];
  }
  return $sql;
}
add_filter('get_meta_sql', 'dyn_book_postchange_meta_key_where_from_and_to_or');


?>