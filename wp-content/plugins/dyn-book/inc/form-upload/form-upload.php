
<?php
//print_r($atts);
	$collaboration_id = '';
	if(@$atts != '' && count($atts) > 0){
		$collaboration_id = "<input type='hidden' name='collaboration_id' value='".$atts['collaboration_id']."' />";
	}
?>
<div id="dny-book-form-create-message" class="alert alert-success">
	<span>Book Uploaded successfully. <a href="">View File</a></span>
</div>
<form id="dny-book-form-create" method="post">	
	<?php echo $collaboration_id; ?>
	<div class="form-group">
		<label>Title </label>
		<div>
			<input class="form-control" type="text" name="post-title" value="" required>
			<input class="form-control" type="hidden" name="profile_id" value="<?php  echo get_current_user_id();  ?>">
			
		</div>
	</div>

	<div>
		<a href="#" class="dbu-add-item">ADD PAGES</a>
	</div>
	<div id="group-dbu">		
		<?php
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/field-repeater.php';
		require_once($path_file);
		?>
	</div>
	
	<?php
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/field-setting.php';
		require_once($path_file);
	?>
	<div class="form-group">
		<label for="dyn-select-description">Description:</label>
		<textarea name="dyn-select-description" id="dyn-select-description" class="form-control" rows="3"></textarea>
	</div>
	<div class="form-group">
		<label>Reference:</label>
		<textarea class="form-control" rows="2" class="form-control" name="dyn-select-reference"></textarea>
	</div>
	<div class="form-group">
		<label for="dyn-tags">Tags:</label>
		<input type="text" class="dyn-select-tags" name="dyn-select-tags">
		<span>Enter tags separated by space</span>
	</div>
	<div class="form-group">
		<label for="dyn-tags">Add Background:</label>					
		<div class="checkbox">
			<label>White Background 
			<input type="radio" checked="checked" value="background_no" class="dyn_checkbox" name="dyn_background"></label>
			<label>Profile Background 
			<input type="radio" value="background_user" class="dyn_checkbox" name="dyn_background"></label>
			<label>New Background 
			<input type="radio" value="background_new" class="dyn_checkbox" name="dyn_background"></label>
		</div>
	</div>
	<div id="upload_container">
		<div class="form-group">
		<label for="dyn_bg">Upload File:</label>
			<?php
				$random = rand(1,100000);
			?>

			<div class="dynbookimage-fields">
				<div id="dynbookimage-featured_image-upload-container-<?php echo $random; ?>">
					<div class="dynbookimage-attachment-upload-filelist-<?php echo $random; ?>" data-type="file" data-required="yes">
						<a id="dynbookimage-featured_image-pickfiles-<?php echo $random; ?>" data-form_id="18" class="button file-selector  dynbookimage_featured_image_18" href="#">Select Image</a>

						<ul class="dynbookimage-attachment-list thumbnails">
						</ul>
						<div class="dynbookimage-attachment-list-temp" data-field-1="dyn_bg" data-field-2="dyn_bg_id">
						</div>
					</div>
				</div><!-- .container -->

				<span class="dynbookimage-help"></span>

			</div> <!-- .dynbookimage-fields -->

        <script type="text/javascript">
            jQuery(function($) {
                new DYNIMAGE_Uploader('dynbookimage-featured_image-pickfiles-<?php echo $random; ?>', 'dynbookimage-featured_image-upload-container-<?php echo $random; ?>', 1, 'featured_image', 'jpg,jpeg,gif,png,bmp', 1024,'dyn_bg','dyn_bg_id');
            });
        </script>
			
			
		</div>
	</div>
	
	<div class="form-group">
	  <label for="dyn-ad-revenue">Ad Revenue Share:</label>
	  <div class="checkbox">
		 <label>Yes
		 <input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox" value="yes"></label>
		 <label>No
		 <input type="radio" name="dyn_ad_revenue" class="dyn_ad_revenue_checkbox" value="no" checked="checked"></label>
	  </div>
	  <div class="revnue" style="display:none">
	  <label for="revenue-paypal-email">Add Paypal Email</label>
	  <input type="email" required name="revenue-paypal-email" class="revenue-paypal-email" id="revenue-paypal-email">
	  <input type="button" value="Add" class="revenue-paypal">
	  <div id="error-msg"></div>
	  <div id="loading" style="display:none"><img id="loadingImage" src="<?php echo get_template_directory_uri();?>/images/ajax-loader.gif"></div>
	  <table id="author-lists" class="author-lists"><tr><th></th><th><b>Users</b></th><th><b> % of Revenue</b></th></tr><tr><td></td><td><p><?php echo $current_user->display_name;?></p></td><td><input id="chnl-perc-val" type="text" value="100" readonly />%</td></tr></table>
	  </div>
   </div>
	
	<script type="text/javascript">
		jQuery(document).ready(function() {
			
			jQuery("#upload_container").hide();
			jQuery(".dyn_checkbox").click( function() {
				var test = jQuery(this).val();
				if( test == 'background_new' ){
					jQuery("#upload_container").show();
				}else{
					jQuery("#upload_container").hide();
				}
			});
		});
	</script>
	<div>
		<input type="hidden" name="action" value="dyn_book_create_post" />
		<input type="submit" value="PREVIEW">
	</div>
</form>
<?php
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/panel/mpc_icon/icon_select/icon_grid.php';
		require_once($path_file);
?>