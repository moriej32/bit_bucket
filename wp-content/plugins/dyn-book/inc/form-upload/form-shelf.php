<?php
$paged = 1;
$profile_id = get_current_user_id();
$args = array(
	'post_type' => array('dyn_book'),
	'ignore_sticky_posts'=>1,
	'paged'=>$paged,
	'posts_per_page' => 10,
	'author' => $profile_id
);
//the loop  
echo '<br />';
$loop = new WP_Query($args); 
while ( $loop->have_posts() ) : $loop->the_post(); 
	echo '<a href="'. get_the_permalink() .'">'. get_the_title() . '</a>';
	echo ' | <a href="'. str_replace('=shelf','=edit',$_SERVER['REQUEST_URI']) . '&post_id=' . get_the_ID() . '&profile_id='. get_current_user_id() .'">Edit</a>';
	echo ' | <a href="'. str_replace('=shelf','=delete',$_SERVER['REQUEST_URI']) . '&post_id=' . get_the_ID() . '&profile_id='. get_current_user_id() .'">Delete</a>';
	echo '<br />';
endwhile;
wp_reset_query();
wp_reset_postdata();
?>