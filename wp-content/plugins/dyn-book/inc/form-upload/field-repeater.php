<div class="group-dbu-repeated-sections">
	<div class="dbu-repeated-section">
	  <div class="dbu-heading dbu-heading-show-hide">
		<b>BOOK ITEM : PAGE</b>
	  </div>
	  <div class="dbu-repeated-item">	
		  <div class="form-group"> 	
			<label>Page Type</label>
			  <select class="form-control" name="rfbwp_fb_page_type[]">
				<option value="Single Page" selected>Single Page</option>
				<option value="Double Page">Double Page</option>
			  </select>
		  </div>
		   <div class="form-group">
			<label>Page Background</label>
			<?php
				$random = rand(1,100000);
			?>

			<div class="dynbookimage-fields">
				<div id="dynbookimage-featured_image-upload-container-<?php echo $random; ?>">
					<div class="dynbookimage-attachment-upload-filelist-<?php echo $random; ?>" data-type="file" data-required="Yes">
						<a id="dynbookimage-featured_image-pickfiles-<?php echo $random; ?>" data-form_id="18" class="button file-selector  dynbookimage_featured_image_18" href="#">Select Image</a>

						<ul class="dynbookimage-attachment-list thumbnails">
						</ul>
						<div class="dynbookimage-attachment-list-temp" data-field-1="rfbwp_fb_page_bg_image[]" data-field-2="rfbwp_fb_page_bg_image_id[]">
						</div>
					</div>
				</div><!-- .container -->

            <span class="dynbookimage-help"></span>

        </div> <!-- .dynbookimage-fields -->

        <script type="text/javascript">
            jQuery(function($) {
                new DYNIMAGE_Uploader('dynbookimage-featured_image-pickfiles-<?php echo $random; ?>', 'dynbookimage-featured_image-upload-container-<?php echo $random; ?>', 1, 'featured_image', 'jpg,jpeg,gif,png,bmp', 5024,'rfbwp_fb_page_bg_image[]','rfbwp_fb_page_bg_image_id[]');
            });
        </script>
		
		  </div>
		   <div class="form-group">
			  <label> HiRes Background </label>
			  <?php
				$random = rand(1,100000);
			?>

			<div class="dynbookimage-fields">
				<div id="dynbookimage-featured_image-upload-container-<?php echo $random; ?>">
					<div class="dynbookimage-attachment-upload-filelist-<?php echo $random; ?>" data-type="file" data-required="Yes">
						<a id="dynbookimage-featured_image-pickfiles-<?php echo $random; ?>" data-form_id="18" class="button file-selector  dynbookimage_featured_image_18" href="#">Select Image</a>

						<ul class="dynbookimage-attachment-list thumbnails">
						</ul>
						<div class="dynbookimage-attachment-list-temp" data-field-1="rfbwp_fb_page_bg_image_zoom[]" data-field-2="rfbwp_fb_page_bg_image_zoom_id[]">
						</div>
					</div>
				</div><!-- .container -->

            <span class="dynbookimage-help"></span>

        </div> <!-- .dynbookimage-fields -->

        <script type="text/javascript">
            jQuery(function($) {
                new DYNIMAGE_Uploader('dynbookimage-featured_image-pickfiles-<?php echo $random; ?>', 'dynbookimage-featured_image-upload-container-<?php echo $random; ?>', 1, 'featured_image', 'jpg,jpeg,gif,png,bmp', 5024,'rfbwp_fb_page_bg_image_zoom[]','rfbwp_fb_page_bg_image_zoom_id[]');
            });
        </script>
			
		  </div>
		  <div class="form-group">
			<label>Page Title</label>
			<input class="form-control" type="text" name="rfbwp_fb_page_title[]" value="">
		  </div>
		  <div class="form-group">
			<label>Page Index</label>
			<input class="form-control" type="text" name="rfbwp_fb_page_index[]" value="">
		  </div>
		  <div class="form-group">
			<label>Page URL</label>
			<input class="form-control" type="text" name="rfbwp_fb_page_url[]" value="">
		  </div>
		  <div class="form-group">
			<label>Left Page Content</label>
			<textarea class="form-control html-editor"  name="rfbwp_page_html[]"></textarea>
		  </div>
		  <div class="form-group">
			<label>Right Page Content</label>
			<textarea class="form-control html-editor" name="rfbwp_page_html_second[]"></textarea>
		  </div>
		  <div class="form-group">
			<label>Page Custom Class</label>
			<input class="form-control" type="text" name="rfbwp_fb_page_custom_class[]" value="">
		  </div>
		  <div class="form-group">
			<label>Page CSS</label>
			<textarea class="form-control" name="rfbwp_fb_page_css[]"></textarea>
		  </div>
		  <div>
			
			<a href="#" class="dbu-add"> Add </a> | <a href="#" class="dbu-remove"> Remove </a> 
		  </div>
	  </div>
	  
	</div>
	
</div>
