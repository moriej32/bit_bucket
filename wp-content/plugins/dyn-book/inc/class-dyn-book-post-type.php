<?php
class Dyn_Book_Post_Type {
	
	function __construct() {
		add_action( 'init', array( $this, 'dyn_book_post_type_register') );
		//add_action( 'save_post', array( $this, 'dyn_book_post_type_update_content_from_meta') );
	
	}
	
	/**
	 * Register a book post type.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	function dyn_book_post_type_register(){
		$labels = array(
			'name'               => _x( 'Books', 'post type general name', 'dyn-book-plugin' ),
			'singular_name'      => _x( 'Book', 'post type singular name', 'dyn-book-plugin' ),
			'menu_name'          => _x( 'Books', 'admin menu', 'dyn-book-plugin' ),
			'name_admin_bar'     => _x( 'Dyn Book', 'add new on admin bar', 'dyn-book-plugin' ),
			'add_new'            => _x( 'Add New', 'dyn-book-plugin', 'dyn-book-plugin' ),
			'add_new_item'       => __( 'Add New Book', 'dyn-book-plugin' ),
			'new_item'           => __( 'New Book', 'dyn-book-plugin' ),
			'edit_item'          => __( 'Edit Book', 'dyn-book-plugin' ),
			'view_item'          => __( 'View  Book', 'dyn-book-plugin' ),
			'all_items'          => __( 'All Books', 'dyn-book-plugin' ),
			'search_items'       => __( 'Search Books', 'dyn-book-plugin' ),
			'parent_item_colon'  => __( 'Parent Books:', 'dyn-book-plugin' ),
			'not_found'          => __( 'No books found.', 'dyn-book-plugin' ),
			'not_found_in_trash' => __( 'No books found in Trash.', 'dyn-book-plugin' )
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Description.', 'dyn-book-plugin' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'books' ,'with_front' => false),
			'capability_type'    => 'post',
			'taxonomies' 		 => array('category', 'post_tag'),
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => null,
			'supports'           => array( 'title','editor','thumbnail' )
		);

		register_post_type( 'dyn_book', $args );
	}


	/***
	Saving dyn-book-mb-post to post_content (so it can be searchable from search.php)
	***/
	function dyn_book_post_type_update_content_from_meta( $post_id ) {

			// If this is just a revision, don't send the email.
			if ( wp_is_post_revision( $post_id ) )
				return;
			$post_type = get_post_type($post_id);
			if ($post_type=='dyn_post') {	
				$dyn_nb_book_post_json =  json_encode(get_post_meta($post_id,'dyn-book-mb-post'));
				
				$my_post = array();
				$my_post['ID'] = $post_id;
				$my_post['post_content'] = $dyn_nb_book_post_json;
				
				remove_action( 'save_post', array( $this, 'dyn_book_post_type_update_content_from_meta') );
				wp_update_post( $my_post );
				add_action( 'save_post', array( $this, 'dyn_book_post_type_update_content_from_meta') );
			}
	}
	
}

new Dyn_Book_Post_Type();
?>