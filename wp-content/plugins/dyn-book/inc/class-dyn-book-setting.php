<?php
class DYN_Book_Setting {
	function __construct() {
		add_action('admin_menu' , array($this,'dyn_book_enable_pages')); 
	}
	function dyn_book_enable_pages() {
		add_submenu_page('edit.php?post_type=dyn_book', 'Custom Post Type Admin', 'Book Settings', 'edit_posts', basename(__FILE__), array($this,'book_setting_function'));
	}
	function book_setting_function(){
		$dyn_book_upload_page_id =  get_option('dyn_book_upload_page_id','');
		if (isset($_POST['save'])) {
			$dyn_book_upload_page_id =$_POST['dyn_book_upload_page_id'];
			update_option( 'dyn_book_upload_page_id', $dyn_book_upload_page_id);
			
		}
?>
    <div class="wrap">
        <h2>Book Settings</h2>
 
        <form method="POST" action="">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            Upload Book Page ID
                        </label> 
                    </th>
                    <td>
                        <input type="text" name="dyn_book_upload_page_id" size="5" value="<?php echo $dyn_book_upload_page_id; ?>" />
                    </td>
                </tr>
				<tr>
					<td>
						<input type="hidden" name="save" value="save"/>
						<input type="submit" value="Save settings" class="button-primary"/>
						
					</td>
				</tr>
            </table>
        </form>
    </div>
<?php
}
	
	
}
new DYN_Book_Setting();
?>