<?php
class Dyn_Book_Post_Create {
	
	// Shortcode and ajax action in wp-admin.php
	function __construct() {
		add_shortcode('dyn-book-upload', array($this, 'dyn_book_upload_form'));
		add_shortcode('dyn-book-upload-collaboration', array($this, 'dyn_book_upload_form_collaboration'));
		
		//ajax field repeater
		add_action('wp_ajax_dyn_book_section_repeater', array($this, 'dyn_book_section_repeater_ajax'));
		add_action('wp_ajax_nopriv_dyn_book_section_repeater', array($this, 'dyn_book_section_repeater_ajax'));
		
		//ajax create post
		add_action('wp_ajax_dyn_book_create_post', array($this, 'dyn_book_create_post_ajax'));
		add_action('wp_ajax_nopriv_dyn_book_create_post', array($this, 'dyn_book_create_post_ajax'));
		//ajax edit post
		
		//ajax create post
		add_action('wp_ajax_dyn_book_edit_post', array($this, 'dyn_book_edit_post_ajax'));
		add_action('wp_ajax_nopriv_dyn_book_edit_post', array($this, 'dyn_book_edit_post_ajax'));
		//ajax edit post
		
		//ajax delete post 
		add_action('wp_ajax_dyn_book_delete_post', array($this, 'dyn_book_delete_post_ajax'));
		add_action('wp_ajax_nopriv_dyn_book_delete_post', array($this, 'dyn_book_delete_post_ajax'));
		//ajax delete post ends
	}
	
	
	// Register Javascript and CSS
	function handle_js_css_upload() {
		wp_enqueue_style( 'rfbwp-et_icons',DYN_BOOK_PLUGIN_URL.'assets/fonts/et-icons.css' );
		wp_enqueue_style( 'rfbwp-et_line',DYN_BOOK_PLUGIN_URL.'assets/fonts/et-line.css' );
		wp_enqueue_style( 'dyn-book-upload',DYN_BOOK_PLUGIN_URL.'assets-admin/css/dyn-book-upload.css' );
		wp_enqueue_style( 'dyn-book-setting',DYN_BOOK_PLUGIN_URL.'assets-admin/css/mp-styles.css' );
		wp_enqueue_script('jquery');
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script('jquery-ui-dialog');
		wp_enqueue_style("wp-jquery-ui-dialog");
		wp_enqueue_script('dyn_mp_theme_tinymce_js', DYN_BOOK_PLUGIN_URL.'inc/form-upload/panel/tinymce/tinymce.min.js', array('jquery'), false, true);
	
		wp_enqueue_script('dyn-book-setting',DYN_BOOK_PLUGIN_URL.'assets-admin/js/dyn.book.setting.js', array('jquery'), '1.0', true);
		wp_enqueue_script('dyn-book-create',DYN_BOOK_PLUGIN_URL.'assets-admin/js/dyn.book.create.js', array('jquery'), '1.0', true);
		
		// Googel Web Font
		wp_enqueue_script('webfonts', '//ajax.googleapis.com/ajax/libs/webfont/1.1.2/webfont.js');

		/* Localize */
		$google_webfonts = get_transient('dyn_mpcth_google_webfonts');
	
		// Get current page protocol
		$protocol = isset( $_SERVER["HTTPS"] ) ? 'https://' : 'http://';
		// Output admin-ajax.php URL with same protocol as current page
		$params = array(
			'ajaxurl' => admin_url( 'admin-ajax.php', $protocol ),
			'create' => 'dyn_book_create_post',
			'update' => 'dyn_book_update_post',
			'delete' => 'dyn_book_delete_post',
			'presetsURL' => DYN_BOOK_PLUGIN_URL .'/inc/form-upload/panel/presets/',
			'googleAPIKey'		=> DYN_MPC_RFBWP_GOOGLE_FONTS_API_ID,
			'googleFonts'		=> stripslashes( $google_webfonts ),
			'googleAPIErrorMsg' => __('There is problem with access to Google Webfonts. Please try again later. If this message keeps appearing please contact our support at <a href="http://mpc.ticksy.com/">mpc.ticksy.com</a>.', 'dyn_book_plugin'),
		);
		wp_localize_script( 'dyn-book-setting', 'DYN_BOOK', $params );
		
		
		/* COLOR PICKER */
		wp_enqueue_style( 'wp-color-picker' );  // color picker
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script(
			'iris',
			admin_url( 'js/iris.min.js' ),
			array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
			false,
			1
		);
		wp_enqueue_script(
			'wp-color-picker',
			admin_url( 'js/color-picker.min.js' ),
			array( 'iris' ),
			false,
			1
		);
		$colorpicker_l10n = array(
			'clear' => __( 'Clear' ),
			'defaultString' => __( 'Default' ),
			'pick' => __( 'Select Color' ),
			'current' => __( 'Current Color' ),
		);
		wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n ); 
		
		/* UPLOAD IMAGE */
		wp_enqueue_script( 'plupload-handlers' );
		global $wp_version;
		if (version_compare($wp_version, '4.2.4', '<=')) {
			// version is 4.2.4 or lower
			wp_enqueue_script( 'dynbookimage-upload', DYN_BOOK_PLUGIN_URL . 'assets-admin/js/dyn.book.img.upload.oldwp.js', array('jquery', 'plupload-handlers') );
			
		} else {
			wp_enqueue_script( 'dynbookimage-upload', DYN_BOOK_PLUGIN_URL . 'assets-admin/js/dyn.book.img.upload.js', array('jquery', 'plupload-handlers') );
			
        
		}
		
        wp_localize_script( 'dynbookimage-upload', 'dynbookimage_frontend_upload', array(
            'confirmMsg' => __( 'Are you sure?', 'wpuf' ),
            'nonce'      => wp_create_nonce( 'wpuf_nonce' ),
            'ajaxurl'    => admin_url( 'admin-ajax.php' ),
            'plupload'   => array(
                'url'              => admin_url( 'admin-ajax.php' ) . '?nonce=' . wp_create_nonce( 'dynbookimage-upload-nonce' ),
                'flash_swf_url'    => includes_url( 'js/plupload/plupload.flash.swf' ),
                'filters'          => array(array('title' => __( 'Allowed Files' ), 'extensions' => '*')),
                'multipart'        => true,
                'urlstream_upload' => true,
            )
        ));
		/* UPLOAD IMAGE ENDS */
	}
	
	// Upload Book Form 
	function dyn_book_upload_form_collaboration( $atts, $content = null ) {
		
		if ( !is_user_logged_in() ) {
			$temp = 'You have no right to access this page. Please login first';
			return $temp;
		}
		
		$this->handle_js_css_upload();
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/form-upload.php';
		if (file_exists($path_file)) {
			ob_start();
			require_once $path_file;
			$file_content = ob_get_clean();
			$file_content = $menu_content . $file_content;
			return $file_content;
		}
		
	}
	
	function dyn_book_upload_form( $atts, $content = null ) {
		if ( !is_user_logged_in() ) {
			$temp = 'You have no right to access this page. Please login first';
			return $temp;
		}
		
		
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/form-choose.php';
		$menu_content = '';
		if (file_exists($path_file)) {
			 ob_start();
			 require_once $path_file;
			 $menu_content = ob_get_clean();
			 
		}
		
		if (isset($_REQUEST['form-dyn']))
		{
		
			$form_dyn = $_REQUEST['form-dyn'];
		
			if ($form_dyn =='create') {
				$this->handle_js_css_upload();
				$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/form-upload.php';
				if (file_exists($path_file)) {
					 ob_start();
					 require_once $path_file;
					 $file_content = ob_get_clean();
					 $file_content = $menu_content . $file_content;
					 return $file_content;
				}
			} else if ($form_dyn =='pdf') {
				$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/form-pdf.php';
				if (file_exists($path_file)) {
					 ob_start();
					 require_once $path_file;
					 $file_content = ob_get_clean();
					  $file_content = $menu_content . $file_content;
					 return $file_content;
				}
			
			} else if ($form_dyn =='shelf') {
				$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/form-shelf.php';
				if (file_exists($path_file)) {
					 ob_start();
					 require_once $path_file;
					 $file_content = ob_get_clean();
					  $file_content = $menu_content . $file_content;
					 return $file_content;
				}
			} else if ($form_dyn =='edit') {
				$this->handle_js_css_upload();
				$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/form-edit.php';
				if (file_exists($path_file)) {
					 ob_start();
					 require_once $path_file;
					 $file_content = ob_get_clean();
					  $file_content = $menu_content . $file_content;
					 return $file_content;
				}
			} 
		}
		return $menu_content;
	}
	
	
	// ajax repeater field
		// Repeater Field
	function dyn_book_section_repeater_ajax() {
		$path_file =  DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/field-repeater.php';
		require_once $path_file;
		die();
		
	}
		
	function dyn_book_delete_post_ajax(){
		if(isset($_POST['file_id']) && $_POST['file_id']!=''){
			$profile_id = $_POST['profile_id'];
				
			if(get_current_user_id() ==  $profile_id){
				$data['msg_type']= 'success';
				$file_id = $_POST['file_id'];
				global $wpdb;
				wp_delete_post($file_id);
				$data['file_id'] = $file_id;
				//die();
			}
			else{
				$data['msg_type']= 'error';
			}
			
			echo json_encode($data);die();
		}
	}
	
	// ajax create post 
	function dyn_book_create_post_ajax(){
		//print_r($_POST);die();
		
		$profile_id = $_POST['profile_id'];
		if ($profile_id!=get_current_user_id()) {
			$response = array(
                'success'      => false,
                'message'  => 'Posting Error : You have no right for posting book. Please login first'
			 );
			echo json_encode( $response );
			die();
		}
				
		// meta repeater
		$rfbwp_fb_page_type = $_POST['rfbwp_fb_page_type'];
		$rfbwp_fb_page_bg_image = $_POST['rfbwp_fb_page_bg_image'];
		$rfbwp_fb_page_bg_image_zoom = $_POST['rfbwp_fb_page_bg_image_zoom'];
		$rfbwp_fb_page_title = $_POST['rfbwp_fb_page_title'];
		$rfbwp_fb_page_index = $_POST['rfbwp_fb_page_index'];
		$rfbwp_fb_page_url = $_POST['rfbwp_fb_page_url'];
		$rfbwp_page_html = $_POST['rfbwp_page_html'];
		$rfbwp_page_html_second = $_POST['rfbwp_page_html_second'];
		$rfbwp_fb_page_custom_class = $_POST['rfbwp_fb_page_custom_class'];
		$rfbwp_fb_page_css = $_POST['rfbwp_fb_page_css'];
		
		foreach ($rfbwp_fb_page_title as $key => $value) {
			$book_group[$key]['rfbwp_fb_page_type'] = $rfbwp_fb_page_type[$key];
			$book_group[$key]['rfbwp_fb_page_bg_image'] = $rfbwp_fb_page_bg_image[$key];
			$book_group[$key]['rfbwp_fb_page_bg_image_zoom'] = $rfbwp_fb_page_bg_image_zoom[$key];
			$book_group[$key]['rfbwp_fb_page_title'] = $rfbwp_fb_page_title[$key];
			$book_group[$key]['rfbwp_fb_page_index'] = $rfbwp_fb_page_index[$key];
			$book_group[$key]['rfbwp_fb_page_url'] = $rfbwp_fb_page_url[$key];
			$book_group[$key]['rfbwp_page_html'] = $rfbwp_page_html[$key];
			$book_group[$key]['rfbwp_page_html_second'] = $rfbwp_page_html_second[$key];
			$book_group[$key]['rfbwp_fb_page_custom_class'] = $rfbwp_fb_page_custom_class[$key];
			$book_group[$key]['rfbwp_fb_page_css'] = $rfbwp_fb_page_css[$key];
		}
		
		// post
		
		$post_title = $_POST['post-title'];
		$description = $_POST['dyn-select-description'];
		$tags = trim($_POST['dyn-select-tags']);
		$reference = trim($_POST['dyn-select-reference']);
		// check authority and permissions: current_user_can()
		// check intention: wp_verify_nonce()
		
		/* create post and get it */
		
		$dyn_post_id = wp_insert_post(
					array(
						'post_title' => $post_title,
						'post_type' => 'dyn_book',
						'post_content' => $description,
						'post_status' => 'draft',
						'post_author' => $profile_id
					)
		);
		
		// set featured image
		//set_post_thumbnail
		$rfbwp_fb_page_bg_image_id = $_POST['rfbwp_fb_page_bg_image_id'];
		if ($rfbwp_fb_page_bg_image_id[0]!='') {
			set_post_thumbnail($dyn_post_id,$rfbwp_fb_page_bg_image_id[0]);
		}
		
		// metabox dyn-book-mb-post
		//$book_item['dyn-book-show'] = 1;
		$book_item['pages'] = $book_group;
		
		update_post_meta( $dyn_post_id, 'dyn-book-mb-post', $book_item);
		$collaboration_post_id   = @$_POST['collaboration_id'];				
		update_post_meta( $dyn_post_id, 'collaboration_id', $collaboration_post_id);
		// metabox dyn-book-mb-setting  
		$book_setting = $_POST['rfbwp_options'];
		if ($book_setting['books'][0]['rfbwp_fb_height']=='') {
			$book_setting['books'][0]['rfbwp_fb_height'] = 400;
		}
		if ($book_setting['books'][0]['rfbwp_fb_width']=='') {
			$book_setting['books'][0]['rfbwp_fb_width'] = 300;
		}
		$book_setting['books'][0]['rfbwp_fb_name'] = $dyn_post_id;
		
		update_post_meta( $dyn_post_id, 'dyn-book-mb-settings', $book_setting);
		$book_setting['books'][0]['pages'] = $book_item['pages'];
		
		$book_options['books'][$dyn_post_id] = $book_setting['books'][0];
		update_post_meta( $dyn_post_id, 'dyn-book-mb-options', $book_options);
		
		// update content so searchable
		/*
		$dyn_nb_book_post_json =  json_encode(get_post_meta($dyn_post_id,'dyn-book-mb-post'));
		wp_update_post( 
			array(
				'ID'=>$dyn_post_id,
				'post_content'=>$dyn_nb_book_post_json
			)
		);
		*/

		if(!empty($tags)){
					$tags = explode(' ', $tags);
					wp_set_post_tags( $dyn_post_id, $tags );
		}
		update_post_meta( $dyn_post_id, 'video_options_refr', $reference );
		
		/*
		update_post_meta( $dyn_post_id, 'dyn_upload_type', 'dyn-file-upload' );
		update_post_meta( $dyn_post_id, 'dyn_upload_value', $attachment_id );
		*/

		
		// Background Page
		if( isset( $_POST['dyn_background'] ) ){
					if( $_POST['dyn_background'] == 'background_no' ){						
						if ( ! delete_post_meta( $dyn_post_id, 'dyn_background_src' ) ) {
							
						}
						
					}
					if( $_POST['dyn_background'] == 'background_user' ){
						if( get_user_meta( $profile_id, 'background_image', true ) ){
							$img = get_user_meta( $profile_id, 'background_image' );
							update_post_meta( $dyn_post_id, 'dyn_background_src', $img[0] );						
						}
					}
					if( $_POST['dyn_background'] == 'background_new' ){
						if (isset($_POST['dyn_bg_id'])) {
							$attach_id = $_POST['dyn_bg_id'];
							$image_full = wp_get_attachment_image_src( $attach_id, 'full' );
							$image_full = $image_full[0];
				
							$image_full = str_replace(get_bloginfo('url').'/','',$image_full);
							update_post_meta( $dyn_post_id, 'dyn_background_src', $image_full);
						}
					}
					

		}
		
		// redirect 
		$redirect_to = get_permalink( $dyn_post_id ).'&preview=true';
		$response = array(
                'success'      => true,
                'redirect_to'  => $redirect_to,
				'message'	=> '<span>Book Uploaded successfully. <a href="'. $redirect_to .'">View File</a></span>'
         );
		echo json_encode( $response );
		die();
	}
	
	// ajax edit post
	function dyn_book_edit_post_ajax(){
		$dyn_post_iddd = $_POST['post_id'];
		$profile_id = $_POST['profile_id'];
		if ($profile_id!=get_current_user_id()) {
			$response = array(
                'success'      => false,
                'message'  => 'Posting Error : You have no right for posting book. Please login first'
			 );
			echo json_encode( $response );
			die();
		}
		
		
		// meta repeater
		/*
		$rfbwp_fb_page_type = $_POST['rfbwp_fb_page_type'];
		$rfbwp_fb_page_bg_image = $_POST['rfbwp_fb_page_bg_image'];
		$rfbwp_fb_page_bg_image_zoom = $_POST['rfbwp_fb_page_bg_image_zoom'];
		$rfbwp_fb_page_title = $_POST['rfbwp_fb_page_title'];
		$rfbwp_fb_page_index = $_POST['rfbwp_fb_page_index'];
		$rfbwp_fb_page_url = $_POST['rfbwp_fb_page_url'];
		$rfbwp_page_html = $_POST['rfbwp_page_html'];
		$rfbwp_page_html_second = $_POST['rfbwp_page_html_second'];
		$rfbwp_fb_page_custom_class = $_POST['rfbwp_fb_page_custom_class'];
		$rfbwp_fb_page_css = $_POST['rfbwp_fb_page_css'];
		
		foreach ($rfbwp_fb_page_title as $key => $value) {
			$book_group[$key]['rfbwp_fb_page_type'] = $rfbwp_fb_page_type[$key];
			$book_group[$key]['rfbwp_fb_page_bg_image'] = $rfbwp_fb_page_bg_image[$key];
			$book_group[$key]['rfbwp_fb_page_bg_image_zoom'] = $rfbwp_fb_page_bg_image_zoom[$key];
			$book_group[$key]['rfbwp_fb_page_title'] = $rfbwp_fb_page_title[$key];
			$book_group[$key]['rfbwp_fb_page_index'] = $rfbwp_fb_page_index[$key];
			$book_group[$key]['rfbwp_fb_page_url'] = $rfbwp_fb_page_url[$key];
			$book_group[$key]['rfbwp_page_html'] = $rfbwp_page_html[$key];
			$book_group[$key]['rfbwp_page_html_second'] = $rfbwp_page_html_second[$key];
			$book_group[$key]['rfbwp_fb_page_custom_class'] = $rfbwp_fb_page_custom_class[$key];
			$book_group[$key]['rfbwp_fb_page_css'] = $rfbwp_fb_page_css[$key];
		}
		*/
		// post
		
		$post_title = $_POST['post-title'];
		$description = $_POST['dyn-select-description'];
		$tags = trim($_POST['dyn-select-tags']);
		$reference = trim($_POST['dyn-select-reference']);
		// check authority and permissions: current_user_can()
		// check intention: wp_verify_nonce()
		
		/* create post and get it */
	if ( get_post_status($dyn_post_iddd) == 'draft' ) {
		
		$dyn_post_id = array(
						'ID' => $dyn_post_iddd,
						'post_title' => $post_title,
						'post_type' => 'dyn_book',
						'post_content' => $description,
						'post_status' => 'draft',
						'post_author' => $profile_id
		);
		
	} else {
		$dyn_post_id = array(
						'ID' => $dyn_post_iddd,
						'post_title' => $post_title,
						'post_type' => 'dyn_book',
						'post_content' => $description,
						'post_status' => 'publish',
						'post_author' => $profile_id
		);
		
		
	}
		wp_update_post( $dyn_post_id );
		
		
		// metabox dyn-book-mb-post
		//$book_item['dyn-book-show'] = 1;
		$book = dyn_get_options_mpcrf($dyn_post_iddd);
		$book_group = $book['books'][$dyn_post_iddd]['pages'];
		$book_item['pages'] = $book_group;
		update_post_meta( $dyn_post_iddd, 'dyn-book-mb-post', $book_item);
		
		
		// metabox dyn-book-mb-setting
		$book_setting = $_POST['rfbwp_options'];
		if ($book_setting['books'][0]['rfbwp_fb_height']=='') {
			$book_setting['books'][0]['rfbwp_fb_height'] = 400;
		}
		if ($book_setting['books'][0]['rfbwp_fb_width']=='') {
			$book_setting['books'][0]['rfbwp_fb_width'] = 300;
		}
		$book_setting['books'][0]['rfbwp_fb_name'] = $dyn_post_iddd;
		
		update_post_meta( $dyn_post_iddd, 'dyn-book-mb-settings', $book_setting);
		$book_setting['books'][0]['pages'] = $book_item['pages'];
		
		$book_options['books'][$dyn_post_iddd] = $book_setting['books'][0];
		update_post_meta( $dyn_post_iddd, 'dyn-book-mb-options', $book_options);
		
		// set featured image
		//set_post_thumbnail
		
		/*$rfbwp_fb_page_bg_image_id = $_POST['rfbwp_fb_page_bg_image_id'];
		if ($rfbwp_fb_page_bg_image_id[0]!='') {
			set_post_thumbnail($dyn_post_iddd,$rfbwp_fb_page_bg_image_id[0]);
		} else {
			$rfbwp_fb_hc_fco_id = $book_setting['books'][0]['rfbwp_fb_hc_fco_id'];
			if ($rfbwp_fb_hc_fco_id!='') {
				set_post_thumbnail($dyn_post_iddd,$rfbwp_fb_hc_fco_id);
			}
		}*/
		
		
		// update tags
		if(!empty($tags)){
					$tags = explode(' ', $tags);
					wp_set_post_tags( $dyn_post_iddd, $tags );
		}
		update_post_meta( $dyn_post_iddd, 'video_options_refr', $reference );
		

		// Background Page
		if( isset( $_POST['dyn_background'] ) ){
					if( $_POST['dyn_background'] == 'background_no' ){						
						if ( ! delete_post_meta( $dyn_post_iddd, 'dyn_background_src' ) ) {
							
						}
						
					}
					if( $_POST['dyn_background'] == 'background_user' ){
						if( get_user_meta( $profile_id, 'background_image', true ) ){
							$img = get_user_meta( $profile_id, 'background_image' );
							update_post_meta( $dyn_post_iddd, 'dyn_background_src', $img[0] );						
						}
					}
					if( $_POST['dyn_background'] == 'background_new' ){
						if (isset($_POST['dyn_bg_id'])) {
							$attach_id = $_POST['dyn_bg_id'];
							$image_full = wp_get_attachment_image_src( $attach_id, 'full' );
							$image_full = $image_full[0];
				
							$image_full = str_replace(get_bloginfo('url').'/','',$image_full);
							update_post_meta( $dyn_post_iddd, 'dyn_background_src', $image_full);
						}
					}
		}
		
		// redirect 
		if ( get_post_status($dyn_post_iddd) == 'draft' ) {
		$redirect_to = get_permalink( $dyn_post_iddd ).'preview=true';
		}
		else {
			$redirect_to = get_permalink( $dyn_post_iddd );
		}
		$response = array(
                'success'      => true,
                'redirect_to'  => $redirect_to,
				'message'	=> '<span>Book Edit successfully. <a href="'. $redirect_to .'">View File</a></span>'
         );
		echo json_encode( $response );
		die();
		
		/* update content to match with metabox */
		/*
		$my_post = array(
			  'ID'           => $dyn_post_id,
			  'post_content' => 'This is the updated content.',
		  ); 
		wp_update_post( $my_post );
		*/
		die();
	}
}	

new Dyn_Book_Post_Create();
?>