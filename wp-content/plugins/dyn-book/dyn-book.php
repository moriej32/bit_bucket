<?php

/*

Plugin name: DYN Book 
Plugin URI: http://www.wpamanuke.com
Description: Customize from responsive flipbook to save on post. Framework used vafpress. Explanation : Create dyn_post post type, copy post meta (dyn-book-mb-post) to content (trick for easy searchable) , for showing custom flipbook using shortcode [dyn-book id=1] , [dyn-book-upload]  for upload book
Version: 1.0
Author: DYN UPWORK PROGRAMMER 
Author URI: http://www.wpamanuke.com

Made from 5 May 2016
Notes
1. Using vafpress for metabox builder
2. Field created
- dyn-book-mb-post on post_meta 
- dyn-book-mb-settings on post meta (in progress)
- dyn-book-mb-options
3. Themes VideoPress Modify
- content-dyn_book.php (for search result)
- single-dyn_book.php (for single page)
- includes/file-book.php
- includes/random-category-book
- template-homepage.php
- includes/widgets/users.php (for menu)
- includes/user/profile.php (for tab)
- functions.php ()

WARNING : is_search is modify using add_filter('get_meta_sql', to make shortcode searchable so if there is another plugin using same filter perhaps there will be conflict

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
/*
function wpuf_get_option( $option, $section, $default = '' ) {

			$options = get_option( $section );

			if ( isset( $options[$option] ) ) {
				return $options[$option];
			}

			return $default;
}
*/	
define('DYN_BOOK_PLUGIN_URL',  plugin_dir_url( __FILE__ ));
define('DYN_BOOK_PLUGIN_DIR',  plugin_dir_path( __FILE__ ));
define( 'DYN_MPC_PLUGIN_ROOT', rtrim( plugin_dir_url( __FILE__ ), '/' ) );
define( 'DYN_MPC_PLUGIN_FILE', __FILE__ );
define( 'DYN_MPC_DEV', defined( 'MPC_DEBUG' ) && MPC_DEBUG );
define('DYN_MPC_RFBWP_GOOGLE_FONTS_API_ID', 'AIzaSyDp98WtnL2USah3Kgzum2puDPF_CrKBxLY');
/*
define('MPC_PLUGIN_ROOT',plugin_dir_url( __FILE__ ));

*/

/** 
CUSTOM POST TYPE dyn_book
**/
require_once(plugin_dir_path( __FILE__ ) .'inc/class-dyn-book-post-type.php');



/**
** FRONT UPLOADER using [dyn-book-upload] using ajax for create and upload dyn_book
**/
require_once(plugin_dir_path( __FILE__ ) .'inc/class-dyn-book-post-create.php');
require_once(plugin_dir_path( __FILE__ ) .'inc/class-dyn-book-image-upload.php');



/** 
Enable Book Search Using Filter Not Work because using Relevanssi Plugin
**/
//require_once(plugin_dir_path( __FILE__ ) .'inc/class-dyn-book-search.php');


/*
Filter for theme
*/
require_once(plugin_dir_path( __FILE__ ) .'inc/class-dyn-book-filter-theme.php');


/*
Flipbook Shortcode [dyn-book id=1][/dyn-book]
*/
require_once(DYN_BOOK_PLUGIN_DIR . 'inc/flipbook/settings.php');
require_once(DYN_BOOK_PLUGIN_DIR . 'inc/flipbook/flipbook.php');

/*
DYN BOOK Setting
*/
require_once(DYN_BOOK_PLUGIN_DIR . 'inc/class-dyn-book-setting.php');
?>