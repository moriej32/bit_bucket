<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

//define vars
define('OIOPUB_PREVIEW', 1);

//init
include_once(str_replace("\\", "/", dirname(__FILE__)) . "/index.php");

//is plugin enabled?
if($oiopub_set->enabled != 1) {
	die("Plugin Currently Offline");
}

//get rand ID
//$rand = oiopub_clean($_GET['id']);
$item = new oiopub_std;
$item_id = intval($_GET['id']);
$item_type = oiopub_clean($_GET['type']);
//get purchase ID
//$purchase = $oiopub_db->GetRow("SELECT * FROM " . $oiopub_set->dbtable_purchases . " WHERE rand_id='$rand'");
$item = $oiopub_db->GetRow("SELECT * FROM $oiopub_set->dbtable_purchases WHERE item_id='$item_id'");
//valid preview?
//if(!$purchase->post_id) die('Invalid Preview Link');

//show preview
//echo oiopub_preview_data($purchase->post_id, $purchase->published_status);
//template vars
if($item_type == "video") {
		$title = __oio("Video Purchase Preview");
		
}
elseif($item_type == "post") {
	$prefix = "p";
	$nofollow = false;
	$item->item_channel = 1;
	$adtype_array = array( 1 => "Blogger", "Advertiser" );
		$title = __oio("post Purchase Preview");
}
elseif($item_type == "link") {
	$prefix = "l";
	$item->item_channel = 2;
	for($z=1; $z <= $oiopub_set->links_zones; $z++) {
		$lz = "links_" . $z;
		if(!empty($oiopub_set->{$lz}['title'])) {
			$adtype_array[$z] = $oiopub_set->{$lz}['title'] . " (zone $z)";
		}
		if(isset($item->item_type) && $item->item_type == $z) {
			$nofollow = $oiopub_set->{$lz}['nofollow'] == 2 ? true : false;
		}
	}
		$title = __oio("Link Purchase Preview");
}
elseif($item_type == "inline") {
	$prefix = "v";
	$item->item_channel = 3;
	if($oiopub_set->inline_ads['selection'] == 1) $inline_select = "Video Ad";
	if($oiopub_set->inline_ads['selection'] == 2) $inline_select = "Banner Ad";
	if($oiopub_set->inline_ads['selection'] == 3) $inline_select = "RSS Feed Ad";
	$adtype_array = array( $oiopub_set->inline_ads['selection'] => $inline_select, 4 => "Intext Link" );
		$title = __oio("Inline Purchase Preview");
}
elseif($item_type == "banner") {
	$prefix = "b";
	$item->item_channel = 5;
	for($z=1; $z <= $oiopub_set->banners_zones; $z++) {
		$bz = "banners_" . $z;
		if(!empty($oiopub_set->{$bz}['title'])) {
			$adtype_array[$z] = $oiopub_set->{$bz}['title'] . " (zone $z)";
		}
		if(isset($item->item_type) && $item->item_type == $z) {
			$nofollow = $oiopub_set->{$bz}['nofollow'] == 2 ? true : false;
		}
	}
		$title = __oio("Banner Purchase Preview");
}
$templates = array();
$templates['page'] = "purchase_previews";
$templates['title'] = $title;

$adstatus_array = array( 0 => "Pending", 1 => "Approved", 2 => "Rejected", 3 => "Expired", -1 => "Queued", -2 => "Queued (pending)" );
$paystatus_array = array( 0 => "Not Paid", 1 => "Paid", 2 => "Invalid" );
//load template
include_once($oiopub_set->folder_dir . "/templates/core/main.tpl");
?>