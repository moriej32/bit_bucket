<div class="purchase">

<!--<span valign="top"><?php echo oiopub_amount($oio_data->payment_amount, $oio_data->payment_currency); ?></span>-->


<?php if(!$oio_data || !$oio_data->rand_id) { ?>

<br /><br />
<?php if(!empty($rand_id)) { ?>
	<b><?php echo __oio("No payment information for this purchase code available!"); ?></b>
	<br /><br /><br /><br />
<?php } ?>
<form method="get" action="payment.php">
<?php echo __oio("Purchase Code"); ?> &nbsp;<input type="text" name="rand" size="20" /> <input type="submit" value="<?php echo __oio("Go"); ?>" />
</form>
<br /><br />

<?php } elseif($_GET['do'] == 'failed') { ?>

<b><?php echo __oio("Your payment has been marked as invalid or incomplete, and not recorded!"); ?></b>

<?php } elseif($_GET['do'] == "success" && $oio_data->payment_status == 0) { ?>

<b><?php echo __oio("Waiting for a response from the payment processor"); ?>...</b>
<meta http-equiv="refresh" content="5" />

<?php } elseif($_GET['do'] == "success" && $oio_data->payment_status != 0) { ?>

<?php if($oiopub_set->posts['price_free'] == 1 && $oiopub_set->posts['price_adv'] == 0 && $oio_data->post_author == 2) { ?>
	<b><?php echo __oio("Your free post has been submitted, please check your e-mail for confirmation!"); ?></b>
<?php } elseif($oio_data->payment_status == 1) { ?>
	<b><?php echo __oio("Your payment has been recorded, please check your e-mail for confirmation!"); ?></b>
<?php } else { ?>
	<b><?php echo __oio("Your payment has been recorded, but failed the verification checks, and will be reviewed manually!"); ?></b>
<?php } if($oio_data->item_status == 0) { ?>
	<br /><br />
	<?php echo __oio("Your purchase must be approved before it is finalised."); ?>
<?php } ?>
<br /><br />
<?php echo __oio("Please note down your unique purchase ID for future reference") . ": <b>" . $rand_id . "</b>\n"; ?>
<?php
if($oio_data->item_status == 1 && $oio_data->payment_status == 1) {
	if($oio_data->item_channel == 4) {
		$cn = "custom_" . $oio_data->item_type;
		if(!empty($oiopub_set->{$cn}['download'])) {
			$url = $oiopub_set->plugin_url . "/download.php?id=" . $oio_data->item_id . "&rand=" . $rand_id;
			echo "<meta http-equiv='refresh' content='5;URL=" . $url . "' />\n";
			echo "<br /><br /><br />\n";
			echo "<a href='" . $url . "' rel='nofollow'>" . __oio("You will shortly be redirected to the download page!") . "</a>\n";
		}
	} elseif($oio_data->item_channel != 1) {
		echo "<br /><br /><br />\n";
		echo __oio("You should now see your purchase displayed on this site.") . "\n";
	}
} elseif($oio_data->item_status == -1 && $oio_data->payment_status == 1) {
	echo "<br /><br /><br />\n";
	echo "<font color='red'><b>" . __oio("Queued") . ":</b> " . __oio("you will receive an email once your ad becomes active") . ".</font>\n";
	echo "<br />\n";
	$est = oiopub_queue_estimate($oio_data->item_channel, $oio_data->item_type, $oio_data->payment_time);
	echo "<i>" . __oio("The estimated publishing date is %s", array($est['date'])) . "</i>";
}
if(($oio_data->payment_status == -1 || $oio_data->payment_status == 1) && !empty($oiopub_set->feedback)) {
	echo "<br /><br /><br />\n";
	echo "<a href='" . $oiopub_set->feedback . "' rel='nofollow'><b>" . __oio("Please leave feedback on my marketplace profile") . "</b></a>\n";
}
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<?php 
 $refurluserid = $_SESSION["referalnow"]; 
 if( !empty( $refurluserid ) )
 {
   //$visit_id = rand(9999);;
   
   //setcookie( 'affwp_ref', $refurluserid, '1 days' ), COOKIEPATH, COOKIE_DOMAIN );
  // setcookie( 'affwp_ref' , $refurluserid, time() + (86400 * 30), "/");
   
  // setcookie( 'affwp_ref_visit_id', $visit_id,  '1 days' ), COOKIEPATH, COOKIE_DOMAIN );
  
   //setcookie( 'affwp_ref_visit_id' , $visit_id, time() + (86400 * 30), "/");
   
    $amountads = $oio_data->payment_amount * 20 /100;
    echo do_shortcode('[affiliate_conversion_script amount="'. $amountads .'" description="Ads affiliate" context="adsaffiliate" reference="'. $refurluserid .'" status="pending"]');
 }

 ?>
<!--<script type="text/javascript">
		jQuery(document).ready(function($) {

			var ref   = $.cookie( 'affwp_ref' );
			var visit = $.cookie( 'affwp_ref_visit_id' );

			// If a referral var is present and a referral cookie is not already set
			if( ref && visit ) {
                   console.log();
				// Fire an ajax request to log the hit
				$.ajax({
					type: "POST",
					data: {
						action      : 'affwp_track_conversion',
						affiliate   : ref,
						amount      : '<?php echo $amountads; ?>',
						status      : 'pending',
						description : 'Ads affiliate',
						context     : 'adsaffiliate',
						reference   : '<?php echo $refurluserid; ?>',
						campaign    : '',
						md5         : 'dc26f4b41fd16f86fe93341a2af81468'
					},
					url:  '<?php echo esc_js( admin_url( 'admin-ajax.php' ) ); ?>',
					success: function (response) {
						if ( window.console && window.console.log ) {
							console.log( response );
						}
					}

				}).fail(function (response) {
					if ( window.console && window.console.log ) {
						console.log( response );
					}
				}).done(function (response) {
				});

			}

		});
		</script>-->
<?php } else { ?>

<?php if($oiopub_set->general_set['paytime'] == 1 && $oiopub_set->api['paytime'] == 0 && $oio_data->payment_status == 0 && ($oio_data->item_status == 0 || $oio_data->item_status == -2)) { ?>
	<b><?php echo __oio("The website owner has selected not to allow payment until approval has been made."); ?></b>
	<br /><br />
	<?php echo __oio("You will receive an email (and payment link) once approval has taken place."); ?>
<?php } elseif($oio_data->item_status == 3 && !$free_space) { ?>
	<b><?php echo __oio("You cannot renew your purchase at this time, as the available spaces have already been filled."); ?></b>
<?php } elseif($oio_data->item_status == 2) { ?>
	<b><?php echo __oio("This purchase has been rejected for use on this site!"); ?></b>
<?php } elseif($oio_data->payment_status == 0 || ($type_check[0] != 'p' && $oio_data->item_status == 3 && $free_space)) {

	if(!isset($item_title))
		$item_title = "Banner Ad";
	$pieces = explode(",", $item_title);


 ?>
	<h3><?php echo __oio("Payment For %s", array($pieces[0])); ?></h3>
	<?php if($oio_data->item_status == -1 || $oio_data->item_status == -2) { ?>
		<br />
		<font color="red"><?php echo __oio("Just a reminder that your ad will be placed in the queue"); ?></font>
		<br />
	<?php } ?>
	<br />
	<div class="col-md-12">
		<div class="paymethod">
			<strong><?php echo __oio("Chose Payment Method"); ?></strong><br/>
			<br/>
			<br/>
			
			<input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="paypal" checked="checked"><label for="payment_method_paypal">
		PayPal <a href="https://www.paypal.com/us/webapps/mpp/paypal-popup" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/us/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="What is PayPal?">What is PayPal?</a>	</label><br/>
		
		<div class="paypalpaymnt">
			<p>
			<?php
			$proc = strtolower($oio_data->payment_processor);
			if(!empty($oiopub_plugin[$proc])) {
				$oiopub_plugin[$proc]->form($rand_id);
			} else {
				echo __oio("An error has occurred. Unable to load the required payment module.");
				die();
			}
			?>
			</p>
		</div>
		
			<input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="card"> <label for="payment_method_simplify_commerce">
		Credit or Debit card <img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/visa.png" alt="Visa"><img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/mastercard.png" alt="MasterCard"><img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/discover.png" alt="Discover"><img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/amex.png" alt="Amex"><img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/jcb.png" alt="JCB">	</label>
			
		<div class="cardpayment" style="display:none">
	         <form id="simplify-payment-form" action="credit-or-debit-card.php"  method="POST">
			      <input id="ammount" name="ammount" type="hidden"class="form-control"  value="<?php echo $oio_data->payment_amount; ?>" />
				   <input id="itemid" name="itemid" type="hidden"class="form-control"  value="<?php echo $oio_data->rand_id; ?>" />
                  <!-- The $10 amount is set on the server side $oio_data->item_id  -->
                 <div class="form-group">
                     <label>Credit Card Number</label><br/>
				      <input id="cc-number" type="text" maxlength="20"  class="form-control" autocomplete="off" value="" autofocus />
                  </div>
				  
				  <div class="form-group">
                     <label>CVC</label><br/>
					  <input id="cc-cvc" type="text" maxlength="4" class="form-control" autocomplete="off" value=""/>
                  </div>
				  
				  <div class="form-group">
                      <label>Expiry Date </label><br/>
					  <select class="half-width" id="cc-exp-month">
							<option value="01">Jan</option>
							<option value="02">Feb</option>
							<option value="03">Mar</option>
							<option value="04">Apr</option>
							<option value="05">May</option>
							<option value="06">Jun</option>
							<option value="07">Jul</option>
							<option value="08">Aug</option>
							<option value="09">Sep</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>
						</select>
						<select class="half-width" id="cc-exp-year">
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>
							<option value="30">2030</option>
						</select>
                  </div>
         <button id="process-payment-btn" class="form-control btn-success" type="submit">Buy Now</button>
      </form>
        </div>
	</div>
		
		
	
	  
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.simplify.com/commerce/v1/simplify.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='payment_method']:checked").val();
            if(radioValue == 'paypal'){
                $('.paypalpaymnt').fadeIn();
				 $('.cardpayment').fadeOut();
            }else{
			
			 $('.cardpayment').fadeIn();
				 $('.paypalpaymnt').fadeOut();
			}
        });
        
    });
</script>

	  		
				</span>
		</div>
	 </div>
<?php } else { ?>
	<b><?php echo __oio("This purchase code exists, but the transaction is already complete!"); ?></b>
	<br /><br />
	<a href="stats.php?rand=<?php echo $rand_id; ?>"><?php echo __oio("Login to your ad dashboard"); ?></a>
<?php } ?>

<?php } ?>
<?php //echo do_shortcode('[accept_simplify_payment name="Test Product" price="20" button_text="Buy Now"]'); ?>
</div>
<style>
.paymethod{

    text-align: left;
    max-width: 400px;
    margin: 0 auto;
}
.paypalpaymnt{
text-align: center;
}
.btn-success,.btn-success:hover {
    color: #fff;
    background-color: #21648F;
    border-color: #21648F;
}
.half-width{
width: 30%;
border: 1px solid #ddd;
}

</style>