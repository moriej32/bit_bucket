<table align="center" border="0" width="550" class="start" style="background-color: rgb(248, 248, 248)">
    <tr><td align="center">
            <form name="type" id="type" method="post" action="purchase.php">
                <input type="hidden" name="process" value="yes" />
                <?php //echo oiopub_zone_select(); ?>
                <div class="payment-link">
                    <h3><a href="payment.php" rel="nofollow"><?php echo __oio("Review Purchased Ads Stats"); ?></a></h3>
                </div>
            </form>
        </td></tr>
</table>
<?php $referal = $_GET['referal'];
 if(!empty( $referal ))
   {
    // $referalurl = '&referal=' . $referal;
	 session_start();
	 $_SESSION["referalnow"] = $referal;
  }
  ?>
<?php if($oiopub_set->links_total > 0) { ?>
<!-- text ads start -->
<table align="center" border="0" width="550" class="start" style="background-color: rgb(248, 248, 248)">
    <tr><td>
            <h3><?php echo __oio("Text Ad Pricing"); ?></h3>
            <table id="link-chart" class="chart" width="100%" border="0" cellspacing="2" cellpadding="2">
                <?php echo $oiopub_purchase->chart(2, "#E0EEEE", "#FFFFFF"); ?>
            </table>
        </td></tr>
</table>
<!--// text ads end -->
<?php } ?>
<?php if($oiopub_set->banners_total > 0) { ?>
<!-- banner ads start -->
<table align="center" border="0" width="550" class="start" style="background-color: rgb(248, 248, 248)">
    <tr><td>
            <h3><?php echo __oio("Banner Ads"); ?></h3>
            <table id="banner-chart" class="chart" width="100%" border="0" cellspacing="2" cellpadding="2">
                <tr style="background:#E0EEEE;">
                    <td width="35%"><a href="purchase.php?do=banner&amp;zone=1&position=under_content">Banner Ads under Content</a><br><i>770x150 size</i></td>
					<td style="text-align: center;"><a href="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/banner_content_large.png" target="_blank"><img src="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/banner_content.png" height="180px" width="300px" alt="Banner Ads under Content" /></a></td>
                </tr>
				
				<tr  style="background:#E0EEEE;"> 
					<td width="35%"><a href="purchase.php?do=banners&amp;zone=1&position=sidebar">Banner Ads in Sidebar</a><br><i>320x240 size </i></td>
					<td style="text-align: center;"><a href="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/banner_sidebar_large.png" target="_blank"><img src="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/banner_sidebar.png" height="180px" width="300px" alt="Banner Ads in sidebar" /></a></td>
                </tr>

                <tr style="background:#E0EEEE;"> 
					<td width="35%"><a href="purchase.php?do=popsupbanners&amp;zone=1&position=inline_video">Inline Banner Ads on Videos</a><br><i>550x130 size</i></td>
					<td style="text-align: center;"><a href="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/banner_inline_large.png" target="_blank"><img src="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/banner_inline.png" height="180px" width="300px" alt="Inline Banner ads on videos" /></a></td>
                </tr>


            </table>
        </td></tr>
</table>

<table align="center" border="0" width="550" class="start" style="background-color: rgb(248, 248, 248)">
    <tr>
		<td>
            <h3><?php echo __oio("Video Ads"); ?></h3>
            <table id="banner-chart" class="chart" width="100%" border="0" cellspacing="2" cellpadding="2">
                <tr style="background:#E0EEEE;">
                    <td width="35%"><a href="purchase.php?do=video&amp;zone=1">Video Ads</a><br><i>Popup & Pre-roll</i></td>
					<td style="text-align: center;">
					<a href="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/video_popup_large.png" target="_blank"><img src="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/video_popup.png" height="180px" width="300px" alt="Video ad (POP-UP)" /></a><br><br>
					<a href="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/video_prerol_large.png" target="_blank"><img src="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/video_prerol.png" height="180px" width="300px" alt="Video ad (POP-UP)" /></a>
					</td>
				</tr>				
			</table>
        </td>
	</tr>
</table>
<!--// banner ads end -->
<?php } ?>
<?php if($oiopub_set->inline_total > 0) { ?>
<!-- inline ads start -->
<table align="center" border="0" width="550" class="start" style="background-color: rgb(248, 248, 248)">
    <tr><td>
            <h3><?php echo __oio("Inline Ad Pricing"); ?></h3>
            <table id="inline-chart" class="chart" width="100%" border="0" cellspacing="2" cellpadding="2">
                <?php echo $oiopub_purchase->chart(3, "#E0EEEE", "#FFFFFF"); ?>
            </table>
        </td></tr>
</table>
<!--// inline ad end -->
<?php } ?>
<?php if($oiopub_set->posts_total > 0) { ?>
<!-- posts start -->
</td></tr>
</table>
<!--// posts end -->
<?php } ?>
<?php if($oiopub_set->custom_total > 0) { ?>
<!-- custom purchases start -->
<table align="center" border="0" width="550" class="start" style="background-color: rgb(248, 248, 248)">
    <tr><td>
            <h3><?php echo __oio("Custom Item Pricing"); ?></h3>
            <table id="custom-chart" class="chart" width="100%" border="0" cellspacing="2" cellpadding="2">
                <?php echo $oiopub_purchase->chart(4, "#E0EEEE", "#FFFFFF"); ?>
            </table>
        </td></tr>
</table>
<!--// custom purchases end -->
<?php } ?>
<?php if(!empty($oiopub_set->rules)) { ?>
<!-- purchase guidelines begin -->
<table align="center" border="0" width="550" class="start"style="background-color: rgb(248, 248, 248)">
    <tr><td>
            <h3><?php echo __oio("Purchasing Guidelines"); ?></h3>
            <?php
$oiopub_set->rules = str_replace("\r\n", "<br />", $oiopub_set->rules);
            $oiopub_set->rules = str_replace("\n", "<br />", $oiopub_set->rules);
            echo stripslashes($oiopub_set->rules);
            ?>
        </td></tr>
</table>
<!--// purchase guidelines end -->
<?php } ?>
