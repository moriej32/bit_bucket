<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/lib/ink-3.1.10/dist/css/ink.min.css?ver=3.1.10">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/wc-vendors-pro/public/assets/css/dashboard.min.css?ver=1.3.6">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/themes/videopress/css/entry.css?ver=4.2.4">
<link rel="stylesheet" type="text/css" href="<?php echo site_url();?>/wp-content/plugins/msh_custom_css_and_js/css/zzz_custom_styles.css?ver=4.2.4">
<style>.wcv-grid nav.wcv-navigation { margin-bottom: 1em;  } .entry ul, .entry ol { margin-left: 0px;}.wcv-navigation { margin-bottom: 1em;} ul li{list-style-type: none}.wcv-navigation ul li {margin: 0.5em 0px 0px; }.empty-attr{    background: #eaebec !important; font-weight: bold !important;}  tr.one-color-change td {  background: rgb(255, 248, 248);  }.wcvendors-table.wcvendors-table-shop_coupon.wcv-table.position{    margin-top: 10px !important;} .group-total{ padding-left: 8px !important; margin-left: 29px; border: 1px solid rgba(220, 217, 217, 0.52); padding: 8px;    background: white; border-radius: 4px;}  .all-100.group-total.latest{margin-top: 10px !important;}  .left-content,.right-content{  float: left;  width: 47%;  }.right-content{    margin-left: 42px;}  .right-content .all-100.group-total.latest:first-child {  margin-top: 0px !important;  } .banner-add{    padding-bottom: 11px;}   .affwp-creative{         padding-right: 12px; }  .affwp-creative p a img{     max-width: 50%; }</style>
<?php $active_tab = affwp_get_active_affiliate_area_tab(); ?>

<div id="affwp-affiliate-dashboard">

	<?php 	echo do_shortcode("[wcv_pro_dashboard_nav]"); ?>

	<?php if ( 'pending' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>

		<p class="affwp-notice"><?php _e( 'Your affiliate account is pending approval', 'affiliate-wp' ); ?></p>

	<?php elseif ( 'inactive' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>

		<p class="affwp-notice"><?php _e( 'Your affiliate account is not active', 'affiliate-wp' ); ?></p>

	<?php elseif ( 'rejected' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>

		<p class="affwp-notice"><?php _e( 'Your affiliate account request has been rejected', 'affiliate-wp' ); ?></p>

	<?php endif; ?>

	<?php if ( 'active' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>

		<?php
		/**
		 * Fires at the top of the affiliate dashboard.
		 *
		 * @since 0.2
		 * @since 1.8.2 Added the `$active_tab` parameter.
		 *
		 * @param int|false $affiliate_id ID for the current affiliate.
		 * @param string    $active_tab   Slug for the currently-active tab.
		 */
		do_action( 'affwp_affiliate_dashboard_top', affwp_get_affiliate_id(), $active_tab );
		?>

		<?php if ( ! empty( $_GET['affwp_notice'] ) && 'profile-updated' == $_GET['affwp_notice'] ) : ?>

			<p class="affwp-notice"><?php _e( 'Your affiliate profile has been updated', 'affiliate-wp' ); ?></p>

		<?php endif; ?>

		<?php
		/**
		 * Fires inside the affiliate dashboard above the tabbed interface.
		 *
		 * @since 0.2
		 * @since 1.8.2 Added the `$active_tab` parameter.
		 *
		 * @param int|false $affiliate_id ID for the current affiliate.
		 * @param string    $active_tab   Slug for the currently-active tab.
		 */

		do_action( 'affwp_affiliate_dashboard_notices', affwp_get_affiliate_id(), $active_tab );
		?>

		<ul id="affwp-affiliate-dashboard-tabs">
		<!--	<?php /*if ( affwp_affiliate_area_show_tab( 'urls' ) ) : */?>
				<li class="affwp-affiliate-dashboard-tab<?php /*echo $active_tab == 'urls' ? ' active' : ''; */?>">
					<a href="<?php /*echo esc_url( affwp_get_affiliate_area_page_url( 'urls' ) ); */?>"><?php /*_e( 'Affiliate URLs', 'affiliate-wp' ); */?></a>
				</li>
			--><?php /*endif; */?>

			<?php if ( affwp_affiliate_area_show_tab( 'stats' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'stats' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'stats' ) ); ?>"><?php _e( 'Statistics', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( affwp_affiliate_area_show_tab( 'graphs' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'graphs' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'graphs' ) ); ?>"><?php _e( 'Graphs', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( affwp_affiliate_area_show_tab( 'referrals' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'referrals' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'referrals' ) ); ?>"><?php _e( 'Referrals', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( affwp_affiliate_area_show_tab( 'payouts' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'payouts' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'payouts' ) ); ?>"><?php _e( 'Payouts', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( affwp_affiliate_area_show_tab( 'visits' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'visits' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'visits' ) ); ?>"><?php _e( 'Visits', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( affwp_affiliate_area_show_tab( 'creatives' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'creatives' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'creatives' ) ); ?>"><?php _e( 'Banners', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( affwp_affiliate_area_show_tab( 'settings' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'settings' ? ' active' : ''; ?>">
					<a href="<?php echo esc_url( affwp_get_affiliate_area_page_url( 'settings' ) ); ?>"><?php _e( 'Settings', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

			<?php
			/**
			 * Fires immediately after core Affiliate Area tabs are output,
			 * but before the 'Log Out' tab is output (if enabled).
			 *
			 * @since 0.2
			 *
			 * @param int    $affiliate_id ID of the current affiliate.
			 * @param string $active_tab   Slug of the active tab.
			 */
			do_action( 'affwp_affiliate_dashboard_tabs', affwp_get_affiliate_id(), $active_tab );
			?>

			<?php if ( affiliate_wp()->settings->get( 'logout_link' ) ) : ?>
				<li class="affwp-affiliate-dashboard-tab">
					<a href="<?php echo esc_url( affwp_get_logout_url() ); ?>"><?php _e( 'Log out', 'affiliate-wp' ); ?></a>
				</li>
			<?php endif; ?>

		</ul>

		<?php
		if ( ! empty( $active_tab ) && affwp_affiliate_area_show_tab( $active_tab ) ) :
			affiliate_wp()->templates->get_template_part( 'dashboard-tab', $active_tab );
		endif;
		?>

		<?php
		/**
		 * Fires at the bottom of the affiliate dashboard.
		 *
		 * @since 0.2
		 * @since 1.8.2 Added the `$active_tab` parameter.
		 *
		 * @param int|false $affiliate_id ID for the current affiliate.
		 * @param string    $active_tab   Slug for the currently-active tab.
		 */
		do_action( 'affwp_affiliate_dashboard_bottom', affwp_get_affiliate_id(), $active_tab );
		?>

	<?php endif; ?>

</div>
<script>
	$('.wcv-navigation ul li:first-child').removeClass('active');
	$('#dashboard-menu-item-affiliate-area').addClass('active');
</script>
