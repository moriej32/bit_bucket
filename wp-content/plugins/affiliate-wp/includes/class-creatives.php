<?php
/**
 * Creatives
 *
 * This class handles the asset management of affiliate banners/HTML/links etc
 *
 * @package     AffiliateWP
 * @copyright   Copyright (c) 2012, Pippin Williamson
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.2
 */

class Affiliate_WP_Creatives {

	/**
	 * The [affiliate_creative] shortcode
	 *
	 * @since  1.2
	 * @return string
	 */
	public function affiliate_creative( $args = array() ) {

		// Creative's ID
		$id = isset( $args['id'] ) ? (int) $args['id'] : 0;

		if ( ! $creative = affwp_get_creative( $id ) ) {
			return;
		}

		// creative's link/URL
		if ( ! empty( $args['link'] ) ) {
			// set link to shortcode parameter
			$link = $args['link'];
		} elseif ( $creative->url ) {
			// set link to creative's link from creatives section
			$link = $creative->url;
		} else {
			// set link to the site URL
			$link = get_site_url();
		}

		// creative's image link
		$image_link = ! empty( $args['image_link'] ) ? $args['image_link'] : $creative->image;

		// creative's text (shown in alt/title tags)
		if ( ! empty( $args['text'] ) ) {
			// set text to shortcode parameter if used
			$text = $args['text'];
		} elseif ( $creative->text ) {
			// set text to creative's text from the creatives section
			$text = $creative->text;
		} else {
			// set text to name of blog
			$text = get_bloginfo( 'name' );
		}

		// creative's description
		$description = ! empty( $args['description'] ) ? $args['description'] : $creative->description;

		// creative's preview parameter
		$preview = ! empty( $args['preview'] ) ? $args['preview'] : 'yes';

		// get the image attributes from image_id
		$attributes = ! empty( $args['image_id'] ) ? wp_get_attachment_image_src( $args['image_id'], 'full' ) : '';

		// load the HTML required for the creative
		return $this->html( $id, $link, $image_link, $attributes, $preview, $text, $description );

	}

	/**
	 * The [affiliate_creatives] shortcode
	 *
	 * @since  1.2
	 * @return string
	 */
	public function affiliate_creatives( $args = array() ) {
		global $wpdb;
		$defaults = array(
				'preview' => 'yes',
				'status'  => 'active'
		);

		$args = wp_parse_args( $args, $defaults );

		ob_start();

		$affiliate_id = affwp_get_affiliate_id();
		if($affiliate_id) {

			$site_admins = '';
			$users_query = new WP_User_Query( array(
					'role' => 'administrator',
					'orderby' => 'display_name'
			) );
			$results = $users_query->get_results();
			foreach($results as $user)
			{
				$site_admins .=  $user->ID.',';
			}
			$admin_id=substr($site_admins,0,-1);
			$creatives_vendor = $wpdb->get_results("SELECT
			*,(SELECT affiliate_id FROM wp_affiliate_wp_affiliates s1 WHERE s1.user_id = k.store_id) as affiliate_referal
			FROM wp_affiliate_wp_affiliates s
 			INNER JOIN wp_vendor_affiliates c ON c.affiliate_id = s.affiliate_id
            INNER JOIN wp_affiliate_wp_creatives k  ON k.store_id = c.vendor_id and k.store_id NOT IN (".$admin_id.")
            INNER JOIN wp_users m  ON m.ID = c.vendor_id
            and c.affiliate_id = " . $affiliate_id . " and k.status='active' order by k.creative_id desc
             ");



			$creatives_admin = $wpdb->get_results(" SELECT  *,'".affwp_get_affiliate_rate( $admin_id ,true)."' as rate,
			'Do It Yourself Nation Advertisements' as email_admin,
			(SELECT affiliate_id FROM wp_affiliate_wp_affiliates s1 WHERE s1.user_id = k.store_id) as affiliate_referal
			FROM  wp_affiliate_wp_creatives k
		    INNER JOIN wp_users m  ON m.ID = k.store_id
 			and k.store_id IN (".$admin_id.") and k.status='active' order by k.creative_id desc ");



			$creatives=array_merge($creatives_admin,$creatives_vendor);
			usort($creatives, array($this, "cmp"));



			if ($creatives) {
				foreach ($creatives as $key => $creative) {
					$url = $creative->url;
					$image = $creative->image;
					$text = $creative->text;
					$affiliate_referal = $creative->affiliate_referal;
					//$rate1=get_user_meta( $creative->ID, 'pv_shop_affiliates_rate',true);
					$rate1=$creative->rate;
					if(substr($rate1,strlen($rate1)-1,1)=="%"){
						$rate=$rate1;
					}else{
						$rate = str_replace($rate1,$rate1,$rate1.'%');
					}
					//$rate=$rate1[0];
					//$rate=affwp_get_affiliate_rate( $creative->affiliate_id,true);

					$user = !empty($creative->user_nicename) ? $creative->user_nicename : $creative->user_login;
					$email_admin = !empty($creative->email_admin) ? $creative->email_admin : $creative->email_admin;
					$desc = !empty($creative->description) ? $creative->description : '';
					echo $this->html($creative->creative_id, $url, $image, '', $args['preview'], $text, $desc,$user ,$rate ,$affiliate_referal ,$email_admin);
				}
			}
		}


		return ob_get_clean();
	}

	/**
	 * Returns the referral link to append to the end of a URL
	 *
	 * @since  1.2
	 * @return string Affiliate's referral link
	 */
	public function ref_link( $url = '' ) {
		return affwp_get_affiliate_referral_url( array( 'base_url' => $url ) );
	}

	/**
	 * Shortcode HTML
	 *
	 * @since  1.2
	 * @param  $image the image URL. Either the URL from the image column in DB or external URL of image.
	 * @return string
	 */
	public function html( $id = '', $url, $image_link, $image_attributes, $preview, $text, $desc = '' , $user ,$rate ,$affiliate_referal ,$email_admin) {

		global $affwp_creative_atts;

		$id_class = $id ? ' creative-' . $id : '';

		$affwp_creative_atts = array(
				'id'                  => $id,
				'url'                 => $url,
				'id_class'            => $id_class,
				'desc'                => $desc,
				'preview'             => $preview,
				'image_attributes'    => $image_attributes,
				'image_link'          => $image_link,
				'text'                => $text,
				'user'                => $user,
				'rate'                => $rate,
				'affiliate_referal'   => $affiliate_referal,
				'email_admin'   => $email_admin
		);

		ob_start();

		affiliate_wp()->templates->get_template_part( 'creative' );

		$html = ob_get_clean();
		return apply_filters( 'affwp_affiliate_creative_html', $html, $url, $image_link, $image_attributes, $preview, $text, $user , $rate , $affiliate_referal ,$email_admin);	}

}