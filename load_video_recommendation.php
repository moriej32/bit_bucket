<?php 

define('WP_USE_THEMES', false);
if (! isset($wp_did_header)):
if ( !file_exists( dirname(__FILE__) . '/wp-config.php') ) {
	if (strpos($_SERVER['PHP_SELF'], 'wp-admin') !== false) $path = '';
	else $path = 'wp-admin/';
require_once( dirname(__FILE__) . '/wp-includes/classes.php');
require_once( dirname(__FILE__) . '/wp-includes/functions.php');
require_once( dirname(__FILE__) . '/wp-includes/plugin.php');

wp_die();
}
$wp_did_header = true;
require_once( dirname(__FILE__) . '/wp-config.php');

require_once(ABSPATH . WPINC . '/template-loader.php');
endif;

if (!isset($_GET['from'])) exit;
$from = intval($_GET['from'] - 1);
if($user_id == 0){
	$res = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_video_recommendation AS video WHERE p.ID = video.post_id AND p.post_type = 'post' GROUP BY p.ID ORDER BY video.interest  DESC LIMIT 10,$from");
}else{
	$res = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_video_recommendation AS video WHERE p.ID = video.post_id AND p.post_type = 'post' AND p.post_author = video.user_id ORDER BY video.interest DESC LIMIT 10,$from");	
}

if($user_id == 0){
	$video = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_video_recommendation AS video WHERE p.ID = video.post_id AND p.post_type = 'post' GROUP BY p.ID ORDER BY video.interest");
}else{
	$video = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_video_recommendation AS video WHERE p.ID = video.post_id AND p.post_type = 'post' AND p.post_author = video.user_id ORDER BY video.interest DESC LIMIT 20");
}
if(count($video) >= $from){
	$remain_videos = intval(count($video) - $from);
}else{
	$remain_videos = 1;
}

$videos = array();
foreach ($res as $r) {
	$video_id = $r->ID;
	$img_thumb = wp_get_attachment_url(get_post_thumbnail_id($video_id));		
	$endorsement = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $video_id);
	$favorite = $wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $video_id);
	$download = get_post_meta( $video_id, 'dyn_download_count', true);  
	if($download == ""){$downloads = '0 Download';}else{$downloads = $download. "Downloads";}	
	$first_name = get_user_meta($r->post_author, 'first_name', true);
	$last_name = get_user_meta($r->post_author, 'last_name', true);
	if($first_name != null && $last_name != null){
		$user = $first_name ." ".$last_name;
	}else{
		$user = get_user_meta($r->post_author, 'nickname', true);
	}

	$output =array();
	$a = array(
		"user" => $user,
		"feature_image" => "$img_thumb", 
		"count_view" => videopress_countviews_showmore($file_id), 
		"endorsement" => count($endorsement),
		"display_review" => apply_filters( 'dyn_display_review', $output, $file_id, "post" ),
		"favorites" => count($favorite),
		"dyn_number_post_review" => apply_filters( 'dyn_number_of_post_review',$file_id, "post"),
		"downloads" => $downloads,
		"dyn_file_images" => apply_filters( 'dyn_file_image', $outputs, $file_id),
		"short_description" => wp_trim_words( $r->post_content, 15, '[...]' ),
		"remain_videos" => $remain_videos
	);
	$videos[] = array_merge(array($r), $a);
}	

echo json_encode($videos);

?>