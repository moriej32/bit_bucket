#!/usr/bin/php
<?php 

$email_template = '
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body style="background:rgba(19, 5, 5, 0.11);font-size: 10px;">
<table class="table" style="background:#3498db; margin-bottom: 1;">
    <tr>
        <td style="width: 13%;"></td>
        <td style="padding: 5px;"><img src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/DYN-logo-white-small.png"/></td>
    </tr>
</table>

<center>
<table style="width:74%; background: white; border: 1px solid #569cc0;">
    <tbody style="font-size: 14px !important;">
    <tr>
        <td style="padding:15px;">
            <p style="line-height: 200%;"><b>Banner ads</b></p>
            <table>
                <tbody style="font-size: 14px !important;">
                <tr>
                    <td width="150px">Ad cost :</td>
                    <td>%cost% USD</td>
                </tr>
                <tr>
                    <td width="150px">Target URL : </td>
                    <td >%target_url%</td>
                </tr>
                <tr>
                    <td width="150px">Image URL :</td>
                    <td ><a href="%img_url%" target="_blank">Click here to view image</a></td>
                </tr>
				<tr>
                    <td width="150px">Purchase Code :</td>
                    <td ><a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/stats.php?rand=%code%" target="_blank">%code%</a></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-left:15px;">
            </br>
            <p><i><b>Update this ad</b></i></p>
            <p>Please contact us to update your ad : <a>dynation4@gmail.com</a></p>
        </td>
    </tr>
    <tr>
        <td style="padding:15px;">
            <p style="padding-bottom: 10px;"><i><b>Daily Statistics</b></i></p>
            <table style="width: 100%;">
                <tbody style="font-size: 14px !important;">
                <th>
                    <tr>
                        <td style=" border-right: 1px solid white;">Date</td>
                        <td style=" border-right: 1px solid white;">Total Clicks</td>
                        <td style=" border-right: 1px solid white;">Clicks Remaining</td>
                        <td style=" border-right: 1px solid white;">Money Spent</td>
                        <td style=" border-right: 1px solid white;">Money Remaining</td>
                    </tr>
                </th>
                <tr>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%date%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%total_click%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%click_left%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%cost_spent%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%cost_left%</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin:15px;border: 1px dashed #000;padding:5px; line-height: 150%;">
                1) Total Clicks:= Total number of clicks for your ad campaign<br>
                2) Clicks Remaining:= Total number of clicks remaining in your ad campaign<br>
                3) Money Spent:= The total number of money spent on your advertisement that day<br>
                4) Money Remaining:= Money remaining in your ad campaign<br>
            </p>
        </td>
    </tr>
    </tbody>
</table>
</center>
</body>
</html>';

$video_template = '
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body style="background:rgba(19, 5, 5, 0.11);font-size: 10px;">
<table class="table" style="background:#3498db; margin-bottom: 1;">
    <tr>
        <td style="width: 13%;"></td>
        <td style="padding: 5px;"><img src="http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/DYN-logo-white-small.png"/></td>
    </tr>
</table>

<center>
<table style="width:74%; background: white; border: 1px solid #569cc0;">
    <tbody style="font-size: 14px !important;">
    <tr>
        <td style="padding:15px;">
            <p style="line-height: 200%;"><b>Video ads</b></p>
            <table>
                <tbody style="font-size: 14px !important;">
                <tr>
                    <td width="150px">Ad cost :</td>
                    <td>%cost% USD</td>
                </tr>
                <tr>
                    <td width="150px">Target URL : </td>
                    <td >%target_url%</td>
                </tr>
                <tr>
                    <td width="150px">Video URL :</td>
                    <td ><a href="%img_url%" target="_blank">Click here to view Video</a></td>
                </tr>
				<tr>
                    <td width="150px">Purchase Code :</td>
                    <td ><a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/stats.php?rand=%code%" target="_blank">%code%</a></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-left:15px;">
            </br>
            <p><i><b>Update this ad</b></i></p>
            <p>Please contact us to update your ad : <a>dynation4@gmail.com</a></p>
        </td>
    </tr>
    <tr>
        <td style="padding:15px;">
            <p style="padding-bottom: 10px;"><i><b>Daily Statistics</b></i></p>
            <table style="width: 100%;">
                <tbody style="font-size: 14px !important;">
                <th>
                    <tr>
                        <td style=" border-right: 1px solid white;">Date</td>
                        <td style=" border-right: 1px solid white;">Total Views</td>
                        <td style=" border-right: 1px solid white;">Views Remaining</td>
                        <td style=" border-right: 1px solid white;">Money Spent</td>
                        <td style=" border-right: 1px solid white;">Money Remaining</td>
                    </tr>
                </th>
                <tr>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%date%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%total_click%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%click_left%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%cost_spent%</td>
                    <td bgcolor="#e8e7f7" style=" border-right: 1px solid white;">%cost_left%</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin:15px;border: 1px dashed #000;padding:5px; line-height: 150%;">
                1) Total Views:= Total number of Views for your ad campaign<br>
                2) Views Remaining:= Total number of Views remaining in your ad campaign<br>
                3) Money Spent:= The total number of money spent on your advertisement that day<br>
                4) Money Remaining:= Money remaining in your ad campaign<br>
            </p>
        </td>
    </tr>
    </tbody>
</table>
</center>
</body>
</html>';

$servername = "diynation-dbinstances.ceduveddnjx7.us-west-2.rds.amazonaws.com";
$username = "doitnation";
$password = "z91PKzM6ygIwXrb";
$dbname = "doitnew";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$conn) { 
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM wp_oiopub_purchases where item_status=1 and payment_status=1 and (item_channel=5 or item_channel=7 or item_channel=8)";

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
	
	$subject = "Do It Yourself Nation - Daily Ad Stats";
    $date_today = date('Y-m-d');		 
	
    while($row = mysqli_fetch_assoc($result)) { 
	
		$email_template1 = str_replace("%date%",$date_today,$email_template);
		$to = $row["adv_email"];
		$cost = $row["payment_amount"];
		$total_click = $row["item_duration"];
		$click_left = $row["item_duration_left"];
		$cost_left = ($cost * $click_left)/$total_click ;
		$cost_spent = $cost - $cost_left;
		$img_url = $row["item_url"];
		$code = $row["rand_id"];
		$target_url = $row["item_page"];
		$email_template1 = str_replace("%cost%",$cost,$email_template1);
		$email_template1 = str_replace("%target_url%",$target_url,$email_template1);
		$email_template1 = str_replace("%img_url%",$img_url,$email_template1);
		$email_template1 = str_replace("%total_click%",$total_click,$email_template1);
		$email_template1 = str_replace("%click_left%",$click_left,$email_template1);
		$email_template1 = str_replace("%cost_spent%",$cost_spent,$email_template1);
		$email_template1 = str_replace("%code%",$code,$email_template1);
		$email_template1 = str_replace("%cost_left%",$cost_left,$email_template1);
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <dynation4@gmail.com>' . "\r\n";
		mail($to,$subject,$email_template1,$headers);
		//exit;
    }
} 

$sql1 = "SELECT * FROM wp_oiopub_purchases where item_status=1 and payment_status=1 and item_channel=6";

$result1 = mysqli_query($conn, $sql1);

if (mysqli_num_rows($result1) > 0) {
	
	$subject = "Do It Yourself Nation - Daily Video Ad Stats";
    $date_today = date('Y-m-d');		 
	
    while($row1 = mysqli_fetch_assoc($result1)) { 
	
		$video_template1 = str_replace("%date%",$date_today,$video_template);
		$to = $row1["adv_email"];
		$cost = $row1["payment_amount"];
		$total_click = $row1["item_duration"];
		$click_left = $row1["item_duration_left"];
		$cost_left = ($cost * $click_left)/$total_click ;
		$cost_spent = $cost - $cost_left;
		$img_url = $row1["item_url"];
		$code = $row1["rand_id"];
		$target_url = $row1["item_page"];
		$video_template1 = str_replace("%cost%",$cost,$video_template1);
		$video_template1 = str_replace("%target_url%",$target_url,$video_template1);
		$video_template1 = str_replace("%img_url%",$img_url,$video_template1);
		$video_template1 = str_replace("%total_click%",$total_click,$video_template1);
		$video_template1 = str_replace("%click_left%",$click_left,$video_template1);
		$video_template1 = str_replace("%cost_spent%",$cost_spent,$video_template1);
		$video_template1 = str_replace("%code%",$code,$video_template1);
		$video_template1 = str_replace("%cost_left%",$cost_left,$video_template1);
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <dynation4@gmail.com>' . "\r\n";
		mail($to,$subject,$video_template1,$headers);
		//exit;
    }
} 

mysqli_close($conn);

?>

  




