<?php
require_once("wp-load.php");
global $wpdb;

$userID = $_REQUEST['userID'];
$postID = $_REQUEST['postID'];
$privacyOption = $_REQUEST['privacyOptionV']; 
update_post_meta( $postID, 'privacy-option', $privacyOption);
if($_REQUEST['select_users_list'] != '')
{
	$select_users_list = implode(",", $_REQUEST['select_users_list']);
	update_post_meta( $postID, 'select_users_list', $select_users_list);
}

echo "<span style='color: blue;'>Privacy Settings has been updated successfully</span>";
?>