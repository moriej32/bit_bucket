<?PHP
/*
	Template Name: After Subscription
*/

// STEP 1: Read POST data

// reading posted data from directly from $_POST causes serialization 
// issues with array data in POST
// reading raw POST data from input stream instead. 


/* $servername = "diynation-dbinstances.ceduveddnjx7.us-west-2.rds.amazonaws.com";
	$username = "doitnation";
	$password = "z91PKzM6ygIwXrb";
	$dbname = "doitnew";
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "INSERT INTO wp_after_subscription (user_id, payment_status)
	VALUES (5, 'created')";

	if ($conn->query($sql) === TRUE) {
		echo "New record created successfully";
	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}

	$conn->close(); */

class Paypal_IPN{
	
	/*
	**param string mode 'Live' or 'sandbox'
	*/
	public function __construct($mode = 'live')
	{
		if($mode == 'live')
			$this->_url = 'https://www.paypal.com/cgi-bin/webscr';
		
		else
			$this->_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	}
	
	public function run()
	{
		$postFields = 'cmd=_notify-validate';
		
		foreach($_POST as $key=>$value)
		{
			$postFields .= "&$key=".urlencode($value);
		}
		
		$ch = curl_init();
		
		curl_setopt_array($ch, array(
			CURLOPT_URL => $this->_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $postFields			
		));
		
		$result = curl_exec($ch);
		curl_close($ch);
		
		/* if($result == 'VERIFIED'){
			$servername = "diynation-dbinstances.ceduveddnjx7.us-west-2.rds.amazonaws.com";
			$username = "doitnation";
			$password = "z91PKzM6ygIwXrb";
			$dbname = "doitnew";
			$conn = new mysqli($servername, $username, $password, $dbname);
			// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			} 

			$sql = "INSERT INTO wp_after_subscription (user_id, payment_status)
			VALUES (5, 'created')";

			if ($conn->query($sql) === TRUE) {
				echo "New record created successfully";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			$conn->close();
		} */
		
		echo $result;
	}
	
}


$paypal = new Paypal_IPN('sandbox');
$paypal->run();