<?php 

define('WP_USE_THEMES', false);
if (! isset($wp_did_header)):
if ( !file_exists( dirname(__FILE__) . '/wp-config.php') ) {
	if (strpos($_SERVER['PHP_SELF'], 'wp-admin') !== false) $path = '';
	else $path = 'wp-admin/';
require_once( dirname(__FILE__) . '/wp-includes/classes.php');
require_once( dirname(__FILE__) . '/wp-includes/functions.php');
require_once( dirname(__FILE__) . '/wp-includes/plugin.php');

wp_die();
}
$wp_did_header = true;
require_once( dirname(__FILE__) . '/wp-config.php');

require_once(ABSPATH . WPINC . '/template-loader.php');
endif;

if (!isset($_GET['from'])) exit;
$from = intval($_GET['from'] - 1);
$user_id = get_current_user_id();
if($user_id == 0){	
	$res = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_book_recommendation AS book WHERE p.post_type = 'dyn_book' AND p.ID = book.post_id GROUP BY p.ID ORDER BY book.interest DESC LIMIT 10 OFFSET $from");
}else{
	$res = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_book_recommendation AS book WHERE p.post_type = 'dyn_book' AND p.ID = book.post_id AND p.post_author = book.user_id ORDER BY book.interest DESC LIMIT 10 OFFSET $from");	
}
if($user_id == 0){
	$book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_book_recommendation AS book WHERE p.post_type = 'dyn_book' AND p.ID = book.post_id GROUP BY p.ID ORDER BY book.interest");
}else{
	$book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_book_recommendation AS book WHERE p.post_type = 'dyn_book' AND p.ID = book.post_id AND p.post_author = book.user_id ORDER BY book.interest");
}
if(count($book) >= $from){
	$remain_books = intval(count($book) - $from);
}else{
	$remain_books = 1;
}

$books = array();
foreach ($res as $r) {
	$book_id = $r->ID;
	$img_thumb = wp_get_attachment_url(get_post_thumbnail_id($book_id));		
	$endorsement = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $book_id);
	$favorite = $wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $book_id);
	$download = get_post_meta( $book_id, 'dyn_download_count', true);  
	if($download == ""){$downloads = '0 Download';}else{$downloads = $download. " Downloads";}	
	$first_name = get_user_meta($r->post_author, 'first_name', true);
	$last_name = get_user_meta($r->post_author, 'last_name', true);
	if($first_name != "" && $last_name != ""){
		$user = $first_name ." ".$last_name;
	}else{
		$user = get_user_meta($r->post_author, 'nickname', true);
	}
	$output =array();
	$outputs =array();

	$a = array(
		"user" => $user,
		"feature_image" => $img_thumb, 
		"count_view" => videopress_countviews_showmore($book_id), 
		"endorsement" => count($endorsement),
		"display_review" => apply_filters( 'dyn_display_review', $output, $book_id, "post" ),
		"favorites" => count($favorite),
		"dyn_number_post_review" => apply_filters( 'dyn_number_of_post_review',$book_id, "post"),
		"downloads" => $downloads,
		"short_description" => wp_trim_words( $r->post_content, 15, '[...]' ),
		"remain_books" => $remain_books
	);
	$books[] = array_merge(array($r), $a);
}	
echo json_encode($books);

?>